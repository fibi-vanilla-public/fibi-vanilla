DELIMITER //
CREATE  PROCEDURE `UPDATE_AWD_REPREQ_DUE_DT`(AV_AWARD_ID INT)
BEGIN
	DECLARE LI_AWARD_ID INT(11);	
	DECLARE LS_FREQUENCY_CODE VARCHAR(3);
	DECLARE LS_AWD_END_DATE DATETIME;
	DECLARE LS_REPORT_STS_PD VARCHAR(3);
	DECLARE	LS_FREQ_TYPE VARCHAR(20);
	DECLARE	LS_FREQ_VALUE VARCHAR(20);
	DECLARE LI_COUNT INT;
	DECLARE LI_AWD_REP_TERMS_ID INT;
	DECLARE LS_TRK_START_DATE DATETIME;
	DECLARE DONE1 BOOLEAN DEFAULT FALSE;
	DECLARE LS_ERROR VARCHAR(2000); 
	DECLARE AWD_RPT_TERMS_CURSOR CURSOR FOR 			
		SELECT  AR.AWARD_ID,
				AR.FREQUENCY_CODE, 
				AR.AWARD_REPORT_TERMS_ID,
			   (SELECT FINAL_EXPIRATION_DATE FROM AWARD WHERE AWARD_ID = AR.AWARD_ID) AWD_END_DATE
		  FROM AWARD_REPORT_TERMS AR,
			   FREQUENCY_BASE FB
		 WHERE AR.AWARD_ID = AV_AWARD_ID
		   AND FB.FREQUENCY_BASE_CODE = AR.FREQUENCY_BASE_CODE
		   AND UPPER(FB.DESCRIPTION) = UPPER('Project End Date')
		   AND EXISTS ( SELECT 1
						  FROM AWARD_REPORT_TRACKING ART,
							   REPORT_STATUS RS
						 WHERE ART.AWARD_REPORT_TERMS_ID = AR.AWARD_REPORT_TERMS_ID
						   AND ART.AWARD_ID = AR.AWARD_ID
						   AND RS.REPORT_STATUS_CODE = ART.STATUS_CODE
						   AND UPPER(RS.DESCRIPTION) = 'PENDING'
						)
			AND NOT EXISTS (SELECT 1 
							  FROM FREQUENCY FR 
							 WHERE FR.FREQUENCY_CODE = AR.FREQUENCY_CODE
							   AND UPPER(FR.DESCRIPTION) = UPPER('As required')	
							) ;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE1 = TRUE;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION 
	BEGIN
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, 
		@errno = MYSQL_ERRNO, @msg = MESSAGE_TEXT;
		SET @full_error = CONCAT("ERROR ", @errno, " (", @sqlstate, "): ", @msg);
		SELECT @full_error INTO LS_ERROR;
		INSERT INTO AWD_REPREQ_DUEDT_LOG 
			(AWARD_ID,AWARD_REPORT_TERMS_ID,ERROR_MESSAGE,UPDATE_TIMESTAMP,UPDATE_USER)
		VALUES  (LI_AWARD_ID,LI_AWD_REP_TERMS_ID,CONCAT(LS_ERROR),UTC_TIMESTAMP(),'quickstart');		
		COMMIT;	
		SET DONE1 = TRUE;
	END;	
	SELECT COUNT(*)
	  INTO LI_COUNT
	  FROM REPORT_STATUS
	WHERE UPPER(DESCRIPTION) = 'PENDING';
	IF LI_COUNT = 0 THEN
		INSERT INTO AWD_REPREQ_DUEDT_LOG 
			(AWARD_ID,AWARD_REPORT_TERMS_ID,ERROR_MESSAGE,UPDATE_TIMESTAMP,UPDATE_USER)
		VALUES  (LI_AWARD_ID,LI_AWD_REP_TERMS_ID,'No Record found for Pending status in lookup',UTC_TIMESTAMP(),'quickstart');
		SET DONE1 = TRUE;
	END IF;
	IF DONE1 = FALSE THEN
	   SELECT REPORT_STATUS_CODE
	     INTO LS_REPORT_STS_PD
	     FROM REPORT_STATUS
	    WHERE UPPER(DESCRIPTION) = 'PENDING';
		OPEN AWD_RPT_TERMS_CURSOR;
		AWD_RPT_TERMS_CURSOR_LOOP : LOOP
			FETCH AWD_RPT_TERMS_CURSOR INTO LI_AWARD_ID,
											LS_FREQUENCY_CODE, 
											LI_AWD_REP_TERMS_ID,
											LS_AWD_END_DATE;
				IF DONE1 THEN				
					LEAVE AWD_RPT_TERMS_CURSOR_LOOP;
				END IF;
				SELECT COUNT(*)
				  INTO LI_COUNT
				  FROM AWARD_REPORT_TRACKING ART
				 WHERE ART.AWARD_REPORT_TERMS_ID = LI_AWD_REP_TERMS_ID
				   AND ART.STATUS_CODE = LS_REPORT_STS_PD;
				IF LI_COUNT > 1 THEN
				   INSERT INTO AWD_REPREQ_DUEDT_LOG 
			             (AWARD_ID,AWARD_REPORT_TERMS_ID,ERROR_MESSAGE,UPDATE_TIMESTAMP,UPDATE_USER)
					VALUES  (LI_AWARD_ID,LI_AWD_REP_TERMS_ID,'Multiple pending entries in Report Tracking Table',UTC_TIMESTAMP(),'quickstart');	
					LEAVE AWD_RPT_TERMS_CURSOR_LOOP;
				END IF;						
				SELECT COUNT(*)
				  INTO LI_COUNT
				  FROM FREQUENCY
				 WHERE UPPER(FREQUENCY_CODE) = UPPER(TRIM(LS_FREQUENCY_CODE))
				 AND IS_INVOICE_FREQUENCY = 'N';
				IF LI_COUNT <> 1 THEN
				   INSERT INTO AWD_REPREQ_DUEDT_LOG 
			             (AWARD_ID,AWARD_REPORT_TERMS_ID,ERROR_MESSAGE,UPDATE_TIMESTAMP,UPDATE_USER)
					VALUES  (LI_AWARD_ID,LI_AWD_REP_TERMS_ID,'No Record found for Frequency code in lookup',UTC_TIMESTAMP(),'quickstart');	
					LEAVE AWD_RPT_TERMS_CURSOR_LOOP;
				END IF;	 
				SELECT CASE WHEN  ABS(IFNULL(NUMBER_OF_DAYS,0)) > 0  THEN 'DAY'
							WHEN  ABS(IFNULL(ADVANCE_NUMBER_OF_DAYS,0)) > 0  THEN 'DAY'
							WHEN  ABS(IFNULL(NUMBER_OF_MONTHS,0)) THEN 'MONTH'
							WHEN  ABS(IFNULL(ADVANCE_NUMBER_OF_MONTHS,0)) THEN 'MONTH'
						END ,
						CASE WHEN  ABS(IFNULL(NUMBER_OF_DAYS,0)) > 0  THEN NUMBER_OF_DAYS
							 WHEN  ABS(IFNULL(ADVANCE_NUMBER_OF_DAYS,0)) > 0  THEN ADVANCE_NUMBER_OF_DAYS * -1
							 WHEN  ABS(IFNULL(NUMBER_OF_MONTHS,0)) THEN NUMBER_OF_MONTHS
							 WHEN  ABS(IFNULL(ADVANCE_NUMBER_OF_MONTHS,0)) THEN ADVANCE_NUMBER_OF_MONTHS * -1
						END 
				   INTO LS_FREQ_TYPE,
						LS_FREQ_VALUE
				   FROM FREQUENCY 
			   WHERE UPPER(FREQUENCY_CODE) = UPPER(TRIM(LS_FREQUENCY_CODE))
				 AND IS_INVOICE_FREQUENCY = 'N'; 	
				IF LS_FREQ_TYPE = 'MONTH' THEN
					SET LS_TRK_START_DATE =  DATE_ADD(LS_AWD_END_DATE, INTERVAL LS_FREQ_VALUE MONTH);
				ELSE
					SET LS_TRK_START_DATE =  DATE_ADD(LS_AWD_END_DATE, INTERVAL LS_FREQ_VALUE DAY);
				END IF;
				SET SQL_SAFE_UPDATES = 0;	
				UPDATE AWARD_REPORT_TRACKING ART	
				   SET DUE_DATE = LS_TRK_START_DATE,
					   UPDATE_TIMESTAMP = UTC_TIMESTAMP()
				 WHERE ART.AWARD_ID = LI_AWARD_ID
				   AND ART.AWARD_REPORT_TERMS_ID = LI_AWD_REP_TERMS_ID
				   AND ART.STATUS_CODE = LS_REPORT_STS_PD;				
				SET SQL_SAFE_UPDATES = 1;	
				COMMIT;		
		END LOOP;
		CLOSE AWD_RPT_TERMS_CURSOR;
	END IF;	
END
//
