DELIMITER //
CREATE  PROCEDURE `GET_AWARD_REVENUE_TRACKER_REPORT`()
BEGIN
SELECT 
    T3.UNIT_NUMBER,
    T3.UNIT_NAME,
    T3.CAMPUS,
    T1.REVENUE_TRACKER_ID,
    T1.AWARD_NUMBER,
    T1.ACCOUNT_NUMBER,
    T1.INTERNAL_ORDER_CODE,
    T1.REMARKS,
    T1.AMOUNT_IN_FMA_CURRENCY,
    T1.ACTUAL_OR_COMMITTED_FLAG,
    T1.ENTRY_DATE,
    T1.FI_POSTING_DATE,
    T1.BP_CODE,
    T1.BP_NAME,
    T1.DOCUMENT_DATE,
    T1.FI_GL_ACCOUNT,
    T1.FI_GL_DESCRIPTION,
    T1.TRANSACTION_REFERENCE_NUMBER,
    T1.REFERENCE_DOCUMENT_NUMBER,
    T1.REFERENCE_POSTING_LINE,
    T1.GUID,
    T1.GMIA_DOCNR,
    T1.DOCLN,
    T1.RBUKRS,
    T1.RVALUETYPE_9,
    T1.RYEAR,
    T1.GL_SIRID,
    T1.BATCH_ID,
    T1.GRANT_NBR,
    T1.SPONSOR_PROGRAM,
    T1.SPONSOR_CLASS,
    T1.FUND,
    T1.FUND_CENTER
FROM
    AWARD_REVENUE_TRANSACTIONS T1
        LEFT OUTER JOIN
    AWARD T2 ON T2.ACCOUNT_NUMBER = T1.ACCOUNT_NUMBER
        AND T2.AWARD_SEQUENCE_STATUS = 'ACTIVE'
        LEFT OUTER JOIN
    UNIT T3 ON T3.UNIT_NUMBER = T2.LEAD_UNIT_NUMBER
WHERE
    T1.FILE_ID IS NOT NULL;
END
//
