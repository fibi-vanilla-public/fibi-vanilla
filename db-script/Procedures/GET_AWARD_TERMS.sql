DELIMITER //
CREATE  PROCEDURE `GET_AWARD_TERMS`(
IN AV_AWARD_ID INT)
    DETERMINISTIC
BEGIN
    SELECT  T1.AWARD_ID,
           T1.AWARD_NUMBER,
           T2.SPONSOR_TERM_TYPE_CODE,
           T3.DESCRIPTION AS SPONSOR_TERM_TYPE,
           T2.DESCRIPTION AS SPONSOR_TERM
    FROM AWARD_SPONSOR_TERM T1
    INNER JOIN SPONSOR_TERM T2 ON T1.SPONSOR_TERM_CODE = T2.SPONSOR_TERM_ID
    INNER JOIN SPONSOR_TERM_TYPE T3 ON T2.SPONSOR_TERM_TYPE_CODE = T3.SPONSOR_TERM_TYPE_CODE
    WHERE T1.AWARD_ID = AV_AWARD_ID;
END
//
