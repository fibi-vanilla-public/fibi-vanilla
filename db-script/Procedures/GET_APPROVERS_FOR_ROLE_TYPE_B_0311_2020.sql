DELIMITER //
CREATE  PROCEDURE `GET_APPROVERS_FOR_ROLE_TYPE_B_0311_2020`(
  AV_MODULE_CODE     DECIMAL(38,0),
  AV_MODULE_ITEM_ID  VARCHAR(20),
  AV_ROLE_TYPE   DECIMAL(38,0) ,
  AV_SUBMODULE_CODE    DECIMAL(38,0),
  AV_SUB_MODULE_ITEM_KEY   VARCHAR(20)
  )
    DETERMINISTIC
BEGIN
DECLARE LI_RETURN    DECIMAL(38,0);
DECLARE LS_LEAD_UNIT VARCHAR(8);
DECLARE LS_SUBMITING_UNIT_NUMBER VARCHAR(8);
DECLARE  LS_UNIT_ADMIN_TYPE_CODE DECIMAL(38,0);
DECLARE  ls_create_user VARCHAR(20);
DECLARE LS_CODE CHAR(5) DEFAULT '00000';
DECLARE LS_MSG TEXT;
DECLARE LI_MODULE_CODE INT(3);
DECLARE LS_MODULE_ITEM_KEY VARCHAR(20);
DECLARE LS_AWARD_ID      VARCHAR(22);
DECLARE LS_AWARD_NUMBER  VARCHAR(12);
DECLARE LI_FLAG INT;
DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
BEGIN
  GET DIAGNOSTICS CONDITION 1
        LS_CODE = RETURNED_SQLSTATE, LS_MSG = MESSAGE_TEXT;
END;
    IF AV_MODULE_CODE = 1 THEN
            SELECT LEAD_UNIT_NUMBER
            INTO LS_LEAD_UNIT
            FROM AWARD
            WHERE AWARD_ID = AV_MODULE_ITEM_ID;
    ELSEIF AV_MODULE_CODE = 3 THEN
            SELECT HOME_UNIT_NUMBER
            INTO LS_LEAD_UNIT
            FROM EPS_PROPOSAL
            WHERE PROPOSAL_ID = AV_MODULE_ITEM_ID;
        ELSEIF AV_MODULE_CODE = 12 THEN
                SELECT UNIT_NUMBER
                INTO LS_LEAD_UNIT
                FROM AGREEMENT_HEADER
                WHERE AGREEMENT_REQUEST_ID = AV_MODULE_ITEM_ID;
                IF LS_LEAD_UNIT IS NULL THEN
                        SELECT UNIT_NUMBER
                        INTO LS_LEAD_UNIT
                        FROM UNIT WHERE PARENT_UNIT_NUMBER IS NULL;
                END IF;
        ELSEIF AV_MODULE_CODE = 15 THEN
               SELECT HOME_UNIT_NUMBER
                   INTO LS_LEAD_UNIT
                   FROM GRANT_CALL_HEADER
                   WHERE GRANT_HEADER_ID = AV_MODULE_ITEM_ID;
        ELSEIF AV_MODULE_CODE = 20 THEN
                   SELECT MODULE_CODE,MODULE_ITEM_KEY INTO LI_MODULE_CODE,LS_MODULE_ITEM_KEY
                   FROM SR_HEADER T1
                   WHERE T1.SR_HEADER_ID = AV_MODULE_ITEM_ID;
                   IF LI_MODULE_CODE = 1 THEN
                                                SELECT AWARD_NUMBER INTO LS_AWARD_NUMBER
                                                FROM AWARD WHERE AWARD_ID = LS_MODULE_ITEM_KEY;
                                                SELECT AWARD_ID,LEAD_UNIT_NUMBER INTO LS_AWARD_ID,LS_LEAD_UNIT
                                                FROM AWARD WHERE SEQUENCE_NUMBER =(
                                                                                                                        SELECT T1.SEQUENCE_NUMBER+1
                                                                                                                        FROM AWARD T1
                                                                                                                        WHERE T1.AWARD_ID = LS_MODULE_ITEM_KEY
                                                                                                                   )
                                                AND AWARD_NUMBER = LS_AWARD_NUMBER;
                        ELSEIF LI_MODULE_CODE = 3 THEN
                                                SELECT HOME_UNIT_NUMBER INTO LS_LEAD_UNIT
                                                FROM EPS_PROPOSAL
                                                WHERE PROPOSAL_ID = LS_MODULE_ITEM_KEY;
                   END IF;
    END IF;
 IF AV_ROLE_TYPE IN (1,3) THEN
                IF AV_MODULE_CODE = 15  AND AV_SUBMODULE_CODE = 1 THEN
                        SELECT SUBMITING_UNIT_NUMBER INTO  LS_SUBMITING_UNIT_NUMBER
                        FROM GRANT_CALL_IOI_HEADER
                        WHERE GRANT_HEADER_ID =AV_MODULE_ITEM_ID
                        AND IOI_ID = AV_SUB_MODULE_ITEM_KEY;
                        SELECT  PERSON_ID
                        FROM UNIT_ADMINISTRATOR
                        WHERE UNIT_NUMBER = LS_SUBMITING_UNIT_NUMBER
                        AND  UNIT_ADMINISTRATOR_TYPE_CODE =
                        (SELECT (CASE AV_ROLE_TYPE WHEN  1 THEN 1
                                                                          WHEN  3 THEN 4 END)  AS UNIT_ADMINISTRATOR_TYPE_CODE
                        FROM DUAL);
                ELSE
                        SELECT  PERSON_ID
                        FROM UNIT_ADMINISTRATOR
                        WHERE UNIT_NUMBER = LS_LEAD_UNIT
                        AND  UNIT_ADMINISTRATOR_TYPE_CODE =
                        (SELECT (CASE AV_ROLE_TYPE WHEN  1 THEN 1
                                                                          WHEN  3 THEN 4 END)  AS UNIT_ADMINISTRATOR_TYPE_CODE
                        FROM DUAL);
                 END IF;
        ELSEIF AV_ROLE_TYPE = 2  THEN
                        IF AV_MODULE_CODE = 12 THEN
                                SELECT DISTINCT PERSON_ID FROM  PERSON_ROLES
                                WHERE UNIT_NUMBER = LS_LEAD_UNIT
                                AND ROLE_ID = 400;
                        ELSE
                                SELECT DISTINCT T1.PERSON_ID
                                FROM PERSON_ROLES T1 LEFT JOIN PERSON T2 ON T1.PERSON_ID =T2.PERSON_ID
                                WHERE ROLE_ID = 400
                                AND T1.UNIT_NUMBER =  LS_LEAD_UNIT;
                        END IF;
    ELSEIF AV_ROLE_TYPE = 4  THEN
                                IF AV_MODULE_CODE = 5 THEN
                                        SELECT
                                                T1.NEGOTIATOR_PERSON_ID
                                        FROM NEGOTIATION T1
                                        WHERE T1.NEGOTIATION_ID = AV_MODULE_ITEM_ID;
                                ELSEIF AV_MODULE_CODE = 12 THEN
                                        SELECT NEGOTIATOR_PERSON_ID AS PERSON_ID
                                        FROM AGREEMENT_HEADER
                                        WHERE AGREEMENT_REQUEST_ID = AV_MODULE_ITEM_ID;
                                END IF;
    ELSEIF AV_ROLE_TYPE = 5  THEN
                                IF AV_MODULE_CODE = 1 THEN
                                        SELECT T2.PERSON_ID
                                        FROM AWARD T1
                                        INNER JOIN AWARD_PERSONS T2 ON T1.AWARD_ID = T2.AWARD_ID
                                        WHERE T1.AWARD_ID = AV_MODULE_ITEM_ID
                                        AND T2.PERSON_ROLE_ID = 3;
                                ELSEIF AV_MODULE_CODE = 3 THEN
                                                SELECT T2.PERSON_ID
                                                FROM EPS_PROPOSAL T1
                                                INNER JOIN EPS_PROPOSAL_PERSONS T2 ON T1.PROPOSAL_ID = T2.PROPOSAL_ID
                                                                                                                        AND T2.PROP_PERSON_ROLE_ID = 3
                                                WHERE T1.PROPOSAL_ID = AV_MODULE_ITEM_ID;
                                ELSEIF AV_MODULE_CODE = 20 THEN
                                           SELECT MODULE_CODE INTO LI_MODULE_CODE,LS_MODULE_ITEM_KEY
                                           FROM SR_HEADER T1
                                           WHERE T1.SR_HEADER_ID = AV_MODULE_ITEM_ID;
                                           IF LI_MODULE_CODE = 1 THEN
                                                                        SELECT T2.PERSON_ID
                                                                        FROM AWARD T1
                                                                        INNER JOIN AWARD_PERSONS T2 ON T1.AWARD_ID = T2.AWARD_ID
                                                                        WHERE T1.AWARD_ID = LS_AWARD_ID
                                                                        AND T2.PERSON_ROLE_ID = 3;
                                                ELSEIF LI_MODULE_CODE = 3 THEN
                                                                        SELECT T2.PERSON_ID
                                                                        FROM EPS_PROPOSAL T1
                                                                        INNER JOIN EPS_PROPOSAL_PERSONS T2 ON T1.PROPOSAL_ID = T2.PROPOSAL_ID
                                                                                                                                                AND T2.PROP_PERSON_ROLE_ID = 3
                                                                        WHERE T1.PROPOSAL_ID = LS_MODULE_ITEM_KEY;
                                           END IF;
                                ELSEIF AV_MODULE_CODE = 15  AND AV_SUBMODULE_CODE = 1 THEN
                                        SELECT PI_PERSON_ID AS PERSON_ID
                                        FROM GRANT_CALL_IOI_HEADER
                                        WHERE GRANT_HEADER_ID =AV_MODULE_ITEM_ID
                                        AND IOI_ID = AV_SUB_MODULE_ITEM_KEY;
                                 END IF;
    ELSEIF AV_ROLE_TYPE = 6  THEN
                                IF AV_MODULE_CODE = 1 THEN
                                        SELECT T2.PERSON_ID
                                        FROM AWARD T1
                                        INNER JOIN AWARD_PERSONS T2 ON T1.AWARD_ID = T2.AWARD_ID
                                        WHERE T1.AWARD_ID = AV_MODULE_ITEM_ID;
                                ELSEIF AV_MODULE_CODE = 3 THEN
                                        SELECT T1.PERSON_ID
                                        FROM  EPS_PROPOSAL_PERSONS T1
                                        WHERE T1.PROPOSAL_ID = AV_MODULE_ITEM_ID;
                                ELSEIF AV_MODULE_CODE = 20 THEN
                                   SELECT MODULE_CODE,MODULE_ITEM_KEY INTO LI_MODULE_CODE,LS_MODULE_ITEM_KEY
                                   FROM SR_HEADER T1
                                   WHERE T1.SR_HEADER_ID = AV_MODULE_ITEM_ID;
                                   IF LI_MODULE_CODE = 1 THEN
                                                                SELECT T2.PERSON_ID
                                                                FROM AWARD T1
                                                                INNER JOIN AWARD_PERSONS T2 ON T1.AWARD_ID = T2.AWARD_ID
                                                                WHERE T1.AWARD_ID = LS_AWARD_ID;
                                        ELSEIF LI_MODULE_CODE = 3 THEN
                                                                SELECT T1.PERSON_ID
                                                                FROM  EPS_PROPOSAL_PERSONS T1
                                                                WHERE T1.PROPOSAL_ID = LS_MODULE_ITEM_KEY;
                                   END IF;
                           END IF;
        ELSEIF AV_ROLE_TYPE = 8  THEN
                IF AV_MODULE_CODE = 3 THEN
          SELECT T2.PERSON_ID
          FROM PERSON T2
                  WHERE T2.PERSON_ID in (
                                                                                SELECT S2.SUPERVISOR_PERSON_ID
                                                                                FROM EPS_PROPOSAL_PERSONS S1
                                                                                INNER JOIN PERSON S2 ON S1.PERSON_ID = S2.PERSON_ID
                                                                                WHERE S1.PROPOSAL_ID = AV_MODULE_ITEM_ID
                                                                                AND S1.PROP_PERSON_ROLE_ID = 3
                                                                        );
                 END IF;
    ELSEIF AV_ROLE_TYPE = 9  THEN
                        SELECT T2.PERSON_ID
                        FROM PERSON T2
                        WHERE T2.DIRECTORY_TITLE = 'Faculty' OR T2.IS_FACULTY = 'Y';
        ELSEIF AV_ROLE_TYPE = 10  THEN
                        SELECT T2.PERSON_ID
                        FROM PERSON T2
                        WHERE T2.DIRECTORY_TITLE = 'Research Staff' OR T2.IS_RESEARCH_STAFF  = 'Y' ;
        ELSEIF AV_ROLE_TYPE = 11  THEN
                SELECT DISTINCT T1.PERSON_ID
        FROM PERSON_ROLES T1 LEFT JOIN PERSON T2 ON T1.PERSON_ID =T2.PERSON_ID
        WHERE T1.ROLE_ID = 46;
        ELSEIF AV_ROLE_TYPE = 12 THEN
        SELECT DISTINCT T1.PERSON_ID
        FROM PERSON_ROLES T1 LEFT JOIN PERSON T2 ON T1.PERSON_ID =T2.PERSON_ID
        WHERE ROLE_ID = 400
        AND T1.UNIT_NUMBER =  LS_LEAD_UNIT;
    ELSEIF AV_ROLE_TYPE = 13 THEN
            IF AV_MODULE_CODE = 3 THEN
						SELECT COUNT(1) INTO LI_FLAG 
						FROM EPS_PROPOSAL T1
						INNER JOIN GRANT_CALL_HEADER T2 ON T1.GRANT_HEADER_ID = T2.GRANT_HEADER_ID
						WHERE T1.PROPOSAL_ID = AV_MODULE_ITEM_ID
						AND T1.GRANT_HEADER_ID IS NOT NULL
						AND T2.HOME_UNIT_NUMBER <> '000001';
						IF LI_FLAG > 0 THEN
								SELECT T2.HOME_UNIT_NUMBER INTO LS_LEAD_UNIT
								FROM EPS_PROPOSAL T1
								INNER JOIN GRANT_CALL_HEADER T2 ON T1.GRANT_HEADER_ID = T2.GRANT_HEADER_ID
								WHERE T1.PROPOSAL_ID = AV_MODULE_ITEM_ID;
						END IF;
                        SELECT DISTINCT T1.PERSON_ID
                        FROM PERSON_ROLES T1
                        WHERE T1.ROLE_ID = 100
                        AND T1.UNIT_NUMBER = LS_LEAD_UNIT;
                ELSEIF AV_MODULE_CODE = 1 THEN
                        SELECT COUNT(1) INTO LI_FLAG FROM AWARD T1
                        WHERE T1.AWARD_ID = AV_MODULE_ITEM_ID
                        AND T1.GRANT_HEADER_ID IS NOT NULL;
                                        IF LI_FLAG > 0 THEN
                                                SELECT T2.HOME_UNIT_NUMBER INTO LS_LEAD_UNIT
                                                FROM AWARD T1
                                                INNER JOIN GRANT_CALL_HEADER T2 ON T1.GRANT_HEADER_ID = T2.GRANT_HEADER_ID
                                                WHERE T1.AWARD_ID = AV_MODULE_ITEM_ID;
                                        END IF;
                        SELECT DISTINCT T1.PERSON_ID
                        FROM PERSON_ROLES T1
                        WHERE T1.ROLE_ID = 100
                        AND T1.UNIT_NUMBER = LS_LEAD_UNIT;
                END IF;
         ELSEIF AV_ROLE_TYPE = 14 THEN
                SELECT DISTINCT T1.PERSON_ID
        FROM PERSON_ROLES T1 LEFT JOIN PERSON T2 ON T1.PERSON_ID =T2.PERSON_ID
        WHERE ROLE_ID = 120
                AND T1.UNIT_NUMBER = LS_LEAD_UNIT;
        ELSEIF AV_ROLE_TYPE = 15 THEN
                SELECT DISTINCT T1.PERSON_ID
        FROM PERSON_ROLES T1
        WHERE T1.ROLE_ID = 28
                AND T1.UNIT_NUMBER = LS_LEAD_UNIT;
   ELSEIF AV_ROLE_TYPE = 16 THEN
                SELECT DISTINCT T1.PERSON_ID
                FROM PERSON_ROLES T1
                WHERE ROLE_ID = 100
                AND T1.UNIT_NUMBER = '252';
  ELSEIF AV_ROLE_TYPE = 17 THEN
                SELECT DISTINCT T1.PERSON_ID
                FROM PERSON_ROLES T1
                WHERE ROLE_ID = 400
                AND T1.UNIT_NUMBER = '252';
  ELSEIF AV_ROLE_TYPE = 18 THEN
                SELECT DISTINCT T1.PERSON_ID
                FROM PERSON_ROLES T1
                WHERE ROLE_ID = 100
                AND T1.UNIT_NUMBER = '654';
  ELSEIF AV_ROLE_TYPE = 19 THEN
                SELECT DISTINCT T1.PERSON_ID
                FROM PERSON_ROLES T1
                WHERE ROLE_ID = 400
                AND T1.UNIT_NUMBER = '654';
END IF;
END
//
