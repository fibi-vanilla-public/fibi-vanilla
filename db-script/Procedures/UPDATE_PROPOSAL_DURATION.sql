DELIMITER //
CREATE  PROCEDURE `UPDATE_PROPOSAL_DURATION`()
BEGIN
DECLARE DONE TINYINT DEFAULT 0;
DECLARE C1 CURSOR FOR 
	SELECT END_DATE, START_DATE, PROPOSAL_ID FROM EPS_PROPOSAL where PROPOSAL_ID = 5000;
OPEN C1;
	LOOP1: LOOP BEGIN
	DECLARE LD_END_DATE DATETIME;
	DECLARE LD_START_DATE DATETIME;
	DECLARE LI_PROPOSAL_ID INT(10);
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET DONE = 1;
	FETCH C1 INTO LD_END_DATE, LD_START_DATE, LI_PROPOSAL_ID;
	IF DONE THEN
		LEAVE LOOP1;
	END IF;
	UPDATE EPS_PROPOSAL SET DURATION =( SELECT datedifference((SELECT DATE_ADD(LD_END_DATE, INTERVAL 1 DAY)), LD_START_DATE)) WHERE PROPOSAL_ID = LI_PROPOSAL_ID;
END; 
END LOOP;
CLOSE C1;
END
//
