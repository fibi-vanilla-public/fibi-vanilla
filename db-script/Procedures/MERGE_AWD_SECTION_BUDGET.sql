DELIMITER //
CREATE  PROCEDURE `MERGE_AWD_SECTION_BUDGET`(
AV_AWARD_ID 			INT,
AV_AWARD_NUMBER			VARCHAR(12),
AV_SEQUENCE_NUMBER		INT(4),
AV_VARIATION_TYPE_CODE	VARCHAR(3),
AV_UPDATE_USER			VARCHAR(60)
)
BEGIN
DECLARE LS_ERROR_MSG				VARCHAR(1000);
DECLARE LS_TABLE_NAME				VARCHAR(30);
DECLARE LI_FLAG						INT;
DECLARE LI_MASTER_AWARD_ID			DECIMAL(22,0);
DECLARE LI_MASTER_SEQ_NUMBER		INT(4);
DECLARE LI_AWD_BUDGT_DETL_ID_NEXT_VAL		BIGINT(20);
DECLARE LI_AWD_BUDGT_PERIOD_NEXT_VAL		BIGINT(20);
DECLARE LI_AWD_BUDGT_HEADER_NEXT_VAL		BIGINT(20);
DECLARE LI_AWD_BUDGT_DETL_ID_CNT			INT(10);
DECLARE LI_AWD_BUDGT_PERIOD_CNT				INT(10);
DECLARE LI_AWD_BUDGT_HEADER_CNT				INT(10);
DECLARE LS_BUDGET_HEADER_ID				INT(12);
DECLARE LI_ANTICIPATED_TOTAL			DECIMAL(12,2);
DECLARE LS_BUDGET_TYPE_CODE				VARCHAR(3);
DECLARE LS_COMMENTS						VARCHAR(4000);
DECLARE LS_CREATE_TIMESTAMP				DATETIME;
DECLARE LS_CREATE_USER					VARCHAR(60);
DECLARE LS_CREATE_USER_NAME				VARCHAR(90);
DECLARE LS_END_DATE						DATETIME;
DECLARE LS_IS_AUTO_CALC					VARCHAR(1);
DECLARE LI_MODULE_ITEM_CODE				INT(4);
DECLARE LS_MODULE_ITEM_KEY				VARCHAR(30);
DECLARE LI_MODULE_SEQUENCE_NUMBER		INT(4);
DECLARE LI_OBLIGATED_CHANGE				DECIMAL(12,2);
DECLARE LI_OBLIGATED_TOTAL				DECIMAL(12,2);
DECLARE LS_ON_OFF_CAMPUS_FLAG			VARCHAR(1);
DECLARE LS_RATE_CLASS_CODE				VARCHAR(3);
DECLARE LS_RATE_TYPE_CODE				VARCHAR(3);
DECLARE LS_START_DATE					DATETIME;
DECLARE LI_TOTAL_COST					DECIMAL(12,2);
DECLARE LI_TOTAL_DIRECT_COST			DECIMAL(12,2);
DECLARE LI_TOTAL_INDIRECT_COST			DECIMAL(12,2);
DECLARE LS_UPDATE_TIMESTAMP				DATETIME;
DECLARE LS_UPDATE_USER					VARCHAR(60);
DECLARE LS_UPDATE_USER_NAME				VARCHAR(90);
DECLARE LI_VERSION_NUMBER				INT(4);
DECLARE LI_TOTAL_SUBCONTRACT_COST		DECIMAL(12,2);
DECLARE LI_AWARD_ID						DECIMAL(22,0);
DECLARE LS_AWARD_NUMBER					VARCHAR(12);
DECLARE LI_SEQUENCE_NUMBER				INT(4);
DECLARE LS_FUND_CODE					VARCHAR(255);
DECLARE LS_FUND_CENTRE					VARCHAR(255);
DECLARE LS_AWARD_BUDGET_STATUS_CODE		VARCHAR(255);
DECLARE LS_IS_LATEST_VERSION			VARCHAR(255);
DECLARE LS_AVAILABLE_FUND_TYPE			VARCHAR(255);
DECLARE LI_VIREMENT						DECIMAL(7,2);
DECLARE LI_CUMULATIVE_VIREMENT			DECIMAL(7,2);
DECLARE LI_TOTAL_COST_SHARE				DECIMAL(12,2);
DECLARE LI_BUDGET_TEMPLATE_TYPE_ID				INT(11);
DECLARE LI_BUDGET_PERIOD			INT(11);
DECLARE LS_IS_OBLIGATED_PERIOD		VARCHAR(1);
DECLARE LS_PERIOD_LABEL				VARCHAR(255);
DECLARE LI_SUBCONTRACT_COST			DECIMAL(12,2);
DECLARE LI_DEV_PROPOSAL_ID			INT(10);
DECLARE LI_DEV_PROP_BUDGET_ID		INT(12);
DECLARE LI_DEV_PROP_BUDGET_PERIOD	INT(3);
DECLARE LI_BUDGET_HEADER_ID			INT(11);
DECLARE LS_BUDGET_DETAILS_ID				INT(12);
DECLARE LI_BUDGET_PERIOD_ID					INT(12);
DECLARE LS_BUDGET_CATEGORY_CODE				VARCHAR(3);
DECLARE LS_COST_ELEMENT						VARCHAR(50);
DECLARE LS_LINE_ITEM_DESCRIPTION			VARCHAR(4000);
DECLARE LI_LINE_ITEM_COST					DECIMAL(12,2);
DECLARE LI_PREVIOUS_LINE_ITEM_COST			DECIMAL(12,2);
DECLARE LS_BUDGET_JUSTIFICATION				VARCHAR(2000);
DECLARE LS_IS_SYSTEM_GENRTED_COST_ELEMENT	VARCHAR(1);
DECLARE LS_SYSTEM_GEN_COST_ELEMENT_TYPE		VARCHAR(60);
DECLARE LS_FULL_NAME						VARCHAR(90);
DECLARE LS_PERSON_ID						VARCHAR(40);
DECLARE LS_PERSON_TYPE						VARCHAR(3);
DECLARE LI_ROLODEX_ID						INT(10);
DECLARE LS_TBN_ID							VARCHAR(9);
DECLARE LS_IS_APPLY_INFLATION_RATE			VARCHAR(1);
DECLARE LI_COST_SHARING_AMOUNT				DECIMAL(12,2);
DECLARE LI_COST_SHARING_PERCENT				DECIMAL(5,2);
DECLARE LS_BUD_DTL_UPDATE_TIMESTAMP			TIMESTAMP(6);
DECLARE LI_COST_ELEMENT_BASE				DECIMAL(12,2);
DECLARE LS_INTERNAL_ORDER_CODE				VARCHAR(255);
DECLARE LI_QUANTITY							DECIMAL(6,2);
DECLARE LI_APPROVED_BUDGET					DECIMAL(12,2);
DECLARE LI_BUDGET_PERSON_DETAIL_ID		INT(12);
DECLARE LI_BUDGET_DETAILS_ID			INT(12);
DECLARE LS_PERSON_NAME					VARCHAR(90);
DECLARE LI_UNDERRECOVERY_AMOUNT			DECIMAL(12,2);
DECLARE LI_PERCENT_CHARGED				DECIMAL(5,2);
DECLARE LI_PERCENT_EFFORT				DECIMAL(5,2);
DECLARE LI_SALARY_REQUESTED				DECIMAL(12,2);
DECLARE LI_TOTAL_SALARY					DECIMAL(12,2);
DECLARE LI_NO_OF_MONTHS					DECIMAL(4,2);
DECLARE LS_BUD_PERSN_UPD_TIMESTAMP		TIMESTAMP(6);
DECLARE LS_TBN_PERSON_NAME				VARCHAR(90);
DECLARE LI_BUDGET_PERSON_ID				INT(10);
DECLARE LS_BUD_PERSN_END_DATE			DATETIME(6);
DECLARE LS_IO_CODE						VARCHAR(255);
DECLARE LI_LINE_ITEM_NUMBER				INT(11);
DECLARE LS_BUD_PERSN_START_DATE			DATETIME(6);
DECLARE LS_DESCRIPTION					VARCHAR(4000);
DECLARE LI_BUDGET_DETAILS_CAL_AMTS_ID	INT(11);
DECLARE LS_APPLY_RATE_FLAG				VARCHAR(1);
DECLARE LI_BUDGET_PERIOD_NUMBER			INT(11);
DECLARE LI_CALCULATED_COST				DECIMAL(12,2);
DECLARE LI_CALCULATED_COST_SHARING		DECIMAL(12,2);
DECLARE LS_RATE_TYPE_DESCRIPTION		VARCHAR(200);
DECLARE LI_APPLICABLE_RATE				DECIMAL(12,2);
DECLARE LI_BUDGET_ID					INT(12);
DECLARE LS_DOCUMENT_UPDATE_USER			VARCHAR(60);
DECLARE LS_DOCUMENT_UPDATE_TIMESTAMP	DATETIME;
DECLARE LS_ACTIVITY_TYPE_CODE		VARCHAR(3);
DECLARE LS_APPLICABLE_RATE			DECIMAL(12,2);
DECLARE LS_FISCAL_YEAR				VARCHAR(4);
DECLARE LS_INSTITUTE_RATE			DECIMAL(12,2);
DECLARE LS_RETURN_VALUE				VARCHAR(10);
DECLARE LI_MASTER_BUDGET_HEADER_ID	INT(11);
DECLARE LI_AWD_BGT_PERSN_ID_NEXT_VAL		BIGINT(20);
DECLARE LS_APPOINTMENT_TYPE_CODE VARCHAR(3);
DECLARE LS_ROLODEX_ID INT(11);
DECLARE LS_JOB_CODE VARCHAR(6);
DECLARE LS_EFFECTIVE_DATE DATETIME;
DECLARE LS_CALCULATION_BASE DECIMAL(12,2);
DECLARE LS_NON_EMPLOYEE_FLAG VARCHAR(1);
DECLARE LS_SALARY_ANNIVERSARY_DATE DATETIME;
DECLARE LS_BUDGET_PERSON_ID INT(11);
DECLARE LS_COUNT INT(11);
DECLARE LS_ON_CAMPUS_RATES VARCHAR(100);
DECLARE LS_OFF_CAMPUS_RATES VARCHAR(100);
DECLARE LS_COST_SHARE_TYPE_CODE INT(11); 
DECLARE LS_FUND_DISBURSEMENT_BASIS_TYPE_CODE VARCHAR(3);
SET LS_COUNT =0;
			BEGIN
				DECLARE EXIT HANDLER FOR SQLEXCEPTION
				BEGIN
					GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, 
					 @errno = MYSQL_ERRNO, @msg = MESSAGE_TEXT;
					SET @full_error = CONCAT(@msg,'|SEC102|',LS_TABLE_NAME );
					SELECT @full_error INTO LS_ERROR_MSG;
					RESIGNAL SET MESSAGE_TEXT = LS_ERROR_MSG;
				END;
				SELECT COUNT(1) INTO LI_FLAG
				FROM AWARD 
				WHERE AWARD_NUMBER = AV_AWARD_NUMBER
				AND SEQUENCE_NUMBER = 0;
				IF LI_FLAG > 0 THEN
					SELECT AWARD_ID,SEQUENCE_NUMBER 
					INTO LI_MASTER_AWARD_ID,LI_MASTER_SEQ_NUMBER
					FROM AWARD 
					WHERE AWARD_NUMBER = AV_AWARD_NUMBER
					AND SEQUENCE_NUMBER = 0;
				END IF;
				DELETE FROM AWARD_BUDGET_DET_CAL_AMT
				WHERE BUDGET_DETAILS_ID IN (SELECT BUDGET_DETAILS_ID FROM AWARD_BUDGET_DETAIL
											WHERE BUDGET_HEADER_ID IN (SELECT BUDGET_HEADER_ID FROM AWARD_BUDGET_HEADER
																	   WHERE AWARD_ID = LI_MASTER_AWARD_ID
																	  )
										   );
				DELETE FROM AWARD_BUDGET_NON_PERSON_DETAIL
				WHERE BUDGET_DETAILS_ID IN (SELECT BUDGET_DETAILS_ID FROM AWARD_BUDGET_DETAIL
											WHERE BUDGET_HEADER_ID IN (SELECT BUDGET_HEADER_ID FROM AWARD_BUDGET_HEADER
																	   WHERE AWARD_ID = LI_MASTER_AWARD_ID
																	  )
										   );
				DELETE FROM AWARD_BUDGET_PERSON_DETAIL 
				WHERE BUDGET_DETAILS_ID IN (SELECT BUDGET_DETAILS_ID FROM AWARD_BUDGET_DETAIL
											WHERE BUDGET_HEADER_ID IN (SELECT BUDGET_HEADER_ID FROM AWARD_BUDGET_HEADER
																	   WHERE AWARD_ID = LI_MASTER_AWARD_ID
																	  )
										   );
				DELETE FROM AWARD_BUDGET_RATE_AND_BASE
				WHERE BUDGET_DETAILS_ID IN (SELECT BUDGET_DETAILS_ID FROM AWARD_BUDGET_DETAIL
											WHERE BUDGET_HEADER_ID IN (SELECT BUDGET_HEADER_ID FROM AWARD_BUDGET_HEADER
																	   WHERE AWARD_ID = LI_MASTER_AWARD_ID
																	  )
										   );
				DELETE FROM AWARD_BUDGET_DETAIL
				WHERE BUDGET_HEADER_ID IN (SELECT BUDGET_HEADER_ID FROM AWARD_BUDGET_HEADER
										   WHERE AWARD_ID = LI_MASTER_AWARD_ID
										  );
				DELETE FROM AWARD_BUDGET_PERIOD
				WHERE BUDGET_HEADER_ID IN (SELECT BUDGET_HEADER_ID FROM AWARD_BUDGET_HEADER
										   WHERE AWARD_ID = LI_MASTER_AWARD_ID
										  );
				DELETE FROM AWARD_RATES		
				WHERE BUDGET_HEADER_ID IN(SELECT BUDGET_HEADER_ID FROM AWARD_BUDGET_HEADER WHERE AWARD_ID = LI_MASTER_AWARD_ID);
				DELETE FROM AWARD_BUDGET_PERSONS		
				WHERE BUDGET_HEADER_ID IN(SELECT BUDGET_HEADER_ID FROM AWARD_BUDGET_HEADER WHERE AWARD_ID = LI_MASTER_AWARD_ID);
				DELETE FROM AWARD_BUDGET_HEADER
				WHERE AWARD_ID = LI_MASTER_AWARD_ID;
				SET LS_TABLE_NAME = 'AWARD_BUDGET_HEADER';
				BEGIN
					DECLARE DONE1 INT DEFAULT FALSE;
					DECLARE CUR_AWARD_BUDGET_HEADER CURSOR FOR
					SELECT 
					BUDGET_HEADER_ID,
					ANTICIPATED_TOTAL, 
					BUDGET_TYPE_CODE, 
					COMMENTS, 
					CREATE_TIMESTAMP, 
					CREATE_USER, 
					CREATE_USER_NAME, 
					END_DATE, 
					IS_AUTO_CALC, 
					MODULE_ITEM_CODE, 
					MODULE_ITEM_KEY, 
					MODULE_SEQUENCE_NUMBER, 
					OBLIGATED_CHANGE, 
					OBLIGATED_TOTAL, 
					ON_OFF_CAMPUS_FLAG, 
					RATE_CLASS_CODE, 
					RATE_TYPE_CODE, 
					START_DATE, 
					TOTAL_COST, 
					TOTAL_DIRECT_COST, 
					TOTAL_INDIRECT_COST, 
					UPDATE_TIMESTAMP, 
					UPDATE_USER, 
					UPDATE_USER_NAME, 
					VERSION_NUMBER, 
					TOTAL_SUBCONTRACT_COST, 
					FUND_CODE, 
					FUND_CENTRE, 
					AWARD_BUDGET_STATUS_CODE, 
					IS_LATEST_VERSION, 
					AVAILABLE_FUND_TYPE, 
					VIREMENT, 
					CUMULATIVE_VIREMENT, 
					TOTAL_COST_SHARE,
					SEQUENCE_NUMBER,
					BUDGET_TEMPLATE_TYPE_ID,
                    ON_CAMPUS_RATES,
                    OFF_CAMPUS_RATES,
					COST_SHARE_TYPE_CODE,
					FUND_DISBURSEMENT_BASIS_TYPE_CODE
					FROM AWARD_BUDGET_HEADER 
					WHERE AWARD_ID = AV_AWARD_ID;
					DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE1 = TRUE;
					OPEN CUR_AWARD_BUDGET_HEADER;
					INSERT_AWARD_BUDGET_LOOP: LOOP 
					FETCH CUR_AWARD_BUDGET_HEADER INTO
					LS_BUDGET_HEADER_ID,
					LI_ANTICIPATED_TOTAL,
					LS_BUDGET_TYPE_CODE,
					LS_COMMENTS,
					LS_CREATE_TIMESTAMP,
					LS_CREATE_USER,
					LS_CREATE_USER_NAME,
					LS_END_DATE,
					LS_IS_AUTO_CALC,
					LI_MODULE_ITEM_CODE,
					LS_MODULE_ITEM_KEY,
					LI_MODULE_SEQUENCE_NUMBER,
					LI_OBLIGATED_CHANGE,
					LI_OBLIGATED_TOTAL,
					LS_ON_OFF_CAMPUS_FLAG,
					LS_RATE_CLASS_CODE,
					LS_RATE_TYPE_CODE,
					LS_START_DATE,
					LI_TOTAL_COST,
					LI_TOTAL_DIRECT_COST,
					LI_TOTAL_INDIRECT_COST,
					LS_UPDATE_TIMESTAMP,
					LS_UPDATE_USER,
					LS_UPDATE_USER_NAME,
					LI_VERSION_NUMBER,
					LI_TOTAL_SUBCONTRACT_COST,
					LS_FUND_CODE,
					LS_FUND_CENTRE,
					LS_AWARD_BUDGET_STATUS_CODE,
					LS_IS_LATEST_VERSION,
					LS_AVAILABLE_FUND_TYPE,
					LI_VIREMENT,
					LI_CUMULATIVE_VIREMENT,
					LI_TOTAL_COST_SHARE,
					LI_SEQUENCE_NUMBER,
					LI_BUDGET_TEMPLATE_TYPE_ID,
                    LS_ON_CAMPUS_RATES,
                    LS_OFF_CAMPUS_RATES,
					LS_COST_SHARE_TYPE_CODE,
					LS_FUND_DISBURSEMENT_BASIS_TYPE_CODE;
					IF DONE1 THEN
						LEAVE INSERT_AWARD_BUDGET_LOOP;
					END IF;
					INSERT INTO AWARD_BUDGET_HEADER( 
					ANTICIPATED_TOTAL, 
					BUDGET_TYPE_CODE,
					COMMENTS, CREATE_TIMESTAMP, 
					CREATE_USER, 
					CREATE_USER_NAME, 
					END_DATE, 
					IS_AUTO_CALC, 
					MODULE_ITEM_CODE, 
					MODULE_ITEM_KEY, 
					MODULE_SEQUENCE_NUMBER, 
					OBLIGATED_CHANGE, 
					OBLIGATED_TOTAL, 
					ON_OFF_CAMPUS_FLAG, 
					RATE_CLASS_CODE, 
					RATE_TYPE_CODE, 
					START_DATE, 
					TOTAL_COST, 
					TOTAL_DIRECT_COST, 
					TOTAL_INDIRECT_COST, 
					UPDATE_TIMESTAMP, 
					UPDATE_USER, 
					UPDATE_USER_NAME, 
					VERSION_NUMBER, 
					TOTAL_SUBCONTRACT_COST, 
					AWARD_ID, 
					AWARD_NUMBER, 
					SEQUENCE_NUMBER, 
					FUND_CODE, 
					FUND_CENTRE, 
					AWARD_BUDGET_STATUS_CODE, 
					IS_LATEST_VERSION, 
					AVAILABLE_FUND_TYPE, 
					VIREMENT, 
					CUMULATIVE_VIREMENT, 
					TOTAL_COST_SHARE,
					BUDGET_TEMPLATE_TYPE_ID,
                    ON_CAMPUS_RATES,
                    OFF_CAMPUS_RATES,
					COST_SHARE_TYPE_CODE,
					FUND_DISBURSEMENT_BASIS_TYPE_CODE
					)
					VALUES(
					LI_ANTICIPATED_TOTAL,
					LS_BUDGET_TYPE_CODE,
					LS_COMMENTS,
					LS_CREATE_TIMESTAMP,
					LS_CREATE_USER,
					LS_CREATE_USER_NAME,
					LS_END_DATE,
					LS_IS_AUTO_CALC,
					LI_MODULE_ITEM_CODE,
					LS_MODULE_ITEM_KEY,
					LI_MODULE_SEQUENCE_NUMBER,
					LI_OBLIGATED_CHANGE,
					LI_OBLIGATED_TOTAL,
					LS_ON_OFF_CAMPUS_FLAG,
					LS_RATE_CLASS_CODE,
					LS_RATE_TYPE_CODE,
					LS_START_DATE,
					LI_TOTAL_COST,
					LI_TOTAL_DIRECT_COST,
					LI_TOTAL_INDIRECT_COST,
					LS_UPDATE_TIMESTAMP,
					LS_UPDATE_USER,
					LS_UPDATE_USER_NAME,
					LI_VERSION_NUMBER,
					LI_TOTAL_SUBCONTRACT_COST,
					LI_MASTER_AWARD_ID,
					AV_AWARD_NUMBER,
					LI_SEQUENCE_NUMBER,
					LS_FUND_CODE,
					LS_FUND_CENTRE,
					LS_AWARD_BUDGET_STATUS_CODE,
					LS_IS_LATEST_VERSION,
					LS_AVAILABLE_FUND_TYPE,
					LI_VIREMENT,
					LI_CUMULATIVE_VIREMENT,
					LI_TOTAL_COST_SHARE,
					LI_BUDGET_TEMPLATE_TYPE_ID,
                    LS_ON_CAMPUS_RATES,
                    LS_OFF_CAMPUS_RATES,
					LS_COST_SHARE_TYPE_CODE,
					LS_FUND_DISBURSEMENT_BASIS_TYPE_CODE
					);
					SELECT LAST_INSERT_ID() INTO LI_AWD_BUDGT_HEADER_NEXT_VAL;
					SET LS_TABLE_NAME = 'AWARD_BUDGET_PERIOD';
					BEGIN
						DECLARE DONE2 INT DEFAULT FALSE;
						DECLARE CUR_AWARD_BUDGET_PERIOD CURSOR FOR
						SELECT 
						BUDGET_PERIOD_ID, 
						BUDGET_PERIOD, 
						END_DATE, 
						IS_OBLIGATED_PERIOD, 
						MODULE_ITEM_CODE, 
						MODULE_ITEM_KEY, 
						PERIOD_LABEL, 
						START_DATE, 
						TOTAL_COST, 
						TOTAL_DIRECT_COST, 
						TOTAL_INDIRECT_COST, 
						UPDATE_TIMESTAMP, 
						UPDATE_USER, 
						VERSION_NUMBER, 
						BUDGET_HEADER_ID,
						SUBCONTRACT_COST, 
						AWARD_NUMBER, 
						DEV_PROPOSAL_ID, 
						DEV_PROP_BUDGET_ID, 
						DEV_PROP_BUDGET_PERIOD 
						FROM AWARD_BUDGET_PERIOD
						WHERE BUDGET_HEADER_ID = LS_BUDGET_HEADER_ID;
						DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE2 = TRUE;
						OPEN CUR_AWARD_BUDGET_PERIOD;
						INSERT_AWARD_BUDGET_PERIOD_LOOP: LOOP 
						FETCH CUR_AWARD_BUDGET_PERIOD INTO
						LI_BUDGET_PERIOD_ID,
						LI_BUDGET_PERIOD,
						LS_END_DATE,
						LS_IS_OBLIGATED_PERIOD,
						LI_MODULE_ITEM_CODE,
						LS_MODULE_ITEM_KEY,
						LS_PERIOD_LABEL,
						LS_START_DATE,
						LI_TOTAL_COST,
						LI_TOTAL_DIRECT_COST,
						LI_TOTAL_INDIRECT_COST,
						LS_UPDATE_TIMESTAMP,
						LS_UPDATE_USER,
						LI_VERSION_NUMBER,
						LI_BUDGET_HEADER_ID,
						LI_SUBCONTRACT_COST,
						LS_AWARD_NUMBER,
						LI_DEV_PROPOSAL_ID,
						LI_DEV_PROP_BUDGET_ID,
						LI_DEV_PROP_BUDGET_PERIOD;
						IF DONE2 THEN
							LEAVE INSERT_AWARD_BUDGET_PERIOD_LOOP;
						END IF;
						INSERT INTO AWARD_BUDGET_PERIOD
						(
						BUDGET_PERIOD, 
						END_DATE, 
						IS_OBLIGATED_PERIOD, 
						MODULE_ITEM_CODE, 
						MODULE_ITEM_KEY, 
						PERIOD_LABEL, 
						START_DATE, 
						TOTAL_COST, 
						TOTAL_DIRECT_COST, 
						TOTAL_INDIRECT_COST, 
						UPDATE_TIMESTAMP, 
						UPDATE_USER, 
						VERSION_NUMBER, 
						BUDGET_HEADER_ID,
						SUBCONTRACT_COST, 
						AWARD_NUMBER, 
						DEV_PROPOSAL_ID, 
						DEV_PROP_BUDGET_ID, 
						DEV_PROP_BUDGET_PERIOD
						)
						VALUES(
						LI_BUDGET_PERIOD,
						LS_END_DATE,
						LS_IS_OBLIGATED_PERIOD,
						LI_MODULE_ITEM_CODE,
						LS_MODULE_ITEM_KEY,
						LS_PERIOD_LABEL,
						LS_START_DATE,
						LI_TOTAL_COST,
						LI_TOTAL_DIRECT_COST,
						LI_TOTAL_INDIRECT_COST,
						LS_UPDATE_TIMESTAMP,
						LS_UPDATE_USER,
						LI_VERSION_NUMBER,
						LI_AWD_BUDGT_HEADER_NEXT_VAL,
						LI_SUBCONTRACT_COST,
						AV_AWARD_NUMBER,
						LI_DEV_PROPOSAL_ID,
						LI_DEV_PROP_BUDGET_ID,
						LI_DEV_PROP_BUDGET_PERIOD
						);
					SELECT LAST_INSERT_ID() INTO LI_AWD_BUDGT_PERIOD_NEXT_VAL;
					SET LS_TABLE_NAME = 'AWARD_BUDGET_DETAIL';
					BEGIN
						DECLARE DONE3 INT DEFAULT FALSE;
						DECLARE CUR_AWARD_BUDGET_DETAIL CURSOR FOR
						SELECT 
						BUDGET_DETAILS_ID, BUDGET_HEADER_ID, 
						BUDGET_PERIOD_ID, VERSION_NUMBER, BUDGET_PERIOD, 
						LINE_ITEM_NUMBER, START_DATE, END_DATE, 
						BUDGET_CATEGORY_CODE, COST_ELEMENT, 
						LINE_ITEM_DESCRIPTION, LINE_ITEM_COST, 
						PREVIOUS_LINE_ITEM_COST, BUDGET_JUSTIFICATION, IS_SYSTEM_GENRTED_COST_ELEMENT,
						ON_OFF_CAMPUS_FLAG, SYSTEM_GEN_COST_ELEMENT_TYPE, FULL_NAME, PERSON_ID, PERSON_TYPE, 
						ROLODEX_ID, TBN_ID, IS_APPLY_INFLATION_RATE, COST_SHARING_AMOUNT, COST_SHARING_PERCENT, 
						UPDATE_TIMESTAMP, UPDATE_USER, COST_ELEMENT_BASE, INTERNAL_ORDER_CODE, AWARD_NUMBER, 
						QUANTITY 
						FROM AWARD_BUDGET_DETAIL
						WHERE BUDGET_PERIOD_ID = LI_BUDGET_PERIOD_ID;
						DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE3 = TRUE;
						OPEN CUR_AWARD_BUDGET_DETAIL;
						INSERT_AWARD_BUDGET_DTL_LOOP: LOOP 
						FETCH CUR_AWARD_BUDGET_DETAIL INTO
						LS_BUDGET_DETAILS_ID,
						LS_BUDGET_HEADER_ID,
						LI_BUDGET_PERIOD_ID,
						LI_VERSION_NUMBER,
						LI_BUDGET_PERIOD,
						LI_LINE_ITEM_NUMBER,
						LS_START_DATE,
						LS_END_DATE,
						LS_BUDGET_CATEGORY_CODE,
						LS_COST_ELEMENT,
						LS_LINE_ITEM_DESCRIPTION,
						LI_LINE_ITEM_COST,
						LI_PREVIOUS_LINE_ITEM_COST,
						LS_BUDGET_JUSTIFICATION,
						LS_IS_SYSTEM_GENRTED_COST_ELEMENT,
						LS_ON_OFF_CAMPUS_FLAG,
						LS_SYSTEM_GEN_COST_ELEMENT_TYPE,
						LS_FULL_NAME,
						LS_PERSON_ID,
						LS_PERSON_TYPE,
						LI_ROLODEX_ID,
						LS_TBN_ID,
						LS_IS_APPLY_INFLATION_RATE,
						LI_COST_SHARING_AMOUNT,
						LI_COST_SHARING_PERCENT,
						LS_BUD_DTL_UPDATE_TIMESTAMP,
						LS_UPDATE_USER,
						LI_COST_ELEMENT_BASE,
						LS_INTERNAL_ORDER_CODE,
						LS_AWARD_NUMBER,
						LI_QUANTITY;
						IF DONE3 THEN
							LEAVE INSERT_AWARD_BUDGET_DTL_LOOP;
						END IF;
						INSERT INTO AWARD_BUDGET_DETAIL
						( 
						BUDGET_HEADER_ID, 
						BUDGET_PERIOD_ID, 
						VERSION_NUMBER, 
						BUDGET_PERIOD, 
						LINE_ITEM_NUMBER, 
						START_DATE, 
						END_DATE, 
						BUDGET_CATEGORY_CODE, 
						COST_ELEMENT, 
						LINE_ITEM_DESCRIPTION, 
						LINE_ITEM_COST, 
						PREVIOUS_LINE_ITEM_COST, 
						BUDGET_JUSTIFICATION, 
						IS_SYSTEM_GENRTED_COST_ELEMENT,
						ON_OFF_CAMPUS_FLAG, 
						SYSTEM_GEN_COST_ELEMENT_TYPE, 
						FULL_NAME, 
						PERSON_ID, 
						PERSON_TYPE, 
						ROLODEX_ID, 
						TBN_ID, 
						IS_APPLY_INFLATION_RATE, 
						COST_SHARING_AMOUNT, 
						COST_SHARING_PERCENT, 
						UPDATE_TIMESTAMP, 
						UPDATE_USER, 
						COST_ELEMENT_BASE, 
						INTERNAL_ORDER_CODE, 
						AWARD_NUMBER, 
						QUANTITY 
						)
						VALUES
						(
						LI_AWD_BUDGT_HEADER_NEXT_VAL,
						LI_AWD_BUDGT_PERIOD_NEXT_VAL,
						LI_VERSION_NUMBER,
						LI_BUDGET_PERIOD,
						LI_LINE_ITEM_NUMBER,
						LS_START_DATE,
						LS_END_DATE,
						LS_BUDGET_CATEGORY_CODE,
						LS_COST_ELEMENT,
						LS_LINE_ITEM_DESCRIPTION,
						LI_LINE_ITEM_COST,
						LI_PREVIOUS_LINE_ITEM_COST,
						LS_BUDGET_JUSTIFICATION,
						LS_IS_SYSTEM_GENRTED_COST_ELEMENT,
						LS_ON_OFF_CAMPUS_FLAG,
						LS_SYSTEM_GEN_COST_ELEMENT_TYPE,
						LS_FULL_NAME,
						LS_PERSON_ID,
						LS_PERSON_TYPE,
						LI_ROLODEX_ID,
						LS_TBN_ID,
						LS_IS_APPLY_INFLATION_RATE,
						LI_COST_SHARING_AMOUNT,
						LI_COST_SHARING_PERCENT,
						LS_BUD_DTL_UPDATE_TIMESTAMP,
						LS_UPDATE_USER,
						LI_COST_ELEMENT_BASE,
						LS_INTERNAL_ORDER_CODE,
						AV_AWARD_NUMBER,
						LI_QUANTITY 
						);
						SELECT LAST_INSERT_ID() INTO LI_AWD_BUDGT_DETL_ID_NEXT_VAL;
						SET LS_TABLE_NAME = 'AWARD_BUDGET_PERSON_DETAIL';
						BEGIN
							DECLARE DONE5 INT DEFAULT FALSE;
							DECLARE CUR_AWD_BUD_PERSON_DETAIL CURSOR FOR
							SELECT 
							BUDGET_PERSON_DETAIL_ID, 
							PERSON_NAME, 
							PERSON_TYPE, 
							PERSON_ID, 
							ROLODEX_ID, 
							TBN_ID, 
							UNDERRECOVERY_AMOUNT, 
							PERCENT_CHARGED, 
							PERCENT_EFFORT, 
							COST_SHARING_AMOUNT, 
							COST_SHARING_PERCENT, 
							SALARY_REQUESTED, 
							TOTAL_SALARY, 
							NO_OF_MONTHS, 
							UPDATE_TIMESTAMP, 
							UPDATE_USER, 
							TBN_PERSON_NAME, 
							BUDGET_PERSON_ID, 
							END_DATE, 
							IO_CODE, 
							LINE_ITEM_NUMBER, 
							START_DATE
							FROM AWARD_BUDGET_PERSON_DETAIL
							WHERE BUDGET_DETAILS_ID = LS_BUDGET_DETAILS_ID; 
							DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE5 = TRUE;
							OPEN CUR_AWD_BUD_PERSON_DETAIL;
							INSERT_AWD_BUD_PERSON_DTL_LOOP: LOOP 
							FETCH CUR_AWD_BUD_PERSON_DETAIL INTO
							LI_BUDGET_PERSON_DETAIL_ID,
							LS_PERSON_NAME,
							LS_PERSON_TYPE,
							LS_PERSON_ID,
							LI_ROLODEX_ID,
							LS_TBN_ID,
							LI_UNDERRECOVERY_AMOUNT,
							LI_PERCENT_CHARGED,
							LI_PERCENT_EFFORT,
							LI_COST_SHARING_AMOUNT,
							LI_COST_SHARING_PERCENT,
							LI_SALARY_REQUESTED,
							LI_TOTAL_SALARY,
							LI_NO_OF_MONTHS,
							LS_BUD_PERSN_UPD_TIMESTAMP,
							LS_UPDATE_USER,
							LS_TBN_PERSON_NAME,
							LI_BUDGET_PERSON_ID,
							LS_BUD_PERSN_END_DATE,
							LS_IO_CODE,
							LI_LINE_ITEM_NUMBER,
							LS_BUD_PERSN_START_DATE;
							IF DONE5 THEN
								LEAVE INSERT_AWD_BUD_PERSON_DTL_LOOP;
							END IF;
							INSERT INTO AWARD_BUDGET_PERSON_DETAIL
							(
							BUDGET_DETAILS_ID,
							PERSON_NAME, 
							PERSON_TYPE, 
							PERSON_ID, 
							ROLODEX_ID, 
							TBN_ID, 
							UNDERRECOVERY_AMOUNT, 
							PERCENT_CHARGED, 
							PERCENT_EFFORT, 
							COST_SHARING_AMOUNT, 
							COST_SHARING_PERCENT, 
							SALARY_REQUESTED, 
							TOTAL_SALARY, 
							NO_OF_MONTHS, 
							UPDATE_TIMESTAMP, 
							UPDATE_USER, 
							TBN_PERSON_NAME, 
							BUDGET_PERSON_ID, 
							END_DATE, 
							IO_CODE, 
							LINE_ITEM_NUMBER, 
							START_DATE)
							VALUES
							(
							LI_AWD_BUDGT_DETL_ID_NEXT_VAL,
							LS_PERSON_NAME,
							LS_PERSON_TYPE,
							LS_PERSON_ID,
							LI_ROLODEX_ID,
							LS_TBN_ID,
							LI_UNDERRECOVERY_AMOUNT,
							LI_PERCENT_CHARGED,
							LI_PERCENT_EFFORT,
							LI_COST_SHARING_AMOUNT,
							LI_COST_SHARING_PERCENT,
							LI_SALARY_REQUESTED,
							LI_TOTAL_SALARY,
							LI_NO_OF_MONTHS,
							LS_BUD_PERSN_UPD_TIMESTAMP,
							LS_UPDATE_USER,
							LS_TBN_PERSON_NAME,
							LI_BUDGET_PERSON_ID,
							LS_BUD_PERSN_END_DATE,
							LS_IO_CODE,
							LI_LINE_ITEM_NUMBER,
							LS_BUD_PERSN_START_DATE
							);
							END LOOP;
							CLOSE CUR_AWD_BUD_PERSON_DETAIL;
						END;
						SET LS_TABLE_NAME = 'AWARD_BUDGET_NON_PERSON_DETAIL';
						BEGIN
							DECLARE DONE6 INT DEFAULT FALSE;
							DECLARE CUR_AWD_BUD_NON_PERSON_DETAIL CURSOR FOR
							SELECT 
							LINE_ITEM_NUMBER, 
							DESCRIPTION, 
							INTERNAL_ORDER_CODE, 
							LINE_ITEM_COST, 
							UPDATE_USER, 
							UPDATE_TIMESTAMP
							FROM AWARD_BUDGET_NON_PERSON_DETAIL
							WHERE BUDGET_DETAILS_ID = LS_BUDGET_DETAILS_ID;
							DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE6 = TRUE;
							OPEN CUR_AWD_BUD_NON_PERSON_DETAIL;
							INSERT_AWD_BUD_NON_PERSON_DTL_LOOP: LOOP 
							FETCH CUR_AWD_BUD_NON_PERSON_DETAIL INTO
							LI_LINE_ITEM_NUMBER,
							LS_DESCRIPTION,
							LS_INTERNAL_ORDER_CODE,
							LI_LINE_ITEM_COST,
							LS_UPDATE_USER,
							LS_UPDATE_TIMESTAMP;
							IF DONE6 THEN
								LEAVE INSERT_AWD_BUD_NON_PERSON_DTL_LOOP;
							END IF;
							INSERT INTO AWARD_BUDGET_NON_PERSON_DETAIL
							(
							BUDGET_DETAILS_ID, 
							LINE_ITEM_NUMBER, 
							DESCRIPTION, 
							INTERNAL_ORDER_CODE, 
							LINE_ITEM_COST, 
							UPDATE_USER, 
							UPDATE_TIMESTAMP
							)
							VALUES
							(
							LI_AWD_BUDGT_DETL_ID_NEXT_VAL,
							LI_LINE_ITEM_NUMBER,
							LS_DESCRIPTION,
							LS_INTERNAL_ORDER_CODE,
							LI_LINE_ITEM_COST,
							LS_UPDATE_USER,
							LS_UPDATE_TIMESTAMP
							);
							END LOOP;
							CLOSE CUR_AWD_BUD_NON_PERSON_DETAIL;
						END;
						SET LS_TABLE_NAME = 'AWARD_BUDGET_DET_CAL_AMT';
						BEGIN
							DECLARE DONE7 INT DEFAULT FALSE;
							DECLARE CUR_AWD_BUD_DET_CAL_AMT CURSOR FOR
							SELECT 
								BUDGET_DETAILS_CAL_AMTS_ID, 
								APPLY_RATE_FLAG, 
								BUDGET_ID, 
								BUDGET_PERIOD, 
								BUDGET_PERIOD_NUMBER, 
								CALCULATED_COST, 
								CALCULATED_COST_SHARING, 
								LINE_ITEM_NUMBER, 
								RATE_CLASS_CODE, 
								RATE_TYPE_CODE, 
								RATE_TYPE_DESCRIPTION, 
								UPDATE_TIMESTAMP, 
								UPDATE_USER, 
								BUDGET_DETAILS_ID, 
								APPLICABLE_RATE, 
								BUDGET_HEADER_ID, 
								BUDGET_PERIOD_ID
							FROM AWARD_BUDGET_DET_CAL_AMT
							WHERE BUDGET_DETAILS_ID = LS_BUDGET_DETAILS_ID;
							DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE7 = TRUE;
							OPEN CUR_AWD_BUD_DET_CAL_AMT;
							INSERT_AWD_BUD_DET_CAL_AMT_LOOP: LOOP 
							FETCH CUR_AWD_BUD_DET_CAL_AMT INTO
							LI_BUDGET_DETAILS_CAL_AMTS_ID,
							LS_APPLY_RATE_FLAG,
							LI_BUDGET_ID,
							LI_BUDGET_PERIOD,
							LI_BUDGET_PERIOD_NUMBER,
							LI_CALCULATED_COST,
							LI_CALCULATED_COST_SHARING,
							LI_LINE_ITEM_NUMBER,
							LS_RATE_CLASS_CODE,
							LS_RATE_TYPE_CODE,
							LS_RATE_TYPE_DESCRIPTION,
							LS_UPDATE_TIMESTAMP,
							LS_UPDATE_USER,
							LI_BUDGET_DETAILS_ID,
							LI_APPLICABLE_RATE,
							LI_BUDGET_HEADER_ID,
							LI_BUDGET_PERIOD_ID;
							IF DONE7 THEN
								LEAVE INSERT_AWD_BUD_DET_CAL_AMT_LOOP;
							END IF;
							INSERT INTO AWARD_BUDGET_DET_CAL_AMT(
							APPLY_RATE_FLAG, 
							BUDGET_ID, 
							BUDGET_PERIOD, 
							BUDGET_PERIOD_NUMBER, 
							CALCULATED_COST, 
							CALCULATED_COST_SHARING, 
							LINE_ITEM_NUMBER, 
							RATE_CLASS_CODE, 
							RATE_TYPE_CODE, 
							RATE_TYPE_DESCRIPTION, 
							UPDATE_TIMESTAMP, 
							UPDATE_USER, 
							BUDGET_DETAILS_ID, 
							APPLICABLE_RATE, 
							BUDGET_HEADER_ID, 
							BUDGET_PERIOD_ID
							)
							VALUES
							(
							LS_APPLY_RATE_FLAG,
							LI_AWD_BUDGT_HEADER_NEXT_VAL,
							LI_BUDGET_PERIOD,
							LI_BUDGET_PERIOD_NUMBER,
							LI_CALCULATED_COST,
							LI_CALCULATED_COST_SHARING,
							LI_LINE_ITEM_NUMBER,
							LS_RATE_CLASS_CODE,
							LS_RATE_TYPE_CODE,
							LS_RATE_TYPE_DESCRIPTION,
							LS_UPDATE_TIMESTAMP,
							LS_UPDATE_USER,
							LI_AWD_BUDGT_DETL_ID_NEXT_VAL,
							LI_APPLICABLE_RATE,
							LI_AWD_BUDGT_HEADER_NEXT_VAL, 
							LI_AWD_BUDGT_PERIOD_NEXT_VAL 
							);
							END LOOP;
							CLOSE CUR_AWD_BUD_DET_CAL_AMT;
						END;
						END LOOP;
						CLOSE CUR_AWARD_BUDGET_DETAIL;
					END;
					END LOOP;
						CLOSE CUR_AWARD_BUDGET_PERIOD;
					END;
					SET LS_TABLE_NAME = 'AWARD_RATES';
					BEGIN
						DECLARE DONE8 INT DEFAULT FALSE;
						DECLARE CUR_AWARD_RATES CURSOR FOR
						SELECT 
						ACTIVITY_TYPE_CODE, APPLICABLE_RATE, FISCAL_YEAR, INSTITUTE_RATE, ON_OFF_CAMPUS_FLAG, RATE_CLASS_CODE, RATE_TYPE_CODE, START_DATE, UPDATE_TIMESTAMP, UPDATE_USER
						FROM AWARD_RATES
						WHERE BUDGET_HEADER_ID = LS_BUDGET_HEADER_ID;
						DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE8 = TRUE;
						OPEN CUR_AWARD_RATES;
						INSERT_AWARD_RATES_LOOP: LOOP 
						FETCH CUR_AWARD_RATES INTO
						LS_ACTIVITY_TYPE_CODE,
						LS_APPLICABLE_RATE,
						LS_FISCAL_YEAR,
						LS_INSTITUTE_RATE,
						LS_ON_OFF_CAMPUS_FLAG,
						LS_RATE_CLASS_CODE,
						LS_RATE_TYPE_CODE,
						LS_START_DATE,
						LS_UPDATE_TIMESTAMP,
						LS_UPDATE_USER;
						IF DONE8 THEN
							LEAVE INSERT_AWARD_RATES_LOOP;
						END IF;
						INSERT INTO AWARD_RATES
						(ACTIVITY_TYPE_CODE, 
						APPLICABLE_RATE, 
						FISCAL_YEAR, 
						INSTITUTE_RATE, 
						ON_OFF_CAMPUS_FLAG, 
						RATE_CLASS_CODE, 
						RATE_TYPE_CODE, 
						START_DATE, 
						UPDATE_TIMESTAMP, 
						UPDATE_USER, 
						BUDGET_HEADER_ID
						)
						VALUES(
						LS_ACTIVITY_TYPE_CODE,
						LS_APPLICABLE_RATE,
						LS_FISCAL_YEAR,
						LS_INSTITUTE_RATE,
						LS_ON_OFF_CAMPUS_FLAG,
						LS_RATE_CLASS_CODE,
						LS_RATE_TYPE_CODE,
						LS_START_DATE,
						LS_UPDATE_TIMESTAMP,
						LS_UPDATE_USER,
						LI_AWD_BUDGT_HEADER_NEXT_VAL
						);
						END LOOP;
						CLOSE CUR_AWARD_RATES;
					END;
										SET LS_TABLE_NAME = 'AWARD_BUDGET_PERSONS';
					BEGIN
						DECLARE DONE8 INT DEFAULT FALSE;
						DECLARE CUR_AWARD_BUDGET_PERSONS CURSOR FOR
						SELECT 
						BUDGET_PERSON_ID, APPOINTMENT_TYPE_CODE,TBN_ID,PERSON_ID,ROLODEX_ID,JOB_CODE,EFFECTIVE_DATE,CALCULATION_BASE
						,PERSON_NAME,NON_EMPLOYEE_FLAG,SALARY_ANNIVERSARY_DATE,PERSON_TYPE,UPDATE_TIMESTAMP,UPDATE_USER
						FROM AWARD_BUDGET_PERSONS
						WHERE BUDGET_HEADER_ID = LS_BUDGET_HEADER_ID;
						DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE8 = TRUE;
						OPEN CUR_AWARD_BUDGET_PERSONS;
						INSERT_AWARD_BUDGET_PERSONS_LOOP: LOOP 
						FETCH CUR_AWARD_BUDGET_PERSONS INTO
						LS_BUDGET_PERSON_ID,
						LS_APPOINTMENT_TYPE_CODE,
						LS_TBN_ID,
						LS_PERSON_ID,
						LS_ROLODEX_ID,
						LS_JOB_CODE,
						LS_EFFECTIVE_DATE,
						LS_CALCULATION_BASE,
						LS_PERSON_NAME,
						LS_NON_EMPLOYEE_FLAG,
						LS_SALARY_ANNIVERSARY_DATE,
						LS_PERSON_TYPE,
						LS_UPDATE_TIMESTAMP,
						LS_UPDATE_USER;
						IF DONE8 THEN
							LEAVE INSERT_AWARD_BUDGET_PERSONS_LOOP;
						END IF;
						INSERT INTO AWARD_BUDGET_PERSONS
						(APPOINTMENT_TYPE_CODE,TBN_ID,PERSON_ID,ROLODEX_ID,JOB_CODE,EFFECTIVE_DATE,CALCULATION_BASE
						,PERSON_NAME,NON_EMPLOYEE_FLAG,SALARY_ANNIVERSARY_DATE,PERSON_TYPE,UPDATE_TIMESTAMP,UPDATE_USER, 
						BUDGET_HEADER_ID
						)
						VALUES(
						LS_APPOINTMENT_TYPE_CODE,
						LS_TBN_ID,
						LS_PERSON_ID,
						LS_ROLODEX_ID,
						LS_JOB_CODE,
						LS_EFFECTIVE_DATE,
						LS_CALCULATION_BASE,
						LS_PERSON_NAME,
						LS_NON_EMPLOYEE_FLAG,
						LS_SALARY_ANNIVERSARY_DATE,
						LS_PERSON_TYPE,
						LS_UPDATE_TIMESTAMP,
						LS_UPDATE_USER,
						LI_AWD_BUDGT_HEADER_NEXT_VAL
						);
						SELECT LAST_INSERT_ID() INTO LI_AWD_BGT_PERSN_ID_NEXT_VAL;
						SELECT COUNT(1) INTO LS_COUNT FROM AWARD_BUDGET_PERSON_DETAIL WHERE BUDGET_DETAILS_ID IN (
						SELECT BUDGET_DETAILS_ID FROM AWARD_BUDGET_DETAIL WHERE BUDGET_HEADER_ID = LI_AWD_BUDGT_HEADER_NEXT_VAL)
						AND BUDGET_PERSON_ID = LS_BUDGET_PERSON_ID;
						IF LS_COUNT > 0 THEN
						UPDATE AWARD_BUDGET_PERSON_DETAIL SET BUDGET_PERSON_ID = LI_AWD_BGT_PERSN_ID_NEXT_VAL
						WHERE BUDGET_DETAILS_ID IN (
						SELECT BUDGET_DETAILS_ID FROM AWARD_BUDGET_DETAIL WHERE BUDGET_HEADER_ID = LI_AWD_BUDGT_HEADER_NEXT_VAL)
						AND BUDGET_PERSON_ID = LS_BUDGET_PERSON_ID;
						SET LS_COUNT = 0;
						END IF;
						END LOOP;
						CLOSE CUR_AWARD_BUDGET_PERSONS;
					END;
					END LOOP;
					CLOSE CUR_AWARD_BUDGET_HEADER;
				END;
				SELECT COUNT(1) INTO LI_FLAG
				FROM PARAMETER
				WHERE PARAMETER_NAME ='ENABLE_SAP_AWARD_FEED'
				AND VALUE = 'Y';
				SELECT MAX(BUDGET_HEADER_ID) INTO LI_MASTER_BUDGET_HEADER_ID 
				FROM AWARD_BUDGET_HEADER
				WHERE AWARD_ID = LI_MASTER_AWARD_ID;
				IF LI_FLAG > 0 THEN
					SELECT COUNT(1) INTO LI_FLAG FROM MODULE_VARIABLE_SECTION
					WHERE MODULE_ITEM_KEY = AV_AWARD_ID
					AND MODULE_CODE = 1
					AND SUB_MODULE_CODE = 0
					AND SECTION_CODE = 102
					AND SUB_MODULE_ITEM_KEY = 0;
					IF LI_FLAG > 0 THEN
						UPDATE AWARD_BUDGET_HEADER
						SET AWARD_BUDGET_STATUS_CODE = 10 
						WHERE BUDGET_HEADER_ID = LI_MASTER_BUDGET_HEADER_ID;
					ELSE
						UPDATE AWARD_BUDGET_HEADER
						SET AWARD_BUDGET_STATUS_CODE = 9 
						WHERE BUDGET_HEADER_ID = LI_MASTER_BUDGET_HEADER_ID;
					END IF;
				ELSE
						UPDATE AWARD_BUDGET_HEADER
						SET AWARD_BUDGET_STATUS_CODE = 10 
						WHERE BUDGET_HEADER_ID = LI_MASTER_BUDGET_HEADER_ID;
				END IF;
			END;
END
//
