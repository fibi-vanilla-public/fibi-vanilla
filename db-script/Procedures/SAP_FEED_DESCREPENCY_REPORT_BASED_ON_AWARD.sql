DELIMITER //
CREATE  PROCEDURE `SAP_FEED_DESCREPENCY_REPORT_BASED_ON_AWARD`(
)
BEGIN
DECLARE LI_SEQ_ERROR_LOG_ID INT;
DECLARE LS_ERROR_MSG		VARCHAR(4000); 
DECLARE LS_BATCH_ID 		INT(10);
DECLARE LI_FEED_ID			INT(10);
DECLARE LI_AWARD_ID			INT(10);
DECLARE LS_AWARD_NUMBER		VARCHAR(12);
DECLARE LI_SEQUENCE_NUMBER	INT(10);
DECLARE LI_FLAG INT;
DECLARE LI_ERROR_FLAG INT;
DECLARE LS_SPONSOR_PRGM_FEED_STATUS VARCHAR(20);
DECLARE LS_SPONSOR_CLASS_FEED_STATUS VARCHAR(20);
DECLARE LS_FUNDED_PGM_FEED_STATUS VARCHAR(20);
DECLARE LS_GM_BUDGET_FEED_STATUS  VARCHAR(20);
DECLARE LS_GM_MASTER_FEED_STATUS  VARCHAR(20);
DECLARE LS_PRJ_DEF_FEED_STATUS    VARCHAR(20);
DECLARE LS_WBS_FEED_STATUS		  VARCHAR(20);
DECLARE LS_FM_FEED_STATUS		  VARCHAR(20);
DECLARE LS_ACCOUNT_NUMBER		  VARCHAR(100);
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
			GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, 
			 @errno = MYSQL_ERRNO, @msg = MESSAGE_TEXT;
			SET @full_error = CONCAT("ERROR ", @errno, " (", @sqlstate, "): ", @msg);
			SELECT @full_error INTO LS_ERROR_MSG;
			INSERT INTO SAP_FEED_REPORT_ERROR_LOG(ERROR_MESSAGE, UPDATE_TIMESTAMP, UPDATE_USER)
			VALUES(LS_ERROR_MSG,utc_timestamp(),'quickstart');
			COMMIT;
		END;
		BEGIN
			DECLARE DONE1 INT DEFAULT FALSE;
			DECLARE CUR_FEED_DATA CURSOR FOR
			SELECT BATCH_ID,FEED_ID,AWARD_ID,AWARD_NUMBER,SEQUENCE_NUMBER FROM sap_award_feed
			WHERE BATCH_ID >= 28 AND BATCH_ID <= 249 AND FEED_STATUS <> 'N' AND SYSTEM_COMMENT <> 'NOT REQUIRED FEED(NO CHANGE IN DATA)'
			ORDER BY BATCH_ID,FEED_ID;
			DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE1 = TRUE;
			OPEN CUR_FEED_DATA;
			INSERT_LOOP: LOOP 
			FETCH CUR_FEED_DATA INTO
			LS_BATCH_ID,
			LI_FEED_ID,
			LI_AWARD_ID,
			LS_AWARD_NUMBER,
			LI_SEQUENCE_NUMBER;
			IF DONE1 THEN
				LEAVE INSERT_LOOP;
			END IF;
			SELECT COUNT(1) INTO LI_FLAG
			FROM AWARD
			WHERE AWARD_ID = LI_AWARD_ID;
			IF LI_FLAG > 0 THEN
				SELECT ACCOUNT_NUMBER INTO LS_ACCOUNT_NUMBER
				FROM AWARD
				WHERE AWARD_ID = LI_AWARD_ID;
			END IF;
			SELECT COUNT(1) INTO LI_FLAG FROM SAP_FEED_TMPL_SPONSOR_PRGM 
			WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID;
			IF LI_FLAG > 0 THEN
				SELECT COUNT(1) INTO LI_ERROR_FLAG FROM SAP_FEED_TMPL_SPONSOR_PRGM 
				WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID
				AND FEED_STATUS = 'E';
				IF LI_ERROR_FLAG > 0 THEN
					SET LS_SPONSOR_PRGM_FEED_STATUS = 'E';
				ELSE
					SELECT COUNT(1) INTO LI_ERROR_FLAG FROM SAP_FEED_TMPL_SPONSOR_PRGM 
					WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID
					AND FEED_STATUS IS NULL;
					IF LI_ERROR_FLAG > 0 THEN
						SET LS_SPONSOR_PRGM_FEED_STATUS = 'NO RESPONSE';
					ELSE 
						SET LS_SPONSOR_PRGM_FEED_STATUS = 'S';
					END IF;
				END IF;
			ELSE
				SET LS_SPONSOR_PRGM_FEED_STATUS = 'S';
			END IF;
			SELECT COUNT(1) INTO LI_FLAG FROM SAP_FEED_TMPL_SPONSOR_CLASS 
			WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID;
			IF LI_FLAG > 0 THEN
				SELECT COUNT(1) INTO LI_ERROR_FLAG FROM SAP_FEED_TMPL_SPONSOR_CLASS 
				WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID
				AND FEED_STATUS = 'E';
				IF LI_ERROR_FLAG > 0 THEN
					SET LS_SPONSOR_CLASS_FEED_STATUS = 'E';
				ELSE
					SELECT COUNT(1) INTO LI_ERROR_FLAG FROM SAP_FEED_TMPL_SPONSOR_CLASS 
					WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID
					AND FEED_STATUS IS NULL;
					IF LI_ERROR_FLAG > 0 THEN
						SET LS_SPONSOR_CLASS_FEED_STATUS = 'NO RESPONSE';
					ELSE
						SET LS_SPONSOR_CLASS_FEED_STATUS = 'S';
					END IF;
				END IF;
			ELSE
				SET LS_SPONSOR_CLASS_FEED_STATUS = 'S';
			END IF;
			SELECT COUNT(1) INTO LI_FLAG FROM SAP_FEED_TMPL_FUNDED_PRGM 
			WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID;
			IF LI_FLAG > 0 THEN
				SELECT COUNT(1) INTO LI_ERROR_FLAG FROM SAP_FEED_TMPL_FUNDED_PRGM 
				WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID
				AND FEED_STATUS = 'E';
				IF LI_ERROR_FLAG > 0 THEN
					SET LS_FUNDED_PGM_FEED_STATUS = 'E';
				ELSE
					SELECT COUNT(1) INTO LI_ERROR_FLAG FROM SAP_FEED_TMPL_FUNDED_PRGM 
					WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID
					AND FEED_STATUS IS NULL;
					IF LI_ERROR_FLAG > 0 THEN
						SET LS_FUNDED_PGM_FEED_STATUS = 'NO RESPONSE';
					ELSE
						SET LS_FUNDED_PGM_FEED_STATUS = 'S';
					END IF;
				END IF;
			ELSE
				SET LS_FUNDED_PGM_FEED_STATUS = 'S';
			END IF;
			SELECT COUNT(1) INTO LI_FLAG FROM SAP_FEED_TMPL_GRANT_BUD_MASTER 
			WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID;
			IF LI_FLAG > 0 THEN
				SELECT COUNT(1) INTO LI_ERROR_FLAG FROM SAP_FEED_TMPL_GRANT_BUD_MASTER 
				WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID
				AND FEED_STATUS = 'E';
				IF LI_ERROR_FLAG > 0 THEN
					SET LS_GM_BUDGET_FEED_STATUS = 'E';
				ELSE
					SELECT COUNT(1) INTO LI_ERROR_FLAG FROM SAP_FEED_TMPL_GRANT_BUD_MASTER 
					WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID
					AND FEED_STATUS IS NULL;
					IF LI_ERROR_FLAG > 0 THEN
						SET LS_GM_BUDGET_FEED_STATUS = 'NO RESPONSE';
					ELSE
						SET LS_GM_BUDGET_FEED_STATUS = 'S';
					END IF;
				END IF;
			ELSE
				SET LS_GM_BUDGET_FEED_STATUS = 'S';
			END IF;
			SELECT COUNT(1) INTO LI_FLAG FROM SAP_FEED_TMPL_GRANT_MASTER 
			WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID;
			IF LI_FLAG > 0 THEN
				SELECT COUNT(1) INTO LI_ERROR_FLAG FROM SAP_FEED_TMPL_GRANT_MASTER 
				WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID
				AND FEED_STATUS = 'E';
				IF LI_ERROR_FLAG > 0 THEN
					SET LS_GM_MASTER_FEED_STATUS = 'E';
				ELSE
					SELECT COUNT(1) INTO LI_ERROR_FLAG FROM SAP_FEED_TMPL_GRANT_MASTER 
					WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID
					AND FEED_STATUS IS NULL;
					IF LI_ERROR_FLAG > 0 THEN
						SET LS_GM_MASTER_FEED_STATUS = 'NO RESPONSE';
					ELSE
						SET LS_GM_MASTER_FEED_STATUS = 'S';
					END IF;
				END IF;
			ELSE
				SET LS_GM_MASTER_FEED_STATUS = 'S';
			END IF;
			SELECT COUNT(1) INTO LI_FLAG FROM SAP_FEED_TMPL_PROJECT_DEF 
			WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID;
			IF LI_FLAG > 0 THEN
				SELECT COUNT(1) INTO LI_ERROR_FLAG FROM SAP_FEED_TMPL_PROJECT_DEF 
				WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID
				AND FEED_STATUS = 'E';
				IF LI_ERROR_FLAG > 0 THEN
					SET LS_PRJ_DEF_FEED_STATUS = 'E';
				ELSE
					SELECT COUNT(1) INTO LI_ERROR_FLAG FROM SAP_FEED_TMPL_PROJECT_DEF 
					WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID
					AND FEED_STATUS IS NULL;
					IF LI_ERROR_FLAG > 0 THEN
						SET LS_PRJ_DEF_FEED_STATUS = 'NO RESPONSE';
					ELSE
						SET LS_PRJ_DEF_FEED_STATUS = 'S';
					END IF;
				END IF;
			ELSE
				SET LS_PRJ_DEF_FEED_STATUS = 'S';
			END IF;
			SELECT COUNT(1) INTO LI_FLAG FROM SAP_FEED_TMPL_WBS 
			WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID;
			IF LI_FLAG > 0 THEN
				SELECT COUNT(1) INTO LI_ERROR_FLAG FROM SAP_FEED_TMPL_WBS 
				WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID
				AND FEED_STATUS = 'E';
				IF LI_ERROR_FLAG > 0 THEN
					SET LS_WBS_FEED_STATUS = 'E';
				ELSE
					SELECT COUNT(1) INTO LI_ERROR_FLAG FROM SAP_FEED_TMPL_WBS 
					WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID
					AND FEED_STATUS IS NULL;
					IF LI_ERROR_FLAG > 0 THEN
						SET LS_WBS_FEED_STATUS  = 'NO RESPONSE';
					ELSE
						SET LS_WBS_FEED_STATUS = 'S';
					END IF;
				END IF;
			ELSE
				SET LS_WBS_FEED_STATUS = 'S';
			END IF;
			SELECT COUNT(1) INTO LI_FLAG FROM SAP_FEED_TMPL_FM_BUDGET 
			WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID;
			IF LI_FLAG > 0 THEN
				SELECT COUNT(1) INTO LI_ERROR_FLAG FROM SAP_FEED_TMPL_FM_BUDGET 
				WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID
				AND FEED_STATUS = 'E';
				IF LI_ERROR_FLAG > 0 THEN
					SET LS_FM_FEED_STATUS = 'E';
				ELSE
					SELECT COUNT(1) INTO LI_ERROR_FLAG FROM SAP_FEED_TMPL_FM_BUDGET 
					WHERE BATCH_ID = LS_BATCH_ID AND FEED_ID = LI_FEED_ID
					AND FEED_STATUS IS NULL;
					IF LI_ERROR_FLAG > 0 THEN
						SET LS_FM_FEED_STATUS = 'NO RESPONSE';
					ELSE
						SET LS_FM_FEED_STATUS = 'S';
					END IF;
				END IF;
			ELSE
				SET LS_FM_FEED_STATUS = 'S';
			END IF;
			INSERT INTO SAP_FEED_DISCREPANCY_AWARDS
			(
				BATCH_ID, 
				FEED_ID, 
				AWARD_ID, 
				AWARD_NUMBER, 
				ACCOUNT_NUMBER, 
				SEQUENCE_NUMBER, 
				SPONSOR_PRGM_STATUS, 
				SPONSOR_CLASS_STATUS, 
				FUNDED_PGM_STATUS, 
				GM_BUDGET_STATUS, 
				GM_MASTER_STATUS, 
				PRJ_DEFINITION_STATUS, 
				WBS_STATUS, 
				FM_BUDGET_STATUS
			)
			VALUES
			(
				LS_BATCH_ID,
				LI_FEED_ID,
				LI_AWARD_ID,
				LS_AWARD_NUMBER,
				LS_ACCOUNT_NUMBER,
				LI_SEQUENCE_NUMBER,
				LS_SPONSOR_PRGM_FEED_STATUS,
				LS_SPONSOR_CLASS_FEED_STATUS,
				LS_FUNDED_PGM_FEED_STATUS,
				LS_GM_BUDGET_FEED_STATUS,
				LS_GM_MASTER_FEED_STATUS,
				LS_PRJ_DEF_FEED_STATUS,
				LS_WBS_FEED_STATUS,
				LS_FM_FEED_STATUS	
			);
			SET LS_SPONSOR_PRGM_FEED_STATUS = NULL;
			SET LS_SPONSOR_CLASS_FEED_STATUS = NULL;
			SET LS_FUNDED_PGM_FEED_STATUS = NULL;
			SET LS_GM_BUDGET_FEED_STATUS  = NULL;
			SET LS_GM_MASTER_FEED_STATUS  = NULL;
			SET LS_PRJ_DEF_FEED_STATUS    = NULL;
			SET LS_WBS_FEED_STATUS		  = NULL;
			SET LS_FM_FEED_STATUS		  = NULL;
			SET LS_ACCOUNT_NUMBER		  = NULL;
			END LOOP;
			CLOSE CUR_FEED_DATA;
			COMMIT;
		END;
END
//
