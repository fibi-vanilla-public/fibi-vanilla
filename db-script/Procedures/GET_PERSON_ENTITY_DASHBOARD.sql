DELIMITER //
CREATE  PROCEDURE `GET_PERSON_ENTITY_DASHBOARD`(
		AV_ENTITY_ID                    INT(11),
        AV_TYPE                         VARCHAR(30),
        AV_INVOLMENT_START_DATE         VARCHAR(30),
        AV_INVOLMENT_END_DATE           VARCHAR(30),
        AV_PERSON_NAME                  VARCHAR(30),
        AV_STATUS_FLAG                  VARCHAR(3),
        AV_SORT_TYPE                    VARCHAR(500),
        AV_PAGED                        INT(10),
        AV_LIMIT                        INT(10)
)
BEGIN
DECLARE LS_DYN_SQL LONGTEXT;
DECLARE LS_FILTER_CONDITION LONGTEXT;
DECLARE LS_OFFSET_CONDITION VARCHAR(600);
DECLARE LS_OFFSET INT(11);
DECLARE TAB_QUERY LONGTEXT;
DECLARE JOIN_CONDITION LONGTEXT;
DECLARE SELECTED_FIELD_LIST LONGTEXT;
SET LS_OFFSET = (AV_LIMIT * AV_PAGED);
SET LS_FILTER_CONDITION ='';
SET LS_DYN_SQL ='';
SET JOIN_CONDITION = '';
SET SELECTED_FIELD_LIST= '';
SET TAB_QUERY = '';
SET LS_OFFSET_CONDITION = CONCAT(' LIMIT ',AV_LIMIT,' OFFSET ',LS_OFFSET);
IF AV_TYPE IS NOT NULL AND AV_TYPE <> '' AND  AV_TYPE = 'PERSON' THEN 
    SET JOIN_CONDITION = CONCAT(JOIN_CONDITION,' WHERE T1.ENTITY_ID =',AV_ENTITY_ID);
END IF;
IF AV_INVOLMENT_START_DATE IS NOT NULL THEN
    SET LS_FILTER_CONDITION = CONCAT(' AND T1.INVOLVEMENT_START_DATE >= ',AV_INVOLMENT_START_DATE);
END IF;
IF AV_INVOLMENT_END_DATE IS NOT NULL THEN
    SET LS_FILTER_CONDITION = CONCAT(' AND T1.INVOLVEMENT_END_DATE <= ',AV_INVOLMENT_END_DATE);
END IF;
IF AV_PERSON_NAME IS NOT NULL THEN
    SET LS_FILTER_CONDITION = CONCAT(' AND T2.FULL_NAME LIKE %',AV_PERSON_NAME, '%');
END IF;
IF AV_STATUS_FLAG IS NOT NULL THEN
    SET LS_FILTER_CONDITION = CONCAT(' AND T1.IS_RELATIONSHIP_ACTIVE IN (',AV_STATUS_FLAG,')');
END IF;
IF AV_SORT_TYPE IS NULL THEN
        SET AV_SORT_TYPE =  CONCAT(' ORDER BY T.UPDATE_TIMESTAMP DESC ');
ELSE
    SET AV_SORT_TYPE = CONCAT(' ORDER BY ',AV_SORT_TYPE);
END IF;
    SET LS_DYN_SQL = CONCAT('SELECT DISTINCT *  FROM (SELECT T1.PERSON_ENTITY_ID, T1.PERSON_ID, T2.FULL_NAME, T1.ENTITY_ID, T1.ENTITY_NUMBER, T1.IS_RELATIONSHIP_ACTIVE, 
                    T1.VERSION_NUMBER, T1.VERSION_STATUS, T1.INVOLVEMENT_START_DATE, T1.INVOLVEMENT_END_DATE, T1.STUDENT_INVOLVEMENT, T1.STAFF_INVOLVEMENT, T1.UPDATE_TIMESTAMP,
                    T1.INSTITUTE_RESOURCE_INVOLVEMENT FROM PERSON_ENTITY T1 INNER JOIN PERSON T2 ON T2.PERSON_ID = T1.PERSON_ID', JOIN_CONDITION,') T', LS_FILTER_CONDITION, ' ',
                    AV_SORT_TYPE,' ',LS_OFFSET_CONDITION);
SET @QUERY_STATEMENT = LS_DYN_SQL;
PREPARE EXECUTABLE_STAEMENT FROM @QUERY_STATEMENT;
EXECUTE EXECUTABLE_STAEMENT;
END
//
