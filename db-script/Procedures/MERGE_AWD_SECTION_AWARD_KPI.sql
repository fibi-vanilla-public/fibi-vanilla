DELIMITER //
CREATE  PROCEDURE `MERGE_AWD_SECTION_AWARD_KPI`(
AV_AWARD_ID 			INT,
AV_AWARD_NUMBER			VARCHAR(12),
AV_SEQUENCE_NUMBER		INT(4),
AV_VARIATION_TYPE_CODE	VARCHAR(3),
AV_UPDATE_USER			VARCHAR(60)
)
BEGIN
DECLARE LS_ERROR_MSG						VARCHAR(1000);
DECLARE LS_TABLE_NAME						VARCHAR(30);
DECLARE LI_FLAG								INT;
DECLARE LI_MASTER_AWARD_ID					DECIMAL(22,0);
DECLARE LI_MASTER_SEQ_NUMBER				INT(4);
DECLARE LI_AWARD_KPI_NEXT_VAL				BIGINT(20);
DECLARE LI_AWARD_KPI_CNT					INT(10);
DECLARE LI_AWARD_KPI_ID			INT(10);
DECLARE LS_KPI_TYPE_CODE		VARCHAR(3);
DECLARE LS_UPDATE_TIMESTAMP		DATETIME;
DECLARE LS_UPDATE_USER			VARCHAR(60);
DECLARE LI_AWARD_KPI_CRITERIA_ID	INT(10);
DECLARE LS_KPI_CRITERIA_TYPE_CODE	VARCHAR(3);
DECLARE LI_TARGET					INT(3);
DECLARE LI_AWARD_KPI_CRIT_NEXT_VAL	BIGINT(20);
DECLARE LI_AWARD_KPI_CRIT_CNT		INT(10);
DECLARE LS_DOCUMENT_UPDATE_USER			VARCHAR(60);
DECLARE LS_DOCUMENT_UPDATE_TIMESTAMP	DATETIME;
	BEGIN
			DECLARE EXIT HANDLER FOR SQLEXCEPTION
			BEGIN
				GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, 
				 @errno = MYSQL_ERRNO, @msg = MESSAGE_TEXT;
				SET @full_error = CONCAT(@msg,'|SEC122|',LS_TABLE_NAME );
				SELECT @full_error INTO LS_ERROR_MSG;
				RESIGNAL SET MESSAGE_TEXT = LS_ERROR_MSG;
			END;
			SELECT COUNT(1) INTO LI_FLAG
			FROM AWARD 
			WHERE AWARD_NUMBER = AV_AWARD_NUMBER
			AND SEQUENCE_NUMBER = 0;
			IF LI_FLAG > 0 THEN
				SELECT AWARD_ID,SEQUENCE_NUMBER 
				INTO LI_MASTER_AWARD_ID,LI_MASTER_SEQ_NUMBER
				FROM AWARD 
				WHERE AWARD_NUMBER = AV_AWARD_NUMBER
				AND SEQUENCE_NUMBER = 0;
			END IF;
			DELETE FROM AWARD_KPI_CRITERIA
			WHERE AWARD_KPI_ID IN(SELECT AWARD_KPI_ID FROM AWARD_KPI
					 WHERE AWARD_ID = LI_MASTER_AWARD_ID);
			DELETE FROM AWARD_KPI
			WHERE AWARD_ID = LI_MASTER_AWARD_ID;
			SET LS_TABLE_NAME = 'AWARD_KPI';
            BEGIN
				DECLARE DONE1 INT DEFAULT FALSE;
				DECLARE CUR_AWARD_KPI CURSOR FOR
				SELECT 
					AWARD_KPI_ID, 
					KPI_TYPE_CODE, 
					UPDATE_TIMESTAMP, 
					UPDATE_USER
				FROM AWARD_KPI
				WHERE AWARD_ID = AV_AWARD_ID;
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE1 = TRUE;
				OPEN CUR_AWARD_KPI;
				INSERT_AWAD_KPI_LOOP: LOOP 
				FETCH CUR_AWARD_KPI INTO
				LI_AWARD_KPI_ID,
				LS_KPI_TYPE_CODE,
				LS_UPDATE_TIMESTAMP,
				LS_UPDATE_USER;
				IF DONE1 THEN
					LEAVE INSERT_AWAD_KPI_LOOP;
				END IF;
				INSERT INTO AWARD_KPI(
				AWARD_ID, 
				AWARD_NUMBER, 
				SEQUENCE_NUMBER, 
				KPI_TYPE_CODE, 
				UPDATE_TIMESTAMP, 
				UPDATE_USER
				)
				VALUES
				(
				LI_MASTER_AWARD_ID,
				AV_AWARD_NUMBER,
				LI_MASTER_SEQ_NUMBER,
				LS_KPI_TYPE_CODE,
				LS_UPDATE_TIMESTAMP,
				LS_UPDATE_USER
				);
				SELECT LAST_INSERT_ID() INTO LI_AWARD_KPI_NEXT_VAL;
				SET LS_TABLE_NAME = 'AWARD_KPI_CRITERIA';
				BEGIN
					DECLARE DONE2 INT DEFAULT FALSE;
					DECLARE CUR_AWARD_KPI_CRITERIA CURSOR FOR
					SELECT 
						AWARD_KPI_CRITERIA_ID, 
						KPI_CRITERIA_TYPE_CODE, 
						TARGET, 
						UPDATE_TIMESTAMP, 
						UPDATE_USER
					FROM AWARD_KPI_CRITERIA
					WHERE AWARD_KPI_ID = LI_AWARD_KPI_ID;
					DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE2 = TRUE;
					OPEN CUR_AWARD_KPI_CRITERIA;
					INSERT_AWAD_KPI_CRIT_LOOP: LOOP 
					FETCH CUR_AWARD_KPI_CRITERIA INTO
					LI_AWARD_KPI_CRITERIA_ID,
					LS_KPI_CRITERIA_TYPE_CODE,
					LI_TARGET,
					LS_UPDATE_TIMESTAMP,
					LS_UPDATE_USER;
					IF DONE2 THEN
						LEAVE INSERT_AWAD_KPI_CRIT_LOOP;
					END IF;
					INSERT INTO AWARD_KPI_CRITERIA(
					AWARD_KPI_ID, 
					KPI_CRITERIA_TYPE_CODE, 
					TARGET, 
					UPDATE_TIMESTAMP, 
					UPDATE_USER)
					VALUES
					(
					LI_AWARD_KPI_NEXT_VAL,
					LS_KPI_CRITERIA_TYPE_CODE,
					LI_TARGET,
					LS_UPDATE_TIMESTAMP,
					LS_UPDATE_USER
					);
					END LOOP;
					CLOSE CUR_AWARD_KPI_CRITERIA;
				END;
				END LOOP;
				CLOSE CUR_AWARD_KPI;
			END;
	END;
END
//
