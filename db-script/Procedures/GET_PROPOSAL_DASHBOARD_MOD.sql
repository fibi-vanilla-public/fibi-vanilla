DELIMITER //
CREATE  PROCEDURE `GET_PROPOSAL_DASHBOARD_MOD`(
AV_PROPOSAL_ID                                   VARCHAR(20),
AV_TITLE                                 VARCHAR(200),
AV_RESEARCH_PERSON_ID            VARCHAR(200),
AV_CATEGORY_TYPE                         VARCHAR(200),
AV_PROPOSAL_TYPE                 VARCHAR(200),
AV_SPONSOR_NAME                  VARCHAR(200),
AV_HOME_UNIT_NAME                VARCHAR(200),
AV_PROPOSAL_STATUS               VARCHAR(200),
AV_EXTERNAL_FUNDING_AGENCY_ID    VARCHAR(200),
AV_GRANT_HEADER_ID                               VARCHAR(200),
AV_KEYWORD                               VARCHAR(200),
AV_PERSON_ID                             VARCHAR(200),
AV_SORT_TYPE                             VARCHAR(500),
AV_PAGED                                         INT(10),
AV_LIMIT                                         INT(10),
AV_TAB_TYPE                                  VARCHAR(30),
AV_UNLIMITED                     BOOLEAN,
AV_TYPE                          VARCHAR(1),
AV_APPLICATION_ID                VARCHAR(255),
AV_ROLE_ID                          VARCHAR(20)
)
BEGIN
DECLARE LS_DYN_SQL LONGTEXT;
DECLARE LS_FILTER_CONDITION LONGTEXT;
DECLARE LS_OFFSET_CONDITION VARCHAR(600);
DECLARE LS_OFFSET INT(11);
DECLARE TAB_QUERY LONGTEXT;
DECLARE JOIN_CONDITION LONGTEXT;
DECLARE SELECTED_FIELD_LIST LONGTEXT;
SET LS_OFFSET = (AV_LIMIT * AV_PAGED);
SET LS_FILTER_CONDITION ='';
SET LS_DYN_SQL ='';
SET JOIN_CONDITION = '';
SET SELECTED_FIELD_LIST= '';
IF AV_TYPE = 'A' THEN
                        IF AV_PROPOSAL_ID IS NOT NULL AND AV_PROPOSAL_ID <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.PROPOSAL_ID LIKE ''%',AV_PROPOSAL_ID,'%'' AND ');
                        END IF;
                        IF AV_CATEGORY_TYPE IS NOT NULL  AND AV_CATEGORY_TYPE <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.ACTIVITY_TYPE_CODE IN (',AV_CATEGORY_TYPE,') AND ');
                     SET SELECTED_FIELD_LIST= CONCAT(SELECTED_FIELD_LIST,' ,T30.DESCRIPTION AS CATEGORY ');
                        END IF;
                        IF AV_PROPOSAL_STATUS IS NOT NULL  AND AV_PROPOSAL_STATUS <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.STATUS_CODE in (',AV_PROPOSAL_STATUS,') AND ');
            END IF;
                        IF AV_TITLE IS NOT NULL  AND AV_TITLE <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.TITLE LIKE ''%',AV_TITLE,'%'' AND ');
                        END IF;
                        IF AV_HOME_UNIT_NAME IS NOT NULL  AND AV_HOME_UNIT_NAME <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.HOME_UNIT_NAME LIKE ''%',AV_HOME_UNIT_NAME,'%'' AND ');
                        END IF;
                        IF AV_SPONSOR_NAME IS NOT NULL  AND AV_SPONSOR_NAME <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.SPONSOR_NAME LIKE ''%',AV_SPONSOR_NAME,'%'' AND ');
                        END IF;
                        IF AV_PROPOSAL_TYPE IS NOT NULL  AND AV_PROPOSAL_TYPE <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.PROPOSAL_TYPE_CODE IN (',AV_PROPOSAL_TYPE,') AND ');
                        END IF;
                        IF AV_EXTERNAL_FUNDING_AGENCY_ID IS NOT NULL  AND AV_EXTERNAL_FUNDING_AGENCY_ID <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.EXTERNAL_FUNDING_AGENCY_ID LIKE ''%',AV_EXTERNAL_FUNDING_AGENCY_ID,'%'' AND ');
                        END IF;
                        IF AV_GRANT_HEADER_ID IS NOT NULL  AND AV_GRANT_HEADER_ID <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.GRANT_HEADER_ID IN (',AV_GRANT_HEADER_ID,') AND ');
                        END IF;
                        IF AV_KEYWORD IS NOT NULL  AND AV_KEYWORD <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.KEYWORD LIKE ''%',AV_KEYWORD,'%'' AND ');
             SET JOIN_CONDITION = CONCAT(JOIN_CONDITION,' LEFT JOIN EPS_PROPOSAL_KEYWORDS T9 ON T9.PROPOSAL_ID = T1.PROPOSAL_ID
                                                                        LEFT JOIN  SCIENCE_KEYWORD  T15 ON T15.SCIENCE_KEYWORD_CODE = T9.SCIENCE_KEYWORD_CODE ');
                         SET SELECTED_FIELD_LIST= CONCAT(SELECTED_FIELD_LIST,' ,GROUP_CONCAT(IFNULL(T9.KEYWORD,''''),IFNULL(T15.DESCRIPTION,'''')   SEPARATOR '','')  AS KEYWORD ');
                        END IF;
            IF AV_APPLICATION_ID IS NOT NULL  AND AV_APPLICATION_ID <> '' THEN
                                SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.APPLICATION_ID LIKE ''%',AV_APPLICATION_ID,'%'' AND ');
            END IF;
                        IF (AV_ROLE_ID IS NOT NULL AND AV_ROLE_ID <> '') and (AV_RESEARCH_PERSON_ID IS NOT NULL AND AV_RESEARCH_PERSON_ID <> '') THEN
                                        SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.PROPOSAL_ID IN (SELECT PROPOSAL_ID
                                                                                                                                                                                 FROM EPS_PROPOSAL_PERSONS WHERE PROP_PERSON_ROLE_ID IN(
                                                                                                                                                                ',AV_ROLE_ID,') AND (PERSON_ID =''',AV_RESEARCH_PERSON_ID,''' OR ROLODEX_ID =''',AV_RESEARCH_PERSON_ID,''')) AND ' );
                        END IF;
                        IF (AV_ROLE_ID IS NOT NULL AND AV_ROLE_ID <> '') and (AV_RESEARCH_PERSON_ID IS NULL OR AV_RESEARCH_PERSON_ID = '') THEN
                                SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.PROPOSAL_ID IN (SELECT PROPOSAL_ID
                                                                                                                                                                                 FROM EPS_PROPOSAL_PERSONS WHERE PROP_PERSON_ROLE_ID IN(
                                                                                                                                                                ',AV_ROLE_ID,')) AND ');
                        END IF;
                        IF (AV_ROLE_ID is null OR AV_ROLE_ID = '') AND (AV_RESEARCH_PERSON_ID IS NOT NULL AND AV_RESEARCH_PERSON_ID <> '') THEN
                                SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.PROPOSAL_ID IN(SELECT PROPOSAL_ID FROM EPS_PROPOSAL_PERSONS
                                                                                                                                                                        WHERE (PERSON_ID =''',AV_RESEARCH_PERSON_ID,''' OR ROLODEX_ID =''',AV_RESEARCH_PERSON_ID,''')) AND ' );
                        END IF;
END IF;
IF AV_TAB_TYPE = 'MY_PROPOSAL' THEN
				 SET TAB_QUERY = CONCAT(' WHERE   T8.PERSON_ID = ''',AV_PERSON_ID,''' AND T8.PERSON_ROLE_ID = 3' );
ELSEIF AV_TAB_TYPE = 'ALL_PROPOSALS' THEN
                  SET TAB_QUERY = CONCAT(' ((T1.CREATE_USER = (SELECT USER_NAME FROM PERSON WHERE PERSON_ID = ''',AV_PERSON_ID,''')
                  OR T14.PERSON_ID = ''',AV_PERSON_ID,''' OR T3.PERSON_ID = ''',AV_PERSON_ID,''' OR T1.HOME_UNIT_NUMBER IN (SELECT UNIT_NUMBER FROM UNIT_ADMINISTRATOR WHERE PERSON_ID = ''',AV_PERSON_ID,''' AND UNIT_ADMINISTRATOR_TYPE_CODE = 3)
                  OR T1.HOME_UNIT_NUMBER IN (SELECT DISTINCT UNIT_NUMBER FROM PERSON_ROLE_RT WHERE RIGHT_NAME IN (''MODIFY_PROPOSAL'',''VIEW_PROPOSAL'',''VIEW_ANY_PROPOSAL'') AND PERSON_ID = ''',AV_PERSON_ID,''') )
           AND T1.STATUS_CODE NOT IN (12) )' );
                  SET JOIN_CONDITION =  CONCAT(JOIN_CONDITION,' LEFT OUTER JOIN EPS_PROPOSAL_PERSON_ROLES T3 ON T1.PROPOSAL_ID = T3.PROPOSAL_ID  LEFT JOIN EPS_PROPOSAL_PERSONS T14 ON T14.PROPOSAL_ID = T1.PROPOSAL_ID ');
                  SET SELECTED_FIELD_LIST= CONCAT(SELECTED_FIELD_LIST,' ,T3.PERSON_ID AS EPS_PROPOSAL_PERSON_ROLE_ID');
ELSEIF AV_TAB_TYPE = 'INPROGRESS_PROPOSAL' THEN
                   SET TAB_QUERY = CONCAT(' (T1.STATUS_CODE NOT IN(1,3,4,5,6,7,9,10,11,12,20,22,24,29,30) AND T1.HOME_UNIT_NUMBER IN
                ((SELECT DISTINCT UNIT_NUMBER FROM PERSON_ROLE_RT WHERE RIGHT_NAME IN (''MODIFY_PROPOSAL'',''VIEW_PROPOSAL'',''VIEW_ANY_PROPOSAL'') AND PERSON_ID = ''',AV_PERSON_ID,''')
                UNION
                (SELECT DISTINCT UNIT_NUMBER FROM UNIT_ADMINISTRATOR WHERE PERSON_ID = ''',AV_PERSON_ID,''' AND UNIT_ADMINISTRATOR_TYPE_CODE = 3))) ' );
ELSEIF AV_TAB_TYPE = 'REVIEW_COMPLETED_PROPOSALS' THEN
                   SET TAB_QUERY = CONCAT(' (T1.STATUS_CODE IN(28,31,18,21,11) AND T1.HOME_UNIT_NUMBER IN
                ((SELECT DISTINCT UNIT_NUMBER FROM PERSON_ROLE_RT WHERE RIGHT_NAME IN (''MODIFY_PROPOSAL'',''VIEW_PROPOSAL'',''VIEW_ANY_PROPOSAL'') AND PERSON_ID = ''',AV_PERSON_ID,''')
                UNION
                (SELECT DISTINCT UNIT_NUMBER FROM UNIT_ADMINISTRATOR WHERE PERSON_ID = ''',AV_PERSON_ID,''' AND UNIT_ADMINISTRATOR_TYPE_CODE = 3))) ' );
ELSEIF AV_TAB_TYPE = 'MY_REVIEW_PENDING_PROPOSAL' THEN
              SET TAB_QUERY = CONCAT(' ((T10.REVIEWER_PERSON_ID = ''',AV_PERSON_ID,'''  AND T10.PRE_REVIEW_STATUS_CODE = ''1'' AND T10.PRE_REVIEW_TYPE_CODE = ''1'' AND T10.MODULE_ITEM_CODE = 3)
                                                                  OR (T11.REVIEW_STATUS_CODE = 1 AND T11.REVIEWER_PERSON_ID = ''',AV_PERSON_ID,''' )
                                                                  OR (T12.IS_WORKFLOW_ACTIVE = ''Y'' AND T13.APPROVER_PERSON_ID = ''',AV_PERSON_ID,'''  AND  T13.APPROVAL_STATUS = ''W'')) ' );
                  SET JOIN_CONDITION =  CONCAT(JOIN_CONDITION,' LEFT JOIN PRE_REVIEW T10 ON T10.MODULE_ITEM_KEY = T1.PROPOSAL_ID AND T10.MODULE_ITEM_CODE = 3 AND T10.MODULE_SUB_ITEM_CODE = 0 AND T10.MODULE_SUB_ITEM_KEY = 0
                                                                                 LEFT  OUTER JOIN PROPOSAL_REVIEW T11 ON T11.PROPOSAL_ID = T1.PROPOSAL_ID
                                                                                 LEFT  OUTER JOIN  WORKFLOW T12 ON T12.MODULE_ITEM_ID = T1.PROPOSAL_ID AND T12.MODULE_CODE =3
                                                                                 LEFT  OUTER JOIN  WORKFLOW_DETAIL T13 ON T13.WORKFLOW_ID = T12.WORKFLOW_ID ');
                  SET SELECTED_FIELD_LIST= CONCAT(SELECTED_FIELD_LIST,' ,T10.REVIEWER_PERSON_ID AS PREREVIEWER_PERSON_ID,
                                                T10.PRE_REVIEW_STATUS_CODE ,
                                                T10.PRE_REVIEW_TYPE_CODE,
                                                T10.MODULE_ITEM_CODE AS PRERVIEW_MODULE_ITEM_CODE,
                                                T11.REVIEW_STATUS_CODE AS PROP_REVIEW_STATUS_CODE,
                                                T11.REVIEWER_PERSON_ID AS PROP_REVIEWER_PERSON_ID,
                                                T12.IS_WORKFLOW_ACTIVE,
                                                T13.APPROVER_PERSON_ID AS WORKFLOW_APPROVER_PERSON_ID,
                                                T12.MODULE_CODE AS WORKFLOW_MODULE_CODE,
                                                T13.APPROVAL_STATUS AS WORKFLOW_APPROVAL_STATUS ');
END IF;
IF AV_SORT_TYPE IS NULL THEN
        SET AV_SORT_TYPE =  CONCAT(' ORDER BY T.UPDATE_TIMESTAMP DESC ');
ELSE
    SET AV_SORT_TYPE = CONCAT(' ORDER BY ',AV_SORT_TYPE);
END IF;
IF AV_TYPE = 'L' THEN
        SET TAB_QUERY =  CONCAT(' T1.STATUS_CODE <> 35 AND ',TAB_QUERY);
END IF;
IF AV_UNLIMITED = TRUE THEN
        SET LS_OFFSET_CONDITION = '';
ELSE
        SET LS_OFFSET_CONDITION = CONCAT(' LIMIT ',AV_LIMIT,' OFFSET ',LS_OFFSET);
END IF;
IF LS_FILTER_CONDITION <>'' THEN
SET LS_FILTER_CONDITION = CONCAT(' WHERE ',LS_FILTER_CONDITION);
SELECT TRIM(TRAILING 'AND ' FROM LS_FILTER_CONDITION) into LS_FILTER_CONDITION from dual;
END IF;
                        SET LS_DYN_SQL =CONCAT('SELECT DISTINCT *  FROM(SELECT DISTINCT  T1.PROPOSAL_ID,
                                                                                        T5.DESCRIPTION AS  PROPOSAL_STATUS,
                                                                                        T1.TITLE,
                                                                                        T1.HOME_UNIT_NAME,
                                                                                        T1.SPONSOR_NAME,
																						T1.GRANT_HEADER_ID,
																						T6.NAME,
																						T6.CLOSING_DATE,
																						T8.FULL_NAME,
																						T8.PERSON_ID AS PI_PERSON_ID,
																						T1.EXTERNAL_FUNDING_AGENCY_ID,
																						T1.CREATE_USER AS EPS_PROPOSAL_CREATE_USER,
																						T8.PERSON_ID AS EPS_PROPOSAL_PERSON_ID,
																						T1.HOME_UNIT_NUMBER,
																						T1.UPDATE_TIMESTAMP,
																						T1.TYPE_CODE AS PROPOSAL_TYPE_CODE,
																						T7.DESCRIPTION AS PROPOSAL_TYPE,
																						T1.SPONSOR_DEADLINE_DATE,
																						T1.APPLICATION_ID,
																						T1.ACTIVITY_TYPE_CODE,
																						T1.IP_NUMBER,
																						T1.GRANT_TYPE_CODE,
																						T1.PROPOSAL_RANK,
																						T1.EVALUATION_RECOMMENDATION_CODE,
																						T1.STATUS_CODE ,
																						T30.DESCRIPTION AS ACTIVITY_TYPE ,
																						T1.SUBMIT_USER ,
																						T1.SUBMISSION_DATE ,
                                            T25.GRANT_CALL_CATEGORY_CODE,T6.ABBREVIATION ',SELECTED_FIELD_LIST,
                                                                                        ' FROM EPS_PROPOSAL T1
                                                                                        LEFT OUTER JOIN EPS_PROPOSAL_STATUS T5 ON T5.STATUS_CODE = T1.STATUS_CODE
                                                                                        LEFT  OUTER  JOIN GRANT_CALL_HEADER T6 ON T6.GRANT_HEADER_ID = T1.GRANT_HEADER_ID
                                            LEFT OUTER JOIN  GRANT_CALL_TYPE T25 ON T25.GRANT_TYPE_CODE = T6.GRANT_TYPE_CODE
                                                                                        LEFT OUTER JOIN EPS_PROPOSAL_TYPE T7 ON T7.TYPE_CODE = T1.TYPE_CODE
                                                                                        LEFT  OUTER  JOIN EPS_PROPOSAL_PERSONS T8 ON T8.PROPOSAL_ID = T1.PROPOSAL_ID AND T8.PROP_PERSON_ROLE_ID = 3 AND T8.PI_FLAG = ''Y''
                                                                                        LEFT JOIN ACTIVITY_TYPE T30 ON T30.ACTIVITY_TYPE_CODE = T1.ACTIVITY_TYPE_CODE ',JOIN_CONDITION,' WHERE ',TAB_QUERY,
                                                                                        ' GROUP BY T1.PROPOSAL_ID)T ',LS_FILTER_CONDITION,' ',AV_SORT_TYPE,' ',LS_OFFSET_CONDITION );
SET @QUERY_STATEMENT = LS_DYN_SQL;
PREPARE EXECUTABLE_STAEMENT FROM @QUERY_STATEMENT;
EXECUTE EXECUTABLE_STAEMENT;
END
//
