DELIMITER //
CREATE  PROCEDURE `GET_AWARD_DASHBOARD_test1`(
AV_AWARD_ID                     VARCHAR(20),
AV_AWARD_NUMBER         VARCHAR(20),
AV_ACCOUNT_NUMBER       VARCHAR(200),
AV_TITLE                VARCHAR(200),
AV_LEAD_UNIT_NUMBER     VARCHAR(200),
AV_SPONSOR_NAME         VARCHAR(200),
AV_RESEARCH_PERSON_ID   VARCHAR(200),
AV_AWARD_STATUS             VARCHAR(200),
AV_GRANT_HEADER_ID                               VARCHAR(200),
AV_KEYWORD                               VARCHAR(200),
AV_PERSON_ID                             VARCHAR(200),
AV_SORT_TYPE                             VARCHAR(500),
AV_PAGED                                         INT(10),
AV_LIMIT                                         INT(10),
AV_TAB_TYPE                                  VARCHAR(30),
AV_UNLIMITED                     BOOLEAN,
AV_TYPE                          VARCHAR(1),
AV_ROLE_ID                          VARCHAR(20),
AV_SPONSOR_AWARD_NUMBER          VARCHAR(200)
)
BEGIN
DECLARE LS_DYN_SQL LONGTEXT;
DECLARE LS_FILTER_CONDITION LONGTEXT;
DECLARE LS_OFFSET_CONDITION VARCHAR(600);
DECLARE LS_OFFSET INT(11);
DECLARE TAB_QUERY LONGTEXT;
DECLARE TAB_QUERY1 LONGTEXT;
DECLARE JOIN_CONDITION LONGTEXT;
DECLARE SELECTED_FIELD_LIST LONGTEXT;
SET LS_OFFSET = (AV_LIMIT * AV_PAGED);
SET LS_FILTER_CONDITION ='';
SET LS_DYN_SQL ='';
SET JOIN_CONDITION = '';
SET SELECTED_FIELD_LIST= '';
IF AV_TYPE = 'A'   THEN
                        IF AV_AWARD_ID IS NOT NULL AND AV_AWARD_ID <> '' THEN
                          SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.AWARD_ID LIKE ''%',AV_AWARD_ID,'%'' AND ');
                        END IF;
                        IF AV_ACCOUNT_NUMBER IS NOT NULL  AND AV_ACCOUNT_NUMBER <> '' THEN
                                SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.ACCOUNT_NUMBER LIKE ''%',AV_ACCOUNT_NUMBER,'%'' AND ');
                        END IF;
                        IF AV_TITLE IS NOT NULL  AND AV_TITLE <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.TITLE LIKE ''%',AV_TITLE,'%'' AND ');
            END IF;
                        IF AV_LEAD_UNIT_NUMBER IS NOT NULL  AND AV_LEAD_UNIT_NUMBER <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.LEAD_UNIT_NUMBER LIKE ''%',AV_LEAD_UNIT_NUMBER,'%'' AND ');
                        END IF;
                        IF AV_SPONSOR_NAME IS NOT NULL  AND AV_SPONSOR_NAME <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.SPONSOR_NAME LIKE ''%',AV_SPONSOR_NAME,'%'' AND ');
                        END IF;
                        IF AV_AWARD_STATUS IS NOT NULL  AND AV_AWARD_STATUS <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.STATUS_CODE IN (',AV_AWARD_STATUS,')  AND ');
                        END IF;
                        IF AV_GRANT_HEADER_ID IS NOT NULL  AND AV_GRANT_HEADER_ID <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.GRANT_HEADER_ID IN (',AV_GRANT_HEADER_ID,') AND ');
                        END IF;
            IF AV_AWARD_NUMBER IS NOT NULL  AND AV_AWARD_NUMBER <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.AWARD_NUMBER LIKE ''%',AV_AWARD_NUMBER,'%'' AND ');
                        END IF;
                        IF AV_KEYWORD IS NOT NULL  AND AV_KEYWORD <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.KEYWORD LIKE ''%',AV_KEYWORD,'%'' AND ');
             SET JOIN_CONDITION = CONCAT(JOIN_CONDITION,' LEFT JOIN AWARD_SCIENCE_KEYWORD T11 ON T11.AWARD_ID = T1.AWARD_ID
                                                                        LEFT JOIN  SCIENCE_KEYWORD  T15 ON T15.SCIENCE_KEYWORD_CODE = T11.SCIENCE_KEYWORD_CODE ');
                         SET SELECTED_FIELD_LIST= CONCAT(SELECTED_FIELD_LIST,' ,GROUP_CONCAT(IFNULL(T11.KEYWORD,''''),IFNULL(T15.DESCRIPTION,'''')   SEPARATOR '','')  AS KEYWORD ');
                        END IF;
                        IF (AV_ROLE_ID IS NOT NULL AND AV_ROLE_ID <> '') and (AV_RESEARCH_PERSON_ID IS NOT NULL AND AV_RESEARCH_PERSON_ID <> '') THEN
                                        SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.AWARD_ID IN (SELECT AWARD_ID
                                                                                                                                                                                 FROM AWARD_PERSONS WHERE PERSON_ROLE_ID IN(
                                                                                                                                                                ',AV_ROLE_ID,') AND (PERSON_ID =''',AV_RESEARCH_PERSON_ID,''' OR ROLODEX_ID =''',AV_RESEARCH_PERSON_ID,''')) AND ');
                        END IF;
                        IF (AV_ROLE_ID IS NOT NULL AND AV_ROLE_ID <> '') and (AV_RESEARCH_PERSON_ID IS NULL OR AV_RESEARCH_PERSON_ID = '') THEN
                                SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.AWARD_ID IN (SELECT AWARD_ID
                                                                                                                                                                                 FROM AWARD_PERSONS WHERE PERSON_ROLE_ID IN(
                                                                                                                                                                ',AV_ROLE_ID,')) AND ');
                        END IF;
                        IF (AV_ROLE_ID is null OR AV_ROLE_ID = '') AND (AV_RESEARCH_PERSON_ID IS NOT NULL AND AV_RESEARCH_PERSON_ID <> '') THEN
                                SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.AWARD_ID IN(SELECT AWARD_ID FROM AWARD_PERSONS
                                                                                                                                                                        WHERE (PERSON_ID =''',AV_RESEARCH_PERSON_ID,''' OR ROLODEX_ID =''',AV_RESEARCH_PERSON_ID,''')) AND ' );
                        END IF;
                        IF AV_SPONSOR_AWARD_NUMBER IS NOT NULL  AND AV_SPONSOR_AWARD_NUMBER <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.SPONSOR_AWARD_NUMBER LIKE ''%',AV_SPONSOR_AWARD_NUMBER,'%'' AND ');
                        END IF;
END IF;
IF AV_TAB_TYPE = 'MY_AWARDS' THEN
                        SET TAB_QUERY = CONCAT(' WHERE (((  T12.PERSON_ID = ''',AV_PERSON_ID,''' AND T12.PERSON_ROLE_ID = 3))
                        AND T1.AWARD_SEQUENCE_STATUS = ''ACTIVE''  AND T4.STATUS_CODE NOT IN (11,12,5,4) )' );
ELSEIF AV_TAB_TYPE = 'ALL_AWARDS' THEN
		SET TAB_QUERY = CONCAT(' WHERE (T1.LEAD_UNIT_NUMBER IN(SELECT DISTINCT UNIT_NUMBER FROM PERSON_ROLE_RT WHERE RIGHT_NAME IN (''VIEW_AWARD'', ''MODIFY_AWARD'')
						   AND PERSON_ID = ''',AV_PERSON_ID,''')
						   OR T7.HOME_UNIT_NUMBER IN(SELECT DISTINCT UNIT_NUMBER FROM PERSON_ROLE_RT WHERE RIGHT_NAME IN (''VIEW_AWARD'', ''MODIFY_AWARD'')
						   AND PERSON_ID = ''',AV_PERSON_ID,''')
						   OR T1.AWARD_ID IN(SELECT AWARD_ID FROM AWARD_PERSONS
																 WHERE PERSON_ID=''',AV_PERSON_ID,''')
						   OR (T1.AWARD_ID IN(SELECT DISTINCT S1.AWARD_ID FROM AWARD_PERSON_ROLES S1
											  WHERE T1.AWARD_DOCUMENT_TYPE_CODE <> 1 AND S1.PERSON_ID=''',AV_PERSON_ID,'''
											  AND T1.AWARD_SEQUENCE_STATUS  IN (''ACTIVE'')
                                                 UNION
											  SELECT DISTINCT S2.AWARD_ID FROM AWARD_PERSON_ROLES S2
											  WHERE  T1.AWARD_DOCUMENT_TYPE_CODE = 1  AND S2.PERSON_ID=''',AV_PERSON_ID,'''
												)             
								)  
						   OR (T1.AWARD_NUMBER IN(SELECT DISTINCT X1.AWARD_NUMBER 
							FROM AWARD_PERSON_ROLES X1 INNER JOIN AWARD Y1 ON X1.AWARD_ID = Y1.AWARD_ID
							WHERE Y1.AWARD_DOCUMENT_TYPE_CODE <> 1 AND X1.PERSON_ID=''',AV_PERSON_ID,'''
							AND Y1.AWARD_SEQUENCE_STATUS  IN (''ACTIVE''))
								AND T1.AWARD_SEQUENCE_STATUS  IN (''PENDING'')
								AND T1.SEQUENCE_NUMBER  > (SELECT MAX(SEQUENCE_NUMBER) FROM AWARD WHERE AWARD_NUMBER = T1.AWARD_NUMBER AND AWARD_SEQUENCE_STATUS  IN (''ACTIVE''))
								) 
						AND T1.AWARD_SEQUENCE_STATUS NOT IN (''ARCHIVE'', ''CANCELLED'') )' );
  ELSEIF AV_TAB_TYPE = 'PENDING_AWARDS' THEN
                   SET TAB_QUERY = CONCAT(' WHERE (
                        T1.AWARD_ID in (SELECT T9.MODULE_ITEM_ID FROM WORKFLOW T9
                                        INNER JOIN WORKFLOW_DETAIL T10 ON T9.WORKFLOW_ID = T10.WORKFLOW_ID
                                        WHERE T9.MODULE_ITEM_ID = T1.AWARD_ID
                                        AND T10.APPROVER_PERSON_ID = ''',AV_PERSON_ID,'''
                                        AND T9.MODULE_CODE = 1 AND T9.IS_WORKFLOW_ACTIVE = ''Y''
                                        AND T10.APPROVAL_STATUS = ''W''
                                 )
                        ) AND T4.STATUS_CODE NOT IN (11,12,5,4) AND T1.AWARD_SEQUENCE_STATUS NOT IN (''ARCHIVE'', ''CANCELLED'')' );
                        SET TAB_QUERY1 = CONCAT(' WHERE
                        T1.AWARD_ID in (SELECT T16.MODULE_ITEM_ID FROM TASK T16 WHERE T16.MODULE_ITEM_ID =  T1.AWARD_ID
                                                                                                                 AND T16.ASSIGNEE_PERSON_ID = ''',AV_PERSON_ID,''' AND T16.TASK_STATUS_CODE IN(1,2)
                                                                                                                 AND T4.STATUS_CODE NOT IN (11,12,5,4))
                         ' );
ELSEIF AV_TAB_TYPE = 'DRAFT_AWARDS' THEN
                  SET TAB_QUERY =  CONCAT('  WHERE  ((T1.CREATE_USER = (SELECT USER_NAME FROM PERSON WHERE PERSON_ID = ''',AV_PERSON_ID,''')
                                                                                                OR (T1.LEAD_UNIT_NUMBER IN (SELECT DISTINCT UNIT_NUMBER FROM PERSON_ROLE_RT WHERE RIGHT_NAME IN (''MODIFY_AWARD'',''VIEW_AWARD'') AND PERSON_ID = ''',AV_PERSON_ID,''')
                                                AND T1.AWARD_DOCUMENT_TYPE_CODE = 1) OR (T2.PERSON_ID = ''',AV_PERSON_ID,'''))
                                                                                                                 AND (  T4.STATUS_CODE NOT IN (11,12,5,4) )
                                                         AND ( T1.WORKFLOW_AWARD_STATUS_CODE NOT IN (3,5))
                                                         AND (T1.AWARD_SEQUENCE_STATUS = ''PENDING'')  )');
ELSEIF AV_TAB_TYPE = 'HOLD_AWARDS' THEN
                  SET TAB_QUERY =  CONCAT('  WHERE  ((T1.CREATE_USER = (SELECT USER_NAME FROM PERSON WHERE PERSON_ID = ''',AV_PERSON_ID,''')
                                                                                                OR (T1.LEAD_UNIT_NUMBER IN (SELECT DISTINCT UNIT_NUMBER FROM PERSON_ROLE_RT WHERE RIGHT_NAME IN (''MODIFY_AWARD'', ''VIEW_AWARD'') AND PERSON_ID = ''',AV_PERSON_ID,''')
                                                ) OR (T2.PERSON_ID = ''',AV_PERSON_ID,'''))
                                                                                                                 AND (  T4.STATUS_CODE NOT IN (11,12,5,4) )
                                                         AND ( T1.WORKFLOW_AWARD_STATUS_CODE IN (5))
                                                         AND (T1.AWARD_SEQUENCE_STATUS = ''PENDING'')  )');
END IF;
IF AV_SORT_TYPE IS NULL THEN
        SET AV_SORT_TYPE =  CONCAT(' ORDER BY T.UPDATE_TIMESTAMP DESC ');
ELSE
    SET AV_SORT_TYPE = CONCAT(' ORDER BY ',AV_SORT_TYPE);
END IF;
IF AV_UNLIMITED = TRUE THEN
        SET LS_OFFSET_CONDITION = '';
ELSE
        SET LS_OFFSET_CONDITION = CONCAT(' LIMIT ',AV_LIMIT,' OFFSET ',LS_OFFSET);
END IF;
IF LS_FILTER_CONDITION <>'' THEN
SET LS_FILTER_CONDITION = CONCAT(' WHERE  T.AWARD_SEQUENCE_STATUS NOT IN (''ARCHIVE'', ''CANCELLED'') AND ',LS_FILTER_CONDITION);
SELECT TRIM(TRAILING 'AND ' FROM LS_FILTER_CONDITION) into LS_FILTER_CONDITION from dual;
END IF;
        IF AV_TAB_TYPE <> 'PENDING_AWARDS' THEN
                        SET LS_DYN_SQL =CONCAT('SELECT DISTINCT *  FROM(SELECT T1.AWARD_ID,
                                                                        T1.AWARD_NUMBER,
                                                                        T1.ACCOUNT_NUMBER,
                                                                        T1.TITLE,
                                                                        T2.FULL_NAME,
                                    COALESCE(T2.PERSON_ID,T2.ROLODEX_ID) AS PI_PERSON_ID,
                                                                        T1.LEAD_UNIT_NUMBER,
                                                                        T3.SPONSOR_NAME,
                                                                        T4.DESCRIPTION AS AWARD_STATUS,
                                    T1.STATUS_CODE,
                                                                        T7.NAME as NAME,
                                                                        T1.GRANT_HEADER_ID,
                                    T1.UPDATE_TIMESTAMP,
                                                                        T8.UNIT_NAME ,
                                    T1.AWARD_DOCUMENT_TYPE_CODE AS AWARD_DOCUMENT_TYPE,
                                    T1.AWARD_SEQUENCE_STATUS,
                                    T1.WORKFLOW_AWARD_STATUS_CODE,
                                    T20.DESCRIPTION AS AWARD_TYPE,
                                    T22.DESCRIPTION AS AWARD_WORKFLOW_STATUS,
                                    T1.SPONSOR_AWARD_NUMBER,
                                    T23.DESCRIPTION AS AWARD_VARIATION_TYPE',SELECTED_FIELD_LIST,
                                                                        ' FROM AWARD T1
                                                                        LEFT JOIN AWARD_PERSONS T2 ON T1.AWARD_ID=T2.AWARD_ID AND T2.PERSON_ROLE_ID = 3
                                                                        LEFT JOIN SPONSOR T3 ON T3.SPONSOR_CODE = T1.SPONSOR_CODE
                                                                        LEFT JOIN AWARD_STATUS T4 ON T4.STATUS_CODE = T1.STATUS_CODE
                                                                        LEFT JOIN GRANT_CALL_HEADER T7 ON T7.GRANT_HEADER_ID = T1.GRANT_HEADER_ID
                                    LEFT JOIN AWARD_TYPE T20 ON T20.AWARD_TYPE_CODE = T1.AWARD_TYPE_CODE
                                    LEFT JOIN AWARD_DOCUMENT_TYPE T21 ON T21.AWARD_DOCUMENT_TYPE_CODE = T1.AWARD_DOCUMENT_TYPE_CODE
                                                                        LEFT JOIN UNIT T8 ON T8.UNIT_NUMBER = T1.LEAD_UNIT_NUMBER
                                    LEFT JOIN AWARD_WORKFLOW_STATUS T22 ON T22.WORKFLOW_AWARD_STATUS_CODE = T1.WORKFLOW_AWARD_STATUS_CODE
                                    LEFT JOIN SR_TYPE T23 ON T23.TYPE_CODE = T1.AWARD_VARIATION_TYPE_CODE
                                                                        LEFT JOIN AWARD_PERSONS T12 ON T1.AWARD_ID = T12.AWARD_ID ',JOIN_CONDITION,TAB_QUERY,
                                                                        ' GROUP BY T1.AWARD_ID)T ',LS_FILTER_CONDITION,' ',AV_SORT_TYPE,' ',LS_OFFSET_CONDITION );
ELSE
                                        SET LS_DYN_SQL =CONCAT('SELECT DISTINCT *  FROM(SELECT T1.AWARD_ID,
                                                                        T1.AWARD_NUMBER,
                                                                        T1.ACCOUNT_NUMBER,
                                                                        T1.TITLE,
                                                                        T2.FULL_NAME,
                                    COALESCE(T2.PERSON_ID,T2.ROLODEX_ID) AS PI_PERSON_ID,
                                                                        T1.LEAD_UNIT_NUMBER,
                                                                        T3.SPONSOR_NAME,
                                                                        T4.DESCRIPTION AS AWARD_STATUS,
                                    T1.STATUS_CODE,
                                                                        T7.NAME as NAME,
                                                                        T1.GRANT_HEADER_ID,
                                    T1.UPDATE_TIMESTAMP,
                                                                        T8.UNIT_NAME ,
                                    T1.AWARD_DOCUMENT_TYPE_CODE AS AWARD_DOCUMENT_TYPE,
                                    T1.AWARD_SEQUENCE_STATUS,
                                    T1.WORKFLOW_AWARD_STATUS_CODE,
                                    T20.DESCRIPTION AS AWARD_TYPE,
                                    T22.DESCRIPTION AS AWARD_WORKFLOW_STATUS,
                                                                        T1.SPONSOR_AWARD_NUMBER,
                                    T23.DESCRIPTION AS AWARD_VARIATION_TYPE',SELECTED_FIELD_LIST,
                                                                        ' FROM AWARD T1
                                                                        LEFT JOIN AWARD_PERSONS T2 ON T1.AWARD_ID=T2.AWARD_ID AND T2.PERSON_ROLE_ID = 3
                                                                        LEFT JOIN SPONSOR T3 ON T3.SPONSOR_CODE = T1.SPONSOR_CODE
                                                                        LEFT JOIN AWARD_STATUS T4 ON T4.STATUS_CODE = T1.STATUS_CODE
                                                                        LEFT JOIN GRANT_CALL_HEADER T7 ON T7.GRANT_HEADER_ID = T1.GRANT_HEADER_ID
                                    LEFT JOIN AWARD_TYPE T20 ON T20.AWARD_TYPE_CODE = T1.AWARD_TYPE_CODE
                                    LEFT JOIN AWARD_DOCUMENT_TYPE T21 ON T21.AWARD_DOCUMENT_TYPE_CODE = T1.AWARD_DOCUMENT_TYPE_CODE
                                                                        LEFT JOIN UNIT T8 ON T8.UNIT_NUMBER = T1.LEAD_UNIT_NUMBER
                                    LEFT JOIN AWARD_WORKFLOW_STATUS T22 ON T22.WORKFLOW_AWARD_STATUS_CODE = T1.WORKFLOW_AWARD_STATUS_CODE
                                    LEFT JOIN SR_TYPE T23 ON T23.TYPE_CODE = T1.AWARD_VARIATION_TYPE_CODE
                                                                        LEFT JOIN AWARD_PERSONS T12 ON T1.AWARD_ID = T12.AWARD_ID ',JOIN_CONDITION,TAB_QUERY,
                                                                        ' GROUP BY T1.AWARD_ID
                                                                        UNION
                                                                        SELECT T1.AWARD_ID,
                                                                        T1.AWARD_NUMBER,
                                                                        T1.ACCOUNT_NUMBER,
                                                                        T1.TITLE,
                                                                        T2.FULL_NAME,
                                    COALESCE(T2.PERSON_ID,T2.ROLODEX_ID) AS PI_PERSON_ID,
                                                                        T1.LEAD_UNIT_NUMBER,
                                                                        T3.SPONSOR_NAME,
                                                                        T4.DESCRIPTION AS AWARD_STATUS,
                                    T1.STATUS_CODE,
                                                                        T7.NAME as NAME,
                                                                        T1.GRANT_HEADER_ID,
                                    T1.UPDATE_TIMESTAMP,
                                                                        T8.UNIT_NAME ,
                                    T1.AWARD_DOCUMENT_TYPE_CODE AS AWARD_DOCUMENT_TYPE,
                                    T1.AWARD_SEQUENCE_STATUS,
                                    T1.WORKFLOW_AWARD_STATUS_CODE,
                                    T20.DESCRIPTION AS AWARD_TYPE,
                                    T22.DESCRIPTION AS AWARD_WORKFLOW_STATUS,
                                    T1.SPONSOR_AWARD_NUMBER,
                                    T23.DESCRIPTION AS AWARD_VARIATION_TYPE',SELECTED_FIELD_LIST,
                                                                        ' FROM AWARD T1
                                                                        LEFT JOIN AWARD_PERSONS T2 ON T1.AWARD_ID=T2.AWARD_ID AND T2.PERSON_ROLE_ID = 3
                                                                        LEFT JOIN SPONSOR T3 ON T3.SPONSOR_CODE = T1.SPONSOR_CODE
                                                                        LEFT JOIN AWARD_STATUS T4 ON T4.STATUS_CODE = T1.STATUS_CODE
                                                                        LEFT JOIN GRANT_CALL_HEADER T7 ON T7.GRANT_HEADER_ID = T1.GRANT_HEADER_ID
                                    LEFT JOIN AWARD_TYPE T20 ON T20.AWARD_TYPE_CODE = T1.AWARD_TYPE_CODE
                                    LEFT JOIN AWARD_DOCUMENT_TYPE T21 ON T21.AWARD_DOCUMENT_TYPE_CODE = T1.AWARD_DOCUMENT_TYPE_CODE
                                                                        LEFT JOIN UNIT T8 ON T8.UNIT_NUMBER = T1.LEAD_UNIT_NUMBER
                                    LEFT JOIN AWARD_WORKFLOW_STATUS T22 ON T22.WORKFLOW_AWARD_STATUS_CODE = T1.WORKFLOW_AWARD_STATUS_CODE
                                    LEFT JOIN SR_TYPE T23 ON T23.TYPE_CODE = T1.AWARD_VARIATION_TYPE_CODE
                                                                        LEFT JOIN AWARD_PERSONS T12 ON T1.AWARD_ID = T12.AWARD_ID ',JOIN_CONDITION,TAB_QUERY1,
                                                                        ' GROUP BY T1.AWARD_ID
                                                                        )T ',LS_FILTER_CONDITION,' ',AV_SORT_TYPE,' ',LS_OFFSET_CONDITION );
END IF;
 SELECT LS_DYN_SQL;
END
//
