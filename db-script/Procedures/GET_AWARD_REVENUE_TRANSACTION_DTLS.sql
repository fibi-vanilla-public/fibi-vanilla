DELIMITER //
CREATE  PROCEDURE `GET_AWARD_REVENUE_TRANSACTION_DTLS`(
AV_ACCOUNT_NUMBER 		 VARCHAR(100),
AV_INTERNAL_ORDER_CODE 	 VARCHAR(50),
AV_ACTUAL_COMMITED_FLAG	 VARCHAR(2),
AV_AWARD_NUMBER			VARCHAR(12)
)
BEGIN
DECLARE LI_AWARD_ID INT;
SELECT AWARD_ID INTO  LI_AWARD_ID FROM AWARD T1
WHERE T1.AWARD_NUMBER = AV_AWARD_NUMBER
AND T1.AWARD_SEQUENCE_STATUS = 'ACTIVE';
	IF AV_ACTUAL_COMMITED_FLAG = 'A' THEN
		IF AV_INTERNAL_ORDER_CODE IS NOT NULL THEN
			SELECT 
				AWARD_NUMBER, ACCOUNT_NUMBER, INTERNAL_ORDER_CODE, REMARKS, AMOUNT_IN_FMA_CURRENCY,
				ACTUAL_OR_COMMITTED_FLAG, FM_POSTING_DATE, FI_POSTING_DATE, BP_CODE, BP_NAME, 
				DOCUMENT_DATE, FI_GL_ACCOUNT, FI_GL_DESCRIPTION, TRANSACTION_REFERENCE_NUMBER,
				REFERENCE_DOCUMENT_NUMBER, REFERENCE_POSTING_LINE, GUID, GMIA_DOCNR, DOCLN, 
				RBUKRS, RVALUETYPE_9, RYEAR, GL_SIRID, BATCH_ID, GRANT_NBR, SPONSOR_PROGRAM, 
				SPONSOR_CLASS, FUND, FUND_CENTER, UPDATE_TIMESTAMP, UPDATE_USER
			FROM AWARD_REVENUE_TRANSACTIONS T1
			WHERE T1.ACCOUNT_NUMBER = AV_ACCOUNT_NUMBER
			AND T1.INTERNAL_ORDER_CODE = AV_INTERNAL_ORDER_CODE
			AND T1.ACTUAL_OR_COMMITTED_FLAG = 'A'
            AND T1.AMOUNT_IN_FMA_CURRENCY <> 0.00;
		ELSE
			SELECT 
				AWARD_NUMBER, ACCOUNT_NUMBER, INTERNAL_ORDER_CODE, REMARKS, AMOUNT_IN_FMA_CURRENCY,
				ACTUAL_OR_COMMITTED_FLAG, FM_POSTING_DATE, FI_POSTING_DATE, BP_CODE, BP_NAME, 
				DOCUMENT_DATE, FI_GL_ACCOUNT, FI_GL_DESCRIPTION, TRANSACTION_REFERENCE_NUMBER,
				REFERENCE_DOCUMENT_NUMBER, REFERENCE_POSTING_LINE, GUID, GMIA_DOCNR, DOCLN, 
				RBUKRS, RVALUETYPE_9, RYEAR, GL_SIRID, BATCH_ID, GRANT_NBR, SPONSOR_PROGRAM, 
				SPONSOR_CLASS, FUND, FUND_CENTER, UPDATE_TIMESTAMP, UPDATE_USER
			FROM AWARD_REVENUE_TRANSACTIONS T1 
			WHERE T1.ACCOUNT_NUMBER = AV_ACCOUNT_NUMBER
			AND T1.ACTUAL_OR_COMMITTED_FLAG = 'A'
            AND T1.AMOUNT_IN_FMA_CURRENCY <> 0.00
			AND ((T1.INTERNAL_ORDER_CODE IS NOT NULL
			AND UPPER(TRIM(T1.INTERNAL_ORDER_CODE)) NOT IN (SELECT IFNULL(UPPER(TRIM(T3.INTERNAL_ORDER_CODE)),'0')
															FROM AWARD_BUDGET_HEADER T2
															INNER JOIN AWARD_BUDGET_DETAIL T3 ON T2.BUDGET_HEADER_ID = T3.BUDGET_HEADER_ID
															WHERE T2.AWARD_ID = LI_AWARD_ID
															AND T2.VERSION_NUMBER IN(SELECT MAX(T4.VERSION_NUMBER) FROM AWARD_BUDGET_HEADER T4
																					  WHERE T2.AWARD_ID = T4.AWARD_ID
																					  )
															UNION 
															SELECT IFNULL(UPPER(TRIM(T7.IO_CODE)),'0')
															FROM AWARD_BUDGET_HEADER T4
															INNER JOIN AWARD_BUDGET_PERIOD T8 ON T4.BUDGET_HEADER_ID = T8.BUDGET_HEADER_ID
															INNER JOIN AWARD_BUDGET_DETAIL T5 ON T8.BUDGET_PERIOD_ID = T5.BUDGET_PERIOD_ID
															LEFT OUTER JOIN AWARD_BUDGET_PERSON_DETAIL T7 ON T5.BUDGET_DETAILS_ID = T7.BUDGET_DETAILS_ID
															WHERE T4.AWARD_ID = LI_AWARD_ID
															AND T4.VERSION_NUMBER IN(SELECT MAX(T6.VERSION_NUMBER) FROM AWARD_BUDGET_HEADER T6
																						 WHERE T4.AWARD_ID = T6.AWARD_ID
																						)
															AND T7.IO_CODE IS NOT NULL
															)
				)
				OR T1.INTERNAL_ORDER_CODE IS NULL
				);
			END IF;
	ELSEIF AV_ACTUAL_COMMITED_FLAG = 'C' THEN
		IF AV_INTERNAL_ORDER_CODE IS NOT NULL THEN
		SELECT 
		AWARD_NUMBER, ACCOUNT_NUMBER, INTERNAL_ORDER_CODE, REMARKS, AMOUNT_IN_FMA_CURRENCY,
				ACTUAL_OR_COMMITTED_FLAG, FM_POSTING_DATE, FI_POSTING_DATE, BP_CODE, BP_NAME, 
				DOCUMENT_DATE, FI_GL_ACCOUNT, FI_GL_DESCRIPTION, TRANSACTION_REFERENCE_NUMBER,
				REFERENCE_DOCUMENT_NUMBER, REFERENCE_POSTING_LINE, GUID, GMIA_DOCNR, DOCLN, 
				RBUKRS, RVALUETYPE_9, RYEAR, GL_SIRID, BATCH_ID, GRANT_NBR, SPONSOR_PROGRAM, 
				SPONSOR_CLASS, FUND, FUND_CENTER, UPDATE_TIMESTAMP, UPDATE_USER
		FROM AWARD_REVENUE_TRANSACTIONS T1
		WHERE T1.ACCOUNT_NUMBER = AV_ACCOUNT_NUMBER
		AND T1.INTERNAL_ORDER_CODE = AV_INTERNAL_ORDER_CODE
		AND T1.ACTUAL_OR_COMMITTED_FLAG = 'C'
        AND T1.AMOUNT_IN_FMA_CURRENCY <> 0.00;
	ELSE
		SELECT 
				AWARD_NUMBER, ACCOUNT_NUMBER, INTERNAL_ORDER_CODE, REMARKS, AMOUNT_IN_FMA_CURRENCY,
				ACTUAL_OR_COMMITTED_FLAG, FM_POSTING_DATE, FI_POSTING_DATE, BP_CODE, BP_NAME, 
				DOCUMENT_DATE, FI_GL_ACCOUNT, FI_GL_DESCRIPTION, TRANSACTION_REFERENCE_NUMBER,
				REFERENCE_DOCUMENT_NUMBER, REFERENCE_POSTING_LINE, GUID, GMIA_DOCNR, DOCLN, 
				RBUKRS, RVALUETYPE_9, RYEAR, GL_SIRID, BATCH_ID, GRANT_NBR, SPONSOR_PROGRAM, 
				SPONSOR_CLASS, FUND, FUND_CENTER, UPDATE_TIMESTAMP, UPDATE_USER
			FROM AWARD_REVENUE_TRANSACTIONS T1 
			WHERE T1.ACCOUNT_NUMBER = AV_ACCOUNT_NUMBER
			AND T1.ACTUAL_OR_COMMITTED_FLAG = 'C'
            AND T1.AMOUNT_IN_FMA_CURRENCY <> 0.00
			AND ((T1.INTERNAL_ORDER_CODE IS NOT NULL
			AND UPPER(TRIM(T1.INTERNAL_ORDER_CODE)) NOT IN (SELECT IFNULL(UPPER(TRIM(T3.INTERNAL_ORDER_CODE)),'0')
															FROM AWARD_BUDGET_HEADER T2
															INNER JOIN AWARD_BUDGET_DETAIL T3 ON T2.BUDGET_HEADER_ID = T3.BUDGET_HEADER_ID
															WHERE T2.AWARD_ID = LI_AWARD_ID
															AND T2.VERSION_NUMBER IN(SELECT MAX(T4.VERSION_NUMBER) FROM AWARD_BUDGET_HEADER T4
																					  WHERE T2.AWARD_ID = T4.AWARD_ID
																					  )
															UNION 
															SELECT IFNULL(UPPER(TRIM(T7.IO_CODE)),'0')
															FROM AWARD_BUDGET_HEADER T4
															INNER JOIN AWARD_BUDGET_PERIOD T8 ON T4.BUDGET_HEADER_ID = T8.BUDGET_HEADER_ID
															INNER JOIN AWARD_BUDGET_DETAIL T5 ON T8.BUDGET_PERIOD_ID = T5.BUDGET_PERIOD_ID
															LEFT OUTER JOIN AWARD_BUDGET_PERSON_DETAIL T7 ON T5.BUDGET_DETAILS_ID = T7.BUDGET_DETAILS_ID
															WHERE T4.AWARD_ID = LI_AWARD_ID
															AND T4.VERSION_NUMBER IN(SELECT MAX(T6.VERSION_NUMBER) FROM AWARD_BUDGET_HEADER T6
																						 WHERE T4.AWARD_ID = T6.AWARD_ID
																						)
															AND T7.IO_CODE IS NOT NULL
															)
				)
				OR T1.INTERNAL_ORDER_CODE IS NULL
				);
	END IF;
	END IF;
END
//
