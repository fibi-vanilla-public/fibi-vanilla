DELIMITER //
CREATE  PROCEDURE `AWARD_EXP_TRACKR_PREVALIDATION`()
BEGIN
DECLARE LS_AWARD_NUMBER 			VARCHAR(12);
DECLARE LS_ACCOUNT_NUMBER			VARCHAR(100);
DECLARE LS_INTERNAL_ORDER_CODE 		VARCHAR(50);
DECLARE LS_REMARKS 					VARCHAR(1000);
DECLARE LS_AMOUNT_IN_FMA_CURRENCY 	DECIMAL(12,2);
DECLARE LS_ACTUAL_OR_COMMITTED_FLAG VARCHAR(1);
DECLARE LS_PO_PR_FLAG				VARCHAR(3);
DECLARE LS_BANK_CLEARING_DATE 		VARCHAR(20);
DECLARE LS_FM_POSTING_DATE 			VARCHAR(20);
DECLARE LS_FI_POSTING_DATE 			VARCHAR(20);
DECLARE LS_VENDOR_CODE 				VARCHAR(50);
DECLARE LS_VENDOR_NAME 				VARCHAR(300);
DECLARE LS_DOCUMENT_DATE 			VARCHAR(20);
DECLARE LS_FI_GL_ACCOUNT 			VARCHAR(50);
DECLARE LS_FI_GL_DESCRIPTION 		VARCHAR(200);
DECLARE LS_GR_DATE 					VARCHAR(20);
DECLARE LS_INVOICE_DATE 			VARCHAR(20);
DECLARE LS_PR_DATE 					VARCHAR(20);
DECLARE LS_REQUESTER_NAME 			VARCHAR(90);
DECLARE LI_SEQ_ERROR_LOG_ID 		INT;
DECLARE LI_AWARD_EXPENSE_TRANS_ID   INT;
DECLARE LS_ERROR_MSG 				VARCHAR(4000);
DECLARE LI_FLAG 					INT;
DECLARE LS_CREATE_STATUS 			VARCHAR(1);
DECLARE LS_ERROR_STATUS				VARCHAR(1);
DECLARE LS_MESSAGE 					VARCHAR(4000);
DECLARE LI_EXPENSE_TRACKER_ID       INT;
DECLARE LS_YEAR 					VARCHAR(10);
DECLARE LS_MONTH					VARCHAR(10);
DECLARE LS_DAY						VARCHAR(10);
DECLARE LS_DATE 					VARCHAR(20);
DECLARE LS_DATE_VALUE				VARCHAR(10);
DECLARE LS_PO_DATE					VARCHAR(20);
DECLARE LI_FILE_ID					INT;
DECLARE LS_DOCUMENT_NUMBER			VARCHAR(100);
BEGIN
		DECLARE DONE1 INT DEFAULT FALSE;
		DECLARE CUR_SEL_DATA CURSOR FOR 
		SELECT 
			EXPENSE_TRACKER_ID,
			CREATE_STATUS,
			ERROR_STATUS,
			ACCOUNT_NUMBER,
			INTERNAL_ORDER_CODE,
			REMARKS,
			AMOUNT_IN_FMA_CURRENCY,
			ACTUAL_OR_COMMITTED_FLAG,
			PO_PR_FLAG,
			BANK_CLEARING_DATE,
			FM_POSTING_DATE,
			FI_POSTING_DATE,
			VENDOR_CODE,
			VENDOR_NAME,
			DOCUMENT_DATE,
			FI_GL_ACCOUNT,
			FI_GL_DESCRIPTION,
			GR_DATE,
			INVOICE_DATE,
			PR_DATE,
			REQUESTER_NAME,
			PO_DATE,
			FILE_ID,
			DOCUMENT_NUMBER
	FROM AWARD_EXPENSE_TRANSACTIONS_RT;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE1 = TRUE;
	OPEN CUR_SEL_DATA;
	INSERT_LOOP: LOOP 
		FETCH CUR_SEL_DATA INTO 
		LI_EXPENSE_TRACKER_ID,
		LS_CREATE_STATUS,
		LS_ERROR_STATUS,
		LS_ACCOUNT_NUMBER,
		LS_INTERNAL_ORDER_CODE,
		LS_REMARKS,
		LS_AMOUNT_IN_FMA_CURRENCY,
		LS_ACTUAL_OR_COMMITTED_FLAG,
		LS_PO_PR_FLAG,
		LS_BANK_CLEARING_DATE,
		LS_FM_POSTING_DATE,
		LS_FI_POSTING_DATE,
		LS_VENDOR_CODE,
		LS_VENDOR_NAME,
		LS_DOCUMENT_DATE,
		LS_FI_GL_ACCOUNT,
		LS_FI_GL_DESCRIPTION,
		LS_GR_DATE,
		LS_INVOICE_DATE,
		LS_PR_DATE,
		LS_REQUESTER_NAME,
		LS_PO_DATE,
		LI_FILE_ID,
		LS_DOCUMENT_NUMBER;
	IF DONE1 THEN
		LEAVE INSERT_LOOP;
	END IF;
	SELECT COUNT(1) INTO LI_FLAG 
	FROM DUAL
	WHERE LENGTH(TRIM(LS_ACCOUNT_NUMBER)) > 100;
	IF LI_FLAG > 0 THEN 
		SET LS_MESSAGE = 'ACCOUNT NUMBER LENGTH EXCEEDED';
	ELSE
		SELECT COUNT(1) INTO LI_FLAG
		FROM AWARD
		WHERE ACCOUNT_NUMBER = LS_ACCOUNT_NUMBER;
		IF LI_FLAG = 0 THEN 
			SET LS_MESSAGE = 'ACCOUNT NUMBER DOES NOT EXIST';
		END IF;
	END IF;
	SELECT COUNT(1) INTO LI_FLAG 
	FROM DUAL
	WHERE LENGTH(TRIM(LS_INTERNAL_ORDER_CODE)) > 50;
	IF LI_FLAG > 0 THEN 
		SET LS_MESSAGE = CONCAT(IFNULL(LS_MESSAGE,''),'|L2_WBS_NUMBER LENGTH EXCEEDED');
	ELSE
		SELECT COUNT(1) INTO LI_FLAG FROM AWARD T1
		INNER JOIN AWARD_BUDGET_HEADER T2 ON T1.AWARD_ID = T2.AWARD_ID
										  AND T2.IS_LATEST_VERSION = 'Y'
		INNER JOIN AWARD_BUDGET_DETAIL T3 ON T2.BUDGET_HEADER_ID = T3.BUDGET_HEADER_ID
		WHERE T1.ACCOUNT_NUMBER = LS_ACCOUNT_NUMBER
		AND T3.INTERNAL_ORDER_CODE = LS_INTERNAL_ORDER_CODE;
		IF LI_FLAG = 0 THEN
			SET LS_MESSAGE = CONCAT(IFNULL(LS_MESSAGE,''),'|L2_WBS_NUMBER DOES NOT EXIST');
		END IF;
	END IF;
	SELECT COUNT(1) INTO LI_FLAG
	FROM DUAL 
	WHERE LENGTH(LS_REMARKS)>1000;
	IF LI_FLAG > 0 THEN 
		SET LS_MESSAGE = CONCAT(IFNULL(LS_MESSAGE,''),'|REMARKS LENGTH EXCEEDED');
	END IF;
	IF LS_ACTUAL_OR_COMMITTED_FLAG IS NULL OR LS_ACTUAL_OR_COMMITTED_FLAG = '' THEN
		SET LS_MESSAGE = CONCAT(IFNULL(LS_MESSAGE,''),'|ACTUAL_OR_COMMITTED_FLAG IS EMPTY');
	END IF;
	SELECT COUNT(1) INTO LI_FLAG
	FROM DUAL 
	WHERE LENGTH(LS_VENDOR_CODE)>50;
	IF LI_FLAG > 0 THEN 
		SET LS_MESSAGE = CONCAT(IFNULL(LS_MESSAGE,''),'|VENDOR_CODE LENGTH EXCEEDED');
	END IF;
	SELECT COUNT(1) INTO LI_FLAG
	FROM DUAL 
	WHERE LENGTH(LS_VENDOR_NAME)>300;
	IF LI_FLAG > 0 THEN 
		SET LS_MESSAGE = CONCAT(IFNULL(LS_MESSAGE,''),'|VENDOR_NAME LENGTH EXCEEDED');
	END IF;
	SELECT COUNT(1) INTO LI_FLAG
	FROM DUAL 
	WHERE LENGTH(LS_FI_GL_ACCOUNT)>50;
	IF LI_FLAG > 0 THEN 
		SET LS_MESSAGE = CONCAT(IFNULL(LS_MESSAGE,''),'|FI_GL_ACCOUNT LENGTH EXCEEDED');
	END IF;
	SELECT COUNT(1) INTO LI_FLAG
	FROM DUAL 
	WHERE LENGTH(LS_FI_GL_DESCRIPTION)>200;
	IF LI_FLAG > 0 THEN 
		SET LS_MESSAGE = CONCAT(IFNULL(LS_MESSAGE,''),'|FI_GL_DESCRIPTION LENGTH EXCEEDED');
	END IF;
	IF LS_MESSAGE IS NOT NULL OR LS_MESSAGE <> '' THEN
		SELECT IFNULL(MAX(ERROR_LOG_ID),0)+1 INTO LI_SEQ_ERROR_LOG_ID FROM AWARD_EXPENSE_PREVAL_ERROR_LOG;
		INSERT INTO AWARD_EXPENSE_PREVAL_ERROR_LOG(ERROR_LOG_ID, ACCOUNT_NUMBER, INTERNAL_ORDER_CODE, ERROR_MESSAGE, FILE_ID, UPDATE_TIMESTAMP, UPDATE_USER)
		VALUES(LI_SEQ_ERROR_LOG_ID,LS_ACCOUNT_NUMBER,LS_INTERNAL_ORDER_CODE,TRIM(BOTH '|' FROM LS_MESSAGE),LI_FILE_ID,NOW(),'quickstart');
	END IF;
	SET LS_ACCOUNT_NUMBER = NULL;
	SET LS_INTERNAL_ORDER_CODE = NULL;
	SET LS_REMARKS = NULL;
	SET LS_ACTUAL_OR_COMMITTED_FLAG = NULL;
	SET LS_PO_PR_FLAG = NULL;
	SET LS_BANK_CLEARING_DATE = NULL;
	SET LS_FM_POSTING_DATE = NULL;
	SET LS_FI_POSTING_DATE = NULL;
	SET LS_VENDOR_CODE = NULL;
	SET LS_VENDOR_NAME = NULL;
	SET LS_DOCUMENT_DATE = NULL;
	SET LS_FI_GL_ACCOUNT = NULL;
	SET LS_FI_GL_DESCRIPTION = NULL;
	SET LS_GR_DATE = NULL;
	SET LS_INVOICE_DATE = NULL;
	SET LS_PR_DATE = NULL;
	SET LS_REQUESTER_NAME = NULL;
	SET LS_MESSAGE = NULL;
	SET LS_YEAR = NULL;
	SET LS_MONTH = NULL;
	SET	LS_DAY = NULL;
	SET LS_DATE = NULL;
	SET LS_DATE_VALUE = NULL;
	SET LS_DOCUMENT_NUMBER = NULL;
	END LOOP;
	CLOSE CUR_SEL_DATA;
	END;
END
//
