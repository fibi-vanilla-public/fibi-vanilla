DELIMITER //
CREATE  PROCEDURE `AWARD_MASTER_DATASET_SYNC`()
BEGIN
DECLARE LS_ACCOUNT_NUMBER VARCHAR(100);
DECLARE LS_AWARD_NUMBER VARCHAR(12);
DECLARE LI_AWARD_ID INT;
DECLARE LI_SEQUENCE_NUMBER INT;
DECLARE LS_TITLE VARCHAR(1000);
DECLARE LS_AWARD_STATUS VARCHAR(200);
DECLARE LS_ACCOUNT_TYPE VARCHAR(200);
DECLARE LS_AWARD_TYPE VARCHAR(200);
DECLARE LS_ACTIVITY_TYPE VARCHAR(200);
DECLARE LS_LEAD_UNIT_NUMBER VARCHAR(8);
DECLARE LS_LEAD_UNIT_NAME VARCHAR(200);
DECLARE LS_GRANT_CALL_TITLE VARCHAR(1000);
DECLARE LS_SPONSOR_CODE VARCHAR(6);
DECLARE LS_SPONSOR_NAME VARCHAR(200);
DECLARE LS_PRIME_SPONSOR_CODE VARCHAR(6);
DECLARE LS_PRIME_SPONSOR_NAME VARCHAR(200);
DECLARE LD_AWARD_EXECUTION_DATE DATE;
DECLARE LD_AWARD_EFFECTIVE_DATE DATE;
DECLARE LD_FINAL_EXPIRATION_DATE DATE;
DECLARE LS_AWARD_SEQUENCE_STATUS VARCHAR(10);
DECLARE LS_KEY_PERSON_NAME VARCHAR(90);
DECLARE LS_KEY_PERSON_ID VARCHAR(40);
DECLARE LI_PERSON_ROLE_ID INT;
DECLARE LS_PERSON_ROLE VARCHAR(50);
DECLARE LB_ANTICIPATED_TOTAL_AMOUNT DECIMAL(12,2);
DECLARE LB_AMOUNT_OBLIGATED_TO_DATE DECIMAL(12,2);
DECLARE LD_CURRENT_FUND_EFFECTIVE_DATE DATE;
DECLARE LD_OBLIGATION_EXPIRATION_DATE DATE;
DECLARE LD_BEGIN_DATE DATE;
DECLARE LS_EMPLOYEE_FLAG VARCHAR(1);
DECLARE LS_PI_PERSON_ID VARCHAR(40);
DECLARE LS_PI_NAME VARCHAR(90);
DECLARE LS_PI_HOME_UNIT_NUMBER VARCHAR(8);
DECLARE LS_PI_HOME_UNIT VARCHAR(100);
DECLARE LS_WORKFLOW_AWARD_STATUS_CODE VARCHAR(3);
DECLARE LS_AWARD_WORKFLOW_STATUS_DESC VARCHAR(200);
DECLARE LS_SPONSOR_AWARD_NUMBER VARCHAR(70);
DECLARE LS_KEY_PERSON_ORGANIZATION VARCHAR(200);
DECLARE LS_KEY_PERSON_DEPARTMENT_NUMBER VARCHAR(8);
DECLARE LS_KEY_PERSON_DEPARTMENT VARCHAR(100);
DECLARE LS_KEY_PERSON_COUNTRY VARCHAR(100);
DECLARE LS_KEY_PERSON_EMAIL VARCHAR(60);
DECLARE LB_TOTAL_COST_SHARE DECIMAL(12,2);
DECLARE LB_TOTAL_PROJECT_COST DECIMAL(12,2);
DECLARE LB_TOTAL_COST_IN_CURRENCY DECIMAL(12,2);
DECLARE LB_TOTAL_COST DECIMAL(12,2);
DECLARE LB_TOTAL_DIRECT_COST DECIMAL(12,2);
DECLARE LB_TOTAL_INDIRECT_COST DECIMAL(12,2);
DECLARE LS_BASIS_OF_PAYMENT_DESC VARCHAR(200);
DECLARE LS_METHOD_OF_PAYMENT_DESC VARCHAR(200);
DECLARE LS_PAYMENT_INVOICE_FREQ_DESC VARCHAR(200);
DECLARE LS_RATE_CLASS_CODE VARCHAR(200);
DECLARE LD_AWARD_CREATE_TIMESTAMP DATETIME;
DECLARE LS_CREATE_USER VARCHAR(60);
DECLARE LS_STEM_NONSTEM LONGTEXT;
DECLARE LS_RIE_DOMAIN LONGTEXT;
DECLARE LS_INPUT_GST_CATEGORY LONGTEXT;
DECLARE LS_OUTPUT_GST_CATEGORY LONGTEXT;
DECLARE LS_GRANT_CODE LONGTEXT;
DECLARE LS_SUB_LEAD_UNIT LONGTEXT;
DECLARE LS_PROFIT_CENTER LONGTEXT;
DECLARE LS_FUND_CENTER LONGTEXT;
DECLARE LS_COST_CENTER LONGTEXT;
DECLARE LS_MULTIPLIER LONGTEXT;
DECLARE LB_CUMULATIVE_REVENUE DECIMAL(12,2);
DECLARE LB_CUMULATIVE_ACTUAL_EXPENSE DECIMAL(12,2);
DECLARE LB_CUMULATIVE_COMMITTED_EXPENSE DECIMAL(12,2);
DECLARE LB_BALANCE_FUND DECIMAL(12,2);
DECLARE LB_BALANCE_LESS_COMMITTED_AMOUNT DECIMAL(12,2);
DECLARE LB_ORIGINAL_APPROVED_BUDGET DECIMAL(12,2);
DECLARE LB_LATEST_APPROVED_BUDGET DECIMAL(12,2);
DECLARE LS_UTILIZATION_RATE VARCHAR(20);
DECLARE LS_DURATION VARCHAR(50);
DECLARE LB_PERCENTAGE_OF_EFFORT DECIMAL(5,2);
DECLARE LS_CUMULATIVE_VIREMENT  DECIMAL(5,2);
DECLARE LS_DISPLAY_AT_ACAD_PROFILE VARCHAR(50);
DECLARE LB_GRANT_PER_MONTH DECIMAL(12,2);
DECLARE LS_F_AND_A_RATE_TYPE VARCHAR(200);
DECLARE LS_PROJECT_AGE VARCHAR(20);
DECLARE LS_FUNDING_SCHEME VARCHAR(1000);
DECLARE LS_SPONSOR_TYPE_CODE VARCHAR(3);
DECLARE LS_SPONSOR_TYPE VARCHAR(200);
DECLARE LD_UPDATE_TIMESTAMP DATETIME;
DECLARE LS_ABBREVIATION VARCHAR(255);
DECLARE LS_GRANT_FUNDING_AGENCY VARCHAR(200);
DECLARE LS_GRANT_FUNDING_AGENCY_TYPE VARCHAR(100);
DECLARE LS_SPONSOR_COUNTRY VARCHAR(100);
DECLARE LS_PRIME_SPONSOR_COUNTRY VARCHAR(100);
DECLARE LS_FUND_CODE VARCHAR(20);
DECLARE LS_STATUS_CODE VARCHAR(3);
DECLARE LB_ACTUAL_EXPENSE_WO_IRC DECIMAL(12,2);
DECLARE LB_COMMITTED_EXPENSE_WO_IRC DECIMAL(12,2);
DECLARE LS_UTILIZATION_RATE_WO_IRC DECIMAL(12,2);
DECLARE LS_LEVEL_2_SUP_ORG LONGTEXT;
DECLARE LS_KEY_PERSON_GENDER VARCHAR(30);
DECLARE LS_FEED_STATUS VARCHAR(200);
DECLARE LS_NTU_PRIORITY VARCHAR(2000);
DECLARE LS_FUNDER_APPROVAL_DATE DATE;
DECLARE LS_DYN_SQL LONGTEXT;
DECLARE AWARD_NUMBERS LONGTEXT;
DECLARE LS_LAST_SYNC_TIMESTAMP DATETIME;
DECLARE LS_CLAIM_PREPARER VARCHAR(255);
DECLARE LS_AREA_RESEARCH VARCHAR(2000);
SELECT LAST_SYNC_TIMESTAMP INTO LS_LAST_SYNC_TIMESTAMP FROM REPORT_LAST_SYNC_TIME;
SET SESSION group_concat_max_len = 1000000;
SELECT GROUP_CONCAT('''', A3.AWARD_NUMBER ,'''') INTO AWARD_NUMBERS FROM AWARD A3
WHERE A3.DOCUMENT_UPDATE_TIMESTAMP>=DATE_SUB(LS_LAST_SYNC_TIMESTAMP,INTERVAL 1 HOUR)
AND ((A3.AWARD_SEQUENCE_STATUS IN('PENDING','ARCHIVE','CANCELLED') AND A3.SEQUENCE_NUMBER = 1)
OR A3.AWARD_SEQUENCE_STATUS IN('ACTIVE'));
IF AWARD_NUMBERS IS NOT NULL THEN
        SET LS_DYN_SQL  = CONCAT('DELETE FROM AWARD_MASTER_DATASET_RT T2
        WHERE T2.AWARD_NUMBER IN(', AWARD_NUMBERS, ')');
        SET @QUERY_STATEMENT = LS_DYN_SQL;
        PREPARE EXECUTABLE_STATEMENT FROM @QUERY_STATEMENT;
        EXECUTE EXECUTABLE_STATEMENT;
END IF;
BEGIN
DECLARE DONE INT DEFAULT FALSE;
DECLARE CUR_AWARD_MASTER_DATA_SET CURSOR FOR
        SELECT DISTINCT
        T1.ACCOUNT_NUMBER,
        T1.AWARD_NUMBER,
        T1.AWARD_ID,
        T1.SEQUENCE_NUMBER,
        T1.TITLE,
        T2.DESCRIPTION AS AWARD_STATUS,
        T3.DESCRIPTION AS ACCOUNT_TYPE,
        T4.DESCRIPTION AS AWARD_TYPE,
        T5.DESCRIPTION AS ACTIVITY_TYPE,
        T1.LEAD_UNIT_NUMBER AS LEAD_UNIT_NUMBER,
        T11.UNIT_NAME AS LEAD_UNIT_NAME,
        T6.NAME AS GRANT_CALL_TITLE,
        T1.SPONSOR_CODE AS SPONSOR_CODE,
        T7.SPONSOR_NAME AS SPONSOR_NAME,
        T1.PRIME_SPONSOR_CODE AS PRIME_SPONSOR_CODE,
        T8.SPONSOR_NAME AS PRIME_SPONSOR_NAME,
        DATE(T1.AWARD_EXECUTION_DATE) AS AWARD_EXECUTION_DATE,
        DATE(T1.AWARD_EFFECTIVE_DATE) AS AWARD_EFFECTIVE_DATE,
        DATE(T1.FINAL_EXPIRATION_DATE) AS FINAL_EXPIRATION_DATE,
        T1.AWARD_SEQUENCE_STATUS AS AWARD_SEQUENCE_STATUS,
        IFNULL(T22.FULL_NAME,T20.FULL_NAME) AS KEY_PERSON_NAME,
        IFNULL(T9.PERSON_ID, T9.ROLODEX_ID) AS KEY_PERSON_ID,
        T9.PERSON_ROLE_ID,
        T12.DESCRIPTION AS PERSON_ROLE,
        IFNULL(T10.ANTICIPATED_TOTAL_AMOUNT,0.00) AS ANTICIPATED_TOTAL_AMOUNT,
        IFNULL(T10.AMOUNT_OBLIGATED_TO_DATE,0.00) AS AMOUNT_OBLIGATED_TO_DATE,
        DATE(T10.CURRENT_FUND_EFFECTIVE_DATE) AS CURRENT_FUND_EFFECTIVE_DATE,
        DATE(T10.OBLIGATION_EXPIRATION_DATE) AS OBLIGATION_EXPIRATION_DATE,
        DATE(T1.BEGIN_DATE) AS BEGIN_DATE,
        (CASE WHEN (T9.PERSON_ID IS NOT NULL) THEN 'Y'
        ELSE 'N'
        END) AS EMPLOYEE_FLAG,
        (SELECT IFNULL(PERSON_ID,ROLODEX_ID) FROM
        AWARD_PERSONS P1 WHERE P1.AWARD_ID = T1.AWARD_ID AND P1.PI_FLAG = 'Y') AS PI_PERSON_ID,
        (SELECT IFNULL(P2.FULL_NAME,R1.FULL_NAME) FROM
        AWARD_PERSONS P1
        LEFT JOIN PERSON P2 ON P1.PERSON_ID = P2.PERSON_ID
        LEFT JOIN ROLODEX R1 ON P1.ROLODEX_ID = R1.ROLODEX_ID
        WHERE P1.AWARD_ID = T1.AWARD_ID AND P1.PI_FLAG = 'Y'
        ) AS PI_NAME,
        (SELECT P2.HOME_UNIT
        FROM AWARD_PERSONS P1
        INNER JOIN PERSON P2 ON P1.PERSON_ID = P2.PERSON_ID
        WHERE P1.AWARD_ID = T1.AWARD_ID AND P1.PI_FLAG = 'Y' AND P1.PERSON_ID IS NOT NULL
        ) AS PI_HOME_UNIT_NUMBER,
        (SELECT U1.UNIT_NAME
        FROM AWARD_PERSONS P1
        INNER JOIN PERSON P2 ON P1.PERSON_ID = P2.PERSON_ID
        INNER JOIN UNIT U1 ON P2.HOME_UNIT = U1.UNIT_NUMBER
        WHERE P1.AWARD_ID = T1.AWARD_ID AND P1.PI_FLAG = 'Y' AND P1.PERSON_ID IS NOT NULL
        ) AS PI_HOME_UNIT,
        T1.WORKFLOW_AWARD_STATUS_CODE,
        T13.DESCRIPTION AS AWARD_WORKFLOW_STATUS_DESC,
        T1.SPONSOR_AWARD_NUMBER,
        T21.ORGANIZATION_NAME AS KEY_PERSON_ORGANIZATION,
        T23.UNIT_NUMBER AS KEY_PERSON_DEPARTMENT_NUMBER,
        T23.UNIT_NAME AS KEY_PERSON_DEPARTMENT,
        IFNULL(T40.COUNTRY_NAME,T30.COUNTRY_NAME) AS KEY_PERSON_COUNTRY,
        IFNULL(T22.EMAIL_ADDRESS,T20.EMAIL_ADDRESS) AS KEY_PERSON_EMAIL,
        IFNULL(T25.COST_SHARE_AMT,0.00) AS TOTAL_COST_SHARE,
        IFNULL(T10.ANTICIPATED_TOTAL_AMOUNT,0)+IFNULL(T25.COST_SHARE_AMT,0) AS TOTAL_PROJECT_COST,
        IFNULL(T10.TOTAL_COST_IN_CURRENCY,0.00) AS TOTAL_COST_IN_CURRENCY,
        IFNULL(T14.TOTAL_COST,0.00) AS TOTAL_COST,
        IFNULL(T14.TOTAL_DIRECT_COST,0.00) AS TOTAL_DIRECT_COST,
        IFNULL(T14.TOTAL_INDIRECT_COST,0.00) AS TOTAL_INDIRECT_COST,
        T16.DESCRIPTION AS BASIS_OF_PAYMENT_DESC,
        T17.DESCRIPTION AS METHOD_OF_PAYMENT_DESC,
        T18.DESCRIPTION AS PAYMENT_INVOICE_FREQ_DESC,
        T19.DESCRIPTION AS RATE_CLASS_CODE,
        T1.CREATE_TIMESTAMP AS AWARD_CREATE_TIMESTAMP,
        T1.CREATE_USER,
        T15.STEM_NONSTEM,
        T15.RIE_DOMAIN,
        T15.INPUT_GST_CATEGORY,
        T15.OUTPUT_GST_CATEGORY,
        T15.GRANT_CODE,
        T15.SUB_LEAD_UNIT,
        T15.PROFIT_CENTER,
        T15.FUND_CENTER,
        T15.COST_CENTER,
        T15.MULTIPLIER,
        IFNULL(T33.CUMULATIVE_REVENUE,0.00) AS CUMULATIVE_REVENUE,
        IFNULL(T31.CUMULATIVE_ACTUAL_EXPENSE,0.00) AS CUMULATIVE_ACTUAL_EXPENSE,
        IFNULL(T32.CUMULATIVE_COMMITTED_EXPENSE,0.00) AS CUMULATIVE_COMMITTED_EXPENSE,
        IFNULL(T34.LATEST_APPROVED_BUDGET,0)-IFNULL(T31.CUMULATIVE_ACTUAL_EXPENSE,0) AS BALANCE_FUND,
        IFNULL(T34.LATEST_APPROVED_BUDGET,0)-(ifnull(T31.CUMULATIVE_ACTUAL_EXPENSE,0) + ifnull(T32.CUMULATIVE_COMMITTED_EXPENSE,0)) AS BALANCE_LESS_COMMITTED_AMOUNT,
        IFNULL(T35.ORIGINAL_APPROVED_BUDGET,0.00) AS ORIGINAL_APPROVED_BUDGET,
        IFNULL(T34.LATEST_APPROVED_BUDGET,0.00) AS LATEST_APPROVED_BUDGET,
        IFNULL(ROUND((T31.CUMULATIVE_ACTUAL_EXPENSE/T34.LATEST_APPROVED_BUDGET)*100,2),0) AS UTILIZATION_RATE,
        T1.DURATION,
        T9.PERCENTAGE_OF_EFFORT,
        IFNULL(T14.CUMULATIVE_VIREMENT,0.00) AS CUMULATIVE_VIREMENT,
        T15.DISPLAY_AT_ACAD_PROFILE,
        (IFNULL(T10.ANTICIPATED_TOTAL_AMOUNT,0.00)/(TIMESTAMPDIFF(MONTH,T1.BEGIN_DATE, DATE_ADD(T1.FINAL_EXPIRATION_DATE, INTERVAL 1 DAY )))) AS GRANT_PER_MONTH,
        T26.DESCRIPTION AS F_AND_A_RATE_TYPE,
        datediff(NOW(),T1.BEGIN_DATE)+1 AS PROJECT_AGE,
        T28.DESCRIPTION AS FUNDING_SCHEME,
        T7.SPONSOR_TYPE_CODE,
        T29.DESCRIPTION AS SPONSOR_TYPE,
        utc_timestamp(),
        T6.ABBREVIATION,
        T37.SPONSOR_NAME AS GRANT_FUNDING_AGENCY,
        T38.DESCRIPTION AS GRANT_FUNDING_AGENCY_TYPE,
        T41.COUNTRY_NAME AS SPONSOR_COUNTRY,
        T42.COUNTRY_NAME AS PRIME_SPONSOR_COUNTRY,
        FN_GET_AWARD_FUND_CODE(T1.AWARD_NUMBER,T1.ACCOUNT_TYPE_CODE) AS FUND_CODE,
        T1.STATUS_CODE,
        IFNULL(T43.ACTUAL_EXPENSE_WO_IRC, 0.00),
        IFNULL(T44.COMMITTED_EXPENSE_WO_IRC, 0.00),
        IFNULL(ROUND((IFNULL(T43.ACTUAL_EXPENSE_WO_IRC, 0.00)/IFNULL(T14.TOTAL_DIRECT_COST,0.00))*100,2),0) AS UTILIZATION_RATE_WO_IRC,
        T15.LEVEL_2_SUP_ORG,
        IFNULL(T22.GENDER,NULL) AS KEY_PERSON_GENDER,
        T45.FEED_STATUS,
        T46.NTU_PRIORITY,
        T1.FUNDER_APPROVAL_DATE,
        T15.CLAIM_PREPARER,
		T47.AREA_OF_RESEARCH
FROM AWARD T1
LEFT OUTER JOIN AWARD_STATUS T2 ON T1.STATUS_CODE = T2.STATUS_CODE
LEFT OUTER JOIN ACCOUNT_TYPE T3 ON T1.ACCOUNT_TYPE_CODE = T3.ACCOUNT_TYPE_CODE
LEFT OUTER JOIN AWARD_TYPE T4 ON T4.AWARD_TYPE_CODE = T1.AWARD_TYPE_CODE
LEFT OUTER JOIN ACTIVITY_TYPE T5 ON T1.ACTIVITY_TYPE_CODE = T5.ACTIVITY_TYPE_CODE
LEFT OUTER JOIN GRANT_CALL_HEADER T6 ON T1.GRANT_HEADER_ID = T6.GRANT_HEADER_ID
LEFT OUTER JOIN SPONSOR T7 ON T1.SPONSOR_CODE = T7.SPONSOR_CODE
LEFT OUTER JOIN SPONSOR T8 ON T1.PRIME_SPONSOR_CODE = T8.SPONSOR_CODE
LEFT OUTER JOIN AWARD_PERSONS T9 ON T1.AWARD_ID = T9.AWARD_ID
LEFT OUTER JOIN (SELECT S1.AWARD_ID AS AWARD_ID,
        S1.AWARD_NUMBER AS AWARD_NUMBER,
        S1.ANTICIPATED_TOTAL_AMOUNT AS ANTICIPATED_TOTAL_AMOUNT,
        S1.AMOUNT_OBLIGATED_TO_DATE AS AMOUNT_OBLIGATED_TO_DATE,
        S1.FINAL_EXPIRATION_DATE AS FINAL_EXPIRATION_DATE,
        S1.CURRENT_FUND_EFFECTIVE_DATE AS CURRENT_FUND_EFFECTIVE_DATE,
        S1.OBLIGATION_EXPIRATION_DATE AS OBLIGATION_EXPIRATION_DATE,
        S1.TOTAL_COST_IN_CURRENCY
        FROM AWARD_AMOUNT_INFO S1
        WHERE S1.AWARD_AMOUNT_INFO_ID = (SELECT MAX(S3.AWARD_AMOUNT_INFO_ID) FROM AWARD_AMOUNT_INFO S3
LEFT JOIN AWARD_AMOUNT_TRANSACTION S2 ON S3.TRANSACTION_ID = S2.TRANSACTION_ID
        WHERE S2.TRANSACTION_STATUS_CODE IN ('A','P')
        AND S3.AWARD_NUMBER = S1.AWARD_NUMBER )
        ) T10 ON T1.AWARD_NUMBER = T10.AWARD_NUMBER
LEFT OUTER JOIN UNIT T11 ON T1.LEAD_UNIT_NUMBER = T11.UNIT_NUMBER
LEFT OUTER JOIN EPS_PROP_PERSON_ROLE T12 ON T9.PERSON_ROLE_ID = T12.PROP_PERSON_ROLE_ID
LEFT OUTER JOIN AWARD_WORKFLOW_STATUS T13 ON T1.WORKFLOW_AWARD_STATUS_CODE = T13.WORKFLOW_AWARD_STATUS_CODE
LEFT OUTER JOIN (SELECT S3.AWARD_ID,
        S3.TOTAL_COST,
        S3.TOTAL_DIRECT_COST,
        S3.TOTAL_INDIRECT_COST,
        S3.RATE_CLASS_CODE,
        S3.CUMULATIVE_VIREMENT,
        S3.RATE_TYPE_CODE
        FROM AWARD_BUDGET_HEADER S3
        WHERE S3.VERSION_NUMBER IN (SELECT MAX(S4.VERSION_NUMBER) FROM AWARD_BUDGET_HEADER S4
        WHERE S3.AWARD_ID = S4.AWARD_ID)) T14 ON T1.AWARD_ID = T14.AWARD_ID
LEFT OUTER JOIN CUSTOM_DATA_REPORT_RT T15 ON T1.AWARD_ID = T15.MODULE_ITEM_KEY
LEFT OUTER JOIN AWARD_BASIS_OF_PAYMENT T16 ON T1.BASIS_OF_PAYMENT_CODE = T16.BASIS_OF_PAYMENT_CODE
LEFT OUTER JOIN AWARD_METHOD_OF_PAYMENT T17 ON T1.METHOD_OF_PAYMENT_CODE = T17.METHOD_OF_PAYMENT_CODE
LEFT OUTER JOIN FREQUENCY T18 ON T1.PAYMENT_INVOICE_FREQ_CODE = T18.FREQUENCY_CODE
LEFT OUTER JOIN RATE_CLASS T19 ON T14.RATE_CLASS_CODE = T19.RATE_CLASS_CODE
LEFT OUTER JOIN ROLODEX T20 ON T9.ROLODEX_ID = T20.ROLODEX_ID
LEFT OUTER JOIN ORGANIZATION T21 ON T20.ORGANIZATION = T21.ORGANIZATION_ID
LEFT OUTER JOIN PERSON T22 ON T9.PERSON_ID = T22.PERSON_ID
LEFT OUTER JOIN UNIT T23 ON T22.HOME_UNIT = T23.UNIT_NUMBER
LEFT OUTER JOIN (SELECT S5.AWARD_ID,SUM(COMMITMENT_AMOUNT) AS COST_SHARE_AMT FROM AWARD_COST_SHARE S5
        GROUP BY S5.AWARD_ID )T25 ON T1.AWARD_ID = T25.AWARD_ID
LEFT OUTER JOIN RATE_TYPE T26 ON T14.RATE_TYPE_CODE = T26.RATE_TYPE_CODE
        AND T14.RATE_CLASS_CODE = T26.RATE_CLASS_CODE
LEFT OUTER JOIN SPONSOR_FUNDING_SCHEME T27 ON T6.FUNDING_SCHEME_ID = T27.FUNDING_SCHEME_ID
LEFT OUTER JOIN FUNDING_SCHEME T28 ON T27.FUNDING_SCHEME_CODE = T28.FUNDING_SCHEME_CODE
LEFT OUTER JOIN SPONSOR_TYPE T29 ON T7.SPONSOR_TYPE_CODE = T29.SPONSOR_TYPE_CODE
LEFT OUTER JOIN COUNTRY T30 ON T20.COUNTRY_CODE = T30.COUNTRY_CODE
LEFT OUTER JOIN COUNTRY T40 ON T22.COUNTRY_CODE = T40.COUNTRY_CODE
LEFT OUTER JOIN (SELECT A2.AWARD_ID,S1.ACCOUNT_NUMBER,
        SUM(S1.TOTAL_EXPENSE_AMOUNT) AS CUMULATIVE_ACTUAL_EXPENSE
        FROM AWARD_BUDGET_HEADER A2
        INNER JOIN AWARD_BUDGET_DETAIL A3 ON A2.BUDGET_HEADER_ID = A3.BUDGET_HEADER_ID
        INNER JOIN AWARD_EXPENSE_DETAILS S1 ON A3.INTERNAL_ORDER_CODE = S1.INTERNAL_ORDER_CODE
        WHERE A2.VERSION_NUMBER IN (SELECT max(AB1.VERSION_NUMBER) FROM AWARD_BUDGET_HEADER AB1
        WHERE AB1.AWARD_ID = A2.AWARD_ID)
        GROUP BY S1.ACCOUNT_NUMBER,A2.AWARD_ID
        ) T31 ON T1.ACCOUNT_NUMBER = T31.ACCOUNT_NUMBER  AND T31.AWARD_ID = T1.AWARD_ID
LEFT OUTER JOIN (SELECT A2.AWARD_ID,
        S2.ACCOUNT_NUMBER,
        SUM(S2.COMMITTED_AMOUNT) AS CUMULATIVE_COMMITTED_EXPENSE
        FROM AWARD_BUDGET_HEADER A2
        INNER JOIN AWARD_BUDGET_DETAIL A3 ON A2.BUDGET_HEADER_ID = A3.BUDGET_HEADER_ID
        INNER JOIN AWARD_EXPENSE_DETAILS_EXT S2 ON S2.INTERNAL_ORDER_CODE = A3.INTERNAL_ORDER_CODE
        WHERE S2.IS_FROM_SAP = 'Y'
        AND A2.VERSION_NUMBER IN (SELECT max(AB1.VERSION_NUMBER) FROM AWARD_BUDGET_HEADER AB1
        WHERE AB1.AWARD_ID = A2.AWARD_ID)
        GROUP BY S2.ACCOUNT_NUMBER,A2.AWARD_ID
        ) T32 ON T1.ACCOUNT_NUMBER = T32.ACCOUNT_NUMBER  AND  T1.AWARD_ID =  T32.AWARD_ID
LEFT OUTER JOIN (SELECT A2.AWARD_ID,S1.ACCOUNT_NUMBER,
        SUM(S1.TOTAL_EXPENSE_AMOUNT) AS ACTUAL_EXPENSE_WO_IRC
        FROM AWARD_BUDGET_HEADER A2
        INNER JOIN AWARD_BUDGET_DETAIL A3 ON A2.BUDGET_HEADER_ID = A3.BUDGET_HEADER_ID
        INNER JOIN AWARD_EXPENSE_DETAILS S1 ON A3.INTERNAL_ORDER_CODE = S1.INTERNAL_ORDER_CODE
        WHERE A2.VERSION_NUMBER IN (SELECT max(AB1.VERSION_NUMBER) FROM AWARD_BUDGET_HEADER AB1
        WHERE AB1.AWARD_ID = A2.AWARD_ID)
        AND substr(A3.INTERNAL_ORDER_CODE,16,3) <> 'IRC'
        GROUP BY S1.ACCOUNT_NUMBER,A2.AWARD_ID
        ) T43 ON T1.ACCOUNT_NUMBER = T43.ACCOUNT_NUMBER  AND T43.AWARD_ID = T1.AWARD_ID
LEFT OUTER JOIN (SELECT A2.AWARD_ID,
        S2.ACCOUNT_NUMBER,
        SUM(S2.COMMITTED_AMOUNT) AS COMMITTED_EXPENSE_WO_IRC
        FROM AWARD_BUDGET_HEADER A2
        INNER JOIN AWARD_BUDGET_DETAIL A3 ON A2.BUDGET_HEADER_ID = A3.BUDGET_HEADER_ID
        INNER JOIN AWARD_EXPENSE_DETAILS_EXT S2 ON S2.INTERNAL_ORDER_CODE = A3.INTERNAL_ORDER_CODE
        WHERE S2.IS_FROM_SAP = 'Y'
        AND A2.VERSION_NUMBER IN (SELECT max(AB1.VERSION_NUMBER) FROM AWARD_BUDGET_HEADER AB1
        WHERE AB1.AWARD_ID = A2.AWARD_ID)
        AND substr(A3.INTERNAL_ORDER_CODE,16,3) <> 'IRC'
        GROUP BY S2.ACCOUNT_NUMBER,A2.AWARD_ID
        ) T44 ON T1.ACCOUNT_NUMBER = T44.ACCOUNT_NUMBER  AND  T1.AWARD_ID =  T44.AWARD_ID
LEFT OUTER JOIN (SELECT S3.ACCOUNT_NUMBER,
        SUM(S3.TOTAL_REVENUE_AMOUNT) AS CUMULATIVE_REVENUE
        FROM AWARD_REVENUE_DETAILS S3
        GROUP BY S3.ACCOUNT_NUMBER
        )T33 ON T1.ACCOUNT_NUMBER = T33.ACCOUNT_NUMBER
LEFT OUTER JOIN (SELECT S4.AWARD_ID,
        S4.TOTAL_COST AS LATEST_APPROVED_BUDGET
        FROM AWARD_BUDGET_HEADER S4
        WHERE S4.VERSION_NUMBER IN (SELECT MAX(S5.VERSION_NUMBER) FROM AWARD_BUDGET_HEADER S5
        WHERE S4.AWARD_ID = S5.AWARD_ID
        ))T34 ON T1.AWARD_ID = T34.AWARD_ID
LEFT OUTER JOIN (SELECT S6.AWARD_ID,
        S6.TOTAL_COST AS ORIGINAL_APPROVED_BUDGET
        FROM AWARD_BUDGET_HEADER S6
        WHERE S6.VERSION_NUMBER IN (SELECT MIN(S7.VERSION_NUMBER) FROM AWARD_BUDGET_HEADER S7
        WHERE S6.AWARD_ID = S7.AWARD_ID
        ))T35 ON T1.AWARD_ID = T35.AWARD_ID
LEFT OUTER JOIN SPONSOR T37 ON T6.SPONSOR_CODE = T37.SPONSOR_CODE
LEFT OUTER JOIN SPONSOR_TYPE T38 ON T6.SPONSOR_TYPE_CODE = T38.SPONSOR_TYPE_CODE
LEFT OUTER JOIN COUNTRY T41 ON T7.COUNTRY_CODE = T41.COUNTRY_CODE
LEFT OUTER JOIN COUNTRY T42 ON T8.COUNTRY_CODE = T42.COUNTRY_CODE
LEFT OUTER JOIN (SELECT T43.AWARD_ID, T44.DESCRIPTION AS FEED_STATUS
        FROM SAP_AWARD_FEED T43
        INNER JOIN SAP_FEED_STATUS T44 ON T44.FEED_STATUS_CODE = T43.FEED_STATUS
        ) T45 ON T1.AWARD_ID = T45.AWARD_ID
LEFT OUTER JOIN (SELECT GROUP_CONCAT(DISTINCT R2.DESCRIPTION SEPARATOR ', ') AS NTU_PRIORITY, R1.AWARD_ID  FROM AWARD_RESEARCH_AREAS R1
        INNER JOIN RESEARCH_TYPE_AREA R2 ON R2.RESRCH_TYPE_AREA_CODE = R1.RESRCH_TYPE_AREA_CODE AND R1.RESRCH_TYPE_CODE = 2 GROUP BY R1.AWARD_ID)
        T46 ON T1.AWARD_ID = T46.AWARD_ID
LEFT OUTER JOIN (SELECT GROUP_CONCAT(DISTINCT R2.DESCRIPTION SEPARATOR ', ') AS AREA_OF_RESEARCH, R1.AWARD_ID  FROM AWARD_RESEARCH_AREAS R1
        INNER JOIN RESEARCH_TYPE_AREA R2 ON R2.RESRCH_TYPE_AREA_CODE = R1.RESRCH_TYPE_AREA_CODE AND R1.RESRCH_TYPE_CODE = 1 GROUP BY R1.AWARD_ID)
        T47 ON T1.AWARD_ID = T47.AWARD_ID
        WHERE T1.AWARD_ID IN (SELECT
        A3.AWARD_ID
        FROM AWARD A3
        WHERE A3.DOCUMENT_UPDATE_TIMESTAMP>=DATE_SUB(LS_LAST_SYNC_TIMESTAMP,INTERVAL 1 HOUR)
        AND ((A3.AWARD_SEQUENCE_STATUS = 'PENDING' AND A3.SEQUENCE_NUMBER = 1)
        OR (A3.AWARD_SEQUENCE_STATUS = 'ACTIVE')));
DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE = TRUE;
OPEN CUR_AWARD_MASTER_DATA_SET;
INSERT_LOOP: LOOP
FETCH CUR_AWARD_MASTER_DATA_SET INTO
LS_ACCOUNT_NUMBER,
LS_AWARD_NUMBER,
LI_AWARD_ID,
LI_SEQUENCE_NUMBER,
LS_TITLE,
LS_AWARD_STATUS,
LS_ACCOUNT_TYPE,
LS_AWARD_TYPE,
LS_ACTIVITY_TYPE,
LS_LEAD_UNIT_NUMBER,
LS_LEAD_UNIT_NAME,
LS_GRANT_CALL_TITLE,
LS_SPONSOR_CODE,
LS_SPONSOR_NAME,
LS_PRIME_SPONSOR_CODE,
LS_PRIME_SPONSOR_NAME,
LD_AWARD_EXECUTION_DATE,
LD_AWARD_EFFECTIVE_DATE,
LD_FINAL_EXPIRATION_DATE,
LS_AWARD_SEQUENCE_STATUS,
LS_KEY_PERSON_NAME,
LS_KEY_PERSON_ID,
LI_PERSON_ROLE_ID,
LS_PERSON_ROLE,
LB_ANTICIPATED_TOTAL_AMOUNT,
LB_AMOUNT_OBLIGATED_TO_DATE,
LD_CURRENT_FUND_EFFECTIVE_DATE,
LD_OBLIGATION_EXPIRATION_DATE,
LD_BEGIN_DATE,
LS_EMPLOYEE_FLAG,
LS_PI_PERSON_ID,
LS_PI_NAME,
LS_PI_HOME_UNIT_NUMBER,
LS_PI_HOME_UNIT,
LS_WORKFLOW_AWARD_STATUS_CODE,
LS_AWARD_WORKFLOW_STATUS_DESC,
LS_SPONSOR_AWARD_NUMBER,
LS_KEY_PERSON_ORGANIZATION,
LS_KEY_PERSON_DEPARTMENT_NUMBER,
LS_KEY_PERSON_DEPARTMENT,
LS_KEY_PERSON_COUNTRY,
LS_KEY_PERSON_EMAIL,
LB_TOTAL_COST_SHARE,
LB_TOTAL_PROJECT_COST,
LB_TOTAL_COST_IN_CURRENCY,
LB_TOTAL_COST,
LB_TOTAL_DIRECT_COST,
LB_TOTAL_INDIRECT_COST,
LS_BASIS_OF_PAYMENT_DESC,
LS_METHOD_OF_PAYMENT_DESC,
LS_PAYMENT_INVOICE_FREQ_DESC,
LS_RATE_CLASS_CODE,
LD_AWARD_CREATE_TIMESTAMP,
LS_CREATE_USER,
LS_STEM_NONSTEM,
LS_RIE_DOMAIN,
LS_INPUT_GST_CATEGORY,
LS_OUTPUT_GST_CATEGORY,
LS_GRANT_CODE,
LS_SUB_LEAD_UNIT,
LS_PROFIT_CENTER,
LS_FUND_CENTER,
LS_COST_CENTER,
LS_MULTIPLIER,
LB_CUMULATIVE_REVENUE,
LB_CUMULATIVE_ACTUAL_EXPENSE,
LB_CUMULATIVE_COMMITTED_EXPENSE,
LB_BALANCE_FUND,
LB_BALANCE_LESS_COMMITTED_AMOUNT,
LB_ORIGINAL_APPROVED_BUDGET,
LB_LATEST_APPROVED_BUDGET,
LS_UTILIZATION_RATE,
LS_DURATION,
LB_PERCENTAGE_OF_EFFORT,
LS_CUMULATIVE_VIREMENT,
LS_DISPLAY_AT_ACAD_PROFILE,
LB_GRANT_PER_MONTH,
LS_F_AND_A_RATE_TYPE,
LS_PROJECT_AGE,
LS_FUNDING_SCHEME,
LS_SPONSOR_TYPE_CODE,
LS_SPONSOR_TYPE,
LD_UPDATE_TIMESTAMP,
LS_ABBREVIATION,
LS_GRANT_FUNDING_AGENCY,
LS_GRANT_FUNDING_AGENCY_TYPE,
LS_SPONSOR_COUNTRY,
LS_PRIME_SPONSOR_COUNTRY,
LS_FUND_CODE,
LS_STATUS_CODE,
LB_ACTUAL_EXPENSE_WO_IRC,
LB_COMMITTED_EXPENSE_WO_IRC,
LS_UTILIZATION_RATE_WO_IRC,
LS_LEVEL_2_SUP_ORG,
LS_KEY_PERSON_GENDER,
LS_FEED_STATUS,
LS_NTU_PRIORITY,
LS_FUNDER_APPROVAL_DATE,
LS_CLAIM_PREPARER,
LS_AREA_RESEARCH;
IF DONE  THEN
LEAVE INSERT_LOOP;
END IF;
INSERT INTO AWARD_MASTER_DATASET_RT(
ACCOUNT_NUMBER,
AWARD_NUMBER,
AWARD_ID,
SEQUENCE_NUMBER,
TITLE,
AWARD_STATUS,
ACCOUNT_TYPE,
AWARD_TYPE,
ACTIVITY_TYPE,
LEAD_UNIT_NUMBER,
UNIT_NAME,
GRANT_CALL_TITLE,
SPONSOR_CODE,
SPONSOR_NAME,
PRIME_SPONSOR_CODE,
PRIME_SPONSOR_NAME,
AWARD_EXECUTION_DATE,
AWARD_EFFECTIVE_DATE,
FINAL_EXPIRATION_DATE,
AWARD_SEQUENCE_STATUS,
KEY_PERSON_NAME,
KEY_PERSON_ID,
PERSON_ROLE_ID,
PERSON_ROLE,
ANTICIPATED_TOTAL_AMOUNT,
AMOUNT_OBLIGATED_TO_DATE,
CURRENT_FUND_EFFECTIVE_DATE,
OBLIGATION_EXPIRATION_DATE,
BEGIN_DATE,
EMPLOYEE_FLAG,
PI_PERSON_ID,
PI_NAME,
PI_HOME_UNIT_NUMBER,
PI_HOME_UNIT,
AWARD_WORKFLOW_STATUS_CODE,
AWARD_WORKFLOW_STATUS,
SPONSOR_AWARD_NUMBER,
KEY_PERSON_ORGANIZATION,
KEY_PERSON_DEPARTMENT_NUMBER,
KEY_PERSON_DEPARTMENT,
KEY_PERSON_COUNTRY,
KEY_PERSON_EMAIL,
TOTAL_COST_SHARE,
TOTAL_PROJECT_COST,
TOTAL_COST_IN_CURRENCY,
TOTAL_COST,
TOTAL_DIRECT_COST,
TOTAL_INDIRECT_COST,
BASIS_OF_PAYMENT_DESC,
METHOD_OF_PAYMENT_DESC,
PAYMENT_INVOICE_FREQ_DESC,
RATE_CLASS_CODE,
AWARD_CREATE_TIMESTAMP,
AWARD_CREATE_USER,
STEM_NONSTEM,
RIE_DOMAIN,
INPUT_GST_CATEGORY,
OUTPUT_GST_CATEGORY,
GRANT_CODE,
SUB_LEAD_UNIT,
PROFIT_CENTER,
FUND_CENTER,
COST_CENTER,
MULTIPLIER,
CUMULATIVE_REVENUE,
CUMULATIVE_ACTUAL_EXPENSE,
CUMULATIVE_COMMITTED_EXPENSE,
BALANCE_FUND,
BALANCE_LESS_COMMITTED_AMOUNT,
ORIGINAL_APPROVED_BUDGET,
LATEST_APPROVED_BUDGET,
UTILIZATION_RATE,
DURATION,
PERCENTAGE_OF_EFFORT,
CUMULATIVE_VIREMENT,
DISPLAY_AT_ACAD_PROFILE,
GRANT_PER_MONTH,
F_AND_A_RATE_TYPE,
PROJECT_AGE,
FUNDING_SCHEME,
SPONSOR_TYPE_CODE,
SPONSOR_TYPE,
LAST_SYNC_TIME,
ABBREVIATION,
GRANT_FUNDING_AGENCY,
GRANT_FUNDING_AGENCY_TYPE,
SPONSOR_COUNTRY,
PRIME_SPONSOR_COUNTRY,
FUND_CODE,
AWARD_STATUS_CODE,
ACTUAL_EXPENSE_WO_IRC,
COMMITTED_EXPENSE_WO_IRC,
UTILIZATION_RATE_WO_IRC,
LEVEL_2_SUP_ORG,
KEY_PERSON_GENDER,
FEED_STATUS,
NTU_PRIORITY,
FUNDER_APPROVAL_DATE,
CLAIM_PREPARER,
AREA_OF_RESEARCH
) VALUES (
LS_ACCOUNT_NUMBER,
LS_AWARD_NUMBER,
LI_AWARD_ID,
LI_SEQUENCE_NUMBER,
LS_TITLE,
LS_AWARD_STATUS,
LS_ACCOUNT_TYPE,
LS_AWARD_TYPE,
LS_ACTIVITY_TYPE,
LS_LEAD_UNIT_NUMBER,
LS_LEAD_UNIT_NAME,
LS_GRANT_CALL_TITLE,
LS_SPONSOR_CODE,
LS_SPONSOR_NAME,
LS_PRIME_SPONSOR_CODE,
LS_PRIME_SPONSOR_NAME,
LD_AWARD_EXECUTION_DATE,
LD_AWARD_EFFECTIVE_DATE,
LD_FINAL_EXPIRATION_DATE,
LS_AWARD_SEQUENCE_STATUS,
LS_KEY_PERSON_NAME,
LS_KEY_PERSON_ID,
LI_PERSON_ROLE_ID,
LS_PERSON_ROLE,
LB_ANTICIPATED_TOTAL_AMOUNT,
LB_AMOUNT_OBLIGATED_TO_DATE,
LD_CURRENT_FUND_EFFECTIVE_DATE,
LD_OBLIGATION_EXPIRATION_DATE,
LD_BEGIN_DATE,
LS_EMPLOYEE_FLAG,
LS_PI_PERSON_ID,
LS_PI_NAME,
LS_PI_HOME_UNIT_NUMBER,
LS_PI_HOME_UNIT,
LS_WORKFLOW_AWARD_STATUS_CODE,
LS_AWARD_WORKFLOW_STATUS_DESC,
LS_SPONSOR_AWARD_NUMBER,
LS_KEY_PERSON_ORGANIZATION,
LS_KEY_PERSON_DEPARTMENT_NUMBER,
LS_KEY_PERSON_DEPARTMENT,
LS_KEY_PERSON_COUNTRY,
LS_KEY_PERSON_EMAIL,
LB_TOTAL_COST_SHARE,
LB_TOTAL_PROJECT_COST,
LB_TOTAL_COST_IN_CURRENCY,
LB_TOTAL_COST,
LB_TOTAL_DIRECT_COST,
LB_TOTAL_INDIRECT_COST,
LS_BASIS_OF_PAYMENT_DESC,
LS_METHOD_OF_PAYMENT_DESC,
LS_PAYMENT_INVOICE_FREQ_DESC,
LS_RATE_CLASS_CODE,
LD_AWARD_CREATE_TIMESTAMP,
LS_CREATE_USER,
LS_STEM_NONSTEM,
LS_RIE_DOMAIN,
LS_INPUT_GST_CATEGORY,
LS_OUTPUT_GST_CATEGORY,
LS_GRANT_CODE,
LS_SUB_LEAD_UNIT,
LS_PROFIT_CENTER,
LS_FUND_CENTER,
LS_COST_CENTER,
LS_MULTIPLIER,
LB_CUMULATIVE_REVENUE,
LB_CUMULATIVE_ACTUAL_EXPENSE,
LB_CUMULATIVE_COMMITTED_EXPENSE,
LB_BALANCE_FUND,
LB_BALANCE_LESS_COMMITTED_AMOUNT,
LB_ORIGINAL_APPROVED_BUDGET,
LB_LATEST_APPROVED_BUDGET,
LS_UTILIZATION_RATE,
LS_DURATION,
LB_PERCENTAGE_OF_EFFORT,
LS_CUMULATIVE_VIREMENT,
LS_DISPLAY_AT_ACAD_PROFILE,
LB_GRANT_PER_MONTH,
LS_F_AND_A_RATE_TYPE,
LS_PROJECT_AGE,
LS_FUNDING_SCHEME,
LS_SPONSOR_TYPE_CODE,
LS_SPONSOR_TYPE,
LD_UPDATE_TIMESTAMP,
LS_ABBREVIATION,
LS_GRANT_FUNDING_AGENCY,
LS_GRANT_FUNDING_AGENCY_TYPE,
LS_SPONSOR_COUNTRY,
LS_PRIME_SPONSOR_COUNTRY,
LS_FUND_CODE,
LS_STATUS_CODE,
LB_ACTUAL_EXPENSE_WO_IRC,
LB_COMMITTED_EXPENSE_WO_IRC,
LS_UTILIZATION_RATE_WO_IRC,
LS_LEVEL_2_SUP_ORG,
LS_KEY_PERSON_GENDER,
LS_FEED_STATUS,
LS_NTU_PRIORITY,
LS_FUNDER_APPROVAL_DATE,
LS_CLAIM_PREPARER,
LS_AREA_RESEARCH
);
END LOOP;
CLOSE CUR_AWARD_MASTER_DATA_SET;
END;
SELECT 1 AS SUCCESS FROM DUAL;
END
//
