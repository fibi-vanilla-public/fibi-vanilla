DELIMITER //
CREATE  FUNCTION `FN_CHK_PROP_BUDGET_PERS_INKIND`(AV_PROPOSAL_ID    INT(10)) RETURNS varchar(10) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
DECLARE LI_COUNT INT(3);
	SELECT COUNT(1) INTO LI_COUNT FROM BUDGET_DETAIL T1
	INNER JOIN BUDGET_PERIOD T2 ON T1.BUDGET_PERIOD_ID = T2.BUDGET_PERIOD_ID
	INNER JOIN BUDGET_HEADER T3 ON T2.BUDGET_HEADER_ID = T3.BUDGET_HEADER_ID
	INNER JOIN BUDGET_PERSON_DETAIL T4 ON T1.BUDGET_DETAILS_ID = T4.BUDGET_DETAILS_ID
	WHERE T3.IS_FINAL_BUDGET = 'Y' AND T1.COST_ELEMENT IN  (12,13,10,9,11)
	AND (T4.COST_SHARING_PERCENT <> '100.00' OR T4.COST_SHARING_PERCENT IS NULL) AND T3.PROPOSAL_ID = AV_PROPOSAL_ID;
IF LI_COUNT > 0 THEN
	RETURN 'TRUE';
ELSE
	RETURN 'FALSE';
END IF;
END
//
