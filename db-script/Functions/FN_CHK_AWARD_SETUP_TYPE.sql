DELIMITER //
CREATE  FUNCTION `FN_CHK_AWARD_SETUP_TYPE`(
AV_AWARD_ID VARCHAR(22)
) RETURNS varchar(6) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
DECLARE LI_FLAG INT;
SELECT COUNT(*) INTO LI_FLAG
FROM AWARD
WHERE AWARD_DOCUMENT_TYPE_CODE = 1
AND AWARD_ID = AV_AWARD_ID;
IF LI_FLAG THEN 
	RETURN 'TRUE';
ELSE 
	RETURN 'FALSE';
END IF;
RETURN 'TRUE';
END
//
