DELIMITER //
CREATE  FUNCTION `FN_CHK_SAP_AWARD_FEED_RESPONSE`(AV_AWARD_NUMBER VARCHAR(12), AV_AWARD_ID DECIMAL(22,0)
) RETURNS varchar(10) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
DECLARE LS_FULL_NAME VARCHAR(90);
DECLARE LI_FLAG INT;
DECLARE LS_AWD_ACCOUNT_TYPE_CODE 			VARCHAR(3);
DECLARE LS_FUND_CODE						VARCHAR(4000);
DECLARE LS_ROOT_AWARD_NUMBER				VARCHAR(12);
DECLARE LS_VARIATION_TYPE_CODE				VARCHAR(3);
DECLARE  LS_AWARD_NUMBER					VARCHAR(12);
DECLARE LS_CHK_FEED_ENTRY					VARCHAR(10);
	SELECT COUNT(1) INTO LI_FLAG FROM SAP_AWARD_FEED
	WHERE AWARD_NUMBER = AV_AWARD_NUMBER;
	IF LI_FLAG  = 0 THEN
		RETURN 'TRUE';
	ELSE
		SELECT COUNT(1) INTO LI_FLAG FROM AWARD WHERE AWARD_ID = AV_AWARD_ID 
		AND AWARD_VARIATION_TYPE_CODE IN ('15','14','8','11','7','10','9','13','2','5');
		IF LI_FLAG > 0 THEN
			UPDATE SAP_AWARD_FEED SET FEED_STATUS = 'N', SYSTEM_COMMENT = 'THIS_VR_NOT_REQUIRED_TO_FEED_SAP',NO_FEED_REPORT_FLAG = 'N' 
			WHERE AWARD_ID = AV_AWARD_ID;
		END IF;
		SELECT COUNT(1) INTO LI_FLAG FROM SAP_AWARD_FEED T1
		WHERE T1.AWARD_NUMBER = AV_AWARD_NUMBER
		AND T1.FEED_ID = (SELECT MAX(T2.FEED_ID) FROM SAP_AWARD_FEED T2 
							 WHERE T1.AWARD_NUMBER = T2.AWARD_NUMBER)
		AND FEED_STATUS IN('E','R','N'); 
		IF LI_FLAG > 0 THEN
			RETURN 'TRUE';
		ELSE
			RETURN 'FALSE';
		END IF;
	END IF;
END
//
