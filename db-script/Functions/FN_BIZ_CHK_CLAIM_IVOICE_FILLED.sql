DELIMITER //
CREATE  FUNCTION `FN_BIZ_CHK_CLAIM_IVOICE_FILLED`(
AV_CLAIM_ID    DECIMAL(22)
) RETURNS varchar(6) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
        DECLARE LI_COUNT INT;
		DECLARE LI_CLAIM_AMOUNT DECIMAL(12,2);
		DECLARE LI_AWARD_NUMBER VARCHAR(12);
		select COUNT(CLAIM_ID) INTO LI_COUNT  from claim_invoice_details where  CLAIM_ID = AV_CLAIM_ID;
		IF LI_COUNT = 0 THEN
			SELECT AWARD_NUMBER INTO LI_AWARD_NUMBER FROM CLAIM WHERE CLAIM_ID = AV_CLAIM_ID;
			SET LI_CLAIM_AMOUNT = FN_GET_CLAIM_AMOUNT(LI_AWARD_NUMBER, AV_CLAIM_ID);
			IF LI_CLAIM_AMOUNT > 0 THEN
				RETURN 'FALSE';
			ELSE	
				RETURN 'TRUE';
			END IF;
		ELSE
                RETURN 'TRUE';
        END IF;
        RETURN 'TRUE';
END
//
