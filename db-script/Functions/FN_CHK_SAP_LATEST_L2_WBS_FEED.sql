DELIMITER //
CREATE  FUNCTION `FN_CHK_SAP_LATEST_L2_WBS_FEED`(
AV_AWARD_NUMBER    	VARCHAR(12),
AV_IO_CODE	   		VARCHAR(50),
AV_TYPE				VARCHAR(10)
) RETURNS varchar(22) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
DECLARE LS_ERROR_MSG 		VARCHAR(1000);
DECLARE LI_SEQ_ERROR_LOG_ID INT;
DECLARE LI_AWARD_ID			DECIMAL(22,0);
DECLARE LS_AWARD_NUMBER 	INT;
DECLARE LI_FLAG 			INT;
DECLARE LI_FEED_ID			INT;
DECLARE LS_TABLE_NAME		VARCHAR(30);
DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
	        GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, 
			@errno = MYSQL_ERRNO, @msg = MESSAGE_TEXT;
            SET @full_error = CONCAT(@msg,'|SEC111|',LS_TABLE_NAME );
			SELECT @full_error INTO LS_ERROR_MSG;
            RESIGNAL SET MESSAGE_TEXT = LS_ERROR_MSG;
            SET LS_TABLE_NAME = 'SAP_AWARD_FEED_BATCH_ERROR_LOG';
		INSERT INTO SAP_AWARD_FEED_BATCH_ERROR_LOG(ERROR_MESSAGE, UPDATE_TIMESTAMP, UPDATE_USER)
		VALUES(CONCAT('IN FN_CHK_SAP_LATEST_L2_WBS_FEED PROCEDURE ',SUBSTR(LS_ERROR_MSG,1,960)),UTC_TIMESTAMP(),'quickstart');
	END;
IF AV_TYPE = 'GM' THEN
		SELECT COUNT(1) INTO LI_FLAG FROM SAP_FEED_TMPL_GRANT_BUD_MASTER T1
		WHERE T1.SPONSOR_CLASS = AV_IO_CODE
		AND T1.FEED_STATUS = 'S'
		AND T1.ID = (SELECT MAX(T2.ID) FROM SAP_FEED_TMPL_GRANT_BUD_MASTER T2 
					 WHERE T1.SPONSOR_CLASS = T2.SPONSOR_CLASS
					 AND T2.FEED_STATUS = 'S'
					 );
		IF LI_FLAG > 0 THEN
			SELECT FEED_ID INTO LI_FEED_ID FROM SAP_FEED_TMPL_GRANT_BUD_MASTER T1
			WHERE T1.SPONSOR_CLASS = AV_IO_CODE
			AND T1.FEED_STATUS = 'S'
			AND T1.ID = (SELECT MAX(T2.ID) FROM SAP_FEED_TMPL_GRANT_BUD_MASTER T2 
						 WHERE T1.SPONSOR_CLASS = T2.SPONSOR_CLASS
						 AND T2.FEED_STATUS = 'S'
						 );
			SELECT AWARD_ID INTO LI_AWARD_ID FROM SAP_AWARD_FEED
			WHERE FEED_ID = LI_FEED_ID;
			RETURN LI_AWARD_ID;
		ELSE
			RETURN NULL;
		END IF;
ELSEIF AV_TYPE = 'FM' THEN
	SELECT COUNT(1) INTO LI_FLAG FROM SAP_FEED_TMPL_FM_BUDGET T1
		WHERE T1.FUNDED_PROGRAM = AV_IO_CODE
		AND T1.FEED_STATUS = 'S'
		AND T1.ID = (SELECT MAX(T2.ID) FROM SAP_FEED_TMPL_FM_BUDGET T2 
					 WHERE T1.FUNDED_PROGRAM = T2.FUNDED_PROGRAM
					 AND T2.FEED_STATUS = 'S'
					 );
		IF LI_FLAG > 0 THEN
			SELECT FEED_ID INTO LI_FEED_ID FROM SAP_FEED_TMPL_FM_BUDGET T1
			WHERE T1.FUNDED_PROGRAM = AV_IO_CODE
			AND T1.FEED_STATUS = 'S'
			AND T1.ID = (SELECT MAX(T2.ID) FROM SAP_FEED_TMPL_FM_BUDGET T2 
						 WHERE T1.FUNDED_PROGRAM = T2.FUNDED_PROGRAM
						 AND T2.FEED_STATUS = 'S'
						 );
			SELECT AWARD_ID INTO LI_AWARD_ID FROM SAP_AWARD_FEED
			WHERE FEED_ID = LI_FEED_ID;
			RETURN LI_AWARD_ID;
		ELSE
			RETURN NULL;
		END IF;
END IF;
END
//
