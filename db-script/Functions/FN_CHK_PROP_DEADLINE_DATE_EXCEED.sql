DELIMITER //
CREATE  FUNCTION `FN_CHK_PROP_DEADLINE_DATE_EXCEED`(
AV_PROPOSAL_ID    INT(11)
) RETURNS varchar(10) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
DECLARE LI_COUNT INT(3);
  SELECT COUNT(*) INTO LI_COUNT
        FROM EPS_PROPOSAL T1
        WHERE T1.PROPOSAL_ID = AV_PROPOSAL_ID
        AND T1.SPONSOR_DEADLINE_DATE < utc_date();
   IF LI_COUNT > 0 THEN
                RETURN 'TRUE';
   END IF;
    SELECT COUNT(*) INTO LI_COUNT
        FROM EPS_PROPOSAL T1
        WHERE T1.PROPOSAL_ID = AV_PROPOSAL_ID
        AND T1.INTERNAL_DEADLINE_DATE < utc_date();
   IF LI_COUNT > 0 THEN
                RETURN 'TRUE';
   END IF;
RETURN 'FALSE';
END
//
