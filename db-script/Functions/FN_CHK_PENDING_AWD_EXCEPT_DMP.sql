DELIMITER //
CREATE  FUNCTION `FN_CHK_PENDING_AWD_EXCEPT_DMP`(
AV_AWARD_ID VARCHAR(22)
) RETURNS varchar(12) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
DECLARE LI_FLAG INT;
DECLARE LI_COUNT INT;
DECLARE LS_AWARD_NUMBER varchar(12);
DECLARE LS_AWARD_ID VARCHAR(12);
SELECT AWARD_NUMBER INTO LS_AWARD_NUMBER FROM AWARD
where (AWARD_VARIATION_TYPE_CODE IS NULL OR AWARD_VARIATION_TYPE_CODE IN (7,19)) AND AWARD_ID = AV_AWARD_ID;
IF LS_AWARD_NUMBER IS NULL THEN
RETURN 'TRUE';
ELSE
SELECT COUNT(1) INTO LI_COUNT FROM AWARD
WHERE AWARD_SEQUENCE_STATUS='PENDING' AND AWARD_NUMBER = LS_AWARD_NUMBER 
AND AWARD_ID <> AV_AWARD_ID AND AWARD_VARIATION_TYPE_CODE <> 5;
IF LI_COUNT  = 0 THEN
RETURN 'TRUE';
ELSE
RETURN 'FALSE';
END IF;
END IF;
END
//
