DELIMITER //
CREATE  FUNCTION `FN_CHK_CLAIM_INV_LINEITEM_ZERO`(
AV_CLAIM_ID VARCHAR(12)) RETURNS varchar(10) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
DECLARE LI_COUNT INT(3);
	SELECT COUNT(T.TOTAL) INTO LI_COUNT FROM (SELECT T2.INVOICE_DETAIL_ID, COALESCE(T2.CLAIM_AMOUNT, 0) + COALESCE(T2.SUB_CONTRACT_AMOUNT, 0) AS TOTAL
		FROM CLAIM_INVOICE T1 
			INNER JOIN CLAIM_INVOICE_DETAILS T2 ON T1.INVOICE_ID = T2.INVOICE_ID
			WHERE T1.CLAIM_ID = AV_CLAIM_ID AND T1.SEQUENCE_NUMBER IN (SELECT MAX(SEQUENCE_NUMBER) FROM CLAIM_INVOICE WHERE CLAIM_ID =AV_CLAIM_ID)) AS T
			WHERE (T.TOTAL='0.00' OR T.TOTAL IS NULL);
		IF LI_COUNT = 0 THEN
			RETURN 'TRUE';
		ELSE
			RETURN 'FALSE';
		END IF;	
END
//
