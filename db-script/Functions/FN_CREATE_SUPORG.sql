DELIMITER //
CREATE  FUNCTION `FN_CREATE_SUPORG`(
AV_TASK_ID			VARCHAR(10)
) RETURNS varchar(10) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
DECLARE LS_MANPOWER_TASK 	INT;
SET LS_MANPOWER_TASK = 0;
	IF AV_TASK_ID > 0 THEN
		SELECT COUNT(1) INTO LS_MANPOWER_TASK FROM TASK WHERE TASK_TYPE_CODE IN (2, 5) AND TASK_ID = AV_TASK_ID;
        IF LS_MANPOWER_TASK = 0 THEN
			RETURN 'FALSE';
		END IF;
	END IF;
RETURN 'TRUE';
END
//
