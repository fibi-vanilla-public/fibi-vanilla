DELIMITER //
CREATE  FUNCTION `FN_CHECK_GRANT_REVENUE_VALUE`(
AV_AWARD_ID	DECIMAL
) RETURNS varchar(10) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
DECLARE LI_FLAG INT;
	SELECT COUNT(1) INTO LI_FLAG
	FROM AWARD WHERE AWARD_ID = AV_AWARD_ID;
	IF LI_FLAG > 0 THEN
		SELECT 
			COUNT(1) INTO LI_FLAG
		FROM AWARD_BUDGET_HEADER T1
		INNER JOIN AWARD_BUDGET_DETAIL T2 ON T2.BUDGET_HEADER_ID = T1.BUDGET_HEADER_ID
		WHERE T1.AWARD_ID = LI_AWARD_ID 
		AND T1.SEQUENCE_NUMBER = (SELECT MAX(T3.SEQUENCE_NUMBER) FROM AWARD_BUDGET_HEADER T3
								  WHERE T1.AWARD_ID = T3.AWARD_ID AND T3.AWARD_BUDGET_STATUS_CODE = 10
								 )
		AND T1.AWARD_BUDGET_STATUS_CODE = 10
		AND T2.BUDGET_CATEGORY_CODE = 'GRT' AND T2.LINE_ITEM_COST <> 0;
		IF LI_FLAG > 0 THEN
			RETURN 'FALSE';
		ELSE
			RETURN 'TRUE';
		END IF;
	END IF;
	RETURN 'TRUE';
END
//
