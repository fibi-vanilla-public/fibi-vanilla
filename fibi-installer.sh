GREEN='\033[1;32m'
NC='\033[0m'

echo -e "${GREEN}"
echo -e '\033[1mFIBI 4.6\033[0m'
echo -e "${NC}"

YELLOW='\033[1;33m'
RED='\033[0;31m'
DOUBLE_RED='\033[0;31;2m'  # Added ;2 for double font size
NC='\033[0m'

echo -e "${YELLOW}Public IP:${NC}"
curl -s https://checkip.amazonaws.com

echo -e "${YELLOW}OS Release:${NC}"
if [ -f /etc/os-release ]; then
    source /etc/os-release
    echo "$PRETTY_NAME"
else
    echo "OS release information not available."
fi
sleep 3

# Disable SELinux
echo -e "${YELLOW}Disabling SELinux...${NC}"
sudo setenforce 0
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=disabled/' /etc/selinux/config
echo -e "${RED}SELinux disabled.${NC}"

# Update system packages
sudo yum update -y
sudo yum -y install epel-release wget vim

# Install wget
#sudo yum install wget -y 

echo -e "${YELLOW}Installing MySQL...${NC}"
cd fibi-docker/shell-scripts
cp -r ../../db-script/* ../../fibi-docker/DB-script/
./mysql-installer.sh
echo -e "${RED}MySQL installation completed.${NC}"

echo -e "${YELLOW}Installing Docker...${NC}"
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo systemctl start docker
sudo systemctl enable docker
echo -e "${RED}Docker installation completed.${NC}"

sudo rm -rf  get-docker*
cd ..

echo -e "${YELLOW}Running Docker Compose...${NC}"
sudo docker compose up -d --build
echo -e "${RED}Docker Compose run completed.${NC}"

sleep 30

cd shell-scripts
#echo -e "${YELLOW}Running index creation sync script...${NC}"
#sudo ./singlescript.sh
#echo -e "${RED}Index creation sync script completed.${NC}"
echo -e "${DOUBLE_RED}updating ip address in conf file${NC}"  # Modified: Double font size
sudo ./update-ip-conf.sh
echo -e "${DOUBLE_RED}ip changed.${NC}"  # Modified: Double font size

echo -e "${DOUBLE_RED}Index sync script running${NC}"  # Modified: Double font size
sudo ./singlescript.sh
echo -e "${DOUBLE_RED}Index sync script completed${NC}"  # Modified: Double font size

echo -e "${YELLOW}Running lock query configuration script...${NC}"
sudo ./update_lock_config.sh
echo -e "${RED}Lock query configuration script completed.${NC}"

cd ../../
cp -r build/* fibi-docker/webapps


echo -e "${RED}restart Tomcat after updating the build conf file.${NC}"
echo -e "${RED} sudo docker restart tomcat${NC}"

