#!/bin/bash

# Folder locations
source_path="build/*"
destination_path="fibi-docker/webapps/"
container_name="tomcat"

# Copy build files to destination
cp -r ${source_path} ${destination_path}

# Restart Docker container
docker restart ${container_name}

echo "Tomcat container restarted successfully."

# Display container logs
#docker logs -f ${container_name}
