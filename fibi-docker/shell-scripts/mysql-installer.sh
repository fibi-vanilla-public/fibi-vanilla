#!/bin/bash

# Set MySQL root password
export MYSQL_ROOT_PASSWORD=Polus@123

# Download and install GPG key package
sudo wget https://dev.mysql.com/get/mysql80-community-release-el7-3.noarch.rpm
sudo rpm -ivh mysql80-community-release-el7-3.noarch.rpm
sudo rm -rf mysql80-community-release*

# Import GPG key
sudo rpm --import https://repo.mysql.com/RPM-GPG-KEY-mysql-2022

# Disable automatic updates for MySQL package
sudo sed -i '/^\[mysql80-community\]$/a enabled=0' /etc/yum.repos.d/mysql-community.repo

# Install MySQL server package
sudo yum install mysql-community-server-8.0.28 -y

# Add or modify lower_case_table_names option in my.cnf
sudo sed -i '/^\[mysqld\]$/a lower_case_table_names=1' /etc/my.cnf

# Start MySQL service
sudo systemctl start mysqld

# Wait for MySQL service to start
sleep 10

# Retrieve root password from MySQL log
root_password=$(sudo grep 'temporary password' /var/log/mysqld.log | awk '{print $NF}')

# Check if root password is found
if [ -z "$root_password" ]; then
  echo "Failed to retrieve root password from MySQL log. Please check the log file."
  exit 1
fi

# Set root password using mysqladmin
sudo mysqladmin -u root -p"$root_password" password "$MYSQL_ROOT_PASSWORD"

# Create a new database
sudo mysql -u root -p"$MYSQL_ROOT_PASSWORD" <<EOF
CREATE USER 'root'@'%' IDENTIFIED BY 'Polus@123';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';
CREATE DATABASE IF NOT EXISTS fibi;
EOF

# Create a new user with full privileges
sudo mysql -u root -p"$MYSQL_ROOT_PASSWORD" <<EOF
CREATE USER 'fibi'@'%' IDENTIFIED BY 'Polus@123';
GRANT ALL PRIVILEGES ON *.* TO 'fibi'@'%';
EOF

#change to sql directory
cd ../DB-script

# Restore SQL file for fibidemo schema
sudo mysql -u root -p"$MYSQL_ROOT_PASSWORD" -f fibi < ../DB-script/all.sql

cd ../shell-scripts

# Stop MySQL package updates
sudo yum-config-manager --disable mysql80-community

# Restart the MySQL service
sudo systemctl restart mysqld

