#!/bin/bash

CONTAINER_NAME="logstash"
LOGSTASH_COMMANDS=("/usr/share/logstash/conf/fibiproposal.conf"
  "/usr/share/logstash/conf/agreement.conf"
  "/usr/share/logstash/conf/awardfibi.conf"
  "/usr/share/logstash/conf/fibi_reviewer.conf"
  "/usr/share/logstash/conf/grantcall.conf"
  "/usr/share/logstash/conf/personfibi.conf"
  )
  
LOGSTASH_DATA_PATH="/usr/share/logstash/sync"

# Loop through the Logstash commands and execute them
for command in "${LOGSTASH_COMMANDS[@]}"; do
  docker exec "$CONTAINER_NAME" logstash -f "$command" --path.data "$LOGSTASH_DATA_PATH"
done

