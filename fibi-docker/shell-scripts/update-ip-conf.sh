#!/bin/bash

# List of configuration files to update
config_files=(
  "../logstash-config/personfibi.conf"
  "../logstash-config/grantcall.conf"
  "../logstash-config/fibi_reviewer.conf"
  "../logstash-config/fibiproposal.conf"
  "../logstash-config/grantcall.conf"
  "../logstash-config/awardfibi.conf"
  "../logstash-config/agreement.conf"
  # Add more file paths as needed
)

# Retrieve the current IP address dynamically
#ip_address=$(ip route get 1 | awk '{print $NF;exit}')
ip_address=$(curl -sS https://ipinfo.io/ip)

# Iterate over the configuration files and replace the IP address
for file in "${config_files[@]}"; do
  sed -i "s/jdbc:mysql:\/\/[0-9\.]\+:/jdbc:mysql:\/\/$ip_address:/g" "$file"
done

# Restart Logstash or perform any necessary actions
# Restart command example: systemctl restart logstash
