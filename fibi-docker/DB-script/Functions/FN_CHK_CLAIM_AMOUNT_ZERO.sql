DELIMITER //
CREATE  FUNCTION `FN_CHK_CLAIM_AMOUNT_ZERO`(AV_CLAIM_ID VARCHAR(12)) RETURNS varchar(10) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
DECLARE LI_CLAIM_AMOUNT DECIMAL(12,2);
DECLARE LI_AWARD_NUMBER VARCHAR(12);
		SELECT AWARD_NUMBER INTO LI_AWARD_NUMBER FROM CLAIM WHERE CLAIM_ID = AV_CLAIM_ID;
		SET LI_CLAIM_AMOUNT = FN_GET_CLAIM_AMOUNT(LI_AWARD_NUMBER, AV_CLAIM_ID);
		IF LI_CLAIM_AMOUNT <= 0 THEN
			RETURN 'TRUE';
		ELSE	
			RETURN 'FALSE';	
	END IF;		
END
//
