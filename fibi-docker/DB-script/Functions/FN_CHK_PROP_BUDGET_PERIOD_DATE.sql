DELIMITER //
CREATE  FUNCTION `FN_CHK_PROP_BUDGET_PERIOD_DATE`(
AV_PROPOSAL_ID    INT(11)
) RETURNS varchar(10) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
DECLARE LI_BUDGET_HEADER_ID  INT(11);
DECLARE LI_BUDGET_PERIOD INT(11);
DECLARE LS_START_DATE DATETIME;
DECLARE LS_END_DATE DATETIME;
BEGIN
	SELECT  BUDGET_HEADER_ID INTO LI_BUDGET_HEADER_ID
	FROM BUDGET_HEADER 
	WHERE PROPOSAL_ID = AV_PROPOSAL_ID
	AND IS_LATEST_VERSION = 'Y';
BEGIN
		DECLARE DONE1 INT DEFAULT FALSE;
		DECLARE CUR_PERIOD_DATES CURSOR FOR 
		SELECT BUDGET_PERIOD,START_DATE,END_DATE 
		FROM BUDGET_PERIOD 
		WHERE BUDGET_HEADER_ID = LI_BUDGET_HEADER_ID;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE1 = TRUE;	
		OPEN CUR_PERIOD_DATES;
		INSERT_LOOP1: LOOP 
		FETCH CUR_PERIOD_DATES INTO LI_BUDGET_PERIOD,
								LS_START_DATE,
								LS_END_DATE ;
		IF DONE1 THEN
			LEAVE INSERT_LOOP1;
		END IF;
		IF LS_START_DATE IS NULL OR LS_END_DATE IS NULL THEN
			RETURN 'FALSE';  
			LEAVE INSERT_LOOP1;
		END IF;
			END LOOP;
			CLOSE CUR_PERIOD_DATES;
		END;
	END;
	RETURN 'TRUE';	
END
//
