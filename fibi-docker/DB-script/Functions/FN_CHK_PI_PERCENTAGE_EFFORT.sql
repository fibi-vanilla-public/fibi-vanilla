DELIMITER //
CREATE  FUNCTION `FN_CHK_PI_PERCENTAGE_EFFORT`(
AV_PROPOSAL_ID    VARCHAR(10)
) RETURNS varchar(6) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
DECLARE LI_COUNT INT;
SELECT COUNT(*) INTO LI_COUNT 
FROM EPS_PROPOSAL_PERSONS WHERE PROPOSAL_ID = AV_PROPOSAL_ID
AND PROP_PERSON_ROLE_ID = 3
AND PERCENTAGE_OF_EFFORT = 100;
IF LI_COUNT = 0 THEN
	RETURN 'TRUE';
ELSE 
	RETURN 'FALSE';
END IF;
END
//
