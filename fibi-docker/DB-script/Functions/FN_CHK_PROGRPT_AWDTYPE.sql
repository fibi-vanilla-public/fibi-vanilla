DELIMITER //
CREATE  FUNCTION `FN_CHK_PROGRPT_AWDTYPE`(AV_PROGRESS_REPORT_ID INT									     
										  ,AV_AWDTYPE_CODE VARCHAR(3)
											) RETURNS varchar(10) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
DECLARE LI_AWARD_ID INT;
DECLARE LI_COUNT INT;
			SELECT AWARD_ID 
			  INTO LI_AWARD_ID 
			  FROM AWARD_PROGRESS_REPORT  APR
			 WHERE APR.PROGRESS_REPORT_ID = AV_PROGRESS_REPORT_ID;
			SELECT COUNT(*)
			 INTO LI_COUNT
			 FROM AWARD AWD
			WHERE AWD.AWARD_ID = LI_AWARD_ID
			  AND UPPER(AWD.AWARD_TYPE_CODE) = UPPER(AV_AWDTYPE_CODE);			
IF LI_COUNT > 0 THEN
	RETURN 'TRUE';
ELSE
	RETURN 'FALSE';
END IF;
END
//
