DELIMITER //
CREATE  FUNCTION `FN_CHK_PERIOD_WITHIN_PROP_DATES`(AV_PROPOSAL_ID VARCHAR(10)) RETURNS varchar(10) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
DECLARE LI_FLAG INT;
DECLARE LI_COUNT INT;
DECLARE LI_PERIOD_COUNT INT;
DECLARE LI_YEAR_COUNT INT;
DECLARE LI_EQUAL_COUNT INT;
DECLARE LI_WITHIN_COUNT INT;
DECLARE LI_FINAL_BUDGET INT;
DECLARE LI_BUDGET_HEADER_ID int(11);
DECLARE LD_START_DATE DATETIME;
DECLARE LD_END_DATE DATETIME;
	SELECT COUNT(*) INTO LI_FINAL_BUDGET
	FROM BUDGET_HEADER T1
	WHERE T1.PROPOSAL_ID = AV_PROPOSAL_ID
	AND IS_FINAL_BUDGET = 'Y';
		IF LI_FINAL_BUDGET = 0 THEN 
			SELECT COUNT(*) INTO LI_FLAG
			FROM BUDGET_HEADER T1
			WHERE T1.PROPOSAL_ID = AV_PROPOSAL_ID
			AND VERSION_NUMBER = (SELECT MAX(S1.VERSION_NUMBER) 
							 FROM BUDGET_HEADER S1 
							 WHERE S1.PROPOSAL_ID = T1.PROPOSAL_ID);
				IF LI_FLAG > 0 THEN 	 
					SELECT T1.BUDGET_HEADER_ID,T2.START_DATE,T2.END_DATE 
					INTO LI_BUDGET_HEADER_ID,LD_START_DATE,LD_END_DATE
					FROM BUDGET_HEADER T1
					INNER JOIN EPS_PROPOSAL T2 ON T2.PROPOSAL_ID = T1.PROPOSAL_ID
					WHERE T1.PROPOSAL_ID = AV_PROPOSAL_ID
					AND VERSION_NUMBER = (SELECT MAX(S1.VERSION_NUMBER) 
									 FROM BUDGET_HEADER S1 
									 WHERE S1.PROPOSAL_ID = T1.PROPOSAL_ID);
				END IF;
		 ELSEIF LI_FINAL_BUDGET > 0 THEN 
				SELECT T1.BUDGET_HEADER_ID,T2.START_DATE,T2.END_DATE 
				INTO LI_BUDGET_HEADER_ID,LD_START_DATE,LD_END_DATE
				FROM BUDGET_HEADER T1
				INNER JOIN EPS_PROPOSAL T2 ON T2.PROPOSAL_ID = T1.PROPOSAL_ID
				WHERE T1.PROPOSAL_ID = AV_PROPOSAL_ID
				AND IS_FINAL_BUDGET = 'Y';
		END IF;
	SELECT TIMESTAMPDIFF(YEAR, date(LD_START_DATE) , date(LD_END_DATE)) into LI_YEAR_COUNT from dual;
	SELECT COUNT(*) INTO LI_PERIOD_COUNT
	FROM BUDGET_PERIOD T1
	WHERE T1.BUDGET_HEADER_ID = LI_BUDGET_HEADER_ID;
    SELECT COUNT(*) INTO LI_EQUAL_COUNT
	FROM BUDGET_PERIOD T1
	WHERE T1.BUDGET_HEADER_ID = LI_BUDGET_HEADER_ID
	AND( (DATE(T1.START_DATE) = DATE(LD_START_DATE) and BUDGET_PERIOD = 1)
		 OR (DATE(T1.END_DATE) = DATE(LD_END_DATE)  and BUDGET_PERIOD = LI_PERIOD_COUNT )
		 OR (DATE(T1.END_DATE) = DATE(LD_END_DATE)  and BUDGET_PERIOD = LI_YEAR_COUNT ));
			SELECT COUNT(*) INTO LI_WITHIN_COUNT
			FROM BUDGET_PERIOD T1
			WHERE T1.BUDGET_HEADER_ID = LI_BUDGET_HEADER_ID
			AND(( DATE(T1.START_DATE) > DATE(LD_START_DATE) and BUDGET_PERIOD = 1)
			or (DATE(T1.END_DATE) < DATE(LD_END_DATE) and BUDGET_PERIOD = LI_PERIOD_COUNT));
   IF ( LI_EQUAL_COUNT =  2 AND LI_PERIOD_COUNT > 1 )THEN
		RETURN 'FALSE'; 
	elseif ( LI_EQUAL_COUNT < 2 and LI_PERIOD_COUNT > 1 ) THEN
			 if LI_WITHIN_COUNT = 0 THEN 
				 RETURN 'FALSE'; 
			 ELSE
				 RETURN 'TRUE'; 
			end if;
  end if ;	
IF (LI_PERIOD_COUNT = 1 and LI_EQUAL_COUNT = 0 )THEN  
 	RETURN 'TRUE'; 
 ELSEIF(LI_PERIOD_COUNT = 1 and LI_EQUAL_COUNT = 1 ) then
			if (LI_WITHIN_COUNT > 0 ) then 
				RETURN 'TRUE'; 
			else 
				return 'FASLE';  
            end if;
END IF;
END
//
