DELIMITER //
CREATE  FUNCTION `FN_CHK_BZ_GRANTCALL_ELGLTY_COI`(
AV_MODULE_ITEM_KEY  INT(11),
AV_PROPOSAL_ID INT(11)
) RETURNS varchar(6) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
DECLARE LS_IS_EXTERNAL_USER varchar(1);
DECLARE LS_IS_RESEARCH_STAFF varchar(1);
DECLARE LS_IS_SUPPORT_STAFF varchar(1);
DECLARE LS_IS_FACULTY varchar(1);
DECLARE LS_ELIGIBILITY_TARGET_TYPE_CODE varchar(1);
DECLARE LS_TARGET_VALUE varchar(40);
DECLARE LS_HOME_UNIT varchar(8);
DECLARE LI_TARGET_FLAG INT(1);
DECLARE LI_FLAG INT(1);
DECLARE LS_DYN_SQL LONGTEXT;
DECLARE LS_DYN_SQL_FINAL LONGTEXT;
DECLARE LS_PERSON_ID VARCHAR(40);
DECLARE LI_GRANT_ELGBLTY_TYPE_CODE INT(11);
	 BEGIN
		SET LS_DYN_SQL ='';
        set LS_DYN_SQL_FINAL = '';
        SET LI_TARGET_FLAG = 0;
        SET LI_FLAG = 0;
		SELECT count(1) into LI_FLAG FROM eps_proposal t1
		inner join eps_proposal_persons t3 on t1.PROPOSAL_ID = t3.PROPOSAL_ID 
		WHERE t1.GRANT_HEADER_ID = AV_MODULE_ITEM_KEY and t1.PROPOSAL_ID = AV_PROPOSAL_ID and t3.PROP_PERSON_ROLE_ID = 1;
		IF LI_FLAG > 0 THEN
        BEGIN
			DECLARE DONE1 INT DEFAULT FALSE;
			DECLARE CUR_GRANT_ELIGIBILITY CURSOR FOR
				SELECT t3.person_id FROM eps_proposal t1
				inner join eps_proposal_persons t3 on t1.PROPOSAL_ID = t3.PROPOSAL_ID 
				WHERE t1.GRANT_HEADER_ID = AV_MODULE_ITEM_KEY and t1.PROPOSAL_ID = AV_PROPOSAL_ID and t3.PROP_PERSON_ROLE_ID = 1;
			DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE1 = TRUE;
			OPEN CUR_GRANT_ELIGIBILITY;
			INSERT_CUR_GRANT_ELIGIBILITY_LOOP: LOOP 
			FETCH CUR_GRANT_ELIGIBILITY INTO
                LS_PERSON_ID;			
			IF DONE1 THEN
				LEAVE INSERT_CUR_GRANT_ELIGIBILITY_LOOP;
			END IF;
				 SELECT count(1) into LI_FLAG FROM GRANT_CALL_ELIGIBILITY t2 
				INNER JOIN GRANT_ELIGIBILITY_TARGET T4 ON T2.GRANT_ELIGIBILITY_ID = T4.GRANT_ELIGIBILITY_ID
				WHERE t2.GRANT_HEADER_ID = AV_MODULE_ITEM_KEY AND t2.PROP_PERSON_ROLE_ID = 1;
				IF LI_FLAG > 0 THEN
					BEGIN
						DECLARE DONE1 INT DEFAULT FALSE;
						DECLARE CUR_GRANT_ELIGIBILITY_PERSON CURSOR FOR
						SELECT  t2.GRANT_ELGBLTY_TYPE_CODE, T4.ELIGIBILITY_TARGET_TYPE_CODE, T4.TARGET_VALUE FROM GRANT_CALL_ELIGIBILITY t2 
						INNER JOIN GRANT_ELIGIBILITY_TARGET T4 ON T2.GRANT_ELIGIBILITY_ID = T4.GRANT_ELIGIBILITY_ID
						WHERE t2.GRANT_HEADER_ID = AV_MODULE_ITEM_KEY AND t2.PROP_PERSON_ROLE_ID = 1;
						DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE1 = TRUE;
						OPEN CUR_GRANT_ELIGIBILITY_PERSON;
						INSERT_CUR_GRANT_ELIGIBILITY_PERSON_LOOP: LOOP 
						FETCH CUR_GRANT_ELIGIBILITY_PERSON INTO
							LI_GRANT_ELGBLTY_TYPE_CODE,
							LS_ELIGIBILITY_TARGET_TYPE_CODE,
							LS_TARGET_VALUE;
						IF DONE1 THEN
							LEAVE INSERT_CUR_GRANT_ELIGIBILITY_PERSON_LOOP;
						END IF;
							SET LI_FLAG = 0;    
							SET LI_TARGET_FLAG = 0;
							SELECT IS_EXTERNAL_USER, IS_RESEARCH_STAFF, IS_SUPPORT_STAFF, IS_FACULTY, HOME_UNIT 
							INTO LS_IS_EXTERNAL_USER, LS_IS_RESEARCH_STAFF, LS_IS_SUPPORT_STAFF, LS_IS_FACULTY, LS_HOME_UNIT FROM PERSON WHERE PERSON_ID = LS_PERSON_ID;
							IF (LS_ELIGIBILITY_TARGET_TYPE_CODE = 2 AND LS_HOME_UNIT = LS_TARGET_VALUE) 
							OR (LS_ELIGIBILITY_TARGET_TYPE_CODE = 1 AND LS_HOME_UNIT IS NOT NULL)THEN
								SET LI_TARGET_FLAG = 1;
							END IF;
							IF (LS_ELIGIBILITY_TARGET_TYPE_CODE = 3 AND LS_PERSON_ID IN (SELECT TARGET_VALUE FROM GRANT_ELIGIBILITY_TARGET WHERE 
							GRANT_ELIGIBILITY_ID IN (SELECT GRANT_ELIGIBILITY_ID FROM GRANT_CALL_ELIGIBILITY WHERE GRANT_HEADER_ID = AV_MODULE_ITEM_KEY
							AND PROP_PERSON_ROLE_ID = 1) AND ELIGIBILITY_TARGET_TYPE_CODE = 3)) THEN 
								SET LI_TARGET_FLAG = 1;
							END IF;
							IF LI_GRANT_ELGBLTY_TYPE_CODE = 1  AND LS_IS_FACULTY = 'Y' AND LI_TARGET_FLAG > 0 THEN
								SET LI_FLAG = 1;     
							ELSEIF LI_GRANT_ELGBLTY_TYPE_CODE = 2  AND LS_IS_RESEARCH_STAFF = 'Y'  AND LI_TARGET_FLAG > 0 THEN
								SET LI_FLAG = 1;
							ELSEIF LI_GRANT_ELGBLTY_TYPE_CODE = 3  AND LS_IS_EXTERNAL_USER = 'Y'  AND LI_TARGET_FLAG > 0 THEN
								SET LI_FLAG = 1;
							ELSEIF LI_GRANT_ELGBLTY_TYPE_CODE = 4  AND LS_IS_SUPPORT_STAFF = 'Y'  AND LI_TARGET_FLAG > 0 THEN
								SET LI_FLAG = 1;
							ELSE
								 SET LS_DYN_SQL = CONCAT(LS_DYN_SQL,' FALSE ');
							END IF;
							IF LI_FLAG = 1 THEN
								SET LS_DYN_SQL = CONCAT(LS_DYN_SQL,' TRUE ');
							ELSE
								SET LS_DYN_SQL = CONCAT(LS_DYN_SQL,' FALSE ');
							END IF;
						END LOOP;
						CLOSE CUR_GRANT_ELIGIBILITY_PERSON;
					 END;
                     IF (SELECT count(1) FROM DUAL WHERE LS_DYN_SQL LIKE '%TRUE%') > 0 then
						SET LS_DYN_SQL_FINAL = CONCAT(LS_DYN_SQL_FINAL,' TRUE ');
					ELSE
						SET LS_DYN_SQL_FINAL = CONCAT(LS_DYN_SQL_FINAL,' FALSE ');
					END IF;
				ELSE
					RETURN 'TRUE';
				END IF;
			END LOOP;
			CLOSE CUR_GRANT_ELIGIBILITY;
            IF (SELECT count(1) FROM DUAL WHERE LS_DYN_SQL_FINAL LIKE '%FALSE%') > 0 then
				RETURN 'FALSE';
            ELSE
				RETURN 'TRUE';
            END IF;
	END;
    ELSE 
		RETURN 'TRUE';
    END IF;
	END;
END
//
