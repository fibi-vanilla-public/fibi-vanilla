DELIMITER //
CREATE  FUNCTION `FN_GRANT_CALL_FUNDNG_SCHEME`(
AV_PROPOSAL_ID    int(10)
) RETURNS varchar(10) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
DECLARE LI_FLAG INT;
DECLARE LS_PROPOSAL_ID INT;
SELECT PROPOSAL_ID INTO LS_PROPOSAL_ID FROM PROPOSAL_REVIEW
where REVIEW_ID = AV_PROPOSAL_ID;
SELECT COUNT(1) INTO LI_FLAG
FROM GRANT_CALL_HEADER
WHERE GRANT_HEADER_ID IN (SELECT GRANT_HEADER_ID FROM EPS_PROPOSAL
						  WHERE PROPOSAL_ID = LS_PROPOSAL_ID
                          )
AND FUNDING_SCHEME_ID = 1;
IF LI_FLAG > 0 THEN
RETURN 'TRUE';
END IF;
RETURN 'FALSE';
END
//
