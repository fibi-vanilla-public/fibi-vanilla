DELIMITER //
CREATE  FUNCTION `FN_PERSON_HAS_RIGHTS`(
AV_RIGHT_NAME            VARCHAR(100),
AV_PERSON_ID             VARCHAR(40),
AV_UNIT_NUMBER           VARCHAR(8),
AV_MODULE_CODE           INT(4),
AV_MODULE_ITEM_KEY       VARCHAR(200),				
AV_PERMISSION_TYPE       VARCHAR(40)	
) RETURNS int
    DETERMINISTIC
BEGIN
DECLARE  LS_UNIT_NUMBER   VARCHAR(8);
DECLARE  LI_COUNT          INT;
SET LI_COUNT = 0;
IF AV_PERMISSION_TYPE = 'RIGHT_IN_A_DOCUMENT' THEN
		IF AV_MODULE_CODE = 1 THEN   
				SELECT  LEAD_UNIT_NUMBER INTO LS_UNIT_NUMBER  
				FROM AWARD WHERE AWARD_NUMBER = AV_MODULE_ITEM_KEY ;
		ELSEIF AV_MODULE_CODE = 2 THEN  
				SELECT HOME_UNIT_NUMBER INTO LS_UNIT_NUMBER  
				FROM PROPOSAL WHERE PROPOSAL_NUMBER = AV_MODULE_ITEM_KEY;
		ELSEIF AV_MODULE_CODE = 3 THEN  
				SELECT HOME_UNIT_NUMBER  INTO LS_UNIT_NUMBER 
				FROM EPS_PROPOSAL WHERE PROPOSAL_ID = AV_MODULE_ITEM_KEY;
		ELSEIF AV_MODULE_CODE = 5 THEN 
				SELECT COALESCE(T3.HOME_UNIT_NUMBER,T2.LEAD_UNIT_NUMBER) INTO LS_UNIT_NUMBER 
				FROM  NEGOTIATION_ASSOCIATION T1 
				LEFT JOIN AWARD T2 ON T1.ASSOCIATED_PROJECT_ID = T2.AWARD_NUMBER AND T1.ASSOCIATION_TYPE_CODE = 1
				LEFT JOIN PROPOSAL T3 ON T3.PROPOSAL_ID = T1.ASSOCIATED_PROJECT_ID AND T1.ASSOCIATION_TYPE_CODE = 2
				WHERE T1.NEGOTIATION_ID = AV_MODULE_ITEM_KEY ;
		ELSEIF AV_MODULE_CODE = 15 THEN 
				SELECT HOME_UNIT_NUMBER INTO LS_UNIT_NUMBER  
				FROM GRANT_CALL_HEADER WHERE GRANT_HEADER_ID = AV_MODULE_ITEM_KEY;
         END IF;
ELSEIF AV_PERMISSION_TYPE = 'RIGHT_IN_DEPT' THEN
          SET LS_UNIT_NUMBER = AV_UNIT_NUMBER;
END IF;
IF AV_PERMISSION_TYPE IN ('RIGHT_IN_A_DOCUMENT','RIGHT_IN_DEPT') THEN
    SELECT COUNT(*) INTO LI_COUNT
    FROM PERSON_ROLES T1
    INNER JOIN ROLE_RIGHTS T2 ON T1.ROLE_ID = T2.ROLE_ID
    INNER JOIN RIGHTS T3 ON T2.RIGHT_ID = T3.RIGHT_ID 
    WHERE T1.PERSON_ID = AV_PERSON_ID
    AND T1.UNIT_NUMBER = LS_UNIT_NUMBER
    AND T3.RIGHT_NAME = AV_RIGHT_NAME;
    IF LI_COUNT > 0 THEN
       RETURN 1;
    END IF;
        SELECT COUNT(*) INTO LI_COUNT
    FROM PERSON_ROLE_RT T1
    WHERE T1.PERSON_ID = AV_PERSON_ID
    AND T1.UNIT_NUMBER = LS_UNIT_NUMBER
    AND T1.RIGHT_NAME = AV_RIGHT_NAME;
    IF LI_COUNT > 0 THEN
       RETURN 1;
    END IF;
	RETURN 0;
ELSEIF AV_PERMISSION_TYPE IN ('RIGHT_IN_ANY_DEPT') THEN
   SELECT COUNT(*) INTO LI_COUNT
    FROM PERSON_ROLES T1
    INNER JOIN ROLE_RIGHTS T2 ON T1.ROLE_ID = T2.ROLE_ID
    INNER JOIN RIGHTS T3 ON T2.RIGHT_ID = T3.RIGHT_ID 
    WHERE T1.PERSON_ID = AV_PERSON_ID
    AND T3.RIGHT_NAME = AV_RIGHT_NAME;
    IF LI_COUNT >= 1 THEN
       RETURN 1;
    END IF;
      SELECT COUNT(*) INTO LI_COUNT
    FROM PERSON_ROLE_RT T1
    WHERE T1.PERSON_ID = AV_PERSON_ID
    AND T1.RIGHT_NAME = AV_RIGHT_NAME;
    IF LI_COUNT >= 1 THEN
       RETURN 1;
    END IF;
	RETURN 0;
END IF;
END
//
