DELIMITER //
CREATE  FUNCTION `FN_CHK_PROGRPT_SUBMIT_RLE`(AV_PROGRESS_REPORT_ID INT
									      ,AV_SUBMITTER_ID VARCHAR(50)
										  ,AV_RLE_ID INT
											) RETURNS varchar(10) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
DECLARE LI_COUNT INT;
			SELECT COUNT(1) 
			  INTO LI_COUNT 
			  FROM PERSON_ROLES  PR
		   WHERE PR.PERSON_ID = AV_SUBMITTER_ID
			 AND PR.ROLE_ID = AV_RLE_ID;
IF LI_COUNT > 0 THEN
	RETURN 'TRUE';
ELSE
	RETURN 'FALSE';
END IF;
END
//
