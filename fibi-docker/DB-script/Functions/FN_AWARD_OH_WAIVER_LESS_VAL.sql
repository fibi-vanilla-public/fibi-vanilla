DELIMITER //
CREATE  FUNCTION `FN_AWARD_OH_WAIVER_LESS_VAL`(
AV_AWARD_ID VARCHAR(11)
) RETURNS varchar(6) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
DECLARE LI_GRANT_HEADER_ID int(11);
DECLARE LI_BUDGET_HEADER_ID int(11);
DECLARE LS_COUNT int(11);
DECLARE LS_FLAG int(11);
DECLARE LI_CUTOFF decimal(10,2);
DECLARE LI_OH_WAIVER_PERCENTAGE decimal(5,2);
DECLARE LI_TOTAL_DIRECT_COST decimal(10,2);
DECLARE LI_TOTAL_INDIRECT_COST decimal(10,2);
DECLARE LI_ATTACHMNT_FLAG INT;
DECLARE LS_RATE_TYPE_CODE VARCHAR(1);
DECLARE LS_RATE_CLASS_CODE VARCHAR(3);
DECLARE LS_GRANT_RATE_CLASS_CODE VARCHAR(10);
DECLARE LS_GRANT_RATE_TYPE_CODE VARCHAR(30);
DECLARE LI_RATE decimal(5,2);
SELECT COUNT(*) INTO LS_COUNT
FROM AWARD WHERE AWARD_ID = AV_AWARD_ID AND GRANT_HEADER_ID IS NOT NULL;
IF LS_COUNT = 0 THEN
RETURN 'TRUE';
END IF;
SELECT GRANT_HEADER_ID INTO LI_GRANT_HEADER_ID
FROM AWARD
WHERE AWARD_ID = AV_AWARD_ID;
SELECT COUNT(*) INTO LS_COUNT
FROM GRANT_CALL_HEADER WHERE GRANT_HEADER_ID = LI_GRANT_HEADER_ID AND RATE_CLASS_CODE IS NOT NULL AND RATE_TYPE_CODE IS NOT NULL;
IF LS_COUNT = 0 THEN
RETURN 'TRUE';
END IF;
SELECT RATE_CLASS_CODE,RATE_TYPE_CODE INTO LS_GRANT_RATE_CLASS_CODE,LS_GRANT_RATE_TYPE_CODE
FROM GRANT_CALL_HEADER WHERE GRANT_HEADER_ID = LI_GRANT_HEADER_ID;
SELECT IFNULL(RATE,0) into LI_RATE
 FROM INSTITUTE_RATES
 WHERE INSTITUTE_RATE_ID IN(SELECT MIN(INSTITUTE_RATE_ID) 
					 FROM  INSTITUTE_RATES
					 WHERE RATE_CLASS_CODE = LS_GRANT_RATE_CLASS_CODE
					 AND RATE_TYPE_CODE = LS_GRANT_RATE_TYPE_CODE
					 AND ON_OFF_CAMPUS_FLAG = 'Y'
					 AND UNIT_NUMBER ='000001');
IF LI_RATE IS NULL THEN
	RETURN 'TRUE';
END IF;
SELECT COUNT(*) INTO LS_COUNT
FROM AWARD_BUDGET_HEADER WHERE AWARD_ID = AV_AWARD_ID;
IF LS_COUNT = 0 THEN
	RETURN 'TRUE';
END IF;
SELECT BUDGET_HEADER_ID INTO LI_BUDGET_HEADER_ID
FROM AWARD_BUDGET_HEADER WHERE AWARD_ID = AV_AWARD_ID AND IS_LATEST_VERSION = 'Y';
SELECT TOTAL_INDIRECT_COST,RATE_CLASS_CODE,RATE_TYPE_CODE  
INTO LI_TOTAL_INDIRECT_COST,LS_RATE_CLASS_CODE,LS_RATE_TYPE_CODE 
FROM AWARD_BUDGET_HEADER WHERE BUDGET_HEADER_ID = LI_BUDGET_HEADER_ID;
SELECT SUM(IFNULL(LINE_ITEM_COST,0)) INTO LI_TOTAL_DIRECT_COST
FROM AWARD_BUDGET_DETAIL  
WHERE BUDGET_PERIOD_ID IN(SELECT BUDGET_PERIOD_ID FROM AWARD_BUDGET_PERIOD WHERE BUDGET_HEADER_ID = LI_BUDGET_HEADER_ID)
AND UPPER(TRIM(COST_ELEMENT)) IN(SELECT UPPER(TRIM(COST_ELEMENT))
					FROM VALID_CE_RATE_TYPES 
					WHERE RATE_CLASS_CODE = LS_RATE_CLASS_CODE
					AND RATE_TYPE_CODE = LS_RATE_TYPE_CODE);
IF ((LI_TOTAL_DIRECT_COST*(LI_RATE/100)) = LI_TOTAL_INDIRECT_COST) THEN
	RETURN 'TRUE';
END IF;
IF (LI_TOTAL_INDIRECT_COST<(LI_TOTAL_DIRECT_COST*(LI_RATE/100)) ) THEN
	RETURN 'FALSE';
END IF;
RETURN 'TRUE';
END
//
