SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `ac_protocol`;
DROP TABLE IF EXISTS `ac_protocol_status`;
DROP TABLE IF EXISTS `ac_protocol_type`;
DROP TABLE IF EXISTS `account_type`;
DROP TABLE IF EXISTS `action_letter_template`;
DROP TABLE IF EXISTS `activity_grantcall_mapping`;
DROP TABLE IF EXISTS `activity_type`;
DROP TABLE IF EXISTS `admin_group`;
DROP TABLE IF EXISTS `agreement_action_log`;
DROP TABLE IF EXISTS `agreement_action_type`;
DROP TABLE IF EXISTS `agreement_association_link`;
DROP TABLE IF EXISTS `agreement_attachment`;
DROP TABLE IF EXISTS `agreement_attachment_file`;
DROP TABLE IF EXISTS `agreement_attachment_status`;
DROP TABLE IF EXISTS `agreement_attachment_type`;
DROP TABLE IF EXISTS `agreement_category`;
DROP TABLE IF EXISTS `agreement_clauses`;
DROP TABLE IF EXISTS `agreement_document_type`;
DROP TABLE IF EXISTS `agreement_field_type`;
DROP TABLE IF EXISTS `agreement_header`;
DROP TABLE IF EXISTS `agreement_link_module`;
DROP TABLE IF EXISTS `agreement_note`;
DROP TABLE IF EXISTS `agreement_note_attachment`;
DROP TABLE IF EXISTS `agreement_note_file_data`;
DROP TABLE IF EXISTS `agreement_people`;
DROP TABLE IF EXISTS `agreement_people_type`;
DROP TABLE IF EXISTS `agreement_place_holder`;
DROP TABLE IF EXISTS `agreement_review_type`;
DROP TABLE IF EXISTS `agreement_section_type`;
DROP TABLE IF EXISTS `agreement_sponsor`;
DROP TABLE IF EXISTS `agreement_sponsor_contact`;
DROP TABLE IF EXISTS `agreement_sponsor_contct_type`;
DROP TABLE IF EXISTS `agreement_sponsor_type`;
DROP TABLE IF EXISTS `agreement_status`;
DROP TABLE IF EXISTS `agreement_type`;
DROP TABLE IF EXISTS `agreement_type_history`;
DROP TABLE IF EXISTS `agreement_type_template`;
DROP TABLE IF EXISTS `agreement_v`;
DROP TABLE IF EXISTS `agreement_workflow_status`;
DROP TABLE IF EXISTS `agreemnt_type_clauses_mapping`;
DROP TABLE IF EXISTS `appointment_type`;
DROP TABLE IF EXISTS `arg_value_lookup`;
DROP TABLE IF EXISTS `attachment`;
DROP TABLE IF EXISTS `audit_log`;
DROP TABLE IF EXISTS `audit_log_config`;
DROP TABLE IF EXISTS `audit_report_type`;
DROP TABLE IF EXISTS `award`;
DROP TABLE IF EXISTS `award_achievements_attachments`;
DROP TABLE IF EXISTS `award_amount_info`;
DROP TABLE IF EXISTS `award_amount_transaction`;
DROP TABLE IF EXISTS `award_amount_transactions_v`;
DROP TABLE IF EXISTS `award_amt_fna_distribution`;
DROP TABLE IF EXISTS `award_amt_trnsctn_history`;
DROP TABLE IF EXISTS `award_approved_equipment`;
DROP TABLE IF EXISTS `award_approved_foreign_travel`;
DROP TABLE IF EXISTS `award_approved_subawards`;
DROP TABLE IF EXISTS `award_assoc_detail`;
DROP TABLE IF EXISTS `award_association`;
DROP TABLE IF EXISTS `award_association_type`;
DROP TABLE IF EXISTS `award_attachment`;
DROP TABLE IF EXISTS `award_attachment_type`;
DROP TABLE IF EXISTS `award_basis_of_payment`;
DROP TABLE IF EXISTS `award_bud_per_det_rate_n_base`;
DROP TABLE IF EXISTS `award_budget_det_cal_amt`;
DROP TABLE IF EXISTS `award_budget_detail`;
DROP TABLE IF EXISTS `award_budget_fund_type`;
DROP TABLE IF EXISTS `award_budget_header`;
DROP TABLE IF EXISTS `award_budget_integration_configuration`;
DROP TABLE IF EXISTS `award_budget_non_person_detail`;
DROP TABLE IF EXISTS `award_budget_period`;
DROP TABLE IF EXISTS `award_budget_person_det_ca_amt`;
DROP TABLE IF EXISTS `award_budget_person_detail`;
DROP TABLE IF EXISTS `award_budget_persons`;
DROP TABLE IF EXISTS `award_budget_rate_and_base`;
DROP TABLE IF EXISTS `award_budget_status`;
DROP TABLE IF EXISTS `award_claim_report_v`;
DROP TABLE IF EXISTS `award_comment`;
DROP TABLE IF EXISTS `award_contact`;
DROP TABLE IF EXISTS `award_contact_type`;
DROP TABLE IF EXISTS `award_cost_share`;
DROP TABLE IF EXISTS `award_dmp_dataset_rt`;
DROP TABLE IF EXISTS `award_document_status`;
DROP TABLE IF EXISTS `award_document_type`;
DROP TABLE IF EXISTS `award_error_log`;
DROP TABLE IF EXISTS `award_exp_transactions_rt_log`;
DROP TABLE IF EXISTS `award_expense_details`;
DROP TABLE IF EXISTS `award_expense_details_ext`;
DROP TABLE IF EXISTS `award_expense_error_log`;
DROP TABLE IF EXISTS `award_expense_files`;
DROP TABLE IF EXISTS `award_expense_header`;
DROP TABLE IF EXISTS `award_expense_report_v`;
DROP TABLE IF EXISTS `award_expense_trans_amt_rt`;
DROP TABLE IF EXISTS `award_expense_transactions`;
DROP TABLE IF EXISTS `award_expense_transactions_rt`;
DROP TABLE IF EXISTS `award_funding_proposals`;
DROP TABLE IF EXISTS `award_hierarchy`;
DROP TABLE IF EXISTS `award_history_log`;
DROP TABLE IF EXISTS `award_hours_log_rt`;
DROP TABLE IF EXISTS `award_hours_logged`;
DROP TABLE IF EXISTS `award_keyperson_timesheet`;
DROP TABLE IF EXISTS `award_kpi`;
DROP TABLE IF EXISTS `award_kpi_criteria`;
DROP TABLE IF EXISTS `award_latest_aproved_budget_rt`;
DROP TABLE IF EXISTS `award_manpower`;
DROP TABLE IF EXISTS `award_manpower_compr_report_v`;
DROP TABLE IF EXISTS `award_manpower_new_hire_report_v`;
DROP TABLE IF EXISTS `award_manpower_payroll`;
DROP TABLE IF EXISTS `award_manpower_payroll_v`;
DROP TABLE IF EXISTS `award_manpower_report_v`;
DROP TABLE IF EXISTS `award_manpower_resource`;
DROP TABLE IF EXISTS `award_master_dataset_rt`;
DROP TABLE IF EXISTS `award_master_dataset_rt_v`;
DROP TABLE IF EXISTS `award_method_of_payment`;
DROP TABLE IF EXISTS `award_milestone`;
DROP TABLE IF EXISTS `award_mv`;
DROP TABLE IF EXISTS `award_nextvalue`;
DROP TABLE IF EXISTS `award_orcid_report_v`;
DROP TABLE IF EXISTS `award_org_approved_budget_rt`;
DROP TABLE IF EXISTS `award_original_l2_amt_rt`;
DROP TABLE IF EXISTS `award_person_attachmnt`;
DROP TABLE IF EXISTS `award_person_orcid_work`;
DROP TABLE IF EXISTS `award_person_roles`;
DROP TABLE IF EXISTS `award_person_unit`;
DROP TABLE IF EXISTS `award_persons`;
DROP TABLE IF EXISTS `award_progress_report`;
DROP TABLE IF EXISTS `award_progress_report_achievement`;
DROP TABLE IF EXISTS `award_progress_report_attachment`;
DROP TABLE IF EXISTS `award_progress_report_kpi_summary`;
DROP TABLE IF EXISTS `award_progress_report_milestone`;
DROP TABLE IF EXISTS `award_project_team`;
DROP TABLE IF EXISTS `award_publications`;
DROP TABLE IF EXISTS `award_rates`;
DROP TABLE IF EXISTS `award_report_recpnt_notify_log`;
DROP TABLE IF EXISTS `award_report_reminder`;
DROP TABLE IF EXISTS `award_report_requirement_report_v`;
DROP TABLE IF EXISTS `award_report_term_recipient`;
DROP TABLE IF EXISTS `award_report_terms`;
DROP TABLE IF EXISTS `award_report_tracking`;
DROP TABLE IF EXISTS `award_report_tracking_file`;
DROP TABLE IF EXISTS `award_research_areas`;
DROP TABLE IF EXISTS `award_research_type`;
DROP TABLE IF EXISTS `award_revenue_details`;
DROP TABLE IF EXISTS `award_revenue_files`;
DROP TABLE IF EXISTS `award_revenue_transactions`;
DROP TABLE IF EXISTS `award_revenue_transactions_rt`;
DROP TABLE IF EXISTS `award_review_comment`;
DROP TABLE IF EXISTS `award_science_keyword`;
DROP TABLE IF EXISTS `award_scopus`;
DROP TABLE IF EXISTS `award_section_type`;
DROP TABLE IF EXISTS `award_special_review`;
DROP TABLE IF EXISTS `award_sponsor_term`;
DROP TABLE IF EXISTS `award_status`;
DROP TABLE IF EXISTS `award_sup_org_mapping`;
DROP TABLE IF EXISTS `award_task_type_mapping`;
DROP TABLE IF EXISTS `award_test_view`;
DROP TABLE IF EXISTS `award_transaction_status`;
DROP TABLE IF EXISTS `award_transaction_type`;
DROP TABLE IF EXISTS `award_type`;
DROP TABLE IF EXISTS `award_v`;
DROP TABLE IF EXISTS `award_variation_report_v`;
DROP TABLE IF EXISTS `award_variation_section_maping`;
DROP TABLE IF EXISTS `award_vie`;
DROP TABLE IF EXISTS `award_workflow_status`;
DROP TABLE IF EXISTS `awd_repreq_duedt_log`;
DROP TABLE IF EXISTS `birt_download_option`;
DROP TABLE IF EXISTS `birt_template`;
DROP TABLE IF EXISTS `budget_api_response`;
DROP TABLE IF EXISTS `budget_category`;
DROP TABLE IF EXISTS `budget_category_type`;
DROP TABLE IF EXISTS `budget_det_cal_amt`;
DROP TABLE IF EXISTS `budget_detail`;
DROP TABLE IF EXISTS `budget_header`;
DROP TABLE IF EXISTS `budget_modular`;
DROP TABLE IF EXISTS `budget_modular_idc`;
DROP TABLE IF EXISTS `budget_per_det_rate_and_base`;
DROP TABLE IF EXISTS `budget_period`;
DROP TABLE IF EXISTS `budget_person_det_cal_amt`;
DROP TABLE IF EXISTS `budget_person_detail`;
DROP TABLE IF EXISTS `budget_persons`;
DROP TABLE IF EXISTS `budget_rate_and_base`;
DROP TABLE IF EXISTS `budget_status`;
DROP TABLE IF EXISTS `budget_template`;
DROP TABLE IF EXISTS `budget_template_type`;
DROP TABLE IF EXISTS `budget_type`;
DROP TABLE IF EXISTS `business_rule_function`;
DROP TABLE IF EXISTS `business_rule_function_arg`;
DROP TABLE IF EXISTS `business_rule_log`;
DROP TABLE IF EXISTS `business_rule_variable`;
DROP TABLE IF EXISTS `business_rules`;
DROP TABLE IF EXISTS `business_rules_exp_type`;
DROP TABLE IF EXISTS `business_rules_experssion`;
DROP TABLE IF EXISTS `canned_cost_element`;
DROP TABLE IF EXISTS `claim`;
DROP TABLE IF EXISTS `claim_action_log`;
DROP TABLE IF EXISTS `claim_age_report_v`;
DROP TABLE IF EXISTS `claim_attachment`;
DROP TABLE IF EXISTS `claim_attachment_type`;
DROP TABLE IF EXISTS `claim_configuration_data`;
DROP TABLE IF EXISTS `claim_files`;
DROP TABLE IF EXISTS `claim_funding_scheme`;
DROP TABLE IF EXISTS `claim_gl_account_code`;
DROP TABLE IF EXISTS `claim_invoice`;
DROP TABLE IF EXISTS `claim_invoice_age_report_v`;
DROP TABLE IF EXISTS `claim_invoice_details`;
DROP TABLE IF EXISTS `claim_invoice_details_log`;
DROP TABLE IF EXISTS `claim_invoice_feed_type`;
DROP TABLE IF EXISTS `claim_invoice_log`;
DROP TABLE IF EXISTS `claim_invoice_metadata`;
DROP TABLE IF EXISTS `claim_letter_template_mapping`;
DROP TABLE IF EXISTS `claim_manpower`;
DROP TABLE IF EXISTS `claim_master_dataset_rt`;
DROP TABLE IF EXISTS `claim_master_dataset_rt_v`;
DROP TABLE IF EXISTS `claim_number_manual_generator`;
DROP TABLE IF EXISTS `claim_number_system_generator`;
DROP TABLE IF EXISTS `claim_output_gst_tax_code`;
DROP TABLE IF EXISTS `claim_pjt_end_not_cls_report_v`;
DROP TABLE IF EXISTS `claim_pjt_end_not_ivc_report_v`;
DROP TABLE IF EXISTS `claim_status`;
DROP TABLE IF EXISTS `claim_status_report_v`;
DROP TABLE IF EXISTS `claim_summary`;
DROP TABLE IF EXISTS `claim_summary_details`;
DROP TABLE IF EXISTS `claim_templates`;
DROP TABLE IF EXISTS `clauses`;
DROP TABLE IF EXISTS `clauses_bank`;
DROP TABLE IF EXISTS `clauses_group`;
DROP TABLE IF EXISTS `code_table_configuration`;
DROP TABLE IF EXISTS `coeus_module`;
DROP TABLE IF EXISTS `coeus_sub_module`;
DROP TABLE IF EXISTS `coi_assignee_type`;
DROP TABLE IF EXISTS `coi_conflict_history`;
DROP TABLE IF EXISTS `coi_conflict_status_type`;
DROP TABLE IF EXISTS `coi_discl_ent_proj_details`;
DROP TABLE IF EXISTS `coi_disclosure`;
DROP TABLE IF EXISTS `coi_disclosure_fcoi_type`;
DROP TABLE IF EXISTS `coi_disclosure_type`;
DROP TABLE IF EXISTS `coi_disposition_status_type`;
DROP TABLE IF EXISTS `coi_proj_conflict_status_type`;
DROP TABLE IF EXISTS `coi_project_award`;
DROP TABLE IF EXISTS `coi_project_award_v`;
DROP TABLE IF EXISTS `coi_project_proposal`;
DROP TABLE IF EXISTS `coi_project_proposal_v`;
DROP TABLE IF EXISTS `coi_project_type`;
DROP TABLE IF EXISTS `coi_review`;
DROP TABLE IF EXISTS `coi_review_activity`;
DROP TABLE IF EXISTS `coi_review_assignee_history`;
DROP TABLE IF EXISTS `coi_review_comment`;
DROP TABLE IF EXISTS `coi_review_comment_attachment`;
DROP TABLE IF EXISTS `coi_review_comment_tags`;
DROP TABLE IF EXISTS `coi_review_status_history`;
DROP TABLE IF EXISTS `coi_review_status_type`;
DROP TABLE IF EXISTS `coi_risk_category`;
DROP TABLE IF EXISTS `coi_risk_category_type`;
DROP TABLE IF EXISTS `coi_sections_type`;
DROP TABLE IF EXISTS `coi_travel_disclosure`;
DROP TABLE IF EXISTS `coi_travel_disclosure_traveler`;
DROP TABLE IF EXISTS `coi_travel_status_type`;
DROP TABLE IF EXISTS `coi_traveler_type`;
DROP TABLE IF EXISTS `coi_with_person`;
DROP TABLE IF EXISTS `column_lookup`;
DROP TABLE IF EXISTS `comment_type`;
DROP TABLE IF EXISTS `concur_staff_travel_dtls`;
DROP TABLE IF EXISTS `congressional_district`;
DROP TABLE IF EXISTS `contact_role_type`;
DROP TABLE IF EXISTS `cost_element`;
DROP TABLE IF EXISTS `cost_element_job_profile_mapping`;
DROP TABLE IF EXISTS `cost_element_rate`;
DROP TABLE IF EXISTS `cost_share_type`;
DROP TABLE IF EXISTS `cost_sharing_type`;
DROP TABLE IF EXISTS `country`;
DROP TABLE IF EXISTS `cp_report_header`;
DROP TABLE IF EXISTS `cp_report_project_details`;
DROP TABLE IF EXISTS `cp_report_project_details_ext`;
DROP TABLE IF EXISTS `currency`;
DROP TABLE IF EXISTS `currency_format`;
DROP TABLE IF EXISTS `currency_rate`;
DROP TABLE IF EXISTS `custom_data`;
DROP TABLE IF EXISTS `custom_data_element_usage`;
DROP TABLE IF EXISTS `custom_data_elements`;
DROP TABLE IF EXISTS `custom_data_elements_data_type`;
DROP TABLE IF EXISTS `custom_data_elements_options`;
DROP TABLE IF EXISTS `custom_data_report_rt`;
DROP TABLE IF EXISTS `custom_data_v`;
DROP TABLE IF EXISTS `custom_data_view`;
DROP TABLE IF EXISTS `degree_type`;
DROP TABLE IF EXISTS `delegations`;
DROP TABLE IF EXISTS `delegations_status`;
DROP TABLE IF EXISTS `delete_workday_resource`;
DROP TABLE IF EXISTS `discl_atta_status`;
DROP TABLE IF EXISTS `discl_atta_type`;
DROP TABLE IF EXISTS `discl_attachment`;
DROP TABLE IF EXISTS `discl_comment`;
DROP TABLE IF EXISTS `discl_comment_type`;
DROP TABLE IF EXISTS `discl_component_type`;
DROP TABLE IF EXISTS `discl_file_data`;
DROP TABLE IF EXISTS `discl_valid_compnent_atta_ty`;
DROP TABLE IF EXISTS `discl_valid_compnent_comnt_ty`;
DROP TABLE IF EXISTS `disclosure_mv`;
DROP TABLE IF EXISTS `distribution`;
DROP TABLE IF EXISTS `document_status`;
DROP TABLE IF EXISTS `dyn_modules_config`;
DROP TABLE IF EXISTS `dyn_section_config`;
DROP TABLE IF EXISTS `dyn_subsection_config`;
DROP TABLE IF EXISTS `elastic_award_v`;
DROP TABLE IF EXISTS `elastic_fibi_proposal_v`;
DROP TABLE IF EXISTS `elastic_institute_proposal_v`;
DROP TABLE IF EXISTS `entity`;
DROP TABLE IF EXISTS `entity_rel_node_type`;
DROP TABLE IF EXISTS `entity_relationship`;
DROP TABLE IF EXISTS `entity_relationship_type`;
DROP TABLE IF EXISTS `entity_risk_category`;
DROP TABLE IF EXISTS `entity_status`;
DROP TABLE IF EXISTS `entity_type`;
DROP TABLE IF EXISTS `eps_prop_discipline_cluster`;
DROP TABLE IF EXISTS `eps_prop_evalpanel_persons`;
DROP TABLE IF EXISTS `eps_prop_evaluation_score`;
DROP TABLE IF EXISTS `eps_prop_evaluation_stop`;
DROP TABLE IF EXISTS `eps_prop_evaluatn_status_flow`;
DROP TABLE IF EXISTS `eps_prop_person_role`;
DROP TABLE IF EXISTS `eps_prop_person_units`;
DROP TABLE IF EXISTS `eps_prop_pre_review`;
DROP TABLE IF EXISTS `eps_prop_pre_review_attachment`;
DROP TABLE IF EXISTS `eps_prop_pre_review_comment`;
DROP TABLE IF EXISTS `eps_proposal`;
DROP TABLE IF EXISTS `eps_proposal_attach_type`;
DROP TABLE IF EXISTS `eps_proposal_attachments`;
DROP TABLE IF EXISTS `eps_proposal_comment_attachments`;
DROP TABLE IF EXISTS `eps_proposal_comments`;
DROP TABLE IF EXISTS `eps_proposal_cong_district`;
DROP TABLE IF EXISTS `eps_proposal_document_status`;
DROP TABLE IF EXISTS `eps_proposal_evaluation_panel`;
DROP TABLE IF EXISTS `eps_proposal_ext`;
DROP TABLE IF EXISTS `eps_proposal_history`;
DROP TABLE IF EXISTS `eps_proposal_irb_protocol`;
DROP TABLE IF EXISTS `eps_proposal_key_personnel_attach_type`;
DROP TABLE IF EXISTS `eps_proposal_keywords`;
DROP TABLE IF EXISTS `eps_proposal_kpi`;
DROP TABLE IF EXISTS `eps_proposal_kpi_criteria`;
DROP TABLE IF EXISTS `eps_proposal_milestone`;
DROP TABLE IF EXISTS `eps_proposal_organization`;
DROP TABLE IF EXISTS `eps_proposal_person_attachmnt`;
DROP TABLE IF EXISTS `eps_proposal_person_degree`;
DROP TABLE IF EXISTS `eps_proposal_person_roles`;
DROP TABLE IF EXISTS `eps_proposal_persons`;
DROP TABLE IF EXISTS `eps_proposal_project_team`;
DROP TABLE IF EXISTS `eps_proposal_rates`;
DROP TABLE IF EXISTS `eps_proposal_report_v`;
DROP TABLE IF EXISTS `eps_proposal_resrch_areas`;
DROP TABLE IF EXISTS `eps_proposal_resrch_type`;
DROP TABLE IF EXISTS `eps_proposal_special_review`;
DROP TABLE IF EXISTS `eps_proposal_sponsors`;
DROP TABLE IF EXISTS `eps_proposal_status`;
DROP TABLE IF EXISTS `eps_proposal_type`;
DROP TABLE IF EXISTS `eps_proposal_v`;
DROP TABLE IF EXISTS `eps_reviewers`;
DROP TABLE IF EXISTS `era_proposal_person_roles`;
DROP TABLE IF EXISTS `error_details`;
DROP TABLE IF EXISTS `evaluation_recommendation`;
DROP TABLE IF EXISTS `excellence_area`;
DROP TABLE IF EXISTS `exception_log`;
DROP TABLE IF EXISTS `expense_tracker_rt`;
DROP TABLE IF EXISTS `expense_zero_exclude_commit_v`;
DROP TABLE IF EXISTS `ext_rev_comment_attachment`;
DROP TABLE IF EXISTS `ext_rev_reviewer_flow_status`;
DROP TABLE IF EXISTS `ext_rev_reviewer_score`;
DROP TABLE IF EXISTS `ext_rev_reviewer_score_comment`;
DROP TABLE IF EXISTS `ext_rev_revwr_score_commnt_attchmnt`;
DROP TABLE IF EXISTS `ext_review`;
DROP TABLE IF EXISTS `ext_review_action_type`;
DROP TABLE IF EXISTS `ext_review_attachment_file`;
DROP TABLE IF EXISTS `ext_review_attachment_type`;
DROP TABLE IF EXISTS `ext_review_attachments`;
DROP TABLE IF EXISTS `ext_review_comment_type`;
DROP TABLE IF EXISTS `ext_review_comments`;
DROP TABLE IF EXISTS `ext_review_history`;
DROP TABLE IF EXISTS `ext_review_questionnaire`;
DROP TABLE IF EXISTS `ext_review_reviewers`;
DROP TABLE IF EXISTS `ext_review_reviewers_status`;
DROP TABLE IF EXISTS `ext_review_scoring_criteria`;
DROP TABLE IF EXISTS `ext_review_service_type`;
DROP TABLE IF EXISTS `ext_review_status`;
DROP TABLE IF EXISTS `ext_reviewer_academic_area`;
DROP TABLE IF EXISTS `ext_reviewer_academic_rank`;
DROP TABLE IF EXISTS `ext_reviewer_academic_sub_area`;
DROP TABLE IF EXISTS `ext_reviewer_affiliation`;
DROP TABLE IF EXISTS `ext_reviewer_attachment_type`;
DROP TABLE IF EXISTS `ext_reviewer_cira`;
DROP TABLE IF EXISTS `ext_reviewer_originality`;
DROP TABLE IF EXISTS `ext_reviewer_thoroughness`;
DROP TABLE IF EXISTS `ext_reviewers_status`;
DROP TABLE IF EXISTS `ext_specialism_keyword`;
DROP TABLE IF EXISTS `external_reviewer`;
DROP TABLE IF EXISTS `external_reviewer_attachments`;
DROP TABLE IF EXISTS `external_reviewer_attachments_file`;
DROP TABLE IF EXISTS `external_reviewer_ext`;
DROP TABLE IF EXISTS `external_reviewer_rights`;
DROP TABLE IF EXISTS `external_reviewer_specialization`;
DROP TABLE IF EXISTS `external_user`;
DROP TABLE IF EXISTS `external_user_feed`;
DROP TABLE IF EXISTS `faq`;
DROP TABLE IF EXISTS `faq_attachment`;
DROP TABLE IF EXISTS `faq_category`;
DROP TABLE IF EXISTS `faq_sub_category`;
DROP TABLE IF EXISTS `feed_award_details`;
DROP TABLE IF EXISTS `fibi_workflow`;
DROP TABLE IF EXISTS `fibi_workflow_attachment`;
DROP TABLE IF EXISTS `fibi_workflow_detail`;
DROP TABLE IF EXISTS `fibi_workflow_map`;
DROP TABLE IF EXISTS `fibi_workflow_map_detail`;
DROP TABLE IF EXISTS `fibi_workflow_reviewer_detail`;
DROP TABLE IF EXISTS `fibi_workflow_role_type`;
DROP TABLE IF EXISTS `fibi_workflow_status`;
DROP TABLE IF EXISTS `field_section_mapping`;
DROP TABLE IF EXISTS `file_data`;
DROP TABLE IF EXISTS `file_type`;
DROP TABLE IF EXISTS `final_evaluation_status`;
DROP TABLE IF EXISTS `frequency`;
DROP TABLE IF EXISTS `frequency_base`;
DROP TABLE IF EXISTS `fund_disbursement_basis_type`;
DROP TABLE IF EXISTS `funding_scheme`;
DROP TABLE IF EXISTS `funding_scheme_attachment`;
DROP TABLE IF EXISTS `funding_source_type`;
DROP TABLE IF EXISTS `grant_call_attach_type`;
DROP TABLE IF EXISTS `grant_call_attachments`;
DROP TABLE IF EXISTS `grant_call_category`;
DROP TABLE IF EXISTS `grant_call_contacts`;
DROP TABLE IF EXISTS `grant_call_criteria`;
DROP TABLE IF EXISTS `grant_call_elgiblity_type`;
DROP TABLE IF EXISTS `grant_call_eligibility`;
DROP TABLE IF EXISTS `grant_call_eligible_department`;
DROP TABLE IF EXISTS `grant_call_evaluation_panel`;
DROP TABLE IF EXISTS `grant_call_funding_scheme_managers`;
DROP TABLE IF EXISTS `grant_call_header`;
DROP TABLE IF EXISTS `grant_call_ioi_header`;
DROP TABLE IF EXISTS `grant_call_ioi_members`;
DROP TABLE IF EXISTS `grant_call_ioi_questionnaire`;
DROP TABLE IF EXISTS `grant_call_keywords`;
DROP TABLE IF EXISTS `grant_call_kpi`;
DROP TABLE IF EXISTS `grant_call_kpi_criteria`;
DROP TABLE IF EXISTS `grant_call_relevant_field`;
DROP TABLE IF EXISTS `grant_call_report_v`;
DROP TABLE IF EXISTS `grant_call_research_areas`;
DROP TABLE IF EXISTS `grant_call_research_type`;
DROP TABLE IF EXISTS `grant_call_status`;
DROP TABLE IF EXISTS `grant_call_type`;
DROP TABLE IF EXISTS `grant_eligibility_target`;
DROP TABLE IF EXISTS `grant_eligibility_target_type`;
DROP TABLE IF EXISTS `grant_scoring_criteria`;
DROP TABLE IF EXISTS `grantcall_action_log`;
DROP TABLE IF EXISTS `grantcall_action_type`;
DROP TABLE IF EXISTS `help_text`;
DROP TABLE IF EXISTS `ics_student_travel_dtls`;
DROP TABLE IF EXISTS `inbox`;
DROP TABLE IF EXISTS `institute_la_rates`;
DROP TABLE IF EXISTS `institute_rates`;
DROP TABLE IF EXISTS `ioi_status`;
DROP TABLE IF EXISTS `ip_budget_header`;
DROP TABLE IF EXISTS `ip_budget_period`;
DROP TABLE IF EXISTS `irb_protocol`;
DROP TABLE IF EXISTS `irb_protocol_status`;
DROP TABLE IF EXISTS `irb_protocol_type`;
DROP TABLE IF EXISTS `job_code`;
DROP TABLE IF EXISTS `keyperson_timesheet_v`;
DROP TABLE IF EXISTS `kpi_criteria_type`;
DROP TABLE IF EXISTS `kpi_manpower_development_current_status`;
DROP TABLE IF EXISTS `kpi_publication_status`;
DROP TABLE IF EXISTS `kpi_technology_disclosure_status`;
DROP TABLE IF EXISTS `kpi_template`;
DROP TABLE IF EXISTS `kpi_template_header`;
DROP TABLE IF EXISTS `kpi_type`;
DROP TABLE IF EXISTS `krcr_parm_t`;
DROP TABLE IF EXISTS `letter_template_type`;
DROP TABLE IF EXISTS `locale`;
DROP TABLE IF EXISTS `lock_configuration`;
DROP TABLE IF EXISTS `lookup_window`;
DROP TABLE IF EXISTS `manpower`;
DROP TABLE IF EXISTS `manpower_base_salary_history`;
DROP TABLE IF EXISTS `manpower_budget_reference_type`;
DROP TABLE IF EXISTS `manpower_candidate_title_type`;
DROP TABLE IF EXISTS `manpower_compensation_type`;
DROP TABLE IF EXISTS `manpower_configuration_data`;
DROP TABLE IF EXISTS `manpower_interface_status`;
DROP TABLE IF EXISTS `manpower_interface_type`;
DROP TABLE IF EXISTS `manpower_job_profile_type`;
DROP TABLE IF EXISTS `manpower_log`;
DROP TABLE IF EXISTS `manpower_log_user`;
DROP TABLE IF EXISTS `manpower_person_activity_log`;
DROP TABLE IF EXISTS `manpower_position_status`;
DROP TABLE IF EXISTS `manpower_resource_type`;
DROP TABLE IF EXISTS `manpower_temp`;
DROP TABLE IF EXISTS `manpower_type`;
DROP TABLE IF EXISTS `manpower_upgrade_type`;
DROP TABLE IF EXISTS `manpower_user_actions`;
DROP TABLE IF EXISTS `manpower_workday_resource`;
DROP TABLE IF EXISTS `message`;
DROP TABLE IF EXISTS `meta_rule_detail`;
DROP TABLE IF EXISTS `meta_rules`;
DROP TABLE IF EXISTS `migration_attachment_error_log`;
DROP TABLE IF EXISTS `migration_manpower_person`;
DROP TABLE IF EXISTS `milestone_status`;
DROP TABLE IF EXISTS `mitkc_unit_with_children`;
DROP TABLE IF EXISTS `module_derived_roles`;
DROP TABLE IF EXISTS `module_variable_section`;
DROP TABLE IF EXISTS `narrative_status`;
DROP TABLE IF EXISTS `negotiation`;
DROP TABLE IF EXISTS `negotiation_activity`;
DROP TABLE IF EXISTS `negotiation_activity_type`;
DROP TABLE IF EXISTS `negotiation_agreement_type`;
DROP TABLE IF EXISTS `negotiation_agreement_value`;
DROP TABLE IF EXISTS `negotiation_assoc_detail`;
DROP TABLE IF EXISTS `negotiation_association`;
DROP TABLE IF EXISTS `negotiation_association_type`;
DROP TABLE IF EXISTS `negotiation_attachment`;
DROP TABLE IF EXISTS `negotiation_attachment_type`;
DROP TABLE IF EXISTS `negotiation_comment`;
DROP TABLE IF EXISTS `negotiation_comment_type`;
DROP TABLE IF EXISTS `negotiation_location`;
DROP TABLE IF EXISTS `negotiation_location_status`;
DROP TABLE IF EXISTS `negotiation_location_type`;
DROP TABLE IF EXISTS `negotiation_negotiator_histry`;
DROP TABLE IF EXISTS `negotiation_personnel`;
DROP TABLE IF EXISTS `negotiation_personnel_type`;
DROP TABLE IF EXISTS `negotiation_status`;
DROP TABLE IF EXISTS `negotiation_status_histry`;
DROP TABLE IF EXISTS `negotiation_workflow_status`;
DROP TABLE IF EXISTS `notification_log`;
DROP TABLE IF EXISTS `notification_log_recipient`;
DROP TABLE IF EXISTS `notification_recipient`;
DROP TABLE IF EXISTS `notification_type`;
DROP TABLE IF EXISTS `orcid_error_log`;
DROP TABLE IF EXISTS `orcid_parameter`;
DROP TABLE IF EXISTS `orcid_webhook_action_types`;
DROP TABLE IF EXISTS `orcid_webhook_notify_log`;
DROP TABLE IF EXISTS `orcid_work`;
DROP TABLE IF EXISTS `orcid_work_category`;
DROP TABLE IF EXISTS `orcid_work_citation_type`;
DROP TABLE IF EXISTS `orcid_work_contributor`;
DROP TABLE IF EXISTS `orcid_work_external_identifier`;
DROP TABLE IF EXISTS `orcid_work_status`;
DROP TABLE IF EXISTS `orcid_work_type`;
DROP TABLE IF EXISTS `organization`;
DROP TABLE IF EXISTS `organization_type`;
DROP TABLE IF EXISTS `parameter`;
DROP TABLE IF EXISTS `person`;
DROP TABLE IF EXISTS `person_degree`;
DROP TABLE IF EXISTS `person_entity`;
DROP TABLE IF EXISTS `person_entity_rel_type`;
DROP TABLE IF EXISTS `person_entity_relationship`;
DROP TABLE IF EXISTS `person_login_details`;
DROP TABLE IF EXISTS `person_orcid_webhook_flag`;
DROP TABLE IF EXISTS `person_orcid_work`;
DROP TABLE IF EXISTS `person_preference`;
DROP TABLE IF EXISTS `person_role_module`;
DROP TABLE IF EXISTS `person_role_rt`;
DROP TABLE IF EXISTS `person_role_type`;
DROP TABLE IF EXISTS `person_roles`;
DROP TABLE IF EXISTS `person_rt`;
DROP TABLE IF EXISTS `person_system_notification_mapping`;
DROP TABLE IF EXISTS `person_training`;
DROP TABLE IF EXISTS `person_training_attachment`;
DROP TABLE IF EXISTS `person_training_comment`;
DROP TABLE IF EXISTS `person_v`;
DROP TABLE IF EXISTS `pi_sup_org_mapping`;
DROP TABLE IF EXISTS `pre_award_summary_report_v`;
DROP TABLE IF EXISTS `pre_review`;
DROP TABLE IF EXISTS `pre_review_attachment`;
DROP TABLE IF EXISTS `pre_review_attachment_file`;
DROP TABLE IF EXISTS `pre_review_comment`;
DROP TABLE IF EXISTS `pre_review_section_type`;
DROP TABLE IF EXISTS `pre_review_status`;
DROP TABLE IF EXISTS `pre_review_type`;
DROP TABLE IF EXISTS `preference_type`;
DROP TABLE IF EXISTS `progress_report_achievement_type`;
DROP TABLE IF EXISTS `progress_report_attachmnt_type`;
DROP TABLE IF EXISTS `progress_report_kpi_cash_funding`;
DROP TABLE IF EXISTS `progress_report_kpi_collaboration_projects`;
DROP TABLE IF EXISTS `progress_report_kpi_competitive_grants`;
DROP TABLE IF EXISTS `progress_report_kpi_conference_presentation`;
DROP TABLE IF EXISTS `progress_report_kpi_grant_specific`;
DROP TABLE IF EXISTS `progress_report_kpi_health_specific_outcomes`;
DROP TABLE IF EXISTS `progress_report_kpi_impact_publications`;
DROP TABLE IF EXISTS `progress_report_kpi_inkind_contributions`;
DROP TABLE IF EXISTS `progress_report_kpi_licenses`;
DROP TABLE IF EXISTS `progress_report_kpi_manpower_development`;
DROP TABLE IF EXISTS `progress_report_kpi_mapping`;
DROP TABLE IF EXISTS `progress_report_kpi_patents`;
DROP TABLE IF EXISTS `progress_report_kpi_post_docs_employed`;
DROP TABLE IF EXISTS `progress_report_kpi_successful_startups`;
DROP TABLE IF EXISTS `progress_report_kpi_technologies_deployed`;
DROP TABLE IF EXISTS `progress_report_kpi_technology_disclosure`;
DROP TABLE IF EXISTS `progress_report_kpi_undergraduate_student`;
DROP TABLE IF EXISTS `progress_report_status`;
DROP TABLE IF EXISTS `progress_report_v`;
DROP TABLE IF EXISTS `progress_reprt_milestone_status`;
DROP TABLE IF EXISTS `prop_person_units`;
DROP TABLE IF EXISTS `proposal`;
DROP TABLE IF EXISTS `proposal_action_item`;
DROP TABLE IF EXISTS `proposal_action_log`;
DROP TABLE IF EXISTS `proposal_action_type`;
DROP TABLE IF EXISTS `proposal_admin_details`;
DROP TABLE IF EXISTS `proposal_attach_type`;
DROP TABLE IF EXISTS `proposal_attachments`;
DROP TABLE IF EXISTS `proposal_comments`;
DROP TABLE IF EXISTS `proposal_funding_status`;
DROP TABLE IF EXISTS `proposal_keywords`;
DROP TABLE IF EXISTS `proposal_persons`;
DROP TABLE IF EXISTS `proposal_persons_attachmnt`;
DROP TABLE IF EXISTS `proposal_project_team`;
DROP TABLE IF EXISTS `proposal_resrch_areas`;
DROP TABLE IF EXISTS `proposal_resrch_type`;
DROP TABLE IF EXISTS `proposal_review`;
DROP TABLE IF EXISTS `proposal_review_attachment`;
DROP TABLE IF EXISTS `proposal_review_comment`;
DROP TABLE IF EXISTS `proposal_special_review`;
DROP TABLE IF EXISTS `proposal_sponsors`;
DROP TABLE IF EXISTS `proposal_status`;
DROP TABLE IF EXISTS `proposal_type`;
DROP TABLE IF EXISTS `proposal_version_history`;
DROP TABLE IF EXISTS `publication`;
DROP TABLE IF EXISTS `quest_answer`;
DROP TABLE IF EXISTS `quest_answer_attachment`;
DROP TABLE IF EXISTS `quest_answer_header`;
DROP TABLE IF EXISTS `quest_column_nextvalue`;
DROP TABLE IF EXISTS `quest_group_type`;
DROP TABLE IF EXISTS `quest_header`;
DROP TABLE IF EXISTS `quest_question`;
DROP TABLE IF EXISTS `quest_question_condition`;
DROP TABLE IF EXISTS `quest_question_option`;
DROP TABLE IF EXISTS `quest_table_answer`;
DROP TABLE IF EXISTS `quest_usage`;
DROP TABLE IF EXISTS `quick_links`;
DROP TABLE IF EXISTS `rate_class`;
DROP TABLE IF EXISTS `rate_class_type`;
DROP TABLE IF EXISTS `rate_type`;
DROP TABLE IF EXISTS `relevant_field`;
DROP TABLE IF EXISTS `remainder_notification`;
DROP TABLE IF EXISTS `reminder_notification`;
DROP TABLE IF EXISTS `remote_detail`;
DROP TABLE IF EXISTS `report`;
DROP TABLE IF EXISTS `report_class`;
DROP TABLE IF EXISTS `report_columns`;
DROP TABLE IF EXISTS `report_last_sync_time`;
DROP TABLE IF EXISTS `report_status`;
DROP TABLE IF EXISTS `report_template`;
DROP TABLE IF EXISTS `report_type`;
DROP TABLE IF EXISTS `report_type_join`;
DROP TABLE IF EXISTS `research_areas`;
DROP TABLE IF EXISTS `research_center`;
DROP TABLE IF EXISTS `research_sub_area`;
DROP TABLE IF EXISTS `research_type`;
DROP TABLE IF EXISTS `research_type_area`;
DROP TABLE IF EXISTS `research_type_sub_area`;
DROP TABLE IF EXISTS `review_status`;
DROP TABLE IF EXISTS `review_type`;
DROP TABLE IF EXISTS `reviewer_rights`;
DROP TABLE IF EXISTS `right_section_mapping`;
DROP TABLE IF EXISTS `rights`;
DROP TABLE IF EXISTS `rights_type`;
DROP TABLE IF EXISTS `rise_error_allocations`;
DROP TABLE IF EXISTS `role`;
DROP TABLE IF EXISTS `role_rights`;
DROP TABLE IF EXISTS `role_rights_audit_tab`;
DROP TABLE IF EXISTS `role_type`;
DROP TABLE IF EXISTS `rolodex`;
DROP TABLE IF EXISTS `salary_discrepancy_report_v`;
DROP TABLE IF EXISTS `sap_award_feed`;
DROP TABLE IF EXISTS `sap_award_feed_batch`;
DROP TABLE IF EXISTS `sap_award_feed_batch_error_log`;
DROP TABLE IF EXISTS `sap_award_feed_batch_files`;
DROP TABLE IF EXISTS `sap_claim_feed`;
DROP TABLE IF EXISTS `sap_claim_feed_batch`;
DROP TABLE IF EXISTS `sap_claim_feed_batch_error_log`;
DROP TABLE IF EXISTS `sap_claim_feed_batch_files`;
DROP TABLE IF EXISTS `sap_claim_feed_response_messge`;
DROP TABLE IF EXISTS `sap_concur_files`;
DROP TABLE IF EXISTS `sap_cost_center`;
DROP TABLE IF EXISTS `sap_feed_prob_grantcode_report`;
DROP TABLE IF EXISTS `sap_feed_status`;
DROP TABLE IF EXISTS `sap_feed_tmpl_fm_budget`;
DROP TABLE IF EXISTS `sap_feed_tmpl_funded_prgm`;
DROP TABLE IF EXISTS `sap_feed_tmpl_grant_bud_master`;
DROP TABLE IF EXISTS `sap_feed_tmpl_grant_master`;
DROP TABLE IF EXISTS `sap_feed_tmpl_project_def`;
DROP TABLE IF EXISTS `sap_feed_tmpl_sponsor_class`;
DROP TABLE IF EXISTS `sap_feed_tmpl_sponsor_prgm`;
DROP TABLE IF EXISTS `sap_feed_tmpl_wbs`;
DROP TABLE IF EXISTS `sap_feed_type`;
DROP TABLE IF EXISTS `sap_feed_unit_mapping`;
DROP TABLE IF EXISTS `sap_feed_user_actions`;
DROP TABLE IF EXISTS `sap_fund_center`;
DROP TABLE IF EXISTS `sap_grant_code`;
DROP TABLE IF EXISTS `sap_integration_log`;
DROP TABLE IF EXISTS `sap_profit_center`;
DROP TABLE IF EXISTS `science_keyword`;
DROP TABLE IF EXISTS `scopus`;
DROP TABLE IF EXISTS `scopus_affiliation`;
DROP TABLE IF EXISTS `scopus_author`;
DROP TABLE IF EXISTS `scopus_configuration`;
DROP TABLE IF EXISTS `scopus_metrics`;
DROP TABLE IF EXISTS `scoring_criteria`;
DROP TABLE IF EXISTS `section_module`;
DROP TABLE IF EXISTS `section_type`;
DROP TABLE IF EXISTS `seq_award_progress_report_number`;
DROP TABLE IF EXISTS `seq_ip_id_gntr`;
DROP TABLE IF EXISTS `sftp_configuration_data`;
DROP TABLE IF EXISTS `societal_challenge_area`;
DROP TABLE IF EXISTS `societal_challenge_subarea`;
DROP TABLE IF EXISTS `sp_rev_approval_type`;
DROP TABLE IF EXISTS `special_review`;
DROP TABLE IF EXISTS `special_review_usage`;
DROP TABLE IF EXISTS `sponsor`;
DROP TABLE IF EXISTS `sponsor_funding_scheme`;
DROP TABLE IF EXISTS `sponsor_hierarchy`;
DROP TABLE IF EXISTS `sponsor_report`;
DROP TABLE IF EXISTS `sponsor_role`;
DROP TABLE IF EXISTS `sponsor_term`;
DROP TABLE IF EXISTS `sponsor_term_report`;
DROP TABLE IF EXISTS `sponsor_term_type`;
DROP TABLE IF EXISTS `sponsor_type`;
DROP TABLE IF EXISTS `sr_action_log`;
DROP TABLE IF EXISTS `sr_action_type`;
DROP TABLE IF EXISTS `sr_attachment`;
DROP TABLE IF EXISTS `sr_comments`;
DROP TABLE IF EXISTS `sr_header`;
DROP TABLE IF EXISTS `sr_header_field`;
DROP TABLE IF EXISTS `sr_header_history`;
DROP TABLE IF EXISTS `sr_priority`;
DROP TABLE IF EXISTS `sr_process_flow`;
DROP TABLE IF EXISTS `sr_project`;
DROP TABLE IF EXISTS `sr_reporter_change_history`;
DROP TABLE IF EXISTS `sr_role_type`;
DROP TABLE IF EXISTS `sr_status`;
DROP TABLE IF EXISTS `sr_status_history`;
DROP TABLE IF EXISTS `sr_type`;
DROP TABLE IF EXISTS `sr_watcher`;
DROP TABLE IF EXISTS `states`;
DROP TABLE IF EXISTS `success_rate_view`;
DROP TABLE IF EXISTS `system_notification`;
DROP TABLE IF EXISTS `target_commitment_item`;
DROP TABLE IF EXISTS `task`;
DROP TABLE IF EXISTS `task_action_log`;
DROP TABLE IF EXISTS `task_action_type`;
DROP TABLE IF EXISTS `task_assignee_history`;
DROP TABLE IF EXISTS `task_attachment`;
DROP TABLE IF EXISTS `task_comment_attachment`;
DROP TABLE IF EXISTS `task_comments`;
DROP TABLE IF EXISTS `task_file_data`;
DROP TABLE IF EXISTS `task_module_mapping`;
DROP TABLE IF EXISTS `task_section_mapping`;
DROP TABLE IF EXISTS `task_section_module_mapping`;
DROP TABLE IF EXISTS `task_status`;
DROP TABLE IF EXISTS `task_status_history`;
DROP TABLE IF EXISTS `task_type`;
DROP TABLE IF EXISTS `tbn`;
DROP TABLE IF EXISTS `temp_attachment_migration`;
DROP TABLE IF EXISTS `temp_user`;
DROP TABLE IF EXISTS `template_letter_template_mapping`;
DROP TABLE IF EXISTS `tmp_proposal_special_review`;
DROP TABLE IF EXISTS `training`;
DROP TABLE IF EXISTS `triage_header`;
DROP TABLE IF EXISTS `triage_template_mapping`;
DROP TABLE IF EXISTS `unit`;
DROP TABLE IF EXISTS `unit_administrator`;
DROP TABLE IF EXISTS `unit_administrator_type`;
DROP TABLE IF EXISTS `unit_level`;
DROP TABLE IF EXISTS `unit_tmp`;
DROP TABLE IF EXISTS `unit_with_children`;
DROP TABLE IF EXISTS `user_selected_widget`;
DROP TABLE IF EXISTS `valid_ce_rate_types`;
DROP TABLE IF EXISTS `valid_person_entity_rel_type`;
DROP TABLE IF EXISTS `valid_report_class`;
DROP TABLE IF EXISTS `variation_request_header`;
DROP TABLE IF EXISTS `variation_section_mapping`;
DROP TABLE IF EXISTS `visibility`;
DROP TABLE IF EXISTS `web_socket_con_config`;
DROP TABLE IF EXISTS `widget_lookup`;
DROP TABLE IF EXISTS `workday_configuration_data`;
DROP TABLE IF EXISTS `workday_job_profile_change`;
DROP TABLE IF EXISTS `workday_long_leave_details`;
DROP TABLE IF EXISTS `workday_manpower_interface`;
DROP TABLE IF EXISTS `workday_position_requisition`;
DROP TABLE IF EXISTS `workday_resource_log`;
DROP TABLE IF EXISTS `workday_termination_details`;
DROP TABLE IF EXISTS `workflow`;
DROP TABLE IF EXISTS `workflow_attachment`;
DROP TABLE IF EXISTS `workflow_column_nextvalue`;
DROP TABLE IF EXISTS `workflow_detail`;
DROP TABLE IF EXISTS `workflow_detail_ext`;
DROP TABLE IF EXISTS `workflow_feedback_type`;
DROP TABLE IF EXISTS `workflow_map`;
DROP TABLE IF EXISTS `workflow_map_detail`;
DROP TABLE IF EXISTS `workflow_map_type`;
DROP TABLE IF EXISTS `workflow_reviewer_attmnts`;
DROP TABLE IF EXISTS `workflow_reviewer_comments`;
DROP TABLE IF EXISTS `workflow_reviewer_detail`;
DROP TABLE IF EXISTS `workflow_reviewer_score`;
DROP TABLE IF EXISTS `workflow_role_type`;
DROP TABLE IF EXISTS `workflow_status`;



CREATE TABLE `ac_protocol` (
  `PROTOCOL_ID` int NOT NULL AUTO_INCREMENT,
  `PROTOCOL_NUMBER` varchar(20) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `PROTOCOL_TYPE_CODE` varchar(3) DEFAULT NULL,
  `PROTOCOL_STATUS_CODE` varchar(3) DEFAULT NULL,
  `TITLE` varchar(2000) DEFAULT NULL,
  `PURPOSE_OF_STUDY` varchar(1000) DEFAULT NULL,
  `LEAD_UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `APPROVAL_DATE` datetime DEFAULT NULL,
  `EXPIRATION_DATE` datetime DEFAULT NULL,
  `LAST_APPROVAL_DATE` datetime DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `INITIAL_SUBMISSION_DATE` datetime DEFAULT NULL,
  `FDA_APPLICATION_NUMBER` varchar(15) DEFAULT NULL,
  `REFERENCE_NUMBER_1` varchar(50) DEFAULT NULL,
  `REFERENCE_NUMBER_2` varchar(50) DEFAULT NULL,
  `LAY_STATEMENT_1` varchar(2000) DEFAULT NULL,
  `LAY_STATEMENT_2` varchar(2000) DEFAULT NULL,
  `ACTIVE` varchar(1) DEFAULT NULL,
  `ASSIGNEE_PERSON_ID` varchar(40) DEFAULT NULL,
  `IS_CANCELLED` varchar(1) DEFAULT NULL,
  `IS_HOLD` varchar(1) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `PROJECT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `DS_LEVEL_ID` int DEFAULT NULL,
  `RENEWAL_DUE_DATE` datetime DEFAULT NULL,
  `YEAR` int DEFAULT NULL,
  `SUBMISSION_DUE_DATE` datetime DEFAULT NULL,
  `NON_EMPLOYEE_FLAG` varchar(1) DEFAULT NULL,
  `FUNDING_SOURCE` varchar(200) DEFAULT NULL,
  `FUNDING_SOURCE_TYPE_CODE` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`PROTOCOL_ID`),
  KEY `AC_PROTOCOL_FK1_idx` (`PROTOCOL_TYPE_CODE`),
  KEY `AC_PROTOCOL_FK2_idx` (`PROTOCOL_STATUS_CODE`),
  KEY `AC_PROTOCOL_FK3_idx` (`FUNDING_SOURCE_TYPE_CODE`),
  CONSTRAINT `AC_PROTOCOL_FK1` FOREIGN KEY (`PROTOCOL_TYPE_CODE`) REFERENCES `ac_protocol_type` (`PROTOCOL_TYPE_CODE`),
  CONSTRAINT `AC_PROTOCOL_FK2` FOREIGN KEY (`PROTOCOL_STATUS_CODE`) REFERENCES `ac_protocol_status` (`PROTOCOL_STATUS_CODE`),
  CONSTRAINT `AC_PROTOCOL_FK3` FOREIGN KEY (`FUNDING_SOURCE_TYPE_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ac_protocol_status` (
  `PROTOCOL_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `STATUS_GROUP_CODE` int NOT NULL,
  PRIMARY KEY (`PROTOCOL_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ac_protocol_type` (
  `PROTOCOL_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`PROTOCOL_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `account_type` (
  `ACCOUNT_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`ACCOUNT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `action_letter_template` (
  `ACTION_TYPE_CODE` decimal(3,0) NOT NULL,
  `LETTER_TEMPLATE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `MODULE_CODE` decimal(3,0) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ACTION_TYPE_CODE`),
  KEY `ACTION_LETTER_TEMPLATE_FK1` (`LETTER_TEMPLATE_TYPE_CODE`),
  CONSTRAINT `ACTION_LETTER_TEMPLATE_FK1` FOREIGN KEY (`LETTER_TEMPLATE_TYPE_CODE`) REFERENCES `letter_template_type` (`LETTER_TEMPLATE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `activity_grantcall_mapping` (
  `ACTIVITY_TYPE_CODE` varchar(3) NOT NULL,
  `GRANT_TYPE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ACTIVITY_TYPE_CODE`,`GRANT_TYPE_CODE`),
  KEY `ACTIVITY_GRANTCALL_MAPPING_FK1` (`GRANT_TYPE_CODE`),
  CONSTRAINT `ACTIVITY_GRANTCALL_MAPPING_FK1` FOREIGN KEY (`GRANT_TYPE_CODE`) REFERENCES `grant_call_type` (`GRANT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `activity_type` (
  `ACTIVITY_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `HIGHER_EDUCATION_FUNCTION_CODE` varchar(20) DEFAULT NULL,
  `GRANT_TYPE_CODE` int DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`ACTIVITY_TYPE_CODE`),
  KEY `ACTIVITY_TYPE_FK1` (`GRANT_TYPE_CODE`),
  CONSTRAINT `ACTIVITY_TYPE_FK1` FOREIGN KEY (`GRANT_TYPE_CODE`) REFERENCES `grant_call_type` (`GRANT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `admin_group` (
  `ADMIN_GROUP_ID` int NOT NULL,
  `ADMIN_GROUP_NAME` varchar(40) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `EMAIL_ID` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `ROLE_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  PRIMARY KEY (`ADMIN_GROUP_ID`),
  KEY `AGREEMENT_ADMIN_GROUPS_FK2` (`PERSON_ID`),
  KEY `AGREEMENT_ADMIN_GROUPS_FKI` (`ROLE_ID`),
  CONSTRAINT `ADMIN_GROUP_FK1` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ROLE_ID`),
  CONSTRAINT `ADMIN_GROUP_FK2` FOREIGN KEY (`PERSON_ID`) REFERENCES `person` (`PERSON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_action_log` (
  `ACTION_LOG_ID` int NOT NULL AUTO_INCREMENT,
  `ACTION_TYPE_CODE` varchar(3) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `AGREEMENT_REQUEST_ID` int DEFAULT NULL,
  PRIMARY KEY (`ACTION_LOG_ID`),
  KEY `AGREEMENT_ACTION_LOG_FK1` (`ACTION_TYPE_CODE`),
  KEY `AGREEMENT_ACTION_LOG_FK2` (`AGREEMENT_REQUEST_ID`),
  CONSTRAINT `AGREEMENT_ACTION_LOG_FK1` FOREIGN KEY (`ACTION_TYPE_CODE`) REFERENCES `agreement_action_type` (`ACTION_TYPE_CODE`),
  CONSTRAINT `AGREEMENT_ACTION_LOG_FK2` FOREIGN KEY (`AGREEMENT_REQUEST_ID`) REFERENCES `agreement_header` (`AGREEMENT_REQUEST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_action_type` (
  `ACTION_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `message` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ACTION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_association_link` (
  `MODULE_ID` int NOT NULL AUTO_INCREMENT,
  `AGREEMENT_REQUEST_ID` int DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(11) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`MODULE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_attachment` (
  `AGREEMENT_ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `ACTION_LOG_ID` int DEFAULT NULL,
  `AGREEMENT_ATTACHMENT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `AGREEMENT_ATTACHMENT_FILE_ID` varchar(36) DEFAULT NULL,
  `DESCRIPTION` varchar(1000) DEFAULT NULL,
  `IS_FINAL` varchar(1) DEFAULT NULL,
  `FINAL_ATTCHMNT_TYPE_CODE` decimal(3,0) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `CONTENT_TYPE` varchar(100) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `AGRMNT_ATTACH_STATUS_CODE` varchar(3) DEFAULT NULL,
  `DOCUMENT_STATUS_CODE` int DEFAULT NULL,
  `DOCUMENT_ID` int DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `AGREEMENT_REQUEST_ID` int DEFAULT NULL,
  PRIMARY KEY (`AGREEMENT_ATTACHMENT_ID`),
  KEY `AGREEMENT_ATTACHMENT_FK2` (`ACTION_LOG_ID`),
  KEY `AGREEMENT_ATTACHMENT_FK3` (`AGREEMENT_ATTACHMENT_TYPE_CODE`),
  KEY `AGREEMENT_ATTACHMENT_FK4` (`AGREEMENT_ATTACHMENT_FILE_ID`),
  KEY `AGREEMENT_ATTACHMENT_FK6` (`AGRMNT_ATTACH_STATUS_CODE`),
  KEY `AGREEMENT_ATTACHMENT_FK5` (`DOCUMENT_STATUS_CODE`),
  KEY `AGREEMENT_ATTACHMENT_FK1` (`AGREEMENT_REQUEST_ID`),
  CONSTRAINT `AGREEMENT_ATTACHMENT_FK1` FOREIGN KEY (`AGREEMENT_REQUEST_ID`) REFERENCES `agreement_header` (`AGREEMENT_REQUEST_ID`),
  CONSTRAINT `AGREEMENT_ATTACHMENT_FK2` FOREIGN KEY (`ACTION_LOG_ID`) REFERENCES `agreement_action_log` (`ACTION_LOG_ID`),
  CONSTRAINT `AGREEMENT_ATTACHMENT_FK3` FOREIGN KEY (`AGREEMENT_ATTACHMENT_TYPE_CODE`) REFERENCES `agreement_attachment_type` (`AGREEMENT_ATTACHMENT_TYPE_CODE`),
  CONSTRAINT `AGREEMENT_ATTACHMENT_FK5` FOREIGN KEY (`DOCUMENT_STATUS_CODE`) REFERENCES `document_status` (`DOCUMENT_STATUS_CODE`),
  CONSTRAINT `AGREEMENT_ATTACHMENT_FK6` FOREIGN KEY (`AGRMNT_ATTACH_STATUS_CODE`) REFERENCES `agreement_attachment_status` (`AGRMNT_ATTACH_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_attachment_file` (
  `AGREEMENT_ATTACHMENT_FILE_ID` varchar(36) NOT NULL,
  `FILEDATA` longblob,
  PRIMARY KEY (`AGREEMENT_ATTACHMENT_FILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_attachment_status` (
  `AGRMNT_ATTACH_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`AGRMNT_ATTACH_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_attachment_type` (
  `AGREEMENT_ATTACHMENT_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`AGREEMENT_ATTACHMENT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_category` (
  `CATEGORY_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`CATEGORY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_clauses` (
  `AGREEMENT_CLAUSES_ID` int NOT NULL AUTO_INCREMENT,
  `AGREEMENT_REQUEST_ID` int DEFAULT NULL,
  `CLAUSES` varchar(1000) DEFAULT NULL,
  `CLAUSES_GROUP_CODE` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`AGREEMENT_CLAUSES_ID`),
  KEY `AGREEMENT_CLAUSES_FK1` (`CLAUSES_GROUP_CODE`),
  KEY `AGREEMENT_TYPE_CLAUSES_FK1` (`AGREEMENT_REQUEST_ID`),
  CONSTRAINT `AGREEMENT_CLAUSES_FK1` FOREIGN KEY (`CLAUSES_GROUP_CODE`) REFERENCES `clauses_group` (`CLAUSES_GROUP_CODE`),
  CONSTRAINT `AGREEMENT_TYPE_CLAUSES_FK1` FOREIGN KEY (`AGREEMENT_REQUEST_ID`) REFERENCES `agreement_header` (`AGREEMENT_REQUEST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_document_type` (
  `DOCUMENT_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`DOCUMENT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_field_type` (
  `FIELD_CODE` varchar(5) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`FIELD_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_header` (
  `AGREEMENT_REQUEST_ID` int NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(1000) DEFAULT NULL,
  `DOCUMENT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `AGREEMENT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `AGREEMENT_STATUS_CODE` varchar(3) DEFAULT NULL,
  `REQUESTOR_PERSON_ID` varchar(40) DEFAULT NULL,
  `REQUESTOR_NAME` varchar(90) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `AMOUNT_IN_WORDS` varchar(300) DEFAULT NULL,
  `CATEGORY_CODE` varchar(3) DEFAULT NULL,
  `CONTRACT_VALUE` decimal(13,2) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(40) DEFAULT NULL,
  `CURRENCY_CODE` varchar(3) DEFAULT NULL,
  `REMARKS` varchar(4000) DEFAULT NULL,
  `SUBMIT_USER` varchar(40) DEFAULT NULL,
  `UNIT_NAME` varchar(200) DEFAULT NULL,
  `UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `WORKFLOW_STATUS_CODE` varchar(3) DEFAULT NULL,
  `ADMIN_PERSON_ID` varchar(40) DEFAULT NULL,
  `ADMIN_GROUP_ID` int DEFAULT NULL,
  `ADMIN_PERSON_NAME` varchar(90) DEFAULT NULL,
  `AGREEMENT_SEQUENCE_STATUS` varchar(30) DEFAULT NULL,
  `SUBMIT_USER_ID` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`AGREEMENT_REQUEST_ID`),
  KEY `AGREEMENT_HEADER_FK1` (`AGREEMENT_STATUS_CODE`),
  KEY `AGREEMENT_HEADER_FK2` (`AGREEMENT_TYPE_CODE`),
  KEY `AGREEMENT_HEADER_FK3` (`CATEGORY_CODE`),
  KEY `AGREEMENT_HEADER_FK4` (`CURRENCY_CODE`),
  KEY `AGREEMENT_HEADER_FK5` (`UNIT_NUMBER`),
  KEY `AGREEMENT_HEADER_FK7` (`WORKFLOW_STATUS_CODE`),
  KEY `AGREEMENT_HEADER_FK8` (`ADMIN_GROUP_ID`),
  CONSTRAINT `AGREEMENT_HEADER_FK1` FOREIGN KEY (`AGREEMENT_STATUS_CODE`) REFERENCES `agreement_status` (`AGREEMENT_STATUS_CODE`),
  CONSTRAINT `AGREEMENT_HEADER_FK2` FOREIGN KEY (`AGREEMENT_TYPE_CODE`) REFERENCES `agreement_type` (`AGREEMENT_TYPE_CODE`),
  CONSTRAINT `AGREEMENT_HEADER_FK3` FOREIGN KEY (`CATEGORY_CODE`) REFERENCES `agreement_category` (`CATEGORY_CODE`),
  CONSTRAINT `AGREEMENT_HEADER_FK4` FOREIGN KEY (`CURRENCY_CODE`) REFERENCES `currency` (`CURRENCY_CODE`),
  CONSTRAINT `AGREEMENT_HEADER_FK5` FOREIGN KEY (`UNIT_NUMBER`) REFERENCES `unit` (`UNIT_NUMBER`),
  CONSTRAINT `AGREEMENT_HEADER_FK7` FOREIGN KEY (`WORKFLOW_STATUS_CODE`) REFERENCES `agreement_workflow_status` (`WORKFLOW_STATUS_CODE`),
  CONSTRAINT `AGREEMENT_HEADER_FK8` FOREIGN KEY (`ADMIN_GROUP_ID`) REFERENCES `admin_group` (`ADMIN_GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_link_module` (
  `MODULE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  `SORT_ORDER` int DEFAULT NULL,
  PRIMARY KEY (`MODULE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_note` (
  `AGREEMENT_NOTE_ID` int NOT NULL AUTO_INCREMENT,
  `ACTION_LOG_ID` int DEFAULT NULL,
  `NOTE` varchar(4000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `AGREEMENT_REQUEST_ID` int DEFAULT NULL,
  PRIMARY KEY (`AGREEMENT_NOTE_ID`),
  KEY `AGREEMENT_NOTE_FK2` (`ACTION_LOG_ID`),
  KEY `AGREEMENT_NOTE_FK1` (`AGREEMENT_REQUEST_ID`),
  CONSTRAINT `AGREEMENT_NOTE_FK1` FOREIGN KEY (`AGREEMENT_REQUEST_ID`) REFERENCES `agreement_header` (`AGREEMENT_REQUEST_ID`),
  CONSTRAINT `AGREEMENT_NOTE_FK2` FOREIGN KEY (`ACTION_LOG_ID`) REFERENCES `agreement_action_log` (`ACTION_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_note_attachment` (
  `AGREEMENT_NOTE_ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `AGREEMENT_ATTACHMENT_FILE_ID` varchar(36) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `MIME_TYPE` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  `AGREEMENT_NOTE_ID` int NOT NULL,
  PRIMARY KEY (`AGREEMENT_NOTE_ATTACHMENT_ID`),
  KEY `AGREEMENT_NOTE_ATTACHMENT_FK1` (`AGREEMENT_NOTE_ID`),
  CONSTRAINT `AGREEMENT_NOTE_ATTACHMENT_FK1` FOREIGN KEY (`AGREEMENT_NOTE_ID`) REFERENCES `agreement_note` (`AGREEMENT_NOTE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_note_file_data` (
  `ID` varchar(36) NOT NULL,
  `DATA` longblob,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_people` (
  `AGREEMENT_PEOPLE_ID` int NOT NULL AUTO_INCREMENT,
  `AGREEMENT_REQUEST_ID` int DEFAULT NULL,
  `FULL_NAME` varchar(100) DEFAULT NULL,
  `PEOPLE_TYPE_ID` int DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `PI_PERSONNEL_TYPE_CODE` varchar(3) DEFAULT NULL,
  `ROLODEX_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AGREEMENT_PEOPLE_ID`),
  KEY `AGREEMENT_PEOPLE_FK2` (`PEOPLE_TYPE_ID`),
  KEY `AGREEMENT_PEOPLE_FK3` (`PI_PERSONNEL_TYPE_CODE`),
  CONSTRAINT `AGREEMENT_PEOPLE_FK2` FOREIGN KEY (`PEOPLE_TYPE_ID`) REFERENCES `agreement_people_type` (`PEOPLE_TYPE_ID`),
  CONSTRAINT `AGREEMENT_PEOPLE_FK3` FOREIGN KEY (`PI_PERSONNEL_TYPE_CODE`) REFERENCES `negotiation_personnel_type` (`PERSONNEL_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_people_type` (
  `PEOPLE_TYPE_ID` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`PEOPLE_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_place_holder` (
  `PLACE_HOLDER_NAME` varchar(60) NOT NULL,
  `PLACE_HOLDER_TYPE` varchar(10) NOT NULL,
  `DESCRIPTION` varchar(1000) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  `VIEW_COLUMN` varchar(30) DEFAULT NULL,
  `VIEW_NAME` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`PLACE_HOLDER_NAME`,`PLACE_HOLDER_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_review_type` (
  `AGREEMENT_REVIEW_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `LOCATION_TYPE_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  `WORKFLOW_STATUS_CODE` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`AGREEMENT_REVIEW_TYPE_CODE`),
  KEY `AGREEMENT_REVIEW_TYPE_FK2` (`WORKFLOW_STATUS_CODE`),
  KEY `AGREEMENT_REVIEW_TYPE_FK1` (`LOCATION_TYPE_CODE`),
  CONSTRAINT `AGREEMENT_REVIEW_TYPE_FK1` FOREIGN KEY (`LOCATION_TYPE_CODE`) REFERENCES `negotiation_location_type` (`LOCATION_TYPE_CODE`),
  CONSTRAINT `AGREEMENT_REVIEW_TYPE_FK2` FOREIGN KEY (`WORKFLOW_STATUS_CODE`) REFERENCES `agreement_workflow_status` (`WORKFLOW_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_section_type` (
  `SECTION_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`SECTION_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_sponsor` (
  `AGREEMENT_SPONSOR_ID` int NOT NULL AUTO_INCREMENT,
  `SPONSOR_NAME` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `CONTACT_ADDRESS_LINE1` varchar(200) DEFAULT NULL,
  `AGREEMENT_SPONSOR_TYPE_CODE` varchar(3) DEFAULT NULL,
  `REG_NUMBER` varchar(50) DEFAULT NULL,
  `SPONSOR_CODE` varchar(6) DEFAULT NULL,
  `SPONSOR_ROLE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `AGREEMENT_REQUEST_ID` int DEFAULT NULL,
  PRIMARY KEY (`AGREEMENT_SPONSOR_ID`),
  KEY `AGREEMENT_SPONSOR_FK2` (`SPONSOR_CODE`),
  KEY `AGREEMENT_SPONSOR_FK3` (`SPONSOR_ROLE_TYPE_CODE`),
  KEY `AGREEMENT_SPONSOR_FK4` (`AGREEMENT_SPONSOR_TYPE_CODE`),
  KEY `AGREEMENT_SPONSOR_FK1` (`AGREEMENT_REQUEST_ID`),
  CONSTRAINT `AGREEMENT_SPONSOR_FK1` FOREIGN KEY (`AGREEMENT_REQUEST_ID`) REFERENCES `agreement_header` (`AGREEMENT_REQUEST_ID`),
  CONSTRAINT `AGREEMENT_SPONSOR_FK2` FOREIGN KEY (`SPONSOR_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`),
  CONSTRAINT `AGREEMENT_SPONSOR_FK3` FOREIGN KEY (`SPONSOR_ROLE_TYPE_CODE`) REFERENCES `sponsor_role` (`SPONSOR_ROLE_TYPE_CODE`),
  CONSTRAINT `AGREEMENT_SPONSOR_FK4` FOREIGN KEY (`AGREEMENT_SPONSOR_TYPE_CODE`) REFERENCES `agreement_sponsor_type` (`AGREEMENT_SPONSOR_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_sponsor_contact` (
  `AGREEMENT_SPONSOR_CONTACT_ID` int NOT NULL AUTO_INCREMENT,
  `AGREEMENT_SPONSOR_ID` int DEFAULT NULL,
  `SPONSOR_CONTCT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `CONTACT_PERSON_NAME` varchar(90) DEFAULT NULL,
  `CONTACT_EMAIL_ID` varchar(200) DEFAULT NULL,
  `CONTACT_PHONE` varchar(20) DEFAULT NULL,
  `CONTACT_ADDRESS_LINE1` varchar(80) DEFAULT NULL,
  `CONTACT_CITY` varchar(30) DEFAULT NULL,
  `CONTACT_STATE` varchar(30) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `DESIGNATION` varchar(200) DEFAULT NULL,
  `SALUTATION` varchar(200) DEFAULT NULL,
  `AGREEMENT_REQUEST_ID` int DEFAULT NULL,
  PRIMARY KEY (`AGREEMENT_SPONSOR_CONTACT_ID`),
  KEY `AGREEMENT_SPONSOR_CONTACT_IDX1` (`AGREEMENT_SPONSOR_ID`),
  KEY `AGREEMENT_SPONSOR_CONTCT_FK2` (`SPONSOR_CONTCT_TYPE_CODE`),
  KEY `AGREEMENT_SPONSOR_CONTCT_FK1` (`AGREEMENT_REQUEST_ID`),
  CONSTRAINT `AGREEMENT_SPONSOR_CONTCT_FK1` FOREIGN KEY (`AGREEMENT_REQUEST_ID`) REFERENCES `agreement_header` (`AGREEMENT_REQUEST_ID`),
  CONSTRAINT `AGREEMENT_SPONSOR_CONTCT_FK2` FOREIGN KEY (`SPONSOR_CONTCT_TYPE_CODE`) REFERENCES `agreement_sponsor_contct_type` (`SPONSOR_CONTCT_TYPE_CODE`),
  CONSTRAINT `AGREEMENT_SPONSOR_CONTCT_FK3` FOREIGN KEY (`AGREEMENT_SPONSOR_ID`) REFERENCES `agreement_sponsor` (`AGREEMENT_SPONSOR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_sponsor_contct_type` (
  `SPONSOR_CONTCT_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`SPONSOR_CONTCT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_sponsor_type` (
  `AGREEMENT_SPONSOR_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`AGREEMENT_SPONSOR_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_status` (
  `AGREEMENT_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `CAN_ASSOCIATE` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`AGREEMENT_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_type` (
  `AGREEMENT_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `CATEGORY_CODE` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`AGREEMENT_TYPE_CODE`),
  KEY `agreement_type_fk1` (`CATEGORY_CODE`),
  CONSTRAINT `agreement_type_fk1` FOREIGN KEY (`CATEGORY_CODE`) REFERENCES `agreement_category` (`CATEGORY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_type_history` (
  `AGREEMENT_TYPE_HISTORY_ID` int NOT NULL AUTO_INCREMENT,
  `AGREEMENT_REQUEST_ID` int DEFAULT NULL,
  `AGREEMENT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`AGREEMENT_TYPE_HISTORY_ID`),
  KEY `AGREEMENT_TYPE_HISTORY_FK2` (`AGREEMENT_TYPE_CODE`),
  CONSTRAINT `AGREEMENT_TYPE_HISTORY_FK2` FOREIGN KEY (`AGREEMENT_TYPE_CODE`) REFERENCES `agreement_type` (`AGREEMENT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_type_template` (
  `TEMPLATE_ID` int NOT NULL AUTO_INCREMENT,
  `AGREEMENT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `CONTENT_TYPE` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `DOCUMENT_ID` int DEFAULT NULL,
  `DOCUMENT_STATUS_CODE` int DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `IS_FINAL` varchar(1) DEFAULT NULL,
  `TEMPLATE` longblob,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  PRIMARY KEY (`TEMPLATE_ID`),
  KEY `AGREEMENT_TYPE_TEMPLATE_FK1` (`AGREEMENT_TYPE_CODE`),
  KEY `AGREEMENT_TYPE_TEMPLATE_FK2` (`DOCUMENT_STATUS_CODE`),
  CONSTRAINT `AGREEMENT_TYPE_TEMPLATE_FK1` FOREIGN KEY (`AGREEMENT_TYPE_CODE`) REFERENCES `agreement_type` (`AGREEMENT_TYPE_CODE`),
  CONSTRAINT `AGREEMENT_TYPE_TEMPLATE_FK2` FOREIGN KEY (`DOCUMENT_STATUS_CODE`) REFERENCES `document_status` (`DOCUMENT_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreement_workflow_status` (
  `WORKFLOW_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`WORKFLOW_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `agreemnt_type_clauses_mapping` (
  `AGREEMENT_TYPE_CODE` varchar(3) NOT NULL,
  `CLAUSES_GROUP_CODE` int NOT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`AGREEMENT_TYPE_CODE`,`CLAUSES_GROUP_CODE`),
  KEY `AGRMNT_TYPE_CLAUSES_MAPING_FK2` (`CLAUSES_GROUP_CODE`),
  CONSTRAINT `AGRMNT_TYPE_CLAUSES_MAPING_FK1` FOREIGN KEY (`AGREEMENT_TYPE_CODE`) REFERENCES `agreement_type` (`AGREEMENT_TYPE_CODE`),
  CONSTRAINT `AGRMNT_TYPE_CLAUSES_MAPING_FK2` FOREIGN KEY (`CLAUSES_GROUP_CODE`) REFERENCES `clauses_group` (`CLAUSES_GROUP_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `appointment_type` (
  `APPOINTMENT_TYPE_CODE` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `DURATION` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`APPOINTMENT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `arg_value_lookup` (
  `ARG_VALUE_LOOKUP_ID` int NOT NULL,
  `ARGUMENT_NAME` varchar(30) DEFAULT NULL,
  `ARGUMENT_VALUE` varchar(200) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ARG_VALUE_LOOKUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `attachment` (
  `ID` varchar(255) NOT NULL,
  `CONTENT` tinyblob,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `audit_log` (
  `LOG_ID` int NOT NULL AUTO_INCREMENT,
  `MODULE` varchar(60) DEFAULT NULL,
  `SUB_MODULE` varchar(60) DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(45) DEFAULT NULL,
  `ACTION_PERSON_ID` varchar(60) DEFAULT NULL,
  `ACTION` json DEFAULT NULL,
  `ACTION_TYPE` varchar(1) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `CHANGES` mediumtext,
  PRIMARY KEY (`LOG_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=562 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `audit_log_config` (
  `MODULE` varchar(45) NOT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `DESCRIPTION` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`MODULE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `audit_report_type` (
  `REPORT_ID` int NOT NULL,
  `REPORT_TYPE` varchar(45) DEFAULT NULL,
  `REPORT_NAME` varchar(100) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`REPORT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award` (
  `AWARD_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `ACCOUNT_NUMBER` varchar(100) DEFAULT NULL,
  `LEAD_UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `TITLE` varchar(300) DEFAULT NULL,
  `AWARD_TYPE_CODE` varchar(3) DEFAULT NULL,
  `ACTIVITY_TYPE_CODE` varchar(3) DEFAULT NULL,
  `ACCOUNT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `STATUS_CODE` varchar(3) DEFAULT NULL,
  `SPONSOR_TEMPLATE_CODE` int DEFAULT NULL,
  `SPONSOR_AWARD_NUMBER` varchar(70) DEFAULT NULL,
  `SPONSOR_CODE` varchar(6) DEFAULT NULL,
  `AWARD_EXECUTION_DATE` datetime DEFAULT NULL,
  `AWARD_EFFECTIVE_DATE` datetime DEFAULT NULL,
  `FINAL_EXPIRATION_DATE` datetime DEFAULT NULL,
  `BEGIN_DATE` datetime DEFAULT NULL,
  `PRE_AWARD_AUTHORIZED_AMOUNT` decimal(12,2) DEFAULT NULL,
  `SPECIAL_EB_RATE_OFF_CAMPUS` decimal(5,2) DEFAULT NULL,
  `SPECIAL_EB_RATE_ON_CAMPUS` decimal(5,2) DEFAULT NULL,
  `PRE_AWARD_EFFECTIVE_DATE` datetime DEFAULT NULL,
  `PRIME_SPONSOR_CODE` varchar(6) DEFAULT NULL,
  `AWARD_SEQUENCE_STATUS` varchar(10) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_LATEST` varchar(1) DEFAULT 'Y',
  `BUDGET_HEADER_ID` int DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `WORKFLOW_AWARD_STATUS_CODE` varchar(3) DEFAULT NULL,
  `AWARD_DOCUMENT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `AWARD_VARIATION_TYPE_CODE` varchar(3) DEFAULT NULL,
  `SUBMIT_USER` varchar(60) DEFAULT NULL,
  `FUND_CENTER` varchar(255) DEFAULT NULL,
  `UNIQUE_ENTITY_NO` varchar(60) DEFAULT NULL,
  `FUND_CENTRE` varchar(60) DEFAULT NULL,
  `SUBMISSION_DATE` datetime DEFAULT NULL,
  `BASIS_OF_PAYMENT_CODE` varchar(255) DEFAULT NULL,
  `DFAFS_NUMBER` varchar(12) DEFAULT NULL,
  `FINAL_INVOICE_DUE` int DEFAULT NULL,
  `INVOICE_INSTRUCTIONS` varchar(255) DEFAULT NULL,
  `INVOICE_NUMBER_OF_COPIES` int DEFAULT NULL,
  `METHOD_OF_PAYMENT_CODE` varchar(255) DEFAULT NULL,
  `PAYMENT_INVOICE_FREQ_CODE` varchar(255) DEFAULT NULL,
  `GRANT_HEADER_ID` int DEFAULT NULL,
  `RESEARCH_AREA_DESC` varchar(4000) DEFAULT NULL,
  `MULTI_DISCIPLINARY_DESC` varchar(4000) DEFAULT NULL,
  `CFDA_NUMBER` varchar(12) DEFAULT NULL,
  `DURATION` varchar(255) DEFAULT NULL,
  `DOCUMENT_UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `FUNDER_APPROVAL_DATE` datetime DEFAULT NULL,
  `DOCUMENT_UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AWARD_ID`),
  KEY `AWARD_FK2` (`PRIME_SPONSOR_CODE`),
  KEY `AWARD_FK4` (`BUDGET_HEADER_ID`),
  KEY `AWARD_FK10` (`ACCOUNT_TYPE_CODE`),
  KEY `AWARD_FK12` (`BASIS_OF_PAYMENT_CODE`),
  KEY `AWARD_FK13` (`METHOD_OF_PAYMENT_CODE`),
  KEY `AWARD_FK14` (`PAYMENT_INVOICE_FREQ_CODE`),
  KEY `AWARD_LEAD_UNIT_NUMBER_IX` (`LEAD_UNIT_NUMBER`),
  KEY `AWARD_TYPE_CODE_IX` (`AWARD_TYPE_CODE`),
  KEY `AWARD_STATUS_CODE_IX` (`STATUS_CODE`),
  KEY `AWARD_SPONSOR_CODE_IX` (`SPONSOR_CODE`),
  KEY `AWARD_ACTIVITY_TYPE_CODE_IX` (`ACTIVITY_TYPE_CODE`),
  KEY `AWARD_IX12` (`SPONSOR_CODE`),
  KEY `AWARD_IX13` (`AWARD_DOCUMENT_TYPE_CODE`),
  KEY `AWARD_IX18` (`WORKFLOW_AWARD_STATUS_CODE`),
  KEY `AWARD_FK8` (`AWARD_VARIATION_TYPE_CODE`),
  CONSTRAINT `AWARD_FK1` FOREIGN KEY (`SPONSOR_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`),
  CONSTRAINT `AWARD_FK10` FOREIGN KEY (`ACCOUNT_TYPE_CODE`) REFERENCES `account_type` (`ACCOUNT_TYPE_CODE`),
  CONSTRAINT `AWARD_FK11` FOREIGN KEY (`AWARD_TYPE_CODE`) REFERENCES `award_type` (`AWARD_TYPE_CODE`),
  CONSTRAINT `AWARD_FK12` FOREIGN KEY (`BASIS_OF_PAYMENT_CODE`) REFERENCES `award_basis_of_payment` (`BASIS_OF_PAYMENT_CODE`),
  CONSTRAINT `AWARD_FK13` FOREIGN KEY (`METHOD_OF_PAYMENT_CODE`) REFERENCES `award_method_of_payment` (`METHOD_OF_PAYMENT_CODE`),
  CONSTRAINT `AWARD_FK14` FOREIGN KEY (`PAYMENT_INVOICE_FREQ_CODE`) REFERENCES `frequency` (`FREQUENCY_CODE`),
  CONSTRAINT `AWARD_FK2` FOREIGN KEY (`PRIME_SPONSOR_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`),
  CONSTRAINT `AWARD_FK3` FOREIGN KEY (`LEAD_UNIT_NUMBER`) REFERENCES `unit` (`UNIT_NUMBER`),
  CONSTRAINT `AWARD_FK4` FOREIGN KEY (`BUDGET_HEADER_ID`) REFERENCES `award_budget_header` (`BUDGET_HEADER_ID`),
  CONSTRAINT `AWARD_FK5` FOREIGN KEY (`STATUS_CODE`) REFERENCES `award_status` (`STATUS_CODE`),
  CONSTRAINT `AWARD_FK6` FOREIGN KEY (`WORKFLOW_AWARD_STATUS_CODE`) REFERENCES `award_workflow_status` (`WORKFLOW_AWARD_STATUS_CODE`),
  CONSTRAINT `AWARD_FK7` FOREIGN KEY (`AWARD_DOCUMENT_TYPE_CODE`) REFERENCES `award_document_type` (`AWARD_DOCUMENT_TYPE_CODE`),
  CONSTRAINT `AWARD_FK8` FOREIGN KEY (`AWARD_VARIATION_TYPE_CODE`) REFERENCES `sr_type` (`TYPE_CODE`),
  CONSTRAINT `AWARD_FK9` FOREIGN KEY (`ACTIVITY_TYPE_CODE`) REFERENCES `activity_type` (`ACTIVITY_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_achievements_attachments` (
  `AWARD_ACHIEVEMENTS_ATTACH_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(255) DEFAULT NULL,
  `COMMENT` varchar(255) DEFAULT NULL,
  `FILE_DATA_ID` varchar(255) DEFAULT NULL,
  `FILE_NAME` varchar(255) DEFAULT NULL,
  `MIME_TYPE` varchar(255) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AWARD_ACHIEVEMENTS_ATTACH_ID`),
  KEY `AWARD_ACHIEVEMENTS_ATTACH_FK1` (`AWARD_ID`),
  CONSTRAINT `AWARD_ACHIEVEMENTS_ATTACH_FK1` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_amount_info` (
  `AWARD_AMOUNT_INFO_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `TRANSACTION_ID` decimal(22,0) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `ANTICIPATED_CHANGE` decimal(12,2) DEFAULT NULL,
  `ANTICIPATED_CHANGE_DIRECT` decimal(12,2) DEFAULT NULL,
  `ANTICIPATED_CHANGE_INDIRECT` decimal(12,2) DEFAULT NULL,
  `OBLIGATED_CHANGE` decimal(12,2) DEFAULT NULL,
  `OBLIGATED_CHANGE_DIRECT` decimal(12,2) DEFAULT NULL,
  `OBLIGATED_CHANGE_INDIRECT` decimal(12,2) DEFAULT NULL,
  `ANTICIPATED_TOTAL_DIRECT` decimal(12,2) DEFAULT NULL,
  `ANTICIPATED_TOTAL_INDIRECT` decimal(12,2) DEFAULT NULL,
  `ANTICIPATED_TOTAL_AMOUNT` decimal(12,2) DEFAULT NULL,
  `OBLIGATED_TOTAL_DIRECT` decimal(12,2) DEFAULT NULL,
  `OBLIGATED_TOTAL_INDIRECT` decimal(12,2) DEFAULT NULL,
  `AMOUNT_OBLIGATED_TO_DATE` decimal(12,2) DEFAULT NULL,
  `ANT_DISTRIBUTABLE_AMOUNT` decimal(12,2) DEFAULT NULL,
  `OBLI_DISTRIBUTABLE_AMOUNT` decimal(12,2) DEFAULT NULL,
  `FINAL_EXPIRATION_DATE` datetime DEFAULT NULL,
  `CURRENT_FUND_EFFECTIVE_DATE` datetime DEFAULT NULL,
  `OBLIGATION_EXPIRATION_DATE` datetime DEFAULT NULL,
  `TOTAL_COST_IN_CURRENCY` decimal(12,2) DEFAULT NULL,
  `CURRENCY_CODE` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`AWARD_AMOUNT_INFO_ID`),
  KEY `AWARD_AMOUNT_INFO_FK1` (`AWARD_ID`),
  KEY `AWARD_AMOUNT_INFO_FK2` (`TRANSACTION_ID`),
  KEY `AWARD_AMOUNT_INFO_FK3` (`CURRENCY_CODE`),
  CONSTRAINT `AWARD_AMOUNT_INFO_FK1` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`),
  CONSTRAINT `AWARD_AMOUNT_INFO_FK2` FOREIGN KEY (`TRANSACTION_ID`) REFERENCES `award_amount_transaction` (`TRANSACTION_ID`),
  CONSTRAINT `AWARD_AMOUNT_INFO_FK3` FOREIGN KEY (`CURRENCY_CODE`) REFERENCES `currency` (`CURRENCY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_amount_transaction` (
  `AWARD_AMOUNT_TRANSACTION_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `TRANSACTION_ID` decimal(22,0) DEFAULT NULL,
  `TRANSACTION_TYPE_CODE` decimal(3,0) DEFAULT NULL,
  `FUNDED_PROPOSAL_ID` decimal(12,0) DEFAULT NULL,
  `NOTICE_DATE` datetime DEFAULT NULL,
  `COMMENTS` varchar(2000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `SOURCE_AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `DESTINATION_AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `TRANSACTION_STATUS_CODE` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`AWARD_AMOUNT_TRANSACTION_ID`),
  UNIQUE KEY `AWARD_AMOUNT_TRANSACTION_UK` (`TRANSACTION_ID`),
  KEY `AWARD_AMOUNT_TRANSACTION_FK1` (`TRANSACTION_TYPE_CODE`),
  KEY `AWARD_AMOUNT_TRANSACTION_FK2` (`TRANSACTION_STATUS_CODE`),
  CONSTRAINT `AWARD_AMOUNT_TRANSACTION_FK1` FOREIGN KEY (`TRANSACTION_TYPE_CODE`) REFERENCES `award_transaction_type` (`AWARD_TRANSACTION_TYPE_CODE`),
  CONSTRAINT `AWARD_AMOUNT_TRANSACTION_FK2` FOREIGN KEY (`TRANSACTION_STATUS_CODE`) REFERENCES `award_transaction_status` (`TRANSACTION_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_amt_fna_distribution` (
  `FNA_DISTRIBUTION_ID` int NOT NULL AUTO_INCREMENT,
  `BUDGET_PERIOD` int DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `TOTAL_DIRECT_COST` decimal(12,2) DEFAULT NULL,
  `TOTAL_INDIRECT_COST` decimal(12,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `COMMENT` varchar(4000) DEFAULT NULL,
  `AWARD_ID` decimal(22,0) DEFAULT NULL,
  PRIMARY KEY (`FNA_DISTRIBUTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_amt_trnsctn_history` (
  `TRNSCTN_HISTORY_ID` int NOT NULL AUTO_INCREMENT,
  `TRANSACTION_ID` decimal(22,0) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  PRIMARY KEY (`TRNSCTN_HISTORY_ID`),
  KEY `AWARD_AMT_TRNSCTN_HISTORY_FK1` (`AWARD_ID`),
  KEY `AWARD_AMT_TRNSCTN_HISTORY_FK2` (`TRANSACTION_ID`),
  CONSTRAINT `AWARD_AMT_TRNSCTN_HISTORY_FK1` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`),
  CONSTRAINT `AWARD_AMT_TRNSCTN_HISTORY_FK2` FOREIGN KEY (`TRANSACTION_ID`) REFERENCES `award_amount_transaction` (`TRANSACTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_approved_equipment` (
  `AWARD_APPROVED_EQUIPMENT_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `ITEM` varchar(100) DEFAULT NULL,
  `VENDOR` varchar(50) DEFAULT NULL,
  `MODEL` varchar(50) DEFAULT NULL,
  `AMOUNT` decimal(12,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`AWARD_APPROVED_EQUIPMENT_ID`),
  KEY `AWARD_APPROVED_EQUIPMENT_FK1` (`AWARD_ID`),
  CONSTRAINT `AWARD_APPROVED_EQUIPMENT_FK1` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_approved_foreign_travel` (
  `AWARD_APPR_FORN_TRAVEL_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `ROLODEX_ID` int DEFAULT NULL,
  `TRAVELER_NAME` varchar(90) DEFAULT NULL,
  `DESTINATION` varchar(30) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `AMOUNT` decimal(12,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`AWARD_APPR_FORN_TRAVEL_ID`),
  KEY `AWARD_APPRV_FOREIGN_TRAVEL_FK1` (`AWARD_ID`),
  CONSTRAINT `AWARD_APPRV_FOREIGN_TRAVEL_FK1` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_approved_subawards` (
  `AWARD_APPROVED_SUBAWARD_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `ORGANIZATION_ID` varchar(20) DEFAULT NULL,
  `AMOUNT` decimal(12,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`AWARD_APPROVED_SUBAWARD_ID`),
  KEY `AWARD_APPROVED_SUBAWARDS_FK2` (`ORGANIZATION_ID`),
  KEY `AWARD_APPROVED_SUBAWARDS_FK1` (`AWARD_ID`),
  CONSTRAINT `AWARD_APPROVED_SUBAWARDS_FK1` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`),
  CONSTRAINT `AWARD_APPROVED_SUBAWARDS_FK2` FOREIGN KEY (`ORGANIZATION_ID`) REFERENCES `organization` (`ORGANIZATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_assoc_detail` (
  `AWARD_ASSOC_DETAIL_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ASSOCIATION_ID` int DEFAULT NULL,
  `title` varchar(300) DEFAULT NULL,
  `PI_PERSON_ID` varchar(40) DEFAULT NULL,
  `PI_ROLODEX_ID` varchar(40) DEFAULT NULL,
  `PI_NAME` varchar(100) DEFAULT NULL,
  `LEAD_UNIT` varchar(8) DEFAULT NULL,
  `SPONSOR_CODE` varchar(6) DEFAULT NULL,
  `PRIME_SPONSOR_CODE` varchar(6) DEFAULT NULL,
  `SPONSOR_AWARD_NUMBER` varchar(70) DEFAULT NULL,
  `CONTACT_ADMIN_PERSON_ID` varchar(40) DEFAULT NULL,
  `SUBAWARD_ORG` varchar(25) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `FUNDING_SCHEME_CODE` varchar(8) DEFAULT NULL,
  `STATUS_DESCRIPTION` varchar(200) DEFAULT NULL,
  `TOTAL_PROJECT_COST` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`AWARD_ASSOC_DETAIL_ID`),
  KEY `AWARD_ASSOC_DETAIL_FK1` (`AWARD_ASSOCIATION_ID`),
  KEY `AWARD_ASSOC_DETAIL_FK2` (`LEAD_UNIT`),
  KEY `AWARD_ASSOC_DETAIL_FK3` (`SPONSOR_CODE`),
  KEY `AWARD_ASSOC_DETAIL_FK4` (`PRIME_SPONSOR_CODE`),
  KEY `AWARD_ASSOC_DETAIL_FK5` (`CONTACT_ADMIN_PERSON_ID`),
  KEY `AWARD_ASSOC_DETAIL_FK6` (`SUBAWARD_ORG`),
  KEY `AWARD_ASSOC_DETAIL_FK7` (`FUNDING_SCHEME_CODE`),
  CONSTRAINT `AWARD_ASSOC_DETAIL_FK1` FOREIGN KEY (`AWARD_ASSOCIATION_ID`) REFERENCES `award_association` (`AWARD_ASSOCIATION_ID`),
  CONSTRAINT `AWARD_ASSOC_DETAIL_FK2` FOREIGN KEY (`LEAD_UNIT`) REFERENCES `unit` (`UNIT_NUMBER`),
  CONSTRAINT `AWARD_ASSOC_DETAIL_FK3` FOREIGN KEY (`SPONSOR_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`),
  CONSTRAINT `AWARD_ASSOC_DETAIL_FK4` FOREIGN KEY (`PRIME_SPONSOR_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`),
  CONSTRAINT `AWARD_ASSOC_DETAIL_FK5` FOREIGN KEY (`CONTACT_ADMIN_PERSON_ID`) REFERENCES `person` (`PERSON_ID`),
  CONSTRAINT `AWARD_ASSOC_DETAIL_FK6` FOREIGN KEY (`SUBAWARD_ORG`) REFERENCES `organization` (`ORGANIZATION_ID`),
  CONSTRAINT `AWARD_ASSOC_DETAIL_FK7` FOREIGN KEY (`FUNDING_SCHEME_CODE`) REFERENCES `funding_scheme` (`FUNDING_SCHEME_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_association` (
  `AWARD_ASSOCIATION_ID` int NOT NULL AUTO_INCREMENT,
  `ASSOCIATED_PROJECT_ID` varchar(255) DEFAULT NULL,
  `ASSOCIATION_TYPE_CODE` varchar(255) DEFAULT NULL,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(255) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AWARD_ASSOCIATION_ID`),
  KEY `AWARD_ASSOCIATION_FK2` (`ASSOCIATION_TYPE_CODE`),
  KEY `AWARD_ASSOCIATION_FK1` (`AWARD_ID`),
  CONSTRAINT `AWARD_ASSOCIATION_FK1` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`),
  CONSTRAINT `AWARD_ASSOCIATION_FK2` FOREIGN KEY (`ASSOCIATION_TYPE_CODE`) REFERENCES `award_association_type` (`ASSOCIATION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_association_type` (
  `ASSOCIATION_TYPE_CODE` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`ASSOCIATION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_attachment` (
  `AWARD_ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(20) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `TYPE_CODE` varchar(3) DEFAULT NULL,
  `FILE_ID` decimal(22,0) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `DOCUMENT_STATUS_CODE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `DOCUMENT_ID` int DEFAULT NULL,
  `FILE_DATA_ID` varchar(255) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `MIME_TYPE` varchar(255) DEFAULT NULL,
  `NARRATIVE_STATUS_CODE` varchar(255) DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  PRIMARY KEY (`AWARD_ATTACHMENT_ID`),
  KEY `AWARD_ATTACHMENT_FK1` (`AWARD_ID`),
  KEY `AWARD_ATTACHMENT_FK2` (`NARRATIVE_STATUS_CODE`),
  CONSTRAINT `AWARD_ATTACHMENT_FK1` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`),
  CONSTRAINT `AWARD_ATTACHMENT_FK2` FOREIGN KEY (`NARRATIVE_STATUS_CODE`) REFERENCES `narrative_status` (`NARRATIVE_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_attachment_type` (
  `TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(300) DEFAULT NULL,
  `UPDATE_TIMESTAMP` date DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_PRIVATE` varchar(1) DEFAULT 'N',
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_basis_of_payment` (
  `BASIS_OF_PAYMENT_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`BASIS_OF_PAYMENT_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_bud_per_det_rate_n_base` (
  `BUD_PER_RATE_BASE_ID` int NOT NULL,
  `BUDGET_PERIOD` int DEFAULT NULL,
  `LINE_ITEM_NUMBER` int DEFAULT NULL,
  `PERSON_NUMBER` int DEFAULT NULL,
  `RATE_NUMBER` int DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `RATE_CLASS_CODE` varchar(3) DEFAULT NULL,
  `RATE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `ON_OFF_CAMPUS_FLAG` varchar(1) DEFAULT NULL,
  `APPLIED_RATE` decimal(5,2) DEFAULT NULL,
  `SALARY_REQUESTED` decimal(12,2) DEFAULT NULL,
  `BASE_COST_SHARING` decimal(12,2) DEFAULT NULL,
  `CALCULATED_COST` decimal(12,2) DEFAULT NULL,
  `CALCULATED_COST_SHARING` decimal(12,2) DEFAULT NULL,
  `JOB_CODE` varchar(6) DEFAULT NULL,
  `BASE_COST` decimal(12,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`BUD_PER_RATE_BASE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_budget_det_cal_amt` (
  `BUDGET_DETAILS_CAL_AMTS_ID` int NOT NULL AUTO_INCREMENT,
  `APPLY_RATE_FLAG` varchar(1) DEFAULT NULL,
  `BUDGET_ID` int DEFAULT NULL,
  `BUDGET_PERIOD` int DEFAULT NULL,
  `BUDGET_PERIOD_NUMBER` int DEFAULT NULL,
  `CALCULATED_COST` decimal(12,2) DEFAULT NULL,
  `CALCULATED_COST_SHARING` decimal(12,2) DEFAULT NULL,
  `LINE_ITEM_NUMBER` int DEFAULT NULL,
  `RATE_CLASS_CODE` varchar(3) DEFAULT NULL,
  `RATE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `RATE_TYPE_DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `BUDGET_DETAILS_ID` int DEFAULT NULL,
  `APPLICABLE_RATE` decimal(12,2) DEFAULT NULL,
  `BUDGET_HEADER_ID` int DEFAULT NULL,
  `BUDGET_PERIOD_ID` int DEFAULT NULL,
  PRIMARY KEY (`BUDGET_DETAILS_CAL_AMTS_ID`),
  KEY `AWARD_BUDGET_DET_CAL_AMT_FK1` (`BUDGET_DETAILS_ID`),
  KEY `FK98vx4s5dccuxhl27mffw40k7o` (`RATE_CLASS_CODE`,`RATE_TYPE_CODE`),
  CONSTRAINT `AWARD_BUDGET_DET_CAL_AMT_FK1` FOREIGN KEY (`BUDGET_DETAILS_ID`) REFERENCES `award_budget_detail` (`BUDGET_DETAILS_ID`),
  CONSTRAINT `AWARD_BUDGET_DET_CAL_AMT_FK2` FOREIGN KEY (`RATE_CLASS_CODE`) REFERENCES `rate_class` (`RATE_CLASS_CODE`),
  CONSTRAINT `AWARD_BUDGET_DET_CAL_AMT_FK3` FOREIGN KEY (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`) REFERENCES `rate_type` (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_budget_detail` (
  `BUDGET_DETAILS_ID` int NOT NULL AUTO_INCREMENT,
  `BUDGET_HEADER_ID` int DEFAULT NULL,
  `BUDGET_PERIOD_ID` int DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `BUDGET_PERIOD` int DEFAULT NULL,
  `LINE_ITEM_NUMBER` int DEFAULT NULL,
  `START_DATE` date DEFAULT NULL,
  `END_DATE` date DEFAULT NULL,
  `BUDGET_CATEGORY_CODE` varchar(3) DEFAULT NULL,
  `COST_ELEMENT` varchar(12) DEFAULT NULL,
  `LINE_ITEM_DESCRIPTION` varchar(4000) DEFAULT NULL,
  `LINE_ITEM_COST` decimal(12,2) DEFAULT NULL,
  `PREVIOUS_LINE_ITEM_COST` decimal(12,2) DEFAULT NULL,
  `BUDGET_JUSTIFICATION` varchar(2000) DEFAULT NULL,
  `IS_SYSTEM_GENRTED_COST_ELEMENT` varchar(1) DEFAULT NULL,
  `ON_OFF_CAMPUS_FLAG` varchar(1) DEFAULT NULL,
  `SYSTEM_GEN_COST_ELEMENT_TYPE` varchar(60) DEFAULT NULL,
  `FULL_NAME` varchar(90) DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `PERSON_TYPE` varchar(3) DEFAULT NULL,
  `ROLODEX_ID` int DEFAULT NULL,
  `TBN_ID` varchar(9) DEFAULT NULL,
  `IS_APPLY_INFLATION_RATE` varchar(1) DEFAULT NULL,
  `COST_SHARING_AMOUNT` decimal(12,2) DEFAULT NULL,
  `COST_SHARING_PERCENT` decimal(5,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` timestamp(6) NULL DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `COST_ELEMENT_BASE` decimal(12,2) DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `INTERNAL_ORDER_CODE` varchar(50) DEFAULT NULL,
  `QUANTITY` decimal(6,2) DEFAULT NULL,
  `APPROVED_BUDGET` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_DETAILS_ID`),
  KEY `AWARD_BUDGET_DETAIL_FK1` (`BUDGET_PERIOD_ID`),
  KEY `AWARD_BUDGET_DETAIL_FK2` (`BUDGET_CATEGORY_CODE`),
  KEY `AWARD_BUDGET_DETAIL_FK3` (`COST_ELEMENT`),
  KEY `AWARD_BUDGET_DETAIL_FK5` (`TBN_ID`),
  KEY `AWARD_BUDGET_DETAILS_IDX7` (`INTERNAL_ORDER_CODE`),
  KEY `AWARD_BUDGET_DETAILS_IDX4` (`BUDGET_CATEGORY_CODE`),
  KEY `award_budget_detail_IDX1` (`AWARD_NUMBER`,`BUDGET_CATEGORY_CODE`),
  CONSTRAINT `AWARD_BUDGET_DETAIL_FK1` FOREIGN KEY (`BUDGET_PERIOD_ID`) REFERENCES `award_budget_period` (`BUDGET_PERIOD_ID`),
  CONSTRAINT `AWARD_BUDGET_DETAIL_FK2` FOREIGN KEY (`BUDGET_CATEGORY_CODE`) REFERENCES `budget_category` (`BUDGET_CATEGORY_CODE`),
  CONSTRAINT `AWARD_BUDGET_DETAIL_FK3` FOREIGN KEY (`COST_ELEMENT`) REFERENCES `cost_element` (`COST_ELEMENT`),
  CONSTRAINT `AWARD_BUDGET_DETAIL_FK5` FOREIGN KEY (`TBN_ID`) REFERENCES `tbn` (`TBN_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_budget_fund_type` (
  `FUND_TYPE_CODE` varchar(3) NOT NULL,
  `FUND_TYPE` varchar(200) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_DEFAULT` varchar(1) DEFAULT NULL,
  `IS_COST_SHARE_ENABLE` varchar(1) DEFAULT NULL,
  `REFERENCE_COLUMN` varchar(60) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`FUND_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_budget_header` (
  `BUDGET_HEADER_ID` int NOT NULL AUTO_INCREMENT,
  `ANTICIPATED_TOTAL` decimal(12,2) DEFAULT NULL,
  `BUDGET_TYPE_CODE` varchar(3) DEFAULT NULL,
  `COMMENTS` varchar(4000) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `CREATE_USER_NAME` varchar(90) DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `IS_AUTO_CALC` varchar(1) DEFAULT NULL,
  `MODULE_ITEM_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(30) DEFAULT NULL,
  `MODULE_SEQUENCE_NUMBER` int DEFAULT NULL,
  `OBLIGATED_CHANGE` decimal(12,2) DEFAULT NULL,
  `OBLIGATED_TOTAL` decimal(12,2) DEFAULT NULL,
  `ON_OFF_CAMPUS_FLAG` varchar(1) DEFAULT NULL,
  `RATE_CLASS_CODE` varchar(3) DEFAULT NULL,
  `RATE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `TOTAL_COST` decimal(12,2) DEFAULT NULL,
  `TOTAL_DIRECT_COST` decimal(12,2) DEFAULT NULL,
  `TOTAL_INDIRECT_COST` decimal(12,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_USER_NAME` varchar(90) DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `TOTAL_SUBCONTRACT_COST` decimal(12,2) DEFAULT NULL,
  `AWARD_ID` decimal(22,0) DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `FUND_CODE` varchar(10) DEFAULT NULL,
  `FUND_CENTRE` varchar(10) DEFAULT NULL,
  `AWARD_BUDGET_STATUS_CODE` varchar(255) DEFAULT NULL,
  `IS_LATEST_VERSION` varchar(255) DEFAULT NULL,
  `AVAILABLE_FUND_TYPE` varchar(1) DEFAULT NULL,
  `VIREMENT` decimal(5,2) DEFAULT NULL,
  `CUMULATIVE_VIREMENT` decimal(5,2) DEFAULT NULL,
  `TOTAL_COST_SHARE` decimal(12,2) DEFAULT NULL,
  `BUDGET_TEMPLATE_TYPE_ID` int DEFAULT NULL,
  `ON_CAMPUS_RATES` varchar(100) DEFAULT NULL,
  `OFF_CAMPUS_RATES` varchar(100) DEFAULT NULL,
  `COST_SHARE_TYPE_CODE` int DEFAULT NULL,
  `FUND_DISBURSEMENT_BASIS_TYPE_CODE` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_HEADER_ID`),
  KEY `AWARD_BUDGET_HEADER_FK2` (`BUDGET_TYPE_CODE`),
  KEY `FK8vb7fykm0ele33yi26u6bgktr` (`RATE_CLASS_CODE`,`RATE_TYPE_CODE`),
  KEY `AWARD_BUDGET_HEADER_IX5` (`AWARD_ID`),
  KEY `AWARD_BUDGET_HEADER_IX6` (`AWARD_NUMBER`),
  KEY `AWARD_BUDGET_HEADER_FK1` (`AWARD_BUDGET_STATUS_CODE`),
  KEY `AWARD_BUDGET_HEADER_FK4` (`COST_SHARE_TYPE_CODE`),
  KEY `AWARD_BUDGET_HEADER_FK5` (`FUND_DISBURSEMENT_BASIS_TYPE_CODE`),
  KEY `award_budget_header_IDX_1` (`AWARD_ID`,`VERSION_NUMBER`),
  CONSTRAINT `AWARD_BUDGET_HEADER_FK1` FOREIGN KEY (`AWARD_BUDGET_STATUS_CODE`) REFERENCES `award_budget_status` (`AWARD_BUDGET_STATUS_CODE`),
  CONSTRAINT `AWARD_BUDGET_HEADER_FK2` FOREIGN KEY (`BUDGET_TYPE_CODE`) REFERENCES `budget_type` (`BUDGET_TYPE_CODE`),
  CONSTRAINT `AWARD_BUDGET_HEADER_FK3` FOREIGN KEY (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`) REFERENCES `rate_type` (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`),
  CONSTRAINT `AWARD_BUDGET_HEADER_FK4` FOREIGN KEY (`COST_SHARE_TYPE_CODE`) REFERENCES `cost_sharing_type` (`COST_SHARE_TYPE_CODE`),
  CONSTRAINT `AWARD_BUDGET_HEADER_FK5` FOREIGN KEY (`FUND_DISBURSEMENT_BASIS_TYPE_CODE`) REFERENCES `fund_disbursement_basis_type` (`FUND_DISBURSEMENT_BASIS_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_budget_integration_configuration` (
  `CONFIGURATION_KEY` varchar(100) NOT NULL,
  `CONFIGURATION_VALUE` varchar(250) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`CONFIGURATION_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_budget_non_person_detail` (
  `BUDGET_NON_PERSON_DTL_ID` int NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `INTERNAL_ORDER_CODE` varchar(255) DEFAULT NULL,
  `LINE_ITEM_COST` decimal(12,2) DEFAULT NULL,
  `LINE_ITEM_NUMBER` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `BUDGET_DETAILS_ID` int NOT NULL,
  PRIMARY KEY (`BUDGET_NON_PERSON_DTL_ID`),
  KEY `AWARD_BUD_NON_PERSON_DTL_FK1` (`BUDGET_DETAILS_ID`),
  CONSTRAINT `AWARD_BUD_NON_PERSON_DTL_FK1` FOREIGN KEY (`BUDGET_DETAILS_ID`) REFERENCES `award_budget_detail` (`BUDGET_DETAILS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_budget_period` (
  `BUDGET_PERIOD_ID` int NOT NULL AUTO_INCREMENT,
  `BUDGET_PERIOD` int DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `IS_OBLIGATED_PERIOD` varchar(1) DEFAULT NULL,
  `MODULE_ITEM_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(30) DEFAULT NULL,
  `PERIOD_LABEL` varchar(255) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `TOTAL_COST` decimal(12,2) DEFAULT NULL,
  `TOTAL_DIRECT_COST` decimal(12,2) DEFAULT NULL,
  `TOTAL_INDIRECT_COST` decimal(12,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `BUDGET_HEADER_ID` int DEFAULT NULL,
  `SUBCONTRACT_COST` decimal(12,2) DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `DEV_PROPOSAL_ID` int DEFAULT NULL,
  `DEV_PROP_BUDGET_ID` int DEFAULT NULL,
  `DEV_PROP_BUDGET_PERIOD` int DEFAULT NULL,
  PRIMARY KEY (`BUDGET_PERIOD_ID`),
  KEY `AWARD_BUDGET_PERIOD_FK1` (`BUDGET_HEADER_ID`),
  KEY `AWARD_BUDGET_PERIOD_IDX3` (`BUDGET_HEADER_ID`),
  CONSTRAINT `AWARD_BUDGET_PERIOD_FK1` FOREIGN KEY (`BUDGET_HEADER_ID`) REFERENCES `award_budget_header` (`BUDGET_HEADER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_budget_person_det_ca_amt` (
  `BUDGET_PERSON_DET_CAL_AMT_ID` int NOT NULL,
  `BUDGET_PERSON_DETAIL_ID` int DEFAULT NULL,
  `BUDGET_ID` int DEFAULT NULL,
  `BUDGET_PERIOD` int DEFAULT NULL,
  `BUDGET_PERIOD_NUMBER` int DEFAULT NULL,
  `APPLY_RATE_FLAG` varchar(1) DEFAULT NULL,
  `CALCULATED_COST` decimal(12,2) DEFAULT NULL,
  `CALCULATED_COST_SHARING` decimal(12,2) DEFAULT NULL,
  `LINE_ITEM_NUMBER` int DEFAULT NULL,
  `RATE_CLASS_CODE` varchar(3) DEFAULT NULL,
  `RATE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `RATE_TYPE_DESCRIPTION` varchar(200) DEFAULT NULL,
  `APPLICABLE_RATE` decimal(12,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_PERSON_DET_CAL_AMT_ID`),
  KEY `AWARD_BUDGT_PER_DET_CA_AMT_FK1` (`BUDGET_PERSON_DETAIL_ID`),
  KEY `AWARD_BUDGT_PER_DET_CA_AMT_FK3` (`RATE_CLASS_CODE`,`RATE_TYPE_CODE`),
  CONSTRAINT `AWARD_BUDGT_PER_DET_CA_AMT_FK1` FOREIGN KEY (`BUDGET_PERSON_DETAIL_ID`) REFERENCES `award_budget_person_detail` (`BUDGET_PERSON_DETAIL_ID`),
  CONSTRAINT `AWARD_BUDGT_PER_DET_CA_AMT_FK2` FOREIGN KEY (`RATE_CLASS_CODE`) REFERENCES `rate_class` (`RATE_CLASS_CODE`),
  CONSTRAINT `AWARD_BUDGT_PER_DET_CA_AMT_FK3` FOREIGN KEY (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`) REFERENCES `rate_type` (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_budget_person_detail` (
  `BUDGET_PERSON_DETAIL_ID` int NOT NULL AUTO_INCREMENT,
  `BUDGET_DETAILS_ID` int DEFAULT NULL,
  `PERSON_NAME` varchar(90) DEFAULT NULL,
  `PERSON_TYPE` varchar(3) DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `ROLODEX_ID` int DEFAULT NULL,
  `TBN_ID` varchar(9) DEFAULT NULL,
  `UNDERRECOVERY_AMOUNT` decimal(12,2) DEFAULT NULL,
  `PERCENT_CHARGED` decimal(5,2) DEFAULT NULL,
  `PERCENT_EFFORT` decimal(5,2) DEFAULT NULL,
  `COST_SHARING_AMOUNT` decimal(12,2) DEFAULT NULL,
  `COST_SHARING_PERCENT` decimal(5,2) DEFAULT NULL,
  `SALARY_REQUESTED` decimal(12,2) DEFAULT NULL,
  `TOTAL_SALARY` decimal(12,2) DEFAULT NULL,
  `NO_OF_MONTHS` decimal(4,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` timestamp(6) NULL DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `TBN_PERSON_NAME` varchar(90) DEFAULT NULL,
  `BUDGET_PERSON_ID` int DEFAULT NULL,
  `LINE_ITEM_NUMBER` int DEFAULT NULL,
  `END_DATE` datetime(6) DEFAULT NULL,
  `IO_CODE` varchar(255) DEFAULT NULL,
  `START_DATE` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_PERSON_DETAIL_ID`),
  KEY `AWARD_BUDGET_PERSONS_FK1` (`BUDGET_DETAILS_ID`),
  KEY `AWARD_BUDGET_PERSONS_FK2` (`BUDGET_PERSON_ID`),
  CONSTRAINT `AWARD_BUDGET_PERSONS_FK1` FOREIGN KEY (`BUDGET_DETAILS_ID`) REFERENCES `award_budget_detail` (`BUDGET_DETAILS_ID`),
  CONSTRAINT `AWARD_BUDGET_PERSONS_FK2` FOREIGN KEY (`BUDGET_PERSON_ID`) REFERENCES `award_budget_persons` (`BUDGET_PERSON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_budget_persons` (
  `BUDGET_PERSON_ID` int NOT NULL AUTO_INCREMENT,
  `APPOINTMENT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `BUDGET_HEADER_ID` int DEFAULT NULL,
  `TBN_ID` varchar(9) DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `ROLODEX_ID` int DEFAULT NULL,
  `JOB_CODE` varchar(6) DEFAULT NULL,
  `EFFECTIVE_DATE` datetime DEFAULT NULL,
  `CALCULATION_BASE` decimal(12,2) DEFAULT NULL,
  `PERSON_NAME` varchar(90) DEFAULT NULL,
  `NON_EMPLOYEE_FLAG` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `SALARY_ANNIVERSARY_DATE` datetime DEFAULT NULL,
  `PERSON_TYPE` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_PERSON_ID`),
  KEY `AWARD_BUDGET_PERS_FK1` (`JOB_CODE`),
  KEY `AWARD_BUDGET_PERS_FK2` (`APPOINTMENT_TYPE_CODE`),
  KEY `AWARD_BUDGET_PERS_FK3` (`TBN_ID`),
  CONSTRAINT `AWARD_BUDGET_PERS_FK1` FOREIGN KEY (`JOB_CODE`) REFERENCES `job_code` (`JOB_CODE`),
  CONSTRAINT `AWARD_BUDGET_PERS_FK2` FOREIGN KEY (`APPOINTMENT_TYPE_CODE`) REFERENCES `appointment_type` (`APPOINTMENT_TYPE_CODE`),
  CONSTRAINT `AWARD_BUDGET_PERS_FK3` FOREIGN KEY (`TBN_ID`) REFERENCES `tbn` (`TBN_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_budget_rate_and_base` (
  `BUDGET_RATE_AND_BASE_ID` bigint NOT NULL AUTO_INCREMENT,
  `BASE_COST` decimal(12,2) DEFAULT NULL,
  `BUDGET_ID` int DEFAULT NULL,
  `BUDGET_PERIOD` int DEFAULT NULL,
  `BUDGET_PERIOD_NUMBER` int DEFAULT NULL,
  `LINE_ITEM_NUMBER` int DEFAULT NULL,
  `RATE_CLASS_CODE` varchar(3) DEFAULT NULL,
  `RATE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `BUDGET_DETAILS_ID` int DEFAULT NULL,
  `BUDGET_HEADER_ID` int DEFAULT NULL,
  `BUDGET_PERIOD_ID` int DEFAULT NULL,
  `RATE_NUMBER` int DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `ON_OFF_CAMPUS_FLAG` varchar(1) DEFAULT NULL,
  `APPLIED_RATE` decimal(5,2) DEFAULT NULL,
  `BASE_COST_SHARING` decimal(14,2) DEFAULT NULL,
  `CALCULATED_COST` decimal(14,2) DEFAULT NULL,
  `CALCULATED_COST_SHARING` decimal(14,2) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_RATE_AND_BASE_ID`),
  KEY `AWARD_BUDGET_RATE_AND_BASE_FK1` (`BUDGET_DETAILS_ID`),
  CONSTRAINT `AWARD_BUDGET_RATE_AND_BASE_FK1` FOREIGN KEY (`BUDGET_DETAILS_ID`) REFERENCES `award_budget_detail` (`BUDGET_DETAILS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_budget_status` (
  `AWARD_BUDGET_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`AWARD_BUDGET_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_comment` (
  `AWARD_COMMENT_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `COMMENT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `COMMENTS` longtext,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`AWARD_COMMENT_ID`),
  KEY `AWARD_COMMENT_FK1` (`AWARD_ID`),
  CONSTRAINT `AWARD_COMMENT_FK1` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_contact` (
  `AWARD_SPONSOR_CONTACT_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `ROLODEX_ID` int DEFAULT NULL,
  `FULL_NAME` varchar(90) DEFAULT NULL,
  `CONTACT_ROLE_CODE` varchar(12) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `CONTACT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `DESIGNATION` varchar(255) DEFAULT NULL,
  `PERSON_ID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AWARD_SPONSOR_CONTACT_ID`),
  KEY `AWARD_CONTACT_FK1` (`AWARD_ID`),
  KEY `AWARD_CONTACT_FK2` (`CONTACT_TYPE_CODE`),
  KEY `AWARD_CONTACT_FK3` (`ROLODEX_ID`),
  CONSTRAINT `AWARD_CONTACT_FK1` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`),
  CONSTRAINT `AWARD_CONTACT_FK2` FOREIGN KEY (`CONTACT_TYPE_CODE`) REFERENCES `award_contact_type` (`CONTACT_TYPE_CODE`),
  CONSTRAINT `AWARD_CONTACT_FK3` FOREIGN KEY (`ROLODEX_ID`) REFERENCES `rolodex` (`ROLODEX_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_contact_type` (
  `CONTACT_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`CONTACT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_cost_share` (
  `AWARD_COST_SHARE_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` decimal(22,0) DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `PROJECT_PERIOD` varchar(4) DEFAULT NULL,
  `COST_SHARE_PERCENTAGE` decimal(5,2) DEFAULT NULL,
  `COST_SHARE_TYPE_CODE` int DEFAULT NULL,
  `SOURCE` varchar(32) DEFAULT NULL,
  `COMMENTS` varchar(4000) DEFAULT NULL,
  `DESTINATION` varchar(32) DEFAULT NULL,
  `COMMITMENT_AMOUNT` decimal(12,2) DEFAULT NULL,
  `VERIFICATION_DATE` datetime DEFAULT NULL,
  `COST_SHARE_MET` decimal(12,2) DEFAULT NULL,
  `UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `COST_SHARE_DISTRIBUTABLE` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`AWARD_COST_SHARE_ID`),
  KEY `AWARD_COST_SHARE_FK1` (`UNIT_NUMBER`),
  KEY `AWARD_COST_SHARE_FK2` (`COST_SHARE_TYPE_CODE`),
  CONSTRAINT `AWARD_COST_SHARE_FK1` FOREIGN KEY (`UNIT_NUMBER`) REFERENCES `unit` (`UNIT_NUMBER`),
  CONSTRAINT `AWARD_COST_SHARE_FK2` FOREIGN KEY (`COST_SHARE_TYPE_CODE`) REFERENCES `cost_share_type` (`COST_SHARE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_dmp_dataset_rt` (
  `DMP_SET_RT_ID` bigint NOT NULL AUTO_INCREMENT,
  `AWARD_ID` decimal(22,0) DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `ACCOUNT_NUMBER` varchar(100) DEFAULT NULL,
  `AWARD_STATUS` varchar(200) DEFAULT NULL,
  `AWARD_TYPE` varchar(200) DEFAULT NULL,
  `ACTIVITY_TYPE` varchar(200) DEFAULT NULL,
  `AWARD_BEGIN_DATE` date DEFAULT NULL,
  `FINAL_EXPIRATION_DATE` date DEFAULT NULL,
  `AWARD_TITLE` varchar(300) DEFAULT NULL,
  `GRANT_CALL_TITLE` varchar(1000) DEFAULT NULL,
  `SPONSOR_CODE` varchar(6) DEFAULT NULL,
  `SPONSOR_NAME` varchar(200) DEFAULT NULL,
  `PRIME_SPONSOR_CODE` varchar(6) DEFAULT NULL,
  `PRIME_SPONSOR_NAME` varchar(200) DEFAULT NULL,
  `SPONSOR_AWARD_NUMBER` varchar(70) DEFAULT NULL,
  `LEAD_UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `UNIT_NAME` varchar(500) DEFAULT NULL,
  `SUB_LEAD_UNIT` longtext,
  `PI_PERSON_ID` varchar(40) DEFAULT NULL,
  `PI_NAME` varchar(90) DEFAULT NULL,
  `KEY_PERSON_EMAIL` varchar(60) DEFAULT NULL,
  `FUNDING_SCHEME` varchar(1000) DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `QUESTIONNAIRE_ID` int DEFAULT NULL,
  `SUBMISSION_DATE` datetime DEFAULT NULL,
  `LAST_SYNC_TIME` datetime DEFAULT NULL,
  `QUESTION_1_A` longtext,
  `QUESTION_1_B` longtext,
  `QUESTION_2_A` longtext,
  `QUESTION_2_B` longtext,
  `QUESTION_3_A` longtext,
  `QUESTION_3_A9` longtext,
  `QUESTION_3_A8` longtext,
  `QUESTION_3_A7` longtext,
  `QUESTION_3_A6` longtext,
  `QUESTION_3_A5` longtext,
  `QUESTION_3_A4` longtext,
  `QUESTION_3_A3` longtext,
  `QUESTION_3_A2` longtext,
  `QUESTION_3_A1` longtext,
  `QUESTION_3_B` longtext,
  `QUESTION_3_C` longtext,
  `QUESTION_4_A` longtext,
  `QUESTION_4_B1` longtext,
  `QUESTION_4_B2` longtext,
  `QUESTION_4_C` longtext,
  `QUESTION_4_D1` longtext,
  `QUESTION_4_D2` longtext,
  `QUESTION_5_A` longtext,
  `QUESTION_5_B` longtext,
  `QUESTION_5_B1` longtext,
  `QUESTION_5_C` longtext,
  `QUESTION_5_D` longtext,
  `QUESTION_5_E` longtext,
  `QUESTION_5_F` longtext,
  `QUESTION_5_F1` longtext,
  `QUESTION_6_A` longtext,
  `QUESTION_6_A3` longtext,
  `QUESTION_6_A2` longtext,
  `QUESTION_6_A2b` longtext,
  `QUESTION_6_A2a` longtext,
  `QUESTION_6_A1` longtext,
  `QUESTION_6_A1b` longtext,
  `QUESTION_6_A1a` longtext,
  `QUESTION_7_A` longtext,
  `QUESTION_7_A1` longtext,
  `QUESTION_7_B` longtext,
  `QUESTION_7_B2` longtext,
  `QUESTION_7_B1` longtext,
  `QUESTION_8_A` longtext,
  `QUESTION_8_B` longtext,
  `QUESTION_8_C` longtext,
  `QUESTION_9A` longtext,
  `QUESTION_9_A1` longtext,
  `QUESTION_9_A2` longtext,
  `QUESTION_9_A3` longtext,
  `QUESTION_9_A4` longtext,
  `QUESTION_9_A5` longtext,
  `FEED_STATUS` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`DMP_SET_RT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_document_status` (
  `STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_document_type` (
  `AWARD_DOCUMENT_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`AWARD_DOCUMENT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_error_log` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` decimal(22,0) DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `ERROR_MESSAGE` varchar(1000) DEFAULT NULL,
  `PROCEDURE_NAME` varchar(50) DEFAULT NULL,
  `TABLE_NAME` varchar(50) DEFAULT NULL,
  `SECTION_CODE` varchar(3) DEFAULT NULL,
  `VARIATION_TYPE_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_exp_transactions_rt_log` (
  `EXPENSE_TRACKER_LOG_ID` int NOT NULL AUTO_INCREMENT,
  `PO_PR_FLAG` varchar(255) DEFAULT NULL,
  `ACCOUNT_NUMBER` varchar(255) DEFAULT NULL,
  `ACTUAL_OR_COMMITTED_FLAG` varchar(255) DEFAULT NULL,
  `AMOUNT_IN_FMA_CURRENCY` decimal(12,2) DEFAULT NULL,
  `BANK_CLEARING_DATE` varchar(255) DEFAULT NULL,
  `CREATE_STATUS` varchar(255) DEFAULT NULL,
  `DOCUMENT_DATE` varchar(255) DEFAULT NULL,
  `DOCUMENT_NUMBER` varchar(255) DEFAULT NULL,
  `ERROR_MESSAGE` varchar(255) DEFAULT NULL,
  `ERROR_STATUS` varchar(255) DEFAULT NULL,
  `FI_GL_ACCOUNT` varchar(255) DEFAULT NULL,
  `FI_GL_DESCRIPTION` varchar(255) DEFAULT NULL,
  `FI_POSTING_DATE` varchar(255) DEFAULT NULL,
  `FILE_ID` int DEFAULT NULL,
  `FM_POSTING_DATE` varchar(255) DEFAULT NULL,
  `GR_DATE` varchar(255) DEFAULT NULL,
  `INTERNAL_ORDER_CODE` varchar(255) DEFAULT NULL,
  `INVOICE_DATE` varchar(255) DEFAULT NULL,
  `PO_DATE` varchar(255) DEFAULT NULL,
  `PR_DATE` varchar(255) DEFAULT NULL,
  `REMARKS` varchar(255) DEFAULT NULL,
  `REQUESTER_NAME` varchar(255) DEFAULT NULL,
  `TRANSACTION_REFERENCE_NUMBER` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `VENDOR_CODE` varchar(255) DEFAULT NULL,
  `VENDOR_NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`EXPENSE_TRACKER_LOG_ID`),
  KEY `FILE_ID` (`FILE_ID`),
  CONSTRAINT `FILE_ID` FOREIGN KEY (`FILE_ID`) REFERENCES `award_expense_files` (`FILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_expense_details` (
  `AWARD_NUMBER` varchar(12) NOT NULL,
  `ACCOUNT_NUMBER` varchar(100) NOT NULL,
  `INTERNAL_ORDER_CODE` varchar(50) NOT NULL,
  `TOTAL_EXPENSE_AMOUNT` decimal(12,2) DEFAULT NULL,
  `TOTAL_LOGGED_HOURS` decimal(6,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`AWARD_NUMBER`,`ACCOUNT_NUMBER`,`INTERNAL_ORDER_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_expense_details_ext` (
  `AWARD_NUMBER` varchar(12) NOT NULL,
  `ACCOUNT_NUMBER` varchar(100) NOT NULL,
  `INTERNAL_ORDER_CODE` varchar(50) NOT NULL,
  `COMMITTED_AMOUNT` decimal(12,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_FROM_SAP` varchar(1) DEFAULT NULL,
  `DESCRIPTION` varchar(4000) DEFAULT NULL,
  `AWARD_EXPENSE_DETAILS_EXT_ID` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`AWARD_EXPENSE_DETAILS_EXT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_expense_error_log` (
  `ERROR_LOG_ID` int NOT NULL AUTO_INCREMENT,
  `SAP_ID` varchar(20) DEFAULT NULL,
  `ACCOUNT_NUMBER` varchar(100) DEFAULT NULL,
  `INTERNAL_ORDER_CODE` varchar(50) DEFAULT NULL,
  `ERROR_MESSAGE` varchar(4000) DEFAULT NULL,
  `PROCEDURE_NAME` varchar(30) DEFAULT NULL,
  `FILE_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  PRIMARY KEY (`ERROR_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_expense_files` (
  `FILE_ID` int NOT NULL AUTO_INCREMENT,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `NO_OF_RECORDS` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `INSERTED_ROWS` int DEFAULT NULL,
  `INSERTED_IN_RT` varchar(1) DEFAULT NULL,
  `SYSTEM_ARCHIVED` varchar(1) DEFAULT NULL,
  `REMOTE_ARCHIVED` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`FILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_expense_header` (
  `AWARD_NUMBER` varchar(12) NOT NULL,
  `ACCOUNT_NUMBER` varchar(100) NOT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `LAST_SYNCH_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`AWARD_NUMBER`,`ACCOUNT_NUMBER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_expense_trans_amt_rt` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `INTERNAL_ORDER_CODE` varchar(20) DEFAULT NULL,
  `BUDGET_CATEGORY_CODE` varchar(3) DEFAULT NULL,
  `FM_POSTING_DATE` date DEFAULT NULL,
  `ACTUAL_AMOUNT` decimal(12,2) DEFAULT NULL,
  `COMMITTED_AMOUNT` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `AWARD_EXPENSE_TRANS_AMT_RT_IDX_1` (`AWARD_NUMBER`,`BUDGET_CATEGORY_CODE`,`ACTUAL_AMOUNT`,`COMMITTED_AMOUNT`),
  KEY `AWARD_EXPENSE_TRANS_AMT_RT_IDX_2` (`FM_POSTING_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_expense_transactions` (
  `AWARD_EXPENSE_TRANS_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `ACCOUNT_NUMBER` varchar(100) DEFAULT NULL,
  `INTERNAL_ORDER_CODE` varchar(50) DEFAULT NULL,
  `REMARKS` varchar(1000) DEFAULT NULL,
  `AMOUNT_IN_FMA_CURRENCY` decimal(12,2) DEFAULT NULL,
  `ACTUAL_OR_COMMITTED_FLAG` varchar(1) DEFAULT NULL,
  `PO_PR_FLAG` varchar(3) DEFAULT NULL,
  `BANK_CLEARING_DATE` datetime DEFAULT NULL,
  `FM_POSTING_DATE` datetime DEFAULT NULL,
  `FI_POSTING_DATE` datetime DEFAULT NULL,
  `GL_DESCRIPTION` varchar(200) DEFAULT NULL,
  `ASSET` varchar(20) DEFAULT NULL,
  `ASSET_INTERNAL_ORDER` varchar(100) DEFAULT NULL,
  `COMMITMENT_ITEM` varchar(200) DEFAULT NULL,
  `VENDOR_CODE` varchar(50) DEFAULT NULL,
  `VENDOR_NAME` varchar(300) DEFAULT NULL,
  `DOCUMENT_DATE` datetime DEFAULT NULL,
  `FI_GL_ACCOUNT` varchar(50) DEFAULT NULL,
  `FI_GL_DESCRIPTION` varchar(200) DEFAULT NULL,
  `GR_DATE` datetime DEFAULT NULL,
  `INVOICE_DATE` datetime DEFAULT NULL,
  `PR_DATE` datetime DEFAULT NULL,
  `REQUESTER_NAME` varchar(90) DEFAULT NULL,
  `TC_AMOUNT` decimal(12,2) DEFAULT NULL,
  `TRANSACTION_CURRENCY` varchar(20) DEFAULT NULL,
  `ASSET_LOCATION` varchar(200) DEFAULT NULL,
  `PREDECESSOR_DOC_NUMBER` varchar(50) DEFAULT NULL,
  `PREDECESSOR_DOC_ITEM` varchar(200) DEFAULT NULL,
  `PREDECESSOR_DOC_ITEM_ACCOUNT` varchar(100) DEFAULT NULL,
  `PREDECESSOR_DOC_ITEM_ORG_UNIT` varchar(20) DEFAULT NULL,
  `PREDECESSOR_DOC_CAT` varchar(20) DEFAULT NULL,
  `PREDECESSOR_DOC_CAT_TEXT` varchar(200) DEFAULT NULL,
  `FM_DOC_NUMBER` varchar(20) DEFAULT NULL,
  `INVOICE_NUMBER` varchar(20) DEFAULT NULL,
  `REFERENCE_DOC_NUMBER` varchar(20) DEFAULT NULL,
  `PO_NUMBER` varchar(20) DEFAULT NULL,
  `PO_DATE` datetime DEFAULT NULL,
  `ASSET_COST_CENTER` varchar(50) DEFAULT NULL,
  `ASSET_LOCATION_CODE` varchar(200) DEFAULT NULL,
  `PO_GL_ACCOUNT` varchar(50) DEFAULT NULL,
  `PO_GL_DESCRIPTION` varchar(200) DEFAULT NULL,
  `FI_PO_COST_CENTRE` varchar(20) DEFAULT NULL,
  `VENDOR_CLEARING_DATE` datetime DEFAULT NULL,
  `FUND_CENTRE` varchar(10) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `TRANSACTION_REFERENCE_NUMBER` varchar(100) DEFAULT NULL,
  `DOCUMENT_NUMBER` varchar(100) DEFAULT NULL,
  `FILE_ID` int DEFAULT NULL,
  `ITEM_NUMBER` varchar(5) DEFAULT NULL,
  `REFERENCE_DOCUMENT_CATEGORY` varchar(3) DEFAULT NULL,
  `REFERENCE_ORG_UNIT` varchar(10) DEFAULT NULL,
  `ACCT_ASSIGNMENT_NUMBER` varchar(5) DEFAULT NULL,
  `SCHEDULE_LINE_NUMBER` varchar(4) DEFAULT NULL,
  `CONDITION_COUNTER` varchar(5) DEFAULT NULL,
  `REFERENCE_PROCEDURE` varchar(5) DEFAULT NULL,
  `DOCUMENT_NUMBER_FM_LINE_ITEM` varchar(10) DEFAULT NULL,
  `NUMBER_OF_POST_FM_LINE_ITEM` varchar(3) DEFAULT NULL,
  `COMPANY_CODE` varchar(4) DEFAULT NULL,
  `VALUE_TYPE` varchar(2) DEFAULT NULL,
  `AMOUNT_TYPE` varchar(4) DEFAULT NULL,
  `FISCAL_YEAR` varchar(4) DEFAULT NULL,
  `TRANSACTION_NUMBER` varchar(40) DEFAULT NULL,
  `FM_AREA` varchar(4) DEFAULT NULL,
  `FUND` varchar(10) DEFAULT NULL,
  `FUND_CENTER` varchar(16) DEFAULT NULL,
  `FUNCTIONAL_AREA` varchar(16) DEFAULT NULL,
  `BATCH_ID` varchar(100) DEFAULT NULL,
  `FAST_ID` varchar(20) DEFAULT NULL,
  `BUDGET_CATEGORY_CODE` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`AWARD_EXPENSE_TRANS_ID`),
  UNIQUE KEY `award_expense_transactions_uk` (`DOCUMENT_NUMBER`,`ITEM_NUMBER`,`REFERENCE_DOCUMENT_CATEGORY`,`REFERENCE_ORG_UNIT`,`ACCT_ASSIGNMENT_NUMBER`,`SCHEDULE_LINE_NUMBER`,`CONDITION_COUNTER`,`REFERENCE_PROCEDURE`,`DOCUMENT_NUMBER_FM_LINE_ITEM`,`NUMBER_OF_POST_FM_LINE_ITEM`,`COMPANY_CODE`,`VALUE_TYPE`,`AMOUNT_TYPE`,`FISCAL_YEAR`,`TRANSACTION_NUMBER`),
  KEY `award_expense_transactions_idx1` (`AWARD_NUMBER`),
  KEY `award_expense_transactions_idx2` (`ACCOUNT_NUMBER`),
  KEY `award_expense_transactions_idx3` (`INTERNAL_ORDER_CODE`),
  KEY `AWARD_EXPENSE_TRANSACTIONS_IDX4` (`FILE_ID`),
  KEY `AWARD_EXPENSE_TRANSACTIONS_IDX5` (`DOCUMENT_NUMBER`,`ITEM_NUMBER`,`REFERENCE_DOCUMENT_CATEGORY`,`REFERENCE_ORG_UNIT`,`ACCT_ASSIGNMENT_NUMBER`,`SCHEDULE_LINE_NUMBER`,`CONDITION_COUNTER`,`REFERENCE_PROCEDURE`,`DOCUMENT_NUMBER_FM_LINE_ITEM`,`NUMBER_OF_POST_FM_LINE_ITEM`,`COMPANY_CODE`,`VALUE_TYPE`,`AMOUNT_TYPE`,`FISCAL_YEAR`,`TRANSACTION_NUMBER`),
  KEY `AWARD_EXPENSE_TRANSACTIONS_IDX6` (`ACTUAL_OR_COMMITTED_FLAG`),
  KEY `AWARD_EXPENSE_TRANSACTIONS_IDX7` (`FM_POSTING_DATE`),
  KEY `AWARD_EXPENSE_TRANSACTIONS_IDX_2` (`AWARD_NUMBER`,`BUDGET_CATEGORY_CODE`,`ACTUAL_OR_COMMITTED_FLAG`),
  KEY `AWARD_EXPENSE_TRANSACTIONS_IDX_3` (`AWARD_NUMBER`,`INTERNAL_ORDER_CODE`,`ACTUAL_OR_COMMITTED_FLAG`),
  KEY `AWARD_EXPENSE_TRANSACTIONS_IDX_4` (`INTERNAL_ORDER_CODE`),
  KEY `AWARD_EXPENSE_TRANSACTIONS_IDX_5` (`ACCOUNT_NUMBER`),
  KEY `AWARD_EXPENSE_TRANSACTIONS_IDX_6` (`AWARD_NUMBER`),
  KEY `AWARD_EXPENSE_TRANSACTIONS_IDX_7` (`AWARD_NUMBER`,`INTERNAL_ORDER_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_expense_transactions_rt` (
  `EXPENSE_TRACKER_ID` int NOT NULL AUTO_INCREMENT,
  `PO_PR_FLAG` varchar(255) DEFAULT NULL,
  `ACCOUNT_NUMBER` varchar(255) DEFAULT NULL,
  `ACTUAL_OR_COMMITTED_FLAG` varchar(255) DEFAULT NULL,
  `AMOUNT_IN_FMA_CURRENCY` decimal(12,2) DEFAULT NULL,
  `BANK_CLEARING_DATE` varchar(255) DEFAULT NULL,
  `CREATE_STATUS` varchar(255) DEFAULT NULL,
  `DOCUMENT_DATE` varchar(255) DEFAULT NULL,
  `DOCUMENT_NUMBER` varchar(255) DEFAULT NULL,
  `ERROR_MESSAGE` varchar(255) DEFAULT NULL,
  `ERROR_STATUS` varchar(255) DEFAULT NULL,
  `FI_GL_ACCOUNT` varchar(255) DEFAULT NULL,
  `FI_GL_DESCRIPTION` varchar(255) DEFAULT NULL,
  `FI_POSTING_DATE` varchar(255) DEFAULT NULL,
  `FILE_ID` int DEFAULT NULL,
  `FM_POSTING_DATE` varchar(255) DEFAULT NULL,
  `GR_DATE` varchar(255) DEFAULT NULL,
  `INTERNAL_ORDER_CODE` varchar(255) DEFAULT NULL,
  `INVOICE_DATE` varchar(255) DEFAULT NULL,
  `PO_DATE` varchar(255) DEFAULT NULL,
  `PR_DATE` varchar(255) DEFAULT NULL,
  `REMARKS` varchar(255) DEFAULT NULL,
  `REQUESTER_NAME` varchar(255) DEFAULT NULL,
  `TRANSACTION_REFERENCE_NUMBER` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `VENDOR_CODE` varchar(255) DEFAULT NULL,
  `VENDOR_NAME` varchar(255) DEFAULT NULL,
  `ACCT_ASSIGNMENT_NUMBER` varchar(255) DEFAULT NULL,
  `AMOUNT_TYPE` varchar(255) DEFAULT NULL,
  `BATCH_ID` varchar(255) DEFAULT NULL,
  `COMPANY_CODE` varchar(255) DEFAULT NULL,
  `CONDITION_COUNTER` varchar(255) DEFAULT NULL,
  `DOCUMENT_NUMBER_FM_LINE_ITEM` varchar(255) DEFAULT NULL,
  `FISCAL_YEAR` varchar(255) DEFAULT NULL,
  `FM_AREA` varchar(255) DEFAULT NULL,
  `FUNCTIONAL_AREA` varchar(255) DEFAULT NULL,
  `FUND` varchar(255) DEFAULT NULL,
  `FUND_CENTER` varchar(255) DEFAULT NULL,
  `ITEM_NUMBER` varchar(255) DEFAULT NULL,
  `NUMBER_OF_POST_FM_LINE_ITEM` varchar(255) DEFAULT NULL,
  `REFERENCE_DOCUMENT_CATEGORY` varchar(255) DEFAULT NULL,
  `REFERENCE_ORG_UNIT` varchar(255) DEFAULT NULL,
  `REFERENCE_PROCEDURE` varchar(255) DEFAULT NULL,
  `SCHEDULE_LINE_NUMBER` varchar(255) DEFAULT NULL,
  `TRANSACTION_NUMBER` varchar(255) DEFAULT NULL,
  `VALUE_TYPE` varchar(255) DEFAULT NULL,
  `AWARD_NUMBER` varchar(255) DEFAULT NULL,
  `PREDECESSOR_DOC_NUMBER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`EXPENSE_TRACKER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_funding_proposals` (
  `AWARD_FUNDING_PROPOSAL_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `PROPOSAL_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` timestamp(6) NULL DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AWARD_FUNDING_PROPOSAL_ID`),
  KEY `AWARD_FUNDING_PROPOSALS_FK2` (`PROPOSAL_ID`),
  KEY `AWARD_FUNDING_PROPOSALS_IX18` (`AWARD_ID`),
  CONSTRAINT `AWARD_FUNDING_PROPOSALS_FK1` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`),
  CONSTRAINT `AWARD_FUNDING_PROPOSALS_FK2` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `proposal` (`PROPOSAL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_hierarchy` (
  `AWARD_HIERARCHY_ID` int NOT NULL AUTO_INCREMENT,
  `ROOT_AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `PARENT_AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `ORIGINATING_AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  PRIMARY KEY (`AWARD_HIERARCHY_ID`),
  KEY `AWARD_HIERARCHY_IDX_1` (`ROOT_AWARD_NUMBER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_history_log` (
  `AWARD_HISTORY_LOG_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `LAST_MERGED_AWARD_CREATON` decimal(22,0) DEFAULT NULL,
  `LAST_MERGED_AWARD_APPRVAL` decimal(22,0) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`AWARD_HISTORY_LOG_ID`),
  KEY `AWARD_HISTORY_LOG_FK1` (`AWARD_ID`),
  CONSTRAINT `AWARD_HISTORY_LOG_FK1` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_hours_log_rt` (
  `AWARD_HOURS_LOG_ID` int NOT NULL AUTO_INCREMENT,
  `FUNDS_CENTER` varchar(255) DEFAULT NULL,
  `FUND_CODE` varchar(255) DEFAULT NULL,
  `IO_CODE` varchar(255) DEFAULT NULL,
  `PAYROLL_HOURS` int DEFAULT NULL,
  `SUBMITTED_HOURS` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `WORK_DATE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AWARD_HOURS_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_hours_logged` (
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `ACCOUNT_NUMBER` varchar(100) DEFAULT NULL,
  `INTERNAL_ORDER_CODE` varchar(50) DEFAULT NULL,
  `SUBMITTED_HOURS` decimal(10,2) DEFAULT NULL,
  `PAYROLL_HOURS` decimal(10,2) DEFAULT NULL,
  `SUBMITTED_DATE` datetime DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `HOURS_LOGGED_ID` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_keyperson_timesheet` (
  `KEYPERSON_TIMESHEET_ID` int NOT NULL AUTO_INCREMENT,
  `YEAR` varchar(5) DEFAULT NULL,
  `AWARD_ID` decimal(22,0) DEFAULT NULL,
  `AWARD_NUMBER` varchar(20) DEFAULT NULL,
  `AWARD_PERSON_ID` int DEFAULT NULL,
  `VALUE` decimal(5,2) DEFAULT NULL,
  `TIMESHEET_TYPE` varchar(60) DEFAULT NULL,
  `ORDER_NUMBER` int DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`KEYPERSON_TIMESHEET_ID`),
  KEY `AWARD_KEYPERSON_TIMESHEET_FK1` (`AWARD_PERSON_ID`),
  CONSTRAINT `AWARD_KEYPERSON_TIMESHEET_FK1` FOREIGN KEY (`AWARD_PERSON_ID`) REFERENCES `award_persons` (`AWARD_PERSON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_kpi` (
  `AWARD_KPI_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(255) DEFAULT NULL,
  `KPI_TYPE_CODE` varchar(255) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AWARD_KPI_ID`),
  KEY `AWARD_KPI_FK1` (`KPI_TYPE_CODE`),
  CONSTRAINT `AWARD_KPI_FK1` FOREIGN KEY (`KPI_TYPE_CODE`) REFERENCES `kpi_type` (`KPI_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_kpi_criteria` (
  `AWARD_KPI_CRITERIA_ID` int NOT NULL AUTO_INCREMENT,
  `KPI_CRITERIA_TYPE_CODE` varchar(255) DEFAULT NULL,
  `TARGET` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `AWARD_KPI_ID` int DEFAULT NULL,
  PRIMARY KEY (`AWARD_KPI_CRITERIA_ID`),
  KEY `AWARD_KPI_CRITERIA_FK1` (`AWARD_KPI_ID`),
  KEY `AWARD_KPI_CRITERIA_FK2` (`KPI_CRITERIA_TYPE_CODE`),
  CONSTRAINT `AWARD_KPI_CRITERIA_FK1` FOREIGN KEY (`AWARD_KPI_ID`) REFERENCES `award_kpi` (`AWARD_KPI_ID`),
  CONSTRAINT `AWARD_KPI_CRITERIA_FK2` FOREIGN KEY (`KPI_CRITERIA_TYPE_CODE`) REFERENCES `kpi_criteria_type` (`KPI_CRITERIA_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_latest_aproved_budget_rt` (
  `BUDGET_RT_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` decimal(22,0) NOT NULL,
  `AWARD_NUMBER` varchar(12) NOT NULL,
  `BUDGET_CATEGORY_CODE` varchar(3) NOT NULL,
  `LATEST_APPROVED_BUDGET` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_RT_ID`),
  KEY `AWARD_LATEST_APROVED_BUDGET_RT_IDX1` (`AWARD_NUMBER`,`BUDGET_CATEGORY_CODE`),
  KEY `AWARD_LATEST_APROVED_BUDGET_RT_IDX3` (`AWARD_ID`,`BUDGET_CATEGORY_CODE`,`LATEST_APPROVED_BUDGET`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_manpower` (
  `AWARD_MANPOWER_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` decimal(22,0) DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `MANPOWER_TYPE_CODE` varchar(3) DEFAULT NULL,
  `BUDGET_REFERENCE_NUMBER` varchar(50) DEFAULT NULL,
  `BUDGET_REFERENCE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `BUDGET_VERSION_NUMBER` int DEFAULT NULL,
  `ACTUAL_HEADCOUNT` int DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `SUB_MODULE_CODE` int DEFAULT NULL,
  PRIMARY KEY (`AWARD_MANPOWER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_manpower_payroll` (
  `PAYROLL_ID` int NOT NULL AUTO_INCREMENT,
  `GL_ACCOUNT_CODE` varchar(50) DEFAULT NULL,
  `EMPLOYEE_NUMBER` varchar(15) DEFAULT NULL,
  `INTERNAL_ORDER_CODE` varchar(50) DEFAULT NULL,
  `COST_SHARING` decimal(5,2) DEFAULT NULL,
  `PAY_ELEMENT_CODE` varchar(15) DEFAULT NULL,
  `PAY_ELEMENT` varchar(100) DEFAULT NULL,
  `AMOUNT` varchar(200) DEFAULT NULL,
  `PAYROLL_PERIOD` varchar(6) DEFAULT NULL,
  `FILE_ID` int DEFAULT NULL,
  `update_timestamp` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `REMARKS` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`PAYROLL_ID`),
  KEY `IDX_AWARD_MANPOWER_PAYROLL` (`EMPLOYEE_NUMBER`,`INTERNAL_ORDER_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_manpower_resource` (
  `AWARD_MANPOWER_RESOURCE_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_MANPOWER_ID` int DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `ROLODEX_ID` int DEFAULT NULL,
  `POSITION_ID` varchar(50) DEFAULT NULL,
  `FULL_NAME` varchar(90) DEFAULT NULL,
  `POSITION_STATUS_CODE` varchar(3) DEFAULT NULL,
  `COST_ALLOCATION` decimal(5,2) DEFAULT NULL,
  `PLAN_COMPENSATION_TYPE_CODE` varchar(3) DEFAULT NULL,
  `PLAN_JOB_PROFILE_TYPE_CODE` varchar(20) DEFAULT NULL,
  `COMPENSATION_GRADE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `JOB_PROFILE_TYPE_CODE` varchar(20) DEFAULT NULL,
  `PLAN_START_DATE` datetime DEFAULT NULL,
  `PLAN_END_DATE` datetime DEFAULT NULL,
  `PLAN_DURATION` varchar(50) DEFAULT NULL,
  `CHARGE_START_DATE` datetime DEFAULT NULL,
  `CHARGE_END_DATE` datetime DEFAULT NULL,
  `CHARGE_DURATION` varchar(50) DEFAULT NULL,
  `COMMITTED_COST` decimal(12,2) DEFAULT NULL,
  `RESOURCE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `DESCRIPTION` varchar(4000) DEFAULT NULL,
  `CANDIDATE_TITLE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `PLANNED_BASE_SALARY` decimal(12,2) DEFAULT NULL,
  `PLANNED_SALARY` decimal(12,2) DEFAULT NULL,
  `RESOURCE_UNIQUE_ID` varchar(100) DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `SUB_MODULE_CODE` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `FREEZE_DATE` datetime DEFAULT NULL,
  `MULTIPLIER_USED` decimal(12,2) DEFAULT NULL,
  `PREVIOUS_CHARGE_END_DATE` datetime DEFAULT NULL,
  `PREVIOUS_CHARGE_START_DATE` datetime DEFAULT NULL,
  `BASE_SALARY_USED` varchar(200) DEFAULT NULL,
  `IS_REMAINING_CA_FROM_WBS` varchar(1) DEFAULT NULL,
  `IS_RESOURCE_CREATED_OR_UPDATED` varchar(1) DEFAULT NULL,
  `IS_MULTI_ACCOUNT` varchar(1) DEFAULT NULL,
  `IS_MAIN_ACCOUNT` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`AWARD_MANPOWER_RESOURCE_ID`),
  KEY `AWARD_MANPOWER_RESOURCE_FK1` (`AWARD_MANPOWER_ID`),
  KEY `AWARD_MANPOWER_RESOURCE_FK2` (`PLAN_COMPENSATION_TYPE_CODE`),
  KEY `AWARD_MANPOWER_RESOURCE_FK3` (`PLAN_JOB_PROFILE_TYPE_CODE`),
  KEY `AWARD_MANPOWER_RESOURCE_FK4` (`COMPENSATION_GRADE_TYPE_CODE`),
  KEY `AWARD_MANPOWER_RESOURCE_FK5` (`JOB_PROFILE_TYPE_CODE`),
  KEY `AWARD_MANPOWER_RESOURCE_FK6` (`RESOURCE_TYPE_CODE`),
  KEY `AWARD_MANPOWER_RESOURCE_FK7` (`CANDIDATE_TITLE_TYPE_CODE`),
  KEY `AWARD_MANPOWER_RESOURCE_FK8` (`POSITION_STATUS_CODE`),
  CONSTRAINT `AWARD_MANPOWER_RESOURCE_FK1` FOREIGN KEY (`AWARD_MANPOWER_ID`) REFERENCES `award_manpower` (`AWARD_MANPOWER_ID`),
  CONSTRAINT `AWARD_MANPOWER_RESOURCE_FK2` FOREIGN KEY (`PLAN_COMPENSATION_TYPE_CODE`) REFERENCES `manpower_compensation_type` (`COMPENSATION_TYPE_CODE`),
  CONSTRAINT `AWARD_MANPOWER_RESOURCE_FK3` FOREIGN KEY (`PLAN_JOB_PROFILE_TYPE_CODE`) REFERENCES `manpower_job_profile_type` (`JOB_PROFILE_TYPE_CODE`),
  CONSTRAINT `AWARD_MANPOWER_RESOURCE_FK4` FOREIGN KEY (`COMPENSATION_GRADE_TYPE_CODE`) REFERENCES `manpower_compensation_type` (`COMPENSATION_TYPE_CODE`),
  CONSTRAINT `AWARD_MANPOWER_RESOURCE_FK5` FOREIGN KEY (`JOB_PROFILE_TYPE_CODE`) REFERENCES `manpower_job_profile_type` (`JOB_PROFILE_TYPE_CODE`),
  CONSTRAINT `AWARD_MANPOWER_RESOURCE_FK6` FOREIGN KEY (`RESOURCE_TYPE_CODE`) REFERENCES `manpower_resource_type` (`RESOURCE_TYPE_CODE`),
  CONSTRAINT `AWARD_MANPOWER_RESOURCE_FK7` FOREIGN KEY (`CANDIDATE_TITLE_TYPE_CODE`) REFERENCES `manpower_candidate_title_type` (`CANDIDATE_TITLE_TYPE_CODE`),
  CONSTRAINT `AWARD_MANPOWER_RESOURCE_FK8` FOREIGN KEY (`POSITION_STATUS_CODE`) REFERENCES `manpower_position_status` (`POSITION_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_master_dataset_rt` (
  `AWARD_MASTER_SET_ID` int NOT NULL AUTO_INCREMENT,
  `ACCOUNT_NUMBER` varchar(100) DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `AWARD_ID` decimal(22,0) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `TITLE` varchar(300) DEFAULT NULL,
  `AWARD_STATUS` varchar(200) DEFAULT NULL,
  `ACCOUNT_TYPE` varchar(200) DEFAULT NULL,
  `AWARD_TYPE` varchar(200) DEFAULT NULL,
  `ACTIVITY_TYPE` varchar(200) DEFAULT NULL,
  `LEAD_UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `unit_name` varchar(200) DEFAULT NULL,
  `GRANT_CALL_TITLE` varchar(1000) DEFAULT NULL,
  `SPONSOR_CODE` varchar(6) DEFAULT NULL,
  `SPONSOR_NAME` varchar(200) DEFAULT NULL,
  `PRIME_SPONSOR_CODE` varchar(6) DEFAULT NULL,
  `PRIME_SPONSOR_NAME` varchar(200) DEFAULT NULL,
  `AWARD_EXECUTION_DATE` date DEFAULT NULL,
  `AWARD_EFFECTIVE_DATE` date DEFAULT NULL,
  `FINAL_EXPIRATION_DATE` date DEFAULT NULL,
  `AWARD_SEQUENCE_STATUS` varchar(10) DEFAULT NULL,
  `KEY_PERSON_NAME` varchar(90) DEFAULT NULL,
  `KEY_PERSON_ID` varchar(40) DEFAULT NULL,
  `PERSON_ROLE_ID` int DEFAULT NULL,
  `PERSON_ROLE` varchar(50) DEFAULT NULL,
  `ANTICIPATED_TOTAL_AMOUNT` decimal(12,2) DEFAULT NULL,
  `AMOUNT_OBLIGATED_TO_DATE` decimal(12,2) DEFAULT NULL,
  `CURRENT_FUND_EFFECTIVE_DATE` date DEFAULT NULL,
  `OBLIGATION_EXPIRATION_DATE` date DEFAULT NULL,
  `BEGIN_DATE` date DEFAULT NULL,
  `EMPLOYEE_FLAG` varchar(1) DEFAULT NULL,
  `PI_PERSON_ID` varchar(40) DEFAULT NULL,
  `PI_NAME` varchar(90) DEFAULT NULL,
  `PI_HOME_UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `PI_HOME_UNIT` varchar(100) DEFAULT NULL,
  `AWARD_WORKFLOW_STATUS_CODE` varchar(3) DEFAULT NULL,
  `AWARD_WORKFLOW_STATUS` varchar(200) DEFAULT NULL,
  `SPONSOR_AWARD_NUMBER` varchar(70) DEFAULT NULL,
  `KEY_PERSON_ORGANIZATION` varchar(200) DEFAULT NULL,
  `KEY_PERSON_DEPARTMENT_NUMBER` varchar(8) DEFAULT NULL,
  `KEY_PERSON_DEPARTMENT` varchar(100) DEFAULT NULL,
  `KEY_PERSON_COUNTRY` varchar(100) DEFAULT NULL,
  `KEY_PERSON_EMAIL` varchar(60) DEFAULT NULL,
  `TOTAL_COST_SHARE` decimal(12,2) DEFAULT NULL,
  `TOTAL_PROJECT_COST` decimal(12,2) DEFAULT NULL,
  `TOTAL_COST_IN_CURRENCY` decimal(12,2) DEFAULT NULL,
  `TOTAL_COST` decimal(12,2) DEFAULT NULL,
  `TOTAL_DIRECT_COST` decimal(12,2) DEFAULT NULL,
  `TOTAL_INDIRECT_COST` decimal(12,2) DEFAULT NULL,
  `BASIS_OF_PAYMENT_DESC` varchar(200) DEFAULT NULL,
  `METHOD_OF_PAYMENT_DESC` varchar(200) DEFAULT NULL,
  `PAYMENT_INVOICE_FREQ_DESC` varchar(200) DEFAULT NULL,
  `RATE_CLASS_CODE` varchar(200) DEFAULT NULL,
  `AWARD_CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `AWARD_CREATE_USER` varchar(60) DEFAULT NULL,
  `STEM_NONSTEM` longtext,
  `RIE_DOMAIN` longtext,
  `INPUT_GST_CATEGORY` longtext,
  `OUTPUT_GST_CATEGORY` longtext,
  `GRANT_CODE` longtext,
  `SUB_LEAD_UNIT` longtext,
  `PROFIT_CENTER` longtext,
  `FUND_CENTER` longtext,
  `COST_CENTER` longtext,
  `CUMULATIVE_REVENUE` decimal(12,2) DEFAULT NULL,
  `CUMULATIVE_ACTUAL_EXPENSE` decimal(12,2) DEFAULT NULL,
  `CUMULATIVE_COMMITTED_EXPENSE` decimal(12,2) DEFAULT NULL,
  `BALANCE_FUND` decimal(12,2) DEFAULT NULL,
  `BALANCE_LESS_COMMITTED_AMOUNT` decimal(12,2) DEFAULT NULL,
  `ORIGINAL_APPROVED_BUDGET` decimal(12,2) DEFAULT NULL,
  `LATEST_APPROVED_BUDGET` decimal(12,2) DEFAULT NULL,
  `UTILIZATION_RATE` varchar(20) DEFAULT NULL,
  `DURATION` varchar(50) DEFAULT NULL,
  `PERCENTAGE_OF_EFFORT` decimal(5,2) DEFAULT NULL,
  `CUMULATIVE_VIREMENT` decimal(5,2) DEFAULT NULL,
  `DISPLAY_AT_ACAD_PROFILE` varchar(50) DEFAULT NULL,
  `GRANT_PER_MONTH` decimal(12,2) DEFAULT NULL,
  `F_AND_A_RATE_TYPE` varchar(200) DEFAULT NULL,
  `PROJECT_AGE` varchar(20) DEFAULT NULL,
  `FUNDING_SCHEME` varchar(1000) DEFAULT NULL,
  `SPONSOR_TYPE` varchar(200) DEFAULT NULL,
  `LAST_SYNC_TIME` datetime DEFAULT NULL,
  `ABBREVIATION` varchar(100) DEFAULT NULL,
  `GRANT_FUNDING_AGENCY` varchar(200) DEFAULT NULL,
  `GRANT_FUNDING_AGENCY_TYPE` varchar(100) DEFAULT NULL,
  `SPONSOR_COUNTRY` varchar(100) DEFAULT NULL,
  `PRIME_SPONSOR_COUNTRY` varchar(100) DEFAULT NULL,
  `SPONSOR_TYPE_CODE` varchar(6) DEFAULT NULL,
  `FUND_CODE` varchar(20) DEFAULT NULL,
  `ACTUAL_EXPENSE_WO_IRC` decimal(12,2) DEFAULT NULL,
  `COMMITTED_EXPENSE_WO_IRC` decimal(12,2) DEFAULT NULL,
  `UTILIZATION_RATE_WO_IRC` decimal(12,2) DEFAULT NULL,
  `LEVEL_2_SUP_ORG` longtext,
  `KEY_PERSON_GENDER` varchar(30) DEFAULT NULL,
  `FEED_STATUS` varchar(200) DEFAULT NULL,
  `AWARD_STATUS_CODE` varchar(3) DEFAULT NULL,
  `NTU_PRIORITY` varchar(2000) DEFAULT NULL,
  `FUNDER_APPROVAL_DATE` date DEFAULT NULL,
  `MULTIPLIER` longtext,
  `CLAIM_PREPARER` varchar(255) DEFAULT NULL,
  `AREA_OF_RESEARCH` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`AWARD_MASTER_SET_ID`),
  KEY `AWARD_MASTER_DATASET_RT_idx1` (`AWARD_ID`),
  KEY `AWARD_MASTER_DATASET_RT_idx2` (`AWARD_NUMBER`),
  KEY `AWARD_MASTER_DATASET_RT_idx3` (`ACCOUNT_NUMBER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_method_of_payment` (
  `METHOD_OF_PAYMENT_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`METHOD_OF_PAYMENT_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_milestone` (
  `AWARD_MILESTONE_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(255) DEFAULT NULL,
  `DURATION` varchar(50) DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `MILESTONE` varchar(1000) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `MILESTONE_NUMBER` varchar(30) DEFAULT NULL,
  `MILESTONE_STATUS_CODE` varchar(3) DEFAULT NULL,
  `COMMENT` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`AWARD_MILESTONE_ID`),
  KEY `AWARD_MILESTONE_FK1` (`MILESTONE_STATUS_CODE`),
  CONSTRAINT `AWARD_MILESTONE_FK1` FOREIGN KEY (`MILESTONE_STATUS_CODE`) REFERENCES `milestone_status` (`MILESTONE_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_nextvalue` (
  `AWARD_NUMBER` int NOT NULL,
  PRIMARY KEY (`AWARD_NUMBER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_org_approved_budget_rt` (
  `BUDGET_RT_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` decimal(22,0) NOT NULL,
  `AWARD_NUMBER` varchar(12) NOT NULL,
  `BUDGET_CATEGORY_CODE` varchar(3) NOT NULL,
  `ORIGINAL_APPROVED_BUDGET` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_RT_ID`),
  KEY `AWARD_ORG_APPROVED_BUDGET_RT_IDX1` (`AWARD_NUMBER`,`BUDGET_CATEGORY_CODE`),
  KEY `AWARD_ORG_APPROVED_BUDGET_RT_IDX3` (`AWARD_ID`,`BUDGET_CATEGORY_CODE`,`ORIGINAL_APPROVED_BUDGET`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_original_l2_amt_rt` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `INTERNAL_ORDER_CODE` varchar(20) DEFAULT NULL,
  `AMOUNT` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `AWARD_ORIGINAL_L2_AMT_RT_IDX_1` (`AWARD_NUMBER`,`INTERNAL_ORDER_CODE`,`AMOUNT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_person_attachmnt` (
  `ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `FILE_DATA_ID` varchar(255) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `MIME_TYPE` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `AWARD_PERSON_ID` int DEFAULT NULL,
  PRIMARY KEY (`ATTACHMENT_ID`),
  KEY `AWARD_PERSON_ATTACHMNT_FK1` (`AWARD_PERSON_ID`),
  CONSTRAINT `AWARD_PERSON_ATTACHMNT_FK1` FOREIGN KEY (`AWARD_PERSON_ID`) REFERENCES `award_persons` (`AWARD_PERSON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_person_orcid_work` (
  `AWARD_PERSON_ORCID_WORK_ID` int NOT NULL AUTO_INCREMENT,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `PERSON_ORCID_WORK_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`AWARD_PERSON_ORCID_WORK_ID`),
  KEY `AWARD_PERSON_ORCID_WORK_FK1` (`PERSON_ORCID_WORK_ID`),
  CONSTRAINT `AWARD_PERSON_ORCID_WORK_FK1` FOREIGN KEY (`PERSON_ORCID_WORK_ID`) REFERENCES `person_orcid_work` (`PERSON_ORCID_WORK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_person_roles` (
  `AWARD_PERSON_ROLE_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(255) DEFAULT NULL,
  `PERSON_ID` varchar(255) DEFAULT NULL,
  `ROLE_ID` int DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `IS_SYSTEM_GENERATED` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`AWARD_PERSON_ROLE_ID`),
  KEY `AWARD_PERSON_ROLES_FK1` (`PERSON_ID`),
  KEY `AWARD_PERSON_ROLES_FK2` (`ROLE_ID`),
  KEY `AWARD_PERSON_ROLES_IDX1` (`AWARD_ID`),
  CONSTRAINT `AWARD_PERSON_ROLES_FK1` FOREIGN KEY (`PERSON_ID`) REFERENCES `person` (`PERSON_ID`),
  CONSTRAINT `AWARD_PERSON_ROLES_FK2` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ROLE_ID`),
  CONSTRAINT `AWARD_PERSON_ROLES_FK3` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_person_unit` (
  `AWARD_PERSON_UNIT_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_PERSON_ID` int DEFAULT NULL,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `LEAD_UNIT_FLAG` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`AWARD_PERSON_UNIT_ID`),
  KEY `AWARD_PERSON_UNIT_FK1` (`AWARD_ID`),
  KEY `AWARD_PERSON_UNIT_FK2` (`AWARD_PERSON_ID`),
  KEY `AWARD_PERSON_UNIT_FK3` (`UNIT_NUMBER`),
  CONSTRAINT `AWARD_PERSON_UNIT_FK1` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`),
  CONSTRAINT `AWARD_PERSON_UNIT_FK2` FOREIGN KEY (`AWARD_PERSON_ID`) REFERENCES `award_persons` (`AWARD_PERSON_ID`),
  CONSTRAINT `AWARD_PERSON_UNIT_FK3` FOREIGN KEY (`UNIT_NUMBER`) REFERENCES `unit` (`UNIT_NUMBER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_persons` (
  `AWARD_PERSON_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `FULL_NAME` varchar(90) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `PERCENTAGE_OF_EFFORT` decimal(5,2) DEFAULT NULL,
  `UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `UNIT_NAME` varchar(200) DEFAULT NULL,
  `PERSON_ROLE_ID` int DEFAULT NULL,
  `DEPARTMENT` varchar(200) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `ROLODEX_ID` int DEFAULT NULL,
  `PROJECT_ROLE` varchar(255) DEFAULT NULL,
  `PI_FLAG` varchar(1) DEFAULT NULL,
  `IS_MULTI_PI` varchar(255) DEFAULT NULL,
  `DESIGNATION` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AWARD_PERSON_ID`),
  KEY `AWARD_PERSONS_FK2` (`PERSON_ROLE_ID`),
  KEY `AWARD_PERSONS_FK1` (`AWARD_ID`),
  CONSTRAINT `AWARD_PERSONS_FK1` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`),
  CONSTRAINT `AWARD_PERSONS_FK2` FOREIGN KEY (`PERSON_ROLE_ID`) REFERENCES `eps_prop_person_role` (`PROP_PERSON_ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_progress_report` (
  `PROGRESS_REPORT_ID` int NOT NULL AUTO_INCREMENT,
  `PROGRESS_REPORT_NUMBER` varchar(20) DEFAULT NULL,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `PROGRESS_REPORT_STATUS_CODE` varchar(3) DEFAULT NULL,
  `AWARD_REPORT_TRACKING_ID` decimal(22,0) DEFAULT NULL,
  `REPORT_CLASS_CODE` varchar(3) DEFAULT NULL,
  `DUE_DATE` date DEFAULT NULL,
  `FUNDER_APPROVAL_DATE` datetime DEFAULT NULL,
  `REPORT_START_DATE` date DEFAULT NULL,
  `REPORT_END_DATE` date DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `TITLE` varchar(300) DEFAULT NULL,
  `SUBMISSION_DATE` datetime DEFAULT NULL,
  `SUBMIT_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`PROGRESS_REPORT_ID`),
  KEY `AWARD_PROGRESS_REPORT_FK1` (`PROGRESS_REPORT_STATUS_CODE`),
  KEY `AWARD_PROGRESS_REPORT_FK2` (`AWARD_ID`),
  CONSTRAINT `AWARD_PROGRESS_REPORT_FK1` FOREIGN KEY (`PROGRESS_REPORT_STATUS_CODE`) REFERENCES `progress_report_status` (`PROGRESS_REPORT_STATUS_CODE`),
  CONSTRAINT `AWARD_PROGRESS_REPORT_FK2` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_progress_report_achievement` (
  `PROGRESS_REPORT_ACHIEVEMENT_ID` int NOT NULL AUTO_INCREMENT,
  `PROGRESS_REPORT_ID` int DEFAULT NULL,
  `ACHIEVEMENT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `DESCRIPTION` varchar(3000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`PROGRESS_REPORT_ACHIEVEMENT_ID`),
  KEY `PROGRESS_REPRT_ACHIEVEMENT_FK1` (`ACHIEVEMENT_TYPE_CODE`),
  KEY `PROGRESS_REPRT_ACHIEVEMENT_FK2` (`PROGRESS_REPORT_ID`),
  CONSTRAINT `PROGRESS_REPRT_ACHIEVEMENT_FK1` FOREIGN KEY (`ACHIEVEMENT_TYPE_CODE`) REFERENCES `progress_report_achievement_type` (`ACHIEVEMENT_TYPE_CODE`),
  CONSTRAINT `PROGRESS_REPRT_ACHIEVEMENT_FK2` FOREIGN KEY (`PROGRESS_REPORT_ID`) REFERENCES `award_progress_report` (`PROGRESS_REPORT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_progress_report_attachment` (
  `PROGRESS_REPORT_ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `PROGRESS_REPORT_ID` int DEFAULT NULL,
  `ATTACHMENT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `DOCUMENT_ID` int DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `DOCUMENT_STATUS_CODE` varchar(1) DEFAULT NULL,
  `FILE_DATA_ID` varchar(36) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `MIME_TYPE` varchar(100) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`PROGRESS_REPORT_ATTACHMENT_ID`),
  KEY `PROGRESS_REPORT_ATTACHMENT_FK1` (`ATTACHMENT_TYPE_CODE`),
  CONSTRAINT `PROGRESS_REPORT_ATTACHMENT_FK1` FOREIGN KEY (`ATTACHMENT_TYPE_CODE`) REFERENCES `progress_report_attachmnt_type` (`ATTACHMENT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_progress_report_kpi_summary` (
  `KPI_SUMMARY_ID` int NOT NULL AUTO_INCREMENT,
  `PROGRESS_REPORT_ID` int DEFAULT NULL,
  `ORGINATING_PROGRESS_REPORT_ID` decimal(22,0) DEFAULT NULL,
  `AWARD_ID` decimal(22,0) DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `KPI_CATEGORY_TYPE_CODE` varchar(3) DEFAULT NULL,
  `KPI_CRITERIA_TYPE_CODE` varchar(3) DEFAULT NULL,
  `TARGET` int DEFAULT NULL,
  `ACHIEVED` decimal(12,2) DEFAULT NULL,
  `SECTION_CODE` varchar(10) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`KPI_SUMMARY_ID`),
  KEY `KPI_SUMMARY_FK1` (`KPI_CRITERIA_TYPE_CODE`),
  KEY `KPI_SUMMARY_FK2` (`PROGRESS_REPORT_ID`),
  KEY `KPI_SUMMARY_FK3` (`KPI_CATEGORY_TYPE_CODE`),
  CONSTRAINT `KPI_SUMMARY_FK1` FOREIGN KEY (`KPI_CRITERIA_TYPE_CODE`) REFERENCES `kpi_criteria_type` (`KPI_CRITERIA_TYPE_CODE`),
  CONSTRAINT `KPI_SUMMARY_FK2` FOREIGN KEY (`PROGRESS_REPORT_ID`) REFERENCES `award_progress_report` (`PROGRESS_REPORT_ID`),
  CONSTRAINT `KPI_SUMMARY_FK3` FOREIGN KEY (`KPI_CATEGORY_TYPE_CODE`) REFERENCES `kpi_type` (`KPI_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_progress_report_milestone` (
  `PROGRESS_REPORT_MILESTONE_ID` int NOT NULL AUTO_INCREMENT,
  `PROGRESS_REPORT_ID` int DEFAULT NULL,
  `AWARD_ID` decimal(22,0) DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `MILESTONE_NUMBER` varchar(30) DEFAULT NULL,
  `ACTUAL_START_MONTH` date DEFAULT NULL,
  `ACTUAL_END_MONTH` date DEFAULT NULL,
  `MILESTONE_STATUS_CODE` varchar(3) DEFAULT NULL,
  `REMARK` varchar(2000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`PROGRESS_REPORT_MILESTONE_ID`),
  KEY `PROGRESS_REPRT_MILESTONE_FK2` (`PROGRESS_REPORT_ID`),
  KEY `AWARD_MILESTONE_FK2` (`MILESTONE_STATUS_CODE`),
  CONSTRAINT `AWARD_MILESTONE_FK2` FOREIGN KEY (`MILESTONE_STATUS_CODE`) REFERENCES `milestone_status` (`MILESTONE_STATUS_CODE`),
  CONSTRAINT `PROGRESS_REPRT_MILESTONE_FK2` FOREIGN KEY (`PROGRESS_REPORT_ID`) REFERENCES `award_progress_report` (`PROGRESS_REPORT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_project_team` (
  `AWARD_PROJECT_TEAM_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `FULL_NAME` varchar(255) DEFAULT NULL,
  `PROJECT_ROLE` varchar(100) DEFAULT NULL,
  `NON_EMPLOYEE_FLAG` varchar(1) DEFAULT NULL,
  `PERCENTAGE_CHARGED` decimal(5,2) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `DESIGNATION` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AWARD_PROJECT_TEAM_ID`),
  KEY `AWARD_PROJECT_TEAM_FK1` (`AWARD_ID`),
  CONSTRAINT `AWARD_PROJECT_TEAM_FK1` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_publications` (
  `AWARD_PUBLICATION_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(255) DEFAULT NULL,
  `PUBLICATION_ID` int DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AWARD_PUBLICATION_ID`),
  KEY `AWARD_PUBLICATIONS_FK1` (`PUBLICATION_ID`),
  KEY `AWARD_PUBLICATIONS_FK2` (`AWARD_ID`),
  CONSTRAINT `AWARD_PUBLICATIONS_FK1` FOREIGN KEY (`PUBLICATION_ID`) REFERENCES `publication` (`PUBLICATION_ID`),
  CONSTRAINT `AWARD_PUBLICATIONS_FK2` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_rates` (
  `AWARD_RATE_ID` int NOT NULL AUTO_INCREMENT,
  `ACTIVITY_TYPE_CODE` varchar(3) DEFAULT NULL,
  `APPLICABLE_RATE` decimal(12,2) DEFAULT NULL,
  `FISCAL_YEAR` varchar(4) DEFAULT NULL,
  `INSTITUTE_RATE` decimal(12,2) DEFAULT NULL,
  `ON_OFF_CAMPUS_FLAG` varchar(1) DEFAULT NULL,
  `RATE_CLASS_CODE` varchar(3) DEFAULT NULL,
  `RATE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `BUDGET_HEADER_ID` int DEFAULT NULL,
  PRIMARY KEY (`AWARD_RATE_ID`),
  KEY `AWARD_RATE_FK2` (`BUDGET_HEADER_ID`),
  KEY `FKdqwa3ijrma4d2jtp8xrh51dkv` (`ACTIVITY_TYPE_CODE`),
  KEY `FKmil5tf2laj1hwmtvv8rki8j5n` (`RATE_CLASS_CODE`,`RATE_TYPE_CODE`),
  CONSTRAINT `AWARD_RATE_FK1` FOREIGN KEY (`ACTIVITY_TYPE_CODE`) REFERENCES `activity_type` (`ACTIVITY_TYPE_CODE`),
  CONSTRAINT `AWARD_RATE_FK2` FOREIGN KEY (`BUDGET_HEADER_ID`) REFERENCES `award_budget_header` (`BUDGET_HEADER_ID`),
  CONSTRAINT `AWARD_RATE_FK3` FOREIGN KEY (`RATE_CLASS_CODE`) REFERENCES `rate_class` (`RATE_CLASS_CODE`),
  CONSTRAINT `AWARD_RATE_FK4` FOREIGN KEY (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`) REFERENCES `rate_type` (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_report_recpnt_notify_log` (
  `AWARD_REP_RECPNT_NOTIFY_LOG_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_REPORT_TERM_RECIPIENT_ID` int DEFAULT NULL,
  `AWARD_REPORT_TERMS_ID` int DEFAULT NULL,
  `NOTIFY_DATE` datetime DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`AWARD_REP_RECPNT_NOTIFY_LOG_ID`),
  KEY `AWARD_REP_RECPNT_NOTFY_LOG_FK1` (`AWARD_REPORT_TERM_RECIPIENT_ID`),
  CONSTRAINT `AWARD_REP_RECPNT_NOTFY_LOG_FK1` FOREIGN KEY (`AWARD_REPORT_TERM_RECIPIENT_ID`) REFERENCES `award_report_term_recipient` (`AWARD_REPORT_TERM_RECIPIENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_report_reminder` (
  `REMINDER_ID` int NOT NULL AUTO_INCREMENT,
  `CREATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `CREATE_USER` varchar(255) DEFAULT NULL,
  `DAYS_TO_DUE_DATE` int DEFAULT NULL,
  `FREQUENCY_CODE` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` varchar(255) DEFAULT NULL,
  `NOTIFICATION_TYPE_ID` int DEFAULT NULL,
  `REPORT_CLASS_CODE` varchar(255) DEFAULT NULL,
  `REPORT_CODE` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`REMINDER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_report_term_recipient` (
  `AWARD_REPORT_TERM_RECIPIENT_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_REPORT_TERMS_ID` int DEFAULT NULL,
  `AWARD_ID` decimal(22,0) DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `RECIPIENT_ID` varchar(40) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`AWARD_REPORT_TERM_RECIPIENT_ID`),
  KEY `AWARD_REPORT_TRM_RECIPIENT_FK1` (`AWARD_REPORT_TERMS_ID`),
  CONSTRAINT `AWARD_REPORT_TRM_RECIPIENT_FK1` FOREIGN KEY (`AWARD_REPORT_TERMS_ID`) REFERENCES `award_report_terms` (`AWARD_REPORT_TERMS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_report_terms` (
  `AWARD_REPORT_TERMS_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `REPORT_CLASS_CODE` varchar(3) DEFAULT NULL,
  `REPORT_CODE` varchar(3) DEFAULT NULL,
  `FREQUENCY_CODE` varchar(3) DEFAULT NULL,
  `FREQUENCY_BASE_CODE` varchar(3) DEFAULT NULL,
  `OSP_DISTRIBUTION_CODE` varchar(3) DEFAULT NULL,
  `DUE_DATE` datetime DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `BASE_DATE` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`AWARD_REPORT_TERMS_ID`),
  KEY `AWARD_REPORT_TERMS_FK1` (`AWARD_ID`),
  CONSTRAINT `AWARD_REPORT_TERMS_FK1` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_report_tracking` (
  `AWARD_REPORT_TRACKING_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_REPORT_TERMS_ID` int DEFAULT NULL,
  `AWARD_ID` decimal(22,0) DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `STATUS_CODE` varchar(3) DEFAULT NULL,
  `ACTIVITY_DATE` datetime DEFAULT NULL,
  `COMMENTS` varchar(2000) DEFAULT NULL,
  `PREPARER_ID` varchar(40) DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `DUE_DATE` datetime(6) DEFAULT NULL,
  `PREPARER_NAME` varchar(255) DEFAULT NULL,
  `PROGRESS_REPORT_ID` int DEFAULT NULL,
  PRIMARY KEY (`AWARD_REPORT_TRACKING_ID`),
  KEY `AWARD_REPORT_TRACKING_FK1` (`AWARD_REPORT_TERMS_ID`),
  CONSTRAINT `AWARD_REPORT_TRACKING_FK1` FOREIGN KEY (`AWARD_REPORT_TERMS_ID`) REFERENCES `award_report_terms` (`AWARD_REPORT_TERMS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_report_tracking_file` (
  `AWARD_REPORT_TRACKING_FILE_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_REPORT_TERMS_ID` int DEFAULT NULL,
  `AWARD_REPORT_TRACKING_ID` int DEFAULT NULL,
  `AWARD_ID` decimal(22,0) DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `DOCUMENT_STATUS_CODE` varchar(1) DEFAULT NULL,
  `FILE_ID` varchar(36) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `CONTENT_TYPE` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`AWARD_REPORT_TRACKING_FILE_ID`),
  KEY `AWARD_REPORT_TRACKING_FILE_FK1` (`AWARD_REPORT_TRACKING_ID`),
  CONSTRAINT `AWARD_REPORT_TRACKING_FILE_FK1` FOREIGN KEY (`AWARD_REPORT_TRACKING_ID`) REFERENCES `award_report_tracking` (`AWARD_REPORT_TRACKING_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_research_areas` (
  `RESRCH_AREA_ID` int NOT NULL AUTO_INCREMENT,
  `RESRCH_TYPE_CODE` varchar(3) DEFAULT NULL,
  `RESEARCH_AREA_CODE` varchar(8) DEFAULT NULL,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `RESRCH_TYPE_AREA_CODE` varchar(3) DEFAULT NULL,
  `RESRCH_TYPE_SUB_AREA_CODE` varchar(255) DEFAULT NULL,
  `RESEARCH_SUB_AREA_CODE` varchar(255) DEFAULT NULL,
  `CHALLENGE_AREA_CODE` varchar(255) DEFAULT NULL,
  `CHALLENGE_SUBAREA_CODE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`RESRCH_AREA_ID`),
  KEY `AWARD_RESEARCH_AREAS_FK1` (`AWARD_ID`),
  KEY `AWARD_RESEARCH_AREAS_FK2` (`RESRCH_TYPE_CODE`),
  KEY `AWARD_RESEARCH_AREAS_FK3` (`RESRCH_TYPE_AREA_CODE`),
  KEY `AWARD_RESEARCH_AREAS_FK4` (`RESRCH_TYPE_SUB_AREA_CODE`),
  CONSTRAINT `AWARD_RESEARCH_AREAS_FK1` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`),
  CONSTRAINT `AWARD_RESEARCH_AREAS_FK2` FOREIGN KEY (`RESRCH_TYPE_CODE`) REFERENCES `research_type` (`RESRCH_TYPE_CODE`),
  CONSTRAINT `AWARD_RESEARCH_AREAS_FK3` FOREIGN KEY (`RESRCH_TYPE_AREA_CODE`) REFERENCES `research_type_area` (`RESRCH_TYPE_AREA_CODE`),
  CONSTRAINT `AWARD_RESEARCH_AREAS_FK4` FOREIGN KEY (`RESRCH_TYPE_SUB_AREA_CODE`) REFERENCES `research_type_sub_area` (`RESRCH_TYPE_SUB_AREA_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_research_type` (
  `RESRCH_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`RESRCH_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_revenue_details` (
  `AWARD_NUMBER` varchar(12) NOT NULL,
  `ACCOUNT_NUMBER` varchar(100) NOT NULL,
  `INTERNAL_ORDER_CODE` varchar(50) NOT NULL,
  `TOTAL_REVENUE_AMOUNT` decimal(12,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`AWARD_NUMBER`,`ACCOUNT_NUMBER`,`INTERNAL_ORDER_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_revenue_files` (
  `FILE_ID` int NOT NULL AUTO_INCREMENT,
  `FILE_NAME` varchar(255) DEFAULT NULL,
  `NO_OF_RECORDS` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `SYSTEM_ARCHIVED` varchar(1) DEFAULT NULL,
  `REMOTE_ARCHIVED` varchar(1) DEFAULT NULL,
  `INSERTED_ROWS` int DEFAULT NULL,
  `INSERTED_IN_RT` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`FILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_revenue_transactions` (
  `REVENUE_TRACKER_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `ACCOUNT_NUMBER` varchar(100) DEFAULT NULL,
  `INTERNAL_ORDER_CODE` varchar(50) DEFAULT NULL,
  `REMARKS` varchar(1000) DEFAULT NULL,
  `AMOUNT_IN_FMA_CURRENCY` decimal(12,2) DEFAULT NULL,
  `ACTUAL_OR_COMMITTED_FLAG` varchar(1) DEFAULT NULL,
  `FM_POSTING_DATE` datetime DEFAULT NULL,
  `FI_POSTING_DATE` datetime DEFAULT NULL,
  `BP_CODE` varchar(50) DEFAULT NULL,
  `BP_NAME` varchar(300) DEFAULT NULL,
  `DOCUMENT_DATE` datetime DEFAULT NULL,
  `FI_GL_ACCOUNT` varchar(50) DEFAULT NULL,
  `FI_GL_DESCRIPTION` varchar(200) DEFAULT NULL,
  `TRANSACTION_REFERENCE_NUMBER` varchar(100) DEFAULT NULL,
  `REFERENCE_DOCUMENT_NUMBER` varchar(10) DEFAULT NULL,
  `REFERENCE_POSTING_LINE` varchar(6) DEFAULT NULL,
  `GUID` varchar(40) DEFAULT NULL,
  `GMIA_DOCNR` varchar(10) DEFAULT NULL,
  `DOCLN` varchar(6) DEFAULT NULL,
  `RBUKRS` varchar(4) DEFAULT NULL,
  `RVALUETYPE_9` varchar(2) DEFAULT NULL,
  `RYEAR` varchar(4) DEFAULT NULL,
  `GL_SIRID` varchar(40) DEFAULT NULL,
  `BATCH_ID` varchar(100) DEFAULT NULL,
  `GRANT_NBR` varchar(20) DEFAULT NULL,
  `SPONSOR_PROGRAM` varchar(100) DEFAULT NULL,
  `SPONSOR_CLASS` varchar(50) DEFAULT NULL,
  `FUND` varchar(10) DEFAULT NULL,
  `FUND_CENTER` varchar(16) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `FILE_ID` int DEFAULT NULL,
  `ENTRY_DATE` datetime DEFAULT NULL,
  `BUDGET_CATEGORY_CODE` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`REVENUE_TRACKER_ID`),
  KEY `AWARD_REVENUE_TRANSACTIONS_IDX6` (`FM_POSTING_DATE`),
  KEY `AWARD_REVENUE_TRANSACTIONS_IDX1` (`AWARD_NUMBER`),
  KEY `AWARD_REVENUE_TRANSACTIONS_IDX2` (`ACCOUNT_NUMBER`),
  KEY `AWARD_REVENUE_TRANSACTIONS_IDX3` (`INTERNAL_ORDER_CODE`),
  KEY `AWARD_REVENUE_TRANSACTIONS_IDX4` (`AWARD_NUMBER`,`INTERNAL_ORDER_CODE`),
  KEY `AWARD_REVENUE_TRANSACTIONS_IDX5` (`ACTUAL_OR_COMMITTED_FLAG`),
  KEY `AWARD_REVENUE_TRANSACTIONS_IDX_1` (`AWARD_NUMBER`,`INTERNAL_ORDER_CODE`,`BUDGET_CATEGORY_CODE`,`ACTUAL_OR_COMMITTED_FLAG`,`AMOUNT_IN_FMA_CURRENCY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_revenue_transactions_rt` (
  `REVENUE_TRACKER_ID` int NOT NULL AUTO_INCREMENT,
  `ACCOUNT_NUMBER` varchar(255) DEFAULT NULL,
  `ACCOUNTING_DOCUMENT_NUMBER` varchar(255) DEFAULT NULL,
  `ACTUAL_OR_COMMITTED_FLAG` varchar(255) DEFAULT NULL,
  `AMOUNT_IN_FMA_CURRENCY` decimal(12,2) DEFAULT NULL,
  `BATCH_ID` varchar(255) DEFAULT NULL,
  `COMPANY_CODE` varchar(255) DEFAULT NULL,
  `DOCUMENT_DATE` varchar(255) DEFAULT NULL,
  `FI_GL_ACCOUNT` varchar(255) DEFAULT NULL,
  `FI_GL_DESCRIPTION` varchar(255) DEFAULT NULL,
  `FI_POSTING_DATE` varchar(255) DEFAULT NULL,
  `FILE_ID` int DEFAULT NULL,
  `FISCAL_YEAR` varchar(255) DEFAULT NULL,
  `FM_POSTING_DATE` varchar(255) DEFAULT NULL,
  `FUND` varchar(255) DEFAULT NULL,
  `FUND_CENTER` varchar(255) DEFAULT NULL,
  `GM_VALUE_TYPE` varchar(255) DEFAULT NULL,
  `GRANT_NBR` varchar(255) DEFAULT NULL,
  `INTERNAL_ORDER_CODE` varchar(255) DEFAULT NULL,
  `LEDGER_POSTING_ITEM` varchar(255) DEFAULT NULL,
  `LINE_ITEM_RECORD` varchar(255) DEFAULT NULL,
  `REFERENCE_DOCUMENT_NUMBER` varchar(255) DEFAULT NULL,
  `REFERENCE_POSTING_LINE` varchar(255) DEFAULT NULL,
  `REMARKS` varchar(255) DEFAULT NULL,
  `SAP_GUID` varchar(255) DEFAULT NULL,
  `SPONSOR_CLASS` varchar(255) DEFAULT NULL,
  `SPONSOR_PROGRAM` varchar(255) DEFAULT NULL,
  `TRANSACTION_REFERENCE_NUMBER` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `VENDOR_CODE` varchar(255) DEFAULT NULL,
  `VENDOR_NAME` varchar(255) DEFAULT NULL,
  `BP_CODE` varchar(255) DEFAULT NULL,
  `BP_NAME` varchar(255) DEFAULT NULL,
  `DOCLN` varchar(255) DEFAULT NULL,
  `ENTRY_DATE` varchar(255) DEFAULT NULL,
  `GL_SIRID` varchar(255) DEFAULT NULL,
  `GMIA_DOCNR` varchar(255) DEFAULT NULL,
  `GUID` varchar(255) DEFAULT NULL,
  `RYEAR` varchar(255) DEFAULT NULL,
  `RBUKRS` varchar(255) DEFAULT NULL,
  `RVALUETYPE_9` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`REVENUE_TRACKER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_review_comment` (
  `AWARD_REVIEW_COMMENT_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(255) DEFAULT NULL,
  `IS_RESOLVED` varchar(255) DEFAULT NULL,
  `PARENT_REVIEW_COMMENT_ID` int DEFAULT NULL,
  `RESOLVED_BY` varchar(255) DEFAULT NULL,
  `RESOLVED_TIMESTAMP` datetime(6) DEFAULT NULL,
  `REVIEW_COMMENT` varchar(4000) DEFAULT NULL,
  `REVIEW_COMMENT_TYPE_CODE` varchar(255) DEFAULT NULL,
  `REVIEW_SECTION_CODE` varchar(255) DEFAULT NULL,
  `REVIEWER_PERSON_ID` varchar(255) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `IS_PRIVATE_COMMENT` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AWARD_REVIEW_COMMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_science_keyword` (
  `AWARD_SCIENCE_KEYWORD_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `SCIENCE_KEYWORD_CODE` varchar(15) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `KEYWORD` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AWARD_SCIENCE_KEYWORD_ID`),
  KEY `AWARD_SCIENCE_KEYWORD_FK2` (`SCIENCE_KEYWORD_CODE`),
  KEY `AWARD_SCIENCE_KEYWORD_FK1` (`AWARD_ID`),
  CONSTRAINT `AWARD_SCIENCE_KEYWORD_FK1` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`),
  CONSTRAINT `AWARD_SCIENCE_KEYWORD_FK2` FOREIGN KEY (`SCIENCE_KEYWORD_CODE`) REFERENCES `science_keyword` (`SCIENCE_KEYWORD_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_scopus` (
  `AWARD_SCOPUS_ID` int NOT NULL AUTO_INCREMENT,
  `SCOPUS_ID` varchar(40) DEFAULT NULL,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`AWARD_SCOPUS_ID`),
  KEY `AWARD_SCOPUS_FK1` (`SCOPUS_ID`),
  KEY `AWARD_SCOPUS_FK2` (`AWARD_ID`),
  CONSTRAINT `AWARD_SCOPUS_FK1` FOREIGN KEY (`SCOPUS_ID`) REFERENCES `scopus` (`SCOPUS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_section_type` (
  `AWARD_SECTION_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`AWARD_SECTION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_special_review` (
  `AWARD_SPECIAL_REVIEW_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` decimal(22,0) NOT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `SPECIAL_REVIEW_CODE` varchar(3) NOT NULL,
  `APPROVAL_TYPE_CODE` varchar(3) DEFAULT NULL,
  `EXPIRATION_DATE` datetime DEFAULT NULL,
  `PROTOCOL_NUMBER` varchar(20) DEFAULT NULL,
  `APPLICATION_DATE` datetime DEFAULT NULL,
  `APPROVAL_DATE` datetime DEFAULT NULL,
  `COMMENTS` longtext,
  `UPDATE_USER` varchar(60) NOT NULL,
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `SPECIAL_REVIEW_NUMBER` int DEFAULT NULL,
  `IS_INTEGRATED_PROTOCOL` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`AWARD_SPECIAL_REVIEW_ID`),
  KEY `AWARD_SPECIAL_REVIEW_FK2` (`SPECIAL_REVIEW_CODE`),
  KEY `AWARD_SPECIAL_REVIEW_FK3` (`APPROVAL_TYPE_CODE`),
  CONSTRAINT `AWARD_SPECIAL_REVIEW_FK2` FOREIGN KEY (`SPECIAL_REVIEW_CODE`) REFERENCES `special_review` (`SPECIAL_REVIEW_CODE`),
  CONSTRAINT `AWARD_SPECIAL_REVIEW_FK3` FOREIGN KEY (`APPROVAL_TYPE_CODE`) REFERENCES `sp_rev_approval_type` (`APPROVAL_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_sponsor_term` (
  `AWARD_SPONSOR_TERM_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `SPONSOR_TERM_TYPE_CODE` varchar(3) DEFAULT NULL,
  `SPONSOR_TERM_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`AWARD_SPONSOR_TERM_ID`),
  KEY `AWARD_SPONSOR_TERM_FK1` (`AWARD_ID`),
  CONSTRAINT `AWARD_SPONSOR_TERM_FK1` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_status` (
  `STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `ACTIVE_FLAG` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_sup_org_mapping` (
  `AWARD_SUP_ORG_MAPPING_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(255) DEFAULT NULL,
  `IS_LEVEL_TWO_SUP_ORG_UNIT` varchar(255) DEFAULT NULL,
  `PI_PERSON_ID` varchar(255) DEFAULT NULL,
  `SUP_ORG_ID` varchar(255) DEFAULT NULL,
  `SUPERIOR_SUP_ORG_ID` varchar(255) DEFAULT NULL,
  `UNIT_NUMBER` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AWARD_SUP_ORG_MAPPING_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_task_type_mapping` (
  `AWARD_TASK_TYPE_MAPPING_ID` int NOT NULL,
  `AWARD_DOCUMENT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `TYPE_CODE` varchar(3) DEFAULT NULL,
  `TASK_TYPE_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`AWARD_TASK_TYPE_MAPPING_ID`),
  KEY `AWARD_TASK_TYPE_MAPPING_FK1` (`AWARD_DOCUMENT_TYPE_CODE`),
  KEY `AWARD_TASK_TYPE_MAPPING_FK3` (`TASK_TYPE_CODE`),
  CONSTRAINT `AWARD_TASK_TYPE_MAPPING_FK1` FOREIGN KEY (`AWARD_DOCUMENT_TYPE_CODE`) REFERENCES `award_document_type` (`AWARD_DOCUMENT_TYPE_CODE`),
  CONSTRAINT `AWARD_TASK_TYPE_MAPPING_FK3` FOREIGN KEY (`TASK_TYPE_CODE`) REFERENCES `task_type` (`TASK_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_transaction_status` (
  `TRANSACTION_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`TRANSACTION_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_transaction_type` (
  `AWARD_TRANSACTION_TYPE_CODE` decimal(3,0) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` date DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`AWARD_TRANSACTION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_type` (
  `AWARD_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`AWARD_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_variation_section_maping` (
  `AWARD_SECTION_TYPE_CODE` varchar(3) NOT NULL,
  `AWARD_VARIATION_TYPE_CODE` varchar(3) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`AWARD_SECTION_TYPE_CODE`),
  KEY `AWARD_VARIATN_SECTN_MAP_FK1` (`AWARD_VARIATION_TYPE_CODE`),
  CONSTRAINT `AWARD_VARIATN_SECTN_MAP_FK1` FOREIGN KEY (`AWARD_VARIATION_TYPE_CODE`) REFERENCES `sr_type` (`TYPE_CODE`),
  CONSTRAINT `AWARD_VARIATN_SECTN_MAP_FK2` FOREIGN KEY (`AWARD_SECTION_TYPE_CODE`) REFERENCES `award_section_type` (`AWARD_SECTION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `award_workflow_status` (
  `WORKFLOW_AWARD_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`WORKFLOW_AWARD_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `awd_repreq_duedt_log` (
  `AWD_REPREQ_DUEDT_LOG_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_REPORT_TERMS_ID` int DEFAULT NULL,
  `ERROR_MESSAGE` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_USER` varchar(40) DEFAULT 'quickstart',
  PRIMARY KEY (`AWD_REPREQ_DUEDT_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `birt_download_option` (
  `ID` int NOT NULL,
  `TYPE_CODE` varchar(3) DEFAULT NULL,
  `DOWNLOAD_TYPE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `BIRT_DOWNLOAD_OPTION_FK1` (`TYPE_CODE`),
  CONSTRAINT `BIRT_DOWNLOAD_OPTION_FK1` FOREIGN KEY (`TYPE_CODE`) REFERENCES `report_type` (`TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `birt_template` (
  `ID` int NOT NULL,
  `TYPE_CODE` varchar(5) DEFAULT NULL,
  `FILE` longblob,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `CONTENT_TYPE` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `BIRT_TEMPLATE_FK1` (`TYPE_CODE`),
  CONSTRAINT `BIRT_TEMPLATE_FK1` FOREIGN KEY (`TYPE_CODE`) REFERENCES `report_type` (`TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `budget_api_response` (
  `ACTUAL_AMOUNT` decimal(19,2) DEFAULT NULL,
  `BUDGET` decimal(19,2) DEFAULT NULL,
  `COMMITMENT_AMOUNT` decimal(19,2) DEFAULT NULL,
  `FINANCE_PROJECT_ACCOUNT_ID` int NOT NULL,
  `EXPENDITURE_TYPE` varchar(255) NOT NULL,
  `FUND_AVAILABLE` decimal(19,2) DEFAULT NULL,
  `LEDGER` varchar(255) DEFAULT NULL,
  `LONG_NAME` varchar(255) DEFAULT NULL,
  `ORGANIZATION_ID` varchar(255) DEFAULT NULL,
  `OPERATING_UNIT` varchar(255) DEFAULT NULL,
  `PROJECT_ID` int DEFAULT NULL,
  `PROJECT_NAME` varchar(255) DEFAULT NULL,
  `PROJECT_NUMBER` varchar(255) NOT NULL,
  `SHORT_NAME` varchar(255) DEFAULT NULL,
  `TASK_NAME` varchar(255) DEFAULT NULL,
  `TASK_NUMBER` varchar(255) NOT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `YEAR` int NOT NULL,
  PRIMARY KEY (`EXPENDITURE_TYPE`,`PROJECT_NUMBER`,`TASK_NUMBER`,`YEAR`,`FINANCE_PROJECT_ACCOUNT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `budget_category` (
  `BUDGET_CATEGORY_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `BUDGET_CATEGORY_TYPE_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `SORT_ORDER` int DEFAULT NULL,
  `BUDGET_CATEGORY_ACRONYM` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_CATEGORY_CODE`),
  KEY `BUDGET_CATEGORY_FK1` (`BUDGET_CATEGORY_TYPE_CODE`),
  CONSTRAINT `BUDGET_CATEGORY_FK1` FOREIGN KEY (`BUDGET_CATEGORY_TYPE_CODE`) REFERENCES `budget_category_type` (`BUDGET_CATEGORY_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `budget_category_type` (
  `BUDGET_CATEGORY_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `SORT_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_CATEGORY_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `budget_det_cal_amt` (
  `BUDGET_DETAILS_CAL_AMTS_ID` int NOT NULL AUTO_INCREMENT,
  `APPLY_RATE_FLAG` varchar(1) DEFAULT NULL,
  `BUDGET_ID` int DEFAULT NULL,
  `BUDGET_PERIOD` int DEFAULT NULL,
  `BUDGET_PERIOD_NUMBER` int DEFAULT NULL,
  `CALCULATED_COST` decimal(12,2) DEFAULT NULL,
  `CALCULATED_COST_SHARING` decimal(12,2) DEFAULT NULL,
  `LINE_ITEM_NUMBER` int DEFAULT NULL,
  `RATE_CLASS_CODE` varchar(3) DEFAULT NULL,
  `RATE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `RATE_TYPE_DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `BUDGET_DETAILS_ID` int DEFAULT NULL,
  `APPLICABLE_RATE` decimal(12,2) DEFAULT NULL,
  `CALCULATED_FUND_REQUESTED` decimal(12,2) DEFAULT NULL,
  `APPLICABLE_COST_SHARING_RATE` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_DETAILS_CAL_AMTS_ID`),
  KEY `BUDGET_DET_CAL_AMT_FK1` (`BUDGET_DETAILS_ID`),
  KEY `FK1yqc7yi3gvdici899nfot13fb` (`RATE_CLASS_CODE`,`RATE_TYPE_CODE`),
  CONSTRAINT `BUDGET_DET_CAL_AMT_FK1` FOREIGN KEY (`BUDGET_DETAILS_ID`) REFERENCES `budget_detail` (`BUDGET_DETAILS_ID`),
  CONSTRAINT `BUDGET_DET_CAL_AMT_FK2` FOREIGN KEY (`RATE_CLASS_CODE`) REFERENCES `rate_class` (`RATE_CLASS_CODE`),
  CONSTRAINT `BUDGET_DET_CAL_AMT_FK3` FOREIGN KEY (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`) REFERENCES `rate_type` (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `budget_detail` (
  `BUDGET_DETAILS_ID` int NOT NULL AUTO_INCREMENT,
  `BUDGET_HEADER_ID` int DEFAULT NULL,
  `BUDGET_PERIOD_ID` int DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `BUDGET_PERIOD` int DEFAULT NULL,
  `LINE_ITEM_NUMBER` int DEFAULT NULL,
  `START_DATE` date DEFAULT NULL,
  `END_DATE` date DEFAULT NULL,
  `BUDGET_CATEGORY_CODE` varchar(3) DEFAULT NULL,
  `COST_ELEMENT` varchar(12) DEFAULT NULL,
  `LINE_ITEM_DESCRIPTION` varchar(4000) DEFAULT NULL,
  `LINE_ITEM_COST` decimal(12,2) DEFAULT NULL,
  `PREVIOUS_LINE_ITEM_COST` decimal(12,2) DEFAULT NULL,
  `BUDGET_JUSTIFICATION` varchar(2000) DEFAULT NULL,
  `IS_SYSTEM_GENRTED_COST_ELEMENT` varchar(1) DEFAULT NULL,
  `ON_OFF_CAMPUS_FLAG` varchar(1) DEFAULT NULL,
  `SYSTEM_GEN_COST_ELEMENT_TYPE` varchar(60) DEFAULT NULL,
  `FULL_NAME` varchar(90) DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `PERSON_TYPE` varchar(3) DEFAULT NULL,
  `ROLODEX_ID` int DEFAULT NULL,
  `TBN_ID` varchar(9) DEFAULT NULL,
  `IS_APPLY_INFLATION_RATE` varchar(1) DEFAULT NULL,
  `COST_SHARING_AMOUNT` decimal(12,2) DEFAULT NULL,
  `COST_SHARING_PERCENT` decimal(5,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` timestamp(6) NULL DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `APPLY_IN_RATE_FLAG` varchar(255) DEFAULT NULL,
  `QUANTITY` decimal(6,2) DEFAULT NULL,
  `SUBMIT_COST_SHARING_FLAG` varchar(255) DEFAULT NULL,
  `UNDERRECOVERY_AMOUNT` decimal(12,2) DEFAULT NULL,
  `COST_ELEMENT_BASE` decimal(12,2) DEFAULT NULL,
  `SPONSOR_REQUESTED_AMOUNT` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_DETAILS_ID`),
  KEY `BUDGET_DETAIL_FK1` (`BUDGET_PERIOD_ID`),
  KEY `BUDGET_DETAIL_FK2` (`BUDGET_CATEGORY_CODE`),
  KEY `BUDGET_DETAIL_FK3` (`COST_ELEMENT`),
  KEY `BUDGET_DETAIL_FK5` (`TBN_ID`),
  CONSTRAINT `BUDGET_DETAIL_FK1` FOREIGN KEY (`BUDGET_PERIOD_ID`) REFERENCES `budget_period` (`BUDGET_PERIOD_ID`),
  CONSTRAINT `BUDGET_DETAIL_FK2` FOREIGN KEY (`BUDGET_CATEGORY_CODE`) REFERENCES `budget_category` (`BUDGET_CATEGORY_CODE`),
  CONSTRAINT `BUDGET_DETAIL_FK3` FOREIGN KEY (`COST_ELEMENT`) REFERENCES `cost_element` (`COST_ELEMENT`),
  CONSTRAINT `BUDGET_DETAIL_FK5` FOREIGN KEY (`TBN_ID`) REFERENCES `tbn` (`TBN_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `budget_header` (
  `BUDGET_HEADER_ID` int NOT NULL AUTO_INCREMENT,
  `ANTICIPATED_TOTAL` decimal(12,2) DEFAULT NULL,
  `BUDGET_STATUS_CODE` varchar(3) DEFAULT NULL,
  `BUDGET_TYPE_CODE` varchar(3) DEFAULT NULL,
  `COMMENTS` varchar(4000) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `CREATE_USER_NAME` varchar(90) DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `IS_AUTO_CALC` varchar(1) DEFAULT NULL,
  `MODULE_ITEM_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(30) DEFAULT NULL,
  `MODULE_SEQUENCE_NUMBER` int DEFAULT NULL,
  `OBLIGATED_CHANGE` decimal(12,2) DEFAULT NULL,
  `OBLIGATED_TOTAL` decimal(12,2) DEFAULT NULL,
  `ON_OFF_CAMPUS_FLAG` varchar(1) DEFAULT NULL,
  `RATE_CLASS_CODE` varchar(3) DEFAULT NULL,
  `RATE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `TOTAL_COST` decimal(12,2) DEFAULT NULL,
  `TOTAL_DIRECT_COST` decimal(12,2) DEFAULT NULL,
  `TOTAL_INDIRECT_COST` decimal(12,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_USER_NAME` varchar(90) DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `TOTAL_SUBCONTRACT_COST` decimal(12,2) DEFAULT NULL,
  `IS_FINAL_BUDGET` varchar(1) DEFAULT 'N',
  `PROPOSAL_ID` int NOT NULL,
  `COST_SHARING_AMOUNT` decimal(12,2) DEFAULT NULL,
  `FINAL_VERSION_FLAG` varchar(255) DEFAULT NULL,
  `MODULAR_BUDGET_FLAG` varchar(255) DEFAULT NULL,
  `RESIDUAL_FUNDS` decimal(12,2) DEFAULT NULL,
  `SUBMIT_COST_SHARING_FLAG` varchar(255) DEFAULT NULL,
  `TOTAL_COST_LIMIT` decimal(12,2) DEFAULT NULL,
  `UNDERRECOVERY_AMOUNT` decimal(12,2) DEFAULT NULL,
  `UNDERRECOVERY_CLASS_CODE` varchar(255) DEFAULT NULL,
  `UNDERRECOVERY_TYPE_CODE` varchar(255) DEFAULT NULL,
  `IS_LATEST_VERSION` varchar(1) DEFAULT NULL,
  `IS_APPROVED_BUDGET` varchar(255) DEFAULT NULL,
  `BUDGET_TEMPLATE_TYPE_ID` int DEFAULT NULL,
  `ON_CAMPUS_RATES` varchar(100) DEFAULT NULL,
  `OFF_CAMPUS_RATES` varchar(100) DEFAULT NULL,
  `COST_SHARE_TYPE_CODE` int DEFAULT NULL,
  PRIMARY KEY (`BUDGET_HEADER_ID`),
  KEY `BUDGET_HEADER_FK1` (`BUDGET_STATUS_CODE`),
  KEY `BUDGET_HEADER_FK2` (`BUDGET_TYPE_CODE`),
  KEY `FKdcpmsga2j8d4vtbpcw0fyhv6o` (`RATE_CLASS_CODE`,`RATE_TYPE_CODE`),
  KEY `BUDGET_HEADER_FK4` (`PROPOSAL_ID`),
  KEY `FK4aqxi3edjo1453rb48uwcytah` (`UNDERRECOVERY_CLASS_CODE`,`UNDERRECOVERY_TYPE_CODE`),
  KEY `BUDGET_HEADER_FK6` (`COST_SHARE_TYPE_CODE`),
  CONSTRAINT `BUDGET_HEADER_FK1` FOREIGN KEY (`BUDGET_STATUS_CODE`) REFERENCES `budget_status` (`BUDGET_STATUS_CODE`),
  CONSTRAINT `BUDGET_HEADER_FK2` FOREIGN KEY (`BUDGET_TYPE_CODE`) REFERENCES `budget_type` (`BUDGET_TYPE_CODE`),
  CONSTRAINT `BUDGET_HEADER_FK3` FOREIGN KEY (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`) REFERENCES `rate_type` (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`),
  CONSTRAINT `BUDGET_HEADER_FK4` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `eps_proposal` (`PROPOSAL_ID`),
  CONSTRAINT `BUDGET_HEADER_FK5` FOREIGN KEY (`UNDERRECOVERY_CLASS_CODE`, `UNDERRECOVERY_TYPE_CODE`) REFERENCES `rate_type` (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`),
  CONSTRAINT `BUDGET_HEADER_FK6` FOREIGN KEY (`COST_SHARE_TYPE_CODE`) REFERENCES `cost_sharing_type` (`COST_SHARE_TYPE_CODE`),
  CONSTRAINT `FK4aqxi3edjo1453rb48uwcytah` FOREIGN KEY (`UNDERRECOVERY_CLASS_CODE`, `UNDERRECOVERY_TYPE_CODE`) REFERENCES `rate_type` (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `budget_modular` (
  `BUDGET_PERIOD_ID` int NOT NULL,
  `BUDGET_HEADER_ID` int DEFAULT NULL,
  `CONSORTIUM_FNA` decimal(12,2) DEFAULT NULL,
  `DIRECT_COST_LESS_CONSOR_FNA` decimal(12,2) DEFAULT NULL,
  `TOTAL_DIRECT_COST` decimal(12,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_PERIOD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `budget_modular_idc` (
  `BUDGET_MODULAR_IDC_ID` int NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` int DEFAULT NULL,
  `FUNDS_REQUESTED` decimal(12,2) DEFAULT NULL,
  `IDC_BASE` decimal(12,2) DEFAULT NULL,
  `IDC_RATE` decimal(5,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `BUDGET_PERIOD_ID` int NOT NULL,
  PRIMARY KEY (`BUDGET_MODULAR_IDC_ID`),
  KEY `BUDGET_MODULAR_IDC_FK1` (`BUDGET_PERIOD_ID`),
  CONSTRAINT `BUDGET_MODULAR_IDC_FK1` FOREIGN KEY (`BUDGET_PERIOD_ID`) REFERENCES `budget_modular` (`BUDGET_PERIOD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `budget_per_det_rate_and_base` (
  `BUD_PER_RATE_BASE_ID` int NOT NULL AUTO_INCREMENT,
  `APPLIED_RATE` decimal(5,2) DEFAULT NULL,
  `BASE_COST` decimal(12,2) DEFAULT NULL,
  `BASE_COST_SHARING` decimal(12,2) DEFAULT NULL,
  `BUDGET_PERIOD` int DEFAULT NULL,
  `CALCULATED_COST` decimal(12,2) DEFAULT NULL,
  `CALCULATED_COST_SHARING` decimal(12,2) DEFAULT NULL,
  `END_DATE` datetime(6) DEFAULT NULL,
  `JOB_CODE` varchar(255) DEFAULT NULL,
  `LINE_ITEM_NUMBER` int DEFAULT NULL,
  `ON_OFF_CAMPUS_FLAG` varchar(255) DEFAULT NULL,
  `PERSON_ID` varchar(255) DEFAULT NULL,
  `PERSON_NUMBER` int DEFAULT NULL,
  `RATE_CLASS_CODE` varchar(255) DEFAULT NULL,
  `RATE_NUMBER` int DEFAULT NULL,
  `RATE_TYPE_CODE` varchar(255) DEFAULT NULL,
  `SALARY_REQUESTED` decimal(12,2) DEFAULT NULL,
  `START_DATE` datetime(6) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`BUD_PER_RATE_BASE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `budget_period` (
  `BUDGET_PERIOD_ID` int NOT NULL AUTO_INCREMENT,
  `BUDGET_PERIOD` int DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `IS_OBLIGATED_PERIOD` varchar(1) DEFAULT NULL,
  `MODULE_ITEM_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(30) DEFAULT NULL,
  `PERIOD_LABEL` varchar(255) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `TOTAL_COST` decimal(12,2) DEFAULT NULL,
  `TOTAL_DIRECT_COST` decimal(12,2) DEFAULT NULL,
  `TOTAL_INDIRECT_COST` decimal(12,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `BUDGET_HEADER_ID` int DEFAULT NULL,
  `SUBCONTRACT_COST` decimal(12,2) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `COST_SHARING_AMOUNT` decimal(12,2) DEFAULT NULL,
  `TOTAL_COST_LIMIT` decimal(12,2) DEFAULT NULL,
  `TOTAL_DIRECT_COST_LIMIT` decimal(12,2) DEFAULT NULL,
  `UNDERRECOVERY_AMOUNT` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_PERIOD_ID`),
  KEY `BUDGET_PERIOD_FK1` (`BUDGET_HEADER_ID`),
  CONSTRAINT `BUDGET_PERIOD_FK1` FOREIGN KEY (`BUDGET_HEADER_ID`) REFERENCES `budget_header` (`BUDGET_HEADER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `budget_person_det_cal_amt` (
  `BUDGET_PERSON_DET_CAL_AMT_ID` int NOT NULL,
  `BUDGET_PERSON_DETAIL_ID` int DEFAULT NULL,
  `BUDGET_ID` int DEFAULT NULL,
  `BUDGET_PERIOD` int DEFAULT NULL,
  `BUDGET_PERIOD_NUMBER` int DEFAULT NULL,
  `APPLY_RATE_FLAG` varchar(1) DEFAULT NULL,
  `CALCULATED_COST` decimal(12,2) DEFAULT NULL,
  `CALCULATED_COST_SHARING` decimal(12,2) DEFAULT NULL,
  `LINE_ITEM_NUMBER` int DEFAULT NULL,
  `RATE_CLASS_CODE` varchar(3) DEFAULT NULL,
  `RATE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `RATE_TYPE_DESCRIPTION` varchar(200) DEFAULT NULL,
  `APPLICABLE_RATE` decimal(12,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_PERSON_DET_CAL_AMT_ID`),
  KEY `BUDGET_PERSON_DET_CAL_AMT_FK1` (`BUDGET_PERSON_DETAIL_ID`),
  KEY `BUDGET_PERSON_DET_CAL_AMT_FK3` (`RATE_CLASS_CODE`,`RATE_TYPE_CODE`),
  CONSTRAINT `BUDGET_PERSON_DET_CAL_AMT_FK1` FOREIGN KEY (`BUDGET_PERSON_DETAIL_ID`) REFERENCES `budget_person_detail` (`BUDGET_PERSON_DETAIL_ID`),
  CONSTRAINT `BUDGET_PERSON_DET_CAL_AMT_FK2` FOREIGN KEY (`RATE_CLASS_CODE`) REFERENCES `rate_class` (`RATE_CLASS_CODE`),
  CONSTRAINT `BUDGET_PERSON_DET_CAL_AMT_FK3` FOREIGN KEY (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`) REFERENCES `rate_type` (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `budget_person_detail` (
  `BUDGET_PERSON_DETAIL_ID` int NOT NULL AUTO_INCREMENT,
  `BUDGET_DETAILS_ID` int DEFAULT NULL,
  `PERSON_NAME` varchar(90) DEFAULT NULL,
  `PERSON_TYPE` varchar(3) DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `ROLODEX_ID` int DEFAULT NULL,
  `TBN_ID` varchar(9) DEFAULT NULL,
  `UNDERRECOVERY_AMOUNT` decimal(12,2) DEFAULT NULL,
  `PERCENT_CHARGED` decimal(5,2) DEFAULT NULL,
  `PERCENT_EFFORT` decimal(5,2) DEFAULT NULL,
  `COST_SHARING_AMOUNT` decimal(12,2) DEFAULT NULL,
  `COST_SHARING_PERCENT` decimal(5,2) DEFAULT NULL,
  `SALARY_REQUESTED` decimal(12,2) DEFAULT NULL,
  `TOTAL_SALARY` decimal(12,2) DEFAULT NULL,
  `NO_OF_MONTHS` decimal(4,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` timestamp(6) NULL DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `BUDGET_PERSON_ID` int DEFAULT NULL,
  `END_DATE` datetime(6) DEFAULT NULL,
  `START_DATE` datetime(6) DEFAULT NULL,
  `SPONSOR_REQUESTED_AMOUNT` decimal(12,2) DEFAULT NULL,
  `SALARY` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_PERSON_DETAIL_ID`),
  KEY `BUDGET_PERSONS_FK1` (`BUDGET_DETAILS_ID`),
  KEY `BUDGET_PERSONS_FK2` (`TBN_ID`),
  KEY `BUDGET_PERSONS_FK3` (`BUDGET_PERSON_ID`),
  CONSTRAINT `BUDGET_PERSONS_FK1` FOREIGN KEY (`BUDGET_DETAILS_ID`) REFERENCES `budget_detail` (`BUDGET_DETAILS_ID`),
  CONSTRAINT `BUDGET_PERSONS_FK2` FOREIGN KEY (`TBN_ID`) REFERENCES `tbn` (`TBN_ID`),
  CONSTRAINT `BUDGET_PERSONS_FK3` FOREIGN KEY (`BUDGET_PERSON_ID`) REFERENCES `budget_persons` (`BUDGET_PERSON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `budget_persons` (
  `BUDGET_PERSON_ID` int NOT NULL AUTO_INCREMENT,
  `APPOINTMENT_TYPE_CODE` varchar(255) DEFAULT NULL,
  `BUDGET_HEADER_ID` int DEFAULT NULL,
  `CALCULATION_BASE` decimal(12,2) DEFAULT NULL,
  `EFFECTIVE_DATE` datetime(6) DEFAULT NULL,
  `JOB_CODE` varchar(255) DEFAULT NULL,
  `NON_EMPLOYEE_FLAG` varchar(255) DEFAULT NULL,
  `PERSON_ID` varchar(255) DEFAULT NULL,
  `PERSON_NAME` varchar(255) DEFAULT NULL,
  `ROLODEX_ID` int DEFAULT NULL,
  `SALARY_ANNIVERSARY_DATE` datetime(6) DEFAULT NULL,
  `TBN_ID` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `personType` varchar(255) DEFAULT NULL,
  `PERSON_TYPE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_PERSON_ID`),
  KEY `BUDGET_PERS_FK2` (`APPOINTMENT_TYPE_CODE`),
  KEY `BUDGET_PERS_FK1` (`JOB_CODE`),
  KEY `BUDGET_PERS_FK3` (`TBN_ID`),
  CONSTRAINT `BUDGET_PERS_FK1` FOREIGN KEY (`JOB_CODE`) REFERENCES `job_code` (`JOB_CODE`),
  CONSTRAINT `BUDGET_PERS_FK2` FOREIGN KEY (`APPOINTMENT_TYPE_CODE`) REFERENCES `appointment_type` (`APPOINTMENT_TYPE_CODE`),
  CONSTRAINT `BUDGET_PERS_FK3` FOREIGN KEY (`TBN_ID`) REFERENCES `tbn` (`TBN_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `budget_rate_and_base` (
  `BUDGET_RATE_AND_BASE_ID` int NOT NULL AUTO_INCREMENT,
  `BASE_COST` decimal(12,2) DEFAULT NULL,
  `BUDGET_ID` int DEFAULT NULL,
  `BUDGET_PERIOD` int DEFAULT NULL,
  `BUDGET_PERIOD_NUMBER` int DEFAULT NULL,
  `LINE_ITEM_NUMBER` int DEFAULT NULL,
  `RATE_CLASS_CODE` varchar(3) DEFAULT NULL,
  `RATE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `BUDGET_DETAILS_ID` int DEFAULT NULL,
  `APPLIED_RATE` decimal(5,2) DEFAULT NULL,
  `BASE_COST_SHARING` decimal(14,2) DEFAULT NULL,
  `CALCULATED_COST` decimal(14,2) DEFAULT NULL,
  `CALCULATED_COST_SHARING` decimal(14,2) DEFAULT NULL,
  `END_DATE` datetime(6) DEFAULT NULL,
  `ON_OFF_CAMPUS_FLAG` varchar(255) DEFAULT NULL,
  `RATE_NUMBER` int DEFAULT NULL,
  `START_DATE` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_RATE_AND_BASE_ID`),
  KEY `BUDGET_RATE_AND_BASE_FK1` (`BUDGET_DETAILS_ID`),
  CONSTRAINT `BUDGET_RATE_AND_BASE_FK1` FOREIGN KEY (`BUDGET_DETAILS_ID`) REFERENCES `budget_detail` (`BUDGET_DETAILS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `budget_status` (
  `BUDGET_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`BUDGET_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `budget_template` (
  `BUDGET_TEMPLATE_ID` int NOT NULL,
  `COST_ELEMENT` varchar(200) DEFAULT NULL,
  `IS_SYSTEM_GENRTED_COST_ELEMENT` varchar(200) DEFAULT NULL,
  `BUDGET_TEMPLATE_TYPE_ID` int DEFAULT NULL,
  `RULE_ID` varchar(50) DEFAULT NULL,
  `SORT_ORDER` int DEFAULT NULL,
  `SYSTEM_GEN_COST_ELEMENT_TYPE` varchar(50) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_TEMPLATE_ID`),
  KEY `BUDGET_TEMPLATE_FK1` (`COST_ELEMENT`),
  KEY `BUDGET_TEMPLATE_FK2` (`BUDGET_TEMPLATE_TYPE_ID`),
  CONSTRAINT `BUDGET_TEMPLATE_FK1` FOREIGN KEY (`COST_ELEMENT`) REFERENCES `cost_element` (`COST_ELEMENT`),
  CONSTRAINT `BUDGET_TEMPLATE_FK2` FOREIGN KEY (`BUDGET_TEMPLATE_TYPE_ID`) REFERENCES `budget_template_type` (`BUDGET_TEMPLATE_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `budget_template_type` (
  `BUDGET_TEMPLATE_TYPE_ID` int NOT NULL,
  `DESCRIPTION` varchar(100) DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_TEMPLATE_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `budget_type` (
  `BUDGET_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `business_rule_function` (
  `FUNCTION_NAME` varchar(50) NOT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `SUB_MODULE_CODE` int DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `DB_FUNCTION_NAME` varchar(32) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`FUNCTION_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `business_rule_function_arg` (
  `FUNCTION_NAME` varchar(50) NOT NULL,
  `ARGUMENT_SEQUENCE_NUMBER` int NOT NULL,
  `ARGUMENT_NAME` varchar(30) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `DATA_TYPE` varchar(9) DEFAULT NULL,
  `DEFAULT_VALUE` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `LOOKUP_WINDOW_NAME` varchar(100) DEFAULT NULL,
  `LOOKUP_TYPE` varchar(200) DEFAULT NULL,
  `ARGUMENT_TYPE` varchar(1) DEFAULT NULL,
  `ARGUMENT_LABEL` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`FUNCTION_NAME`,`ARGUMENT_SEQUENCE_NUMBER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `business_rule_log` (
  `RULE_LOG_ID` int NOT NULL,
  `MODULE_ITEM_KEY` varchar(20) DEFAULT NULL,
  `RULE_ID` int DEFAULT NULL,
  `RULE_EXPRESSION_ID` varchar(1000) DEFAULT NULL,
  `ERROR_MESSAGE` varchar(1000) DEFAULT NULL,
  `ERROR_DATE` date DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `SUB_MODULE_CODE` int DEFAULT NULL,
  `SUB_MODULE_ITEM_KEY` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`RULE_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `business_rule_variable` (
  `VARIABLE_NAME` varchar(30) NOT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `SUB_MODULE_CODE` int DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `TABLE_NAME` varchar(30) DEFAULT NULL,
  `COLUMN_NAME` varchar(30) DEFAULT NULL,
  `SHOW_LOOKUP` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  `LOOKUP_WINDOW_NAME` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`VARIABLE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `business_rules` (
  `RULE_ID` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `RULE_TYPE` varchar(2) DEFAULT NULL,
  `UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `SUB_MODULE_CODE` int DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `RULE_EXPRESSION` varchar(300) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  `NOTIFICATION_ID` int DEFAULT NULL,
  `USER_MESSAGE` varchar(4000) DEFAULT NULL,
  `RULE_EVALUATION_ORDER` int DEFAULT NULL,
  `MAP_ID` int DEFAULT NULL,
  `RULE_CATEGORY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RULE_ID`),
  KEY `BUSINESS_RULES_FK1` (`MAP_ID`),
  CONSTRAINT `BUSINESS_RULES_FK1` FOREIGN KEY (`MAP_ID`) REFERENCES `workflow_map` (`MAP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `business_rules_exp_type` (
  `EXPRESSION_TYPE_CODE` varchar(1) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`EXPRESSION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `business_rules_experssion` (
  `RULES_EXPERSSION_ID` int NOT NULL,
  `RULE_ID` int DEFAULT NULL,
  `EXPRESSION_NUMBER` int DEFAULT NULL,
  `EXPRESSION_TYPE_CODE` varchar(1) DEFAULT NULL,
  `LVALUE` varchar(4000) DEFAULT NULL,
  `CONDITION_OPERATOR` varchar(25) DEFAULT NULL,
  `RVALUE` varchar(4000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  `PARENT_EXPRESSION_NUMBER` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `canned_cost_element` (
  `CANNED_COST_ELEMENT_ID` int NOT NULL,
  `MODULE_ITEM_CODE` int DEFAULT NULL,
  `COST_ELEMENT` varchar(50) DEFAULT NULL,
  `RULE_ID` varchar(10) DEFAULT NULL,
  `UPDATE_TIMESTAMP` date DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `SORT_ORDER` int DEFAULT NULL,
  `IS_SYSTEM_GENRTED_COST_ELEMENT` varchar(1) DEFAULT NULL,
  `SYSTEM_GEN_COST_ELEMENT_TYPE` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`CANNED_COST_ELEMENT_ID`),
  KEY `CANNED_COST_ELEMENT_FK1` (`COST_ELEMENT`),
  CONSTRAINT `CANNED_COST_ELEMENT_FK1` FOREIGN KEY (`COST_ELEMENT`) REFERENCES `cost_element` (`COST_ELEMENT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim` (
  `CLAIM_ID` int NOT NULL AUTO_INCREMENT,
  `ACCOUNT_NUMBER` varchar(255) DEFAULT NULL,
  `AUDITOR_NAME` varchar(255) DEFAULT NULL,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(255) DEFAULT NULL,
  `CLAIM_STATUS_CODE` varchar(255) DEFAULT NULL,
  `CLAIM_SUBMISSION_DATE` datetime(6) DEFAULT NULL,
  `INVOICE_NUMBER` varchar(100) DEFAULT NULL,
  `INVOICE_DATE` datetime DEFAULT NULL,
  `FUND_RECEIVED_DATE` datetime DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `CREATE_USER` varchar(255) DEFAULT NULL,
  `DURATION` varchar(255) DEFAULT NULL,
  `END_DATE` datetime(6) DEFAULT NULL,
  `EXTERNAL_AUDITOR_NAME` varchar(255) DEFAULT NULL,
  `FO_APPROVAL_DATE` datetime(6) DEFAULT NULL,
  `FO_DESIGNATION` varchar(255) DEFAULT NULL,
  `FO_EMAIL` varchar(255) DEFAULT NULL,
  `FO_NAME` varchar(255) DEFAULT NULL,
  `FUNDING_SCHEME_CERTIFICATION1` varchar(4000) DEFAULT NULL,
  `FUNDING_SCHEME_CERTIFICATION2` varchar(2000) DEFAULT NULL,
  `FUNDING_SCHEME_ENDORSEMENT` varchar(2000) DEFAULT NULL,
  `HOST_UNIT_NUMBER` varchar(255) DEFAULT NULL,
  `PAYEE_ACCOUNT_NAME` varchar(255) DEFAULT NULL,
  `PAYEE_ACCOUNT_NUMBER` varchar(255) DEFAULT NULL,
  `PAYEE_BANK` varchar(255) DEFAULT NULL,
  `RDO_APPROVAL_DATE` datetime(6) DEFAULT NULL,
  `RDO_DESIGNATION` varchar(255) DEFAULT NULL,
  `RDO_EMAIL` varchar(255) DEFAULT NULL,
  `RDO_NAME` varchar(255) DEFAULT NULL,
  `RECIPIENT_ORGANIZATION` varchar(255) DEFAULT NULL,
  `RECIPIENT_POSTAL_CODE` varchar(255) DEFAULT NULL,
  `RECIPIENT_STATE` varchar(255) DEFAULT NULL,
  `RECIPIENT_STREET_NAME` varchar(255) DEFAULT NULL,
  `RSO_APPROVAL_DATE` datetime(6) DEFAULT NULL,
  `RSO_DESIGNATION` varchar(255) DEFAULT NULL,
  `RSO_EMAIL` varchar(255) DEFAULT NULL,
  `RSO_NAME` varchar(255) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `START_DATE` datetime(6) DEFAULT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `RSO_PHONE_NUMBER` varchar(20) DEFAULT NULL,
  `FO_PHONE_NUMBER` varchar(20) DEFAULT NULL,
  `RDO_PHONE_NUMBER` varchar(20) DEFAULT NULL,
  `CLAIM_SUBMIT_USER` varchar(255) DEFAULT NULL,
  `OVERHEAD_PERCENTAGE` decimal(19,2) DEFAULT NULL,
  `CLAIM_NUMBER` varchar(12) DEFAULT NULL,
  `TOTAL_AMOUNT` decimal(12,2) DEFAULT NULL,
  `ADJUSTED_INDIRECT_COST` decimal(12,2) DEFAULT NULL,
  `MIGRATED_REIM_PREV_IDC_AMT` decimal(12,2) DEFAULT NULL,
  `IDC_CUM_CLAIM_AMT_UPTO_CLAIM` decimal(12,2) DEFAULT NULL,
  `IDC_CUM_EXP_UPTO_PREV_CLAIM` decimal(12,2) DEFAULT NULL,
  `IDC_COMMITMENT_UPTO_PREV_CLAIM` decimal(12,2) DEFAULT NULL,
  `IDC_AMOUNT_REQUESTED` decimal(12,2) DEFAULT NULL,
  `IDC_AMOUNT_FORCASTED` decimal(12,2) DEFAULT NULL,
  `IDC_CUM_CLAIM_AMT_UPTO_LATEST` decimal(12,2) DEFAULT NULL,
  `IDC_LATEST_AMOUNT` decimal(12,2) DEFAULT NULL,
  `FUNDER_APPROVAL_DATE` datetime DEFAULT NULL,
  `IDC_ORGINAL_AMOUNT` decimal(12,2) DEFAULT NULL,
  `PAYMENT_DATE` datetime DEFAULT NULL,
  `DOCUMENT_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`CLAIM_ID`),
  KEY `CLAIM_FK1` (`CLAIM_STATUS_CODE`),
  KEY `CLAIM_IDX1` (`AWARD_NUMBER`),
  KEY `CLAIM_IDX2` (`UPDATE_USER`),
  KEY `CLAIM_IDX3` (`CREATE_USER`),
  KEY `CLAIM_FK2` (`AWARD_ID`),
  CONSTRAINT `CLAIM_FK1` FOREIGN KEY (`CLAIM_STATUS_CODE`) REFERENCES `claim_status` (`CLAIM_STATUS_CODE`),
  CONSTRAINT `CLAIM_FK2` FOREIGN KEY (`AWARD_ID`) REFERENCES `award` (`AWARD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_action_log` (
  `ACTION_LOG_ID` int NOT NULL AUTO_INCREMENT,
  `CLAIM_ID` int DEFAULT NULL,
  `CLAIM_STATUS_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ACTION_LOG_ID`),
  KEY `CLAIM_ACTION_LOG_FK1` (`CLAIM_STATUS_CODE`),
  CONSTRAINT `CLAIM_ACTION_LOG_FK1` FOREIGN KEY (`CLAIM_STATUS_CODE`) REFERENCES `claim_status` (`CLAIM_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_attachment` (
  `CLAIM_ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `CLAIM_ID` int DEFAULT NULL,
  `TYPE_CODE` varchar(3) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `DOCUMENT_STATUS_CODE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `DOCUMENT_ID` int DEFAULT NULL,
  `FILE_DATA_ID` varchar(36) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `MIME_TYPE` varchar(100) DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `CLAIM_NUMBER` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`CLAIM_ATTACHMENT_ID`),
  KEY `CLAIM_ATTACHMENT_FK1` (`CLAIM_ID`),
  KEY `CLAIM_ATTACHMENT_FK2` (`TYPE_CODE`),
  CONSTRAINT `CLAIM_ATTACHMENT_FK1` FOREIGN KEY (`CLAIM_ID`) REFERENCES `claim` (`CLAIM_ID`),
  CONSTRAINT `CLAIM_ATTACHMENT_FK2` FOREIGN KEY (`TYPE_CODE`) REFERENCES `claim_attachment_type` (`TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_attachment_type` (
  `TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(300) DEFAULT NULL,
  `UPDATE_TIMESTAMP` date DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `IS_PRIVATE` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_configuration_data` (
  `CONFIGURATION_KEY` varchar(100) NOT NULL,
  `CONFIGURATION_VALUE` varchar(250) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`CONFIGURATION_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_files` (
  `FILE_ID` int NOT NULL AUTO_INCREMENT,
  `FILE_NAME` varchar(50) DEFAULT NULL,
  `NO_OF_RECORDS` int DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `FILE_INTERFACE` varchar(45) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`FILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_funding_scheme` (
  `FUNDING_SCHEME_CODE` varchar(8) NOT NULL,
  `CERTIFICATION1` longtext,
  `CERTIFICATION2` longtext,
  `ENDORSEMENT` longtext,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `OVERRIDE_NEGATIVE_AMOUNT` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`FUNDING_SCHEME_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_gl_account_code` (
  `GL_ACCOUNT_CODE` varchar(50) NOT NULL,
  `DESCRIPTION` varchar(200) NOT NULL,
  `IS_CONTROLLED_GL` varchar(1) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  PRIMARY KEY (`GL_ACCOUNT_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_invoice` (
  `INVOICE_ID` int NOT NULL AUTO_INCREMENT,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `CLAIM_ID` int DEFAULT NULL,
  `CLAIM_NUMBER` varchar(12) DEFAULT NULL,
  `DOCUMENT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `CURRENCY_CODE` varchar(4) DEFAULT NULL,
  `DOCUMENT_HEADER_TEXT` varchar(25) DEFAULT NULL,
  `CUSTOMER_EMAIL_ADDRESS` varchar(60) DEFAULT NULL,
  `REQUESTER_EMAIL_ADDRESS` varchar(60) DEFAULT NULL,
  `HEADER_POSTING_KEY` varchar(2) DEFAULT NULL,
  `DOCUMENT_DATE` datetime DEFAULT NULL,
  `BASE_DATE` datetime DEFAULT NULL,
  `COMPANY_CODE` varchar(5) DEFAULT NULL,
  `PARTICULARS1` varchar(50) DEFAULT NULL,
  `PARTICULARS2` varchar(50) DEFAULT NULL,
  `PARTICULARS3` varchar(50) DEFAULT NULL,
  `PARTICULARS4` varchar(50) DEFAULT NULL,
  `CONTACT_TELEPHONE_NO` varchar(50) DEFAULT NULL,
  `ACTION_INDICATOR` varchar(10) DEFAULT NULL,
  `ASSIGNMENT_FIELD` varchar(50) DEFAULT NULL,
  `DESCRIPTION` varchar(50) DEFAULT NULL,
  `GL_ACCOUNT_CODE` varchar(10) DEFAULT NULL,
  `GRANT_CODE` varchar(20) DEFAULT NULL,
  `PROFIT_CENTRE` varchar(10) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  `IS_E_INVOICE` varchar(1) DEFAULT NULL,
  `VENDOR_SB_UNIT_NUMBER` varchar(5) DEFAULT NULL,
  `GE_BIZ_PO_NUMBER` varchar(12) DEFAULT NULL,
  `ATTENTION_TO` varchar(100) DEFAULT NULL,
  `CUSTOMER_NUMBER` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`INVOICE_ID`),
  KEY `CLAIM_INVOICE_FK1` (`GL_ACCOUNT_CODE`),
  CONSTRAINT `CLAIM_INVOICE_FK1` FOREIGN KEY (`GL_ACCOUNT_CODE`) REFERENCES `claim_gl_account_code` (`GL_ACCOUNT_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_invoice_details` (
  `INVOICE_DETAIL_ID` int NOT NULL AUTO_INCREMENT,
  `INVOICE_ID` int NOT NULL,
  `CLAIM_ID` int DEFAULT NULL,
  `LINE_ITEM_POSTING_KEY` varchar(2) DEFAULT NULL,
  `GRT_WBS` varchar(20) DEFAULT NULL,
  `BA_CODE` varchar(4) DEFAULT NULL,
  `GL_ACCOUNT_CODE` varchar(10) DEFAULT NULL,
  `TAX_CODE` varchar(2) DEFAULT NULL,
  `CLAIM_AMOUNT` decimal(12,2) DEFAULT NULL,
  `SUB_CONTRACT_AMOUNT` decimal(12,2) DEFAULT NULL,
  `DESCRIPTION` varchar(50) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  `campus` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`INVOICE_DETAIL_ID`),
  KEY `CLAIM_INVOICE_DETAILS_FK2` (`GL_ACCOUNT_CODE`),
  KEY `CLAIM_INVOICE_DETAILS_FK1_idx` (`INVOICE_ID`),
  CONSTRAINT `CLAIM_INVOICE_DETAILS_FK1` FOREIGN KEY (`INVOICE_ID`) REFERENCES `claim_invoice` (`INVOICE_ID`),
  CONSTRAINT `CLAIM_INVOICE_DETAILS_FK2` FOREIGN KEY (`GL_ACCOUNT_CODE`) REFERENCES `claim_gl_account_code` (`GL_ACCOUNT_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_invoice_details_log` (
  `INVOICE_DETAIL_ID` int NOT NULL AUTO_INCREMENT,
  `CLAIM_INVOICE_LOG_ID` int NOT NULL,
  `INVOICE_ID` int DEFAULT NULL,
  `CLAIM_ID` int DEFAULT NULL,
  `LINE_ITEM_POSTING_KEY` varchar(2) DEFAULT NULL,
  `GRT_WBS` varchar(24) DEFAULT NULL,
  `BA_CODE` varchar(4) DEFAULT NULL,
  `GL_ACCOUNT_CODE` varchar(10) DEFAULT NULL,
  `TAX_CODE` varchar(2) DEFAULT NULL,
  `CLAIM_AMOUNT` decimal(12,2) DEFAULT NULL,
  `SUB_CONTRACT_AMOUNT` decimal(12,2) DEFAULT NULL,
  `DESCRIPTION` varchar(50) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  `campus` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`INVOICE_DETAIL_ID`),
  KEY `CLAIM_INVOICE_DETAILS_FK2` (`GL_ACCOUNT_CODE`),
  KEY `CLAIM_INVOICE_DETAILS_LOG_FK1_idx` (`CLAIM_INVOICE_LOG_ID`),
  CONSTRAINT `CLAIM_INVOICE_DETAILS_LOG_FK1` FOREIGN KEY (`CLAIM_INVOICE_LOG_ID`) REFERENCES `claim_invoice_log` (`CLAIM_INVOICE_LOG_ID`),
  CONSTRAINT `CLAIM_INVOICE_DETAILS_LOG_FK2` FOREIGN KEY (`GL_ACCOUNT_CODE`) REFERENCES `claim_gl_account_code` (`GL_ACCOUNT_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_invoice_feed_type` (
  `TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(100) NOT NULL,
  `IS_ACTIVE` varchar(1) NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_invoice_log` (
  `CLAIM_INVOICE_LOG_ID` int NOT NULL AUTO_INCREMENT,
  `INVOICE_ID` int DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `CLAIM_ID` int DEFAULT NULL,
  `CLAIM_NUMBER` varchar(12) DEFAULT NULL,
  `INDICATOR` varchar(1) DEFAULT NULL,
  `DOCUMENT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `CURRENCY_CODE` varchar(4) DEFAULT NULL,
  `DOCUMENT_HEADER_TEXT` varchar(25) DEFAULT NULL,
  `CUSTOMER_EMAIL_ADDRESS` varchar(60) DEFAULT NULL,
  `REQUESTER_EMAIL_ADDRESS` varchar(60) DEFAULT NULL,
  `HEADER_POSTING_KEY` varchar(2) DEFAULT NULL,
  `DOCUMENT_DATE` datetime DEFAULT NULL,
  `BASE_DATE` datetime DEFAULT NULL,
  `COMPANY_CODE` varchar(5) DEFAULT NULL,
  `PARTICULARS1` varchar(50) DEFAULT NULL,
  `PARTICULARS2` varchar(50) DEFAULT NULL,
  `PARTICULARS3` varchar(50) DEFAULT NULL,
  `PARTICULARS4` varchar(50) DEFAULT NULL,
  `CONTACT_TELEPHONE_NO` varchar(50) DEFAULT NULL,
  `ACTION_INDICATOR` varchar(10) DEFAULT NULL,
  `INPUT_DOCUMENT_NUMBER` varchar(30) DEFAULT NULL,
  `FISCAL_YEAR` varchar(4) DEFAULT NULL,
  `TYPE_CODE` varchar(3) DEFAULT NULL,
  `BATCH_ID` varchar(100) DEFAULT NULL,
  `ASSIGNMENT_FIELD` varchar(50) DEFAULT NULL,
  `DESCRIPTION` varchar(50) DEFAULT NULL,
  `GL_ACCOUNT_CODE` varchar(10) DEFAULT NULL,
  `GRANT_CODE` varchar(20) DEFAULT NULL,
  `PROFIT_CENTRE` varchar(10) DEFAULT NULL,
  `OUTPUT_DOCUMENT_NUMBER` varchar(30) DEFAULT NULL,
  `STATUS` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  `IS_E_INVOICE` varchar(1) DEFAULT NULL,
  `VENDOR_SB_UNIT_NUMBER` varchar(5) DEFAULT NULL,
  `GE_BIZ_PO_NUMBER` varchar(12) DEFAULT NULL,
  `ATTENTION_TO` varchar(100) DEFAULT NULL,
  `CUSTOMER_NUMBER` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`CLAIM_INVOICE_LOG_ID`),
  KEY `CLAIM_INVOICE_LOG_FK1` (`GL_ACCOUNT_CODE`),
  KEY `CLAIM_INVOICE_LOG_FK2` (`TYPE_CODE`),
  CONSTRAINT `CLAIM_INVOICE_LOG_FK1` FOREIGN KEY (`GL_ACCOUNT_CODE`) REFERENCES `claim_gl_account_code` (`GL_ACCOUNT_CODE`),
  CONSTRAINT `CLAIM_INVOICE_LOG_FK2` FOREIGN KEY (`TYPE_CODE`) REFERENCES `claim_invoice_feed_type` (`TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_invoice_metadata` (
  `BA_CODE` varchar(4) NOT NULL,
  `DOCUMENT_TYPE_CODE` varchar(3) NOT NULL,
  `DOCUMENT_TYPE_DESC` varchar(50) DEFAULT NULL,
  `REVERSAL_DOCUMENT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `REVERSAL_DOCUMENT_TYPE_DESC` varchar(50) DEFAULT NULL,
  `HEADER_POSTING_KEY` varchar(2) DEFAULT NULL,
  `LINE_ITEM_POSTING_KEY` varchar(2) DEFAULT NULL,
  `REVERSAL_HEADER_POSTING_KEY` varchar(2) DEFAULT NULL,
  `REVERSAL_LINE_ITEM_POSTING_KEY` varchar(2) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  PRIMARY KEY (`BA_CODE`,`DOCUMENT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_letter_template_mapping` (
  `CLAIM_TEMPLATE_CODE` varchar(10) NOT NULL,
  `LETTER_TEMPLATE_TYPE_CODE` varchar(10) NOT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`CLAIM_TEMPLATE_CODE`,`LETTER_TEMPLATE_TYPE_CODE`),
  KEY `CLM_MAP_FKEY_2_idx` (`LETTER_TEMPLATE_TYPE_CODE`),
  CONSTRAINT `CLM_MAP_FKEY_1` FOREIGN KEY (`CLAIM_TEMPLATE_CODE`) REFERENCES `claim_templates` (`CLAIM_TEMPLATE_CODE`),
  CONSTRAINT `CLM_MAP_FKEY_2` FOREIGN KEY (`LETTER_TEMPLATE_TYPE_CODE`) REFERENCES `letter_template_type` (`LETTER_TEMPLATE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_manpower` (
  `CLAIM_MANPOWER_ID` int NOT NULL AUTO_INCREMENT,
  `CLAIM_ID` int DEFAULT NULL,
  `CLAIM_NUMBER` varchar(12) DEFAULT NULL,
  `AWARD_MANPOWER_ID` int DEFAULT NULL,
  `AWARD_MANPOWER_RESOURCE_ID` int DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `IS_GRANT_USED` varchar(1) DEFAULT NULL,
  `SOURCE_OF_FUNDING` varchar(150) DEFAULT NULL,
  `IS_APPROVED_FOR_FOREIGN_STAFF` varchar(1) DEFAULT NULL,
  `DATE_OF_APPROVAL` datetime DEFAULT NULL,
  `IS_JOB_REQ_PHD_QUALIFICATION` varchar(1) DEFAULT NULL,
  `PERCNTGE_TIME_SPENT_ON_PROG` decimal(5,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `INSTITUTION` varchar(100) DEFAULT NULL,
  `NATIONALITY_WAIVER_DESC` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`CLAIM_MANPOWER_ID`),
  KEY `CLAIM_MANPOWER_FK1` (`CLAIM_ID`),
  CONSTRAINT `CLAIM_MANPOWER_FK1` FOREIGN KEY (`CLAIM_ID`) REFERENCES `claim` (`CLAIM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_master_dataset_rt` (
  `CLAIM_MASTER_SET_ID` int NOT NULL AUTO_INCREMENT,
  `ACCOUNT_NUMBER` varchar(100) DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `AWARD_ID` decimal(22,0) DEFAULT NULL,
  `CLAIM_ID` int DEFAULT NULL,
  `CLAIM_NUMBER` varchar(12) DEFAULT NULL,
  `CLAIM_TITLE` varchar(300) DEFAULT NULL,
  `CLAIM_STATUS_CODE` varchar(100) DEFAULT NULL,
  `CLAIM_STATUS` varchar(200) DEFAULT NULL,
  `CLAIM_SUBMISSION_DATE` datetime(6) DEFAULT NULL,
  `START_DATE` datetime(6) DEFAULT NULL,
  `END_DATE` datetime(6) DEFAULT NULL,
  `CLAIM_DURATION` varchar(255) DEFAULT NULL,
  `CLAIM_AMOUNT` decimal(12,2) DEFAULT NULL,
  `CREATE_USER` varchar(200) DEFAULT NULL,
  `CLAIM_PREPARER` varchar(200) DEFAULT NULL,
  `ORIGINAL_APPROVED_BUDGET` decimal(12,2) DEFAULT NULL,
  `LATEST_APPROVED_BUDGET` decimal(12,2) DEFAULT NULL,
  `BANK_CLEARANCE_DATE` datetime DEFAULT NULL,
  `INVOICE_NUMBER` varchar(100) DEFAULT NULL,
  `INVOICE_DATE` datetime DEFAULT NULL,
  `CUM_CLAIM_AMOUNT_TO_DATE` decimal(12,2) DEFAULT NULL,
  `TOTAL_CUM_CLAIM_AMOUNT_UPTO_CLAIM` decimal(12,2) DEFAULT NULL,
  `FM_POSTING_DATE` datetime DEFAULT NULL,
  `REFERENCE_DOC_NUMBER` varchar(100) DEFAULT NULL,
  `L2_WBS_NUMBER` varchar(100) DEFAULT NULL,
  `BUDGET_CATEGORY` varchar(100) DEFAULT NULL,
  `BASIS_OF_CLAIM_SUBMISSION_DATE` int DEFAULT NULL,
  `BASIS_OF_CLAIM_INVOICE_DATE` int DEFAULT NULL,
  `INVOICE_AMOUNT` decimal(12,2) DEFAULT NULL,
  `ACTUAL_REVENUE` decimal(12,2) DEFAULT NULL,
  `ACTUAL_EXPENSE` decimal(12,2) DEFAULT NULL,
  `LAST_SYNC_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`CLAIM_MASTER_SET_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_number_manual_generator` (
  `next_val` bigint NOT NULL,
  `FISCAL_YEAR` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`next_val`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_number_system_generator` (
  `next_val` bigint NOT NULL,
  `FISCAL_YEAR` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`next_val`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_output_gst_tax_code` (
  `OUTPUT_GST_CATEGORY` varchar(20) NOT NULL,
  `TAX_CODE` varchar(2) NOT NULL,
  `TAX_DESCRIPTION` varchar(50) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  PRIMARY KEY (`OUTPUT_GST_CATEGORY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_status` (
  `CLAIM_STATUS_CODE` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CLAIM_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_summary` (
  `CLAIM_SUMMARY_ID` int NOT NULL AUTO_INCREMENT,
  `AMOUNT_FORCASTED` decimal(19,2) DEFAULT NULL,
  `AMOUNT_REQUESTED` decimal(19,2) DEFAULT NULL,
  `BUDGET_CATEGORY_CODE` varchar(255) DEFAULT NULL,
  `BUDGET_HEADER_ID` int DEFAULT NULL,
  `CLAIM_ID` int DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `TOTAL_AMOUNT` decimal(19,2) DEFAULT NULL,
  `AMOUNT_REQ_CURRENT_CLAIM` decimal(12,2) DEFAULT NULL,
  `CUM_CLAIM_AMOUNT_UPTO_CLAIM` decimal(12,2) DEFAULT NULL,
  `CUM_EXPENSE_UPTO_PREV_CLAIM` decimal(12,2) DEFAULT NULL,
  `COMMITMENTS_UPTO_PREV_CLAIM` decimal(12,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `LATEST_APPROVED_BUDGET` decimal(19,2) DEFAULT NULL,
  `ORIGINAL_APPROVED_BUDGET` decimal(19,2) DEFAULT NULL,
  `PREV_CLAIM_TOTAL_AMOUNT` decimal(19,2) DEFAULT NULL,
  `CLAIM_NUMBER` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`CLAIM_SUMMARY_ID`),
  KEY `CLAIM_SUMMARY_FK3` (`BUDGET_CATEGORY_CODE`),
  KEY `CLAIM_SUMMARY_FK1` (`CLAIM_ID`),
  CONSTRAINT `CLAIM_SUMMARY_FK1` FOREIGN KEY (`CLAIM_ID`) REFERENCES `claim` (`CLAIM_ID`),
  CONSTRAINT `CLAIM_SUMMARY_FK3` FOREIGN KEY (`BUDGET_CATEGORY_CODE`) REFERENCES `budget_category` (`BUDGET_CATEGORY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_summary_details` (
  `CLAIM_DETAILS_ID` int NOT NULL AUTO_INCREMENT,
  `CLAIM_NUMBER` varchar(12) DEFAULT NULL,
  `CLAIM_SUMMARY_ID` int DEFAULT NULL,
  `BUDGET_CATEGORY_CODE` varchar(3) DEFAULT NULL,
  `AWARD_EXPENSE_TRANS_ID` int DEFAULT NULL,
  `LEVEL_OF_SUPPORT_PERCENT` decimal(5,2) DEFAULT NULL,
  `TOTAL_AMOUNT` decimal(12,2) DEFAULT NULL,
  `ADJUSTED_TOTAL` decimal(12,2) DEFAULT NULL,
  `DESCRIPTION` longtext,
  `INVOICE_DATE` datetime DEFAULT NULL,
  `BUDGET_APPROVAL_DATE` datetime DEFAULT NULL,
  `DOCUMENT_NUMBER` varchar(100) DEFAULT NULL,
  `FM_POSTING_DATE` datetime DEFAULT NULL,
  `PAYMENT_DATE` datetime DEFAULT NULL,
  `DELIVERY_DATE` datetime DEFAULT NULL,
  `VENDOR_NAME` varchar(300) DEFAULT NULL,
  `EXPENSE_ITEM_DESCRIPTION` varchar(2000) DEFAULT NULL,
  `ORIGINAL_LINE_ITEM_DESCRIPTION` varchar(2000) DEFAULT NULL,
  `COUNTRY` varchar(100) DEFAULT NULL,
  `TRAVEL_TYPE` varchar(100) DEFAULT NULL,
  `PERIOD_OF_VISIT` varchar(50) DEFAULT NULL,
  `NUMBER_OF_DAYS_OF_VISIT` int DEFAULT NULL,
  `PERIOD_OF_OFFICIAL_VISIT` varchar(50) DEFAULT NULL,
  `NUMBER_OF_DAYS_OF_OFFICIAL_VISIT` int DEFAULT NULL,
  `TRIP_TOTAL_COST` decimal(12,2) DEFAULT NULL,
  `EXPENSE_CAPPED_AMOUNT` decimal(12,2) DEFAULT NULL,
  `QUALIFYING_COST` decimal(12,2) DEFAULT NULL,
  `PREV_EXLUDED_SUMMARY_DTL_ID` int DEFAULT NULL,
  `IS_EXCLUDED_FLAG` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `EXPENSE_CAPPED` varchar(255) DEFAULT NULL,
  `PREV_ADJUSTED_SUMMARY_DTL_ID` int DEFAULT NULL,
  `INTERNAL_ORDER_CODE` varchar(50) DEFAULT NULL,
  `CLAIM_ID` int DEFAULT NULL,
  `ENCRYPTED_AMOUNT` varchar(200) DEFAULT NULL,
  `MANPOWER_PAYROLL_ID` int DEFAULT NULL,
  `UNIT_PRICE` decimal(12,2) DEFAULT NULL,
  `DESCRIPTION_OF_EQUIPMENT` varchar(100) DEFAULT NULL,
  `CITY` varchar(50) DEFAULT NULL,
  `ORGANIZATION_OR_CONFERENCE_NAME` varchar(100) DEFAULT NULL,
  `COLA_RATE_PER_DAY` decimal(12,2) DEFAULT NULL,
  `AIR_FARE_AMOUNT` decimal(12,2) DEFAULT NULL,
  `LOCAL_CLAIM_REIMBURSMENT` varchar(255) DEFAULT NULL,
  `OTHER_EXPENSE_AMOUNT` decimal(12,2) DEFAULT NULL,
  `COUNTRY_CODE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CLAIM_DETAILS_ID`),
  KEY `CLAIM_SUMMARY_DETAILS_FK2` (`CLAIM_SUMMARY_ID`),
  KEY `CLAIM_SUMMARY_DETAILS_FK3` (`AWARD_EXPENSE_TRANS_ID`),
  KEY `CLAIM_SUMMARY_DETAILS_FK4` (`COUNTRY`),
  CONSTRAINT `CLAIM_SUMMARY_DETAILS_FK2` FOREIGN KEY (`CLAIM_SUMMARY_ID`) REFERENCES `claim_summary` (`CLAIM_SUMMARY_ID`),
  CONSTRAINT `CLAIM_SUMMARY_DETAILS_FK3` FOREIGN KEY (`AWARD_EXPENSE_TRANS_ID`) REFERENCES `award_expense_transactions` (`AWARD_EXPENSE_TRANS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `claim_templates` (
  `CLAIM_TEMPLATE_CODE` varchar(10) NOT NULL,
  `DESCRIPTION` varchar(300) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`CLAIM_TEMPLATE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `clauses` (
  `CLAUSES_CODE` int NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(1000) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `IS_SYSTEM_SPECIFIC` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  `CLAUSES_GROUP_CODE` int DEFAULT NULL,
  PRIMARY KEY (`CLAUSES_CODE`),
  KEY `CLAUSES_FK1` (`CLAUSES_GROUP_CODE`),
  CONSTRAINT `CLAUSES_FK1` FOREIGN KEY (`CLAUSES_GROUP_CODE`) REFERENCES `clauses_group` (`CLAUSES_GROUP_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `clauses_bank` (
  `CLAUSES_CODE` int NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(1000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`CLAUSES_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `clauses_group` (
  `CLAUSES_GROUP_CODE` int NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`CLAUSES_GROUP_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `code_table_configuration` (
  `TABLE_NAME` varchar(60) NOT NULL,
  `DISPLAY_NAME` varchar(60) DEFAULT NULL,
  `CONTENT` longtext,
  `GROUP_NAME` varchar(60) DEFAULT NULL,
  `DESCRIPTION` varchar(500) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `IS_LOG_ENABLED` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`TABLE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coeus_module` (
  `MODULE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) NOT NULL,
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`MODULE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coeus_sub_module` (
  `COEUS_SUB_MODULE_ID` int NOT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `SUB_MODULE_CODE` int DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` date DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `REQUIRE_UNIQUE_QUESTIONNAIRE` varchar(1) DEFAULT 'N',
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`COEUS_SUB_MODULE_ID`),
  KEY `COEUS_SUB_MODULE_FK1` (`MODULE_CODE`),
  CONSTRAINT `COEUS_SUB_MODULE_FK1` FOREIGN KEY (`MODULE_CODE`) REFERENCES `coeus_module` (`MODULE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_assignee_type` (
  `ASSIGNEE_TYPE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ASSIGNEE_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_conflict_history` (
  `COI_CONFLICT_HISTORY_ID` int NOT NULL AUTO_INCREMENT,
  `DISCLOSURE_DETAILS_ID` int NOT NULL,
  `COMMENT` varchar(2000) DEFAULT NULL,
  `DISC_DET_STATUS_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`COI_CONFLICT_HISTORY_ID`),
  KEY `COI_CONFLICT_HISTORY_FK1` (`DISC_DET_STATUS_CODE`),
  CONSTRAINT `COI_CONFLICT_HISTORY_FK1` FOREIGN KEY (`DISC_DET_STATUS_CODE`) REFERENCES `coi_disc_det_status` (`DISC_DET_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_conflict_status_type` (
  `CONFLICT_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`CONFLICT_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_discl_ent_proj_details` (
  `DISCLOSURE_DETAILS_ID` int NOT NULL AUTO_INCREMENT,
  `DISCLOSURE_ID` int DEFAULT NULL,
  `DISCLOSURE_NUMBER` varchar(45) DEFAULT NULL,
  `PERSON_ENTITY_ID` int DEFAULT NULL,
  `ENTITY_ID` int DEFAULT NULL,
  `ENTITY_NUMBER` int DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(20) DEFAULT NULL,
  `PROJECT_CONFLICT_STATUS_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`DISCLOSURE_DETAILS_ID`),
  KEY `COI_DISCL_ENT_PROJ_DETAILS_FK1_idx` (`DISCLOSURE_ID`),
  KEY `COI_DISCL_ENT_PROJ_DETAILS_FK2_idx` (`PERSON_ENTITY_ID`),
  KEY `COI_DISCL_ENT_PROJ_DETAILS_FK3_idx` (`ENTITY_ID`),
  KEY `COI_DISCL_ENT_PROJ_DETAILS_FK4_idx` (`PROJECT_CONFLICT_STATUS_CODE`),
  CONSTRAINT `COI_DISCL_ENT_PROJ_DETAILS_FK1` FOREIGN KEY (`DISCLOSURE_ID`) REFERENCES `coi_disclosure` (`DISCLOSURE_ID`),
  CONSTRAINT `COI_DISCL_ENT_PROJ_DETAILS_FK2` FOREIGN KEY (`PERSON_ENTITY_ID`) REFERENCES `person_entity` (`PERSON_ENTITY_ID`),
  CONSTRAINT `COI_DISCL_ENT_PROJ_DETAILS_FK3` FOREIGN KEY (`ENTITY_ID`) REFERENCES `entity` (`ENTITY_ID`),
  CONSTRAINT `COI_DISCL_ENT_PROJ_DETAILS_FK4` FOREIGN KEY (`PROJECT_CONFLICT_STATUS_CODE`) REFERENCES `coi_proj_conflict_status_type` (`PROJECT_CONFLICT_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_disclosure` (
  `DISCLOSURE_ID` int NOT NULL AUTO_INCREMENT,
  `PERSON_ID` varchar(45) DEFAULT NULL,
  `DISCLOSURE_NUMBER` int DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `VERSION_STATUS` varchar(45) DEFAULT NULL,
  `FCOI_TYPE_CODE` varchar(3) DEFAULT NULL,
  `CONFLICT_STATUS_CODE` varchar(3) DEFAULT NULL,
  `DISPOSITION_STATUS_CODE` varchar(3) DEFAULT NULL,
  `REVIEW_STATUS_CODE` varchar(3) DEFAULT NULL,
  `RISK_CATEGORY_CODE` varchar(3) DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(20) DEFAULT NULL,
  `EXPIRATION_DATE` date DEFAULT NULL,
  `CERTIFICATION_TEXT` varchar(1000) DEFAULT NULL,
  `CERTIFIED_BY` varchar(60) DEFAULT NULL,
  `CERTIFIED_AT` datetime DEFAULT NULL,
  `REVISION_COMMENT` varchar(1000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`DISCLOSURE_ID`),
  KEY `COI_DISCLOSURE1_FK1_idx` (`PERSON_ID`),
  KEY `COI_DISCLOSURE1_FK2_idx` (`FCOI_TYPE_CODE`),
  KEY `COI_DISCLOSURE1_FK3_idx` (`CONFLICT_STATUS_CODE`),
  KEY `COI_DISCLOSURE1_FK4_idx` (`DISPOSITION_STATUS_CODE`),
  KEY `COI_DISCLOSURE1_FK5_idx` (`REVIEW_STATUS_CODE`),
  KEY `COI_DISCLOSURE1_FK6_idx` (`RISK_CATEGORY_CODE`),
  CONSTRAINT `COI_DISCLOSURE1_FK1` FOREIGN KEY (`PERSON_ID`) REFERENCES `person` (`PERSON_ID`),
  CONSTRAINT `COI_DISCLOSURE1_FK2` FOREIGN KEY (`FCOI_TYPE_CODE`) REFERENCES `coi_disclosure_fcoi_type` (`FCOI_TYPE_CODE`),
  CONSTRAINT `COI_DISCLOSURE1_FK3` FOREIGN KEY (`CONFLICT_STATUS_CODE`) REFERENCES `coi_conflict_status_type` (`CONFLICT_STATUS_CODE`),
  CONSTRAINT `COI_DISCLOSURE1_FK4` FOREIGN KEY (`DISPOSITION_STATUS_CODE`) REFERENCES `coi_disposition_status_type` (`DISPOSITION_STATUS_CODE`),
  CONSTRAINT `COI_DISCLOSURE1_FK5` FOREIGN KEY (`REVIEW_STATUS_CODE`) REFERENCES `coi_review_status_type` (`REVIEW_STATUS_CODE`),
  CONSTRAINT `COI_DISCLOSURE1_FK6` FOREIGN KEY (`RISK_CATEGORY_CODE`) REFERENCES `coi_risk_category` (`RISK_CATEGORY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_disclosure_fcoi_type` (
  `FCOI_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`FCOI_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_disclosure_type` (
  `DISCLOSURE_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`DISCLOSURE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_disposition_status_type` (
  `DISPOSITION_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`DISPOSITION_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_proj_conflict_status_type` (
  `PROJECT_CONFLICT_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`PROJECT_CONFLICT_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_project_award` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `EXTERNAL_SYSTEM_REF_ID` int DEFAULT NULL,
  `COI_PROJECT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `AWARD_NUMBER` varchar(45) DEFAULT NULL,
  `TITLE` varchar(2000) DEFAULT NULL,
  `PI_PERSON_ID` varchar(45) DEFAULT NULL,
  `PI_NAME` varchar(60) DEFAULT NULL,
  `SPONSOR_NAME` varchar(60) DEFAULT NULL,
  `PRIME_SPONSOR_NAME` varchar(60) DEFAULT NULL,
  `AWARD_START_DATE` date DEFAULT NULL,
  `AWARD_END_DATE` date DEFAULT NULL,
  `LEAD_UNIT_NAME` varchar(45) DEFAULT NULL,
  `FED_AT` datetime DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `COI_PROJECT_AWARD_FK1_idx` (`COI_PROJECT_TYPE_CODE`),
  CONSTRAINT `COI_PROJECT_AWARD_FK1` FOREIGN KEY (`COI_PROJECT_TYPE_CODE`) REFERENCES `coi_project_type` (`COI_PROJECT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_project_proposal` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `EXTERNAL_SYSTEM_REF_ID` int DEFAULT NULL,
  `COI_PROJECT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `PROPOSAL_NUMBER` varchar(45) DEFAULT NULL,
  `TITLE` varchar(2000) DEFAULT NULL,
  `PI_PERSON_ID` varchar(45) DEFAULT NULL,
  `PI_NAME` varchar(60) DEFAULT NULL,
  `SPONSOR_NAME` varchar(60) DEFAULT NULL,
  `PRIME_SPONSOR_NAME` varchar(60) DEFAULT NULL,
  `PROPOSAL_START_DATE` date DEFAULT NULL,
  `PROPOSAL_END_DATE` date DEFAULT NULL,
  `LEAD_UNIT_NAME` varchar(45) DEFAULT NULL,
  `FED_AT` datetime DEFAULT NULL,
  `coi_project_proposalcol` varchar(45) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `COI_PROJECT_PROPOSAL_FK1_idx` (`COI_PROJECT_TYPE_CODE`),
  CONSTRAINT `COI_PROJECT_PROPOSAL_FK1` FOREIGN KEY (`COI_PROJECT_TYPE_CODE`) REFERENCES `coi_project_type` (`COI_PROJECT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_project_type` (
  `COI_PROJECT_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`COI_PROJECT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_review` (
  `COI_REVIEW_ID` int NOT NULL AUTO_INCREMENT,
  `ASSIGNEE_PERSON_ID` varchar(40) DEFAULT NULL,
  `DISCLOSURE_ID` int DEFAULT NULL,
  `ADMIN_GROUP_ID` int DEFAULT NULL,
  `REVIEW_STATUS_TYPE_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`COI_REVIEW_ID`),
  KEY `COI_REVIEW_FK1` (`ASSIGNEE_PERSON_ID`),
  KEY `COI_REVIEW_FK2` (`DISCLOSURE_ID`),
  KEY `COI_REVIEW_FK3` (`ADMIN_GROUP_ID`),
  KEY `COI_REVIEW_FK4_idx` (`REVIEW_STATUS_TYPE_CODE`),
  CONSTRAINT `COI_REVIEW_FK1` FOREIGN KEY (`ASSIGNEE_PERSON_ID`) REFERENCES `person` (`PERSON_ID`),
  CONSTRAINT `COI_REVIEW_FK2` FOREIGN KEY (`DISCLOSURE_ID`) REFERENCES `coi_disclosure` (`DISCLOSURE_ID`),
  CONSTRAINT `COI_REVIEW_FK3` FOREIGN KEY (`ADMIN_GROUP_ID`) REFERENCES `admin_group` (`ADMIN_GROUP_ID`),
  CONSTRAINT `COI_REVIEW_FK4` FOREIGN KEY (`REVIEW_STATUS_TYPE_CODE`) REFERENCES `coi_review_status_type` (`REVIEW_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_review_activity` (
  `COI_REVIEW_ACTIVITY_ID` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(2000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`COI_REVIEW_ACTIVITY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_review_assignee_history` (
  `COI_REVIEW_ASSIGNEE_HISTORY_ID` int NOT NULL AUTO_INCREMENT,
  `COI_REVIEW_ID` int NOT NULL,
  `ASSIGNEE_TYPE` varchar(1) NOT NULL,
  `ASSIGNEE_PERSON_ID` varchar(40) DEFAULT NULL,
  `ADMIN_GROUP_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `COI_REVIEW_ACTIVITY_ID` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`COI_REVIEW_ASSIGNEE_HISTORY_ID`),
  KEY `COI_REVIEW_ASSIGNEE_HST_FK1` (`COI_REVIEW_ID`),
  CONSTRAINT `COI_REVIEW_ASSIGNEE_HST_FK1` FOREIGN KEY (`COI_REVIEW_ID`) REFERENCES `coi_review` (`COI_REVIEW_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_review_comment` (
  `COI_REVIEW_COMMENT_ID` int NOT NULL AUTO_INCREMENT,
  `COI_REVIEW_ID` int DEFAULT NULL,
  `COI_SECTIONS_TYPE_CODE` varchar(3) DEFAULT NULL,
  `COMMENT` varchar(2000) DEFAULT NULL,
  `COI_REVIEW_ACTIVITY_ID` varchar(3) DEFAULT NULL,
  `COI_PARENT_COMMENT_ID` int DEFAULT NULL,
  `COI_SUB_SECTIONS_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `COMMENTED_BY_PERSON_ID` varchar(40) DEFAULT NULL,
  `IS_PRIVATE` varchar(1) DEFAULT NULL,
  `DISCLOSURE_ID` int DEFAULT NULL,
  PRIMARY KEY (`COI_REVIEW_COMMENT_ID`),
  KEY `COI_REVIEW_COMMENT_FK1` (`COI_REVIEW_ID`),
  KEY `COI_REVIEW_COMMENT_FK2` (`COI_SECTIONS_TYPE_CODE`),
  KEY `COI_REVIEW_COMMENT_FK5` (`COI_REVIEW_ACTIVITY_ID`),
  KEY `COI_REVIEW_COMMENT_FK6` (`DISCLOSURE_ID`),
  CONSTRAINT `COI_REVIEW_COMMENT_FK1` FOREIGN KEY (`COI_REVIEW_ID`) REFERENCES `coi_review` (`COI_REVIEW_ID`),
  CONSTRAINT `COI_REVIEW_COMMENT_FK2` FOREIGN KEY (`COI_SECTIONS_TYPE_CODE`) REFERENCES `coi_sections_type` (`COI_SECTIONS_TYPE_CODE`),
  CONSTRAINT `COI_REVIEW_COMMENT_FK5` FOREIGN KEY (`COI_REVIEW_ACTIVITY_ID`) REFERENCES `coi_review_activity` (`COI_REVIEW_ACTIVITY_ID`),
  CONSTRAINT `COI_REVIEW_COMMENT_FK6` FOREIGN KEY (`DISCLOSURE_ID`) REFERENCES `coi_disclosure` (`DISCLOSURE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_review_comment_attachment` (
  `COI_REVIEW_COMMENT_ATT_ID` int NOT NULL AUTO_INCREMENT,
  `COI_REVIEW_COMMENT_ID` int NOT NULL,
  `COI_REVIEW_ID` int DEFAULT NULL,
  `FILE_NAME` varchar(255) DEFAULT NULL,
  `MIME_TYPE` varchar(255) DEFAULT NULL,
  `FILE_DATA_ID` varchar(40) NOT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`COI_REVIEW_COMMENT_ATT_ID`),
  KEY `COI_REVIEW_COMMENT_ATTACHMENT_FK1` (`COI_REVIEW_ID`),
  KEY `COI_REVIEW_COMMENT_ATTACHMENT_FK2` (`COI_REVIEW_COMMENT_ID`),
  KEY `COI_REVIEW_COMMENT_ATTACHMENT_FK3` (`FILE_DATA_ID`),
  CONSTRAINT `COI_REVIEW_COMMENT_ATTACHMENT_FK1` FOREIGN KEY (`COI_REVIEW_ID`) REFERENCES `coi_review` (`COI_REVIEW_ID`),
  CONSTRAINT `COI_REVIEW_COMMENT_ATTACHMENT_FK2` FOREIGN KEY (`COI_REVIEW_COMMENT_ID`) REFERENCES `coi_review_comment` (`COI_REVIEW_COMMENT_ID`),
  CONSTRAINT `COI_REVIEW_COMMENT_ATTACHMENT_FK3` FOREIGN KEY (`FILE_DATA_ID`) REFERENCES `coi_file_data` (`FILE_DATA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_review_comment_tags` (
  `COI_REVIEW_COMMENT_TAGS_ID` int NOT NULL AUTO_INCREMENT,
  `COI_REVIEW_COMMENT_ID` int DEFAULT NULL,
  `TAG_REF` varchar(20) DEFAULT NULL,
  `TAGGED_PERSON_ID` varchar(40) DEFAULT NULL,
  `TAGGED_GROUP_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `COI_REVIEW_ID` int DEFAULT NULL,
  PRIMARY KEY (`COI_REVIEW_COMMENT_TAGS_ID`),
  KEY `COI_REVIEW_CMT_TAGS_FK2` (`COI_REVIEW_COMMENT_ID`),
  KEY `COI_REVIEW_CMT_TAGS_FK1` (`COI_REVIEW_ID`),
  CONSTRAINT `COI_REVIEW_CMT_TAGS_FK1` FOREIGN KEY (`COI_REVIEW_ID`) REFERENCES `coi_review` (`COI_REVIEW_ID`),
  CONSTRAINT `COI_REVIEW_CMT_TAGS_FK2` FOREIGN KEY (`COI_REVIEW_COMMENT_ID`) REFERENCES `coi_review_comment` (`COI_REVIEW_COMMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_review_status_history` (
  `COI_REVIEW_STATUS_HISTORY_ID` int NOT NULL AUTO_INCREMENT,
  `COI_REVIEW_ID` int NOT NULL,
  `COI_REVIEW_STATUS_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`COI_REVIEW_STATUS_HISTORY_ID`),
  KEY `COI_REVIEW_STATUS_HISTORY_FK1` (`COI_REVIEW_ID`),
  CONSTRAINT `COI_REVIEW_STATUS_HISTORY_FK1` FOREIGN KEY (`COI_REVIEW_ID`) REFERENCES `coi_review` (`COI_REVIEW_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_review_status_type` (
  `REVIEW_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`REVIEW_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_risk_category` (
  `RISK_CATEGORY_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`RISK_CATEGORY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_risk_category_type` (
  `RISK_CATEGORY_TYPE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`RISK_CATEGORY_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_sections_type` (
  `COI_SECTIONS_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`COI_SECTIONS_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_travel_disclosure` (
  `TRAVEL_DISCLOSURE_ID` int NOT NULL AUTO_INCREMENT,
  `TRAVEL_NUMBER` int DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `VERSION_STATUS` varchar(45) DEFAULT NULL,
  `PERSON_ENTITY_ID` int DEFAULT NULL,
  `ENTITY_ID` int DEFAULT NULL,
  `ENTITY_NUMBER` int DEFAULT NULL,
  `PERSON_ID` varchar(45) DEFAULT NULL,
  `TRAVEL_STATUS_CODE` varchar(3) DEFAULT NULL,
  `IS_SPONSORED_TRAVEL` varchar(1) DEFAULT NULL,
  `TRAVEL_TITLE` varchar(200) DEFAULT NULL,
  `PURPOSE_OF_THE_TRIP` varchar(2000) DEFAULT NULL,
  `TRAVEL_AMOUNT` decimal(12,2) DEFAULT NULL,
  `TRAVEL_START_DATE` date DEFAULT NULL,
  `NO_OF_DAYS` int DEFAULT NULL,
  `DESTINATION_CITY` varchar(90) DEFAULT NULL,
  `DESTINATION_COUNTRY` varchar(45) DEFAULT NULL,
  `RELATIONSHIP_TO_YOUR_RESEARCH` varchar(60) DEFAULT NULL,
  `ACKNOWLEDGE_BY` varchar(60) DEFAULT NULL,
  `ACKNOWLEDGE_AT` varchar(45) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `TRAVEL_END_DATE` date DEFAULT NULL,
  `STATE` varchar(60) DEFAULT NULL,
  `IS_INTERNATIONAL_TRAVEL` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`TRAVEL_DISCLOSURE_ID`),
  KEY `COI_TRAVEL_DISCLOSURE_FK1_idx` (`PERSON_ENTITY_ID`),
  KEY `COI_TRAVEL_DISCLOSURE_FK2_idx` (`ENTITY_ID`),
  KEY `COI_TRAVEL_DISCLOSURE_FK3_idx` (`PERSON_ID`),
  KEY `COI_TRAVEL_DISCLOSURE_FK4_idx` (`TRAVEL_STATUS_CODE`),
  CONSTRAINT `COI_TRAVEL_DISCLOSURE_FK1` FOREIGN KEY (`PERSON_ENTITY_ID`) REFERENCES `person_entity` (`PERSON_ENTITY_ID`),
  CONSTRAINT `COI_TRAVEL_DISCLOSURE_FK2` FOREIGN KEY (`ENTITY_ID`) REFERENCES `entity` (`ENTITY_ID`),
  CONSTRAINT `COI_TRAVEL_DISCLOSURE_FK3` FOREIGN KEY (`PERSON_ID`) REFERENCES `person` (`PERSON_ID`),
  CONSTRAINT `COI_TRAVEL_DISCLOSURE_FK4` FOREIGN KEY (`TRAVEL_STATUS_CODE`) REFERENCES `coi_travel_status_type` (`TRAVEL_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_travel_disclosure_traveler` (
  `TRAVEL_TRAVELER_ID` int NOT NULL AUTO_INCREMENT,
  `TRAVEL_DISCLOSURE_ID` int DEFAULT NULL,
  `TRAVELER_TYPE_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`TRAVEL_TRAVELER_ID`),
  KEY `COI_TRAVEL_DISCLOSURE_TRAVELER_FK1_idx` (`TRAVEL_DISCLOSURE_ID`),
  KEY `COI_TRAVEL_DISCLOSURE_TRAVELER_FK2_idx` (`TRAVELER_TYPE_CODE`),
  CONSTRAINT `COI_TRAVEL_DISCLOSURE_TRAVELER_FK1` FOREIGN KEY (`TRAVEL_DISCLOSURE_ID`) REFERENCES `coi_travel_disclosure` (`TRAVEL_DISCLOSURE_ID`),
  CONSTRAINT `COI_TRAVEL_DISCLOSURE_TRAVELER_FK2` FOREIGN KEY (`TRAVELER_TYPE_CODE`) REFERENCES `coi_traveler_type` (`TRAVELER_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_travel_status_type` (
  `TRAVEL_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`TRAVEL_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_traveler_type` (
  `TRAVELER_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`TRAVELER_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `coi_with_person` (
  `COI_WITH_PERSON_ID` int NOT NULL AUTO_INCREMENT,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `EXTERNAL_REVIEWER_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`COI_WITH_PERSON_ID`),
  KEY `COI_WITH_PERSON_MAPPING_FK1` (`EXTERNAL_REVIEWER_ID`),
  KEY `COI_WITH_PERSON_MAPPING_FK2` (`PERSON_ID`),
  CONSTRAINT `COI_WITH_PERSON_MAPPING_FK1` FOREIGN KEY (`EXTERNAL_REVIEWER_ID`) REFERENCES `external_reviewer` (`EXTERNAL_REVIEWER_ID`),
  CONSTRAINT `COI_WITH_PERSON_MAPPING_FK2` FOREIGN KEY (`PERSON_ID`) REFERENCES `person` (`PERSON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `column_lookup` (
  `COLUMN_ID` int NOT NULL,
  `COLUMN_NAME` varchar(250) DEFAULT NULL,
  `FILTER_TYPE` varchar(250) DEFAULT NULL,
  `VALUE_FIELD` varchar(300) DEFAULT NULL,
  `INDEX_FIELD` varchar(200) DEFAULT NULL,
  `QUERY_TYPE` varchar(100) DEFAULT NULL,
  `INNER_JOIN` varchar(1) DEFAULT NULL,
  `SUMMABLE` varchar(1) DEFAULT NULL,
  `IS_CURRENCY` varchar(1) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`COLUMN_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `comment_type` (
  `COMMENT_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `MODULE_CODE` int DEFAULT NULL,
  PRIMARY KEY (`COMMENT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `concur_staff_travel_dtls` (
  `PAYROLL_ID` int NOT NULL AUTO_INCREMENT,
  `CONCUR_REFERENCE_NUMBER` varchar(20) DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `DESTINATION_CITY` varchar(100) DEFAULT NULL,
  `DESTINATION_COUNTRY` varchar(100) DEFAULT NULL,
  `PARENT_EXPENSE_TYPE` varchar(60) DEFAULT NULL,
  `PURPOSE_OF_TRIP` varchar(100) DEFAULT NULL,
  `BUSINESS_PURPOSE` varchar(100) DEFAULT NULL,
  `COMMENT` varchar(4000) DEFAULT NULL,
  `VISIT_START_DATE` datetime DEFAULT NULL,
  `VISIT_END_DATE` datetime DEFAULT NULL,
  `EVENT_START_DATE` datetime DEFAULT NULL,
  `EVENT_END_DATE` datetime DEFAULT NULL,
  `TOTAL_COLA` decimal(12,2) DEFAULT NULL,
  `DURATION` varchar(20) DEFAULT NULL,
  `FILE_ID` int DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `REMARKS` varchar(100) DEFAULT NULL,
  `CONCUR_STAFF_TRAVEL_ID` int NOT NULL,
  `DESTINATION_COUNTRY_CODE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PAYROLL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `congressional_district` (
  `CONG_DISTRICT_CODE` int NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`CONG_DISTRICT_CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `contact_role_type` (
  `CONTACT_ROLE_CODE` varchar(10) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`CONTACT_ROLE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `cost_element` (
  `COST_ELEMENT` varchar(50) NOT NULL,
  `ACTIVE_FLAG` varchar(1) DEFAULT NULL,
  `BUDGET_CATEGORY_CODE` varchar(3) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `FIN_OBJECT_CODE` varchar(3) DEFAULT NULL,
  `ON_OFF_CAMPUS_FLAG` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `TBN_ID` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`COST_ELEMENT`),
  KEY `FKm9luclpm3yifg48nyqoki7ybu` (`BUDGET_CATEGORY_CODE`),
  KEY `COST_ELEMENT_FK2` (`TBN_ID`),
  CONSTRAINT `COST_ELEMENT_FK1` FOREIGN KEY (`BUDGET_CATEGORY_CODE`) REFERENCES `budget_category` (`BUDGET_CATEGORY_CODE`),
  CONSTRAINT `COST_ELEMENT_FK2` FOREIGN KEY (`TBN_ID`) REFERENCES `tbn` (`TBN_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `cost_element_job_profile_mapping` (
  `COST_JOB_MAPPING_ID` varchar(10) NOT NULL,
  `COST_ELEMENT` varchar(50) DEFAULT NULL,
  `JOB_PROFILE_CODE` varchar(30) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`COST_JOB_MAPPING_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `cost_element_rate` (
  `COST_ELEMENT_RATE_ID` int NOT NULL,
  `COST_ELEMENT` varchar(255) DEFAULT NULL,
  `RATE` decimal(12,2) DEFAULT NULL,
  `RATE_TYPE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`COST_ELEMENT_RATE_ID`),
  KEY `COST_ELEMENT_RATE_FK1` (`COST_ELEMENT`),
  CONSTRAINT `COST_ELEMENT_RATE_FK1` FOREIGN KEY (`COST_ELEMENT`) REFERENCES `cost_element` (`COST_ELEMENT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `cost_share_type` (
  `COST_SHARE_TYPE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `CAN_INCLUDE_IN_BUDGET` varchar(1) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`COST_SHARE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `cost_sharing_type` (
  `COST_SHARE_TYPE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`COST_SHARE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `country` (
  `COUNTRY_CODE` varchar(3) NOT NULL,
  `COUNTRY_NAME` varchar(100) DEFAULT NULL,
  `CURRENCY_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  `COUNTRY_CODE_ISO2` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`COUNTRY_CODE`),
  KEY `COUNTRY_FK1` (`CURRENCY_CODE`),
  CONSTRAINT `COUNTRY_FK1` FOREIGN KEY (`CURRENCY_CODE`) REFERENCES `currency` (`CURRENCY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `cp_report_header` (
  `CP_REPORT_HEADER_ID` int NOT NULL AUTO_INCREMENT,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `NON_EMPLOYEE_FLAG` varchar(1) DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `MODULE_ITEM_ID` varchar(20) DEFAULT NULL,
  `OVERLAP` longtext,
  `FOOTNOTE` longtext,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`CP_REPORT_HEADER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `cp_report_project_details` (
  `CP_REPORT_PROJECT_DETAIL_ID` int NOT NULL AUTO_INCREMENT,
  `CP_REPORT_HEADER_ID` int DEFAULT NULL,
  `LINKED_MODULE_CODE` int DEFAULT NULL,
  `LINKED_MODULE_ITEM_ID` varchar(20) DEFAULT NULL,
  `IS_EXCLUDED` varchar(1) DEFAULT NULL,
  `IS_MANUALLY_ADDED` varchar(1) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `PROJECT_TITLE` varchar(500) DEFAULT NULL,
  `GRANT_CALL_NAME` varchar(500) DEFAULT NULL,
  `PROP_PERSON_ROLE_ID` int DEFAULT NULL,
  `AMOUNT` decimal(12,2) DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `SPONSOR_TYPE_CODE` varchar(20) DEFAULT NULL,
  `SPONSOR` varchar(500) DEFAULT NULL,
  `FUNDING_STATUS_CODE` int DEFAULT NULL,
  `SPONSOR_CODE` varchar(20) DEFAULT NULL,
  `CURRENCY_CODE` varchar(20) DEFAULT NULL,
  `PERCENTAGE_OF_EFFORT` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`CP_REPORT_PROJECT_DETAIL_ID`),
  KEY `CP_REPORT_PROJECT_DETAILS_FK` (`CP_REPORT_HEADER_ID`),
  CONSTRAINT `CP_REPORT_PROJECT_DETAILS_FK` FOREIGN KEY (`CP_REPORT_HEADER_ID`) REFERENCES `cp_report_header` (`CP_REPORT_HEADER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `cp_report_project_details_ext` (
  `CP_REPORT_PROJECT_DETAIL_ID` int NOT NULL,
  `ACADEMIC_MONTHS` varchar(5) DEFAULT NULL,
  `CALENDAR_MONTHS` varchar(5) DEFAULT NULL,
  `SUMMER_MONTHS` varchar(5) DEFAULT NULL,
  `TOTAL_AWARD_AMOUNT` decimal(12,2) DEFAULT NULL,
  `ANNUAL_DIRECT_COST` decimal(12,2) DEFAULT NULL,
  `LEAD_PI_NON_EMPLOYEE_FLAG` varchar(1) DEFAULT NULL,
  `LEAD_PI_PERSON_ID` varchar(40) DEFAULT NULL,
  `LOCATION` varchar(1000) DEFAULT NULL,
  `PERSON_ROLE_ID` int DEFAULT NULL,
  `PERCENTAGE_OF_EFFORT` decimal(5,2) DEFAULT NULL,
  `PRIME_AWARD` varchar(12) DEFAULT NULL,
  `ACADEMIC_CONTACT_PERSON_ID` varchar(40) DEFAULT NULL,
  `TECHNICAL_CONTACT_PERSON_ID` varchar(40) DEFAULT NULL,
  `PROJECT_REL_TO_PROPOSED_EFFORT` decimal(5,2) DEFAULT NULL,
  `PROJECT_GOALS` varchar(4000) DEFAULT NULL,
  `SPECIFIC_AIMS` varchar(4000) DEFAULT NULL,
  `PROJECT_SUMMARY` varchar(4000) DEFAULT NULL,
  `COMMENTS` varchar(2000) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`CP_REPORT_PROJECT_DETAIL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `currency` (
  `CURRENCY_CODE` varchar(3) NOT NULL,
  `CURRENCY` varchar(40) DEFAULT NULL,
  `CURRENCY_SYMBOL` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`CURRENCY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `currency_format` (
  `CURRENCY_CODE` varchar(3) NOT NULL,
  `COUNTRY_CODE` varchar(3) NOT NULL,
  `LANGUAGE` varchar(30) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`CURRENCY_CODE`,`COUNTRY_CODE`),
  CONSTRAINT `CURRENCY_FORMAT_FK1` FOREIGN KEY (`CURRENCY_CODE`) REFERENCES `currency` (`CURRENCY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `currency_rate` (
  `COUNTRY_CODE` varchar(3) NOT NULL,
  `CURRENCY_RATE` float NOT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`COUNTRY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `custom_data` (
  `CUSTOM_DATA_ID` int NOT NULL AUTO_INCREMENT,
  `CUSTOM_DATA_ELEMENTS_ID` int DEFAULT NULL,
  `MODULE_ITEM_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(255) DEFAULT NULL,
  `MODULE_SUB_ITEM_CODE` int DEFAULT NULL,
  `MODULE_SUB_ITEM_KEY` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `VALUE` varchar(4000) DEFAULT NULL,
  `COLUMN_ID` int DEFAULT NULL,
  `COLUMN_VERSION_NUMBER` int DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CUSTOM_DATA_ID`),
  KEY `custom_data_idx_2` (`MODULE_ITEM_KEY`),
  KEY `custom_data_idx_3` (`CUSTOM_DATA_ELEMENTS_ID`),
  KEY `custom_data_idx_4` (`MODULE_ITEM_KEY`,`CUSTOM_DATA_ELEMENTS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `custom_data_element_usage` (
  `CUSTOM_DATA_ELEMENT_USAGE_ID` int NOT NULL,
  `IS_REQUIRED` varchar(255) DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `CUSTOM_DATA_ELEMENTS_ID` int DEFAULT NULL,
  `ORDER_NUMBER` int DEFAULT NULL,
  PRIMARY KEY (`CUSTOM_DATA_ELEMENT_USAGE_ID`),
  KEY `CUSTOM_DATA_ELEMENT_USAGE_FK1` (`CUSTOM_DATA_ELEMENTS_ID`),
  KEY `CUSTOM_DATA_ELEMENT_USAGE_FK3` (`MODULE_CODE`),
  CONSTRAINT `CUSTOM_DATA_ELEMENT_USAGE_FK1` FOREIGN KEY (`CUSTOM_DATA_ELEMENTS_ID`) REFERENCES `custom_data_elements` (`CUSTOM_DATA_ELEMENTS_ID`),
  CONSTRAINT `CUSTOM_DATA_ELEMENT_USAGE_FK2` FOREIGN KEY (`MODULE_CODE`) REFERENCES `coeus_module` (`MODULE_CODE`),
  CONSTRAINT `CUSTOM_DATA_ELEMENT_USAGE_FK3` FOREIGN KEY (`MODULE_CODE`) REFERENCES `coeus_module` (`MODULE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `custom_data_elements` (
  `CUSTOM_DATA_ELEMENTS_ID` int NOT NULL,
  `COLUMN_LABEL` varchar(255) DEFAULT NULL,
  `DATA_LENGTH` int DEFAULT NULL,
  `DATA_TYPE` varchar(255) DEFAULT NULL,
  `DEFAULT_VALUE` varchar(255) DEFAULT NULL,
  `HAS_LOOKUP` varchar(255) DEFAULT NULL,
  `LOOKUP_ARGUMENT` varchar(255) DEFAULT NULL,
  `LOOKUP_WINDOW` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `COLUMN_ID` int DEFAULT NULL,
  `COLUMN_VERSION_NUMBER` int DEFAULT NULL,
  `IS_LATEST_VERSION` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'N',
  `custom_element_name` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`CUSTOM_DATA_ELEMENTS_ID`),
  KEY `CUSTOM_DATA_ELEMENTS_FK1` (`DATA_TYPE`),
  CONSTRAINT `CUSTOM_DATA_ELEMENTS_FK1` FOREIGN KEY (`DATA_TYPE`) REFERENCES `custom_data_elements_data_type` (`DATA_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `custom_data_elements_data_type` (
  `DATA_TYPE_CODE` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`DATA_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `custom_data_elements_options` (
  `CUSTOM_DATA_OPTION_ID` int NOT NULL,
  `CUSTOM_DATA_ELEMENTS_ID` int DEFAULT NULL,
  `OPTION_NAME` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` tinyblob,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CUSTOM_DATA_OPTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `custom_data_report_rt` (
  `MODULE_ITEM_KEY` decimal(22,0) NOT NULL,
  `STEM_NONSTEM` longtext,
  `RIE_DOMAIN` longtext,
  `INPUT_GST_CATEGORY` longtext,
  `OUTPUT_GST_CATEGORY` longtext,
  `GRANT_CODE` longtext,
  `SUB_LEAD_UNIT` longtext,
  `PROFIT_CENTER` longtext,
  `FUND_CENTER` longtext,
  `COST_CENTER` longtext,
  `DISPLAY_AT_ACAD_PROFILE` longtext,
  `LEVEL_2_SUP_ORG` longtext,
  `LAST_SYNC_TIME` datetime DEFAULT NULL,
  `MULTIPLIER` longtext,
  `CLAIM_PREPARER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MODULE_ITEM_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `degree_type` (
  `DEGREE_CODE` varchar(10) NOT NULL,
  `DESCRIPTION` varchar(100) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` date DEFAULT NULL,
  PRIMARY KEY (`DEGREE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `delegations` (
  `DELEGATION_ID` int NOT NULL AUTO_INCREMENT,
  `DELEGATED_BY` varchar(40) DEFAULT NULL,
  `DELEGATED_TO` varchar(40) DEFAULT NULL,
  `EFFECTIVE_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `DELEGATION_STATUS_CODE` varchar(1) DEFAULT NULL,
  `COMMENT` varchar(100) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`DELEGATION_ID`),
  KEY `DELEGATIONS_FK1` (`DELEGATED_BY`),
  KEY `DELEGATIONS_FK2` (`DELEGATED_TO`),
  KEY `DELEGATIONS_FK3` (`DELEGATION_STATUS_CODE`),
  CONSTRAINT `DELEGATIONS_FK1` FOREIGN KEY (`DELEGATED_BY`) REFERENCES `person` (`PERSON_ID`),
  CONSTRAINT `DELEGATIONS_FK2` FOREIGN KEY (`DELEGATED_TO`) REFERENCES `person` (`PERSON_ID`),
  CONSTRAINT `DELEGATIONS_FK3` FOREIGN KEY (`DELEGATION_STATUS_CODE`) REFERENCES `delegations_status` (`DELEGATION_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `delegations_status` (
  `DELEGATION_STATUS_CODE` varchar(1) NOT NULL,
  `DESCRIPTION` varchar(15) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`DELEGATION_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `delete_workday_resource` (
  `DELETE_WORKDAY_RESOURCE_ID` int NOT NULL AUTO_INCREMENT,
  `WBS_NUMBER` varchar(50) DEFAULT NULL,
  `COST_ALLOCATION` decimal(5,2) DEFAULT NULL,
  `CHARGE_START_DATE` datetime DEFAULT NULL,
  `CHARGE_END_DATE` datetime DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`DELETE_WORKDAY_RESOURCE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `discl_atta_status` (
  `ATTA_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ATTA_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `discl_atta_type` (
  `ATTA_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ATTA_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `discl_attachment` (
  `ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `COMMENT_ID` int DEFAULT NULL,
  `COMPONENT_TYPE_CODE` varchar(1) DEFAULT NULL,
  `COMPONENT_REFERENCE_ID` int DEFAULT NULL,
  `COMPONENT_REFERENCE_NUMBER` varchar(45) DEFAULT NULL,
  `ATTACHMENT_NUMBER` int DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `VERSION_STATUS` varchar(45) DEFAULT NULL,
  `ATTA_TYPE_CODE` varchar(1) DEFAULT NULL,
  `ATTA_STATUS_CODE` varchar(1) DEFAULT NULL,
  `DOCUMENT_OWNER_PERSON_ID` varchar(45) DEFAULT NULL,
  `FILE_NAME` varchar(45) DEFAULT NULL,
  `MIME_TYPE` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `FILE_DATA_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ATTACHMENT_ID`),
  KEY `DISCL_ATTACHMENT_FK1_idx` (`COMMENT_ID`),
  KEY `DISCL_ATTACHMENT_FK2_idx` (`COMPONENT_TYPE_CODE`),
  KEY `DISCL_ATTACHMENT_FK3_idx` (`ATTA_TYPE_CODE`),
  KEY `DISCL_ATTACHMENT_FK4_idx` (`ATTA_STATUS_CODE`),
  KEY `DISCL_ATTACHMENT_FK5_idx` (`FILE_DATA_ID`),
  CONSTRAINT `DISCL_ATTACHMENT_FK1` FOREIGN KEY (`COMMENT_ID`) REFERENCES `discl_comment` (`COMMENT_ID`),
  CONSTRAINT `DISCL_ATTACHMENT_FK2` FOREIGN KEY (`COMPONENT_TYPE_CODE`) REFERENCES `discl_component_type` (`COMPONENT_TYPE_CODE`),
  CONSTRAINT `DISCL_ATTACHMENT_FK3` FOREIGN KEY (`ATTA_TYPE_CODE`) REFERENCES `discl_atta_type` (`ATTA_TYPE_CODE`),
  CONSTRAINT `DISCL_ATTACHMENT_FK4` FOREIGN KEY (`ATTA_STATUS_CODE`) REFERENCES `discl_atta_status` (`ATTA_STATUS_CODE`),
  CONSTRAINT `DISCL_ATTACHMENT_FK5` FOREIGN KEY (`FILE_DATA_ID`) REFERENCES `discl_file_data` (`FILE_DATA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `discl_comment` (
  `COMMENT_ID` int NOT NULL AUTO_INCREMENT,
  `COMPONENT_TYPE_CODE` varchar(1) DEFAULT NULL,
  `COMPONENT_REFERENCE_ID` int DEFAULT NULL,
  `COMPONENT_REFERENCE_NUMBER` varchar(45) DEFAULT NULL,
  `COMMENT_TYPE` varchar(1) DEFAULT NULL,
  `COMMENT_BY_PERSON_ID` varchar(45) DEFAULT NULL,
  `DOCUMENT_OWNER_PERSON_ID` varchar(45) DEFAULT NULL,
  `IS_PRIVATE` varchar(1) DEFAULT NULL,
  `COMMENT` varchar(2000) DEFAULT NULL,
  `PARENT_COMMENT_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`COMMENT_ID`),
  KEY `DISCL_COMMENT_FK1_idx` (`COMPONENT_TYPE_CODE`),
  KEY `DISCL_COMMENT_FK2_idx` (`COMMENT_TYPE`),
  CONSTRAINT `DISCL_COMMENT_FK1` FOREIGN KEY (`COMPONENT_TYPE_CODE`) REFERENCES `discl_component_type` (`COMPONENT_TYPE_CODE`),
  CONSTRAINT `DISCL_COMMENT_FK2` FOREIGN KEY (`COMMENT_TYPE`) REFERENCES `discl_comment_type` (`COMMENT_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `discl_comment_type` (
  `COMMENT_TYPE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`COMMENT_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `discl_component_type` (
  `COMPONENT_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`COMPONENT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `discl_file_data` (
  `FILE_DATA_ID` int NOT NULL AUTO_INCREMENT,
  `FILE_PATH` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`FILE_DATA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `discl_valid_compnent_atta_ty` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `COMPONENT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `ATTA_TYPE_CODE` varchar(3) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `DISCL_VALID_COMPONENT_ATTA_TYPE_FK1_idx` (`COMPONENT_TYPE_CODE`),
  KEY `DISCL_VALID_COMPONENT_ATTA_TYPE_FK2_idx` (`ATTA_TYPE_CODE`),
  CONSTRAINT `DISCL_VALID_COMPONENT_ATTA_TYPE_FK1` FOREIGN KEY (`COMPONENT_TYPE_CODE`) REFERENCES `discl_component_type` (`COMPONENT_TYPE_CODE`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `DISCL_VALID_COMPONENT_ATTA_TYPE_FK2` FOREIGN KEY (`ATTA_TYPE_CODE`) REFERENCES `discl_atta_type` (`ATTA_TYPE_CODE`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `discl_valid_compnent_comnt_ty` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `COMPONENT_TYPE_CODE` varchar(1) DEFAULT NULL,
  `COMMENT_TYPE` varchar(1) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `DISCL_VALID_CMPNENT_COMNT_TY_FK1_idx` (`COMPONENT_TYPE_CODE`),
  KEY `DISCL_VALID_CMPNENT_COMNT_TY_FK2_idx` (`COMMENT_TYPE`),
  CONSTRAINT `DISCL_VALID_CMPNENT_COMNT_TY_FK1` FOREIGN KEY (`COMPONENT_TYPE_CODE`) REFERENCES `discl_component_type` (`COMPONENT_TYPE_CODE`),
  CONSTRAINT `DISCL_VALID_CMPNENT_COMNT_TY_FK2` FOREIGN KEY (`COMMENT_TYPE`) REFERENCES `discl_comment_type` (`COMMENT_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `disclosure_mv` (
  `DOCUMENT_NUMBER` varchar(40) DEFAULT NULL,
  `COI_DISCLOSURE_ID` decimal(12,0) NOT NULL,
  `COI_DISCLOSURE_NUMBER` varchar(20) DEFAULT NULL,
  `SEQUENCE_NUMBER` decimal(6,0) DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `FULL_NAME` varchar(163) DEFAULT NULL,
  `DISCLOSURE_DISPOSITION_CODE` decimal(3,0) DEFAULT NULL,
  `DISCLOSURE_DISPOSITION` varchar(200) DEFAULT NULL,
  `DISCLOSURE_STATUS_CODE` decimal(3,0) DEFAULT NULL,
  `DISCLOSURE_STATUS` varchar(200) DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(20) DEFAULT NULL,
  `DISC_ACTIVE_STATUS` decimal(1,0) DEFAULT NULL,
  `EXPIRATION_DATE` date DEFAULT NULL,
  `UPDATE_TIMESTAMP` date DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`COI_DISCLOSURE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `distribution` (
  `OSP_DISTRIBUTION_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `ACTIVE_FLAG` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OSP_DISTRIBUTION_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `document_status` (
  `DOCUMENT_STATUS_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`DOCUMENT_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `dyn_modules_config` (
  `MODULE_CODE` varchar(5) NOT NULL,
  `DESCRIPTION` varchar(200) NOT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  PRIMARY KEY (`MODULE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `dyn_section_config` (
  `SECTION_CODE` varchar(6) NOT NULL,
  `MODULE_CODE` varchar(5) NOT NULL,
  `DESCRIPTION` varchar(200) NOT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  PRIMARY KEY (`SECTION_CODE`),
  KEY `DYN_SECTION_CONFIG_FK1` (`MODULE_CODE`),
  CONSTRAINT `DYN_SECTION_CONFIG_FK1` FOREIGN KEY (`MODULE_CODE`) REFERENCES `dyn_modules_config` (`MODULE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `dyn_subsection_config` (
  `SUB_SECTION_CODE` varchar(5) NOT NULL,
  `SECTION_CODE` varchar(6) NOT NULL,
  `PARENT_SUB_SECTION_CODE` varchar(5) DEFAULT NULL,
  `DESCRIPTION` varchar(200) NOT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  PRIMARY KEY (`SUB_SECTION_CODE`),
  KEY `DYN_SUBSECTION_CONFIG_FK1` (`SECTION_CODE`),
  CONSTRAINT `DYN_SUBSECTION_CONFIG_FK1` FOREIGN KEY (`SECTION_CODE`) REFERENCES `dyn_section_config` (`SECTION_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `entity` (
  `ENTITY_ID` int NOT NULL AUTO_INCREMENT,
  `ENTITY_NUMBER` int DEFAULT NULL,
  `ENTITY_NAME` varchar(90) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `VERSION_STATUS` varchar(45) DEFAULT NULL,
  `ENTITY_STATUS_CODE` varchar(3) DEFAULT NULL,
  `ENTITY_TYPE_CODE` varchar(3) DEFAULT NULL,
  `RISK_CATEGORY_CODE` varchar(3) DEFAULT NULL,
  `PHONE` varchar(45) DEFAULT NULL,
  `COUNTRY_CODE` varchar(3) DEFAULT NULL,
  `CITY` varchar(90) DEFAULT NULL,
  `ADDRESS` varchar(2000) DEFAULT NULL,
  `ZIP_CODE` varchar(15) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(60) DEFAULT NULL,
  `WEB_URL` varchar(2000) DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `APPROVED_USER` varchar(60) DEFAULT NULL,
  `APPROVED_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`ENTITY_ID`),
  KEY `ENTITY_FK1_idx` (`ENTITY_STATUS_CODE`),
  KEY `ENTITY_FK2_idx` (`ENTITY_TYPE_CODE`),
  KEY `ENTITY_FK3_idx` (`COUNTRY_CODE`),
  KEY `ENTITY_FK5_idx` (`RISK_CATEGORY_CODE`),
  CONSTRAINT `ENTITY1_FK1` FOREIGN KEY (`ENTITY_STATUS_CODE`) REFERENCES `entity_status` (`ENTITY_STATUS_CODE`),
  CONSTRAINT `ENTITY1_FK2` FOREIGN KEY (`ENTITY_TYPE_CODE`) REFERENCES `entity_type` (`ENTITY_TYPE_CODE`),
  CONSTRAINT `ENTITY1_FK3` FOREIGN KEY (`COUNTRY_CODE`) REFERENCES `country` (`COUNTRY_CODE`),
  CONSTRAINT `ENTITY1_FK5` FOREIGN KEY (`RISK_CATEGORY_CODE`) REFERENCES `entity_risk_category` (`RISK_CATEGORY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `entity_rel_node_type` (
  `REL_NODE_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`REL_NODE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `entity_relationship` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `ENTITY_NUMBER` int NOT NULL,
  `NODE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `NODE_ID` int DEFAULT NULL,
  `ENTITY_REL_TYPE_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ENITY_RELATIONSHIP_FK1_idx` (`ENTITY_REL_TYPE_CODE`),
  KEY `ENITY_RELATIONSHIP_FK2_idx` (`NODE_TYPE_CODE`),
  CONSTRAINT `ENITY_RELATIONSHIP_FK1` FOREIGN KEY (`ENTITY_REL_TYPE_CODE`) REFERENCES `entity_relationship_type` (`ENTITY_REL_TYPE_CODE`),
  CONSTRAINT `ENITY_RELATIONSHIP_FK2` FOREIGN KEY (`NODE_TYPE_CODE`) REFERENCES `entity_rel_node_type` (`REL_NODE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `entity_relationship_type` (
  `ENTITY_REL_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ENTITY_REL_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `entity_risk_category` (
  `RISK_CATEGORY_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(45) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`RISK_CATEGORY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `entity_status` (
  `ENTITY_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ENTITY_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `entity_type` (
  `ENTITY_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ENTITY_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_prop_discipline_cluster` (
  `CLUSTER_CODE` int NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CLUSTER_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_prop_evalpanel_persons` (
  `EVALPANEL_PERSONS_ID` int NOT NULL AUTO_INCREMENT,
  `APPROVER_NUMBER` int DEFAULT NULL,
  `APPROVER_PERSON_ID` varchar(255) DEFAULT NULL,
  `APPROVER_PERSON_NAME` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `PROPOSAL_EVALUATION_ID` int DEFAULT NULL,
  PRIMARY KEY (`EVALPANEL_PERSONS_ID`),
  KEY `EPS_PROP_EVALPANEL_PERSONS_FK1` (`PROPOSAL_EVALUATION_ID`),
  CONSTRAINT `EPS_PROP_EVALPANEL_PERSONS_FK1` FOREIGN KEY (`PROPOSAL_EVALUATION_ID`) REFERENCES `eps_proposal_evaluation_panel` (`PROPOSAL_EVALUATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_prop_evaluation_score` (
  `EPS_PROP_EVALUATION_SCORE_ID` int NOT NULL AUTO_INCREMENT,
  `ADJUSTED_SCORE` decimal(4,2) DEFAULT NULL,
  `GRANT_HEADER_ID` int DEFAULT NULL,
  `IS_SHORTLISTED` varchar(255) DEFAULT NULL,
  `PROPOSAL_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `PROPOSAL_RANK` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`EPS_PROP_EVALUATION_SCORE_ID`),
  KEY `EPS_PROP_EVALUATION_SCORE_FK1` (`GRANT_HEADER_ID`),
  KEY `EPS_PROP_EVALUATION_SCORE_FK2` (`PROPOSAL_ID`),
  CONSTRAINT `EPS_PROP_EVALUATION_SCORE_FK1` FOREIGN KEY (`GRANT_HEADER_ID`) REFERENCES `grant_call_header` (`GRANT_HEADER_ID`),
  CONSTRAINT `EPS_PROP_EVALUATION_SCORE_FK2` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `eps_proposal` (`PROPOSAL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_prop_evaluation_stop` (
  `EVALUATION_STOP_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `ROLE_ID` int DEFAULT NULL,
  `STATUS_CODE` int DEFAULT NULL,
  `HAS_RANK` varchar(1) DEFAULT NULL,
  `HAS_ENDORSE` varchar(1) DEFAULT NULL,
  `IS_FINAL` varchar(1) DEFAULT NULL,
  `STOP_NUMBER` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `ACTIVITY_TYPE_CODE` varchar(3) DEFAULT NULL,
  `HAS_QUESTIONNAIRE` varchar(1) DEFAULT NULL,
  `HAS_RECOMMENDATION` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`EVALUATION_STOP_CODE`),
  KEY `EPS_PROP_EVALUATION_STOP_FK1` (`ROLE_ID`),
  KEY `EPS_PROP_EVALUATION_STOP_FK2` (`ACTIVITY_TYPE_CODE`),
  KEY `EPS_PROP_EVALUATION_STOP_FK3` (`STATUS_CODE`),
  CONSTRAINT `EPS_PROP_EVALUATION_STOP_FK1` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ROLE_ID`),
  CONSTRAINT `EPS_PROP_EVALUATION_STOP_FK2` FOREIGN KEY (`ACTIVITY_TYPE_CODE`) REFERENCES `activity_type` (`ACTIVITY_TYPE_CODE`),
  CONSTRAINT `EPS_PROP_EVALUATION_STOP_FK3` FOREIGN KEY (`STATUS_CODE`) REFERENCES `eps_proposal_status` (`STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_prop_evaluatn_status_flow` (
  `EPS_PROP_EVAL_STATUS_FLOW_CODE` int NOT NULL,
  `STOP_NUMBER` int DEFAULT NULL,
  `STATUS_CODE` int DEFAULT NULL,
  `REVIEW_STATUS_CODE` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `ROLE_ID` int DEFAULT NULL,
  `NEXT_EVALUATION_STOP` int DEFAULT NULL,
  PRIMARY KEY (`EPS_PROP_EVAL_STATUS_FLOW_CODE`),
  KEY `EPS_PROP_EVAL_STATUS_FLOW_FK1` (`ROLE_ID`),
  KEY `EPS_PROP_EVAL_STATUS_FLOW_FK2` (`REVIEW_STATUS_CODE`),
  KEY `EPS_PROP_EVAL_STATUS_FLOW_FK3` (`STATUS_CODE`),
  KEY `EPS_PROP_EVAL_STATUS_FLOW_FK4` (`NEXT_EVALUATION_STOP`),
  CONSTRAINT `EPS_PROP_EVAL_STATUS_FLOW_FK1` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ROLE_ID`),
  CONSTRAINT `EPS_PROP_EVAL_STATUS_FLOW_FK2` FOREIGN KEY (`REVIEW_STATUS_CODE`) REFERENCES `review_status` (`REVIEW_STATUS_CODE`),
  CONSTRAINT `EPS_PROP_EVAL_STATUS_FLOW_FK3` FOREIGN KEY (`STATUS_CODE`) REFERENCES `eps_proposal_status` (`STATUS_CODE`),
  CONSTRAINT `EPS_PROP_EVAL_STATUS_FLOW_FK4` FOREIGN KEY (`NEXT_EVALUATION_STOP`) REFERENCES `eps_prop_evaluation_stop` (`EVALUATION_STOP_CODE`),
  CONSTRAINT `EPS_PROP_EVAL_STATUS_FLOWS_FK2` FOREIGN KEY (`REVIEW_STATUS_CODE`) REFERENCES `review_status` (`REVIEW_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_prop_person_role` (
  `UNIT_DETAILS_REQUIRED` varchar(1) DEFAULT 'Y',
  `PROP_PERSON_ROLE_ID` int NOT NULL,
  `DESCRIPTION` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `CERTIFICATION_REQUIRED` varchar(1) DEFAULT 'Y',
  `PROP_PERSON_ROLE_CODE` varchar(12) DEFAULT NULL,
  `SPONSOR_HIERARCHY_NAME` varchar(50) DEFAULT 'DEFAULT',
  `READ_ONLY_ROLE` varchar(1) DEFAULT 'N',
  `IS_MULTI_PI` varchar(255) DEFAULT NULL,
  `SORT_ID` int DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `SHOW_PROJECT_ROLE` varchar(1) DEFAULT 'N',
  `INCLUDE_IN_CURRENT_AND_PENDING` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`PROP_PERSON_ROLE_ID`),
  UNIQUE KEY `EPS_PROP_PERSON_ROLE_UK` (`PROP_PERSON_ROLE_CODE`,`SPONSOR_HIERARCHY_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_prop_person_units` (
  `PROP_PERSON_UNIT_ID` int NOT NULL AUTO_INCREMENT,
  `PROPOSAL_ID` int DEFAULT NULL,
  `PROPOSAL_PERSON_ID` int DEFAULT NULL,
  `LEAD_UNIT_FLAG` varchar(1) DEFAULT NULL,
  `UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `UPDATE_TIMESTAMP` timestamp(6) NULL DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`PROP_PERSON_UNIT_ID`),
  KEY `EPS_PROP_PERSON_UNITS_FK1` (`PROPOSAL_PERSON_ID`),
  KEY `EPS_PROP_PERSON_UNITS_FK2` (`UNIT_NUMBER`),
  CONSTRAINT `EPS_PROP_PERSON_UNITS_FK1` FOREIGN KEY (`PROPOSAL_PERSON_ID`) REFERENCES `eps_proposal_persons` (`PROPOSAL_PERSON_ID`),
  CONSTRAINT `EPS_PROP_PERSON_UNITS_FK2` FOREIGN KEY (`UNIT_NUMBER`) REFERENCES `unit` (`UNIT_NUMBER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_prop_pre_review` (
  `PRE_REVIEW_ID` int NOT NULL,
  `PROPOSAL_ID` int DEFAULT NULL,
  `PRE_REVIEW_TYPE_CODE` varchar(3) DEFAULT NULL,
  `PRE_REVIEW_STATUS_CODE` varchar(3) DEFAULT NULL,
  `REVIEWER_PERSON_ID` varchar(40) DEFAULT NULL,
  `REVIEWER_FULLNAME` varchar(90) DEFAULT NULL,
  `REVIEWER_EMAIL` varchar(255) DEFAULT NULL,
  `REQUESTOR_PERSON_ID` varchar(90) DEFAULT NULL,
  `REQUESTOR_FULLNAME` varchar(90) DEFAULT NULL,
  `REQUESTOR_COMMENT` varchar(4000) DEFAULT NULL,
  `REQUESTOR_EMAIL` varchar(255) DEFAULT NULL,
  `REQUEST_DATE` datetime DEFAULT NULL,
  `COMPLETION_DATE` datetime DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`PRE_REVIEW_ID`),
  KEY `EPS_PROP_PRE_REVIEW_FK1` (`PRE_REVIEW_TYPE_CODE`),
  KEY `EPS_PROP_PRE_REVIEW_FK2` (`PRE_REVIEW_STATUS_CODE`),
  CONSTRAINT `EPS_PROP_PRE_REVIEW_FK1` FOREIGN KEY (`PRE_REVIEW_TYPE_CODE`) REFERENCES `pre_review_type` (`PRE_REVIEW_TYPE_CODE`),
  CONSTRAINT `EPS_PROP_PRE_REVIEW_FK2` FOREIGN KEY (`PRE_REVIEW_STATUS_CODE`) REFERENCES `pre_review_status` (`PRE_REVIEW_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_prop_pre_review_attachment` (
  `PRE_REVIEW_ATTACHMENT_ID` int NOT NULL,
  `PRE_REVIEW_COMMENT_ID` int DEFAULT NULL,
  `PRE_REVIEW_ID` int DEFAULT NULL,
  `PROPOSAL_ID` int DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `ATTACHMENT` blob,
  `FILE_NAME` varchar(1000) DEFAULT NULL,
  `MIME_TYPE` varchar(100) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `FILE_DATA_ID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PRE_REVIEW_ATTACHMENT_ID`),
  KEY `PRE_REVIEWER_ATTACHMENT_FK1` (`PRE_REVIEW_COMMENT_ID`),
  CONSTRAINT `PRE_REVIEWER_ATTACHMENT_FK1` FOREIGN KEY (`PRE_REVIEW_COMMENT_ID`) REFERENCES `eps_prop_pre_review_comment` (`PRE_REVIEW_COMMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_prop_pre_review_comment` (
  `PRE_REVIEW_COMMENT_ID` int NOT NULL,
  `PRE_REVIEW_ID` int DEFAULT NULL,
  `PROPOSAL_ID` int DEFAULT NULL,
  `REVIEW_COMMENT` varchar(4000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_PRIVATE_COMMENT` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PRE_REVIEW_COMMENT_ID`),
  KEY `EPS_PRP_PRE_REVIEW_COMMENT_FK1` (`PRE_REVIEW_ID`),
  CONSTRAINT `EPS_PRP_PRE_REVIEW_COMMENT_FK1` FOREIGN KEY (`PRE_REVIEW_ID`) REFERENCES `eps_prop_pre_review` (`PRE_REVIEW_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal` (
  `PROPOSAL_ID` int NOT NULL AUTO_INCREMENT,
  `ABSTRACT_DESC` text,
  `ACTIVITY_TYPE_CODE` varchar(3) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `DELIVERABLES` text,
  `DETAILS` varchar(4000) DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `FUNDING_STRATEGY` varchar(255) DEFAULT NULL,
  `GRANT_HEADER_ID` int DEFAULT NULL,
  `GRANT_TYPE_CODE` int DEFAULT NULL,
  `HOME_UNIT_NAME` varchar(200) DEFAULT NULL,
  `HOME_UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `IP_NUMBER` varchar(30) DEFAULT NULL,
  `RESEARCH_AREA_DESC` varchar(4000) DEFAULT NULL,
  `SPONSOR_CODE` varchar(6) DEFAULT NULL,
  `SPONSOR_PROPOSAL_NUMBER` varchar(100) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `STATUS_CODE` int DEFAULT NULL,
  `SUBMISSION_DATE` datetime DEFAULT NULL,
  `SUBMIT_USER` varchar(60) DEFAULT NULL,
  `TITLE` varchar(1000) DEFAULT NULL,
  `TYPE_CODE` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `BUDGET_HEADER_ID` int DEFAULT NULL,
  `INTERNAL_DEADLINE_DATE` datetime DEFAULT NULL,
  `IS_SUBCONTRACT` varchar(1) DEFAULT NULL,
  `IS_DOMESTIC_SITE` varchar(1) DEFAULT NULL,
  `IS_MULTISITE_STUDY` varchar(1) DEFAULT NULL,
  `AWARD_TYPE_CODE` varchar(255) DEFAULT NULL,
  `BASE_PROPOSAL_NUMBER` varchar(255) DEFAULT NULL,
  `CFDA_NUMBER` varchar(100) DEFAULT NULL,
  `PRIME_SPONSOR_CODE` varchar(255) DEFAULT NULL,
  `PROGRAM_ANNOUNCEMENT_NUMBER` varchar(255) DEFAULT NULL,
  `APPLICATION_ID` varchar(255) DEFAULT NULL,
  `IS_ENDORSED_ONCE` varchar(255) DEFAULT NULL,
  `MULTI_DISCIPLINARY_DESC` longtext,
  `PROPOSAL_RANK` int DEFAULT NULL,
  `SPONSOR_DEADLINE_DATE` datetime(6) DEFAULT NULL,
  `CLUSTER_CODE` int DEFAULT NULL,
  `EXTERNAL_FUNDING_AGENCY_ID` varchar(50) DEFAULT NULL,
  `IS_ELIGIBLE_CRITERIA_MET` varchar(1) DEFAULT NULL,
  `EVALUATION_RECOMMENDATION_CODE` int DEFAULT NULL,
  `isReviewExist` bit(1) DEFAULT NULL,
  `DURATION` varchar(255) DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `DOCUMENT_STATUS_CODE` varchar(3) DEFAULT NULL,
  `SOURCE_PROPOSAL_ID` int DEFAULT NULL,
  PRIMARY KEY (`PROPOSAL_ID`),
  KEY `EPS_PROPOSAL_FK7` (`BUDGET_HEADER_ID`),
  KEY `EPS_PROPOSAL_FK4` (`GRANT_HEADER_ID`),
  KEY `EPS_PROPOSAL_FK9` (`AWARD_TYPE_CODE`),
  KEY `EPS_PROPOSAL_FK10` (`CLUSTER_CODE`),
  KEY `EPS_PROPOSAL_FK11` (`EVALUATION_RECOMMENDATION_CODE`),
  KEY `EPS_PROPOSAL_IX6` (`TYPE_CODE`),
  KEY `EPS_PROPOSAL_IX7` (`ACTIVITY_TYPE_CODE`),
  KEY `EPS_PROPOSAL_IX8` (`GRANT_TYPE_CODE`),
  KEY `EPS_PROPOSAL_IX9` (`HOME_UNIT_NUMBER`),
  KEY `EPS_PROPOSAL_IX10` (`STATUS_CODE`),
  KEY `EPS_PROPOSAL_IX11` (`SPONSOR_CODE`),
  KEY `EPS_PROPOSAL_FK13` (`DOCUMENT_STATUS_CODE`),
  KEY `EPS_PROPOSAL_FK16` (`PRIME_SPONSOR_CODE`),
  CONSTRAINT `EPS_PROPOSAL_FK1` FOREIGN KEY (`TYPE_CODE`) REFERENCES `eps_proposal_type` (`TYPE_CODE`),
  CONSTRAINT `EPS_PROPOSAL_FK10` FOREIGN KEY (`CLUSTER_CODE`) REFERENCES `eps_prop_discipline_cluster` (`CLUSTER_CODE`),
  CONSTRAINT `EPS_PROPOSAL_FK11` FOREIGN KEY (`EVALUATION_RECOMMENDATION_CODE`) REFERENCES `evaluation_recommendation` (`EVALUATION_RECOMMENDATION_CODE`),
  CONSTRAINT `EPS_PROPOSAL_FK12` FOREIGN KEY (`HOME_UNIT_NUMBER`) REFERENCES `unit` (`UNIT_NUMBER`),
  CONSTRAINT `EPS_PROPOSAL_FK13` FOREIGN KEY (`DOCUMENT_STATUS_CODE`) REFERENCES `eps_proposal_document_status` (`DOCUMENT_STATUS_CODE`),
  CONSTRAINT `EPS_PROPOSAL_FK15` FOREIGN KEY (`SPONSOR_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`),
  CONSTRAINT `EPS_PROPOSAL_FK16` FOREIGN KEY (`PRIME_SPONSOR_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`),
  CONSTRAINT `EPS_PROPOSAL_FK2` FOREIGN KEY (`STATUS_CODE`) REFERENCES `eps_proposal_status` (`STATUS_CODE`),
  CONSTRAINT `EPS_PROPOSAL_FK4` FOREIGN KEY (`GRANT_HEADER_ID`) REFERENCES `grant_call_header` (`GRANT_HEADER_ID`),
  CONSTRAINT `EPS_PROPOSAL_FK6` FOREIGN KEY (`GRANT_TYPE_CODE`) REFERENCES `grant_call_type` (`GRANT_TYPE_CODE`),
  CONSTRAINT `EPS_PROPOSAL_FK7` FOREIGN KEY (`BUDGET_HEADER_ID`) REFERENCES `budget_header` (`BUDGET_HEADER_ID`),
  CONSTRAINT `EPS_PROPOSAL_FK8` FOREIGN KEY (`ACTIVITY_TYPE_CODE`) REFERENCES `activity_type` (`ACTIVITY_TYPE_CODE`),
  CONSTRAINT `EPS_PROPOSAL_FK9` FOREIGN KEY (`AWARD_TYPE_CODE`) REFERENCES `award_type` (`AWARD_TYPE_CODE`),
  CONSTRAINT `EPS_PROPOSAL_ibfk_1` FOREIGN KEY (`EVALUATION_RECOMMENDATION_CODE`) REFERENCES `evaluation_recommendation` (`EVALUATION_RECOMMENDATION_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_attach_type` (
  `ATTACHMNT_TYPE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `IS_PRIVATE` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`ATTACHMNT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_attachments` (
  `ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `ATTACHMNT_TYPE_CODE` int DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `MIME_TYPE` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `PROPOSAL_ID` int DEFAULT NULL,
  `NARRATIVE_STATUS_CODE` varchar(3) DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `FILE_DATA_ID` varchar(36) DEFAULT NULL,
  `DOCUMENT_STATUS_CODE` int DEFAULT NULL,
  `DOCUMENT_ID` int DEFAULT NULL,
  PRIMARY KEY (`ATTACHMENT_ID`),
  KEY `EPS_PROP_ATTACHMENTS_FK2` (`ATTACHMNT_TYPE_CODE`),
  KEY `EPS_PROP_ATTACHMENTS_FK1` (`PROPOSAL_ID`),
  KEY `EPS_PROP_ATTACHMENTS_FK3` (`NARRATIVE_STATUS_CODE`),
  KEY `EPS_PROPOSAL_ATTACHMENTS_FK5` (`DOCUMENT_STATUS_CODE`),
  CONSTRAINT `EPS_PROP_ATTACHMENTS_FK1` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `eps_proposal` (`PROPOSAL_ID`),
  CONSTRAINT `EPS_PROP_ATTACHMENTS_FK2` FOREIGN KEY (`ATTACHMNT_TYPE_CODE`) REFERENCES `eps_proposal_attach_type` (`ATTACHMNT_TYPE_CODE`),
  CONSTRAINT `EPS_PROP_ATTACHMENTS_FK3` FOREIGN KEY (`NARRATIVE_STATUS_CODE`) REFERENCES `narrative_status` (`NARRATIVE_STATUS_CODE`),
  CONSTRAINT `EPS_PROPOSAL_ATTACHMENTS_FK5` FOREIGN KEY (`DOCUMENT_STATUS_CODE`) REFERENCES `document_status` (`DOCUMENT_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_comment_attachments` (
  `COMMENT_ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `EPS_PROPOSAL_COMMENT_ID` int DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `FILE_DATA_ID` varchar(36) DEFAULT NULL,
  `MIME_TYPE` varchar(250) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`COMMENT_ATTACHMENT_ID`),
  KEY `EPS_PROP_COMMENT_ATTACHMENT_FK1` (`EPS_PROPOSAL_COMMENT_ID`),
  CONSTRAINT `EPS_PROP_COMMENT_ATTACHMENT_FK1` FOREIGN KEY (`EPS_PROPOSAL_COMMENT_ID`) REFERENCES `eps_proposal_comments` (`EPS_PROPOSAL_COMMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_comments` (
  `EPS_PROPOSAL_COMMENT_ID` int NOT NULL AUTO_INCREMENT,
  `PROPOSAL_ID` int DEFAULT NULL,
  `COMMENT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `COMMENTS` longtext,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_PRIVATE` varchar(1) DEFAULT NULL,
  `IS_PUBLIC` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`EPS_PROPOSAL_COMMENT_ID`),
  KEY `EPS_PROPOSAL_COMMENTS_FK1` (`PROPOSAL_ID`),
  KEY `EPS_PROPOSAL_COMMENTS_FK2` (`COMMENT_TYPE_CODE`),
  CONSTRAINT `EPS_PROPOSAL_COMMENTS_FK1` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `eps_proposal` (`PROPOSAL_ID`),
  CONSTRAINT `EPS_PROPOSAL_COMMENTS_FK2` FOREIGN KEY (`COMMENT_TYPE_CODE`) REFERENCES `comment_type` (`COMMENT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_cong_district` (
  `PROPOSAL_CONG_DISTRICT_ID` int NOT NULL AUTO_INCREMENT,
  `PROPOSAL_ORGANIZATION_ID` int DEFAULT NULL,
  `CONG_DISTRICT_CODE` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` date NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  PRIMARY KEY (`PROPOSAL_CONG_DISTRICT_ID`),
  KEY `EPS_PROPOSAL_CONG_DISTRICT_FK1` (`CONG_DISTRICT_CODE`),
  KEY `EPS_PROPOSAL_CONG_DISTRICT_FK2` (`PROPOSAL_ORGANIZATION_ID`),
  CONSTRAINT `EPS_PROPOSAL_CONG_DISTRICT_FK1` FOREIGN KEY (`CONG_DISTRICT_CODE`) REFERENCES `congressional_district` (`CONG_DISTRICT_CODE`),
  CONSTRAINT `EPS_PROPOSAL_CONG_DISTRICT_FK2` FOREIGN KEY (`PROPOSAL_ORGANIZATION_ID`) REFERENCES `eps_proposal_organization` (`PROPOSAL_ORGANIZATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_document_status` (
  `DOCUMENT_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`DOCUMENT_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_evaluation_panel` (
  `PROPOSAL_EVALUATION_ID` int NOT NULL AUTO_INCREMENT,
  `CAN_SCORE` varchar(255) DEFAULT NULL,
  `IS_ADMIN_SELECTED` varchar(255) DEFAULT NULL,
  `IS_PI_SELECTED` varchar(255) DEFAULT NULL,
  `MAP_ID` int DEFAULT NULL,
  `PROPOSAL_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PROPOSAL_EVALUATION_ID`),
  KEY `EPS_PROPOSAL_EVALUATION_FK2` (`MAP_ID`),
  CONSTRAINT `EPS_PROPOSAL_EVALUATION_FK2` FOREIGN KEY (`MAP_ID`) REFERENCES `workflow_map` (`MAP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_ext` (
  `PROPOSAL_ID` int NOT NULL,
  `IS_FOREIGN_ACTIVITIES` varchar(1) DEFAULT NULL,
  `IS_SUBCONTRACT` varchar(1) DEFAULT NULL,
  `IS_MULTISITE_STUDY` varchar(1) DEFAULT NULL,
  `IS_DOMESTIC_SITE` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`PROPOSAL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_history` (
  `HISTORY_ID` int NOT NULL AUTO_INCREMENT,
  `ACTIVE_PROPOSAL_ID` int DEFAULT NULL,
  `ARCHIVE_PROPOSAL_ID` int DEFAULT NULL,
  `REQUEST_TYPE` varchar(200) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`HISTORY_ID`),
  KEY `EPS_PROPOSAL_HISTORY_FK1` (`ACTIVE_PROPOSAL_ID`),
  KEY `EPS_PROPOSAL_HISTORY_FK2` (`ARCHIVE_PROPOSAL_ID`),
  CONSTRAINT `EPS_PROPOSAL_HISTORY_FK1` FOREIGN KEY (`ACTIVE_PROPOSAL_ID`) REFERENCES `eps_proposal` (`PROPOSAL_ID`),
  CONSTRAINT `EPS_PROPOSAL_HISTORY_FK2` FOREIGN KEY (`ARCHIVE_PROPOSAL_ID`) REFERENCES `eps_proposal` (`PROPOSAL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_irb_protocol` (
  `IRB_PROTOCOL_ID` int NOT NULL AUTO_INCREMENT,
  `PROTOCOL_ID` decimal(12,0) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `PROPOSAL_ID` int DEFAULT NULL,
  PRIMARY KEY (`IRB_PROTOCOL_ID`),
  KEY `EPS_PROP_PROTOCOL_FK1` (`PROPOSAL_ID`),
  KEY `EPS_PROP_PROTOCOL_FK2` (`PROTOCOL_ID`),
  CONSTRAINT `EPS_PROP_PROTOCOL_FK1` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `eps_proposal` (`PROPOSAL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_key_personnel_attach_type` (
  `ATTACHMNT_TYPE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `IS_PRIVATE` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`ATTACHMNT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_keywords` (
  `KEYWORD_ID` int NOT NULL AUTO_INCREMENT,
  `SCIENCE_KEYWORD_CODE` varchar(15) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `PROPOSAL_ID` int DEFAULT NULL,
  `KEYWORD` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`KEYWORD_ID`),
  KEY `EPS_PROP_KEYWORDS_FK1` (`PROPOSAL_ID`),
  KEY `EPS_PROP_KEYWORDS_FK2` (`SCIENCE_KEYWORD_CODE`),
  CONSTRAINT `EPS_PROP_KEYWORDS_FK1` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `eps_proposal` (`PROPOSAL_ID`),
  CONSTRAINT `EPS_PROP_KEYWORDS_FK2` FOREIGN KEY (`SCIENCE_KEYWORD_CODE`) REFERENCES `science_keyword` (`SCIENCE_KEYWORD_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_kpi` (
  `PROPOSAL_KPI_ID` int NOT NULL AUTO_INCREMENT,
  `PROPOSAL_ID` int DEFAULT NULL,
  `KPI_TYPE_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`PROPOSAL_KPI_ID`),
  KEY `EPS_PROPOSAL_KPI_FK` (`PROPOSAL_ID`),
  KEY `EPS_PROPOSAL_KPI_FK2` (`KPI_TYPE_CODE`),
  CONSTRAINT `EPS_PROPOSAL_KPI_FK` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `eps_proposal` (`PROPOSAL_ID`),
  CONSTRAINT `EPS_PROPOSAL_KPI_FK2` FOREIGN KEY (`KPI_TYPE_CODE`) REFERENCES `kpi_type` (`KPI_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_kpi_criteria` (
  `EPS_PROPOSAL_KPI_CRITERIA_ID` int NOT NULL AUTO_INCREMENT,
  `KPI_CRITERIA_TYPE_CODE` varchar(255) DEFAULT NULL,
  `KPI_TYPE_CODE` varchar(255) DEFAULT NULL,
  `PROPOSAL_KPI_ID` int DEFAULT NULL,
  `TARGET` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`EPS_PROPOSAL_KPI_CRITERIA_ID`),
  KEY `EPS_PROPOSAL_KPI_CRITERIA_FK3` (`KPI_CRITERIA_TYPE_CODE`),
  KEY `EPS_PROPOSAL_KPI_CRITERIA_FK2` (`KPI_TYPE_CODE`),
  KEY `EPS_PROPOSAL_KPI_CRITERIA_FK1` (`PROPOSAL_KPI_ID`),
  CONSTRAINT `EPS_PROPOSAL_KPI_CRITERIA_FK1` FOREIGN KEY (`PROPOSAL_KPI_ID`) REFERENCES `eps_proposal_kpi` (`PROPOSAL_KPI_ID`),
  CONSTRAINT `EPS_PROPOSAL_KPI_CRITERIA_FK2` FOREIGN KEY (`KPI_TYPE_CODE`) REFERENCES `kpi_type` (`KPI_TYPE_CODE`),
  CONSTRAINT `EPS_PROPOSAL_KPI_CRITERIA_FK3` FOREIGN KEY (`KPI_CRITERIA_TYPE_CODE`) REFERENCES `kpi_criteria_type` (`KPI_CRITERIA_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_milestone` (
  `PROPOSAL_MILESTONE_ID` int NOT NULL AUTO_INCREMENT,
  `PROPOSAL_ID` int DEFAULT NULL,
  `MILESTONE` varchar(1000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `DURATION` int DEFAULT NULL,
  `START_MONTH` int DEFAULT NULL,
  PRIMARY KEY (`PROPOSAL_MILESTONE_ID`),
  KEY `EPS_PROPOSAL_MILESTONE_FK` (`PROPOSAL_ID`),
  CONSTRAINT `EPS_PROPOSAL_MILESTONE_FK` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `eps_proposal` (`PROPOSAL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_organization` (
  `PROPOSAL_ORGANIZATION_ID` int NOT NULL AUTO_INCREMENT,
  `PROPOSAL_ID` int DEFAULT NULL,
  `ORGANIZATION_TYPE_CODE` varchar(3) DEFAULT NULL,
  `ORGANIZATION_ID` varchar(20) DEFAULT NULL,
  `ROLODEX_ID` int DEFAULT NULL,
  `LOCATION` varchar(300) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  PRIMARY KEY (`PROPOSAL_ORGANIZATION_ID`),
  KEY `EPS_PROP_SITES_FK1` (`ORGANIZATION_ID`),
  KEY `EPS_PROP_SITES_FK2` (`ORGANIZATION_TYPE_CODE`),
  CONSTRAINT `EPS_PROP_SITES_FK1` FOREIGN KEY (`ORGANIZATION_ID`) REFERENCES `organization` (`ORGANIZATION_ID`),
  CONSTRAINT `EPS_PROP_SITES_FK2` FOREIGN KEY (`ORGANIZATION_TYPE_CODE`) REFERENCES `organization_type` (`ORGANIZATION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_person_attachmnt` (
  `ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `FILE_DATA_ID` varchar(255) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `MIME_TYPE` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `PROPOSAL_PERSON_ID` int NOT NULL,
  `DOCUMENT_ID` int DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `DOCUMENT_STATUS_CODE` int DEFAULT NULL,
  `ATTACHMNT_TYPE_CODE` int DEFAULT NULL,
  PRIMARY KEY (`ATTACHMENT_ID`),
  KEY `EPS_PROP_PERSON_ATTACHMENT_FK1` (`PROPOSAL_PERSON_ID`),
  CONSTRAINT `EPS_PROP_PERSON_ATTACHMENT_FK1` FOREIGN KEY (`PROPOSAL_PERSON_ID`) REFERENCES `eps_proposal_persons` (`PROPOSAL_PERSON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_person_degree` (
  `PROPOSAL_PERSON_DEGREE_ID` int NOT NULL AUTO_INCREMENT,
  `PROPOSAL_PERSON_ID` int DEFAULT NULL,
  `DEGREE_CODE` varchar(10) DEFAULT NULL,
  `DEGREE` varchar(200) DEFAULT NULL,
  `FIELD_OF_STUDY` varchar(200) DEFAULT NULL,
  `SPECIALIZATION` varchar(200) DEFAULT NULL,
  `SCHOOL` varchar(200) DEFAULT NULL,
  `GRADUATION_DATE` varchar(20) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`PROPOSAL_PERSON_DEGREE_ID`),
  KEY `EPS_PROP_PERSON_DEGREE_FK1` (`PROPOSAL_PERSON_ID`),
  KEY `DEGREE_CODE_FK3` (`DEGREE_CODE`),
  CONSTRAINT `DEGREE_CODE_FK3` FOREIGN KEY (`DEGREE_CODE`) REFERENCES `degree_type` (`DEGREE_CODE`),
  CONSTRAINT `EPS_PROP_PERSON_DEGREE_FK1` FOREIGN KEY (`PROPOSAL_PERSON_ID`) REFERENCES `eps_proposal_persons` (`PROPOSAL_PERSON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_person_roles` (
  `PROPOSAL_PERSON_ROLE_ID` int NOT NULL AUTO_INCREMENT,
  `PROPOSAL_ID` int DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `ROLE_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`PROPOSAL_PERSON_ROLE_ID`),
  KEY `EPS_PROPOSAL_PERSON_ROLES_FK1` (`PERSON_ID`),
  KEY `EPS_PROPOSAL_PERSON_ROLES_FK2` (`ROLE_ID`),
  KEY `EPS_PROPOSAL_PERSON_ROLES_IX5` (`PROPOSAL_ID`),
  CONSTRAINT `EPS_PROPOSAL_PERSON_ROLES_FK1` FOREIGN KEY (`PERSON_ID`) REFERENCES `person` (`PERSON_ID`),
  CONSTRAINT `EPS_PROPOSAL_PERSON_ROLES_FK2` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_persons` (
  `PROPOSAL_PERSON_ID` int NOT NULL AUTO_INCREMENT,
  `DEPARTMENT` varchar(90) DEFAULT NULL,
  `FULL_NAME` varchar(90) DEFAULT NULL,
  `UNIT_NAME` varchar(90) DEFAULT NULL,
  `UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `PROP_PERSON_ROLE_ID` int DEFAULT NULL,
  `ROLODEX_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `PROPOSAL_ID` int DEFAULT NULL,
  `PERCENTAGE_OF_EFFORT` decimal(5,2) DEFAULT NULL,
  `PI_FLAG` varchar(255) DEFAULT NULL,
  `DESIGNATION` varchar(255) DEFAULT NULL,
  `IS_MULTI_PI` varchar(255) DEFAULT NULL,
  `IS_PERSON_CERTIFIED` varchar(1) DEFAULT 'N',
  `PROJECT_ROLE` varchar(200) DEFAULT NULL,
  `IS_TRAINING_COMPLETED` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`PROPOSAL_PERSON_ID`),
  KEY `EPS_PROPOSAL_PERSONS_FK2` (`PROP_PERSON_ROLE_ID`),
  KEY `FK3r283i2k00xk3nfhj13tfck7o` (`PROPOSAL_ID`),
  CONSTRAINT `EPS_PROPOSAL_PERSONS_FK1` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `eps_proposal` (`PROPOSAL_ID`),
  CONSTRAINT `EPS_PROPOSAL_PERSONS_FK2` FOREIGN KEY (`PROP_PERSON_ROLE_ID`) REFERENCES `eps_prop_person_role` (`PROP_PERSON_ROLE_ID`),
  CONSTRAINT `FK3r283i2k00xk3nfhj13tfck7o` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `eps_proposal` (`PROPOSAL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_project_team` (
  `PROPOSAL_PROJECT_TEAM_ID` int NOT NULL AUTO_INCREMENT,
  `PROPOSAL_ID` int DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `FULL_NAME` varchar(255) DEFAULT NULL,
  `PROJECT_ROLE` varchar(100) DEFAULT NULL,
  `NON_EMPLOYEE_FLAG` varchar(1) DEFAULT NULL,
  `PERCENTAGE_CHARGED` decimal(5,2) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `DESIGNATION` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`PROPOSAL_PROJECT_TEAM_ID`),
  KEY `EPS_PROPOSAL_PROJECT_TEAM_FK1` (`PROPOSAL_ID`),
  CONSTRAINT `EPS_PROPOSAL_PROJECT_TEAM_FK1` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `eps_proposal` (`PROPOSAL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_rates` (
  `PROPOSAL_RATE_ID` int NOT NULL AUTO_INCREMENT,
  `ACTIVITY_TYPE_CODE` varchar(3) DEFAULT NULL,
  `APPLICABLE_RATE` decimal(12,2) DEFAULT NULL,
  `FISCAL_YEAR` varchar(4) DEFAULT NULL,
  `INSTITUTE_RATE` decimal(12,2) DEFAULT NULL,
  `ON_OFF_CAMPUS_FLAG` varchar(1) DEFAULT NULL,
  `RATE_CLASS_CODE` varchar(3) DEFAULT NULL,
  `RATE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `BUDGET_HEADER_ID` int DEFAULT NULL,
  PRIMARY KEY (`PROPOSAL_RATE_ID`),
  KEY `EPS_PROPOSAL_RATE_FK2` (`BUDGET_HEADER_ID`),
  KEY `FK3t8ixuavb1ep1n2i4cbdu5nb0` (`ACTIVITY_TYPE_CODE`),
  KEY `FK76sd9fip7pq3agis9fatepq1u` (`RATE_CLASS_CODE`,`RATE_TYPE_CODE`),
  CONSTRAINT `EPS_PROPOSAL_RATE_FK1` FOREIGN KEY (`ACTIVITY_TYPE_CODE`) REFERENCES `activity_type` (`ACTIVITY_TYPE_CODE`),
  CONSTRAINT `EPS_PROPOSAL_RATE_FK2` FOREIGN KEY (`BUDGET_HEADER_ID`) REFERENCES `budget_header` (`BUDGET_HEADER_ID`),
  CONSTRAINT `EPS_PROPOSAL_RATE_FK3` FOREIGN KEY (`RATE_CLASS_CODE`) REFERENCES `rate_class` (`RATE_CLASS_CODE`),
  CONSTRAINT `EPS_PROPOSAL_RATE_FK4` FOREIGN KEY (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`) REFERENCES `rate_type` (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_resrch_areas` (
  `RESRCH_AREA_ID` int NOT NULL AUTO_INCREMENT,
  `EXCELLENCE_AREA_CODE` int DEFAULT NULL,
  `RESEARCH_AREA_CODE` varchar(3) DEFAULT NULL,
  `RESRCH_TYPE_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `PROPOSAL_ID` int DEFAULT NULL,
  `RESRCH_TYPE_AREA_CODE` varchar(255) DEFAULT NULL,
  `RESRCH_TYPE_SUB_AREA_CODE` varchar(255) DEFAULT NULL,
  `RESEARCH_SUB_AREA_CODE` varchar(255) DEFAULT NULL,
  `CHALLENGE_AREA_CODE` varchar(255) DEFAULT NULL,
  `CHALLENGE_SUBAREA_CODE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`RESRCH_AREA_ID`),
  KEY `EPS_PROP_RESRCH_AREA_FK1` (`PROPOSAL_ID`),
  KEY `EPS_PROP_RESRCH_AREA_FK2` (`RESRCH_TYPE_CODE`),
  KEY `EPS_PROP_RESRCH_AREA_FK3` (`RESRCH_TYPE_AREA_CODE`),
  KEY `EPS_PROP_RESRCH_AREA_FK4` (`RESRCH_TYPE_SUB_AREA_CODE`),
  KEY `EPS_PROP_RESRCH_AREA_FK5` (`CHALLENGE_AREA_CODE`),
  KEY `EPS_PROP_RESRCH_AREA_FK6` (`CHALLENGE_SUBAREA_CODE`),
  CONSTRAINT `EPS_PROP_RESRCH_AREA_FK1` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `eps_proposal` (`PROPOSAL_ID`),
  CONSTRAINT `EPS_PROP_RESRCH_AREA_FK2` FOREIGN KEY (`RESRCH_TYPE_CODE`) REFERENCES `research_type` (`RESRCH_TYPE_CODE`),
  CONSTRAINT `EPS_PROP_RESRCH_AREA_FK3` FOREIGN KEY (`RESRCH_TYPE_AREA_CODE`) REFERENCES `research_type_area` (`RESRCH_TYPE_AREA_CODE`),
  CONSTRAINT `EPS_PROP_RESRCH_AREA_FK4` FOREIGN KEY (`RESRCH_TYPE_SUB_AREA_CODE`) REFERENCES `research_type_sub_area` (`RESRCH_TYPE_SUB_AREA_CODE`),
  CONSTRAINT `EPS_PROP_RESRCH_AREA_FK5` FOREIGN KEY (`CHALLENGE_AREA_CODE`) REFERENCES `societal_challenge_area` (`CHALLENGE_AREA_CODE`),
  CONSTRAINT `EPS_PROP_RESRCH_AREA_FK6` FOREIGN KEY (`CHALLENGE_SUBAREA_CODE`) REFERENCES `societal_challenge_subarea` (`CHALLENGE_SUBAREA_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_resrch_type` (
  `RESRCH_TYPE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`RESRCH_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_special_review` (
  `PROPOSAL_SPECIAL_REVIEW_ID` int NOT NULL AUTO_INCREMENT,
  `APPLICATION_DATE` datetime DEFAULT NULL,
  `APPROVAL_DATE` datetime DEFAULT NULL,
  `APPROVAL_TYPE_CODE` varchar(3) DEFAULT NULL,
  `COMMENTS` longtext,
  `EXPIRATION_DATE` datetime DEFAULT NULL,
  `PROTOCOL_NUMBER` varchar(30) DEFAULT NULL,
  `PROTOCOL_STATUS_DESCRIPTION` varchar(200) DEFAULT NULL,
  `SPECIAL_REVIEW_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `PROPOSAL_ID` int DEFAULT NULL,
  `IS_INTEGRATED_PROTOCOL` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`PROPOSAL_SPECIAL_REVIEW_ID`),
  KEY `EPS_PROP_SPL_REVIEW_FK3` (`APPROVAL_TYPE_CODE`),
  KEY `EPS_PROP_SPL_REVIEW_FK1` (`PROPOSAL_ID`),
  KEY `EPS_PROP_SPL_REVIEW_FK2` (`SPECIAL_REVIEW_CODE`),
  CONSTRAINT `EPS_PROP_SPL_REVIEW_FK1` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `eps_proposal` (`PROPOSAL_ID`),
  CONSTRAINT `EPS_PROP_SPL_REVIEW_FK2` FOREIGN KEY (`SPECIAL_REVIEW_CODE`) REFERENCES `special_review` (`SPECIAL_REVIEW_CODE`),
  CONSTRAINT `EPS_PROP_SPL_REVIEW_FK3` FOREIGN KEY (`APPROVAL_TYPE_CODE`) REFERENCES `sp_rev_approval_type` (`APPROVAL_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_sponsors` (
  `SPONSOR_ID` int NOT NULL AUTO_INCREMENT,
  `AMOUNT` decimal(12,2) DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `SPONSOR_CODE` varchar(20) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `PROPOSAL_ID` int DEFAULT NULL,
  `FULL_NAME` varchar(255) DEFAULT NULL,
  `FUNDING_STATUS_CODE` int DEFAULT NULL,
  `SPONSOR_NAME` varchar(255) DEFAULT NULL,
  `SPONSOR_TYPE_CODE` varchar(255) DEFAULT NULL,
  `CURRENCY_CODE` varchar(255) DEFAULT NULL,
  `GRANT_CALL_NAME` varchar(255) DEFAULT NULL,
  `PERCENTAGE_OF_EFFORT` decimal(19,2) DEFAULT NULL,
  `PROP_PERSON_ROLE_ID` int DEFAULT NULL,
  `PROJECT_TITLE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SPONSOR_ID`),
  KEY `EPS_PROPOSAL_SPONSORS_FK1` (`PROPOSAL_ID`),
  KEY `EPS_PROPOSAL_SPONSORS_FK2` (`SPONSOR_CODE`),
  KEY `EPS_PROPOSAL_SPONSORS_FK3` (`FUNDING_STATUS_CODE`),
  KEY `EPS_PROPOSAL_SPONSORS_FK4` (`SPONSOR_TYPE_CODE`),
  KEY `EPS_PROPOSAL_SPONSORS_FK6` (`CURRENCY_CODE`),
  KEY `EPS_PROPOSAL_SPONSORS_FK5` (`PROP_PERSON_ROLE_ID`),
  CONSTRAINT `EPS_PROPOSAL_SPONSORS_FK1` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `eps_proposal` (`PROPOSAL_ID`),
  CONSTRAINT `EPS_PROPOSAL_SPONSORS_FK2` FOREIGN KEY (`SPONSOR_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`),
  CONSTRAINT `EPS_PROPOSAL_SPONSORS_FK3` FOREIGN KEY (`FUNDING_STATUS_CODE`) REFERENCES `proposal_funding_status` (`FUNDING_STATUS_CODE`),
  CONSTRAINT `EPS_PROPOSAL_SPONSORS_FK4` FOREIGN KEY (`SPONSOR_TYPE_CODE`) REFERENCES `sponsor_type` (`SPONSOR_TYPE_CODE`),
  CONSTRAINT `EPS_PROPOSAL_SPONSORS_FK5` FOREIGN KEY (`PROP_PERSON_ROLE_ID`) REFERENCES `eps_prop_person_role` (`PROP_PERSON_ROLE_ID`),
  CONSTRAINT `EPS_PROPOSAL_SPONSORS_FK6` FOREIGN KEY (`CURRENCY_CODE`) REFERENCES `currency` (`CURRENCY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_status` (
  `STATUS_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_proposal_type` (
  `TYPE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `CAN_MERGE_TO_IP` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `eps_reviewers` (
  `EPS_REVIEWERS_ID` int NOT NULL,
  `PRE_REVIEW_TYPE_CODE` varchar(3) DEFAULT NULL,
  `REVIEWER_PERSON_ID` varchar(40) DEFAULT NULL,
  `REVIEWER_FULLNAME` varchar(90) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `REVIEWER_EMAIL` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`EPS_REVIEWERS_ID`),
  KEY `EPS_REVIEWERS_FK1` (`PRE_REVIEW_TYPE_CODE`),
  CONSTRAINT `EPS_REVIEWERS_FK1` FOREIGN KEY (`PRE_REVIEW_TYPE_CODE`) REFERENCES `pre_review_type` (`PRE_REVIEW_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `era_proposal_person_roles` (
  `PERSON_ROLE_ID` int NOT NULL AUTO_INCREMENT,
  `FULL_NAME` varchar(255) DEFAULT NULL,
  `PERSON_ID` varchar(255) DEFAULT NULL,
  `ROLE_ID` varchar(255) DEFAULT NULL,
  `ROLE_NAME` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `PROPOSAL_ID` int NOT NULL,
  PRIMARY KEY (`PERSON_ROLE_ID`),
  KEY `FK1_ERA_PROPOSAL_PERSON_ROLES` (`PROPOSAL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `error_details` (
  `TICKET_NO` int NOT NULL,
  `ERROR_MESSAGE` varchar(255) DEFAULT NULL,
  `REQUEST_BODY` varchar(255) DEFAULT NULL,
  `STACKTRACE_TXT` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`TICKET_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `evaluation_recommendation` (
  `EVALUATION_RECOMMENDATION_CODE` int NOT NULL,
  `DESCRIPTION` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`EVALUATION_RECOMMENDATION_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `excellence_area` (
  `EXCELLENCE_AREA_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`EXCELLENCE_AREA_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `exception_log` (
  `ERROR_ID` int NOT NULL AUTO_INCREMENT,
  `ERROR_CODE` varchar(500) DEFAULT NULL,
  `MESSAGE` varchar(1000) DEFAULT NULL,
  `API_REQUEST` varchar(500) DEFAULT NULL,
  `METHOD` varchar(20) DEFAULT NULL,
  `REQUEST_BODY` varchar(4000) DEFAULT NULL,
  `DEBUG_MESSSAGE` varchar(1000) DEFAULT NULL,
  `STACKTRACE` mediumtext,
  `REQUESTER_PERSON` varchar(50) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ERROR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `expense_tracker_rt` (
  `EXPENSE_TRACKER_ID` int NOT NULL AUTO_INCREMENT,
  `AC` varchar(255) DEFAULT NULL,
  `AMOUNT_IN_FMA_CURRENCY` varchar(255) DEFAULT NULL,
  `ASSET` varchar(255) DEFAULT NULL,
  `ASSET_COST_CENTER` varchar(255) DEFAULT NULL,
  `ASSET_INTERNAL_ORDER` varchar(255) DEFAULT NULL,
  `ASSET_LOCATION_CODE` varchar(255) DEFAULT NULL,
  `ASSET_LOCATION_DESC` varchar(255) DEFAULT NULL,
  `BANK_CLEARING_DATE` varchar(255) DEFAULT NULL,
  `COMMITMENT_ITEM` varchar(255) DEFAULT NULL,
  `DOCUMENT_DATE` varchar(255) DEFAULT NULL,
  `FI_GL_ACCOUNT` varchar(255) DEFAULT NULL,
  `FI_GL_DESCRIPTION` varchar(255) DEFAULT NULL,
  `FI_PO_COST_CENTRE` varchar(255) DEFAULT NULL,
  `FI_POSTING_DATE` varchar(255) DEFAULT NULL,
  `FI_REFERENCE` varchar(255) DEFAULT NULL,
  `FM_DOC_NUMBER` varchar(255) DEFAULT NULL,
  `FM_POSTING_DATE` varchar(255) DEFAULT NULL,
  `FUND` varchar(255) DEFAULT NULL,
  `FUND_CENTRE` varchar(255) DEFAULT NULL,
  `INTERNAL_ORDER_CODE` varchar(255) DEFAULT NULL,
  `PO_DATE` varchar(255) DEFAULT NULL,
  `PO_GL_ACCOUNT` varchar(255) DEFAULT NULL,
  `PO_GL_DESCRIPTION` varchar(255) DEFAULT NULL,
  `PO_NUMBER` varchar(255) DEFAULT NULL,
  `PREDECESSOR_DOC_CAT` varchar(255) DEFAULT NULL,
  `PREDECESSOR_DOC_CAT_TEXT` varchar(255) DEFAULT NULL,
  `PREDECESSOR_DOC_ITEM` varchar(255) DEFAULT NULL,
  `PREDECESSOR_DOC_ITEM_ACCOUNT` varchar(255) DEFAULT NULL,
  `PREDECESSOR_DOC_ITEM_ORG_UNIT` varchar(255) DEFAULT NULL,
  `PREDECESSOR_DOC_NUMBER` varchar(255) DEFAULT NULL,
  `REMARKS` varchar(255) DEFAULT NULL,
  `SUBNUMBER` varchar(255) DEFAULT NULL,
  `TC_AMOUNT` varchar(255) DEFAULT NULL,
  `TRANSACTION_CURRENCY` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `VENDOR_CLEARING_DATE` varchar(255) DEFAULT NULL,
  `VENDOR_CODE` varchar(255) DEFAULT NULL,
  `VENDOR_NAME_1` varchar(255) DEFAULT NULL,
  `VENDOR_NAME_2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`EXPENSE_TRACKER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_rev_comment_attachment` (
  `EXT_REV_COMMENT_ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `EXT_REVIEW_ID` int DEFAULT NULL,
  `DESCRIPTION` longtext,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `ATTACHMENT_TYPE_CODE` int DEFAULT NULL,
  `CONTENT_TYPE` varchar(255) DEFAULT NULL,
  `FILE_DATA_ID` varchar(40) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`EXT_REV_COMMENT_ATTACHMENT_ID`),
  KEY `EXT_REV_COMMENT_ATTACHMENT_FK_01` (`EXT_REVIEW_ID`),
  KEY `EXT_REV_COMMENT_ATTACHMENT_FK_02` (`ATTACHMENT_TYPE_CODE`),
  CONSTRAINT `EXT_REV_COMMENT_ATTACHMENT_FK_01` FOREIGN KEY (`EXT_REVIEW_ID`) REFERENCES `ext_review` (`EXT_REVIEW_ID`),
  CONSTRAINT `EXT_REV_COMMENT_ATTACHMENT_FK_02` FOREIGN KEY (`ATTACHMENT_TYPE_CODE`) REFERENCES `ext_review_attachment_type` (`ATTACHMENT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_rev_reviewer_flow_status` (
  `EXT_REV_REVIEWER_FLOW_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`EXT_REV_REVIEWER_FLOW_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_rev_reviewer_score` (
  `EXT_REV_REVIEWER_SCORE_ID` int NOT NULL AUTO_INCREMENT,
  `REVIEW_REVIEWER_ID` int DEFAULT NULL,
  `SCORE` decimal(4,2) DEFAULT NULL,
  `EXT_REVIEW_SCORING_CRITERIA_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`EXT_REV_REVIEWER_SCORE_ID`),
  KEY `EXT_REV_REVIEWER_SCORE_FK1` (`EXT_REVIEW_SCORING_CRITERIA_ID`),
  KEY `EXT_REV_REVIEWER_SCORE_FK2` (`REVIEW_REVIEWER_ID`),
  CONSTRAINT `EXT_REV_REVIEWER_SCORE_FK1` FOREIGN KEY (`EXT_REVIEW_SCORING_CRITERIA_ID`) REFERENCES `ext_review_scoring_criteria` (`EXT_REVIEW_SCORING_CRITERIA_ID`),
  CONSTRAINT `EXT_REV_REVIEWER_SCORE_FK2` FOREIGN KEY (`REVIEW_REVIEWER_ID`) REFERENCES `ext_review_reviewers` (`REVIEW_REVIEWER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_rev_reviewer_score_comment` (
  `EXT_REV_REVIEWER_SCORE_COMMENT_ID` int NOT NULL AUTO_INCREMENT,
  `COMMENT` varchar(4000) DEFAULT NULL,
  `EXT_REV_REVIEWER_SCORE_ID` int NOT NULL,
  `IS_PRIVATE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`EXT_REV_REVIEWER_SCORE_COMMENT_ID`),
  KEY `EXT_REV_REVIEWER_SCORE_COMMENT_FK1` (`EXT_REV_REVIEWER_SCORE_ID`),
  CONSTRAINT `EXT_REV_REVIEWER_SCORE_COMMENT_FK1` FOREIGN KEY (`EXT_REV_REVIEWER_SCORE_ID`) REFERENCES `ext_rev_reviewer_score` (`EXT_REV_REVIEWER_SCORE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_rev_revwr_score_commnt_attchmnt` (
  `EXT_REV_REVWR_SCORE_COMMNT_ATTCHMNT_ID` int NOT NULL AUTO_INCREMENT,
  `EXT_REV_REVIEWER_SCORE_COMMENT_ID` int NOT NULL,
  `DESCRIPTION` longtext,
  `FILE_NAME` varchar(200) DEFAULT NULL,
  `ATTACHMENT_TYPE_CODE` int DEFAULT NULL,
  `FILE_DATA_ID` varchar(40) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `CONTENT_TYPE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`EXT_REV_REVWR_SCORE_COMMNT_ATTCHMNT_ID`),
  KEY `EXT_REV_REVWR_SCORE_COMMNT_ATTCHMNT_FK_01` (`EXT_REV_REVIEWER_SCORE_COMMENT_ID`),
  KEY `EXT_REV_REVWR_SCORE_COMMNT_ATTCHMNT_FK_02` (`ATTACHMENT_TYPE_CODE`),
  KEY `EXT_REV_REVWR_SCORE_COMMNT_ATTCHMNT_FK_03` (`FILE_DATA_ID`),
  CONSTRAINT `EXT_REV_REVWR_SCORE_COMMNT_ATTCHMNT_FK_01` FOREIGN KEY (`EXT_REV_REVIEWER_SCORE_COMMENT_ID`) REFERENCES `ext_rev_reviewer_score_comment` (`EXT_REV_REVIEWER_SCORE_COMMENT_ID`),
  CONSTRAINT `EXT_REV_REVWR_SCORE_COMMNT_ATTCHMNT_FK_02` FOREIGN KEY (`ATTACHMENT_TYPE_CODE`) REFERENCES `ext_review_attachment_type` (`ATTACHMENT_TYPE_CODE`),
  CONSTRAINT `EXT_REV_REVWR_SCORE_COMMNT_ATTCHMNT_FK_03` FOREIGN KEY (`FILE_DATA_ID`) REFERENCES `ext_review_attachment_file` (`FILE_DATA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_review` (
  `EXT_REVIEW_ID` int NOT NULL AUTO_INCREMENT,
  `FIBI_REVIEW_ID` int DEFAULT NULL,
  `MODULE_ITEM_CODE` int DEFAULT NULL,
  `MODULE_SUB_ITEM_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(20) DEFAULT NULL,
  `MODULE_SUB_ITEM_KEY` varchar(20) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `EXT_REVIEW_SERVICE_TYPE_CODE` int DEFAULT NULL,
  `EXT_REVIEW_STATUS_CODE` int DEFAULT NULL,
  `REVIEW_REQUESTOR_ID` varchar(40) DEFAULT NULL,
  `REQUEST_DATE` datetime DEFAULT NULL,
  `DEADLINE_DATE` datetime DEFAULT NULL,
  `COMPLETION_DATE` datetime DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `REVIEW_MODULE_CODE` int DEFAULT NULL,
  `PRE_PROPOSAL_EXT_REVIEW_ID` int DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  PRIMARY KEY (`EXT_REVIEW_ID`),
  KEY `EXT_REVIEW_FK_01_idx` (`EXT_REVIEW_SERVICE_TYPE_CODE`),
  KEY `EXT_REVIEW_FK_02_idx` (`EXT_REVIEW_STATUS_CODE`),
  KEY `EXT_REVIEW_FK_03_idx` (`REVIEW_REQUESTOR_ID`),
  CONSTRAINT `EXT_REVIEW_FK_01` FOREIGN KEY (`EXT_REVIEW_SERVICE_TYPE_CODE`) REFERENCES `ext_review_service_type` (`EXT_REVIEW_SERVICE_TYPE_CODE`),
  CONSTRAINT `EXT_REVIEW_FK_02` FOREIGN KEY (`EXT_REVIEW_STATUS_CODE`) REFERENCES `ext_review_status` (`EXT_REVIEW_STATUS_CODE`),
  CONSTRAINT `EXT_REVIEW_FK_03` FOREIGN KEY (`REVIEW_REQUESTOR_ID`) REFERENCES `person` (`PERSON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_review_action_type` (
  `EXT_REVIEW_ACTION_TYPE_CODE` int NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `MESSAGE` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`EXT_REVIEW_ACTION_TYPE_CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_review_attachment_file` (
  `FILE_DATA_ID` varchar(40) NOT NULL,
  `ATTACHMENT` longblob NOT NULL,
  PRIMARY KEY (`FILE_DATA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_review_attachment_type` (
  `ATTACHMENT_TYPE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ATTACHMENT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_review_attachments` (
  `EXT_REVIEW_ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `EXT_REVIEW_ID` int DEFAULT NULL,
  `DESCRIPTION` longtext,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `ATTACHMENT_TYPE_CODE` int DEFAULT NULL,
  `FILE_DATA_ID` varchar(40) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ATTACHMENT_MANDATORY` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`EXT_REVIEW_ATTACHMENT_ID`),
  KEY `EXT_REVIEW_ATTACH_FK_01_idx` (`EXT_REVIEW_ID`),
  KEY `EXT_REVIEW_ATTACH_FK_01_idx1` (`ATTACHMENT_TYPE_CODE`),
  KEY `EXT_REVIEW_ATTACH_FK_03_idx` (`FILE_DATA_ID`),
  CONSTRAINT `EXT_REVIEW_ATTACH_FK_01` FOREIGN KEY (`EXT_REVIEW_ID`) REFERENCES `ext_review` (`EXT_REVIEW_ID`),
  CONSTRAINT `EXT_REVIEW_ATTACH_FK_02` FOREIGN KEY (`ATTACHMENT_TYPE_CODE`) REFERENCES `ext_review_attachment_type` (`ATTACHMENT_TYPE_CODE`),
  CONSTRAINT `EXT_REVIEW_ATTACH_FK_03` FOREIGN KEY (`FILE_DATA_ID`) REFERENCES `ext_review_attachment_file` (`FILE_DATA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_review_comment_type` (
  `EXT_REVIEW_COMMENT_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`EXT_REVIEW_COMMENT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_review_comments` (
  `EXT_REVIEW_COMMENT_ID` int NOT NULL AUTO_INCREMENT,
  `REVIEW_REVIEWER_ID` int DEFAULT NULL,
  `EXT_REVIEW_ID` int DEFAULT NULL,
  `REVIEW_COMMENT` varchar(400) DEFAULT NULL,
  `COMMENT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `IS_PRIVATE_COMMENT` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `ADMIN_USER_ID` int DEFAULT NULL,
  `EXT_REV_COMMENT_ATTACHMENT_ID` int DEFAULT NULL,
  PRIMARY KEY (`EXT_REVIEW_COMMENT_ID`),
  KEY `EXT_REVIEW_COMMENTS_FK_01` (`REVIEW_REVIEWER_ID`),
  KEY `EXT_REVIEW_COMMENTS_FK_02` (`EXT_REVIEW_ID`),
  KEY `EXT_REVIEW_COMMENTS_FK_04` (`COMMENT_TYPE_CODE`),
  KEY `EXT_REVIEW_COMMENTS_FK_03` (`EXT_REV_COMMENT_ATTACHMENT_ID`),
  CONSTRAINT `EXT_REVIEW_COMMENTS_FK_01` FOREIGN KEY (`REVIEW_REVIEWER_ID`) REFERENCES `ext_review_reviewers` (`REVIEW_REVIEWER_ID`),
  CONSTRAINT `EXT_REVIEW_COMMENTS_FK_02` FOREIGN KEY (`EXT_REVIEW_ID`) REFERENCES `ext_review` (`EXT_REVIEW_ID`),
  CONSTRAINT `EXT_REVIEW_COMMENTS_FK_03` FOREIGN KEY (`EXT_REV_COMMENT_ATTACHMENT_ID`) REFERENCES `ext_rev_comment_attachment` (`EXT_REV_COMMENT_ATTACHMENT_ID`),
  CONSTRAINT `EXT_REVIEW_COMMENTS_FK_04` FOREIGN KEY (`COMMENT_TYPE_CODE`) REFERENCES `ext_review_comment_type` (`EXT_REVIEW_COMMENT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_review_history` (
  `EXT_REVIEW_HISTORY_ID` int NOT NULL AUTO_INCREMENT,
  `EXT_REVIEW_ACTION_TYPE_CODE` int DEFAULT NULL,
  `EXT_REVIEW_ID` int DEFAULT NULL,
  `DESCRIPTION` varchar(2000) DEFAULT NULL,
  `EXT_REVIEW_STATUS_CODE` int DEFAULT NULL,
  `EXT_REVIEW_REVIEWERS_STATUS_CODE` int DEFAULT NULL,
  `EXT_REV_REVIEWER_FLOW_STATUS_CODE` varchar(3) DEFAULT NULL,
  `REVIEW_REVIEWER_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`EXT_REVIEW_HISTORY_ID`),
  KEY `EXT_REVIEW_HISTORY_FK_01` (`EXT_REVIEW_ACTION_TYPE_CODE`),
  KEY `EXT_REVIEW_HISTORY_FK_02` (`EXT_REVIEW_ID`),
  KEY `EXT_REVIEW_HISTORY_FK_03` (`EXT_REVIEW_STATUS_CODE`),
  KEY `EXT_REVIEW_HISTORY_FK_04` (`EXT_REVIEW_REVIEWERS_STATUS_CODE`),
  KEY `EXT_REVIEW_HISTORY_FK_05` (`REVIEW_REVIEWER_ID`),
  KEY `EXT_REVIEW_HISTORY_FK_07` (`EXT_REV_REVIEWER_FLOW_STATUS_CODE`),
  CONSTRAINT `EXT_REVIEW_HISTORY_FK_01` FOREIGN KEY (`EXT_REVIEW_ACTION_TYPE_CODE`) REFERENCES `ext_review_action_type` (`EXT_REVIEW_ACTION_TYPE_CODE`),
  CONSTRAINT `EXT_REVIEW_HISTORY_FK_02` FOREIGN KEY (`EXT_REVIEW_ID`) REFERENCES `ext_review` (`EXT_REVIEW_ID`),
  CONSTRAINT `EXT_REVIEW_HISTORY_FK_03` FOREIGN KEY (`EXT_REVIEW_STATUS_CODE`) REFERENCES `ext_review_status` (`EXT_REVIEW_STATUS_CODE`),
  CONSTRAINT `EXT_REVIEW_HISTORY_FK_04` FOREIGN KEY (`EXT_REVIEW_REVIEWERS_STATUS_CODE`) REFERENCES `ext_review_reviewers_status` (`EXT_REVIEW_REVIEWERS_STATUS_CODE`),
  CONSTRAINT `EXT_REVIEW_HISTORY_FK_07` FOREIGN KEY (`EXT_REV_REVIEWER_FLOW_STATUS_CODE`) REFERENCES `ext_rev_reviewer_flow_status` (`EXT_REV_REVIEWER_FLOW_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_review_questionnaire` (
  `EXT_REVIEW_QUESTIONNAIRE_ID` int NOT NULL AUTO_INCREMENT,
  `EXT_REVIEW_ID` int DEFAULT NULL,
  `QUESTIONNAIRE_NUMBER` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`EXT_REVIEW_QUESTIONNAIRE_ID`),
  KEY `EXT_REVIEW_FK_1_idx` (`EXT_REVIEW_ID`),
  CONSTRAINT `EXT_REVIEW_FK_1` FOREIGN KEY (`EXT_REVIEW_ID`) REFERENCES `ext_review` (`EXT_REVIEW_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_review_reviewers` (
  `REVIEW_REVIEWER_ID` int NOT NULL AUTO_INCREMENT,
  `EXTERNAL_REVIEWER_ID` int DEFAULT NULL,
  `EXT_REVIEW_ID` int DEFAULT NULL,
  `EXT_REVIEW_REVIEWERS_STATUS_CODE` int DEFAULT NULL,
  `REVIEW_START_DATE` datetime DEFAULT NULL,
  `EXT_REVIEW_COMMENT_ID` int DEFAULT NULL,
  `REVIEW_REQUESTOR_ID` varchar(60) DEFAULT NULL,
  `REVIEW_COMPLETED_DATE` datetime DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `EXT_REV_REVIEWER_FLOW_STATUS_CODE` varchar(3) DEFAULT NULL,
  `COMMENT` varchar(400) DEFAULT NULL,
  `EXT_REVIEWERS_STATUS_CODE` int DEFAULT NULL,
  PRIMARY KEY (`REVIEW_REVIEWER_ID`),
  KEY `EXT_REVIEW_REVIEWERS_FK_01` (`EXTERNAL_REVIEWER_ID`),
  KEY `EXT_REVIEW_REVIEWERS_FK_02` (`EXT_REVIEW_ID`),
  KEY `EXT_REVIEW_REVIEWERS_FK_03` (`REVIEW_REQUESTOR_ID`),
  KEY `EXT_REVIEW_REVIEWERS_FK_04` (`EXT_REVIEW_COMMENT_ID`),
  KEY `EXT_REVIEW_REVIEWERS_FK_05` (`EXT_REVIEW_REVIEWERS_STATUS_CODE`),
  KEY `EXT_REVIEW_REVIEWERS_FK_06` (`EXT_REV_REVIEWER_FLOW_STATUS_CODE`),
  KEY `EXT_REVIEW_REVIEWERS_FK_07_idx` (`EXT_REVIEWERS_STATUS_CODE`),
  CONSTRAINT `EXT_REVIEW_REVIEWERS_FK_01` FOREIGN KEY (`EXTERNAL_REVIEWER_ID`) REFERENCES `external_reviewer` (`EXTERNAL_REVIEWER_ID`),
  CONSTRAINT `EXT_REVIEW_REVIEWERS_FK_02` FOREIGN KEY (`EXT_REVIEW_ID`) REFERENCES `ext_review` (`EXT_REVIEW_ID`),
  CONSTRAINT `EXT_REVIEW_REVIEWERS_FK_03` FOREIGN KEY (`REVIEW_REQUESTOR_ID`) REFERENCES `person` (`PERSON_ID`),
  CONSTRAINT `EXT_REVIEW_REVIEWERS_FK_04` FOREIGN KEY (`EXT_REVIEW_COMMENT_ID`) REFERENCES `ext_review_comments` (`EXT_REVIEW_COMMENT_ID`),
  CONSTRAINT `EXT_REVIEW_REVIEWERS_FK_05` FOREIGN KEY (`EXT_REVIEW_REVIEWERS_STATUS_CODE`) REFERENCES `ext_review_reviewers_status` (`EXT_REVIEW_REVIEWERS_STATUS_CODE`),
  CONSTRAINT `EXT_REVIEW_REVIEWERS_FK_06` FOREIGN KEY (`EXT_REV_REVIEWER_FLOW_STATUS_CODE`) REFERENCES `ext_rev_reviewer_flow_status` (`EXT_REV_REVIEWER_FLOW_STATUS_CODE`),
  CONSTRAINT `EXT_REVIEW_REVIEWERS_FK_07` FOREIGN KEY (`EXT_REVIEWERS_STATUS_CODE`) REFERENCES `ext_reviewers_status` (`EXT_REVIEWERS_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_review_reviewers_status` (
  `EXT_REVIEW_REVIEWERS_STATUS_CODE` int NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`EXT_REVIEW_REVIEWERS_STATUS_CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_review_scoring_criteria` (
  `EXT_REVIEW_SCORING_CRITERIA_ID` int NOT NULL AUTO_INCREMENT,
  `EXT_REVIEW_ID` int DEFAULT NULL,
  `SCORING_CRITERIA_TYPE_CODE` varchar(6) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`EXT_REVIEW_SCORING_CRITERIA_ID`),
  KEY `EXT_REVIEW_S_CRITERIA_01_idx` (`EXT_REVIEW_ID`),
  KEY `EXT_REVIEW_S_CRITERIA_02_idx` (`SCORING_CRITERIA_TYPE_CODE`),
  CONSTRAINT `EXT_REVIEW_S_C_FK_01` FOREIGN KEY (`EXT_REVIEW_ID`) REFERENCES `ext_review` (`EXT_REVIEW_ID`),
  CONSTRAINT `EXT_REVIEW_S_C_FK_02` FOREIGN KEY (`SCORING_CRITERIA_TYPE_CODE`) REFERENCES `scoring_criteria` (`SCORING_CRITERIA_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_review_service_type` (
  `EXT_REVIEW_SERVICE_TYPE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `IS_SCORING_NEEDED` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`EXT_REVIEW_SERVICE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_review_status` (
  `EXT_REVIEW_STATUS_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`EXT_REVIEW_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_reviewer_academic_area` (
  `ACADEMIC_AREA_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ACADEMIC_AREA_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_reviewer_academic_rank` (
  `ACADEMIC_RANK_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ACADEMIC_RANK_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_reviewer_academic_sub_area` (
  `ACADEMIC_SUB_AREA_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `ACADEMIC_AREA_CODE` varchar(3) NOT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`ACADEMIC_SUB_AREA_CODE`),
  KEY `ACADEMIC_SUB_AREA_FK1` (`ACADEMIC_AREA_CODE`),
  CONSTRAINT `ACADEMIC_SUB_AREA_FK1` FOREIGN KEY (`ACADEMIC_AREA_CODE`) REFERENCES `ext_reviewer_academic_area` (`ACADEMIC_AREA_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_reviewer_affiliation` (
  `AFFILATION_INSTITUITION_CODE` varchar(15) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`AFFILATION_INSTITUITION_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_reviewer_attachment_type` (
  `ATTACHMENT_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ATTACHMENT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_reviewer_cira` (
  `CIRA_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`CIRA_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_reviewer_originality` (
  `ORGINALITY_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ORGINALITY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_reviewer_thoroughness` (
  `THOROUGHNESS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`THOROUGHNESS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_reviewers_status` (
  `EXT_REVIEWERS_STATUS_CODE` int NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`EXT_REVIEWERS_STATUS_CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ext_specialism_keyword` (
  `SPECIALISM_KEYWORD_CODE` varchar(15) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`SPECIALISM_KEYWORD_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `external_reviewer` (
  `EXTERNAL_REVIEWER_ID` int NOT NULL AUTO_INCREMENT,
  `LAST_NAME` varchar(100) DEFAULT NULL,
  `FIRST_NAME` varchar(100) DEFAULT NULL,
  `MIDDLE_NAME` varchar(100) DEFAULT NULL,
  `FULL_NAME` varchar(300) DEFAULT NULL,
  `GENDER` varchar(10) DEFAULT NULL,
  `EMAIL_ADDRESS_PRIMARY` varchar(100) DEFAULT NULL,
  `EMAIL_ADDRESS_SECONDARY` varchar(100) DEFAULT NULL,
  `COUNTRY_CODE` varchar(3) DEFAULT NULL,
  `ACADEMIC_RANK_CODE` varchar(3) DEFAULT NULL,
  `AFFILATION_INSTITUITION_CODE` varchar(15) DEFAULT NULL,
  `IS_TOP_INSTITUITION` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `ACADEMIC_AREA_CODE_PRIMARY` varchar(3) DEFAULT NULL,
  `STATUS` varchar(1) NOT NULL,
  `ACADEMIC_AREA_CODE_SECONDARY` varchar(3) DEFAULT NULL,
  `USER_NAME` varchar(60) DEFAULT NULL,
  `PASSWORD` varchar(400) DEFAULT NULL,
  `AGREEMENT_END_DATE` datetime DEFAULT NULL,
  `AGREEMENT_START_DATE` datetime DEFAULT NULL,
  `ADDITIONAL_INFORMATION` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`EXTERNAL_REVIEWER_ID`),
  KEY `EXTERNAL_REVIEWER_FK2` (`ACADEMIC_RANK_CODE`),
  KEY `EXTERNAL_REVIEWER_FK3` (`AFFILATION_INSTITUITION_CODE`),
  KEY `EXTERNAL_REVIEWER_FK5` (`ACADEMIC_AREA_CODE_PRIMARY`),
  KEY `EXTERNAL_REVIEWER_FK1` (`COUNTRY_CODE`),
  KEY `EXTERNAL_REVIEWER_FK4` (`ACADEMIC_AREA_CODE_SECONDARY`),
  CONSTRAINT `EXTERNAL_REVIEWER_FK1` FOREIGN KEY (`COUNTRY_CODE`) REFERENCES `country` (`COUNTRY_CODE`),
  CONSTRAINT `EXTERNAL_REVIEWER_FK2` FOREIGN KEY (`ACADEMIC_RANK_CODE`) REFERENCES `ext_reviewer_academic_rank` (`ACADEMIC_RANK_CODE`),
  CONSTRAINT `EXTERNAL_REVIEWER_FK3` FOREIGN KEY (`AFFILATION_INSTITUITION_CODE`) REFERENCES `ext_reviewer_affiliation` (`AFFILATION_INSTITUITION_CODE`),
  CONSTRAINT `EXTERNAL_REVIEWER_FK4` FOREIGN KEY (`ACADEMIC_AREA_CODE_SECONDARY`) REFERENCES `ext_reviewer_academic_area` (`ACADEMIC_AREA_CODE`),
  CONSTRAINT `EXTERNAL_REVIEWER_FK5` FOREIGN KEY (`ACADEMIC_AREA_CODE_PRIMARY`) REFERENCES `ext_reviewer_academic_area` (`ACADEMIC_AREA_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `external_reviewer_attachments` (
  `EXTERNAL_REVIEWER_ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `ATTACHMENT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `FILE_DATA_ID` varchar(255) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `MIME_TYPE` varchar(255) DEFAULT NULL,
  `EXTERNAL_REVIEWER_ID` int NOT NULL,
  PRIMARY KEY (`EXTERNAL_REVIEWER_ATTACHMENT_ID`),
  KEY `EXTERNAL_REVIEWER_ATTACHMENTS_FK1` (`ATTACHMENT_TYPE_CODE`),
  KEY `EXTERNAL_REVIEWER_ATTACHMENTS_FK2` (`EXTERNAL_REVIEWER_ID`),
  CONSTRAINT `EXTERNAL_REVIEWER_ATTACHMENTS_FK1` FOREIGN KEY (`ATTACHMENT_TYPE_CODE`) REFERENCES `ext_reviewer_attachment_type` (`ATTACHMENT_TYPE_CODE`),
  CONSTRAINT `EXTERNAL_REVIEWER_ATTACHMENTS_FK2` FOREIGN KEY (`EXTERNAL_REVIEWER_ID`) REFERENCES `external_reviewer` (`EXTERNAL_REVIEWER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `external_reviewer_attachments_file` (
  `ID` varchar(40) NOT NULL,
  `DATA` longblob,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `external_reviewer_ext` (
  `EXTERNAL_REVIEWER_EXT_ID` int NOT NULL AUTO_INCREMENT,
  `EXTERNAL_REVIEWER_ID` int DEFAULT NULL,
  `HI_INDEX` int DEFAULT NULL,
  `SCOPUS_URL` varchar(1000) DEFAULT NULL,
  `SUPPLIER_DOF` varchar(50) DEFAULT NULL,
  `CIRA_CODE` varchar(3) DEFAULT NULL,
  `ORGINALITY_CODE` varchar(15) DEFAULT NULL,
  `THOROUGHNESS_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`EXTERNAL_REVIEWER_EXT_ID`),
  KEY `EXTERNAL_REVIEWER_EXT_FK2` (`CIRA_CODE`),
  KEY `EXTERNAL_REVIEWER_EXT_FK3` (`ORGINALITY_CODE`),
  KEY `EXTERNAL_REVIEWER_EXT_FK4` (`THOROUGHNESS_CODE`),
  KEY `EXTERNAL_REVIEWER_EXT_FK1` (`EXTERNAL_REVIEWER_ID`),
  CONSTRAINT `EXTERNAL_REVIEWER_EXT_FK1` FOREIGN KEY (`EXTERNAL_REVIEWER_ID`) REFERENCES `external_reviewer` (`EXTERNAL_REVIEWER_ID`),
  CONSTRAINT `EXTERNAL_REVIEWER_EXT_FK2` FOREIGN KEY (`CIRA_CODE`) REFERENCES `ext_reviewer_cira` (`CIRA_CODE`),
  CONSTRAINT `EXTERNAL_REVIEWER_EXT_FK3` FOREIGN KEY (`ORGINALITY_CODE`) REFERENCES `ext_reviewer_originality` (`ORGINALITY_CODE`),
  CONSTRAINT `EXTERNAL_REVIEWER_EXT_FK4` FOREIGN KEY (`THOROUGHNESS_CODE`) REFERENCES `ext_reviewer_thoroughness` (`THOROUGHNESS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `external_reviewer_rights` (
  `PERSON_ROLES_ID` int NOT NULL AUTO_INCREMENT,
  `REVIEWER_RIGHTS_ID` varchar(3) DEFAULT NULL,
  `EXTERNAL_REVIEWER_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`PERSON_ROLES_ID`),
  KEY `EXTERNAL_REVIEWER_RIGHTS_FK1` (`REVIEWER_RIGHTS_ID`),
  KEY `EXTERNAL_REVIEWER_RIGHTS_FK2` (`EXTERNAL_REVIEWER_ID`),
  CONSTRAINT `EXTERNAL_REVIEWER_RIGHTS_FK1` FOREIGN KEY (`REVIEWER_RIGHTS_ID`) REFERENCES `reviewer_rights` (`REVIEWER_RIGHTS_ID`),
  CONSTRAINT `EXTERNAL_REVIEWER_RIGHTS_FK2` FOREIGN KEY (`EXTERNAL_REVIEWER_ID`) REFERENCES `external_reviewer` (`EXTERNAL_REVIEWER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `external_reviewer_specialization` (
  `EXTERNAL_REVIEWER_SPECIALIZATION_ID` int NOT NULL AUTO_INCREMENT,
  `SPECIALIZATION_CODE` varchar(15) DEFAULT NULL,
  `EXTERNAL_REVIEWER_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`EXTERNAL_REVIEWER_SPECIALIZATION_ID`),
  KEY `EXTERNAL_REVIEWER_SPECIALIZATION_FK1` (`SPECIALIZATION_CODE`),
  KEY `EXTERNAL_REVIEWER_SPECIALIZATION_FK2` (`EXTERNAL_REVIEWER_ID`),
  CONSTRAINT `EXTERNAL_REVIEWER_SPECIALIZATION_FK1` FOREIGN KEY (`SPECIALIZATION_CODE`) REFERENCES `science_keyword` (`SCIENCE_KEYWORD_CODE`),
  CONSTRAINT `EXTERNAL_REVIEWER_SPECIALIZATION_FK2` FOREIGN KEY (`EXTERNAL_REVIEWER_ID`) REFERENCES `external_reviewer` (`EXTERNAL_REVIEWER_ID`),
  CONSTRAINT `SPECIALIZATION_FK2` FOREIGN KEY (`SPECIALIZATION_CODE`) REFERENCES `ext_specialism_keyword` (`SPECIALISM_KEYWORD_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `external_user` (
  `PERSON_ID` int NOT NULL AUTO_INCREMENT,
  `FULL_NAME` varchar(100) DEFAULT NULL,
  `USER_NAME` varchar(60) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(60) DEFAULT NULL,
  `FUNDING_OFFICE` varchar(100) DEFAULT NULL,
  `PASSWORD` varchar(400) DEFAULT NULL,
  `ORGANIZATION_ID` varchar(20) DEFAULT NULL,
  `VERIFIED_FLAG` varchar(1) DEFAULT NULL,
  `CREATED_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATED_TIMESTAMP` datetime DEFAULT NULL,
  `VERIFIED_AT` datetime DEFAULT NULL,
  `VERIFIED_BY` varchar(60) DEFAULT NULL,
  `IS_SGAF_USER` varchar(1) DEFAULT NULL,
  `ADMIN_COMMENT` varchar(600) DEFAULT NULL,
  `IS_MAIL_SENT` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`PERSON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `external_user_feed` (
  `EXTERNAL_USER_FEED_ID` int NOT NULL AUTO_INCREMENT,
  `PERSON_ID` varchar(40) NOT NULL,
  `ACTION` varchar(15) DEFAULT NULL,
  `USER_NAME` varchar(60) DEFAULT NULL,
  `FULL_NAME` varchar(100) DEFAULT NULL,
  `PASSWORD` varchar(400) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(60) DEFAULT NULL,
  `ORGANIZATION_NAME` varchar(100) DEFAULT NULL,
  `TYPE` varchar(45) DEFAULT NULL,
  `IS_SENT` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`EXTERNAL_USER_FEED_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `faq` (
  `QUESTION_ID` int NOT NULL,
  `QUESTION` varchar(500) DEFAULT NULL,
  `ANSWER` varchar(4000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  `CATEGORY_CODE` int DEFAULT NULL,
  `SUB_CATEGORY_CODE` int DEFAULT NULL,
  `URL` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`QUESTION_ID`),
  KEY `FAQ_FK1` (`CATEGORY_CODE`),
  KEY `FAQ_FK2` (`SUB_CATEGORY_CODE`),
  CONSTRAINT `FAQ_FK1` FOREIGN KEY (`CATEGORY_CODE`) REFERENCES `faq_category` (`CATEGORY_CODE`),
  CONSTRAINT `FAQ_FK2` FOREIGN KEY (`SUB_CATEGORY_CODE`) REFERENCES `faq_sub_category` (`SUB_CATEGORY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `faq_attachment` (
  `FAQ_ATTACHMENT_ID` int NOT NULL,
  `QUESTION_ID` int DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `MIME_TYPE` varchar(100) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `FILE_DATA_ID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`FAQ_ATTACHMENT_ID`),
  KEY `FAQ_ATTACHMENT_FK` (`QUESTION_ID`),
  CONSTRAINT `FAQ_ATTACHMENT_FK` FOREIGN KEY (`QUESTION_ID`) REFERENCES `faq` (`QUESTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `faq_category` (
  `CATEGORY_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `CATEGORY_TYPE_CODE` int DEFAULT NULL,
  `faq` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CATEGORY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `faq_sub_category` (
  `SUB_CATEGORY_CODE` int NOT NULL,
  `CATEGORY_CODE` int DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`SUB_CATEGORY_CODE`),
  KEY `FAQ_CATEGORY_FK1` (`CATEGORY_CODE`),
  CONSTRAINT `FAQ_CATEGORY_FK1` FOREIGN KEY (`CATEGORY_CODE`) REFERENCES `faq_category` (`CATEGORY_CODE`),
  CONSTRAINT `FAQ_SUB_CATEGORY_FK` FOREIGN KEY (`CATEGORY_CODE`) REFERENCES `faq_category` (`CATEGORY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `feed_award_details` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` int DEFAULT NULL,
  `AWARD_NUMBER` varchar(255) DEFAULT NULL,
  `CREATE_USER` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` varchar(255) DEFAULT NULL,
  `PI_FULL_NAME` varchar(255) DEFAULT NULL,
  `PI_USER_NAME` varchar(255) DEFAULT NULL,
  `SCENARIO` varchar(255) DEFAULT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `UNIT_NAME` varchar(255) DEFAULT NULL,
  `UNIT_NUMBER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `fibi_workflow` (
  `WORKFLOW_ID` int NOT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(255) DEFAULT NULL,
  `IS_WORKFLOW_ACTIVE` varchar(255) DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `MODULE_ITEM_ID` int DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `WORKFLOW_END_DATE` date DEFAULT NULL,
  `WORKFLOW_END_PERSON` varchar(255) DEFAULT NULL,
  `WORKFLOW_SEQUENCE` int DEFAULT NULL,
  `WORKFLOW_START_DATE` date DEFAULT NULL,
  `WORKFLOW_START_PERSON` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`WORKFLOW_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `fibi_workflow_attachment` (
  `ATTACHMENT_ID` int NOT NULL,
  `ATTACHMENT` longblob,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `FILE_NAME` varchar(255) DEFAULT NULL,
  `MIME_TYPE` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `WORKFLOW_DETAIL_ID` int DEFAULT NULL,
  `REVIEWER_DETAILS_ID` int DEFAULT NULL,
  `FILE_DATA_ID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ATTACHMENT_ID`),
  KEY `FK1_FIBI_WORKFLOW_ATTACHMENT` (`WORKFLOW_DETAIL_ID`),
  KEY `FK2_FIBI_WORKFLOW_ATTACHMENT` (`REVIEWER_DETAILS_ID`),
  CONSTRAINT `FK1_FIBI_WORKFLOW_ATTACHMENT` FOREIGN KEY (`WORKFLOW_DETAIL_ID`) REFERENCES `fibi_workflow_detail` (`WORKFLOW_DETAIL_ID`),
  CONSTRAINT `FK2_FIBI_WORKFLOW_ATTACHMENT` FOREIGN KEY (`REVIEWER_DETAILS_ID`) REFERENCES `fibi_workflow_reviewer_detail` (`REVIEWER_DETAILS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `fibi_workflow_detail` (
  `WORKFLOW_DETAIL_ID` int NOT NULL,
  `APPROVAL_COMMENT` varchar(255) DEFAULT NULL,
  `APPROVAL_DATE` date DEFAULT NULL,
  `APPROVAL_STATUS_CODE` varchar(255) DEFAULT NULL,
  `APPROVAL_STOP_NUMBER` int DEFAULT NULL,
  `APPROVER_NUMBER` int DEFAULT NULL,
  `APPROVER_PERSON_ID` varchar(255) DEFAULT NULL,
  `APPROVER_PERSON_NAME` varchar(255) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `MAP_ID` int DEFAULT NULL,
  `MAP_NUMBER` int DEFAULT NULL,
  `PRIMARY_APPROVER_FLAG` varchar(255) DEFAULT NULL,
  `ROLE_TYPE_CODE` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `WORKFLOW_ID` int DEFAULT NULL,
  `FIRST_CRON_EMAIL_FLAG` varchar(255) DEFAULT NULL,
  `SECOND_CRON_EMAIL_FLAG` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`WORKFLOW_DETAIL_ID`),
  KEY `FK1_FIBI_WORKFLOW_DETAIL` (`WORKFLOW_ID`),
  KEY `FK2_FIBI_WORKFLOW_DETAIL` (`MAP_ID`),
  KEY `FK4_FIBI_WORKFLOW_DETAIL` (`ROLE_TYPE_CODE`),
  KEY `FK3_FIBI_WORKFLOW_DETAIL` (`APPROVAL_STATUS_CODE`),
  CONSTRAINT `FK1_FIBI_WORKFLOW_DETAIL` FOREIGN KEY (`WORKFLOW_ID`) REFERENCES `fibi_workflow` (`WORKFLOW_ID`),
  CONSTRAINT `FK2_FIBI_WORKFLOW_DETAIL` FOREIGN KEY (`MAP_ID`) REFERENCES `fibi_workflow_map` (`MAP_ID`),
  CONSTRAINT `FK3_FIBI_WORKFLOW_DETAIL` FOREIGN KEY (`APPROVAL_STATUS_CODE`) REFERENCES `fibi_workflow_status` (`APPROVAL_STATUS_CODE`),
  CONSTRAINT `FK4_FIBI_WORKFLOW_DETAIL` FOREIGN KEY (`ROLE_TYPE_CODE`) REFERENCES `fibi_workflow_role_type` (`ROLE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `fibi_workflow_map` (
  `MAP_ID` int NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `MAP_TYPE` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`MAP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `fibi_workflow_map_detail` (
  `MAP_DETAIL_ID` int NOT NULL,
  `APPROVAL_STOP_NUMBER` int DEFAULT NULL,
  `APPROVER_NUMBER` int DEFAULT NULL,
  `APPROVER_PERSON_ID` varchar(255) DEFAULT NULL,
  `APPROVER_PERSON_NAME` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `IS_ROLE` varchar(255) DEFAULT NULL,
  `MAP_ID` int DEFAULT NULL,
  `PRIMARY_APPROVER_FLAG` varchar(255) DEFAULT NULL,
  `ROLE_TYPE_CODE` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MAP_DETAIL_ID`),
  KEY `FK1_FIBI_WORKFLOW_MAP_DETAIL` (`MAP_ID`),
  KEY `FK2_FIBI_WORKFLOW_MAP_DETAIL` (`ROLE_TYPE_CODE`),
  CONSTRAINT `FK1_FIBI_WORKFLOW_MAP_DETAIL` FOREIGN KEY (`MAP_ID`) REFERENCES `fibi_workflow_map` (`MAP_ID`),
  CONSTRAINT `FK2_FIBI_WORKFLOW_MAP_DETAIL` FOREIGN KEY (`ROLE_TYPE_CODE`) REFERENCES `fibi_workflow_role_type` (`ROLE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `fibi_workflow_reviewer_detail` (
  `REVIEWER_DETAILS_ID` int NOT NULL,
  `APPROVAL_STATUS_CODE` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `REVIEW_DATE` date DEFAULT NULL,
  `REVIEWER_PERSON_ID` varchar(255) DEFAULT NULL,
  `REVIEWER_PERSON_NAME` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `WORKFLOW_DETAIL_ID` int DEFAULT NULL,
  PRIMARY KEY (`REVIEWER_DETAILS_ID`),
  KEY `FK1_FIBI_WRKFLW_REVIWER_DTL` (`WORKFLOW_DETAIL_ID`),
  KEY `FK2_FIBI_WRKFLW_REVIWER_DTL` (`APPROVAL_STATUS_CODE`),
  CONSTRAINT `FK1_FIBI_WRKFLW_REVIWER_DTL` FOREIGN KEY (`WORKFLOW_DETAIL_ID`) REFERENCES `fibi_workflow_detail` (`WORKFLOW_DETAIL_ID`),
  CONSTRAINT `FK2_FIBI_WRKFLW_REVIWER_DTL` FOREIGN KEY (`APPROVAL_STATUS_CODE`) REFERENCES `fibi_workflow_status` (`APPROVAL_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `fibi_workflow_role_type` (
  `ROLE_TYPE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ROLE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `fibi_workflow_status` (
  `APPROVAL_STATUS_CODE` varchar(255) NOT NULL,
  `APPROVAL_STATUS` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`APPROVAL_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `field_section_mapping` (
  `FIELD_CODE` varchar(5) NOT NULL,
  `SECTION_CODE` varchar(3) NOT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`FIELD_CODE`,`SECTION_CODE`),
  KEY `FIELD_SECTION_MAPPING_FK2` (`SECTION_CODE`),
  CONSTRAINT `FIELD_SECTION_MAPPING_FK1` FOREIGN KEY (`FIELD_CODE`) REFERENCES `agreement_field_type` (`FIELD_CODE`),
  CONSTRAINT `FIELD_SECTION_MAPPING_FK2` FOREIGN KEY (`SECTION_CODE`) REFERENCES `agreement_section_type` (`SECTION_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `file_data` (
  `ID` varchar(36) NOT NULL,
  `DATA` longblob,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `file_type` (
  `FILE_TYPE_CODE` varchar(255) NOT NULL,
  `EXTENSION` varchar(255) DEFAULT NULL,
  `FILE_TYPE` varchar(30) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`FILE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `final_evaluation_status` (
  `FINAL_EVALUATION_STATUS_CODE` int NOT NULL,
  `STATUS_CODE` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`FINAL_EVALUATION_STATUS_CODE`),
  KEY `FINAL_EVALUATION_STATUS_FK1` (`STATUS_CODE`),
  CONSTRAINT `FINAL_EVALUATION_STATUS_FK1` FOREIGN KEY (`STATUS_CODE`) REFERENCES `eps_proposal_status` (`STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `frequency` (
  `FREQUENCY_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `NUMBER_OF_DAYS` decimal(3,0) DEFAULT NULL,
  `NUMBER_OF_MONTHS` decimal(2,0) DEFAULT NULL,
  `REPEAT_FLAG` char(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` date DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `ADVANCE_NUMBER_OF_DAYS` decimal(3,0) DEFAULT NULL,
  `ADVANCE_NUMBER_OF_MONTHS` decimal(2,0) DEFAULT NULL,
  `ACTIVE_FLAG` varchar(255) DEFAULT NULL,
  `SORT_ORDER` int DEFAULT NULL,
  `IS_INVOICE_FREQUENCY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`FREQUENCY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `frequency_base` (
  `FREQUENCY_BASE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` date DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `ACTIVE_FLAG` varchar(255) DEFAULT NULL,
  `REGENERATION_TYPE_NAME` varchar(255) DEFAULT NULL,
  `SORT_ORDER` int DEFAULT NULL,
  `INCLUDE_BASE_DATE` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`FREQUENCY_BASE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `fund_disbursement_basis_type` (
  `FUND_DISBURSEMENT_BASIS_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`FUND_DISBURSEMENT_BASIS_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `funding_scheme` (
  `FUNDING_SCHEME_CODE` varchar(8) NOT NULL,
  `DESCRIPTION` varchar(1000) DEFAULT NULL,
  `SCHEME_NAME` varchar(200) DEFAULT NULL,
  `REPORTING_PERIOD_TYPE` varchar(5) DEFAULT NULL,
  `ACTIVE_FLAG` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`FUNDING_SCHEME_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `funding_scheme_attachment` (
  `FUNDING_SCHEME_ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `FILE_DATA_ID` varchar(255) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `GRANT_ATTACHMNT_TYPE_CODE` int DEFAULT NULL,
  `MIME_TYPE` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `FUNDING_SCHEME_ID` int DEFAULT NULL,
  PRIMARY KEY (`FUNDING_SCHEME_ATTACHMENT_ID`),
  KEY `FUNDING_SCHEME_ATTACHMENT_FK2` (`GRANT_ATTACHMNT_TYPE_CODE`),
  KEY `FUNDING_SCHEME_ATTACHMENT_FK1` (`FUNDING_SCHEME_ID`),
  CONSTRAINT `FUNDING_SCHEME_ATTACHMENT_FK1` FOREIGN KEY (`FUNDING_SCHEME_ID`) REFERENCES `sponsor_funding_scheme` (`FUNDING_SCHEME_ID`),
  CONSTRAINT `FUNDING_SCHEME_ATTACHMENT_FK2` FOREIGN KEY (`GRANT_ATTACHMNT_TYPE_CODE`) REFERENCES `grant_call_attach_type` (`GRANT_ATTACHMNT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `funding_source_type` (
  `FUNDING_SOURCE_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `FUNDING_SOURCE_TYPE_FLAG` varchar(1) DEFAULT NULL,
  `SPONSOR_CODE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`FUNDING_SOURCE_TYPE_CODE`),
  KEY `FUNDING_SOURCE_TYPE_FK1` (`SPONSOR_CODE`),
  CONSTRAINT `FUNDING_SOURCE_TYPE_FK1` FOREIGN KEY (`SPONSOR_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_call_attach_type` (
  `GRANT_ATTACHMNT_TYPE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`GRANT_ATTACHMNT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_call_attachments` (
  `ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `GRANT_HEADER_ID` int DEFAULT NULL,
  `GRANT_ATTACHMNT_TYPE_CODE` int DEFAULT NULL,
  `DESCRIPTION` varchar(2000) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `ATTACHMENT` blob,
  `MIME_TYPE` varchar(100) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `FILE_DATA_ID` varchar(36) DEFAULT NULL,
  `DOCUMENT_STATUS_CODE` int DEFAULT NULL,
  `DOCUMENTSTATUSCODE` int DEFAULT NULL,
  `DOCUMENT_ID` int DEFAULT NULL,
  PRIMARY KEY (`ATTACHMENT_ID`),
  KEY `GRANT_CALL_ATTACHMNTS_FK1` (`GRANT_HEADER_ID`),
  KEY `GRANT_CALL_ATTACHMNTS_FK2` (`GRANT_ATTACHMNT_TYPE_CODE`),
  KEY `GRANT_CALL_ATTACHMNTS_FK4` (`DOCUMENT_STATUS_CODE`),
  KEY `GRANT_CALL_ATTACHMNTS_FK3` (`FILE_DATA_ID`),
  CONSTRAINT `GRANT_CALL_ATTACHMNTS_FK1` FOREIGN KEY (`GRANT_HEADER_ID`) REFERENCES `grant_call_header` (`GRANT_HEADER_ID`),
  CONSTRAINT `GRANT_CALL_ATTACHMNTS_FK2` FOREIGN KEY (`GRANT_ATTACHMNT_TYPE_CODE`) REFERENCES `grant_call_attach_type` (`GRANT_ATTACHMNT_TYPE_CODE`),
  CONSTRAINT `GRANT_CALL_ATTACHMNTS_FK4` FOREIGN KEY (`DOCUMENT_STATUS_CODE`) REFERENCES `document_status` (`DOCUMENT_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_call_category` (
  `GRANT_CALL_CATEGORY_CODE` int NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`GRANT_CALL_CATEGORY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_call_contacts` (
  `GRANT_CONTACT_ID` int NOT NULL AUTO_INCREMENT,
  `GRANT_HEADER_ID` int DEFAULT NULL,
  `PERSON_ID` varchar(90) DEFAULT NULL,
  `FULL_NAME` varchar(90) DEFAULT NULL,
  `DESIGNATION` varchar(100) DEFAULT NULL,
  `EMAIL` varchar(200) DEFAULT NULL,
  `MOBILE` varchar(20) DEFAULT NULL,
  `IS_SMU_PERSON` varchar(1) DEFAULT NULL,
  `IS_EMPLOYEE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`GRANT_CONTACT_ID`),
  KEY `GRANT_CALL_CONTACTS_FK1` (`GRANT_HEADER_ID`),
  CONSTRAINT `GRANT_CALL_CONTACTS_FK1` FOREIGN KEY (`GRANT_HEADER_ID`) REFERENCES `grant_call_header` (`GRANT_HEADER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_call_criteria` (
  `GRANT_CRITERIA_CODE` int NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`GRANT_CRITERIA_CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_call_elgiblity_type` (
  `GRANT_ELGBLTY_TYPE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `HAS_TARGET` varchar(255) DEFAULT NULL,
  `MORE_INFORMATION` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`GRANT_ELGBLTY_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_call_eligibility` (
  `GRANT_ELIGIBILITY_ID` int NOT NULL AUTO_INCREMENT,
  `GRANT_HEADER_ID` int DEFAULT NULL,
  `GRANT_CRITERIA_CODE` int DEFAULT NULL,
  `GRANT_ELGBLTY_TYPE_CODE` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `PROP_PERSON_ROLE_ID` int DEFAULT NULL,
  PRIMARY KEY (`GRANT_ELIGIBILITY_ID`),
  KEY `GRANT_CALL_ELGIBILITY_FK1` (`GRANT_HEADER_ID`),
  KEY `GRANT_CALL_ELGIBILITY_FK2` (`GRANT_CRITERIA_CODE`),
  KEY `GRANT_CALL_ELGIBILITY_FK3` (`GRANT_ELGBLTY_TYPE_CODE`),
  CONSTRAINT `GRANT_CALL_ELGIBILITY_FK1` FOREIGN KEY (`GRANT_HEADER_ID`) REFERENCES `grant_call_header` (`GRANT_HEADER_ID`),
  CONSTRAINT `GRANT_CALL_ELGIBILITY_FK2` FOREIGN KEY (`GRANT_CRITERIA_CODE`) REFERENCES `grant_call_criteria` (`GRANT_CRITERIA_CODE`),
  CONSTRAINT `GRANT_CALL_ELGIBILITY_FK3` FOREIGN KEY (`GRANT_ELGBLTY_TYPE_CODE`) REFERENCES `grant_call_elgiblity_type` (`GRANT_ELGBLTY_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_call_eligible_department` (
  `ELIGIBILE_DEPT_ID` int NOT NULL AUTO_INCREMENT,
  `UNIT_NAME` varchar(255) DEFAULT NULL,
  `UNIT_NUMBER` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `GRANT_HEADER_ID` int DEFAULT NULL,
  PRIMARY KEY (`ELIGIBILE_DEPT_ID`),
  KEY `GRANT_CALL_ELIGIBLE_DEPT_FK1` (`GRANT_HEADER_ID`),
  CONSTRAINT `GRANT_CALL_ELIGIBLE_DEPT_FK1` FOREIGN KEY (`GRANT_HEADER_ID`) REFERENCES `grant_call_header` (`GRANT_HEADER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_call_evaluation_panel` (
  `GRANT_CALL_EVALUATION_PANEL_ID` int NOT NULL AUTO_INCREMENT,
  `GRANT_HEADER_ID` int DEFAULT NULL,
  `MAP_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `IS_MAIN_PANEL` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`GRANT_CALL_EVALUATION_PANEL_ID`),
  KEY `GRANT_CALL_EVAL_PANEL_FK1` (`GRANT_HEADER_ID`),
  KEY `GRANT_CALL_EVAL_PANEL_FK2` (`MAP_ID`),
  CONSTRAINT `GRANT_CALL_EVAL_PANEL_FK1` FOREIGN KEY (`GRANT_HEADER_ID`) REFERENCES `grant_call_header` (`GRANT_HEADER_ID`),
  CONSTRAINT `GRANT_CALL_EVAL_PANEL_FK2` FOREIGN KEY (`MAP_ID`) REFERENCES `workflow_map` (`MAP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_call_funding_scheme_managers` (
  `GM_FUNDING_SCHEME_ID` int NOT NULL,
  `FUNDING_SCHEME_CODE` varchar(8) NOT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `DESCRIPTION` varchar(1000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`GM_FUNDING_SCHEME_ID`),
  KEY `GRANT_CALL_FUNDING_SCHEME_MANAGERS_FK1` (`FUNDING_SCHEME_CODE`),
  KEY `GRANT_CALL_FUNDING_SCHEME_MANAGERS_FK2` (`PERSON_ID`),
  CONSTRAINT `GRANT_CALL_FUNDING_SCHEME_MANAGERS_FK1` FOREIGN KEY (`FUNDING_SCHEME_CODE`) REFERENCES `funding_scheme` (`FUNDING_SCHEME_CODE`),
  CONSTRAINT `GRANT_CALL_FUNDING_SCHEME_MANAGERS_FK2` FOREIGN KEY (`PERSON_ID`) REFERENCES `person` (`PERSON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_call_header` (
  `GRANT_HEADER_ID` int NOT NULL AUTO_INCREMENT,
  `GRANT_TYPE_CODE` int DEFAULT NULL,
  `GRANT_STATUS_CODE` int DEFAULT NULL,
  `OPENING_DATE` datetime DEFAULT NULL,
  `CLOSING_DATE` datetime DEFAULT NULL,
  `NAME` varchar(1000) DEFAULT NULL,
  `DESCRIPTION` longtext,
  `MAX_BUDGET` decimal(14,2) DEFAULT NULL,
  `QUANTUM` decimal(14,2) DEFAULT NULL,
  `SPONSOR_CODE` varchar(6) DEFAULT NULL,
  `SPONSOR_TYPE_CODE` varchar(3) DEFAULT NULL,
  `ACTIVITY_TYPE_CODE` varchar(3) DEFAULT NULL,
  `APPLICATION_PROCEDURE` longtext,
  `OTHER_INFORMATION` longtext,
  `EXTERNAL_URL` varchar(1000) DEFAULT NULL,
  `GRANT_THEME` longtext,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `HOME_UNIT_NAME` varchar(60) DEFAULT NULL,
  `HOME_UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `FUNDING_SCHEME` varchar(255) DEFAULT NULL,
  `INTERNAL_DEADLINE_DATE` datetime DEFAULT NULL,
  `COUNTRY_ID` int DEFAULT NULL,
  `INTERNAL_SUBMISSION_DEADLINE_DATE` datetime(6) DEFAULT NULL,
  `KEYWORD` varchar(255) DEFAULT NULL,
  `IS_PUBLISHED` varchar(1) DEFAULT NULL,
  `PUBLISH_TIMESTAMP` datetime DEFAULT NULL,
  `CURRENCY_CODE` varchar(3) DEFAULT NULL,
  `OH_WAIVER_PERCENTAGE` decimal(5,2) DEFAULT NULL,
  `abbrevation` varchar(255) DEFAULT NULL,
  `ABBREVIATION` varchar(255) DEFAULT NULL,
  `FUNDING_SCHEME_ID` int DEFAULT NULL,
  `PRIME_SPONSOR_CODE` varchar(6) DEFAULT NULL,
  `OVERHEAD_COMMENT` varchar(400) DEFAULT NULL,
  `RATE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `RATE_CLASS_CODE` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`GRANT_HEADER_ID`),
  KEY `GRANT_CALL_HEADER_FK1` (`GRANT_TYPE_CODE`),
  KEY `GRANT_CALL_HEADER_FK2` (`GRANT_STATUS_CODE`),
  KEY `GRANT_CALL_HEADER_FK4` (`SPONSOR_TYPE_CODE`),
  KEY `GRANT_CALL_HEADER_FK5` (`ACTIVITY_TYPE_CODE`),
  KEY `GRANT_CALL_HEADER_FK6` (`SPONSOR_CODE`),
  KEY `GRANT_CALL_HEADER_FK7` (`COUNTRY_ID`),
  KEY `GRANT_CALL_HEADER_FK9` (`CURRENCY_CODE`),
  KEY `GRANT_CALL_HEADER_FK3` (`FUNDING_SCHEME_ID`),
  KEY `GRANT_CALL_HEADER_FK11` (`PRIME_SPONSOR_CODE`),
  KEY `GRANT_CALL_HEADER_FK12` (`RATE_CLASS_CODE`,`RATE_TYPE_CODE`),
  CONSTRAINT `GRANT_CALL_HEADER_FK1` FOREIGN KEY (`GRANT_TYPE_CODE`) REFERENCES `grant_call_type` (`GRANT_TYPE_CODE`),
  CONSTRAINT `GRANT_CALL_HEADER_FK11` FOREIGN KEY (`PRIME_SPONSOR_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`),
  CONSTRAINT `GRANT_CALL_HEADER_FK12` FOREIGN KEY (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`) REFERENCES `rate_type` (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`),
  CONSTRAINT `GRANT_CALL_HEADER_FK2` FOREIGN KEY (`GRANT_STATUS_CODE`) REFERENCES `grant_call_status` (`GRANT_STATUS_CODE`),
  CONSTRAINT `GRANT_CALL_HEADER_FK3` FOREIGN KEY (`FUNDING_SCHEME_ID`) REFERENCES `sponsor_funding_scheme` (`FUNDING_SCHEME_ID`),
  CONSTRAINT `GRANT_CALL_HEADER_FK4` FOREIGN KEY (`SPONSOR_TYPE_CODE`) REFERENCES `sponsor_type` (`SPONSOR_TYPE_CODE`),
  CONSTRAINT `GRANT_CALL_HEADER_FK5` FOREIGN KEY (`ACTIVITY_TYPE_CODE`) REFERENCES `activity_type` (`ACTIVITY_TYPE_CODE`),
  CONSTRAINT `GRANT_CALL_HEADER_FK6` FOREIGN KEY (`SPONSOR_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`),
  CONSTRAINT `GRANT_CALL_HEADER_FK9` FOREIGN KEY (`CURRENCY_CODE`) REFERENCES `currency` (`CURRENCY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_call_ioi_header` (
  `IOI_ID` int NOT NULL AUTO_INCREMENT,
  `GRANT_HEADER_ID` int DEFAULT NULL,
  `PROJECT_TITLE` varchar(1000) DEFAULT NULL,
  `REQUESTED_DIRECT_COST` decimal(14,2) DEFAULT NULL,
  `UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `PI_PERSON_ID` varchar(40) DEFAULT NULL,
  `SUBMITING_UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `STATUS_CODE` int DEFAULT NULL,
  `QUESTIONNAIRE_ANSWER_ID` int DEFAULT NULL,
  PRIMARY KEY (`IOI_ID`),
  KEY `GRANT_CALL_IOI_HEADER_FK` (`GRANT_HEADER_ID`),
  KEY `GRANT_CALL_IOI_HEADER_FK3` (`UNIT_NUMBER`),
  KEY `GRANT_CALL_IOI_HEADER_FK4` (`PI_PERSON_ID`),
  KEY `GRANT_CALL_IOI_HEADER_FK2` (`STATUS_CODE`),
  KEY `GRANT_CALL_IOI_HEADER_FK5` (`SUBMITING_UNIT_NUMBER`),
  CONSTRAINT `GRANT_CALL_IOI_HEADER_FK` FOREIGN KEY (`GRANT_HEADER_ID`) REFERENCES `grant_call_header` (`GRANT_HEADER_ID`),
  CONSTRAINT `GRANT_CALL_IOI_HEADER_FK2` FOREIGN KEY (`STATUS_CODE`) REFERENCES `ioi_status` (`STATUS_CODE`),
  CONSTRAINT `GRANT_CALL_IOI_HEADER_FK3` FOREIGN KEY (`UNIT_NUMBER`) REFERENCES `unit` (`UNIT_NUMBER`),
  CONSTRAINT `GRANT_CALL_IOI_HEADER_FK4` FOREIGN KEY (`PI_PERSON_ID`) REFERENCES `person` (`PERSON_ID`),
  CONSTRAINT `GRANT_CALL_IOI_HEADER_FK5` FOREIGN KEY (`SUBMITING_UNIT_NUMBER`) REFERENCES `unit` (`UNIT_NUMBER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_call_ioi_members` (
  `IOI_MEMBER_ID` int NOT NULL AUTO_INCREMENT,
  `IOI_ID` int DEFAULT NULL,
  `IS_NON_EMPLOYEE` varchar(1) DEFAULT NULL,
  `MEMBER_ID` varchar(40) DEFAULT NULL,
  `MEMBER_NAME` varchar(90) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `MEMBER_ROLE` int DEFAULT NULL,
  PRIMARY KEY (`IOI_MEMBER_ID`),
  KEY `GRANT_CALL_IOI_MEMBERS_FK` (`IOI_ID`),
  KEY `GRANT_CALL_IOI_MEMBERS_FK2` (`MEMBER_ROLE`),
  CONSTRAINT `GRANT_CALL_IOI_MEMBERS_FK` FOREIGN KEY (`IOI_ID`) REFERENCES `grant_call_ioi_header` (`IOI_ID`),
  CONSTRAINT `GRANT_CALL_IOI_MEMBERS_FK2` FOREIGN KEY (`MEMBER_ROLE`) REFERENCES `eps_prop_person_role` (`PROP_PERSON_ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_call_ioi_questionnaire` (
  `IOI_QUESTIONNAIRE_ID` int NOT NULL AUTO_INCREMENT,
  `GRANT_HEADER_ID` int DEFAULT NULL,
  `QUESTIONNAIRE_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`IOI_QUESTIONNAIRE_ID`),
  KEY `GRANT_CALL_IOI_QNR_FK` (`GRANT_HEADER_ID`),
  CONSTRAINT `GRANT_CALL_IOI_QNR_FK` FOREIGN KEY (`GRANT_HEADER_ID`) REFERENCES `grant_call_header` (`GRANT_HEADER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_call_keywords` (
  `GRANT_KEYWORD_ID` int NOT NULL AUTO_INCREMENT,
  `GRANT_HEADER_ID` int DEFAULT NULL,
  `SCIENCE_KEYWORD_CODE` varchar(15) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `KEYWORD` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`GRANT_KEYWORD_ID`),
  KEY `GRANT_CALL_KEYWORD_FK1` (`GRANT_HEADER_ID`),
  KEY `GRANT_CALL_KEYWORD_FK2` (`SCIENCE_KEYWORD_CODE`),
  CONSTRAINT `GRANT_CALL_KEYWORDS_FK1` FOREIGN KEY (`GRANT_HEADER_ID`) REFERENCES `grant_call_header` (`GRANT_HEADER_ID`),
  CONSTRAINT `GRANT_CALL_KEYWORDS_FK2` FOREIGN KEY (`SCIENCE_KEYWORD_CODE`) REFERENCES `science_keyword` (`SCIENCE_KEYWORD_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_call_kpi` (
  `GRANT_CALL_KPI_ID` int NOT NULL AUTO_INCREMENT,
  `GRANT_HEADER_ID` int DEFAULT NULL,
  `KPI_TYPE_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`GRANT_CALL_KPI_ID`),
  KEY `GRANT_CALL_KPI_FK` (`GRANT_HEADER_ID`),
  KEY `GRANT_CALL_KPI_FK2` (`KPI_TYPE_CODE`),
  CONSTRAINT `GRANT_CALL_KPI_FK` FOREIGN KEY (`GRANT_HEADER_ID`) REFERENCES `grant_call_header` (`GRANT_HEADER_ID`),
  CONSTRAINT `GRANT_CALL_KPI_FK2` FOREIGN KEY (`KPI_TYPE_CODE`) REFERENCES `kpi_type` (`KPI_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_call_kpi_criteria` (
  `GRANT_CALL_KPI_CRITERIA_ID` int NOT NULL AUTO_INCREMENT,
  `GRANT_CALL_KPI_ID` int DEFAULT NULL,
  `KPI_CRITERIA_TYPE_CODE` varchar(3) DEFAULT NULL,
  `KPI_TYPE_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`GRANT_CALL_KPI_CRITERIA_ID`),
  KEY `GRANT_CALL_KPI_CRITERIA_FK1` (`GRANT_CALL_KPI_ID`),
  KEY `GRANT_CALL_KPI_CRITERIA_FK2` (`KPI_CRITERIA_TYPE_CODE`),
  CONSTRAINT `GRANT_CALL_KPI_CRITERIA_FK1` FOREIGN KEY (`GRANT_CALL_KPI_ID`) REFERENCES `grant_call_kpi` (`GRANT_CALL_KPI_ID`),
  CONSTRAINT `GRANT_CALL_KPI_CRITERIA_FK2` FOREIGN KEY (`KPI_CRITERIA_TYPE_CODE`) REFERENCES `kpi_criteria_type` (`KPI_CRITERIA_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_call_relevant_field` (
  `GRANT_CALL_RELEVANT_ID` int NOT NULL AUTO_INCREMENT,
  `GRANT_HEADER_ID` int DEFAULT NULL,
  `RELEVANT_FIELD_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`GRANT_CALL_RELEVANT_ID`),
  KEY `GRANT_CALL_RELEVANT_FIELD_FK1` (`RELEVANT_FIELD_CODE`),
  KEY `GRANT_CALL_RELEVANT_FIELD_FK2` (`GRANT_HEADER_ID`),
  CONSTRAINT `GRANT_CALL_RELEVANT_FIELD_FK1` FOREIGN KEY (`RELEVANT_FIELD_CODE`) REFERENCES `relevant_field` (`RELEVANT_FIELD_CODE`),
  CONSTRAINT `GRANT_CALL_RELEVANT_FIELD_FK2` FOREIGN KEY (`GRANT_HEADER_ID`) REFERENCES `grant_call_header` (`GRANT_HEADER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_call_research_areas` (
  `GRANT_RESEARCH_AREAS_ID` int NOT NULL AUTO_INCREMENT,
  `GRANT_HEADER_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `RESRCH_TYPE_AREA_CODE` varchar(255) DEFAULT NULL,
  `RESRCH_TYPE_CODE` varchar(255) DEFAULT NULL,
  `RESRCH_TYPE_SUB_AREA_CODE` varchar(255) DEFAULT NULL,
  `CHALLENGE_AREA_CODE` varchar(255) DEFAULT NULL,
  `RESEARCH_AREA_CODE` varchar(255) DEFAULT NULL,
  `RESEARCH_SUB_AREA_CODE` varchar(255) DEFAULT NULL,
  `RESEARCH_TYPE_CODE` int DEFAULT NULL,
  `CHALLENGE_SUBAREA_CODE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`GRANT_RESEARCH_AREAS_ID`),
  KEY `GRANT_CALL_RSRCH_AREA_FK1` (`GRANT_HEADER_ID`),
  KEY `GRANT_CALL_RSRCH_AREA_FK2` (`RESRCH_TYPE_CODE`),
  KEY `GRANT_CALL_RSRCH_AREA_FK3` (`RESRCH_TYPE_AREA_CODE`),
  KEY `GRANT_CALL_RSRCH_AREA_FK4` (`RESRCH_TYPE_SUB_AREA_CODE`),
  KEY `GRANT_CALL_RSRCH_AREA_FK5` (`CHALLENGE_AREA_CODE`),
  KEY `GRANT_CALL_RSRCH_AREA_FK6` (`CHALLENGE_SUBAREA_CODE`),
  CONSTRAINT `GRANT_CALL_RSRCH_AREA_FK1` FOREIGN KEY (`GRANT_HEADER_ID`) REFERENCES `grant_call_header` (`GRANT_HEADER_ID`),
  CONSTRAINT `GRANT_CALL_RSRCH_AREA_FK2` FOREIGN KEY (`RESRCH_TYPE_CODE`) REFERENCES `research_type` (`RESRCH_TYPE_CODE`),
  CONSTRAINT `GRANT_CALL_RSRCH_AREA_FK3` FOREIGN KEY (`RESRCH_TYPE_AREA_CODE`) REFERENCES `research_type_area` (`RESRCH_TYPE_AREA_CODE`),
  CONSTRAINT `GRANT_CALL_RSRCH_AREA_FK4` FOREIGN KEY (`RESRCH_TYPE_SUB_AREA_CODE`) REFERENCES `research_type_sub_area` (`RESRCH_TYPE_SUB_AREA_CODE`),
  CONSTRAINT `GRANT_CALL_RSRCH_AREA_FK5` FOREIGN KEY (`CHALLENGE_AREA_CODE`) REFERENCES `societal_challenge_area` (`CHALLENGE_AREA_CODE`),
  CONSTRAINT `GRANT_CALL_RSRCH_AREA_FK6` FOREIGN KEY (`CHALLENGE_SUBAREA_CODE`) REFERENCES `societal_challenge_subarea` (`CHALLENGE_SUBAREA_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_call_research_type` (
  `RESEARCH_TYPE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`RESEARCH_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_call_status` (
  `GRANT_STATUS_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`GRANT_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_call_type` (
  `GRANT_TYPE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `GRANT_CALL_CATEGORY_CODE` int DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`GRANT_TYPE_CODE`),
  KEY `GRANT_CALL_TYPE_FK1` (`GRANT_CALL_CATEGORY_CODE`),
  CONSTRAINT `GRANT_CALL_TYPE_FK1` FOREIGN KEY (`GRANT_CALL_CATEGORY_CODE`) REFERENCES `grant_call_category` (`GRANT_CALL_CATEGORY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_eligibility_target` (
  `GRANT_ELIGIBILITY_TARGET_ID` int NOT NULL AUTO_INCREMENT,
  `GRANT_ELIGIBILITY_ID` int DEFAULT NULL,
  `ELIGIBILITY_TARGET_TYPE_CODE` varchar(3) DEFAULT NULL,
  `TARGET_CATEGORY_TYPE_CODE` varchar(3) DEFAULT NULL,
  `TARGET_VALUE` varchar(40) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`GRANT_ELIGIBILITY_TARGET_ID`),
  KEY `GRANT_ELIGIBILITY_TARGET_FK` (`GRANT_ELIGIBILITY_ID`),
  KEY `GRANT_ELIGIBILITY_TARGET_FK2` (`ELIGIBILITY_TARGET_TYPE_CODE`),
  CONSTRAINT `GRANT_ELIGIBILITY_TARGET_FK` FOREIGN KEY (`GRANT_ELIGIBILITY_ID`) REFERENCES `grant_call_eligibility` (`GRANT_ELIGIBILITY_ID`),
  CONSTRAINT `GRANT_ELIGIBILITY_TARGET_FK2` FOREIGN KEY (`ELIGIBILITY_TARGET_TYPE_CODE`) REFERENCES `grant_eligibility_target_type` (`ELIGIBILITY_TARGET_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_eligibility_target_type` (
  `ELIGIBILITY_TARGET_TYPE_CODE` varchar(3) NOT NULL,
  `TARGET_CATEGORY_TYPE_CODE` varchar(3) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`ELIGIBILITY_TARGET_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grant_scoring_criteria` (
  `GRANT_SCORE_CRITERIA_ID` int NOT NULL AUTO_INCREMENT,
  `GRANT_HEADER_ID` int DEFAULT NULL,
  `SCORING_CRITERIA_TYPE_CODE` varchar(6) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`GRANT_SCORE_CRITERIA_ID`),
  KEY `GRANT_SCORING_CRITERIA_FK1` (`SCORING_CRITERIA_TYPE_CODE`),
  KEY `GRANT_SCORING_CRITERIA_FK2` (`GRANT_HEADER_ID`),
  CONSTRAINT `GRANT_SCORING_CRITERIA_FK1` FOREIGN KEY (`SCORING_CRITERIA_TYPE_CODE`) REFERENCES `scoring_criteria` (`SCORING_CRITERIA_TYPE_CODE`),
  CONSTRAINT `GRANT_SCORING_CRITERIA_FK2` FOREIGN KEY (`GRANT_HEADER_ID`) REFERENCES `grant_call_header` (`GRANT_HEADER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grantcall_action_log` (
  `ACTION_LOG_ID` int NOT NULL AUTO_INCREMENT,
  `ACTION_TYPE_CODE` varchar(255) DEFAULT NULL,
  `GRANT_HEADER_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ACTION_LOG_ID`),
  KEY `GRANTCALL_ACTION_LOG_FK1` (`GRANT_HEADER_ID`),
  KEY `GRANTCALL_ACTION_LOG_FK2` (`ACTION_TYPE_CODE`),
  CONSTRAINT `GRANTCALL_ACTION_LOG_FK1` FOREIGN KEY (`GRANT_HEADER_ID`) REFERENCES `grant_call_header` (`GRANT_HEADER_ID`),
  CONSTRAINT `GRANTCALL_ACTION_LOG_FK2` FOREIGN KEY (`ACTION_TYPE_CODE`) REFERENCES `grantcall_action_type` (`ACTION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `grantcall_action_type` (
  `ACTION_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ACTION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `help_text` (
  `HELP_TEXT_ID` int NOT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `SECTION_CODE` int DEFAULT NULL,
  `CATEGORY` varchar(50) DEFAULT NULL,
  `HELP_TEXT` varchar(500) DEFAULT NULL,
  `PARENT_HELP_TEXT_ID` int DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`HELP_TEXT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ics_student_travel_dtls` (
  `ICS_REFERENCE_NUMBER` varchar(20) NOT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `COUNTRY` varchar(100) DEFAULT NULL,
  `PURPOSE_HEADER` varchar(100) DEFAULT NULL,
  `PURPOSE_OF_TRIP` varchar(100) DEFAULT NULL,
  `TITLE` varchar(200) DEFAULT NULL,
  `VISIT_START_DATE` datetime DEFAULT NULL,
  `VISIT_END_DATE` datetime DEFAULT NULL,
  `EVENT_START_DATE` datetime DEFAULT NULL,
  `EVENT_END_DATE` datetime DEFAULT NULL,
  `SA_RATE` decimal(14,2) DEFAULT NULL,
  `RECEIPT_AMT` decimal(12,2) DEFAULT NULL,
  `CLAIM_ITEM_DESCRIPTION` varchar(100) DEFAULT NULL,
  `CLAIM_TYPE` varchar(15) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `COUNTRY_CODE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ICS_REFERENCE_NUMBER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `inbox` (
  `INBOX_ID` int NOT NULL AUTO_INCREMENT,
  `ARRIVAL_DATE` datetime(6) DEFAULT NULL,
  `MESSAGE_TYPE_CODE` varchar(4) DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(255) DEFAULT NULL,
  `OPENED_FLAG` varchar(255) DEFAULT NULL,
  `SUBJECT_TYPE` varchar(255) DEFAULT NULL,
  `TO_PERSON_ID` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `USER_MESSAGE` varchar(255) DEFAULT NULL,
  `SUB_MODULE_CODE` int DEFAULT NULL,
  `SUB_MODULE_ITEM_KEY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`INBOX_ID`),
  KEY `INBOX_FK2` (`MODULE_CODE`),
  KEY `INBOX_FK1` (`MESSAGE_TYPE_CODE`),
  KEY `IDX_MODULE_DTLS` (`MODULE_CODE`,`MODULE_ITEM_KEY`,`SUB_MODULE_CODE`,`SUB_MODULE_ITEM_KEY`),
  CONSTRAINT `INBOX_FK1` FOREIGN KEY (`MESSAGE_TYPE_CODE`) REFERENCES `message` (`MESSAGE_TYPE_CODE`),
  CONSTRAINT `INBOX_FK2` FOREIGN KEY (`MODULE_CODE`) REFERENCES `coeus_module` (`MODULE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `institute_la_rates` (
  `ACTIVE_FLAG` varchar(1) DEFAULT 'Y',
  `UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `RATE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `FISCAL_YEAR` varchar(4) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `ON_OFF_CAMPUS_FLAG` varchar(1) DEFAULT NULL,
  `RATE` decimal(5,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `RATE_CLASS_CODE` varchar(3) DEFAULT NULL,
  `CAMPUSFLAG` varchar(255) DEFAULT NULL,
  `INSTITUTE_LA_RATE_ID` int NOT NULL,
  PRIMARY KEY (`INSTITUTE_LA_RATE_ID`),
  KEY `INSTITUTE_LA_RATES_FK3` (`RATE_CLASS_CODE`,`RATE_TYPE_CODE`),
  KEY `INSTITUTE_LA_RATES_FK1` (`UNIT_NUMBER`),
  CONSTRAINT `INSTITUTE_LA_RATES_FK1` FOREIGN KEY (`UNIT_NUMBER`) REFERENCES `unit` (`UNIT_NUMBER`),
  CONSTRAINT `INSTITUTE_LA_RATES_FK2` FOREIGN KEY (`RATE_CLASS_CODE`) REFERENCES `rate_class` (`RATE_CLASS_CODE`),
  CONSTRAINT `INSTITUTE_LA_RATES_FK3` FOREIGN KEY (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`) REFERENCES `rate_type` (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `institute_rates` (
  `ACTIVE_FLAG` varchar(1) NOT NULL DEFAULT 'Y',
  `RATE_CLASS_CODE` varchar(3) DEFAULT NULL,
  `RATE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `ACTIVITY_TYPE_CODE` varchar(3) DEFAULT NULL,
  `FISCAL_YEAR` varchar(4) DEFAULT NULL,
  `START_DATE` date DEFAULT NULL,
  `ON_OFF_CAMPUS_FLAG` varchar(1) DEFAULT NULL,
  `RATE` decimal(5,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `INSTITUTE_RATE_ID` int NOT NULL,
  `CAMPUSFLAG` varchar(255) DEFAULT NULL,
  `UNITNAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`INSTITUTE_RATE_ID`),
  UNIQUE KEY `INSTITUTE_RATES_UQ` (`RATE_CLASS_CODE`,`RATE_TYPE_CODE`,`ACTIVITY_TYPE_CODE`,`FISCAL_YEAR`,`START_DATE`,`ON_OFF_CAMPUS_FLAG`,`UNIT_NUMBER`),
  KEY `INSTITUTE_RATES_FK1` (`ACTIVITY_TYPE_CODE`),
  KEY `INSTITUTE_RATES_FK3` (`UNIT_NUMBER`),
  CONSTRAINT `INSTITUTE_RATES_FK1` FOREIGN KEY (`ACTIVITY_TYPE_CODE`) REFERENCES `activity_type` (`ACTIVITY_TYPE_CODE`),
  CONSTRAINT `INSTITUTE_RATES_FK2` FOREIGN KEY (`RATE_CLASS_CODE`) REFERENCES `rate_class` (`RATE_CLASS_CODE`),
  CONSTRAINT `INSTITUTE_RATES_FK3` FOREIGN KEY (`UNIT_NUMBER`) REFERENCES `unit` (`UNIT_NUMBER`),
  CONSTRAINT `INSTITUTE_RATES_FK4` FOREIGN KEY (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`) REFERENCES `rate_type` (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ioi_status` (
  `STATUS_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ip_budget_header` (
  `BUDGET_HEADER_ID` int NOT NULL AUTO_INCREMENT,
  `ANTICIPATED_TOTAL` decimal(12,2) DEFAULT NULL,
  `BUDGET_STATUS_CODE` varchar(3) DEFAULT NULL,
  `BUDGET_TYPE_CODE` varchar(3) DEFAULT NULL,
  `COMMENTS` varchar(4000) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `CREATE_USER_NAME` varchar(90) DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `IS_AUTO_CALC` varchar(1) DEFAULT NULL,
  `MODULE_ITEM_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(30) DEFAULT NULL,
  `MODULE_SEQUENCE_NUMBER` int DEFAULT NULL,
  `OBLIGATED_CHANGE` decimal(12,2) DEFAULT NULL,
  `OBLIGATED_TOTAL` decimal(12,2) DEFAULT NULL,
  `ON_OFF_CAMPUS_FLAG` varchar(1) DEFAULT NULL,
  `RATE_CLASS_CODE` varchar(3) DEFAULT NULL,
  `RATE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `TOTAL_COST` decimal(12,2) DEFAULT NULL,
  `TOTAL_DIRECT_COST` decimal(12,2) DEFAULT NULL,
  `TOTAL_INDIRECT_COST` decimal(12,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_USER_NAME` varchar(90) DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `TOTAL_SUBCONTRACT_COST` decimal(12,2) DEFAULT NULL,
  `PROPOSAL_ID` int DEFAULT NULL,
  `COST_SHARING_AMOUNT` decimal(12,2) DEFAULT NULL,
  `UNDERRECOVERY_AMOUNT` decimal(12,2) DEFAULT NULL,
  `RESIDUAL_FUNDS` decimal(12,2) DEFAULT NULL,
  `TOTAL_COST_LIMIT` decimal(12,2) DEFAULT NULL,
  `FINAL_VERSION_FLAG` varchar(1) DEFAULT NULL,
  `MODULAR_BUDGET_FLAG` varchar(1) DEFAULT NULL,
  `SUBMIT_COST_SHARING_FLAG` varchar(1) DEFAULT NULL,
  `UNDERRECOVERY_TYPE_CODE` varchar(3) DEFAULT NULL,
  `UNDERRECOVERY_CLASS_CODE` varchar(3) DEFAULT NULL,
  `IS_FINAL_BUDGET` varchar(1) DEFAULT NULL,
  `IS_LATEST_VERSION` varchar(1) DEFAULT NULL,
  `IS_APPROVED_BUDGET` varchar(1) DEFAULT NULL,
  `BUDGET_TEMPLATE_TYPE_ID` int DEFAULT NULL,
  `ON_CAMPUS_RATES` varchar(100) DEFAULT NULL,
  `OFF_CAMPUS_RATES` varchar(100) DEFAULT NULL,
  `COST_SHARE_TYPE_CODE` int DEFAULT NULL,
  PRIMARY KEY (`BUDGET_HEADER_ID`),
  KEY `IP_BUDGET_HEADER_FK1` (`BUDGET_STATUS_CODE`),
  KEY `IP_BUDGET_HEADER_FK2` (`BUDGET_TYPE_CODE`),
  KEY `IP_BUDGET_HEADER_FK3` (`RATE_CLASS_CODE`,`RATE_TYPE_CODE`),
  KEY `IP_BUDGET_HEADER_FK4` (`PROPOSAL_ID`),
  KEY `IP_BUDGET_HEADER_FK5` (`UNDERRECOVERY_CLASS_CODE`,`UNDERRECOVERY_TYPE_CODE`),
  KEY `IP_BUDGET_HEADER_FK6` (`COST_SHARE_TYPE_CODE`),
  CONSTRAINT `IP_BUDGET_HEADER_FK1` FOREIGN KEY (`BUDGET_STATUS_CODE`) REFERENCES `budget_status` (`BUDGET_STATUS_CODE`),
  CONSTRAINT `IP_BUDGET_HEADER_FK2` FOREIGN KEY (`BUDGET_TYPE_CODE`) REFERENCES `budget_type` (`BUDGET_TYPE_CODE`),
  CONSTRAINT `IP_BUDGET_HEADER_FK3` FOREIGN KEY (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`) REFERENCES `rate_type` (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`),
  CONSTRAINT `IP_BUDGET_HEADER_FK4` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `proposal` (`PROPOSAL_ID`),
  CONSTRAINT `IP_BUDGET_HEADER_FK5` FOREIGN KEY (`UNDERRECOVERY_CLASS_CODE`, `UNDERRECOVERY_TYPE_CODE`) REFERENCES `rate_type` (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`),
  CONSTRAINT `IP_BUDGET_HEADER_FK6` FOREIGN KEY (`COST_SHARE_TYPE_CODE`) REFERENCES `cost_sharing_type` (`COST_SHARE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ip_budget_period` (
  `BUDGET_PERIOD_ID` int NOT NULL AUTO_INCREMENT,
  `BUDGET_PERIOD` int DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `IS_OBLIGATED_PERIOD` varchar(1) DEFAULT NULL,
  `MODULE_ITEM_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(30) DEFAULT NULL,
  `PERIOD_LABEL` varchar(255) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `TOTAL_COST` decimal(12,2) DEFAULT NULL,
  `TOTAL_DIRECT_COST` decimal(12,2) DEFAULT NULL,
  `TOTAL_INDIRECT_COST` decimal(12,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `BUDGET_HEADER_ID` int DEFAULT NULL,
  `SUBCONTRACT_COST` decimal(12,2) DEFAULT NULL,
  `COST_SHARING_AMOUNT` decimal(12,2) DEFAULT NULL,
  `UNDERRECOVERY_AMOUNT` decimal(12,2) DEFAULT NULL,
  `TOTAL_COST_LIMIT` decimal(12,2) DEFAULT NULL,
  `COMMENTS` longtext,
  `TOTAL_DIRECT_COST_LIMIT` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_PERIOD_ID`),
  KEY `IP_BUDGET_PERIOD_FK1` (`BUDGET_HEADER_ID`),
  CONSTRAINT `IP_BUDGET_PERIOD_FK1` FOREIGN KEY (`BUDGET_HEADER_ID`) REFERENCES `ip_budget_header` (`BUDGET_HEADER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `irb_protocol` (
  `PROTOCOL_ID` int NOT NULL AUTO_INCREMENT,
  `PROTOCOL_NUMBER` varchar(20) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `PROTOCOL_TYPE_CODE` varchar(3) DEFAULT NULL,
  `PROTOCOL_STATUS_CODE` varchar(3) DEFAULT NULL,
  `TITLE` varchar(2000) DEFAULT NULL,
  `APPROVAL_DATE` datetime DEFAULT NULL,
  `EXPIRATION_DATE` datetime DEFAULT NULL,
  `LAST_APPROVAL_DATE` datetime DEFAULT NULL,
  `FDA_APPLICATION_NUMBER` varchar(15) DEFAULT NULL,
  `REFERENCE_NUMBER_1` varchar(50) DEFAULT NULL,
  `REFERENCE_NUMBER_2` varchar(50) DEFAULT NULL,
  `INITIAL_SUBMISSION_DATE` datetime DEFAULT NULL,
  `ACTIVE` varchar(1) DEFAULT NULL,
  `IS_LATEST` varchar(1) DEFAULT NULL,
  `ASSIGNEE_PERSON_ID` varchar(40) DEFAULT NULL,
  `ORIGINAL_ASSIGNEE_PERSON_ID` varchar(40) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `LEAD_UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `IS_NEW_RULE` varchar(1) DEFAULT NULL,
  `IS_CANCELLED` varchar(1) DEFAULT NULL,
  `RISK_LEVEL_CODE` varchar(3) DEFAULT NULL,
  `RISK_LEVEL_COMMENTS` varchar(2000) DEFAULT NULL,
  `RISK_LVL_DATE_ASSIGNED` datetime DEFAULT NULL,
  `FDA_RISK_LEVEL_CODE` varchar(3) DEFAULT NULL,
  `FDA_RISK_LVL_COMMENTS` varchar(2000) DEFAULT NULL,
  `FDA_RISK_LVL_DATE_ASSIGNED` datetime DEFAULT NULL,
  `DESCRIPTION` varchar(300) DEFAULT NULL,
  `IS_HOLD` varchar(1) DEFAULT NULL,
  `WORKFLOW_PROTOCOL_STATUS_CODE` varchar(3) DEFAULT NULL,
  `DS_LEVEL_ID` int DEFAULT NULL,
  `NON_EMPLOYEE_FLAG` varchar(1) DEFAULT NULL,
  `FUNDING_SOURCE` varchar(200) DEFAULT NULL,
  `FUNDING_SOURCE_TYPE_CODE` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`PROTOCOL_ID`),
  KEY `IRB_PROTOCOL_FK1_idx` (`PROTOCOL_TYPE_CODE`),
  KEY `IRB_PROTOCOL_FK2_idx` (`PROTOCOL_STATUS_CODE`),
  KEY `AC_PROTOCOL_FK3_idx` (`FUNDING_SOURCE_TYPE_CODE`),
  CONSTRAINT `IRB_PROTOCOL_FK1` FOREIGN KEY (`PROTOCOL_TYPE_CODE`) REFERENCES `irb_protocol_type` (`PROTOCOL_TYPE_CODE`),
  CONSTRAINT `IRB_PROTOCOL_FK2` FOREIGN KEY (`PROTOCOL_STATUS_CODE`) REFERENCES `irb_protocol_status` (`PROTOCOL_STATUS_CODE`),
  CONSTRAINT `IRB_PROTOCOL_FK3` FOREIGN KEY (`FUNDING_SOURCE_TYPE_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `irb_protocol_status` (
  `PROTOCOL_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`PROTOCOL_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `irb_protocol_type` (
  `PROTOCOL_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`PROTOCOL_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `job_code` (
  `JOB_CODE` varchar(6) NOT NULL,
  `JOB_TITLE` varchar(50) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `MONTHLY_SALARY` decimal(12,2) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`JOB_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `kpi_criteria_type` (
  `KPI_CRITERIA_TYPE_CODE` varchar(3) NOT NULL,
  `KPI_TYPE_CODE` varchar(3) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`KPI_CRITERIA_TYPE_CODE`),
  KEY `KPI_CRITERIA_TYPE_FK` (`KPI_TYPE_CODE`),
  CONSTRAINT `KPI_CRITERIA_TYPE_FK` FOREIGN KEY (`KPI_TYPE_CODE`) REFERENCES `kpi_type` (`KPI_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `kpi_manpower_development_current_status` (
  `CURRENT_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`CURRENT_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `kpi_publication_status` (
  `PUBLICATION_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`PUBLICATION_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `kpi_technology_disclosure_status` (
  `TECHNOLOGY_DISCLOSURE_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`TECHNOLOGY_DISCLOSURE_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `kpi_template` (
  `KPI_TEMPLATE_ID` int NOT NULL,
  `KPI_TEMPLATE_HEADER_ID` int DEFAULT NULL,
  `KPI_TYPE_CODE` varchar(3) DEFAULT NULL,
  `KPI_CRITERIA_TYPE_CODE` varchar(3) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`KPI_TEMPLATE_ID`),
  KEY `KPI_TEMPLATE_FK` (`KPI_TEMPLATE_HEADER_ID`),
  CONSTRAINT `KPI_TEMPLATE_FK` FOREIGN KEY (`KPI_TEMPLATE_HEADER_ID`) REFERENCES `kpi_template_header` (`KPI_TEMPLATE_HEADER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `kpi_template_header` (
  `KPI_TEMPLATE_HEADER_ID` int NOT NULL,
  `TEMPLATE_NAME` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`KPI_TEMPLATE_HEADER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `kpi_type` (
  `KPI_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`KPI_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `krcr_parm_t` (
  `NMSPC_CD` varchar(20) NOT NULL,
  `CMPNT_CD` varchar(100) NOT NULL,
  `PARM_NM` varchar(255) NOT NULL,
  `PARM_TYP_CD` varchar(5) DEFAULT NULL,
  `VAL` varchar(4000) DEFAULT NULL,
  `PARM_DESC_TXT` varchar(4000) DEFAULT NULL,
  `EVAL_OPRTR_CD` varchar(1) DEFAULT NULL,
  `APPL_ID` varchar(255) NOT NULL DEFAULT 'KUALI',
  `OBJ_ID` varchar(36) DEFAULT NULL,
  `VER_NBR` int DEFAULT '1',
  PRIMARY KEY (`NMSPC_CD`,`CMPNT_CD`,`PARM_NM`,`APPL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `letter_template_type` (
  `LETTER_TEMPLATE_TYPE_CODE` varchar(10) NOT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `CORRESPONDENCE_TEMPLATE` longblob,
  `CONTENT_TYPE` varchar(100) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `SUB_MODULE_CODE` int DEFAULT NULL,
  `PRINT_FILE_TYPE` varchar(20) DEFAULT 'pdf',
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`LETTER_TEMPLATE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `locale` (
  `LOCALE_CODE` varchar(5) NOT NULL,
  `DESCRIPTION` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`LOCALE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `lock_configuration` (
  `MODULE_CODE` int NOT NULL,
  `SUB_MODULE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(45) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) NOT NULL,
  `IS_CHAT_ENABLED` varchar(1) NOT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`MODULE_CODE`,`SUB_MODULE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `lookup_window` (
  `LOOKUP_WINDOW_NAME` varchar(100) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `TABLE_NAME` varchar(30) DEFAULT NULL,
  `COLUMN_NAME` varchar(30) DEFAULT NULL,
  `OTHERS_DISPLAY_COLUMNS` varchar(300) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  `DATA_TYPE_CODE` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`LOOKUP_WINDOW_NAME`),
  KEY `LOOKUP_WINDOW_FK2` (`DATA_TYPE_CODE`),
  CONSTRAINT `LOOKUP_WINDOW_FK2` FOREIGN KEY (`DATA_TYPE_CODE`) REFERENCES `custom_data_elements_data_type` (`DATA_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `manpower` (
  `PERSON_ID` varchar(40) NOT NULL,
  `POSITION_ID` varchar(50) DEFAULT NULL,
  `FULL_NAME` varchar(90) DEFAULT NULL,
  `NATIONALITY` varchar(50) DEFAULT NULL,
  `CITIZENSHIP` varchar(50) DEFAULT NULL,
  `CADIDATURE_START_DATE` datetime DEFAULT NULL,
  `CADIDATURE_END_DATE` datetime DEFAULT NULL,
  `HIRE_DATE` datetime DEFAULT NULL,
  `BASE_SALARY` varchar(60) DEFAULT NULL,
  `CONTRACT_END_DATE` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `FILE_ID` int DEFAULT NULL,
  `JOB_CODE` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`PERSON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `manpower_base_salary_history` (
  `BASE_SALARY_HISTORY_ID` int NOT NULL AUTO_INCREMENT,
  `RESOURCE_UNIQUE_ID` varchar(100) DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `POSITION_ID` varchar(40) DEFAULT NULL,
  `CHARGE_START_DATE` datetime DEFAULT NULL,
  `CHARGE_END_DATE` datetime DEFAULT NULL,
  `CURRENT_BASE_SALARY` varchar(60) DEFAULT NULL,
  `PREVIOUS_BASE_SALARY` varchar(60) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`BASE_SALARY_HISTORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `manpower_budget_reference_type` (
  `BUDGET_REFERENCE_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`BUDGET_REFERENCE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `manpower_candidate_title_type` (
  `CANDIDATE_TITLE_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`CANDIDATE_TITLE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `manpower_compensation_type` (
  `COMPENSATION_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`COMPENSATION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `manpower_configuration_data` (
  `CONFIGURATION_KEY` varchar(100) NOT NULL,
  `CONFIGURATION_VALUE` varchar(250) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`CONFIGURATION_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `manpower_interface_status` (
  `INTERFACE_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`INTERFACE_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `manpower_interface_type` (
  `INTERFACE_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`INTERFACE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `manpower_job_profile_type` (
  `JOB_PROFILE_TYPE_CODE` varchar(20) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `JOB_PROFILE_CODE` varchar(20) DEFAULT NULL,
  `DEFAULT_JOB_TITLE` varchar(200) DEFAULT NULL,
  `JOB_CATEGORY` varchar(200) DEFAULT NULL,
  `JOB_CATEGORY_ID` varchar(200) DEFAULT NULL,
  `EFFECTIVE_DATE` date DEFAULT NULL,
  `WORK_SHIFT_REQUIRED` varchar(1) DEFAULT NULL,
  `PUBLIC_JOB` varchar(1) DEFAULT NULL,
  `IS_WORKDAY_ACTIVE` varchar(1) DEFAULT NULL,
  `SORT_ORDER` int DEFAULT NULL,
  `JOB_FAMILY` varchar(65) DEFAULT NULL,
  `JOB_FAMILY_ID` varchar(65) DEFAULT NULL,
  `JOB_FAMILY_GROUP` varchar(65) DEFAULT NULL,
  PRIMARY KEY (`JOB_PROFILE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `manpower_log` (
  `MANPOWER_LOG_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_NUMBER` varchar(255) DEFAULT NULL,
  `ERROR_DETAIL_MESSAGE` varchar(255) DEFAULT NULL,
  `ERROR_XPATH` varchar(255) DEFAULT NULL,
  `INTERFACE_TYPE_CODE` varchar(255) DEFAULT NULL,
  `IS_MAIL_SENT` varchar(255) DEFAULT NULL,
  `MESSAGE` varchar(255) DEFAULT NULL,
  `MESSAGE_TYPE` varchar(255) DEFAULT NULL,
  `REQUEST_PARAM` varchar(255) DEFAULT NULL,
  `STATUS_CODE` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `WORKDAY_MANPOWER_INTERFACE_ID` int DEFAULT NULL,
  `SUCCESS_COUNT` int DEFAULT NULL,
  `TOTAL_COUNT` int DEFAULT NULL,
  PRIMARY KEY (`MANPOWER_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `manpower_log_user` (
  `MANPOWER_LOG_USER_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_NUMBER` varchar(40) DEFAULT NULL,
  `ACCOUNT_NUMBER` varchar(100) DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `LOGIN_PERSON_ID` varchar(40) DEFAULT NULL,
  `ACCESS_STATUS` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`MANPOWER_LOG_USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `manpower_person_activity_log` (
  `PERSON_ID` varchar(40) NOT NULL,
  `PERSON_STATUS_CHANGE_DATE` datetime DEFAULT NULL,
  `FULL_NAME` varchar(90) DEFAULT NULL,
  `PERSON_STATUS` varchar(3) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`PERSON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `manpower_position_status` (
  `POSITION_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`POSITION_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `manpower_resource_type` (
  `RESOURCE_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`RESOURCE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `manpower_temp` (
  `PERSON_ID` varchar(255) NOT NULL,
  `CITIZENSHIP` varchar(255) DEFAULT NULL,
  `NATIONALITY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PERSON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `manpower_type` (
  `MANPOWER_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`MANPOWER_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `manpower_upgrade_type` (
  `UPGRADE_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`UPGRADE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `manpower_user_actions` (
  `MANPOWER_USER_ACTION_CODE` varchar(3) NOT NULL,
  `MANPOWER_USER_ACTION` varchar(200) DEFAULT NULL,
  `DESCRIPTION` varchar(4000) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MANPOWER_USER_ACTION_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `manpower_workday_resource` (
  `MANPOWER_WORKDAY_RESOURCE_ID` int NOT NULL AUTO_INCREMENT,
  `WBS_NUMBER` varchar(50) DEFAULT NULL,
  `POSITION_STATUS_CODE` varchar(3) DEFAULT NULL,
  `COST_ALLOCATION` decimal(5,2) DEFAULT NULL,
  `CHARGE_DURATION` varchar(50) DEFAULT NULL,
  `CHARGE_START_DATE` datetime DEFAULT NULL,
  `CHARGE_END_DATE` datetime DEFAULT NULL,
  `BASE_SALARY_USED` varchar(200) DEFAULT NULL,
  `COMMITTED_COST` decimal(12,2) DEFAULT NULL,
  `MULTIPLIER_USED` decimal(12,2) DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `POSITION_ID` varchar(50) DEFAULT NULL,
  `ADJUSTED_COMMITTED_COST` decimal(12,2) DEFAULT NULL,
  `JOB_PROFILE_TYPE_CODE` varchar(20) DEFAULT NULL,
  `WORKDAY_REFERENCE_ID` varchar(45) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `ALLOCATION_PERSON_INACTIVE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`MANPOWER_WORKDAY_RESOURCE_ID`),
  KEY `MANPOWER_WORKDAY_RESOURCE_FK1_idx` (`POSITION_STATUS_CODE`),
  KEY `MANPOWER_WORKDAY_RESOURCE_FK2_idx` (`JOB_PROFILE_TYPE_CODE`),
  CONSTRAINT `MANPOWER_WORKDAY_RESOURCE_FK1` FOREIGN KEY (`POSITION_STATUS_CODE`) REFERENCES `manpower_position_status` (`POSITION_STATUS_CODE`),
  CONSTRAINT `MANPOWER_WORKDAY_RESOURCE_FK2` FOREIGN KEY (`JOB_PROFILE_TYPE_CODE`) REFERENCES `manpower_job_profile_type` (`JOB_PROFILE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `message` (
  `MESSAGE_TYPE_CODE` varchar(4) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MESSAGE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `meta_rule_detail` (
  `META_RULE_DETAIL_ID` int NOT NULL AUTO_INCREMENT,
  `META_RULE_ID` int NOT NULL,
  `NODE_NUMBER` int NOT NULL,
  `RULE_ID` int NOT NULL,
  `PARENT_NODE` int NOT NULL,
  `NEXT_NODE` int DEFAULT NULL,
  `NODE_IF_TRUE` int DEFAULT NULL,
  `NODE_IF_FALSE` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`META_RULE_DETAIL_ID`),
  KEY `META_RULE_DETAIL_FK1` (`META_RULE_ID`),
  KEY `META_RULE_DETAIL_RULE_FK2` (`RULE_ID`),
  CONSTRAINT `META_RULE_DETAIL_FK1` FOREIGN KEY (`META_RULE_ID`) REFERENCES `meta_rules` (`META_RULE_ID`),
  CONSTRAINT `META_RULE_DETAIL_RULE_FK2` FOREIGN KEY (`RULE_ID`) REFERENCES `business_rules` (`RULE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `meta_rules` (
  `META_RULE_ID` int NOT NULL AUTO_INCREMENT,
  `UNIT_NUMBER` varchar(8) NOT NULL,
  `META_RULE_TYPE` varchar(2) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `MODULE_CODE` int NOT NULL,
  `SUB_MODULE_CODE` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`META_RULE_ID`),
  KEY `META_RULES_FK1` (`UNIT_NUMBER`),
  CONSTRAINT `META_RULES_FK1` FOREIGN KEY (`UNIT_NUMBER`) REFERENCES `unit` (`UNIT_NUMBER`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `migration_attachment_error_log` (
  `MIGRATION_ATTCHMNT_ERROR_LOG_ID` int NOT NULL AUTO_INCREMENT,
  `ERROR_MESSAGE` varchar(255) DEFAULT NULL,
  `ERROR_TYPE` varchar(255) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `FILE_TYPE` varchar(255) DEFAULT NULL,
  `GRANT_HEADER_ID` varchar(255) DEFAULT NULL,
  `LEGACY_WBS_NUMBER` varchar(255) DEFAULT NULL,
  `LEGACY_PROJECT_ID` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `VALIDATION_TYPE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MIGRATION_ATTCHMNT_ERROR_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `migration_manpower_person` (
  `MIGRATION_MANPOWER_PERSON_ID` varchar(255) NOT NULL,
  `CITIZENSHIP` varchar(255) DEFAULT NULL,
  `NATIONALITY` varchar(255) DEFAULT NULL,
  `WORKERID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MIGRATION_MANPOWER_PERSON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `milestone_status` (
  `MILESTONE_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`MILESTONE_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `mitkc_unit_with_children` (
  `UNIT_NUMBER` varchar(8) NOT NULL,
  `UNIT_NAME` varchar(60) NOT NULL,
  `CHILD_UNIT_NUMBER` varchar(8) NOT NULL,
  `CHILD_UNIT_NAME` varchar(60) NOT NULL,
  PRIMARY KEY (`UNIT_NUMBER`,`UNIT_NAME`,`CHILD_UNIT_NUMBER`,`CHILD_UNIT_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `module_derived_roles` (
  `MODULE_CODE` int NOT NULL,
  `ROLE_ID` int NOT NULL,
  `DERIVED_ROLE_NAME` varchar(50) DEFAULT NULL,
  `CAN_DISPLAY` varchar(1) DEFAULT NULL,
  `AUTO_GRANT_TO_PI` varchar(1) DEFAULT NULL,
  `AUTO_GRANT_TO_COI` varchar(1) DEFAULT NULL,
  `AUTO_GRANT_TO_MODULE_CREATOR` varchar(1) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `IS_ACTIVE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MODULE_CODE`,`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `module_variable_section` (
  `VARIABLE_SECTION_ID` int NOT NULL AUTO_INCREMENT,
  `MODULE_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(255) DEFAULT NULL,
  `SECTION_CODE` varchar(255) DEFAULT NULL,
  `SUB_MODULE_CODE` int DEFAULT NULL,
  `TYPE_CODE` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `VARIABLE_TYPE` varchar(255) DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `SUB_MODULE_ITEM_KEY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`VARIABLE_SECTION_ID`),
  KEY `MODULE_VARIABLE_SECTION_FK2` (`SECTION_CODE`),
  CONSTRAINT `MODULE_VARIABLE_SECTION_FK2` FOREIGN KEY (`SECTION_CODE`) REFERENCES `section_type` (`SECTION_CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=211 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `narrative_status` (
  `NARRATIVE_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(20) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`NARRATIVE_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `negotiation` (
  `NEGOTIATION_ID` int NOT NULL AUTO_INCREMENT,
  `NEGOTIATION_STATUS_CODE` varchar(3) DEFAULT NULL,
  `WORKFLOW_STATUS_CODE` varchar(3) DEFAULT NULL,
  `AGREEMENT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `NEGOTIATOR_PERSON_ID` varchar(40) DEFAULT NULL,
  `NEGOTIATOR_FULL_NAME` varchar(90) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `FINAL_CONTRACT_DOCUMENT` longblob,
  `SUMMARY_COMMENT` varchar(4000) DEFAULT NULL,
  `NEGOTIATOR_COMMENT` varchar(4000) DEFAULT NULL,
  `LEGAL_COMMENT` varchar(4000) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `ASSOCIATED_PROJECT_ID` varchar(255) DEFAULT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `SUBMIT_USER` varchar(40) DEFAULT NULL,
  `SUBMISSION_DATE` datetime DEFAULT NULL,
  `TOTAL_BUDGET_AMOUNT` decimal(12,2) DEFAULT NULL,
  `TOTAL_FUND_REQUESTED` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`NEGOTIATION_ID`),
  KEY `NEGOTIATION_FK1` (`NEGOTIATION_STATUS_CODE`),
  KEY `NEGOTIATION_FK2` (`WORKFLOW_STATUS_CODE`),
  KEY `NEGOTIATION_FK3` (`AGREEMENT_TYPE_CODE`),
  CONSTRAINT `NEGOTIATION_FK1` FOREIGN KEY (`NEGOTIATION_STATUS_CODE`) REFERENCES `negotiation_status` (`NEGOTIATION_STATUS_CODE`),
  CONSTRAINT `NEGOTIATION_FK2` FOREIGN KEY (`WORKFLOW_STATUS_CODE`) REFERENCES `negotiation_workflow_status` (`WORKFLOW_STATUS_CODE`),
  CONSTRAINT `NEGOTIATION_FK3` FOREIGN KEY (`AGREEMENT_TYPE_CODE`) REFERENCES `negotiation_agreement_type` (`AGREEMENT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `negotiation_activity` (
  `NEGOTIATION_ACTIVITY_ID` int NOT NULL AUTO_INCREMENT,
  `NEGOTIATION_ID` int DEFAULT NULL,
  `NEGOTIATION_LOCATION_ID` int DEFAULT NULL,
  `LOCATION_TYPE_CODE` varchar(3) DEFAULT NULL,
  `ACTIVITY_TYPE_CODE` varchar(3) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `FOLLOWUP_DATE` datetime DEFAULT NULL,
  `DESCRIPTION` varchar(2000) DEFAULT NULL,
  `RESTRICTED` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`NEGOTIATION_ACTIVITY_ID`),
  KEY `NEGOTIATION_ACTIVITY_FK1` (`ACTIVITY_TYPE_CODE`),
  KEY `NEGOTIATION_ACTIVITY_FK2` (`NEGOTIATION_LOCATION_ID`),
  KEY `NEGOTIATION_ACTIVITY_FK3` (`NEGOTIATION_ID`),
  KEY `NEGOTIATION_ACTIVITY_FK4` (`LOCATION_TYPE_CODE`),
  CONSTRAINT `NEGOTIATION_ACTIVITY_FK1` FOREIGN KEY (`ACTIVITY_TYPE_CODE`) REFERENCES `negotiation_activity_type` (`ACTIVITY_TYPE_CODE`),
  CONSTRAINT `NEGOTIATION_ACTIVITY_FK2` FOREIGN KEY (`NEGOTIATION_LOCATION_ID`) REFERENCES `negotiation_location` (`NEGOTIATION_LOCATION_ID`),
  CONSTRAINT `NEGOTIATION_ACTIVITY_FK3` FOREIGN KEY (`NEGOTIATION_ID`) REFERENCES `negotiation` (`NEGOTIATION_ID`),
  CONSTRAINT `NEGOTIATION_ACTIVITY_FK4` FOREIGN KEY (`LOCATION_TYPE_CODE`) REFERENCES `negotiation_location_type` (`LOCATION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `negotiation_activity_type` (
  `ACTIVITY_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`ACTIVITY_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `negotiation_agreement_type` (
  `AGREEMENT_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`AGREEMENT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `negotiation_agreement_value` (
  `NEGOTIATIONS_AGREEMENT_ID` int NOT NULL AUTO_INCREMENT,
  `NEGOTIATIONS_ASSOCIATION_ID` int DEFAULT NULL,
  `NEGOTIATION_ID` int DEFAULT NULL,
  `PERIOD_NUMBER` int DEFAULT NULL,
  `SPONSOR_DISTRIBUTION_AMT` decimal(12,2) DEFAULT NULL,
  `INSTITUTION_DISTRIBUTION_AMT` decimal(12,2) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`NEGOTIATIONS_AGREEMENT_ID`),
  KEY `NEGOTIATION_AGRMNT_VALUE_FK1` (`NEGOTIATIONS_ASSOCIATION_ID`),
  KEY `NEGOTIATION_AGRMNT_VALUE_FK2` (`NEGOTIATION_ID`),
  CONSTRAINT `NEGOTIATION_AGRMNT_VALUE_FK1` FOREIGN KEY (`NEGOTIATIONS_ASSOCIATION_ID`) REFERENCES `negotiation_association` (`NEGOTIATIONS_ASSOCIATION_ID`),
  CONSTRAINT `NEGOTIATION_AGRMNT_VALUE_FK2` FOREIGN KEY (`NEGOTIATION_ID`) REFERENCES `negotiation` (`NEGOTIATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `negotiation_assoc_detail` (
  `NEGOTIATIONS_ASSOC_DETAIL_ID` int NOT NULL AUTO_INCREMENT,
  `NEGOTIATIONS_ASSOCIATION_ID` int DEFAULT NULL,
  `NEGOTIATION_ID` int DEFAULT NULL,
  `TITLE` varchar(45) DEFAULT NULL,
  `PI_PERSON_ID` varchar(40) DEFAULT NULL,
  `PI_ROLODEX_ID` varchar(40) DEFAULT NULL,
  `PI_NAME` varchar(100) DEFAULT NULL,
  `LEAD_UNIT` varchar(8) DEFAULT NULL,
  `SPONSOR_CODE` varchar(6) DEFAULT NULL,
  `PRIME_SPONSOR_CODE` varchar(6) DEFAULT NULL,
  `SPONSOR_AWARD_NUMBER` varchar(70) DEFAULT NULL,
  `CONTACT_ADMIN_PERSON_ID` varchar(40) DEFAULT NULL,
  `SUBAWARD_ORG` varchar(8) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `associationTypeCodeNumber` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`NEGOTIATIONS_ASSOC_DETAIL_ID`),
  KEY `NEGOTIATION_ASSOC_DETAIL_FK1` (`NEGOTIATIONS_ASSOCIATION_ID`),
  KEY `NEGOTIATION_ASSOC_DETAIL_FK6` (`CONTACT_ADMIN_PERSON_ID`),
  KEY `NEGOTIATION_ASSOC_DETAIL_FK2` (`NEGOTIATION_ID`),
  KEY `NEGOTIATION_ASSOC_DETAIL_FK7` (`SUBAWARD_ORG`),
  KEY `NEGOTIATION_ASSOC_DETAIL_FK5` (`PRIME_SPONSOR_CODE`),
  KEY `NEGOTIATION_ASSOC_DETAIL_FK4` (`SPONSOR_CODE`),
  KEY `NEGOTIATION_ASSOC_DETAIL_FK3` (`LEAD_UNIT`),
  CONSTRAINT `NEGOTIATION_ASSOC_DETAIL_FK1` FOREIGN KEY (`NEGOTIATIONS_ASSOCIATION_ID`) REFERENCES `negotiation_association` (`NEGOTIATIONS_ASSOCIATION_ID`),
  CONSTRAINT `NEGOTIATION_ASSOC_DETAIL_FK2` FOREIGN KEY (`NEGOTIATION_ID`) REFERENCES `negotiation` (`NEGOTIATION_ID`),
  CONSTRAINT `NEGOTIATION_ASSOC_DETAIL_FK3` FOREIGN KEY (`LEAD_UNIT`) REFERENCES `unit` (`UNIT_NUMBER`),
  CONSTRAINT `NEGOTIATION_ASSOC_DETAIL_FK4` FOREIGN KEY (`SPONSOR_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`),
  CONSTRAINT `NEGOTIATION_ASSOC_DETAIL_FK5` FOREIGN KEY (`PRIME_SPONSOR_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`),
  CONSTRAINT `NEGOTIATION_ASSOC_DETAIL_FK6` FOREIGN KEY (`CONTACT_ADMIN_PERSON_ID`) REFERENCES `person` (`PERSON_ID`),
  CONSTRAINT `NEGOTIATION_ASSOC_DETAIL_FK7` FOREIGN KEY (`SUBAWARD_ORG`) REFERENCES `organization` (`ORGANIZATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `negotiation_association` (
  `NEGOTIATIONS_ASSOCIATION_ID` int NOT NULL AUTO_INCREMENT,
  `NEGOTIATION_ID` int DEFAULT NULL,
  `ASSOCIATION_TYPE_CODE` varchar(3) DEFAULT NULL,
  `ASSOCIATED_PROJECT_ID` varchar(25) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`NEGOTIATIONS_ASSOCIATION_ID`),
  KEY `NEGOTIATION_ASSOCIATION_FK1` (`NEGOTIATION_ID`),
  KEY `NEGOTIATION_ASSOCIATION_FK2` (`ASSOCIATION_TYPE_CODE`),
  CONSTRAINT `NEGOTIATION_ASSOCIATION_FK1` FOREIGN KEY (`NEGOTIATION_ID`) REFERENCES `negotiation` (`NEGOTIATION_ID`),
  CONSTRAINT `NEGOTIATION_ASSOCIATION_FK2` FOREIGN KEY (`ASSOCIATION_TYPE_CODE`) REFERENCES `negotiation_association_type` (`ASSOCIATION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `negotiation_association_type` (
  `ASSOCIATION_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ASSOCIATION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `negotiation_attachment` (
  `NEGOTIATIONS_ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `NEGOTIATION_ACTIVITY_ID` int DEFAULT NULL,
  `NEGOTIATION_ID` int DEFAULT NULL,
  `ATTACHMENT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `RESTRICTED` varchar(1) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `CONTENT_TYPE` varchar(200) DEFAULT NULL,
  `FILE_ID` varchar(36) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `DOCUMENT_ID` int DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `DOCUMENT_STATUS_CODE` int DEFAULT NULL,
  `NARRATIVE_STATUS_CODE` varchar(255) DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  PRIMARY KEY (`NEGOTIATIONS_ATTACHMENT_ID`),
  KEY `NEGOTIATION_ATTACHMENT_FK1` (`ATTACHMENT_TYPE_CODE`),
  KEY `NEGOTIATION_ATTACHMENT_FK2` (`NEGOTIATION_ACTIVITY_ID`),
  KEY `NEGOTIATION_ATTACHMENT_FK3` (`FILE_ID`),
  KEY `NEGOTIATION_ATTACHMENT_FK4` (`NEGOTIATION_ID`),
  KEY `NEGOTIATION_ATTACHMENT_FK5` (`DOCUMENT_STATUS_CODE`),
  KEY `NEGOTIATION_ATTACHMENT_FK6` (`NARRATIVE_STATUS_CODE`),
  CONSTRAINT `NEGOTIATION_ATTACHMENT_FK1` FOREIGN KEY (`ATTACHMENT_TYPE_CODE`) REFERENCES `negotiation_attachment_type` (`ATTACHMENT_TYPE_CODE`),
  CONSTRAINT `NEGOTIATION_ATTACHMENT_FK2` FOREIGN KEY (`NEGOTIATION_ACTIVITY_ID`) REFERENCES `negotiation_activity` (`NEGOTIATION_ACTIVITY_ID`),
  CONSTRAINT `NEGOTIATION_ATTACHMENT_FK3` FOREIGN KEY (`FILE_ID`) REFERENCES `file_data` (`ID`),
  CONSTRAINT `NEGOTIATION_ATTACHMENT_FK4` FOREIGN KEY (`NEGOTIATION_ID`) REFERENCES `negotiation` (`NEGOTIATION_ID`),
  CONSTRAINT `NEGOTIATION_ATTACHMENT_FK5` FOREIGN KEY (`DOCUMENT_STATUS_CODE`) REFERENCES `document_status` (`DOCUMENT_STATUS_CODE`),
  CONSTRAINT `NEGOTIATION_ATTACHMENT_FK6` FOREIGN KEY (`NARRATIVE_STATUS_CODE`) REFERENCES `narrative_status` (`NARRATIVE_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `negotiation_attachment_type` (
  `ATTACHMENT_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`ATTACHMENT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `negotiation_comment` (
  `NEGOTIATION_COMMENT_ID` int NOT NULL AUTO_INCREMENT,
  `NEGOTIATION_ID` int DEFAULT NULL,
  `COMMENT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `COMMENTS` varchar(4000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `NEGOTIATION_LOCATION_ID` int DEFAULT NULL,
  PRIMARY KEY (`NEGOTIATION_COMMENT_ID`),
  KEY `NEGOTIATION_COMMENT_FK2` (`NEGOTIATION_ID`),
  KEY `NEGOTIATION_COMMENT_FK3` (`COMMENT_TYPE_CODE`),
  KEY `NEGOTIATION_COMMENT_FK4` (`NEGOTIATION_LOCATION_ID`),
  CONSTRAINT `NEGOTIATION_COMMENT_FK1` FOREIGN KEY (`COMMENT_TYPE_CODE`) REFERENCES `negotiation_comment_type` (`COMMENT_TYPE_CODE`),
  CONSTRAINT `NEGOTIATION_COMMENT_FK2` FOREIGN KEY (`NEGOTIATION_ID`) REFERENCES `negotiation` (`NEGOTIATION_ID`),
  CONSTRAINT `NEGOTIATION_COMMENT_FK3` FOREIGN KEY (`COMMENT_TYPE_CODE`) REFERENCES `negotiation_comment_type` (`COMMENT_TYPE_CODE`),
  CONSTRAINT `NEGOTIATION_COMMENT_FK4` FOREIGN KEY (`NEGOTIATION_LOCATION_ID`) REFERENCES `negotiation_location` (`NEGOTIATION_LOCATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `negotiation_comment_type` (
  `COMMENT_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`COMMENT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `negotiation_location` (
  `NEGOTIATION_LOCATION_ID` int NOT NULL AUTO_INCREMENT,
  `NEGOTIATION_ID` int DEFAULT NULL,
  `LOCATION_TYPE_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `ASSIGNEE_PERSON_ID` varchar(40) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `LOCATION_STATUS_CODE` varchar(3) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`NEGOTIATION_LOCATION_ID`),
  KEY `NEGOTIATION_LOCATION_FK1` (`LOCATION_TYPE_CODE`),
  KEY `NEGOTIATION_LOCATION_FK2` (`NEGOTIATION_ID`),
  KEY `NEGOTIATION_LOCATION_FK3` (`ASSIGNEE_PERSON_ID`),
  KEY `NEGOTIATION_LOCATION_FK4` (`LOCATION_STATUS_CODE`),
  CONSTRAINT `NEGOTIATION_LOCATION_FK1` FOREIGN KEY (`LOCATION_TYPE_CODE`) REFERENCES `negotiation_location_type` (`LOCATION_TYPE_CODE`),
  CONSTRAINT `NEGOTIATION_LOCATION_FK2` FOREIGN KEY (`NEGOTIATION_ID`) REFERENCES `negotiation` (`NEGOTIATION_ID`),
  CONSTRAINT `NEGOTIATION_LOCATION_FK3` FOREIGN KEY (`ASSIGNEE_PERSON_ID`) REFERENCES `person` (`PERSON_ID`),
  CONSTRAINT `NEGOTIATION_LOCATION_FK4` FOREIGN KEY (`LOCATION_STATUS_CODE`) REFERENCES `negotiation_location_status` (`LOCATION_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `negotiation_location_status` (
  `LOCATION_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`LOCATION_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `negotiation_location_type` (
  `LOCATION_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`LOCATION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `negotiation_negotiator_histry` (
  `NEGO_NEGOTIATOR_HISTRY_ID` decimal(22,0) NOT NULL,
  `NEGOTIATION_ID` decimal(22,0) DEFAULT NULL,
  `NEGOTIATOR_PERSON_ID` varchar(40) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`NEGO_NEGOTIATOR_HISTRY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `negotiation_personnel` (
  `NEGOTIATION_PERSONNEL_ID` int NOT NULL AUTO_INCREMENT,
  `NEGOTIATION_ID` int DEFAULT NULL,
  `PERSONNEL_TYPE_CODE` varchar(3) DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `FULL_NAME` varchar(90) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `ROLODEX_ID` int DEFAULT NULL,
  PRIMARY KEY (`NEGOTIATION_PERSONNEL_ID`),
  KEY `NEGOTIATION_PERSONNEL_FK1` (`PERSONNEL_TYPE_CODE`),
  KEY `NEGOTIATION_PERSONNEL_FK2` (`NEGOTIATION_ID`),
  CONSTRAINT `NEGOTIATION_PERSONNEL_FK1` FOREIGN KEY (`PERSONNEL_TYPE_CODE`) REFERENCES `negotiation_personnel_type` (`PERSONNEL_TYPE_CODE`),
  CONSTRAINT `NEGOTIATION_PERSONNEL_FK2` FOREIGN KEY (`NEGOTIATION_ID`) REFERENCES `negotiation` (`NEGOTIATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `negotiation_personnel_type` (
  `PERSONNEL_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`PERSONNEL_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `negotiation_status` (
  `NEGOTIATION_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` date DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`NEGOTIATION_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `negotiation_status_histry` (
  `NEGO_STATUS_HISTRY_ID` decimal(22,0) NOT NULL,
  `NEGOTIATION_ID` decimal(22,0) DEFAULT NULL,
  `NEGOTIATION_STATUS_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`NEGO_STATUS_HISTRY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `negotiation_workflow_status` (
  `WORKFLOW_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`WORKFLOW_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `notification_log` (
  `NOTIFICATION_LOG_ID` int NOT NULL AUTO_INCREMENT,
  `NOTIFICATION_TYPE_ID` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(20) DEFAULT NULL,
  `FROM_USER_EMAIL_ID` varchar(200) DEFAULT NULL,
  `TO_USER_EMAIL_ID` varchar(2000) DEFAULT NULL,
  `SUBJECT` varchar(1000) DEFAULT NULL,
  `MESSAGE` longtext,
  `SEND_DATE` datetime DEFAULT NULL,
  `IS_SUCCESS` varchar(1) DEFAULT NULL,
  `ERROR_MESSAGE` varchar(1000) DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  PRIMARY KEY (`NOTIFICATION_LOG_ID`),
  KEY `NOTIFICATION_LOG_FK1` (`NOTIFICATION_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `notification_log_recipient` (
  `NOTIFICATION_LOG_RECIPIENT_ID` int NOT NULL AUTO_INCREMENT,
  `NOTIFICATION_LOG_ID` int DEFAULT NULL,
  `TO_USER_EMAIL_ID` varchar(200) DEFAULT NULL,
  `MAIL_SENT_FLAG` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`NOTIFICATION_LOG_RECIPIENT_ID`),
  KEY `NOTIFICATION_LOG_RECIPIENT_FK2` (`NOTIFICATION_LOG_ID`),
  CONSTRAINT `NOTIFICATION_LOG_RECIPIENT_FK2` FOREIGN KEY (`NOTIFICATION_LOG_ID`) REFERENCES `notification_log` (`NOTIFICATION_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `notification_recipient` (
  `NOTIFICATION_RECIPIENT_ID` int NOT NULL AUTO_INCREMENT,
  `NOTIFICATION_TYPE_ID` int DEFAULT NULL,
  `RECIPIENT_PERSON_ID` varchar(90) DEFAULT NULL,
  `ROLE_TYPE_CODE` int DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `CREATE_TIMESTAMP` date DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` date DEFAULT NULL,
  `RECIPIENT_TYPE` varchar(5) DEFAULT NULL,
  `RECIPIENT_NAME` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`NOTIFICATION_RECIPIENT_ID`),
  KEY `NOTIFICATION_RECIPIENT_FK1` (`NOTIFICATION_TYPE_ID`),
  CONSTRAINT `NOTIFICATION_RECIPIENT_FK1` FOREIGN KEY (`NOTIFICATION_TYPE_ID`) REFERENCES `notification_type` (`NOTIFICATION_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `notification_type` (
  `NOTIFICATION_TYPE_ID` int NOT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `SUB_MODULE_CODE` int DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `SUBJECT` varchar(1000) DEFAULT NULL,
  `MESSAGE` longtext,
  `PROMPT_USER` varchar(1) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `IS_SYSTEM_SPECIFIC` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`NOTIFICATION_TYPE_ID`),
  KEY `NOTIFICATION_TYPE_FK1` (`MODULE_CODE`),
  CONSTRAINT `NOTIFICATION_TYPE_FK1` FOREIGN KEY (`MODULE_CODE`) REFERENCES `coeus_module` (`MODULE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `orcid_error_log` (
  `ORCID_ERROR_LOG_ID` int NOT NULL AUTO_INCREMENT,
  `ORCID_ID` varchar(30) DEFAULT NULL,
  `ERROR_MESSAGE` longtext,
  `ERROR_TYPE` varchar(50) DEFAULT NULL,
  `PUT_CODE` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  `IS_MAIL_SENT` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`ORCID_ERROR_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `orcid_parameter` (
  `PARAMETER_NAME` varchar(100) NOT NULL,
  `VALUE` varchar(1000) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) NOT NULL,
  PRIMARY KEY (`PARAMETER_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `orcid_webhook_action_types` (
  `WEBHOOK_ACTION_TYPE_CODE` varchar(1) NOT NULL,
  `DESCRIPTION` varchar(20) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`WEBHOOK_ACTION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `orcid_webhook_notify_log` (
  `ORCID_WEBHOOK_NOTIFY_LOG_ID` int NOT NULL AUTO_INCREMENT,
  `ORCID_ID` varchar(30) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `WEBHOOK_ACTION_TYPE_CODE` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`ORCID_WEBHOOK_NOTIFY_LOG_ID`),
  KEY `ORCID_WEBHOOK_NOTIFY_LOG_FK1` (`WEBHOOK_ACTION_TYPE_CODE`),
  CONSTRAINT `ORCID_WEBHOOK_NOTIFY_LOG_FK1` FOREIGN KEY (`WEBHOOK_ACTION_TYPE_CODE`) REFERENCES `orcid_webhook_action_types` (`WEBHOOK_ACTION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `orcid_work` (
  `PUT_CODE` int NOT NULL,
  `ORCID_WORK_STATUS_CODE` varchar(3) DEFAULT NULL,
  `SOURCE` varchar(100) DEFAULT NULL,
  `PATH` varchar(100) DEFAULT NULL,
  `TITLE` varchar(1000) DEFAULT NULL,
  `TRANSLATED_TITLE` varchar(1000) DEFAULT NULL,
  `TRANSLATED_TITLE_LOCALE_CODE` varchar(5) DEFAULT NULL,
  `JOURNAL_TITLE` varchar(1000) DEFAULT NULL,
  `SUB_TITLE` varchar(1000) DEFAULT NULL,
  `SHORT_DESCRIPTION` longtext,
  `ORCID_WORK_CITATION_TYPE_CODE` varchar(40) DEFAULT NULL,
  `CITATION_VALUE` longtext,
  `ORCID_WORK_TYPE_CODE` varchar(30) DEFAULT NULL,
  `ORCID_WORK_CATEGORY_CODE` varchar(3) DEFAULT NULL,
  `URL` varchar(2000) DEFAULT NULL,
  `LOCALE_CODE` varchar(5) DEFAULT NULL,
  `COUNTRY_CODE` varchar(3) DEFAULT NULL,
  `VISIBILITY` varchar(20) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `LAST_MODIFIED_DATE` datetime DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `CITATION_TYPE` varchar(255) DEFAULT NULL,
  `PUBLICATION_DAY` varchar(255) DEFAULT NULL,
  `PUBLICATION_MONTH` varchar(255) DEFAULT NULL,
  `PUBLICATION_YEAR` varchar(255) DEFAULT NULL,
  `PUBLICATION_DATE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PUT_CODE`),
  KEY `ORCID_WORK_FK1` (`LOCALE_CODE`),
  KEY `ORCID_WORK_FK2` (`TRANSLATED_TITLE_LOCALE_CODE`),
  KEY `ORCID_WORK_FK3` (`ORCID_WORK_TYPE_CODE`,`ORCID_WORK_CATEGORY_CODE`),
  KEY `ORCID_WORK_FK4` (`ORCID_WORK_STATUS_CODE`),
  KEY `ORCID_WORK_FK5` (`ORCID_WORK_CITATION_TYPE_CODE`),
  CONSTRAINT `ORCID_WORK_FK1` FOREIGN KEY (`LOCALE_CODE`) REFERENCES `locale` (`LOCALE_CODE`),
  CONSTRAINT `ORCID_WORK_FK2` FOREIGN KEY (`TRANSLATED_TITLE_LOCALE_CODE`) REFERENCES `locale` (`LOCALE_CODE`),
  CONSTRAINT `ORCID_WORK_FK3` FOREIGN KEY (`ORCID_WORK_TYPE_CODE`, `ORCID_WORK_CATEGORY_CODE`) REFERENCES `orcid_work_type` (`ORCID_WORK_TYPE_CODE`, `ORCID_WORK_CATEGORY_CODE`),
  CONSTRAINT `ORCID_WORK_FK4` FOREIGN KEY (`ORCID_WORK_STATUS_CODE`) REFERENCES `orcid_work_status` (`ORCID_WORK_STATUS_CODE`),
  CONSTRAINT `ORCID_WORK_FK5` FOREIGN KEY (`ORCID_WORK_CITATION_TYPE_CODE`) REFERENCES `orcid_work_citation_type` (`ORCID_WORK_CITATION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `orcid_work_category` (
  `ORCID_WORK_CATEGORY_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ORCID_WORK_CATEGORY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `orcid_work_citation_type` (
  `ORCID_WORK_CITATION_TYPE_CODE` varchar(40) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ORCID_WORK_CITATION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `orcid_work_contributor` (
  `ORCID_WORK_CONTRIBUTORS_ID` int NOT NULL AUTO_INCREMENT,
  `PUT_CODE` int DEFAULT NULL,
  `CREDIT_NAME` varchar(300) DEFAULT NULL,
  `EMAIL_ADDREES` varchar(60) DEFAULT NULL,
  `CONTRIBUTOR_SEQUENCE` varchar(10) DEFAULT NULL,
  `CONTRIBUTOR_ROLE` varchar(50) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ORCID_WORK_CONTRIBUTORS_ID`),
  KEY `ORCID_WORK_CONTRIBUTORS_FK1` (`PUT_CODE`),
  CONSTRAINT `ORCID_WORK_CONTRIBUTORS_FK1` FOREIGN KEY (`PUT_CODE`) REFERENCES `orcid_work` (`PUT_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `orcid_work_external_identifier` (
  `ORCID_WORK_EXTERNAL_IDENTIFIER_ID` int NOT NULL AUTO_INCREMENT,
  `PUT_CODE` int DEFAULT NULL,
  `IDENTIFIER_TYPE` varchar(100) DEFAULT NULL,
  `IDENTIFIER_VALUE` varchar(1000) DEFAULT NULL,
  `IDENTIFIER_URL` varchar(1000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ORCID_WORK_EXTERNAL_IDENTIFIER_ID`),
  KEY `ORCID_WORK_EXTERNAL_IDENTIFIER_FK1` (`PUT_CODE`),
  CONSTRAINT `ORCID_WORK_EXTERNAL_IDENTIFIER_FK1` FOREIGN KEY (`PUT_CODE`) REFERENCES `orcid_work` (`PUT_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `orcid_work_status` (
  `ORCID_WORK_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ORCID_WORK_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `orcid_work_type` (
  `ORCID_WORK_TYPE_CODE` varchar(30) NOT NULL,
  `ORCID_WORK_CATEGORY_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `SORT_ORDER` int DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ORCID_WORK_TYPE_CODE`,`ORCID_WORK_CATEGORY_CODE`),
  KEY `ORCID_WORK_TYPE_FK1` (`ORCID_WORK_CATEGORY_CODE`),
  CONSTRAINT `ORCID_WORK_TYPE_FK1` FOREIGN KEY (`ORCID_WORK_CATEGORY_CODE`) REFERENCES `orcid_work_category` (`ORCID_WORK_CATEGORY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `organization` (
  `ORGANIZATION_ID` varchar(20) NOT NULL,
  `ORGANIZATION_NAME` varchar(200) DEFAULT NULL,
  `CONTACT_ADDRESS_ID` int DEFAULT NULL,
  `ADDRESS` varchar(400) DEFAULT NULL,
  `CABLE_ADDRESS` varchar(20) DEFAULT NULL,
  `TELEX_NUMBER` varchar(20) DEFAULT NULL,
  `COUNTY` varchar(30) DEFAULT NULL,
  `CONGRESSIONAL_DISTRICT` varchar(50) DEFAULT NULL,
  `INCORPORATED_IN` varchar(50) DEFAULT NULL,
  `INCORPORATED_DATE` datetime DEFAULT NULL,
  `NUMBER_OF_EMPLOYEES` int DEFAULT NULL,
  `IRS_TAX_EXCEMPTION` varchar(30) DEFAULT NULL,
  `FEDRAL_EMPLOYER_ID` varchar(15) DEFAULT NULL,
  `MASS_TAX_EXCEMPT_NUM` varchar(30) DEFAULT NULL,
  `AGENCY_SYMBOL` varchar(30) DEFAULT NULL,
  `VENDOR_CODE` varchar(30) DEFAULT NULL,
  `COM_GOV_ENTITY_CODE` varchar(30) DEFAULT NULL,
  `MASS_EMPLOYEE_CLAIM` varchar(30) DEFAULT NULL,
  `DUNS_NUMBER` varchar(20) DEFAULT NULL,
  `DUNS_PLUS_FOUR_NUMBER` varchar(20) DEFAULT NULL,
  `DODAC_NUMBER` varchar(20) DEFAULT NULL,
  `CAGE_NUMBER` varchar(20) DEFAULT NULL,
  `HUMAN_SUB_ASSURANCE` varchar(30) DEFAULT NULL,
  `ANIMAL_WELFARE_ASSURANCE` varchar(20) DEFAULT NULL,
  `SCIENCE_MISCONDUCT_COMPL_DATE` datetime DEFAULT NULL,
  `PHS_ACOUNT` varchar(30) DEFAULT NULL,
  `NSF_INSTITUTIONAL_CODE` varchar(30) DEFAULT NULL,
  `INDIRECT_COST_RATE_AGREEMENT` varchar(50) DEFAULT NULL,
  `COGNIZANT_AUDITOR` int DEFAULT NULL,
  `ONR_RESIDENT_REP` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `COUNTRY` varchar(255) DEFAULT NULL,
  `IS_PARTNERING_ORGANIZATION` varchar(3) DEFAULT NULL,
  `COUNTRY_CODE` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `CONG_DISTRICT_CODE` int DEFAULT NULL,
  PRIMARY KEY (`ORGANIZATION_ID`),
  KEY `ORGANIZATION_FK1` (`COUNTRY_CODE`),
  KEY `ORGANIZATION_FK2_idx` (`CONG_DISTRICT_CODE`),
  CONSTRAINT `ORGANIZATION_FK1` FOREIGN KEY (`COUNTRY_CODE`) REFERENCES `country` (`COUNTRY_CODE`),
  CONSTRAINT `ORGANIZATION_FK2` FOREIGN KEY (`CONG_DISTRICT_CODE`) REFERENCES `congressional_district` (`CONG_DISTRICT_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `organization_type` (
  `ORGANIZATION_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `IS_DEFAULT_ENTRY_REQUIRED` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`ORGANIZATION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `parameter` (
  `PARAMETER_NAME` varchar(100) NOT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(500) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) NOT NULL,
  PRIMARY KEY (`PARAMETER_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `person` (
  `PERSON_ID` varchar(40) NOT NULL,
  `LAST_NAME` varchar(30) DEFAULT NULL,
  `FIRST_NAME` varchar(30) DEFAULT NULL,
  `MIDDLE_NAME` varchar(30) DEFAULT NULL,
  `FULL_NAME` varchar(90) DEFAULT NULL,
  `PRIOR_NAME` varchar(30) DEFAULT NULL,
  `USER_NAME` varchar(60) DEFAULT NULL,
  `PASSWORD` varchar(400) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(60) DEFAULT NULL,
  `DATE_OF_BIRTH` datetime DEFAULT NULL,
  `AGE` int DEFAULT NULL,
  `AGE_BY_FISCAL_YEAR` int DEFAULT NULL,
  `GENDER` varchar(30) DEFAULT NULL,
  `EDUCATION_LEVEL` varchar(30) DEFAULT NULL,
  `OFFICE_LOCATION` varchar(30) DEFAULT NULL,
  `OFFICE_PHONE` varchar(20) DEFAULT NULL,
  `SECONDRY_OFFICE_LOCATION` varchar(30) DEFAULT NULL,
  `SECONDRY_OFFICE_PHONE` varchar(20) DEFAULT NULL,
  `SCHOOL` varchar(50) DEFAULT NULL,
  `DIRECTORY_DEPARTMENT` varchar(30) DEFAULT NULL,
  `SALUTATION` varchar(30) DEFAULT NULL,
  `COUNTRY_OF_CITIZENSHIP` varchar(30) DEFAULT NULL,
  `PRIMARY_TITLE` varchar(51) DEFAULT NULL,
  `DIRECTORY_TITLE` varchar(50) DEFAULT NULL,
  `HOME_UNIT` varchar(8) DEFAULT NULL,
  `IS_FACULTY` varchar(1) DEFAULT NULL,
  `IS_GRADUATE_STUDENT_STAFF` varchar(1) DEFAULT NULL,
  `IS_RESEARCH_STAFF` varchar(1) DEFAULT NULL,
  `IS_SERVICE_STAFF` varchar(1) DEFAULT NULL,
  `IS_SUPPORT_STAFF` varchar(1) DEFAULT NULL,
  `IS_OTHER_ACCADEMIC_GROUP` varchar(1) DEFAULT NULL,
  `IS_MEDICAL_STAFF` varchar(1) DEFAULT NULL,
  `ADDRESS_LINE_1` varchar(80) DEFAULT NULL,
  `ADDRESS_LINE_2` varchar(80) DEFAULT NULL,
  `ADDRESS_LINE_3` varchar(80) DEFAULT NULL,
  `CITY` varchar(30) DEFAULT NULL,
  `COUNTY` varchar(30) DEFAULT NULL,
  `STATE` varchar(30) DEFAULT NULL,
  `POSTAL_CODE` varchar(15) DEFAULT NULL,
  `COUNTRY_CODE` varchar(3) DEFAULT NULL,
  `FAX_NUMBER` varchar(20) DEFAULT NULL,
  `PAGER_NUMBER` varchar(20) DEFAULT NULL,
  `MOBILE_PHONE_NUMBER` varchar(20) DEFAULT NULL,
  `STATUS` varchar(1) NOT NULL,
  `SALARY_ANNIVERSARY_DATE` datetime DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  `SUPERVISOR_PERSON_ID` varchar(255) DEFAULT NULL,
  `ORCID_ID` varchar(30) DEFAULT NULL,
  `ACCESS_TOKEN` varchar(60) DEFAULT NULL,
  `IS_WEBHOOK_ACTIVE` varchar(1) DEFAULT NULL,
  `JOB_CODE` varchar(255) DEFAULT NULL,
  `DATE_WHEN_PERSON_INACTIVE` datetime DEFAULT NULL,
  `IS_EXTERNAL_USER` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`PERSON_ID`),
  KEY `PERSON_FK1` (`HOME_UNIT`),
  KEY `PERSON_IDX1` (`USER_NAME`),
  KEY `PERSON_COUNTRY_FK2` (`COUNTRY_CODE`),
  KEY `PERSON_COUNTRY_FK1` (`COUNTRY_OF_CITIZENSHIP`),
  CONSTRAINT `PERSON_COUNTRY_FK1` FOREIGN KEY (`COUNTRY_OF_CITIZENSHIP`) REFERENCES `country` (`COUNTRY_CODE`),
  CONSTRAINT `PERSON_COUNTRY_FK2` FOREIGN KEY (`COUNTRY_CODE`) REFERENCES `country` (`COUNTRY_CODE`),
  CONSTRAINT `PERSON_FK1` FOREIGN KEY (`HOME_UNIT`) REFERENCES `unit` (`UNIT_NUMBER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `person_degree` (
  `PERSON_DEGREE_ID` int NOT NULL AUTO_INCREMENT,
  `PERSON_ID` varchar(40) NOT NULL,
  `DEGREE_CODE` varchar(10) DEFAULT NULL,
  `DEGREE` varchar(200) DEFAULT NULL,
  `FIELD_OF_STUDY` varchar(200) DEFAULT NULL,
  `SPECIALIZATION` varchar(200) DEFAULT NULL,
  `SCHOOL` varchar(200) DEFAULT NULL,
  `SCHOOL_ID_CODE` varchar(3) DEFAULT NULL,
  `SCHOOL_ID` varchar(20) DEFAULT NULL,
  `GRADUATION_DATE` varchar(20) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`PERSON_DEGREE_ID`),
  KEY `DEGREE_CODE_FK4` (`DEGREE_CODE`),
  KEY `PERSON_ID_FK9` (`PERSON_ID`),
  CONSTRAINT `DEGREE_CODE_FK4` FOREIGN KEY (`DEGREE_CODE`) REFERENCES `degree_type` (`DEGREE_CODE`),
  CONSTRAINT `PERSON_ID_FK4` FOREIGN KEY (`PERSON_ID`) REFERENCES `person` (`PERSON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `person_entity` (
  `PERSON_ENTITY_ID` int NOT NULL AUTO_INCREMENT,
  `PERSON_ID` varchar(45) DEFAULT NULL,
  `ENTITY_ID` int DEFAULT NULL,
  `ENTITY_NUMBER` int DEFAULT NULL,
  `IS_RELATIONSHIP_ACTIVE` varchar(1) DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `VERSION_STATUS` varchar(45) DEFAULT NULL,
  `SPONSORS_RESEARCH` varchar(1) DEFAULT NULL,
  `INVOLVEMENT_START_DATE` date DEFAULT NULL,
  `INVOLVEMENT_END_DATE` date DEFAULT NULL,
  `STUDENT_INVOLVEMENT` varchar(4000) DEFAULT NULL,
  `STAFF_INVOLVEMENT` varchar(4000) DEFAULT NULL,
  `INSTITUTE_RESOURCE_INVOLVEMENT` varchar(4000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`PERSON_ENTITY_ID`),
  KEY `PERSON_ENTITY_FK1_idx` (`PERSON_ID`),
  KEY `PERSON_ENTITY_FK2_idx` (`ENTITY_ID`),
  CONSTRAINT `PERSON_ENTITY_FK1` FOREIGN KEY (`PERSON_ID`) REFERENCES `person` (`PERSON_ID`),
  CONSTRAINT `PERSON_ENTITY_FK2` FOREIGN KEY (`ENTITY_ID`) REFERENCES `entity` (`ENTITY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `person_entity_rel_type` (
  `RELATIONSHIP_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`RELATIONSHIP_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `person_entity_relationship` (
  `PERSON_ENTITY_REL_ID` int NOT NULL AUTO_INCREMENT,
  `PERSON_ENTITY_ID` int DEFAULT NULL,
  `VALID_PERSON_ENTITY_REL_TYPE_CODE` int DEFAULT NULL,
  `QUESTIONNAIRE_ANS_HEADER_ID` int DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `START_DATE` date DEFAULT NULL,
  `END_DATE` date DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`PERSON_ENTITY_REL_ID`),
  KEY `PERSON_ENTITY_RELATIONSHIP_FK1_idx` (`PERSON_ENTITY_ID`),
  KEY `PERSON_ENTITY_RELATIONSHIP_FK2_idx` (`VALID_PERSON_ENTITY_REL_TYPE_CODE`),
  CONSTRAINT `PERSON_ENTITY_RELATIONSHIP_FK1` FOREIGN KEY (`PERSON_ENTITY_ID`) REFERENCES `person_entity` (`PERSON_ENTITY_ID`),
  CONSTRAINT `PERSON_ENTITY_RELATIONSHIP_FK2` FOREIGN KEY (`VALID_PERSON_ENTITY_REL_TYPE_CODE`) REFERENCES `valid_person_entity_rel_type` (`VALID_PERSON_ENTITY_REL_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `person_login_details` (
  `LOGIN_DETAIL_ID` int NOT NULL AUTO_INCREMENT,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `LOGIN_STATUS` varchar(10) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `FULL_NAME` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`LOGIN_DETAIL_ID`),
  KEY `PERSON_LOGIN_DETAILS_FK1` (`PERSON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `person_orcid_webhook_flag` (
  `PERSON_ID` varchar(40) NOT NULL,
  `IS_REGISTERED_FLAG` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`PERSON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `person_orcid_work` (
  `PERSON_ORCID_WORK_ID` int NOT NULL AUTO_INCREMENT,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `ORCID_ID` varchar(30) DEFAULT NULL,
  `PUT_CODE` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`PERSON_ORCID_WORK_ID`),
  KEY `PERSON_ORCID_WORK_FK1` (`PUT_CODE`),
  CONSTRAINT `PERSON_ORCID_WORK_FK1` FOREIGN KEY (`PUT_CODE`) REFERENCES `orcid_work` (`PUT_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `person_preference` (
  `PERSON_ID` varchar(40) NOT NULL,
  `PREFERENCES_TYPE_CODE` varchar(5) NOT NULL,
  `VALUE` varchar(30) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `PREFERENCE_ID` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`PREFERENCE_ID`),
  KEY `PERSON_PREFERENCE_FK1` (`PREFERENCES_TYPE_CODE`),
  CONSTRAINT `PERSON_PREFERENCE_FK1` FOREIGN KEY (`PREFERENCES_TYPE_CODE`) REFERENCES `preference_type` (`PREFERENCES_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `person_role_module` (
  `ROLE_TYPE_CODE` int NOT NULL,
  `MODULE_CODE` int NOT NULL,
  `SUB_MODULE_CODE` int NOT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ROLE_TYPE_CODE`,`MODULE_CODE`,`SUB_MODULE_CODE`),
  CONSTRAINT `PERSON_ROLE_MODULE_FK1` FOREIGN KEY (`ROLE_TYPE_CODE`) REFERENCES `person_role_type` (`ROLE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `person_role_rt` (
  `PERSON_ID` varchar(90) NOT NULL,
  `UNIT_NUMBER` varchar(8) NOT NULL,
  `RIGHT_NAME` varchar(50) NOT NULL,
  `ROLE_ID` int NOT NULL,
  PRIMARY KEY (`PERSON_ID`,`UNIT_NUMBER`,`RIGHT_NAME`,`ROLE_ID`),
  KEY `PERSON_ROLE_RT_IX1` (`PERSON_ID`),
  KEY `PERSON_ROLE_RT_IX2` (`RIGHT_NAME`),
  KEY `PERSON_ROLE_RT_IX3` (`PERSON_ID`,`UNIT_NUMBER`,`RIGHT_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `person_role_type` (
  `ROLE_TYPE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`ROLE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `person_roles` (
  `PERSON_ROLES_ID` int NOT NULL AUTO_INCREMENT,
  `PERSON_ID` varchar(40) NOT NULL,
  `ROLE_ID` int NOT NULL,
  `UNIT_NUMBER` varchar(8) NOT NULL,
  `DESCEND_FLAG` varchar(1) NOT NULL,
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  PRIMARY KEY (`PERSON_ROLES_ID`),
  KEY `PERSON_ROLES_FK1` (`ROLE_ID`),
  KEY `PERSON_ROLE_IX1` (`PERSON_ID`),
  KEY `PERSON_ROLE_IX2` (`UNIT_NUMBER`),
  CONSTRAINT `PERSON_ROLES_FK1` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ROLE_ID`),
  CONSTRAINT `PERSON_ROLES_FK2` FOREIGN KEY (`PERSON_ID`) REFERENCES `person` (`PERSON_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1118 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `person_rt` (
  `PERSON_RT_ID` int NOT NULL AUTO_INCREMENT,
  `ADDRESS_LINE_1` varchar(255) DEFAULT NULL,
  `CITY` varchar(255) DEFAULT NULL,
  `DEGREE` varchar(255) DEFAULT NULL,
  `DIRECTORY_DEPARTMENT` varchar(255) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `FIRST_NAME` varchar(255) DEFAULT NULL,
  `HOME_UNIT` varchar(255) DEFAULT NULL,
  `LAST_NAME` varchar(255) DEFAULT NULL,
  `OFFICE_LOCATION` varchar(255) DEFAULT NULL,
  `OFFICE_PHONE` varchar(255) DEFAULT NULL,
  `PERSON_ID` varchar(255) DEFAULT NULL,
  `PRIMARY_TITLE` varchar(255) DEFAULT NULL,
  `RELATIONSHIP` varchar(255) DEFAULT NULL,
  `STATE_PROVINCE` varchar(255) DEFAULT NULL,
  `USER_NAME` varchar(255) DEFAULT NULL,
  `ZIP_CODE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PERSON_RT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `person_system_notification_mapping` (
  `PERSON_ID` varchar(40) NOT NULL,
  `SYSTEM_NOTIFICATION_ID` int NOT NULL,
  `READ_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`PERSON_ID`,`SYSTEM_NOTIFICATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `person_training` (
  `PERSON_TRAINING_ID` int NOT NULL AUTO_INCREMENT,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `DATE_ACKNOWLEDGED` datetime(6) DEFAULT NULL,
  `DATE_REQUESTED` datetime(6) DEFAULT NULL,
  `DATE_SUBMITTED` datetime(6) DEFAULT NULL,
  `FOLLOWUP_DATE` datetime(6) DEFAULT NULL,
  `PERSON_ID` varchar(255) DEFAULT NULL,
  `SCORE` varchar(255) DEFAULT NULL,
  `TRAINING_CODE` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `IS_NON_EMPLOYEE` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`PERSON_TRAINING_ID`),
  KEY `PERSON_TRAINING_FK1` (`TRAINING_CODE`),
  CONSTRAINT `PERSON_TRAINING_FK1` FOREIGN KEY (`TRAINING_CODE`) REFERENCES `training` (`TRAINING_CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `person_training_attachment` (
  `TRAINING_ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `PERSON_TRAINING_ID` int DEFAULT NULL,
  `DESCRIPTION` varchar(2000) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `MIME_TYPE` varchar(100) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `FILE_DATA_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`TRAINING_ATTACHMENT_ID`),
  KEY `PERSON_TRAING_ATTACHMNT_FK1` (`PERSON_TRAINING_ID`),
  CONSTRAINT `PERSON_TRAING_ATTACHMNT_FK1` FOREIGN KEY (`PERSON_TRAINING_ID`) REFERENCES `person_training` (`PERSON_TRAINING_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `person_training_comment` (
  `TRAINING_COMMENT_ID` int NOT NULL AUTO_INCREMENT,
  `PERSON_TRAINING_ID` int DEFAULT NULL,
  `COMMENT` varchar(2000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`TRAINING_COMMENT_ID`),
  KEY `PERSON_TRAINING_COMMENT_FK1` (`PERSON_TRAINING_ID`),
  CONSTRAINT `PERSON_TRAINING_COMMENT_FK1` FOREIGN KEY (`PERSON_TRAINING_ID`) REFERENCES `person_training` (`PERSON_TRAINING_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `pi_sup_org_mapping` (
  `PI_SUP_ORG_MAPPING_ID` int NOT NULL,
  `PI_PERSON_ID` varchar(255) DEFAULT NULL,
  `SUP_ORG_ID` varchar(255) DEFAULT NULL,
  `SUPERIOR_SUP_ORG_ID` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PI_SUP_ORG_MAPPING_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `pre_review` (
  `PRE_REVIEW_ID` int NOT NULL AUTO_INCREMENT,
  `COMPLETION_DATE` datetime(6) DEFAULT NULL,
  `MODULE_ITEM_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(255) DEFAULT NULL,
  `MODULE_SUB_ITEM_CODE` int DEFAULT NULL,
  `MODULE_SUB_ITEM_KEY` varchar(255) DEFAULT NULL,
  `REQUEST_DATE` datetime(6) DEFAULT NULL,
  `REQUESTOR_COMMENT` varchar(4000) DEFAULT NULL,
  `REQUESTOR_EMAIL` varchar(255) DEFAULT NULL,
  `REQUESTOR_FULLNAME` varchar(255) DEFAULT NULL,
  `REQUESTOR_PERSON_ID` varchar(255) DEFAULT NULL,
  `PRE_REVIEW_SECTION_TYPE_CODE` varchar(255) DEFAULT NULL,
  `PRE_REVIEW_STATUS_CODE` varchar(255) DEFAULT NULL,
  `PRE_REVIEW_TYPE_CODE` varchar(255) DEFAULT NULL,
  `REVIEWER_EMAIL` varchar(255) DEFAULT NULL,
  `REVIEWER_FULLNAME` varchar(255) DEFAULT NULL,
  `REVIEWER_PERSON_ID` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PRE_REVIEW_ID`),
  KEY `PRE_REVIEW_FK3` (`PRE_REVIEW_SECTION_TYPE_CODE`),
  KEY `PRE_REVIEW_FK2` (`PRE_REVIEW_STATUS_CODE`),
  KEY `PRE_REVIEW_FK1` (`PRE_REVIEW_TYPE_CODE`),
  KEY `PRE_REVIEW_IX7` (`MODULE_ITEM_KEY`),
  KEY `PRE_REVIEW_IX14` (`MODULE_ITEM_CODE`),
  KEY `PRE_REVIEW_IX15` (`MODULE_SUB_ITEM_CODE`),
  CONSTRAINT `PRE_REVIEW_FK1` FOREIGN KEY (`PRE_REVIEW_TYPE_CODE`) REFERENCES `pre_review_type` (`PRE_REVIEW_TYPE_CODE`),
  CONSTRAINT `PRE_REVIEW_FK2` FOREIGN KEY (`PRE_REVIEW_STATUS_CODE`) REFERENCES `pre_review_status` (`PRE_REVIEW_STATUS_CODE`),
  CONSTRAINT `PRE_REVIEW_FK3` FOREIGN KEY (`PRE_REVIEW_SECTION_TYPE_CODE`) REFERENCES `pre_review_section_type` (`PRE_REVIEW_SECTION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `pre_review_attachment` (
  `PRE_REVIEW_ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `FILE_DATA_ID` varchar(255) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `MIME_TYPE` varchar(255) DEFAULT NULL,
  `PRE_REVIEW_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `PRE_REVIEW_COMMENT_ID` int NOT NULL,
  PRIMARY KEY (`PRE_REVIEW_ATTACHMENT_ID`),
  KEY `PRE_REVIEW_ATTACHMENT_FK1` (`PRE_REVIEW_COMMENT_ID`),
  CONSTRAINT `PRE_REVIEW_ATTACHMENT_FK1` FOREIGN KEY (`PRE_REVIEW_COMMENT_ID`) REFERENCES `pre_review_comment` (`PRE_REVIEW_COMMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `pre_review_attachment_file` (
  `FILE_DATA_ID` varchar(255) NOT NULL,
  `ATTACHMENT` longblob,
  PRIMARY KEY (`FILE_DATA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `pre_review_comment` (
  `PRE_REVIEW_COMMENT_ID` int NOT NULL AUTO_INCREMENT,
  `IS_PRIVATE_COMMENT` varchar(255) DEFAULT NULL,
  `REVIEW_COMMENT` varchar(4000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `PRE_REVIEW_ID` int NOT NULL,
  `FULL_NAME` varchar(255) DEFAULT NULL,
  `PERSON_ID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PRE_REVIEW_COMMENT_ID`),
  KEY `PRE_REVIEW_COMMENT_FK1` (`PRE_REVIEW_ID`),
  CONSTRAINT `PRE_REVIEW_COMMENT_FK1` FOREIGN KEY (`PRE_REVIEW_ID`) REFERENCES `pre_review` (`PRE_REVIEW_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `pre_review_section_type` (
  `PRE_REVIEW_SECTION_TYPE_CODE` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `PRE_REVIEW_TYPE_CODE` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`PRE_REVIEW_SECTION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `pre_review_status` (
  `PRE_REVIEW_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(50) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`PRE_REVIEW_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `pre_review_type` (
  `PRE_REVIEW_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(50) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`PRE_REVIEW_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `preference_type` (
  `PREFERENCES_TYPE_CODE` varchar(5) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`PREFERENCES_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `progress_report_achievement_type` (
  `ACHIEVEMENT_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(300) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `SECTION_TYPE` varchar(1) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `TEMPLATE_DESCRIPTION` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ACHIEVEMENT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `progress_report_attachmnt_type` (
  `ATTACHMENT_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(300) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `IS_PRIVATE` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`ATTACHMENT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `progress_report_kpi_cash_funding` (
  `KPI_CASH_FUNDING_ID` int NOT NULL AUTO_INCREMENT,
  `KPI_SUMMARY_ID` int DEFAULT NULL,
  `PROGRESS_REPORT_ID` int DEFAULT NULL,
  `KPI_CRITERIA_TYPE_CODE` varchar(3) DEFAULT NULL,
  `NAME_OF_COMPANY` varchar(200) DEFAULT NULL,
  `COUNTRY_CODE` varchar(3) DEFAULT NULL,
  `DATE_OF_CONTRIBUTION` datetime DEFAULT NULL,
  `COMPANY_UEN` varchar(200) DEFAULT NULL,
  `AMOUNT` decimal(12,2) DEFAULT NULL,
  `COMMENTS` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`KPI_CASH_FUNDING_ID`),
  KEY `KPI_CASH_FUNDING_FK1` (`COUNTRY_CODE`),
  CONSTRAINT `KPI_CASH_FUNDING_FK1` FOREIGN KEY (`COUNTRY_CODE`) REFERENCES `country` (`COUNTRY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `progress_report_kpi_collaboration_projects` (
  `KPI_COLLABORATION_PROJECTS_ID` int NOT NULL AUTO_INCREMENT,
  `KPI_SUMMARY_ID` int DEFAULT NULL,
  `PROGRESS_REPORT_ID` int DEFAULT NULL,
  `KPI_CRITERIA_TYPE_CODE` varchar(3) DEFAULT NULL,
  `PROJECT_TITLE` varchar(200) DEFAULT NULL,
  `PROJECT_DESCRIPTION` varchar(200) DEFAULT NULL,
  `PROJECT_START_DATE` datetime DEFAULT NULL,
  `PROJECT_END_DATE` datetime DEFAULT NULL,
  `COLLABORATING_ORGANIZATION` varchar(200) DEFAULT NULL,
  `COUNTRY_CODE` varchar(200) DEFAULT NULL,
  `COMPANY_UEN` varchar(100) DEFAULT NULL,
  `COMMENTS` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`KPI_COLLABORATION_PROJECTS_ID`),
  KEY `KPI_COLLABORATION_PROJECTS_FK1` (`COUNTRY_CODE`),
  CONSTRAINT `KPI_COLLABORATION_PROJECTS_FK1` FOREIGN KEY (`COUNTRY_CODE`) REFERENCES `country` (`COUNTRY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `progress_report_kpi_competitive_grants` (
  `KPI_COMPETITIVE_GRANTS_ID` int NOT NULL AUTO_INCREMENT,
  `KPI_SUMMARY_ID` int DEFAULT NULL,
  `PROGRESS_REPORT_ID` int DEFAULT NULL,
  `KPI_CRITERIA_TYPE_CODE` varchar(3) DEFAULT NULL,
  `PROJECT_TITLE` varchar(200) DEFAULT NULL,
  `PROJECT_DESCRIPTION` varchar(200) DEFAULT NULL,
  `PROJECT_START_DATE` datetime DEFAULT NULL,
  `PROJECT_END_DATE` datetime DEFAULT NULL,
  `NAME_OF_GRANT_RECEIVED` varchar(200) DEFAULT NULL,
  `PROJECT_REFERENCE_NO` varchar(30) DEFAULT NULL,
  `SPONSOR_CODE` varchar(100) DEFAULT NULL,
  `RECIPIENT_OF_GRANT` varchar(100) DEFAULT NULL,
  `HOST_INSTITUTION` varchar(100) DEFAULT NULL,
  `DIRECT_COST` decimal(12,2) DEFAULT NULL,
  `INDIRECT_COST` decimal(12,2) DEFAULT NULL,
  `COMMENTS` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`KPI_COMPETITIVE_GRANTS_ID`),
  KEY `KPI_COMPETITIVE_GRANTS_FK1` (`SPONSOR_CODE`),
  CONSTRAINT `KPI_COMPETITIVE_GRANTS_FK1` FOREIGN KEY (`SPONSOR_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `progress_report_kpi_conference_presentation` (
  `KPI_CONFERENCE_PRESENTATION_ID` int NOT NULL AUTO_INCREMENT,
  `KPI_SUMMARY_ID` int DEFAULT NULL,
  `PROGRESS_REPORT_ID` int DEFAULT NULL,
  `KPI_CRITERIA_TYPE_CODE` varchar(3) DEFAULT NULL,
  `NAME_OF_PRESENTER` varchar(200) DEFAULT NULL,
  `TITLE` varchar(300) DEFAULT NULL,
  `CONFERENCE_TITLE` varchar(200) DEFAULT NULL,
  `ORGANISER` varchar(200) DEFAULT NULL,
  `CONFERENCE_LOCATION` varchar(200) DEFAULT NULL,
  `DATE` datetime DEFAULT NULL,
  `COMMENTS` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`KPI_CONFERENCE_PRESENTATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `progress_report_kpi_grant_specific` (
  `KPI_GRANT_SPECIFIC_ID` int NOT NULL AUTO_INCREMENT,
  `KPI_SUMMARY_ID` int DEFAULT NULL,
  `PROGRESS_REPORT_ID` int DEFAULT NULL,
  `KPI_CRITERIA_TYPE_CODE` varchar(3) DEFAULT NULL,
  `TYPE` varchar(200) DEFAULT NULL,
  `COMMENTS` varchar(200) DEFAULT NULL,
  `DATE` datetime DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`KPI_GRANT_SPECIFIC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `progress_report_kpi_health_specific_outcomes` (
  `KPI_HEALTH_SPECIFIC_OUTCOMES_ID` int NOT NULL AUTO_INCREMENT,
  `KPI_SUMMARY_ID` int DEFAULT NULL,
  `PROGRESS_REPORT_ID` int DEFAULT NULL,
  `KPI_CRITERIA_TYPE_CODE` varchar(3) DEFAULT NULL,
  `TITLE` varchar(200) DEFAULT NULL,
  `DATE_ESTABLISHED` datetime DEFAULT NULL,
  `NUMBER_OF_LIFE_YEARS` varchar(10) DEFAULT NULL,
  `COMMENTS` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`KPI_HEALTH_SPECIFIC_OUTCOMES_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `progress_report_kpi_impact_publications` (
  `KPI_IMPACT_PUBLICATIONS_ID` int NOT NULL AUTO_INCREMENT,
  `KPI_SUMMARY_ID` int DEFAULT NULL,
  `PROGRESS_REPORT_ID` int DEFAULT NULL,
  `KPI_CRITERIA_TYPE_CODE` varchar(3) DEFAULT NULL,
  `PUBLICATION_STATUS_CODE` varchar(20) DEFAULT NULL,
  `AUTHOR_NAME` varchar(2000) DEFAULT NULL,
  `TITLE_OF_ARTICLE` varchar(200) DEFAULT NULL,
  `JOURNAL_NAME` varchar(200) DEFAULT NULL,
  `PUBLISHER` varchar(5000) DEFAULT NULL,
  `YEAR` varchar(30) DEFAULT NULL,
  `PAGE_NO` varchar(10) DEFAULT NULL,
  `IMPACT_FACTOR` varchar(200) DEFAULT NULL,
  `FUNDING_ACKNOWLEDGEMENT` varchar(200) DEFAULT NULL,
  `COMMENTS` varchar(200) DEFAULT NULL,
  `PUBLICATION_DATE` datetime DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `PUT_CODE` int DEFAULT NULL,
  PRIMARY KEY (`KPI_IMPACT_PUBLICATIONS_ID`),
  KEY `KPI_IMPACT_PUBLICATIONS_FK1` (`PUBLICATION_STATUS_CODE`),
  CONSTRAINT `KPI_IMPACT_PUBLICATIONS_FK1` FOREIGN KEY (`PUBLICATION_STATUS_CODE`) REFERENCES `kpi_publication_status` (`PUBLICATION_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `progress_report_kpi_inkind_contributions` (
  `KPI_INKIND_CONTRIBUTIONS_ID` int NOT NULL AUTO_INCREMENT,
  `KPI_SUMMARY_ID` int DEFAULT NULL,
  `PROGRESS_REPORT_ID` int DEFAULT NULL,
  `KPI_CRITERIA_TYPE_CODE` varchar(3) DEFAULT NULL,
  `NAME_OF_COMPANY` varchar(200) DEFAULT NULL,
  `COUNTRY_CODE` varchar(3) DEFAULT NULL,
  `DATE_OF_CONTRIBUTION` datetime DEFAULT NULL,
  `COMPANY_UEN` varchar(200) DEFAULT NULL,
  `AMOUNT` decimal(12,2) DEFAULT NULL,
  `COMMENTS` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`KPI_INKIND_CONTRIBUTIONS_ID`),
  KEY `KPI_INKIND_CONTRIBUTIONS_FK1` (`COUNTRY_CODE`),
  CONSTRAINT `KPI_INKIND_CONTRIBUTIONS_FK1` FOREIGN KEY (`COUNTRY_CODE`) REFERENCES `country` (`COUNTRY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `progress_report_kpi_licenses` (
  `KPI_LICENSES_ID` int NOT NULL AUTO_INCREMENT,
  `KPI_SUMMARY_ID` int DEFAULT NULL,
  `PROGRESS_REPORT_ID` int DEFAULT NULL,
  `KPI_CRITERIA_TYPE_CODE` varchar(3) DEFAULT NULL,
  `NAME_OF_LICENSE` varchar(200) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `COMPANY_UEN` varchar(200) DEFAULT NULL,
  `LICENSING_PERIOD` varchar(3) DEFAULT NULL,
  `DETAILS_OF_LICENSE` varchar(200) DEFAULT NULL,
  `COMMENTS` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`KPI_LICENSES_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `progress_report_kpi_manpower_development` (
  `KPI_MANPOWER_DEVELOPMENT_ID` int NOT NULL AUTO_INCREMENT,
  `KPI_SUMMARY_ID` int DEFAULT NULL,
  `PROGRESS_REPORT_ID` int DEFAULT NULL,
  `KPI_CRITERIA_TYPE_CODE` varchar(3) DEFAULT NULL,
  `NAME_OF_STUDENT` varchar(200) DEFAULT NULL,
  `CITIZENSHIP` varchar(200) DEFAULT NULL,
  `CURRENT_STATUS_CODE` varchar(3) DEFAULT NULL,
  `DATE_ENROLLED` datetime DEFAULT NULL,
  `DATE_GRADUATED` datetime DEFAULT NULL,
  `DATE_OF_JOINING` datetime DEFAULT NULL,
  `DATE_OF_LEAVING` datetime DEFAULT NULL,
  `COMMENTS` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`KPI_MANPOWER_DEVELOPMENT_ID`),
  KEY `KPI_MANPOWER_DEVELOPMENT_FK1` (`CURRENT_STATUS_CODE`),
  CONSTRAINT `KPI_MANPOWER_DEVELOPMENT_FK1` FOREIGN KEY (`CURRENT_STATUS_CODE`) REFERENCES `kpi_manpower_development_current_status` (`CURRENT_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `progress_report_kpi_mapping` (
  `KPI_TYPE_CODE` varchar(3) NOT NULL,
  `KPI_CRITERIA_TYPE_CODE` varchar(3) NOT NULL,
  `TEMPLATE_TABLE` varchar(60) DEFAULT NULL,
  `SECTION_CODE` varchar(10) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`KPI_TYPE_CODE`,`KPI_CRITERIA_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `progress_report_kpi_patents` (
  `KPI_PATENTS_ID` int NOT NULL AUTO_INCREMENT,
  `KPI_SUMMARY_ID` int DEFAULT NULL,
  `PROGRESS_REPORT_ID` int DEFAULT NULL,
  `KPI_CRITERIA_TYPE_CODE` varchar(3) DEFAULT NULL,
  `TITLE` varchar(200) DEFAULT NULL,
  `DATE_FILED` datetime DEFAULT NULL,
  `DATE_GRANTED` datetime DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `OWNERSHIP` varchar(200) DEFAULT NULL,
  `PATENT_NUMBER` varchar(200) DEFAULT NULL,
  `COMMENTS` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`KPI_PATENTS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `progress_report_kpi_post_docs_employed` (
  `KPI_POST_DOCS_EMPLOYED_ID` int NOT NULL AUTO_INCREMENT,
  `KPI_SUMMARY_ID` int DEFAULT NULL,
  `PROGRESS_REPORT_ID` int DEFAULT NULL,
  `KPI_CRITERIA_TYPE_CODE` varchar(3) DEFAULT NULL,
  `EMPLOYEE_NAME` varchar(90) DEFAULT NULL,
  `EMPLOYMENT_START_DATE` datetime DEFAULT NULL,
  `NATIONALITY` varchar(30) DEFAULT NULL,
  `PERMANENT_RESIDENCE` varchar(30) DEFAULT NULL,
  `IDENTIFICATION_NUMBER` varchar(50) DEFAULT NULL,
  `COMMENTS` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`KPI_POST_DOCS_EMPLOYED_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `progress_report_kpi_successful_startups` (
  `KPI_SUCCESSFUL_STARTUPS_ID` int NOT NULL AUTO_INCREMENT,
  `KPI_SUMMARY_ID` int DEFAULT NULL,
  `PROGRESS_REPORT_ID` int DEFAULT NULL,
  `KPI_CRITERIA_TYPE_CODE` varchar(3) DEFAULT NULL,
  `NAME_OF_COMPANY` varchar(200) DEFAULT NULL,
  `DATE_OF_ESTABLISHMENT` datetime DEFAULT NULL,
  `DATE_ESTABLISHED` datetime DEFAULT NULL,
  `COMPANY_UEN` varchar(200) DEFAULT NULL,
  `EXTERNAL_FUNDING_CRITERIA` varchar(200) DEFAULT NULL,
  `VALUATION_CRITERIA` varchar(200) DEFAULT NULL,
  `ANNUAL_REVENUE_CRITERIA` varchar(200) DEFAULT NULL,
  `COMMENTS` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`KPI_SUCCESSFUL_STARTUPS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `progress_report_kpi_technologies_deployed` (
  `KPI_TECHNOLOGIES_DEPLOYED_ID` int NOT NULL AUTO_INCREMENT,
  `KPI_SUMMARY_ID` decimal(10,0) DEFAULT NULL,
  `PROGRESS_REPORT_ID` int DEFAULT NULL,
  `KPI_CRITERIA_TYPE_CODE` varchar(3) DEFAULT NULL,
  `NAME_OF_COMPANY` varchar(200) DEFAULT NULL,
  `COUNTRY_CODE` varchar(3) DEFAULT NULL,
  `DATE_OF_DEPLOYING` datetime DEFAULT NULL,
  `COMPANY_UEN` varchar(200) DEFAULT NULL,
  `DETAILS_OF_TECHNOLOGIES` varchar(200) DEFAULT NULL,
  `COMMENTS` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`KPI_TECHNOLOGIES_DEPLOYED_ID`),
  KEY `KPI_TECHNOLOGIES_DEPLOYED_FK1` (`COUNTRY_CODE`),
  CONSTRAINT `KPI_TECHNOLOGIES_DEPLOYED_FK1` FOREIGN KEY (`COUNTRY_CODE`) REFERENCES `country` (`COUNTRY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `progress_report_kpi_technology_disclosure` (
  `KPI_TECHNOLOGY_DISCLOSURE_ID` int NOT NULL AUTO_INCREMENT,
  `KPI_SUMMARY_ID` int DEFAULT NULL,
  `PROGRESS_REPORT_ID` int DEFAULT NULL,
  `KPI_CRITERIA_TYPE_CODE` varchar(3) DEFAULT NULL,
  `TECHNOLOGY_DISCLOSURE_STATUS_CODE` varchar(20) DEFAULT NULL,
  `AUTHOR_NAME` varchar(200) DEFAULT NULL,
  `TITLE_OF_PATENT` varchar(200) DEFAULT NULL,
  `COVERING_COUNTRIES` varchar(200) DEFAULT NULL,
  `FILING_OFFICE` varchar(200) DEFAULT NULL,
  `DATE_OF_FILING` datetime DEFAULT NULL,
  `DATE_OF_AWARD` datetime DEFAULT NULL,
  `COMMENTS` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`KPI_TECHNOLOGY_DISCLOSURE_ID`),
  KEY `KPI_TECHNOLOGY_DISCLOSURE_FK1` (`TECHNOLOGY_DISCLOSURE_STATUS_CODE`),
  CONSTRAINT `KPI_TECHNOLOGY_DISCLOSURE_FK1` FOREIGN KEY (`TECHNOLOGY_DISCLOSURE_STATUS_CODE`) REFERENCES `kpi_technology_disclosure_status` (`TECHNOLOGY_DISCLOSURE_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `progress_report_kpi_undergraduate_student` (
  `KPI_UNDERGRADUATE_STUDENT_ID` int NOT NULL AUTO_INCREMENT,
  `KPI_SUMMARY_ID` int DEFAULT NULL,
  `PROGRESS_REPORT_ID` int DEFAULT NULL,
  `KPI_CRITERIA_TYPE_CODE` varchar(3) DEFAULT NULL,
  `NAME_OF_STUDENT` varchar(200) DEFAULT NULL,
  `CITIZENSHIP` varchar(200) DEFAULT NULL,
  `CURRENT_STATUS_CODE` varchar(3) DEFAULT NULL,
  `DATE_OF_JOINING` datetime DEFAULT NULL,
  `DATE_OF_LEAVING` datetime DEFAULT NULL,
  `COMMENTS` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`KPI_UNDERGRADUATE_STUDENT_ID`),
  KEY `KPI_UNDERGRADUATE_STUDENT_FK1` (`CURRENT_STATUS_CODE`),
  CONSTRAINT `KPI_UNDERGRADUATE_STUDENT_FK1` FOREIGN KEY (`CURRENT_STATUS_CODE`) REFERENCES `kpi_manpower_development_current_status` (`CURRENT_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `progress_report_status` (
  `PROGRESS_REPORT_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`PROGRESS_REPORT_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `progress_reprt_milestone_status` (
  `MILESTONE_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`MILESTONE_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `prop_person_units` (
  `PROP_PERSON_UNIT_ID` int NOT NULL AUTO_INCREMENT,
  `PROPOSAL_PERSON_ID` int DEFAULT NULL,
  `PROPOSAL_ID` int DEFAULT NULL,
  `PROPOSAL_NUMBER` varchar(20) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `LEAD_UNIT_FLAG` varchar(1) DEFAULT NULL,
  `UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `UPDATE_TIMESTAMP` timestamp(6) NULL DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`PROP_PERSON_UNIT_ID`),
  KEY `PROP_PERSON_UNITS_FK1` (`PROPOSAL_PERSON_ID`),
  KEY `PROP_PERSON_UNITS_FK2` (`UNIT_NUMBER`),
  CONSTRAINT `PROP_PERSON_UNITS_FK1` FOREIGN KEY (`PROPOSAL_PERSON_ID`) REFERENCES `proposal_persons` (`PROPOSAL_PERSON_ID`),
  CONSTRAINT `PROP_PERSON_UNITS_FK2` FOREIGN KEY (`UNIT_NUMBER`) REFERENCES `unit` (`UNIT_NUMBER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal` (
  `PROPOSAL_ID` int NOT NULL AUTO_INCREMENT,
  `PROPOSAL_NUMBER` varchar(20) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `ABSTRACT_DESC` text,
  `ACTIVITY_TYPE_CODE` varchar(3) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `DELIVERABLES` text,
  `DETAILS` varchar(4000) DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `FUNDING_STRATEGY` varchar(255) DEFAULT NULL,
  `HOME_UNIT_NAME` varchar(200) DEFAULT NULL,
  `HOME_UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `IP_NUMBER` varchar(30) DEFAULT NULL,
  `RESEARCH_AREA_DESC` varchar(4000) DEFAULT NULL,
  `SPONSOR_CODE` varchar(6) DEFAULT NULL,
  `SPONSOR_PROPOSAL_NUMBER` varchar(100) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `STATUS_CODE` int DEFAULT NULL,
  `SUBMISSION_DATE` datetime DEFAULT NULL,
  `SUBMIT_USER` varchar(60) DEFAULT NULL,
  `TITLE` varchar(1000) DEFAULT NULL,
  `TYPE_CODE` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `BUDGET_HEADER_ID` int DEFAULT NULL,
  `INTERNAL_DEADLINE_DATE` datetime DEFAULT NULL,
  `IS_SUBCONTRACT` varchar(1) DEFAULT NULL,
  `IS_DOMESTIC_SITE` varchar(1) DEFAULT NULL,
  `IS_MULTISITE_STUDY` varchar(1) DEFAULT NULL,
  `GRANT_TYPE_CODE` int DEFAULT NULL,
  `GRANT_HEADER_ID` int DEFAULT NULL,
  `AWARD_TYPE_CODE` varchar(3) DEFAULT NULL,
  `BASE_PROPOSAL_NUMBER` varchar(20) DEFAULT NULL,
  `PROGRAM_ANNOUNCEMENT_NUMBER` varchar(50) DEFAULT NULL,
  `PRIME_SPONSOR_CODE` varchar(6) DEFAULT NULL,
  `CFDA_NUMBER` varchar(100) DEFAULT NULL,
  `TOTAL_COST` decimal(12,2) DEFAULT NULL,
  `TOTAL_DIRECT_COST` decimal(12,2) DEFAULT NULL,
  `TOTAL_INDIRECT_COST` decimal(12,2) DEFAULT NULL,
  `SPONSOR_DEADLINE_DATE` datetime DEFAULT NULL,
  `EXTERNAL_FUNDING_AGENCY_ID` varchar(255) DEFAULT NULL,
  `MULTI_DISCIPLINARY_DESC` varchar(255) DEFAULT NULL,
  `PROPOSAL_SEQUENCE_STATUS` varchar(30) DEFAULT NULL,
  `CLUSTER_CODE` int DEFAULT NULL,
  `DURATION` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PROPOSAL_ID`),
  KEY `PROPOSAL_FK8` (`ACTIVITY_TYPE_CODE`),
  KEY `PROPOSAL_FK2` (`STATUS_CODE`),
  KEY `PROPOSAL_FK1` (`TYPE_CODE`),
  KEY `PROPOSAL_FK9` (`AWARD_TYPE_CODE`),
  KEY `PROPOSAL_FK7` (`BUDGET_HEADER_ID`),
  KEY `PROPOSAL_FK4` (`GRANT_HEADER_ID`),
  KEY `PROPOSAL_IX20` (`PROPOSAL_ID`),
  KEY `PROPOSAL_FK10` (`GRANT_TYPE_CODE`),
  KEY `PROPOSAL_FK11` (`HOME_UNIT_NUMBER`),
  KEY `PROPOSAL_FK12` (`SPONSOR_CODE`),
  KEY `PROPOSAL_FK13` (`PRIME_SPONSOR_CODE`),
  KEY `PROPOSAL_FK14` (`CLUSTER_CODE`),
  CONSTRAINT `PROPOSAL_FK1` FOREIGN KEY (`TYPE_CODE`) REFERENCES `proposal_type` (`TYPE_CODE`),
  CONSTRAINT `PROPOSAL_FK10` FOREIGN KEY (`GRANT_TYPE_CODE`) REFERENCES `grant_call_type` (`GRANT_TYPE_CODE`),
  CONSTRAINT `PROPOSAL_FK11` FOREIGN KEY (`HOME_UNIT_NUMBER`) REFERENCES `unit` (`UNIT_NUMBER`),
  CONSTRAINT `PROPOSAL_FK12` FOREIGN KEY (`SPONSOR_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`),
  CONSTRAINT `PROPOSAL_FK13` FOREIGN KEY (`PRIME_SPONSOR_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`),
  CONSTRAINT `PROPOSAL_FK14` FOREIGN KEY (`CLUSTER_CODE`) REFERENCES `eps_prop_discipline_cluster` (`CLUSTER_CODE`),
  CONSTRAINT `PROPOSAL_FK2` FOREIGN KEY (`STATUS_CODE`) REFERENCES `proposal_status` (`STATUS_CODE`),
  CONSTRAINT `PROPOSAL_FK4` FOREIGN KEY (`GRANT_HEADER_ID`) REFERENCES `grant_call_header` (`GRANT_HEADER_ID`),
  CONSTRAINT `PROPOSAL_FK7` FOREIGN KEY (`BUDGET_HEADER_ID`) REFERENCES `budget_header` (`BUDGET_HEADER_ID`),
  CONSTRAINT `PROPOSAL_FK8` FOREIGN KEY (`ACTIVITY_TYPE_CODE`) REFERENCES `activity_type` (`ACTIVITY_TYPE_CODE`),
  CONSTRAINT `PROPOSAL_FK9` FOREIGN KEY (`AWARD_TYPE_CODE`) REFERENCES `award_type` (`AWARD_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal_action_item` (
  `ACTION_ITEM_ID` varchar(6) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ACTION_ITEM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal_action_log` (
  `ACTION_LOG_ID` int NOT NULL AUTO_INCREMENT,
  `ACTION_TYPE_CODE` varchar(3) DEFAULT NULL,
  `DESCRIPTION` varchar(2000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `PROPOSAL_ID` int DEFAULT NULL,
  PRIMARY KEY (`ACTION_LOG_ID`),
  KEY `PROPOSAL_ACTION_LOG_FK1` (`ACTION_TYPE_CODE`),
  KEY `PROPOSAL_ACTION_LOG_FK2` (`PROPOSAL_ID`),
  CONSTRAINT `APROPOSAL_ACTION_LOG_FK2` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `proposal` (`PROPOSAL_ID`),
  CONSTRAINT `PROPOSAL_ACTION_LOG_FK1` FOREIGN KEY (`ACTION_TYPE_CODE`) REFERENCES `proposal_action_type` (`ACTION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal_action_type` (
  `ACTION_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(6) DEFAULT 'Y',
  `STATUS_CODE` int DEFAULT NULL,
  `ACTION_ITEM_ID` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`ACTION_TYPE_CODE`),
  KEY `PROPOSAL_ACTION_TYPE_FK1` (`STATUS_CODE`),
  KEY `PROPOSAL_ACTION_TYPE_FK2` (`ACTION_ITEM_ID`),
  CONSTRAINT `PROPOSAL_ACTION_TYPE_FK1` FOREIGN KEY (`STATUS_CODE`) REFERENCES `proposal_status` (`STATUS_CODE`),
  CONSTRAINT `PROPOSAL_ACTION_TYPE_FK2` FOREIGN KEY (`ACTION_ITEM_ID`) REFERENCES `proposal_action_item` (`ACTION_ITEM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal_admin_details` (
  `PROPOSAL_ADMIN_DETAIL_ID` int NOT NULL AUTO_INCREMENT,
  `DEV_PROPOSAL_ID` int DEFAULT NULL,
  `INST_PROPOSAL_ID` int DEFAULT NULL,
  `DATE_SUBMITTED_BY_DEPT` datetime DEFAULT NULL,
  `DATE_RETURNED_TO_DEPT` datetime DEFAULT NULL,
  `DATE_APPROVED_BY_OSP` datetime DEFAULT NULL,
  `DATE_SUBMITTED_TO_AGENCY` datetime DEFAULT NULL,
  `INST_PROP_CREATE_DATE` datetime DEFAULT NULL,
  `INST_PROP_CREATE_USER` varchar(60) DEFAULT NULL,
  `SIGNED_BY` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  PRIMARY KEY (`PROPOSAL_ADMIN_DETAIL_ID`),
  KEY `PROPOSAL_ADMIN_DETAILS_FK2` (`INST_PROPOSAL_ID`),
  KEY `PROPOSAL_ADMIN_DETAILS_FK1` (`DEV_PROPOSAL_ID`),
  CONSTRAINT `PROPOSAL_ADMIN_DETAILS_FK1` FOREIGN KEY (`DEV_PROPOSAL_ID`) REFERENCES `eps_proposal` (`PROPOSAL_ID`),
  CONSTRAINT `PROPOSAL_ADMIN_DETAILS_FK2` FOREIGN KEY (`INST_PROPOSAL_ID`) REFERENCES `proposal` (`PROPOSAL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal_attach_type` (
  `ATTACHMNT_TYPE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`ATTACHMNT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal_attachments` (
  `ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `PROPOSAL_ID` int DEFAULT NULL,
  `PROPOSAL_NUMBER` varchar(20) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `ATTACHMNT_TYPE_CODE` int DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `MIME_TYPE` varchar(255) DEFAULT NULL,
  `NARRATIVE_STATUS_CODE` varchar(3) DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `FILE_DATA_ID` varchar(36) DEFAULT NULL,
  `DOCUMENT_STATUS_CODE` int DEFAULT NULL,
  `DOCUMENT_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ATTACHMENT_ID`),
  KEY `PROP_ATTACHMENTS_FK2` (`ATTACHMNT_TYPE_CODE`),
  KEY `PROP_ATTACHMENTS_FK1` (`PROPOSAL_ID`),
  KEY `PROP_ATTACHMENTS_FK3` (`NARRATIVE_STATUS_CODE`),
  KEY `PROPOSAL_ATTACHMENTS_FK5` (`DOCUMENT_STATUS_CODE`),
  CONSTRAINT `PROP_ATTACHMENTS_FK1` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `proposal` (`PROPOSAL_ID`),
  CONSTRAINT `PROP_ATTACHMENTS_FK2` FOREIGN KEY (`ATTACHMNT_TYPE_CODE`) REFERENCES `proposal_attach_type` (`ATTACHMNT_TYPE_CODE`),
  CONSTRAINT `PROP_ATTACHMENTS_FK3` FOREIGN KEY (`NARRATIVE_STATUS_CODE`) REFERENCES `narrative_status` (`NARRATIVE_STATUS_CODE`),
  CONSTRAINT `PROPOSAL_ATTACHMENTS_FK5` FOREIGN KEY (`DOCUMENT_STATUS_CODE`) REFERENCES `document_status` (`DOCUMENT_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal_comments` (
  `PROPOSAL_COMMENT_ID` int NOT NULL AUTO_INCREMENT,
  `PROPOSAL_ID` int DEFAULT NULL,
  `PROPOSAL_NUMBER` varchar(20) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `COMMENT_TYPE_CODE` varchar(3) DEFAULT NULL,
  `COMMENTS` longtext,
  `IS_PRIVATE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`PROPOSAL_COMMENT_ID`),
  KEY `PROPOSAL_COMMENTS_FK1` (`PROPOSAL_ID`),
  KEY `PROPOSAL_COMMENTS_FK2` (`COMMENT_TYPE_CODE`),
  CONSTRAINT `PROPOSAL_COMMENTS_FK1` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `proposal` (`PROPOSAL_ID`),
  CONSTRAINT `PROPOSAL_COMMENTS_FK2` FOREIGN KEY (`COMMENT_TYPE_CODE`) REFERENCES `comment_type` (`COMMENT_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal_funding_status` (
  `FUNDING_STATUS_CODE` int NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`FUNDING_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal_keywords` (
  `KEYWORD_ID` int NOT NULL AUTO_INCREMENT,
  `PROPOSAL_ID` int DEFAULT NULL,
  `PROPOSAL_NUMBER` varchar(20) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `SCIENCE_KEYWORD_CODE` varchar(15) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `KEYWORD` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`KEYWORD_ID`),
  KEY `PROP_KEYWORDS_FK1` (`PROPOSAL_ID`),
  KEY `PROP_KEYWORDS_FK2` (`SCIENCE_KEYWORD_CODE`),
  CONSTRAINT `PROP_KEYWORDS_FK1` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `proposal` (`PROPOSAL_ID`),
  CONSTRAINT `PROP_KEYWORDS_FK2` FOREIGN KEY (`SCIENCE_KEYWORD_CODE`) REFERENCES `science_keyword` (`SCIENCE_KEYWORD_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal_persons` (
  `PROPOSAL_PERSON_ID` int NOT NULL AUTO_INCREMENT,
  `PROPOSAL_ID` int DEFAULT NULL,
  `PROPOSAL_NUMBER` varchar(20) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `DEPARTMENT` varchar(90) DEFAULT NULL,
  `FULL_NAME` varchar(90) DEFAULT NULL,
  `UNIT_NAME` varchar(90) DEFAULT NULL,
  `UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `PROP_PERSON_ROLE_ID` int DEFAULT NULL,
  `ROLODEX_ID` int DEFAULT NULL,
  `PERCENTAGE_OF_EFFORT` decimal(5,2) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `DESIGNATION` varchar(255) DEFAULT NULL,
  `IS_MULTI_PI` varchar(255) DEFAULT NULL,
  `PI_FLAG` varchar(255) DEFAULT NULL,
  `LS_IS_MULTI_PI` varchar(1) DEFAULT NULL,
  `LS_PI_FLAG` varchar(1) DEFAULT NULL,
  `PROJECT_ROLE` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`PROPOSAL_PERSON_ID`),
  KEY `PROPOSAL_PERSONS_FK1` (`PROPOSAL_ID`),
  KEY `PROPOSAL_PERSONS_FK2` (`PROP_PERSON_ROLE_ID`),
  CONSTRAINT `PROPOSAL_PERSONS_FK1` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `proposal` (`PROPOSAL_ID`),
  CONSTRAINT `PROPOSAL_PERSONS_FK2` FOREIGN KEY (`PROP_PERSON_ROLE_ID`) REFERENCES `eps_prop_person_role` (`PROP_PERSON_ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal_persons_attachmnt` (
  `ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `FILE_DATA_ID` varchar(255) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `MIME_TYPE` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `PROPOSAL_PERSON_ID` int NOT NULL,
  PRIMARY KEY (`ATTACHMENT_ID`),
  KEY `PROP_PERSON_ATTACHMENT_FK1` (`PROPOSAL_PERSON_ID`),
  CONSTRAINT `PROP_PERSON_ATTACHMENT_FK1` FOREIGN KEY (`PROPOSAL_PERSON_ID`) REFERENCES `proposal_persons` (`PROPOSAL_PERSON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal_project_team` (
  `PROPOSAL_PROJECT_TEAM_ID` int NOT NULL AUTO_INCREMENT,
  `END_DATE` datetime(6) DEFAULT NULL,
  `FULL_NAME` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` varchar(255) DEFAULT NULL,
  `NON_EMPLOYEE_FLAG` varchar(255) DEFAULT NULL,
  `PERCENTAGE_CHARGED` varchar(255) DEFAULT NULL,
  `PERSON_ID` varchar(255) DEFAULT NULL,
  `PROJECT_ROLE` varchar(255) DEFAULT NULL,
  `START_DATE` datetime(6) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `PROPOSAL_ID` int NOT NULL,
  PRIMARY KEY (`PROPOSAL_PROJECT_TEAM_ID`),
  KEY `PROPOSAL_PROJECT_TEAM_FK1` (`PROPOSAL_ID`),
  CONSTRAINT `PROPOSAL_PROJECT_TEAM_FK1` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `proposal` (`PROPOSAL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal_resrch_areas` (
  `RESRCH_AREA_ID` int NOT NULL AUTO_INCREMENT,
  `PROPOSAL_ID` int DEFAULT NULL,
  `PROPOSAL_NUMBER` varchar(20) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `RESEARCH_AREA_CODE` varchar(3) DEFAULT NULL,
  `RESRCH_TYPE_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `RESRCH_TYPE_AREA_CODE` varchar(255) DEFAULT NULL,
  `RESRCH_TYPE_SUB_AREA_CODE` varchar(255) DEFAULT NULL,
  `RESEARCH_SUB_AREA_CODE` varchar(255) DEFAULT NULL,
  `CHALLENGE_AREA_CODE` varchar(255) DEFAULT NULL,
  `CHALLENGE_SUBAREA_CODE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`RESRCH_AREA_ID`),
  KEY `PROP_RESRCH_AREA_FK1` (`PROPOSAL_ID`),
  KEY `PROP_RESRCH_AREA_FK2` (`RESRCH_TYPE_CODE`),
  KEY `PROP_RESRCH_AREA_FK3` (`RESRCH_TYPE_AREA_CODE`),
  KEY `PROP_RESRCH_AREA_FK4` (`RESRCH_TYPE_SUB_AREA_CODE`),
  KEY `PROP_RESRCH_AREA_FK5` (`RESEARCH_SUB_AREA_CODE`),
  KEY `PROP_RESRCH_AREA_FK6` (`CHALLENGE_AREA_CODE`),
  KEY `PROP_RESRCH_AREA_FK7` (`CHALLENGE_SUBAREA_CODE`),
  CONSTRAINT `PROP_RESRCH_AREA_FK1` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `proposal` (`PROPOSAL_ID`),
  CONSTRAINT `PROP_RESRCH_AREA_FK2` FOREIGN KEY (`RESRCH_TYPE_CODE`) REFERENCES `research_type` (`RESRCH_TYPE_CODE`),
  CONSTRAINT `PROP_RESRCH_AREA_FK3` FOREIGN KEY (`RESRCH_TYPE_AREA_CODE`) REFERENCES `research_type_area` (`RESRCH_TYPE_AREA_CODE`),
  CONSTRAINT `PROP_RESRCH_AREA_FK4` FOREIGN KEY (`RESRCH_TYPE_SUB_AREA_CODE`) REFERENCES `research_type_sub_area` (`RESRCH_TYPE_SUB_AREA_CODE`),
  CONSTRAINT `PROP_RESRCH_AREA_FK5` FOREIGN KEY (`RESEARCH_SUB_AREA_CODE`) REFERENCES `research_sub_area` (`RESEARCH_SUB_AREA_CODE`),
  CONSTRAINT `PROP_RESRCH_AREA_FK6` FOREIGN KEY (`CHALLENGE_AREA_CODE`) REFERENCES `societal_challenge_area` (`CHALLENGE_AREA_CODE`),
  CONSTRAINT `PROP_RESRCH_AREA_FK7` FOREIGN KEY (`CHALLENGE_SUBAREA_CODE`) REFERENCES `societal_challenge_subarea` (`CHALLENGE_SUBAREA_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal_resrch_type` (
  `RESRCH_TYPE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`RESRCH_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal_review` (
  `REVIEW_ID` int NOT NULL AUTO_INCREMENT,
  `ALT_REVIEWER_EMAIL` varchar(200) DEFAULT NULL,
  `ALT_REVIEWER_FULLNAME` varchar(90) DEFAULT NULL,
  `ALT_REVIEWER_PERSON_ID` varchar(90) DEFAULT NULL,
  `PI_REVIEW_DEADLINE_DATE` datetime(6) DEFAULT NULL,
  `REVIEW_DEADLINE_DATE` datetime(6) DEFAULT NULL,
  `REVIEW_END_DATE` datetime(6) DEFAULT NULL,
  `REVIEW_START_DATE` datetime(6) DEFAULT NULL,
  `REVIEW_STATUS_CODE` int DEFAULT NULL,
  `REVIEW_TYPE_CODE` int DEFAULT NULL,
  `REVIEWER_EMAIL` varchar(200) DEFAULT NULL,
  `REVIEWER_FULLNAME` varchar(90) DEFAULT NULL,
  `REVIEWER_PERSON_ID` varchar(90) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `PROPOSAL_ID` int NOT NULL,
  `ROLE_ID` int DEFAULT NULL,
  `HAS_RANK` varchar(1) DEFAULT NULL,
  `HAS_ENDORSE` varchar(1) DEFAULT NULL,
  `IS_FINAL` varchar(1) DEFAULT NULL,
  `HAS_RECOMMENDATION` varchar(1) DEFAULT NULL,
  `HAS_QUESTION` varchar(1) DEFAULT NULL,
  `EVALUATION_STOP_CODE` int DEFAULT NULL,
  `HAS_QUESTIONNAIRE` varchar(255) DEFAULT NULL,
  `COMPLETE_REVIEWER_EMAIL` varchar(200) DEFAULT NULL,
  `COMPLETE_REVIEWER_FULLNAME` varchar(90) DEFAULT NULL,
  `COMPLETE_REVIEWER_PERSON_ID` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`REVIEW_ID`),
  KEY `PROPOSAL_REVIEW_FK3` (`PROPOSAL_ID`),
  KEY `PROPOSAL_REVIEW_FK2` (`REVIEW_STATUS_CODE`),
  KEY `PROPOSAL_REVIEW_FK1` (`REVIEW_TYPE_CODE`),
  KEY `PROPOSAL_REVIEW_FK5` (`ROLE_ID`),
  KEY `PROPOSAL_REVIEW_FK4` (`EVALUATION_STOP_CODE`),
  CONSTRAINT `PROPOSAL_REVIEW_FK1` FOREIGN KEY (`REVIEW_TYPE_CODE`) REFERENCES `review_type` (`REVIEW_TYPE_CODE`),
  CONSTRAINT `PROPOSAL_REVIEW_FK2` FOREIGN KEY (`REVIEW_STATUS_CODE`) REFERENCES `review_status` (`REVIEW_STATUS_CODE`),
  CONSTRAINT `PROPOSAL_REVIEW_FK3` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `eps_proposal` (`PROPOSAL_ID`),
  CONSTRAINT `PROPOSAL_REVIEW_FK4` FOREIGN KEY (`EVALUATION_STOP_CODE`) REFERENCES `eps_prop_evaluation_stop` (`EVALUATION_STOP_CODE`),
  CONSTRAINT `PROPOSAL_REVIEW_FK5` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ROLE_ID`),
  CONSTRAINT `PROPOSAL_REVIEW_ibfk_1` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal_review_attachment` (
  `REVIEW_ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `FILE_DATA_ID` varchar(255) DEFAULT NULL,
  `FILE_NAME` varchar(255) DEFAULT NULL,
  `MIME_TYPE` varchar(255) DEFAULT NULL,
  `PROPOSAL_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `REVIEW_COMMENT_ID` int NOT NULL,
  PRIMARY KEY (`REVIEW_ATTACHMENT_ID`),
  KEY `REVIEWER_ATTACHMENT_FK1` (`REVIEW_COMMENT_ID`),
  CONSTRAINT `REVIEWER_ATTACHMENT_FK1` FOREIGN KEY (`REVIEW_COMMENT_ID`) REFERENCES `proposal_review_comment` (`REVIEW_COMMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal_review_comment` (
  `REVIEW_COMMENT_ID` int NOT NULL AUTO_INCREMENT,
  `IS_PRIVATE_COMMENT` varchar(255) DEFAULT NULL,
  `PROPOSAL_ID` int DEFAULT NULL,
  `REVIEW_COMMENT` varchar(4000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `REVIEW_ID` int NOT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `FULL_NAME` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`REVIEW_COMMENT_ID`),
  KEY `PROPOSAL_REVIEW_COMMENT_FK1` (`REVIEW_ID`),
  CONSTRAINT `PROPOSAL_REVIEW_COMMENT_FK1` FOREIGN KEY (`REVIEW_ID`) REFERENCES `proposal_review` (`REVIEW_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal_special_review` (
  `PROPOSAL_SPECIAL_REVIEW_ID` int NOT NULL AUTO_INCREMENT,
  `PROPOSAL_ID` int DEFAULT NULL,
  `PROPOSAL_NUMBER` varchar(20) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `APPLICATION_DATE` datetime DEFAULT NULL,
  `APPROVAL_DATE` datetime DEFAULT NULL,
  `APPROVAL_TYPE_CODE` varchar(3) DEFAULT NULL,
  `COMMENTS` longtext,
  `EXPIRATION_DATE` datetime DEFAULT NULL,
  `PROTOCOL_NUMBER` varchar(30) DEFAULT NULL,
  `PROTOCOL_STATUS_DESCRIPTION` varchar(200) DEFAULT NULL,
  `SPECIAL_REVIEW_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_INTEGRATED_PROTOCOL` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`PROPOSAL_SPECIAL_REVIEW_ID`),
  KEY `PROP_SPL_REVIEW_FK3` (`APPROVAL_TYPE_CODE`),
  KEY `PROP_SPL_REVIEW_FK1` (`PROPOSAL_ID`),
  KEY `PROP_SPL_REVIEW_FK2` (`SPECIAL_REVIEW_CODE`),
  CONSTRAINT `PROP_SPL_REVIEW_FK1` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `proposal` (`PROPOSAL_ID`),
  CONSTRAINT `PROP_SPL_REVIEW_FK2` FOREIGN KEY (`SPECIAL_REVIEW_CODE`) REFERENCES `special_review` (`SPECIAL_REVIEW_CODE`),
  CONSTRAINT `PROP_SPL_REVIEW_FK3` FOREIGN KEY (`APPROVAL_TYPE_CODE`) REFERENCES `sp_rev_approval_type` (`APPROVAL_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal_sponsors` (
  `SPONSOR_ID` int NOT NULL,
  `PROPOSAL_ID` int DEFAULT NULL,
  `PROPOSAL_NUMBER` varchar(20) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `AMOUNT` int DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `SPONSOR_CODE` varchar(20) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`SPONSOR_ID`),
  KEY `PROPOSAL_SPONSORS_FK1` (`PROPOSAL_ID`),
  KEY `PROPOSAL_SPONSORS_FK2` (`SPONSOR_CODE`),
  CONSTRAINT `PROPOSAL_SPONSORS_FK1` FOREIGN KEY (`PROPOSAL_ID`) REFERENCES `proposal` (`PROPOSAL_ID`),
  CONSTRAINT `PROPOSAL_SPONSORS_FK2` FOREIGN KEY (`SPONSOR_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal_status` (
  `STATUS_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `CAN_CREATE_AWARD` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal_type` (
  `TYPE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `proposal_version_history` (
  `HISTORY_ID` int NOT NULL AUTO_INCREMENT,
  `ACTIVE_PROPOSAL_ID` int DEFAULT NULL,
  `ORIGINATED_PROPOSAL_ID` int DEFAULT NULL,
  `REQUEST_TYPE` varchar(200) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`HISTORY_ID`),
  KEY `PROPOSAL_VERSION_HISTORY_FK1` (`ACTIVE_PROPOSAL_ID`),
  KEY `PROPOSAL_VERSION_HISTORY_FK2` (`ORIGINATED_PROPOSAL_ID`),
  CONSTRAINT `PROPOSAL_VERSION_HISTORY_FK1` FOREIGN KEY (`ACTIVE_PROPOSAL_ID`) REFERENCES `proposal` (`PROPOSAL_ID`),
  CONSTRAINT `PROPOSAL_VERSION_HISTORY_FK2` FOREIGN KEY (`ORIGINATED_PROPOSAL_ID`) REFERENCES `proposal` (`PROPOSAL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `publication` (
  `PUBLICATION_ID` int NOT NULL,
  `AUTHORS` varchar(255) DEFAULT NULL,
  `AUTHORGANIZATION` varchar(255) DEFAULT NULL,
  `EDITION` varchar(255) DEFAULT NULL,
  `ISSN` varchar(255) DEFAULT NULL,
  `PAGE` varchar(200) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  `REVIEW_STATUS` varchar(255) DEFAULT NULL,
  `SCHOOL_OF_AUTHOR` varchar(255) DEFAULT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `VOLUME` varchar(255) DEFAULT NULL,
  `YEAR` varchar(200) DEFAULT NULL,
  `NAME_OF_JOURNAL` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PUBLICATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `quest_answer` (
  `QUESTIONNAIRE_ANSWER_ID` int NOT NULL AUTO_INCREMENT,
  `QUESTIONNAIRE_ANS_HEADER_ID` int DEFAULT NULL,
  `QUESTION_ID` int DEFAULT NULL,
  `OPTION_NUMBER` int DEFAULT NULL,
  `ANSWER_NUMBER` int DEFAULT NULL,
  `ANSWER` varchar(4000) DEFAULT NULL,
  `ANSWER_LOOKUP_CODE` varchar(100) DEFAULT NULL,
  `EXPLANATION` varchar(4000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`QUESTIONNAIRE_ANSWER_ID`),
  KEY `QUEST_ANSWER_FK1` (`QUESTIONNAIRE_ANS_HEADER_ID`),
  KEY `QUEST_ANSWER_FK2` (`QUESTION_ID`),
  CONSTRAINT `QUEST_ANSWER_FK1` FOREIGN KEY (`QUESTIONNAIRE_ANS_HEADER_ID`) REFERENCES `quest_answer_header` (`QUESTIONNAIRE_ANS_HEADER_ID`),
  CONSTRAINT `QUEST_ANSWER_FK2` FOREIGN KEY (`QUESTION_ID`) REFERENCES `quest_question` (`QUESTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `quest_answer_attachment` (
  `QUESTIONNAIRE_ANSWER_ATT_ID` int NOT NULL AUTO_INCREMENT,
  `QUESTIONNAIRE_ANSWER_ID` int DEFAULT NULL,
  `ATTACHMENT` longblob,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `CONTENT_TYPE` varchar(100) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`QUESTIONNAIRE_ANSWER_ATT_ID`),
  KEY `QUEST_ANSWER_ATTACHMENT_FK1` (`QUESTIONNAIRE_ANSWER_ID`),
  CONSTRAINT `QUEST_ANSWER_ATTACHMENT_FK1` FOREIGN KEY (`QUESTIONNAIRE_ANSWER_ID`) REFERENCES `quest_answer` (`QUESTIONNAIRE_ANSWER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `quest_answer_header` (
  `QUESTIONNAIRE_ANS_HEADER_ID` int NOT NULL AUTO_INCREMENT,
  `QUESTIONNAIRE_ID` int DEFAULT NULL,
  `MODULE_ITEM_CODE` int DEFAULT NULL,
  `MODULE_SUB_ITEM_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(20) DEFAULT NULL,
  `MODULE_SUB_ITEM_KEY` varchar(20) DEFAULT NULL,
  `QUESTIONNAIRE_COMPLETED_FLAG` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`QUESTIONNAIRE_ANS_HEADER_ID`),
  KEY `QUEST_ANSWER_HEADER_FK1` (`QUESTIONNAIRE_ID`),
  CONSTRAINT `QUEST_ANSWER_HEADER_FK1` FOREIGN KEY (`QUESTIONNAIRE_ID`) REFERENCES `quest_header` (`QUESTIONNAIRE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `quest_column_nextvalue` (
  `QUESTIONNAIRE_ID` int NOT NULL,
  `QUESTIONNAIRE_NUMBER` int DEFAULT NULL,
  `QUESTION_ID` int DEFAULT NULL,
  `QUESTION_NUMBER` int DEFAULT NULL,
  `QUESTION_CONDITION_ID` int DEFAULT NULL,
  `QUESTION_OPTION_ID` int DEFAULT NULL,
  `QUESTIONNAIRE_ANS_HEADER_ID` int DEFAULT NULL,
  `QUESTIONNAIRE_ANSWER_ID` int DEFAULT NULL,
  `QUESTIONNAIRE_ANSWER_ATT_ID` int DEFAULT NULL,
  `QUESTIONNAIRE_USAGE_ID` int DEFAULT NULL,
  `QUESTIONNAIRE_ANSWER_TABLE_ID` int DEFAULT NULL,
  PRIMARY KEY (`QUESTIONNAIRE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `quest_group_type` (
  `QUEST_GROUP_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`QUEST_GROUP_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `quest_header` (
  `QUESTIONNAIRE_ID` int NOT NULL,
  `QUEST_GROUP_TYPE_CODE` varchar(3) DEFAULT NULL,
  `IS_FINAL` varchar(1) DEFAULT NULL,
  `QUESTIONNAIRE_NUMBER` int DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  `QUESTIONNAIRE` varchar(200) DEFAULT NULL,
  `DESCRIPTION` varchar(4000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`QUESTIONNAIRE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `quest_question` (
  `QUESTION_ID` int NOT NULL,
  `QUESTION_NUMBER` int DEFAULT NULL,
  `QUESTION_VERSION_NUMBER` int DEFAULT NULL,
  `QUESTIONNAIRE_ID` int DEFAULT NULL,
  `SORT_ORDER` int DEFAULT NULL,
  `QUESTION` varchar(1000) DEFAULT NULL,
  `DESCRIPTION` text,
  `PARENT_QUESTION_ID` int DEFAULT NULL,
  `HELP_LINK` varchar(500) DEFAULT NULL,
  `ANSWER_TYPE` varchar(90) DEFAULT NULL,
  `ANSWER_LENGTH` int DEFAULT NULL,
  `NO_OF_ANSWERS` int DEFAULT NULL,
  `LOOKUP_TYPE` varchar(100) DEFAULT NULL,
  `LOOKUP_NAME` varchar(100) DEFAULT NULL,
  `LOOKUP_FIELD` varchar(30) DEFAULT NULL,
  `GROUP_NAME` varchar(60) DEFAULT NULL,
  `GROUP_LABEL` varchar(300) DEFAULT NULL,
  `HAS_CONDITION` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` date DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `RULE_ID` int DEFAULT NULL,
  PRIMARY KEY (`QUESTION_ID`),
  KEY `QUEST_QUESTION_FK1` (`QUESTIONNAIRE_ID`),
  CONSTRAINT `QUEST_QUESTION_FK1` FOREIGN KEY (`QUESTIONNAIRE_ID`) REFERENCES `quest_header` (`QUESTIONNAIRE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `quest_question_condition` (
  `QUESTION_CONDITION_ID` int NOT NULL,
  `QUESTION_ID` int DEFAULT NULL,
  `CONDITION_TYPE` varchar(90) DEFAULT NULL,
  `CONDITION_VALUE` varchar(2000) DEFAULT NULL,
  `GROUP_NAME` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` date DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`QUESTION_CONDITION_ID`),
  KEY `QUEST_QUESTION_CONDITION_FK1` (`QUESTION_ID`),
  CONSTRAINT `QUEST_QUESTION_CONDITION_FK1` FOREIGN KEY (`QUESTION_ID`) REFERENCES `quest_question` (`QUESTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `quest_question_option` (
  `QUESTION_OPTION_ID` int NOT NULL,
  `QUESTION_ID` int DEFAULT NULL,
  `OPTION_NUMBER` int DEFAULT NULL,
  `OPTION_LABEL` varchar(600) DEFAULT NULL,
  `REQUIRE_EXPLANATION` varchar(1) DEFAULT NULL,
  `EXPLANTION_LABEL` varchar(2000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` date DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`QUESTION_OPTION_ID`),
  KEY `QUEST_QUESTION_OPTION_FK1` (`QUESTION_ID`),
  CONSTRAINT `QUEST_QUESTION_OPTION_FK1` FOREIGN KEY (`QUESTION_ID`) REFERENCES `quest_question` (`QUESTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `quest_table_answer` (
  `QUEST_TABLE_ANSWER_ID` int NOT NULL AUTO_INCREMENT,
  `QUESTIONNAIRE_ANS_HEADER_ID` int DEFAULT NULL,
  `QUESTION_ID` int DEFAULT NULL,
  `ORDER_NUMBER` int DEFAULT NULL,
  `COLUMN_1` varchar(250) DEFAULT NULL,
  `COLUMN_2` varchar(250) DEFAULT NULL,
  `COLUMN_3` varchar(250) DEFAULT NULL,
  `COLUMN_4` varchar(250) DEFAULT NULL,
  `COLUMN_5` varchar(250) DEFAULT NULL,
  `COLUMN_6` varchar(250) DEFAULT NULL,
  `COLUMN_7` varchar(250) DEFAULT NULL,
  `COLUMN_8` varchar(250) DEFAULT NULL,
  `COLUMN_9` varchar(250) DEFAULT NULL,
  `COLUMN_10` varchar(250) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`QUEST_TABLE_ANSWER_ID`),
  KEY `QUESTIONNAIRE_ANS_HEADER_ID_FK1` (`QUESTIONNAIRE_ANS_HEADER_ID`),
  CONSTRAINT `QUESTIONNAIRE_ANS_HEADER_ID_FK1` FOREIGN KEY (`QUESTIONNAIRE_ANS_HEADER_ID`) REFERENCES `quest_answer_header` (`QUESTIONNAIRE_ANS_HEADER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `quest_usage` (
  `QUESTIONNAIRE_USAGE_ID` int NOT NULL,
  `MODULE_ITEM_CODE` int DEFAULT NULL,
  `MODULE_SUB_ITEM_CODE` int DEFAULT NULL,
  `QUESTIONNAIRE_ID` int DEFAULT NULL,
  `QUESTIONNAIRE_LABEL` varchar(200) DEFAULT NULL,
  `IS_MANDATORY` varchar(1) DEFAULT NULL,
  `RULE_ID` varchar(40) DEFAULT NULL,
  `UPDATE_TIMESTAMP` date DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `SORT_ORDER` int DEFAULT NULL,
  `TRIGGER_POST_EVALUATION` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`QUESTIONNAIRE_USAGE_ID`),
  KEY `QUEST_USAGE_FK1` (`QUESTIONNAIRE_ID`),
  CONSTRAINT `QUEST_USAGE_FK1` FOREIGN KEY (`QUESTIONNAIRE_ID`) REFERENCES `quest_header` (`QUESTIONNAIRE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `quick_links` (
  `QUICK_LINK_ID` int NOT NULL,
  `URL` varchar(2000) DEFAULT NULL,
  `NAME` varchar(200) DEFAULT NULL,
  `DESCRIPTION` varchar(2000) DEFAULT NULL,
  `TYPE` varchar(1) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `SORT_ORDER` int DEFAULT NULL,
  PRIMARY KEY (`QUICK_LINK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `rate_class` (
  `RATE_CLASS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `RATE_CLASS_TYPE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`RATE_CLASS_CODE`),
  KEY `RATE_CLASS_FK1` (`RATE_CLASS_TYPE`),
  CONSTRAINT `RATE_CLASS_FK1` FOREIGN KEY (`RATE_CLASS_TYPE`) REFERENCES `rate_class_type` (`RATE_CLASS_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `rate_class_type` (
  `SORT_ID` int DEFAULT NULL,
  `PREFIX_ACTIVITY_TYPE` varchar(1) DEFAULT 'N',
  `RATE_CLASS_TYPE` varchar(1) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`RATE_CLASS_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `rate_type` (
  `RATE_CLASS_CODE` varchar(3) NOT NULL,
  `RATE_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`RATE_CLASS_CODE`,`RATE_TYPE_CODE`),
  CONSTRAINT `RATE_TYPE_FK1` FOREIGN KEY (`RATE_CLASS_CODE`) REFERENCES `rate_class` (`RATE_CLASS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `relevant_field` (
  `RELEVANT_FIELD_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UNIT_NUMBER` varchar(6) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`RELEVANT_FIELD_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `remainder_notification` (
  `REMAINDER_NOTIFICATION_ID` int NOT NULL,
  `DAYS_TO_DUE_DATE` int DEFAULT NULL,
  `IS_ACTVE` varchar(255) DEFAULT NULL,
  `NOTIFICATION_TYPE_ID` int DEFAULT NULL,
  `PLACEHOLDER_VALUES` varchar(255) DEFAULT NULL,
  `PROCEDURE_NAME` varchar(255) DEFAULT NULL,
  `REMAINDER_NAME` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`REMAINDER_NOTIFICATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `reminder_notification` (
  `REMINDER_NOTIFICATION_ID` int NOT NULL,
  `REMINDER_NAME` varchar(60) DEFAULT NULL,
  `NOTIFICATION_TYPE_ID` int DEFAULT NULL,
  `DAYS_TO_DUE_DATE` int DEFAULT NULL,
  `PROCEDURE_NAME` varchar(200) DEFAULT NULL,
  `IS_ACTVE` varchar(1) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `PLACEHOLDER_VALUES` longtext,
  PRIMARY KEY (`REMINDER_NOTIFICATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `remote_detail` (
  `IP` varchar(255) NOT NULL,
  `REMOTE_USER` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `report` (
  `REPORT_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `FINAL_REPORT_FLAG` char(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` date DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`REPORT_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `report_class` (
  `REPORT_CLASS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `GENERATE_REPORT_REQUIREMENTS` varchar(1) DEFAULT 'N',
  `ACTIVE_FLAG` varchar(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`REPORT_CLASS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `report_columns` (
  `REPORT_COLUMN_ID` int NOT NULL,
  `TYPE_CODE` varchar(5) DEFAULT NULL,
  `COLUMN_ID` int DEFAULT NULL,
  `IS_FILTER` varchar(1) DEFAULT NULL,
  `IS_COLUMN` varchar(1) DEFAULT NULL,
  `DISPLAY_NAME` varchar(250) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`REPORT_COLUMN_ID`),
  KEY `REPORT_COLUMNS_FK1` (`COLUMN_ID`),
  KEY `REPORT_COLUMNS_FK2` (`TYPE_CODE`),
  CONSTRAINT `REPORT_COLUMNS_FK1` FOREIGN KEY (`COLUMN_ID`) REFERENCES `column_lookup` (`COLUMN_ID`),
  CONSTRAINT `REPORT_COLUMNS_FK2` FOREIGN KEY (`TYPE_CODE`) REFERENCES `report_type` (`TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `report_last_sync_time` (
  `LAST_SYNC_TIMESTAMP` datetime DEFAULT NULL,
  `CLAIM_LAST_SYNC_TIMESTAMP` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `report_status` (
  `REPORT_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`REPORT_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `report_template` (
  `REPORT_TEMPLATE_ID` int NOT NULL AUTO_INCREMENT,
  `TEMPLATE_NAME` varchar(90) DEFAULT NULL,
  `TEMPLATE_DESCRIPTION` varchar(200) DEFAULT NULL,
  `TEMPLATE_OWNER_PERSON_ID` varchar(40) DEFAULT NULL,
  `TEMPLATE_SQL` longtext,
  `TEMPLATE_JSON` longtext,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `TEMPLATE_TYPE` varchar(20) DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `SUB_MODULE_CODE` int DEFAULT NULL,
  `TYPE_CODE` varchar(255) DEFAULT NULL,
  `SORT_ORDER` int DEFAULT NULL,
  `MODULE_DESCRIPTION` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`REPORT_TEMPLATE_ID`),
  KEY `REPORT_TEMPLATE_FK1` (`MODULE_CODE`),
  KEY `REPORT_TEMPLATE_FK2` (`TYPE_CODE`),
  CONSTRAINT `REPORT_TEMPLATE_FK1` FOREIGN KEY (`MODULE_CODE`) REFERENCES `coeus_module` (`MODULE_CODE`),
  CONSTRAINT `REPORT_TEMPLATE_FK2` FOREIGN KEY (`TYPE_CODE`) REFERENCES `report_type` (`TYPE_CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=324 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `report_type` (
  `TYPE_CODE` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `JSON_NAME` varchar(100) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `VIEW_NAME` varchar(100) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `TYPE` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `report_type_join` (
  `TYPE_JOIN_ID` int NOT NULL,
  `TYPE_CODE` varchar(3) DEFAULT NULL,
  `JOIN_QUERY` longtext,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`TYPE_JOIN_ID`),
  KEY `REPORT_TYPE_JOIN_FK1` (`TYPE_CODE`),
  CONSTRAINT `REPORT_TYPE_JOIN_FK1` FOREIGN KEY (`TYPE_CODE`) REFERENCES `report_type` (`TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `research_areas` (
  `RESEARCH_AREA_CODE` varchar(8) NOT NULL,
  `PARENT_RESEARCH_AREA_CODE` varchar(8) DEFAULT NULL,
  `HAS_CHILDREN_FLAG` varchar(1) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `ACTIVE_FLAG` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RESEARCH_AREA_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `research_center` (
  `CENTER_ID` int NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `ACTIVE_FLAG` varchar(1) DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT 'quickstart',
  `CREATE_TIMESTAMP` datetime DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_USER` varchar(60) DEFAULT 'quickstart',
  `UPDATE_TIMESTAMP` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`CENTER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `research_sub_area` (
  `RESEARCH_SUB_AREA_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(4000) DEFAULT NULL,
  `RESEARCH_AREA_CODE` varchar(8) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`RESEARCH_SUB_AREA_CODE`),
  KEY `RESEARCH_SUB_AREA_FK1` (`RESEARCH_AREA_CODE`),
  CONSTRAINT `RESEARCH_SUB_AREA_FK1` FOREIGN KEY (`RESEARCH_AREA_CODE`) REFERENCES `research_areas` (`RESEARCH_AREA_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `research_type` (
  `RESRCH_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `ACTIVE_FLAG` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`RESRCH_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `research_type_area` (
  `RESRCH_TYPE_AREA_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(4000) DEFAULT NULL,
  `RESRCH_TYPE_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `ACTIVE_FLAG` varchar(1) DEFAULT 'Y',
  `OLD_PK` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`RESRCH_TYPE_AREA_CODE`),
  KEY `RESEARCH_TYPE_AREA_FK1` (`RESRCH_TYPE_CODE`),
  CONSTRAINT `RESEARCH_TYPE_AREA_FK1` FOREIGN KEY (`RESRCH_TYPE_CODE`) REFERENCES `research_type` (`RESRCH_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `research_type_sub_area` (
  `RESRCH_TYPE_SUB_AREA_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(4000) DEFAULT NULL,
  `RESRCH_TYPE_CODE` varchar(3) NOT NULL,
  `RESRCH_TYPE_AREA_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `ACTIVE_FLAG` varchar(1) DEFAULT 'Y',
  `OLD_PK` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`RESRCH_TYPE_SUB_AREA_CODE`),
  KEY `RESEARCH_TYPE_SUB_AREA_FK1` (`RESRCH_TYPE_AREA_CODE`),
  CONSTRAINT `RESEARCH_TYPE_SUB_AREA_FK1` FOREIGN KEY (`RESRCH_TYPE_AREA_CODE`) REFERENCES `research_type_area` (`RESRCH_TYPE_AREA_CODE`),
  CONSTRAINT `RESEARCH_TYPE_SUB_AREA_FK2` FOREIGN KEY (`RESRCH_TYPE_AREA_CODE`) REFERENCES `research_type_area` (`RESRCH_TYPE_AREA_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `review_status` (
  `REVIEW_STATUS_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`REVIEW_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `review_type` (
  `REVIEW_TYPE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_EXTERNAL` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`REVIEW_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `reviewer_rights` (
  `REVIEWER_RIGHTS_ID` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`REVIEWER_RIGHTS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `right_section_mapping` (
  `RIGHT_ID` int NOT NULL,
  `SECTION_CODE` varchar(3) NOT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`RIGHT_ID`,`SECTION_CODE`),
  KEY `RIGHT_SECTION_MAPPING_FK2` (`SECTION_CODE`),
  CONSTRAINT `RIGHT_SECTION_MAPPING_FK1` FOREIGN KEY (`RIGHT_ID`) REFERENCES `rights` (`RIGHT_ID`),
  CONSTRAINT `RIGHT_SECTION_MAPPING_FK2` FOREIGN KEY (`SECTION_CODE`) REFERENCES `agreement_section_type` (`SECTION_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `rights` (
  `RIGHT_ID` int NOT NULL,
  `RIGHT_NAME` varchar(100) NOT NULL,
  `DESCRIPTION` varchar(500) NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `RIGHTS_TYPE_CODE` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`RIGHT_ID`),
  KEY `RIGHTS_FK1` (`RIGHTS_TYPE_CODE`),
  CONSTRAINT `RIGHTS_FK1` FOREIGN KEY (`RIGHTS_TYPE_CODE`) REFERENCES `rights_type` (`RIGHTS_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `rights_type` (
  `RIGHTS_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`RIGHTS_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `rise_error_allocations` (
  `RESOURCE_UNIQUE_ID` varchar(100) NOT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `BUDGET_REFERENCE_NUMBER` varchar(50) DEFAULT NULL,
  `POSITION_ID` varchar(50) DEFAULT NULL,
  `COST_ALLOCATION` decimal(5,2) DEFAULT NULL,
  `ALLOCATION_START_DATE` datetime DEFAULT NULL,
  `ALLOCATION_END_DATE` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`RESOURCE_UNIQUE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `role` (
  `ROLE_ID` int NOT NULL,
  `ROLE_NAME` varchar(50) NOT NULL,
  `DESCRIPTION` varchar(200) NOT NULL,
  `ROLE_TYPE_CODE` varchar(1) NOT NULL,
  `STATUS_FLAG` varchar(1) NOT NULL,
  `CREATE_TIMESTAMP` datetime NOT NULL,
  `CREATE_USER` varchar(60) NOT NULL,
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  PRIMARY KEY (`ROLE_ID`),
  KEY `ROLE_FK1` (`ROLE_TYPE_CODE`),
  CONSTRAINT `ROLE_FK1` FOREIGN KEY (`ROLE_TYPE_CODE`) REFERENCES `role_type` (`ROLE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `role_rights` (
  `ROLE_RIGHTS_ID` decimal(22,0) NOT NULL,
  `RIGHT_ID` int NOT NULL,
  `ROLE_ID` int NOT NULL,
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  PRIMARY KEY (`ROLE_RIGHTS_ID`),
  KEY `ROLE_RIGHTS_FK1` (`ROLE_ID`),
  KEY `ROLE_RIGHTS_FK2` (`RIGHT_ID`),
  CONSTRAINT `ROLE_RIGHTS_FK1` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ROLE_ID`),
  CONSTRAINT `ROLE_RIGHTS_FK2` FOREIGN KEY (`RIGHT_ID`) REFERENCES `rights` (`RIGHT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `role_rights_audit_tab` (
  `ROLE_RIGHTS_AUDIT_TAB_ID` int NOT NULL AUTO_INCREMENT,
  `SOURCE` varchar(25) NOT NULL,
  `MODE` varchar(10) NOT NULL,
  `OLD_RLE_RIGHTS_ID` decimal(22,0) DEFAULT NULL,
  `NEW_RLE_RIGHTS_ID` decimal(22,0) DEFAULT NULL,
  `OLD_PER_ROLES_ID` decimal(22,0) DEFAULT NULL,
  `NEW_PER_ROLES_ID` decimal(22,0) DEFAULT NULL,
  `OLD_PERSON_ID` varchar(40) DEFAULT NULL,
  `NEW_PERSON_ID` varchar(40) DEFAULT NULL,
  `OLD_ROLE_ID` int DEFAULT NULL,
  `NEW_ROLE_ID` int DEFAULT NULL,
  `OLD_ROLE_NAME` varchar(50) DEFAULT NULL,
  `NEW_ROLE_NAME` varchar(50) DEFAULT NULL,
  `OLD_RIGHT_ID` int DEFAULT NULL,
  `NEW_RIGHT_ID` int DEFAULT NULL,
  `OLD_UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `NEW_UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `OLD_DESCEND_FLG` varchar(1) DEFAULT NULL,
  `NEW_DESCEND_FLG` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  PRIMARY KEY (`ROLE_RIGHTS_AUDIT_TAB_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `role_type` (
  `ROLE_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`ROLE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `rolodex` (
  `ROLODEX_ID` int NOT NULL AUTO_INCREMENT,
  `LAST_NAME` varchar(50) DEFAULT NULL,
  `FIRST_NAME` varchar(50) DEFAULT NULL,
  `MIDDLE_NAME` varchar(50) DEFAULT NULL,
  `FULL_NAME` varchar(250) DEFAULT NULL,
  `SUFFIX` varchar(10) DEFAULT NULL,
  `PREFIX` varchar(10) DEFAULT NULL,
  `TITLE` varchar(35) DEFAULT NULL,
  `ORGANIZATION` varchar(200) DEFAULT NULL,
  `ORGANIZATION_NAME` varchar(200) DEFAULT NULL,
  `ADDRESS_LINE_1` varchar(80) DEFAULT NULL,
  `ADDRESS_LINE_2` varchar(80) DEFAULT NULL,
  `ADDRESS_LINE_3` varchar(80) DEFAULT NULL,
  `FAX_NUMBER` varchar(20) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(60) DEFAULT NULL,
  `CITY` varchar(30) DEFAULT NULL,
  `COUNTY` varchar(30) DEFAULT NULL,
  `STATE` varchar(30) DEFAULT NULL,
  `POSTAL_CODE` varchar(15) DEFAULT NULL,
  `COMMENTS` varchar(4000) DEFAULT NULL,
  `PHONE_NUMBER` varchar(20) DEFAULT NULL,
  `COUNTRY_CODE` varchar(3) DEFAULT NULL,
  `SPONSOR_CODE` varchar(6) DEFAULT NULL,
  `OWNED_BY_UNIT` varchar(8) DEFAULT NULL,
  `SPONSOR_ADDRESS_FLAG` varchar(1) DEFAULT 'N',
  `DELETE_FLAG` varchar(1) DEFAULT 'N',
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `ACTV_IND` varchar(1) DEFAULT 'Y',
  `DESIGNATION` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ROLODEX_ID`),
  KEY `ROLODEX_FK1` (`ORGANIZATION`),
  KEY `ROLODEX_FK2` (`SPONSOR_CODE`),
  KEY `ROLODEX_FK3` (`COUNTRY_CODE`),
  CONSTRAINT `ROLODEX_FK1` FOREIGN KEY (`ORGANIZATION`) REFERENCES `organization` (`ORGANIZATION_ID`),
  CONSTRAINT `ROLODEX_FK2` FOREIGN KEY (`SPONSOR_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`),
  CONSTRAINT `ROLODEX_FK3` FOREIGN KEY (`COUNTRY_CODE`) REFERENCES `country` (`COUNTRY_CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=15766 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_award_feed` (
  `FEED_ID` int NOT NULL AUTO_INCREMENT,
  `BATCH_ID` int DEFAULT NULL,
  `AWARD_ID` int NOT NULL,
  `AWARD_NUMBER` varchar(12) NOT NULL,
  `SEQUENCE_NUMBER` int NOT NULL,
  `FEED_TYPE` varchar(1) NOT NULL,
  `FEED_STATUS` varchar(3) DEFAULT NULL,
  `SYSTEM_COMMENT` varchar(500) DEFAULT NULL,
  `BUSINESS_AREA` varchar(4) DEFAULT NULL,
  `CREATE_USER` varchar(60) NOT NULL,
  `CREATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `NO_FEED_REPORT_FLAG` varchar(1) DEFAULT NULL,
  `USER_ACTION` varchar(20) DEFAULT NULL,
  `USER_COMMENT` varchar(4000) DEFAULT NULL,
  `USER_ACTION_CODE` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`FEED_ID`),
  KEY `SAP_AWARD_FEED_FK1` (`BATCH_ID`),
  KEY `SAP_AWARD_FEED_idx1` (`FEED_STATUS`),
  KEY `SAP_AWARD_FEED_FK2_idx` (`FEED_TYPE`),
  KEY `SAP_AWARD_FEED_FK4` (`USER_ACTION_CODE`),
  CONSTRAINT `SAP_AWARD_FEED_FK1` FOREIGN KEY (`BATCH_ID`) REFERENCES `sap_award_feed_batch` (`BATCH_ID`),
  CONSTRAINT `SAP_AWARD_FEED_FK2` FOREIGN KEY (`FEED_TYPE`) REFERENCES `sap_feed_type` (`FEED_TYPE_CODE`),
  CONSTRAINT `SAP_AWARD_FEED_FK3` FOREIGN KEY (`FEED_STATUS`) REFERENCES `sap_feed_status` (`FEED_STATUS_CODE`),
  CONSTRAINT `SAP_AWARD_FEED_FK4` FOREIGN KEY (`USER_ACTION_CODE`) REFERENCES `sap_feed_user_actions` (`USER_ACTION_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_award_feed_batch` (
  `BATCH_ID` int NOT NULL AUTO_INCREMENT,
  `NO_OF_RECORDS` int DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `RESPONSE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`BATCH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_award_feed_batch_error_log` (
  `BATCH_ERROR_ID` int NOT NULL AUTO_INCREMENT,
  `BATCH_ID` int DEFAULT NULL,
  `ERROR_MESSAGE` varchar(255) DEFAULT NULL,
  `ERROR_TYPE` varchar(255) DEFAULT NULL,
  `FEED_ID` int DEFAULT NULL,
  `GRANT_CODE` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `BUSINESS_AREA` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`BATCH_ERROR_ID`),
  KEY `AWARD_FEED_BATCH_ERROR_LOG_FK2` (`FEED_ID`),
  KEY `AWARD_FEED_BATCH_ERROR_LOG_FK1` (`BATCH_ID`),
  CONSTRAINT `AWARD_FEED_BATCH_ERROR_LOG_FK1` FOREIGN KEY (`BATCH_ID`) REFERENCES `sap_award_feed_batch` (`BATCH_ID`),
  CONSTRAINT `AWARD_FEED_BATCH_ERROR_LOG_FK2` FOREIGN KEY (`FEED_ID`) REFERENCES `sap_award_feed` (`FEED_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_award_feed_batch_files` (
  `BATCH_FILE_ID` int NOT NULL AUTO_INCREMENT,
  `BATCH_FILE_NAME` varchar(255) DEFAULT NULL,
  `BATCH_ID` int DEFAULT NULL,
  `NO_OF_RECORDS` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`BATCH_FILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_claim_feed` (
  `FEED_ID` int NOT NULL AUTO_INCREMENT,
  `BATCH_ID` int DEFAULT NULL,
  `CLAIM_ID` int NOT NULL,
  `CLAIM_NUMBER` varchar(12) NOT NULL,
  `INVOICE_ID` int NOT NULL,
  `SEQUENCE_NUMBER` int NOT NULL,
  `FEED_TYPE_CODE` varchar(1) NOT NULL,
  `FEED_STATUS_CODE` varchar(1) NOT NULL,
  `SYSTEM_COMMENT` varchar(500) DEFAULT NULL,
  `BUSINESS_AREA` varchar(4) DEFAULT NULL,
  `CREATE_USER` varchar(60) NOT NULL,
  `CREATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `NO_FEED_REPORT_FLAG` varchar(1) DEFAULT NULL,
  `USER_COMMENT` varchar(4000) DEFAULT NULL,
  `USER_ACTION_CODE` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`FEED_ID`),
  KEY `SAP_CLAIM_FEED_FK1` (`FEED_STATUS_CODE`),
  KEY `SAP_CLAIM_FEED_FK2` (`FEED_TYPE_CODE`),
  KEY `SAP_CLAIM_FEED_FK3` (`USER_ACTION_CODE`),
  KEY `SAP_CLAIM_FEED_FK4_idx` (`BATCH_ID`),
  CONSTRAINT `SAP_CLAIM_FEED_FK1` FOREIGN KEY (`FEED_STATUS_CODE`) REFERENCES `sap_feed_status` (`FEED_STATUS_CODE`),
  CONSTRAINT `SAP_CLAIM_FEED_FK2` FOREIGN KEY (`FEED_TYPE_CODE`) REFERENCES `sap_feed_type` (`FEED_TYPE_CODE`),
  CONSTRAINT `SAP_CLAIM_FEED_FK3` FOREIGN KEY (`USER_ACTION_CODE`) REFERENCES `sap_feed_user_actions` (`USER_ACTION_CODE`),
  CONSTRAINT `SAP_CLAIM_FEED_FK4` FOREIGN KEY (`BATCH_ID`) REFERENCES `sap_claim_feed_batch` (`BATCH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_claim_feed_batch` (
  `BATCH_ID` int NOT NULL AUTO_INCREMENT,
  `NO_OF_RECORDS` int NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `RESPONSE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`BATCH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_claim_feed_batch_error_log` (
  `BATCH_ERROR_ID` int NOT NULL AUTO_INCREMENT,
  `BATCH_ID` int DEFAULT NULL,
  `FEED_ID` int DEFAULT NULL,
  `ERROR_MESSAGE` varchar(1000) DEFAULT NULL,
  `BUSINESS_AREA` varchar(4) DEFAULT NULL,
  `ERROR_TYPE` varchar(50) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  PRIMARY KEY (`BATCH_ERROR_ID`),
  KEY `CLAIM_FEED_BATCH_ERROR_LOG_FK2_idx` (`FEED_ID`),
  KEY `CLAIM_FEED_BATCH_ERROR_LOG_FK1_idx` (`BATCH_ID`),
  CONSTRAINT `CLAIM_FEED_BATCH_ERROR_LOG_FK1` FOREIGN KEY (`BATCH_ID`) REFERENCES `sap_claim_feed_batch` (`BATCH_ID`),
  CONSTRAINT `CLAIM_FEED_BATCH_ERROR_LOG_FK2` FOREIGN KEY (`FEED_ID`) REFERENCES `sap_claim_feed` (`FEED_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_claim_feed_batch_files` (
  `BATCH_FILE_ID` int NOT NULL AUTO_INCREMENT,
  `BATCH_ID` int NOT NULL,
  `BATCH_FILE_NAME` varchar(50) NOT NULL,
  `NO_OF_RECORDS` int NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  PRIMARY KEY (`BATCH_FILE_ID`),
  KEY `CLAIM_FEED_BATCH_FILES_FK1_idx` (`BATCH_ID`),
  CONSTRAINT `CLAIM_FEED_BATCH_FILES_FK1` FOREIGN KEY (`BATCH_ID`) REFERENCES `sap_claim_feed_batch` (`BATCH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_claim_feed_response_messge` (
  `RESPONSE_MESSGE_ID` int NOT NULL AUTO_INCREMENT,
  `CLAIM_INVOICE_LOG_ID` int DEFAULT NULL,
  `MESSGAE` varchar(200) DEFAULT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`RESPONSE_MESSGE_ID`),
  KEY `CLAIM_FEED_RESPONSE_MESSGE_FK1_idx` (`CLAIM_INVOICE_LOG_ID`),
  CONSTRAINT `CLAIM_FEED_RESPONSE_MESSGE_FK1` FOREIGN KEY (`CLAIM_INVOICE_LOG_ID`) REFERENCES `claim_invoice_log` (`CLAIM_INVOICE_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_concur_files` (
  `FILE_ID` int NOT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `NO_OF_RECORDS` int DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`FILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_cost_center` (
  `COST_CENTER_CODE` varchar(255) NOT NULL,
  `CAMPUS` varchar(255) DEFAULT NULL,
  `COMPANY_CODE` varchar(255) DEFAULT NULL,
  `CONTROLLING_AREA` varchar(255) DEFAULT NULL,
  `COST_CENTER_CATEGORY` varchar(255) DEFAULT NULL,
  `COST_CENTER_NAME` varchar(255) DEFAULT NULL,
  `CURRENCY` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `HIERARCHY_AREA` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` varchar(255) DEFAULT NULL,
  `RESPONSIBLE_PERSON` varchar(255) DEFAULT NULL,
  `RESPONSIBLE_USER` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `VALID_FROM` datetime(6) DEFAULT NULL,
  `VALID_TO` datetime(6) DEFAULT NULL,
  `PROFIT_CENTER` varchar(255) DEFAULT NULL,
  `COST_CENTER_TYPE` varchar(45) DEFAULT NULL,
  `DATE_WHEN_FEED_INACTIVE` datetime DEFAULT NULL,
  PRIMARY KEY (`COST_CENTER_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_feed_prob_grantcode_report` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `BATCH_ID` int DEFAULT NULL,
  `FEED_ID` int DEFAULT NULL,
  `AWARD_ID` decimal(22,0) DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `ACCOUNT_NUMBER` varchar(100) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `BUSINESS_AREA` varchar(4) DEFAULT NULL,
  `VARIATION_TYPE_CODE` varchar(3) DEFAULT NULL,
  `VARIATION_TYPE` varchar(200) DEFAULT NULL,
  `ACCOUNT_TYPE` varchar(200) DEFAULT NULL,
  `TITLE` varchar(300) DEFAULT NULL,
  `GRANT_CODE` varchar(20) DEFAULT NULL,
  `IO_CODE` varchar(50) DEFAULT NULL,
  `FUND_CODE` varchar(10) DEFAULT NULL,
  `PROCESS` varchar(4) DEFAULT NULL,
  `BUDGET_AMOUNT` decimal(12,2) DEFAULT NULL,
  `CUR_LINE_ITEM_COST` decimal(12,2) DEFAULT NULL,
  `PREV_LINE_ITEM_COST` decimal(12,2) DEFAULT NULL,
  `CUR_PI_NAME` varchar(40) DEFAULT NULL,
  `COST_ELEMENT_DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `PROFIT_CENTER` varchar(4000) DEFAULT NULL,
  `COST_CENTER` varchar(10) DEFAULT NULL,
  `FUND_CENTER` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_feed_status` (
  `FEED_STATUS_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`FEED_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_feed_tmpl_fm_budget` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `BATCH_ID` int DEFAULT NULL,
  `BCS_VALUE_TYPE` varchar(255) DEFAULT NULL,
  `BUDGET_AMOUNT` decimal(12,2) DEFAULT NULL,
  `BUDGET_TYPE` varchar(255) DEFAULT NULL,
  `BUDGET_VERSION` varchar(255) DEFAULT NULL,
  `COMMITMENT_ITEM` varchar(255) DEFAULT NULL,
  `CREATE_STATUS` varchar(255) DEFAULT NULL,
  `DISTRIBUTION_KEY` varchar(255) DEFAULT NULL,
  `DOC_TYPE` varchar(255) DEFAULT NULL,
  `DOCUMENT_DATE` datetime(6) DEFAULT NULL,
  `ERROR_MESSAGE` varchar(255) DEFAULT NULL,
  `FEED_ID` int DEFAULT NULL,
  `FEED_STATUS` varchar(255) DEFAULT NULL,
  `FM_GRANT` varchar(255) DEFAULT NULL,
  `FUNCTIONAL_AREA` varchar(255) DEFAULT NULL,
  `FUND_CENTER` varchar(255) DEFAULT NULL,
  `FUND_CODE` varchar(255) DEFAULT NULL,
  `FUNDED_PROGRAM` varchar(255) DEFAULT NULL,
  `LINE_ITEM` varchar(255) DEFAULT NULL,
  `LINE_ITEM_TEXT` varchar(255) DEFAULT NULL,
  `LOCAL_CURRENCY` varchar(255) DEFAULT NULL,
  `PROCESS` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `YEAR` int DEFAULT NULL,
  `USER_COMMENT` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_feed_tmpl_funded_prgm` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `BATCH_ID` int DEFAULT NULL,
  `CREATE_STATUS` varchar(255) DEFAULT NULL,
  `ERROR_MESSAGE` varchar(255) DEFAULT NULL,
  `FEED_ID` int DEFAULT NULL,
  `FEED_STATUS` varchar(255) DEFAULT NULL,
  `FUNDED_PROGRAM` varchar(255) DEFAULT NULL,
  `FUNDED_PROGRAM_TYPE` varchar(255) DEFAULT NULL,
  `PROGRAM_DESCRIPTION` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `USER_COMMENT` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_feed_tmpl_grant_bud_master` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `BATCH_ID` int DEFAULT NULL,
  `BUDGET_AMOUNT` decimal(12,2) DEFAULT NULL,
  `BUDGET_VERSION` varchar(255) DEFAULT NULL,
  `COMMITMENT_ITEM` varchar(255) DEFAULT NULL,
  `CREATE_STATUS` varchar(255) DEFAULT NULL,
  `DISTR_KEY` varchar(255) DEFAULT NULL,
  `DOCUMENT_DATE` datetime(6) DEFAULT NULL,
  `ERROR_MESSAGE` varchar(255) DEFAULT NULL,
  `FEED_ID` int DEFAULT NULL,
  `FEED_STATUS` varchar(255) DEFAULT NULL,
  `FM_AREA` varchar(255) DEFAULT NULL,
  `FUNCTIONAL_AREA` varchar(255) DEFAULT NULL,
  `FUND_CENTER` varchar(255) DEFAULT NULL,
  `FUND_CODE` varchar(255) DEFAULT NULL,
  `FUNDED_PROGRAM` varchar(255) DEFAULT NULL,
  `GM_DOC_TYPE` varchar(255) DEFAULT NULL,
  `GRANT_CODE` varchar(255) DEFAULT NULL,
  `GRANT_CUR` varchar(255) DEFAULT NULL,
  `HEADER_DESCRIPTION` varchar(255) DEFAULT NULL,
  `LINE_ITEM_TEXT` varchar(255) DEFAULT NULL,
  `POSTING_DATE` datetime(6) DEFAULT NULL,
  `PROCESS` varchar(255) DEFAULT NULL,
  `SPONSOR_CLASS` varchar(255) DEFAULT NULL,
  `SPONSOR_PROGRAM` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `YEAR` int DEFAULT NULL,
  `USER_COMMENT` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_feed_tmpl_grant_master` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `BATCH_ID` int DEFAULT NULL,
  `COMPANY_CODE` varchar(255) DEFAULT NULL,
  `CREATE_STATUS` varchar(255) DEFAULT NULL,
  `ERROR_MESSAGE` varchar(255) DEFAULT NULL,
  `FEED_ID` int DEFAULT NULL,
  `FEED_STATUS` varchar(255) DEFAULT NULL,
  `FUND_CODE` varchar(255) DEFAULT NULL,
  `GRANT_CODE` varchar(255) DEFAULT NULL,
  `GRANT_CURRENCY` varchar(255) DEFAULT NULL,
  `SPONSOR_CLASS` varchar(255) DEFAULT NULL,
  `SPONSOR_PROGRAM` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `USER_COMMENT` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_feed_tmpl_project_def` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_STATUS` varchar(255) DEFAULT NULL,
  `BATCH_ID` int DEFAULT NULL,
  `BUDGET_PROFILE` varchar(255) DEFAULT NULL,
  `BUSINESS_AREA` varchar(255) DEFAULT NULL,
  `COMPANY_CODE` varchar(255) DEFAULT NULL,
  `CONTROLLING_AREA` varchar(255) DEFAULT NULL,
  `CREATE_STATUS` varchar(255) DEFAULT NULL,
  `ERROR_MESSAGE` varchar(255) DEFAULT NULL,
  `FACTORY_CALENDER_KEY` varchar(255) DEFAULT NULL,
  `FEED_ID` int DEFAULT NULL,
  `FEED_STATUS` varchar(255) DEFAULT NULL,
  `FINISH_DATE` datetime(6) DEFAULT NULL,
  `FUNCTIONAL_AREA` varchar(255) DEFAULT NULL,
  `NUM_OF_THE_RESPONSIBLE_PERSON` varchar(255) DEFAULT NULL,
  `PLANNING_PROFILE` varchar(255) DEFAULT NULL,
  `PLANT` varchar(255) DEFAULT NULL,
  `PROFIT_CENTER` varchar(255) DEFAULT NULL,
  `PROJECT_CURRENCY` varchar(255) DEFAULT NULL,
  `PROJECT_DEFINITION` varchar(255) DEFAULT NULL,
  `PROJECT_PROFILE` varchar(255) DEFAULT NULL,
  `SHORT_DESCRIPTION` varchar(255) DEFAULT NULL,
  `START_DATE` datetime(6) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `USER_COMMENT` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_feed_tmpl_sponsor_class` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `BATCH_ID` int DEFAULT NULL,
  `CLASS_DESCRIPTION` varchar(255) DEFAULT NULL,
  `CLASS_TYPE` varchar(255) DEFAULT NULL,
  `CREATE_STATUS` varchar(255) DEFAULT NULL,
  `ERROR_MESSAGE` varchar(255) DEFAULT NULL,
  `FEED_ID` int DEFAULT NULL,
  `FEED_STATUS` varchar(255) DEFAULT NULL,
  `SPONSOR_CLASS` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `USER_COMMENT` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_feed_tmpl_sponsor_prgm` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `BATCH_ID` int DEFAULT NULL,
  `CREATE_STATUS` varchar(255) DEFAULT NULL,
  `ERROR_MESSAGE` varchar(255) DEFAULT NULL,
  `FEED_ID` int DEFAULT NULL,
  `FEED_STATUS` varchar(255) DEFAULT NULL,
  `PROGRAM_DESCRIPTION` varchar(255) DEFAULT NULL,
  `SPONSOR_PROGRAM` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `BUSINESS_AREA` varchar(4) DEFAULT NULL,
  `USER_COMMENT` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_feed_tmpl_wbs` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `ACCOUNT_ASSIGNMENT_ELEMENT` varchar(255) DEFAULT NULL,
  `BASIC_FINISH_DATE` datetime(6) DEFAULT NULL,
  `BASIC_START_DATE` datetime(6) DEFAULT NULL,
  `BATCH_ID` int DEFAULT NULL,
  `BILLING_ELEMENT` varchar(255) DEFAULT NULL,
  `BUSINESS_AREA` varchar(255) DEFAULT NULL,
  `COMPANY_CODE` varchar(255) DEFAULT NULL,
  `CONTROLLING_AREA` varchar(255) DEFAULT NULL,
  `CREATE_STATUS` varchar(255) DEFAULT NULL,
  `CURRENCY` varchar(255) DEFAULT NULL,
  `ERROR_MESSAGE` varchar(255) DEFAULT NULL,
  `FACTORY_CALENDER` varchar(255) DEFAULT NULL,
  `FEED_ID` int DEFAULT NULL,
  `FEED_STATUS` varchar(255) DEFAULT NULL,
  `FUND` varchar(255) DEFAULT NULL,
  `GST_CLAIMABLE` varchar(255) DEFAULT NULL,
  `KEYWORD_ID` varchar(255) DEFAULT NULL,
  `OBJECT_CLASS` varchar(255) DEFAULT NULL,
  `PERSON_RESPONSIBLE_NUMBER` varchar(255) DEFAULT NULL,
  `PLANT` varchar(255) DEFAULT NULL,
  `PRNCP_INVESTGTR` varchar(255) DEFAULT NULL,
  `PROFIT_CENTER` varchar(255) DEFAULT NULL,
  `PROJECT` varchar(255) DEFAULT NULL,
  `PROJECT_TYPE` varchar(255) DEFAULT NULL,
  `RESPONSIBLE_COST_CENTER` varchar(255) DEFAULT NULL,
  `SHORT_DESCRIPTION` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `WBS_ELEMENT` varchar(255) DEFAULT NULL,
  `WBS_ELEMENT_HIERARCHY` varchar(255) DEFAULT NULL,
  `WBS_LEVEL` int DEFAULT NULL,
  `USER_COMMENT` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_feed_type` (
  `FEED_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(15) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`FEED_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_feed_unit_mapping` (
  `UNIT_NUMBER` varchar(8) NOT NULL,
  `PERSON_RESPONSIBLE_NUMBER` varchar(8) NOT NULL,
  `COMPANY_CODE` varchar(80) NOT NULL,
  `CONTROLLING_AREA` varchar(80) NOT NULL,
  `PLANT` varchar(80) NOT NULL,
  `BA_CODE` varchar(4) DEFAULT NULL,
  `UNIT_TYPE` varchar(20) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime NOT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  `REFER_SUB_UNIT_FLAG` varchar(1) DEFAULT NULL,
  `SUPERIOR_SUP_ORG` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`UNIT_NUMBER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_feed_user_actions` (
  `USER_ACTION_CODE` varchar(3) NOT NULL,
  `USER_ACTION` varchar(200) DEFAULT NULL,
  `DESCRIPTION` varchar(4000) DEFAULT NULL,
  `COMMENT` varchar(4000) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(50) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `INVOICE_DESCRIPTION` varchar(4000) DEFAULT NULL,
  UNIQUE KEY `USER_ACTION_CODE` (`USER_ACTION_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_fund_center` (
  `FUND_CENTER_CODE` varchar(255) NOT NULL,
  `BUSINESS_AREA` varchar(255) DEFAULT NULL,
  `CAMPUS` varchar(255) DEFAULT NULL,
  `COMPANY_CODE` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `FM_AREA` varchar(255) DEFAULT NULL,
  `FUND_CENTER_NAME` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` varchar(255) DEFAULT NULL,
  `RESPONSIBLE_PERSON` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `VALID_FROM` datetime(6) DEFAULT NULL,
  `VALID_TO` datetime(6) DEFAULT NULL,
  `PROFIT_CENTER` varchar(255) DEFAULT NULL,
  `DATE_WHEN_FEED_INACTIVE` datetime DEFAULT NULL,
  PRIMARY KEY (`FUND_CENTER_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_grant_code` (
  `GRANT_CODE` varchar(255) NOT NULL,
  `CAMPUS` varchar(255) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `GRANT_CODE_NAME` varchar(255) DEFAULT NULL,
  `GRANT_CURRENCY` varchar(255) DEFAULT NULL,
  `GRANT_TYPE` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` varchar(255) DEFAULT NULL,
  `REMARKS` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `VALID_FROM` datetime(6) DEFAULT NULL,
  `VALID_TO` datetime(6) DEFAULT NULL,
  `COMPANY_CODE` varchar(255) DEFAULT NULL,
  `GM_SPONSOR` varchar(255) DEFAULT NULL,
  `DATE_WHEN_FEED_INACTIVE` datetime DEFAULT NULL,
  PRIMARY KEY (`GRANT_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_integration_log` (
  `SAP_INTEGRATION_LOG_ID` int NOT NULL AUTO_INCREMENT,
  `MESSAGE` varchar(1000) DEFAULT NULL,
  `MESSAGE_TYPE` varchar(45) DEFAULT NULL,
  `INTERFACE_TYPE` varchar(45) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`SAP_INTEGRATION_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sap_profit_center` (
  `PROFIT_CENTER_CODE` varchar(255) NOT NULL,
  `CAMPUS` varchar(255) DEFAULT NULL,
  `CONTROLLING_AREA` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` varchar(255) DEFAULT NULL,
  `PROFIT_CENTER_GROUP` varchar(255) DEFAULT NULL,
  `PROFIT_CENTER_NAME` varchar(255) DEFAULT NULL,
  `RESPONSIBLE_PERSON` varchar(255) DEFAULT NULL,
  `RESPONSIBLE_USER` varchar(255) DEFAULT NULL,
  `SEGMENT` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `VALID_FROM` datetime(6) DEFAULT NULL,
  `VALID_TO` datetime(6) DEFAULT NULL,
  `ENTERED_ON` varchar(10) DEFAULT NULL,
  `ENTERED_BY` varchar(60) DEFAULT NULL,
  `COMPANY_CODE` varchar(45) DEFAULT NULL,
  `SEARCH_TERM` varchar(60) DEFAULT NULL,
  `DATE_WHEN_FEED_INACTIVE` datetime DEFAULT NULL,
  PRIMARY KEY (`PROFIT_CENTER_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `science_keyword` (
  `SCIENCE_KEYWORD_CODE` varchar(15) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`SCIENCE_KEYWORD_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `scopus` (
  `SCOPUS_ID` varchar(40) NOT NULL,
  `TITLE` varchar(4000) DEFAULT NULL,
  `COVER_DATE` date DEFAULT NULL,
  `SOURCE_TITLE` varchar(2000) DEFAULT NULL,
  `ISSN` varchar(40) DEFAULT NULL,
  `SOURCE_TYPE` varchar(40) DEFAULT NULL,
  `CITATIONS` int DEFAULT NULL,
  `REFERENCE` varchar(1000) DEFAULT NULL,
  `DOI` varchar(500) DEFAULT NULL,
  `PUBLICATION_TYPE` varchar(500) DEFAULT NULL,
  `PUB_MEDIA_ID` varchar(1000) DEFAULT NULL,
  `COVER_DISPLAY_DATE` varchar(40) DEFAULT NULL,
  `CREATOR` varchar(100) DEFAULT NULL,
  `DESCRIPTION` longtext,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`SCOPUS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `scopus_affiliation` (
  `AFFILIATION_ID` varchar(40) NOT NULL,
  `SCOPUS_ID` varchar(40) NOT NULL,
  `URL` varchar(200) DEFAULT NULL,
  `NAME` varchar(200) DEFAULT NULL,
  `CITY` varchar(200) DEFAULT NULL,
  `COUNTRY` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`AFFILIATION_ID`,`SCOPUS_ID`),
  KEY `SCOPUS_AFFILIATION_FK1` (`SCOPUS_ID`),
  CONSTRAINT `SCOPUS_AFFILIATION_FK1` FOREIGN KEY (`SCOPUS_ID`) REFERENCES `scopus` (`SCOPUS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `scopus_author` (
  `AUTHOR_ID` varchar(40) NOT NULL,
  `SCOPUS_ID` varchar(40) NOT NULL,
  `AUTHOR_URL` varchar(200) DEFAULT NULL,
  `AUTHOR_NAME` varchar(100) DEFAULT NULL,
  `GIVEN_NAME` varchar(100) DEFAULT NULL,
  `SUR_NAME` varchar(100) DEFAULT NULL,
  `INITIAL` varchar(10) DEFAULT NULL,
  `AUTHOR_FLAG` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`AUTHOR_ID`,`SCOPUS_ID`),
  KEY `SCOPUS_AUTHOR_FK1` (`SCOPUS_ID`),
  CONSTRAINT `SCOPUS_AUTHOR_FK1` FOREIGN KEY (`SCOPUS_ID`) REFERENCES `scopus` (`SCOPUS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `scopus_configuration` (
  `CONFIGURATION_KEY` varchar(100) NOT NULL,
  `CONFIGURATION_VALUE` varchar(250) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`CONFIGURATION_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `scopus_metrics` (
  `SCOPUS_ID` varchar(40) NOT NULL,
  `METRIC_TYPE` varchar(100) DEFAULT NULL,
  `YEAR` int NOT NULL,
  `VALUE` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`SCOPUS_ID`,`YEAR`),
  CONSTRAINT `SCOPUS_METRICS_FK1` FOREIGN KEY (`SCOPUS_ID`) REFERENCES `scopus` (`SCOPUS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `scoring_criteria` (
  `SCORING_CRITERIA_TYPE_CODE` varchar(6) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `EXPLANATION` varchar(3000) DEFAULT NULL,
  PRIMARY KEY (`SCORING_CRITERIA_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `section_module` (
  `MODULE_CODE` int NOT NULL,
  `SECTION_CODE` varchar(255) NOT NULL,
  `SUB_MODULE_CODE` int NOT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MODULE_CODE`,`SECTION_CODE`,`SUB_MODULE_CODE`),
  KEY `SECTION_MODULE_FK1` (`SECTION_CODE`),
  CONSTRAINT `SECTION_MODULE_FK1` FOREIGN KEY (`SECTION_CODE`) REFERENCES `section_type` (`SECTION_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `section_type` (
  `SECTION_CODE` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SECTION_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `seq_award_progress_report_number` (
  `next_val` bigint NOT NULL,
  PRIMARY KEY (`next_val`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `seq_ip_id_gntr` (
  `NEXT_VAL` bigint NOT NULL,
  PRIMARY KEY (`NEXT_VAL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sftp_configuration_data` (
  `CONFIGURATION_KEY` varchar(100) NOT NULL,
  `CONFIGURATION_VALUE` varchar(250) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`CONFIGURATION_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `societal_challenge_area` (
  `CHALLENGE_AREA_CODE` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`CHALLENGE_AREA_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `societal_challenge_subarea` (
  `CHALLENGE_SUBAREA_CODE` varchar(255) NOT NULL,
  `CHALLENGE_AREA_CODE` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`CHALLENGE_SUBAREA_CODE`),
  KEY `SOCIETAL_CHALLENGE_SUBAREA_FK1` (`CHALLENGE_AREA_CODE`),
  CONSTRAINT `SOCIETAL_CHALLENGE_SUBAREA_FK1` FOREIGN KEY (`CHALLENGE_AREA_CODE`) REFERENCES `societal_challenge_area` (`CHALLENGE_AREA_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sp_rev_approval_type` (
  `APPROVAL_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`APPROVAL_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `special_review` (
  `SPECIAL_REVIEW_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) NOT NULL,
  `SORT_ID` int DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `IS_INTEGRATED` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SPECIAL_REVIEW_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `special_review_usage` (
  `SPECIAL_REVIEW_USAGE_ID` int NOT NULL,
  `SPECIAL_REVIEW_CODE` varchar(3) DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `ACTIVE_FLAG` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `GLOBAL_FLAG` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SPECIAL_REVIEW_USAGE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sponsor` (
  `SPONSOR_CODE` varchar(6) NOT NULL,
  `SPONSOR_NAME` varchar(200) DEFAULT NULL,
  `ACRONYM` varchar(10) DEFAULT NULL,
  `SPONSOR_TYPE_CODE` varchar(3) DEFAULT NULL,
  `DUN_AND_BRADSTREET_NUMBER` varchar(20) DEFAULT NULL,
  `DUNS_PLUS_FOUR_NUMBER` varchar(20) DEFAULT NULL,
  `DODAC_NUMBER` varchar(20) DEFAULT NULL,
  `CAGE_NUMBER` varchar(20) DEFAULT NULL,
  `POSTAL_CODE` varchar(15) DEFAULT NULL,
  `STATE` varchar(30) DEFAULT NULL,
  `COUNTRY_CODE` varchar(3) DEFAULT NULL,
  `ROLODEX_ID` int DEFAULT NULL,
  `AUDIT_REPORT_SENT_FOR_FY` varchar(4) DEFAULT NULL,
  `OWNED_BY_UNIT` varchar(8) DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `ACTV_IND` varchar(1) DEFAULT 'Y',
  `DUNNING_CAMPAIGN_ID` varchar(6) DEFAULT NULL,
  `CUSTOMER_NUMBER` varchar(40) DEFAULT NULL,
  `ADDRESS_LINE_1` varchar(80) DEFAULT NULL,
  `ADDRESS_LINE_2` varchar(80) DEFAULT NULL,
  `ADDRESS_LINE_3` varchar(80) DEFAULT NULL,
  `SPONSOR_LOCATION` varchar(200) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(120) DEFAULT NULL,
  `PHONE_NUMBER` varchar(20) DEFAULT NULL,
  `SPONSOR_GROUP` varchar(100) DEFAULT NULL,
  `CONTACT_PERSON` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`SPONSOR_CODE`),
  KEY `SPONSOR_FK2` (`SPONSOR_TYPE_CODE`),
  KEY `FK_SPONSOR_ROLODEX_KRA` (`ROLODEX_ID`),
  KEY `SPONSOR_FK3` (`OWNED_BY_UNIT`),
  KEY `SPONSOR_FK4` (`COUNTRY_CODE`),
  CONSTRAINT `SPONSOR_FK1` FOREIGN KEY (`ROLODEX_ID`) REFERENCES `rolodex` (`ROLODEX_ID`),
  CONSTRAINT `SPONSOR_FK2` FOREIGN KEY (`SPONSOR_TYPE_CODE`) REFERENCES `sponsor_type` (`SPONSOR_TYPE_CODE`),
  CONSTRAINT `SPONSOR_FK3` FOREIGN KEY (`OWNED_BY_UNIT`) REFERENCES `unit` (`UNIT_NUMBER`),
  CONSTRAINT `SPONSOR_FK4` FOREIGN KEY (`COUNTRY_CODE`) REFERENCES `country` (`COUNTRY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sponsor_funding_scheme` (
  `FUNDING_SCHEME_ID` int NOT NULL,
  `FUNDING_SCHEME_CODE` varchar(8) DEFAULT NULL,
  `SPONSOR_CODE` varchar(6) DEFAULT NULL,
  `DESCRIPTION` varchar(1000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`FUNDING_SCHEME_ID`),
  KEY `SPONSOR_FUNDING_SCHEME_FK1` (`FUNDING_SCHEME_CODE`),
  KEY `SPONSOR_FUNDING_SCHEME_FK2` (`SPONSOR_CODE`),
  CONSTRAINT `SPONSOR_FUNDING_SCHEME_FK1` FOREIGN KEY (`FUNDING_SCHEME_CODE`) REFERENCES `funding_scheme` (`FUNDING_SCHEME_CODE`),
  CONSTRAINT `SPONSOR_FUNDING_SCHEME_FK2` FOREIGN KEY (`SPONSOR_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sponsor_hierarchy` (
  `SPONSOR_GROUP_ID` int NOT NULL,
  `SPONSOR_GROUP_NAME` varchar(60) NOT NULL,
  `SPONSOR_ORIGINATING_GROUP_ID` int DEFAULT NULL,
  `SPONSOR_ROOT_GROUP_ID` int DEFAULT NULL,
  `SPONSOR_CODE` varchar(60) DEFAULT NULL,
  `ORDER_NUMBER` int NOT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`SPONSOR_GROUP_ID`),
  KEY `SPONSOR_FK1_idx` (`SPONSOR_CODE`),
  KEY `SPON_HIER_GROUP_FK2_idx` (`SPONSOR_ORIGINATING_GROUP_ID`),
  CONSTRAINT `SPON_HIER_GROUP_FK2` FOREIGN KEY (`SPONSOR_ORIGINATING_GROUP_ID`) REFERENCES `sponsor_hierarchy` (`SPONSOR_GROUP_ID`),
  CONSTRAINT `SPON_HIER_SPON_FK1` FOREIGN KEY (`SPONSOR_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sponsor_report` (
  `SPONSOR_REPORT_ID` int NOT NULL AUTO_INCREMENT,
  `FREQUENCY_BASE_CODE` varchar(255) DEFAULT NULL,
  `FREQUENCY_CODE` varchar(255) DEFAULT NULL,
  `REPORT_CLASS_CODE` varchar(255) DEFAULT NULL,
  `REPORT_CODE` varchar(255) DEFAULT NULL,
  `SPONSOR_CODE` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `FUNDING_SCHEME_ID` int DEFAULT NULL,
  PRIMARY KEY (`SPONSOR_REPORT_ID`),
  KEY `SPONSOR_REPORT_FK5` (`FREQUENCY_CODE`),
  KEY `SPONSOR_REPORT_FK6` (`FREQUENCY_BASE_CODE`),
  KEY `SPONSOR_REPORT_FK4` (`REPORT_CODE`),
  KEY `SPONSOR_REPORT_FK3` (`REPORT_CLASS_CODE`),
  KEY `SPONSOR_REPORT_FK1` (`SPONSOR_CODE`),
  KEY `SPONSOR_REPORT_FK2` (`FUNDING_SCHEME_ID`),
  CONSTRAINT `SPONSOR_REPORT_FK1` FOREIGN KEY (`SPONSOR_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`),
  CONSTRAINT `SPONSOR_REPORT_FK2` FOREIGN KEY (`FUNDING_SCHEME_ID`) REFERENCES `sponsor_funding_scheme` (`FUNDING_SCHEME_ID`),
  CONSTRAINT `SPONSOR_REPORT_FK3` FOREIGN KEY (`REPORT_CLASS_CODE`) REFERENCES `report_class` (`REPORT_CLASS_CODE`),
  CONSTRAINT `SPONSOR_REPORT_FK4` FOREIGN KEY (`REPORT_CODE`) REFERENCES `report` (`REPORT_CODE`),
  CONSTRAINT `SPONSOR_REPORT_FK5` FOREIGN KEY (`FREQUENCY_CODE`) REFERENCES `frequency` (`FREQUENCY_CODE`),
  CONSTRAINT `SPONSOR_REPORT_FK6` FOREIGN KEY (`FREQUENCY_BASE_CODE`) REFERENCES `frequency_base` (`FREQUENCY_BASE_CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sponsor_role` (
  `SPONSOR_ROLE_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`SPONSOR_ROLE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sponsor_term` (
  `SPONSOR_TERM_ID` int NOT NULL,
  `SPONSOR_TERM_CODE` varchar(3) DEFAULT NULL,
  `SPONSOR_TERM_TYPE_CODE` varchar(3) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`SPONSOR_TERM_ID`),
  KEY `SPONSOR_TERM_FK` (`SPONSOR_TERM_TYPE_CODE`),
  CONSTRAINT `SPONSOR_TERM_FK` FOREIGN KEY (`SPONSOR_TERM_TYPE_CODE`) REFERENCES `sponsor_term_type` (`SPONSOR_TERM_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sponsor_term_report` (
  `SPONSOR_TERM_REPORT_ID` int NOT NULL AUTO_INCREMENT,
  `SPONSOR_CODE` varchar(255) DEFAULT NULL,
  `SPONSOR_TERM_ID` int DEFAULT NULL,
  `FUNDING_SCHEME_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`SPONSOR_TERM_REPORT_ID`),
  KEY `SPONSOR_TERM_REPORT_ID_FK1` (`SPONSOR_CODE`),
  KEY `SPONSOR_TERM_REPORT_ID_FK2` (`FUNDING_SCHEME_ID`),
  KEY `SPONSOR_TERM_REPORT_ID_FK3` (`SPONSOR_TERM_ID`),
  CONSTRAINT `SPONSOR_TERM_REPORT_ID_FK1` FOREIGN KEY (`SPONSOR_CODE`) REFERENCES `sponsor` (`SPONSOR_CODE`),
  CONSTRAINT `SPONSOR_TERM_REPORT_ID_FK2` FOREIGN KEY (`FUNDING_SCHEME_ID`) REFERENCES `sponsor_funding_scheme` (`FUNDING_SCHEME_ID`),
  CONSTRAINT `SPONSOR_TERM_REPORT_ID_FK3` FOREIGN KEY (`SPONSOR_TERM_ID`) REFERENCES `sponsor_term` (`SPONSOR_TERM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sponsor_term_type` (
  `SPONSOR_TERM_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`SPONSOR_TERM_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sponsor_type` (
  `SPONSOR_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(100) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `FROM_GL_MAPPING` varchar(60) DEFAULT NULL,
  `TO_GL_MAPPING` varchar(60) DEFAULT NULL,
  `BUDGET_CATEGORY_CODE` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`SPONSOR_TYPE_CODE`),
  KEY `SPONSOR_TYPE_FK1` (`BUDGET_CATEGORY_CODE`),
  CONSTRAINT `SPONSOR_TYPE_FK1` FOREIGN KEY (`BUDGET_CATEGORY_CODE`) REFERENCES `budget_category` (`BUDGET_CATEGORY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sr_action_log` (
  `ACTION_LOG_ID` int NOT NULL AUTO_INCREMENT,
  `ACTION_TYPE_CODE` int DEFAULT NULL,
  `ASSIGNEE_PERSON_ID` varchar(40) DEFAULT NULL,
  `ASSIGNEE_PERSON_NAME` varchar(90) DEFAULT NULL,
  `STATUS_CODE` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `SR_HEADER_ID` int DEFAULT NULL,
  PRIMARY KEY (`ACTION_LOG_ID`),
  KEY `SR_ACTION_LOG_FK1` (`SR_HEADER_ID`),
  KEY `SR_ACTION_LOG_FK2` (`ACTION_TYPE_CODE`),
  KEY `SR_ACTION_LOG_FK3` (`STATUS_CODE`),
  CONSTRAINT `SR_ACTION_LOG_FK1` FOREIGN KEY (`SR_HEADER_ID`) REFERENCES `sr_header` (`SR_HEADER_ID`),
  CONSTRAINT `SR_ACTION_LOG_FK2` FOREIGN KEY (`ACTION_TYPE_CODE`) REFERENCES `sr_action_type` (`ACTION_TYPE_CODE`),
  CONSTRAINT `SR_ACTION_LOG_FK3` FOREIGN KEY (`STATUS_CODE`) REFERENCES `sr_status` (`STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sr_action_type` (
  `ACTION_TYPE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `MESSAGE` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`ACTION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sr_attachment` (
  `ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `ACTION_LOG_ID` int DEFAULT NULL,
  `ATTACHMENT` tinyblob,
  `CONTENT_TYPE` varchar(100) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `SR_HEADER_ID` int NOT NULL,
  `DOCUMENT_ID` int DEFAULT NULL,
  `DOCUMENT_STATUS_CODE` int DEFAULT NULL,
  `FILE_DATA_ID` varchar(255) DEFAULT NULL,
  `VERSION_NUMBER` int DEFAULT NULL,
  PRIMARY KEY (`ATTACHMENT_ID`),
  KEY `SR_ATTACHMENT_FK1` (`SR_HEADER_ID`),
  KEY `SR_ATTACHMENT_FK2` (`ACTION_LOG_ID`),
  CONSTRAINT `SR_ATTACHMENT_FK1` FOREIGN KEY (`SR_HEADER_ID`) REFERENCES `sr_header` (`SR_HEADER_ID`),
  CONSTRAINT `SR_ATTACHMENT_FK2` FOREIGN KEY (`ACTION_LOG_ID`) REFERENCES `sr_action_log` (`ACTION_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sr_comments` (
  `ACTION_LOG_ID` int DEFAULT NULL,
  `COMMENT_NUMBER` int DEFAULT NULL,
  `COMMENTS` varchar(4000) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `SR_HEADER_ID` int NOT NULL,
  `COMMENT_ID` int NOT NULL AUTO_INCREMENT,
  `PRIVATE_FLAG` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`COMMENT_ID`),
  KEY `SR_COMMENTS_FK1` (`SR_HEADER_ID`),
  KEY `SR_COMMENTS_FK2` (`ACTION_LOG_ID`),
  CONSTRAINT `SR_COMMENTS_FK1` FOREIGN KEY (`SR_HEADER_ID`) REFERENCES `sr_header` (`SR_HEADER_ID`),
  CONSTRAINT `SR_COMMENTS_FK2` FOREIGN KEY (`ACTION_LOG_ID`) REFERENCES `sr_action_log` (`ACTION_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sr_header` (
  `SR_HEADER_ID` int NOT NULL AUTO_INCREMENT,
  `ASSIGNEE_PERSON_ID` varchar(40) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `DESCRIPTION` varchar(4000) DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(12) DEFAULT NULL,
  `REPORTER_PERSON_ID` varchar(40) DEFAULT NULL,
  `STATUS_CODE` int DEFAULT NULL,
  `SUBJECT` varchar(1000) DEFAULT NULL,
  `TYPE_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `ORIGINATING_MODULE_ITEM_KEY` varchar(12) DEFAULT NULL,
  `IS_SYSTEM_GENERATED` varchar(1) DEFAULT NULL,
  `PRIORITY_ID` int DEFAULT NULL,
  `ADMIN_GROUP_ID` int DEFAULT NULL,
  PRIMARY KEY (`SR_HEADER_ID`),
  KEY `SR_HEADER_FK3` (`MODULE_CODE`),
  KEY `SR_HEADER_FK2` (`STATUS_CODE`),
  KEY `SR_HEADER_FK1` (`TYPE_CODE`),
  KEY `SR_HEADER_FK4` (`PRIORITY_ID`),
  KEY `SR_HEADER_FK5` (`UNIT_NUMBER`),
  KEY `SR_HEADER_FK6` (`ADMIN_GROUP_ID`),
  CONSTRAINT `SR_HEADER_FK1` FOREIGN KEY (`TYPE_CODE`) REFERENCES `sr_type` (`TYPE_CODE`),
  CONSTRAINT `SR_HEADER_FK2` FOREIGN KEY (`STATUS_CODE`) REFERENCES `sr_status` (`STATUS_CODE`),
  CONSTRAINT `SR_HEADER_FK3` FOREIGN KEY (`MODULE_CODE`) REFERENCES `coeus_module` (`MODULE_CODE`),
  CONSTRAINT `SR_HEADER_FK4` FOREIGN KEY (`PRIORITY_ID`) REFERENCES `sr_priority` (`PRIORITY_ID`),
  CONSTRAINT `SR_HEADER_FK5` FOREIGN KEY (`UNIT_NUMBER`) REFERENCES `unit` (`UNIT_NUMBER`),
  CONSTRAINT `SR_HEADER_FK6` FOREIGN KEY (`ADMIN_GROUP_ID`) REFERENCES `admin_group` (`ADMIN_GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sr_header_field` (
  `SR_HEADER_FIELD_ID` int NOT NULL AUTO_INCREMENT,
  `FIELD_NAME` varchar(30) DEFAULT NULL,
  `FIELD_TYPE` varchar(30) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `VALUE` varchar(1000) DEFAULT NULL,
  `SR_HEADER_ID` int NOT NULL,
  `IS_EDITABLE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SR_HEADER_FIELD_ID`),
  KEY `SR_HEADER_FIELD_FK1` (`SR_HEADER_ID`),
  CONSTRAINT `SR_HEADER_FIELD_FK1` FOREIGN KEY (`SR_HEADER_ID`) REFERENCES `sr_header` (`SR_HEADER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sr_header_history` (
  `SR_HEADER_HISTORY_ID` int NOT NULL AUTO_INCREMENT,
  `ACTION_LOG_ID` int DEFAULT NULL,
  `ASSIGNEE_PERSON_ID` varchar(40) DEFAULT NULL,
  `DESCRIPTION` varchar(2000) DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(12) DEFAULT NULL,
  `PREV_ASSIGNEE_PERSON_ID` varchar(40) DEFAULT NULL,
  `PREV_DESCRIPTION` varchar(2000) DEFAULT NULL,
  `PREV_MODULE_CODE` int DEFAULT NULL,
  `PREV_MODULE_ITEM_KEY` varchar(12) DEFAULT NULL,
  `PREV_REPORTER_PERSON_ID` varchar(9) DEFAULT NULL,
  `PREV_SUBJECT` varchar(1000) DEFAULT NULL,
  `PREV_TYPE_CODE` varchar(3) DEFAULT NULL,
  `REPORTER_PERSON_ID` varchar(40) DEFAULT NULL,
  `SUBJECT` varchar(1000) DEFAULT NULL,
  `TYPE_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `SR_HEADER_ID` int NOT NULL,
  `PRIORITY_ID` int DEFAULT NULL,
  `ADMIN_GROUP_ID` int DEFAULT NULL,
  `PREV_PRIORITY_ID` int DEFAULT NULL,
  `PREV_ADMIN_GROUP_ID` int DEFAULT NULL,
  `UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `PREV_UNIT_NUMBER` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`SR_HEADER_HISTORY_ID`),
  KEY `SR_HEADER_HISTORY_FK6` (`PREV_MODULE_CODE`),
  KEY `SR_HEADER_HISTORY_FK5` (`MODULE_CODE`),
  KEY `SR_HEADER_HISTORY_FK1` (`SR_HEADER_ID`),
  KEY `SR_HEADER_HISTORY_FK2` (`ACTION_LOG_ID`),
  KEY `SR_HEADER_HISTORY_FK3` (`TYPE_CODE`),
  KEY `SR_HEADER_HISTORY_FK4` (`PREV_TYPE_CODE`),
  CONSTRAINT `SR_HEADER_HISTORY_FK1` FOREIGN KEY (`SR_HEADER_ID`) REFERENCES `sr_header` (`SR_HEADER_ID`),
  CONSTRAINT `SR_HEADER_HISTORY_FK2` FOREIGN KEY (`ACTION_LOG_ID`) REFERENCES `sr_action_log` (`ACTION_LOG_ID`),
  CONSTRAINT `SR_HEADER_HISTORY_FK3` FOREIGN KEY (`TYPE_CODE`) REFERENCES `sr_type` (`TYPE_CODE`),
  CONSTRAINT `SR_HEADER_HISTORY_FK4` FOREIGN KEY (`PREV_TYPE_CODE`) REFERENCES `sr_type` (`TYPE_CODE`),
  CONSTRAINT `SR_HEADER_HISTORY_FK5` FOREIGN KEY (`MODULE_CODE`) REFERENCES `coeus_module` (`MODULE_CODE`),
  CONSTRAINT `SR_HEADER_HISTORY_FK6` FOREIGN KEY (`PREV_MODULE_CODE`) REFERENCES `coeus_module` (`MODULE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sr_priority` (
  `PRIORITY_ID` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `ACTIVE_FLAG` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`PRIORITY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sr_process_flow` (
  `PROCESS_FLOW_ID` int NOT NULL AUTO_INCREMENT,
  `ACTION_LOG_ID` int DEFAULT NULL,
  `HAS_REVIEW_RIGHT` varchar(255) DEFAULT NULL,
  `PROCESS_END_TIMESTAMP` datetime(6) DEFAULT NULL,
  `PROCESS_START_TIMESTAMP` datetime(6) DEFAULT NULL,
  `ROLE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `STATUS_CODE` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `SR_HEADER_ID` int NOT NULL,
  PRIMARY KEY (`PROCESS_FLOW_ID`),
  KEY `SR_PROCESS_FLOW_FK1` (`SR_HEADER_ID`),
  KEY `SR_PROCESS_FLOW_FK3` (`ROLE_TYPE_CODE`),
  KEY `SR_PROCESS_FLOW_FK2` (`STATUS_CODE`),
  CONSTRAINT `SR_PROCESS_FLOW_FK1` FOREIGN KEY (`SR_HEADER_ID`) REFERENCES `sr_header` (`SR_HEADER_ID`),
  CONSTRAINT `SR_PROCESS_FLOW_FK2` FOREIGN KEY (`STATUS_CODE`) REFERENCES `sr_status` (`STATUS_CODE`),
  CONSTRAINT `SR_PROCESS_FLOW_FK3` FOREIGN KEY (`ROLE_TYPE_CODE`) REFERENCES `sr_role_type` (`ROLE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sr_project` (
  `SR_PROJECT_ID` int NOT NULL AUTO_INCREMENT,
  `MODULE_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(22) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `SR_HEADER_ID` int NOT NULL,
  PRIMARY KEY (`SR_PROJECT_ID`),
  KEY `SR_PROJECT_FK1` (`SR_HEADER_ID`),
  KEY `SR_PROJECT_FK2` (`MODULE_CODE`),
  CONSTRAINT `SR_PROJECT_FK1` FOREIGN KEY (`SR_HEADER_ID`) REFERENCES `sr_header` (`SR_HEADER_ID`),
  CONSTRAINT `SR_PROJECT_FK2` FOREIGN KEY (`MODULE_CODE`) REFERENCES `coeus_module` (`MODULE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sr_reporter_change_history` (
  `REPORTER_CHANGE_HISTORY_ID` int NOT NULL AUTO_INCREMENT,
  `ACTION_LOG_ID` int DEFAULT NULL,
  `NEW_REPORTER_PERSON_ID` varchar(40) DEFAULT NULL,
  `OLD_REPORTER_PERSON_ID` varchar(40) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `SR_HEADER_ID` int NOT NULL,
  PRIMARY KEY (`REPORTER_CHANGE_HISTORY_ID`),
  KEY `SR_REPORTER_CHANGE_HISTORY_FK1` (`SR_HEADER_ID`),
  KEY `SR_REPORTER_CHANGE_HISTORY_FK2` (`ACTION_LOG_ID`),
  CONSTRAINT `SR_REPORTER_CHANGE_HISTORY_FK1` FOREIGN KEY (`SR_HEADER_ID`) REFERENCES `sr_header` (`SR_HEADER_ID`),
  CONSTRAINT `SR_REPORTER_CHANGE_HISTORY_FK2` FOREIGN KEY (`ACTION_LOG_ID`) REFERENCES `sr_action_log` (`ACTION_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sr_role_type` (
  `ROLE_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ROLE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sr_status` (
  `STATUS_CODE` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sr_status_history` (
  `SR_STATUS_HISTORY_ID` int NOT NULL AUTO_INCREMENT,
  `ACTION_END_TIME` datetime(6) DEFAULT NULL,
  `ACTION_LOG_ID` int DEFAULT NULL,
  `ACTION_START_TIME` datetime(6) DEFAULT NULL,
  `STATUS_CODE` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `SR_HEADER_ID` int NOT NULL,
  `ADMIN_GROUP_ID` int DEFAULT NULL,
  PRIMARY KEY (`SR_STATUS_HISTORY_ID`),
  KEY `SR_STATUS_HISTORY_FK1` (`SR_HEADER_ID`),
  KEY `SR_STATUS_HISTORY_FK3` (`ACTION_LOG_ID`),
  KEY `SR_STATUS_HISTORY_FK2` (`STATUS_CODE`),
  KEY `SR_STATUS_HISTORY_FK4` (`ADMIN_GROUP_ID`),
  CONSTRAINT `SR_STATUS_HISTORY_FK1` FOREIGN KEY (`SR_HEADER_ID`) REFERENCES `sr_header` (`SR_HEADER_ID`),
  CONSTRAINT `SR_STATUS_HISTORY_FK2` FOREIGN KEY (`STATUS_CODE`) REFERENCES `sr_status` (`STATUS_CODE`),
  CONSTRAINT `SR_STATUS_HISTORY_FK3` FOREIGN KEY (`ACTION_LOG_ID`) REFERENCES `sr_action_log` (`ACTION_LOG_ID`),
  CONSTRAINT `SR_STATUS_HISTORY_FK4` FOREIGN KEY (`ADMIN_GROUP_ID`) REFERENCES `admin_group` (`ADMIN_GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sr_type` (
  `TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(500) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `INSTRUCTION` varchar(4000) DEFAULT NULL,
  `SUBJECT` varchar(255) DEFAULT NULL,
  `ACTIVE_FLAG` varchar(255) DEFAULT NULL,
  `sort_order` int DEFAULT NULL,
  `module_code` int DEFAULT NULL,
  `ADMIN_GROUP_ID` int DEFAULT NULL,
  `HELP_TEXT` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`TYPE_CODE`),
  KEY `SR_TYPE_IX21` (`TYPE_CODE`),
  KEY `SR_TYPE_FK1` (`ADMIN_GROUP_ID`),
  CONSTRAINT `SR_TYPE_FK1` FOREIGN KEY (`ADMIN_GROUP_ID`) REFERENCES `admin_group` (`ADMIN_GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sr_watcher` (
  `WATCHER_ID` int NOT NULL AUTO_INCREMENT,
  `ACTION_LOG_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `WATCHER_PERSON_ID` varchar(40) DEFAULT NULL,
  `SR_HEADER_ID` int NOT NULL,
  PRIMARY KEY (`WATCHER_ID`),
  KEY `SR_WATCHER_FK1` (`SR_HEADER_ID`),
  KEY `SR_WATCHER_FK2` (`ACTION_LOG_ID`),
  CONSTRAINT `SR_WATCHER_FK1` FOREIGN KEY (`SR_HEADER_ID`) REFERENCES `sr_header` (`SR_HEADER_ID`),
  CONSTRAINT `SR_WATCHER_FK2` FOREIGN KEY (`ACTION_LOG_ID`) REFERENCES `sr_action_log` (`ACTION_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `states` (
  `STATE_CODE` varchar(3) NOT NULL,
  `COUNTRY_CODE` varchar(3) NOT NULL,
  `STATE_NAME` varchar(100) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`STATE_CODE`),
  KEY `COUNTRY_FK1` (`COUNTRY_CODE`),
  CONSTRAINT `STATE_FK1` FOREIGN KEY (`COUNTRY_CODE`) REFERENCES `country` (`COUNTRY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `system_notification` (
  `SYSTEM_NOTIFICATION_ID` int NOT NULL AUTO_INCREMENT,
  `PUBLISHED_START_DATE` datetime DEFAULT NULL,
  `PUBLISHED_END_DATE` datetime DEFAULT NULL,
  `MESSAGE` varchar(4000) DEFAULT NULL,
  `SORT_ORDER` int DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `PRIORITY` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`SYSTEM_NOTIFICATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `target_commitment_item` (
  `TARGET_COMMITMENT_ITEM_CODE` int NOT NULL AUTO_INCREMENT,
  `BUDGET_CATEGORY_CODE` varchar(3) NOT NULL,
  `COMMITMENT_ITEM` varchar(60) DEFAULT NULL,
  `COMMITMENT_DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`TARGET_COMMITMENT_ITEM_CODE`),
  KEY `TARGET_COMMITMENT_ITEM_FK1` (`BUDGET_CATEGORY_CODE`),
  CONSTRAINT `TARGET_COMMITMENT_ITEM_FK1` FOREIGN KEY (`BUDGET_CATEGORY_CODE`) REFERENCES `budget_category` (`BUDGET_CATEGORY_CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `task` (
  `TASK_ID` int NOT NULL AUTO_INCREMENT,
  `ASSIGNEE_PERSON_ID` varchar(255) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `CREATE_USER` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `DUE_DATE` datetime(6) DEFAULT NULL,
  `END_MODULE_SUB_ITEM_KEY` int DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `MODULE_ITEM_ID` varchar(255) DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(255) DEFAULT NULL,
  `START_DATE` datetime(6) DEFAULT NULL,
  `START_MODULE_SUB_ITEM_KEY` int DEFAULT NULL,
  `TASK_STATUS_CODE` varchar(255) DEFAULT NULL,
  `TASK_TYPE_CODE` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`TASK_ID`),
  KEY `TASK_FK2` (`TASK_STATUS_CODE`),
  KEY `TASK_FK1` (`TASK_TYPE_CODE`),
  KEY `TASK_IDX3` (`ASSIGNEE_PERSON_ID`),
  CONSTRAINT `TASK_FK1` FOREIGN KEY (`TASK_TYPE_CODE`) REFERENCES `task_type` (`TASK_TYPE_CODE`),
  CONSTRAINT `TASK_FK2` FOREIGN KEY (`TASK_STATUS_CODE`) REFERENCES `task_status` (`TASK_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `task_action_log` (
  `ACTION_LOG_ID` int NOT NULL AUTO_INCREMENT,
  `ACTION_TYPE_CODE` varchar(255) DEFAULT NULL,
  `SYSTEM_COMMENT` varchar(255) DEFAULT NULL,
  `TASK_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ACTION_LOG_ID`),
  KEY `TASK_ACTION_LOG_FK1` (`TASK_ID`),
  KEY `TASK_ACTION_LOG_FK2` (`ACTION_TYPE_CODE`),
  CONSTRAINT `TASK_ACTION_LOG_FK1` FOREIGN KEY (`TASK_ID`) REFERENCES `task` (`TASK_ID`),
  CONSTRAINT `TASK_ACTION_LOG_FK2` FOREIGN KEY (`ACTION_TYPE_CODE`) REFERENCES `task_action_type` (`ACTION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `task_action_type` (
  `ACTION_TYPE_CODE` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ACTION_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `task_assignee_history` (
  `ASSIGNEE_HISTORY_ID` int NOT NULL AUTO_INCREMENT,
  `ACTION_LOG_ID` int DEFAULT NULL,
  `NEW_ASSIGNEE_PERSON_ID` varchar(255) DEFAULT NULL,
  `OLD_ASSIGNEE_PERSON_ID` varchar(255) DEFAULT NULL,
  `TASK_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ASSIGNEE_HISTORY_ID`),
  KEY `TASK_ASSIGNEE_HISTORY_FK1` (`TASK_ID`),
  KEY `TASK_ASSIGNEE_HISTORY_FK2` (`ACTION_LOG_ID`),
  CONSTRAINT `TASK_ASSIGNEE_HISTORY_FK1` FOREIGN KEY (`TASK_ID`) REFERENCES `task` (`TASK_ID`),
  CONSTRAINT `TASK_ASSIGNEE_HISTORY_FK2` FOREIGN KEY (`ACTION_LOG_ID`) REFERENCES `task_action_log` (`ACTION_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `task_attachment` (
  `ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `FILE_DATA_ID` varchar(255) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `MIME_TYPE` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `TASK_ID` int NOT NULL,
  PRIMARY KEY (`ATTACHMENT_ID`),
  KEY `TASK_ATTACHMENT_FK1` (`TASK_ID`),
  CONSTRAINT `TASK_ATTACHMENT_FK1` FOREIGN KEY (`TASK_ID`) REFERENCES `task` (`TASK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `task_comment_attachment` (
  `ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `FILE_DATA_ID` varchar(255) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `MIME_TYPE` varchar(255) DEFAULT NULL,
  `TASK_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `COMMENT_ID` int NOT NULL,
  PRIMARY KEY (`ATTACHMENT_ID`),
  KEY `TASK_COMMENT_ATTACHMENT_FK1` (`TASK_ID`),
  KEY `TASK_COMMENT_ATTACHMENT_FK2` (`COMMENT_ID`),
  CONSTRAINT `TASK_COMMENT_ATTACHMENT_FK1` FOREIGN KEY (`TASK_ID`) REFERENCES `task` (`TASK_ID`),
  CONSTRAINT `TASK_COMMENT_ATTACHMENT_FK2` FOREIGN KEY (`COMMENT_ID`) REFERENCES `task_comments` (`COMMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `task_comments` (
  `COMMENT_ID` int NOT NULL AUTO_INCREMENT,
  `ACTION_LOG_ID` int DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TASK_ID` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`COMMENT_ID`),
  KEY `TASK_COMMENTS_FK1` (`TASK_ID`),
  KEY `TASK_COMMENTS_FK2` (`ACTION_LOG_ID`),
  CONSTRAINT `TASK_COMMENTS_FK1` FOREIGN KEY (`TASK_ID`) REFERENCES `task` (`TASK_ID`),
  CONSTRAINT `TASK_COMMENTS_FK2` FOREIGN KEY (`ACTION_LOG_ID`) REFERENCES `task_action_log` (`ACTION_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `task_file_data` (
  `FILE_DATA_ID` varchar(255) NOT NULL,
  `DATA` longblob,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`FILE_DATA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `task_module_mapping` (
  `MODULE_CODE` int NOT NULL,
  `SUB_MODULE_CODE` int NOT NULL,
  `TASK_TYPE_CODE` varchar(255) NOT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MODULE_CODE`,`SUB_MODULE_CODE`,`TASK_TYPE_CODE`),
  KEY `TASK_MODULE_MAPPING_FK` (`TASK_TYPE_CODE`),
  CONSTRAINT `TASK_MODULE_MAPPING_FK` FOREIGN KEY (`TASK_TYPE_CODE`) REFERENCES `task_type` (`TASK_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `task_section_mapping` (
  `SECTION_CODE` varchar(255) NOT NULL,
  `TASK_TYPE_CODE` varchar(255) NOT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SECTION_CODE`,`TASK_TYPE_CODE`),
  KEY `TASK_SECTION_MAPPING_FK1` (`TASK_TYPE_CODE`),
  CONSTRAINT `TASK_SECTION_MAPPING_FK1` FOREIGN KEY (`TASK_TYPE_CODE`) REFERENCES `task_type` (`TASK_TYPE_CODE`),
  CONSTRAINT `TASK_SECTION_MAPPING_FK2` FOREIGN KEY (`SECTION_CODE`) REFERENCES `section_type` (`SECTION_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `task_section_module_mapping` (
  `SECTION_MODULE_MAPPING_ID` int NOT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `SUB_MODULE_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(12) DEFAULT NULL,
  `SECTION_CODE` varchar(3) DEFAULT NULL,
  `TASK_TYPE_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`SECTION_MODULE_MAPPING_ID`),
  KEY `TASK_SECTION_MOD_MAPPING_FK1` (`TASK_TYPE_CODE`),
  KEY `TASK_SECTION_MOD_MAPPING_FK2` (`SECTION_CODE`),
  CONSTRAINT `TASK_SECTION_MOD_MAPPING_FK1` FOREIGN KEY (`TASK_TYPE_CODE`) REFERENCES `task_type` (`TASK_TYPE_CODE`),
  CONSTRAINT `TASK_SECTION_MOD_MAPPING_FK2` FOREIGN KEY (`SECTION_CODE`) REFERENCES `section_type` (`SECTION_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `task_status` (
  `TASK_STATUS_CODE` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`TASK_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `task_status_history` (
  `STATUS_HISTORY_ID` int NOT NULL AUTO_INCREMENT,
  `ACTION_LOG_ID` int DEFAULT NULL,
  `TASK_ID` int DEFAULT NULL,
  `TASK_STATUS_CODE` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`STATUS_HISTORY_ID`),
  KEY `TASK_STATUS_HISTORY_FK1` (`TASK_ID`),
  KEY `TASK_STATUS_HISTORY_FK2` (`ACTION_LOG_ID`),
  KEY `TASK_STATUS_HISTORY_FK3` (`TASK_STATUS_CODE`),
  CONSTRAINT `TASK_STATUS_HISTORY_FK1` FOREIGN KEY (`TASK_ID`) REFERENCES `task` (`TASK_ID`),
  CONSTRAINT `TASK_STATUS_HISTORY_FK2` FOREIGN KEY (`ACTION_LOG_ID`) REFERENCES `task_action_log` (`ACTION_LOG_ID`),
  CONSTRAINT `TASK_STATUS_HISTORY_FK3` FOREIGN KEY (`TASK_STATUS_CODE`) REFERENCES `task_status` (`TASK_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `task_type` (
  `TASK_TYPE_CODE` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `INSTRUCTION` varchar(255) DEFAULT NULL,
  `IS_REVIEW_TASK` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`TASK_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `tbn` (
  `TBN_ID` varchar(9) NOT NULL,
  `PERSON_NAME` varchar(90) DEFAULT NULL,
  `JOB_CODE` varchar(6) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `ACTIVE_FLAG` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`TBN_ID`),
  KEY `TBN_FK1` (`JOB_CODE`),
  CONSTRAINT `TBN_FK1` FOREIGN KEY (`JOB_CODE`) REFERENCES `job_code` (`JOB_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `temp_attachment_migration` (
  `ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `DATA` longblob,
  `ATTACHMENT_TYPE` varchar(255) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `MIME_TYPE` varchar(255) DEFAULT NULL,
  `PROJECT_ID` varchar(255) DEFAULT NULL,
  `PROJECT_TYPE` varchar(255) DEFAULT NULL,
  `FINANCE_PROJECT_ID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ATTACHMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `temp_user` (
  `ID` int NOT NULL,
  `CREATED_USER_NAME` varchar(255) DEFAULT NULL,
  `IS_FO_USER` varchar(255) DEFAULT NULL,
  `UNIT_NUMBER` varchar(255) DEFAULT NULL,
  `USER_NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `template_letter_template_mapping` (
  `TEMPLATE_CODE` varchar(4) NOT NULL,
  `TEMPLATE_DESCRIPTION` varchar(200) DEFAULT NULL,
  `LETTER_TEMPLATE_TYPE_CODE` varchar(4) NOT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`TEMPLATE_CODE`,`LETTER_TEMPLATE_TYPE_CODE`),
  KEY `TEMP_MAP_FKEY_1_idx` (`LETTER_TEMPLATE_TYPE_CODE`),
  CONSTRAINT `TEMP_MAP_FKEY_1` FOREIGN KEY (`LETTER_TEMPLATE_TYPE_CODE`) REFERENCES `letter_template_type` (`LETTER_TEMPLATE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `tmp_proposal_special_review` (
  `PROPOSAL_SPECIAL_REVIEW_ID` int DEFAULT NULL,
  `PROPOSAL_ID` int DEFAULT NULL,
  `APPROVAL_TYPE_CODE` varchar(3) DEFAULT NULL,
  `COMMENTS` varchar(4000) DEFAULT NULL,
  `SPECIAL_REVIEW_CODE` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `training` (
  `TRAINING_CODE` int NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`TRAINING_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `triage_header` (
  `TRIAGE_HEADER_ID` int NOT NULL AUTO_INCREMENT,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(20) DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `SUB_MODULE_CODE` int DEFAULT NULL,
  `SUB_MODULE_ITEM_KEY` varchar(20) DEFAULT NULL,
  `TRIAGE_STATUS` varchar(40) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`TRIAGE_HEADER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `triage_template_mapping` (
  `TRIAGE_TEMPLATE_ID` int NOT NULL AUTO_INCREMENT,
  `MODULE_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(20) DEFAULT NULL,
  `SUB_MODULE_CODE` int DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`TRIAGE_TEMPLATE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `unit` (
  `UNIT_NUMBER` varchar(8) NOT NULL,
  `unit_name` varchar(200) DEFAULT NULL,
  `ORGANIZATION_ID` varchar(20) DEFAULT NULL,
  `UPDATE_TIMESTAMP` date DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `PARENT_UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `ACTIVE_FLAG` varchar(1) DEFAULT 'Y',
  `PARENTNAME` varchar(100) DEFAULT NULL,
  `PARENTUNITNAME` varchar(100) DEFAULT NULL,
  `ACRONYM` varchar(255) DEFAULT NULL,
  `IS_FUNDING_UNIT` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`UNIT_NUMBER`),
  KEY `UNIT_FK1` (`PARENT_UNIT_NUMBER`),
  CONSTRAINT `UNIT_FK1` FOREIGN KEY (`PARENT_UNIT_NUMBER`) REFERENCES `unit` (`UNIT_NUMBER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `unit_administrator` (
  `UNIT_NUMBER` varchar(8) NOT NULL,
  `PERSON_ID` varchar(40) NOT NULL,
  `UNIT_ADMINISTRATOR_TYPE_CODE` varchar(3) NOT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`UNIT_NUMBER`,`PERSON_ID`,`UNIT_ADMINISTRATOR_TYPE_CODE`),
  KEY `UNIT_ADMINISTRATOR_FK2` (`UNIT_ADMINISTRATOR_TYPE_CODE`),
  CONSTRAINT `UNIT_ADMINISTRATOR_FK1` FOREIGN KEY (`UNIT_NUMBER`) REFERENCES `unit` (`UNIT_NUMBER`),
  CONSTRAINT `UNIT_ADMINISTRATOR_FK2` FOREIGN KEY (`UNIT_ADMINISTRATOR_TYPE_CODE`) REFERENCES `unit_administrator_type` (`UNIT_ADMINISTRATOR_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `unit_administrator_type` (
  `UNIT_ADMINISTRATOR_TYPE_CODE` varchar(3) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `DEFAULT_GROUP_FLAG` varchar(1) DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`UNIT_ADMINISTRATOR_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `unit_level` (
  `UNIT_NUMBER` varchar(8) NOT NULL,
  `UNIT_NAME` varchar(200) DEFAULT NULL,
  `PARENT_UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `ACTIVE_FLAG` varchar(1) DEFAULT NULL,
  `LVL` int DEFAULT NULL,
  PRIMARY KEY (`UNIT_NUMBER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `unit_tmp` (
  `ID` int NOT NULL,
  `UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `UNIT_NAME` varchar(60) DEFAULT NULL,
  `PARENT_UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `PARENT_ID` int DEFAULT NULL,
  `ACTIVE_FLAG` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `unit_with_children` (
  `UNIT_NUMBER` varchar(8) NOT NULL,
  `CHILD_UNIT_NUMBER` varchar(8) NOT NULL,
  PRIMARY KEY (`UNIT_NUMBER`,`CHILD_UNIT_NUMBER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `user_selected_widget` (
  `SELECTED_WIDGET_ID` int NOT NULL AUTO_INCREMENT,
  `WIDGET_ID` int DEFAULT NULL,
  `SORT_ORDER` int DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`SELECTED_WIDGET_ID`),
  UNIQUE KEY `USER_SELECTED_WIDGET_UK` (`PERSON_ID`,`WIDGET_ID`),
  KEY `USER_SELECTED_WIDGET_FK1` (`WIDGET_ID`),
  CONSTRAINT `USER_SELECTED_WIDGET_FK1` FOREIGN KEY (`WIDGET_ID`) REFERENCES `widget_lookup` (`WIDGET_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `valid_ce_rate_types` (
  `ACTIVE_FLAG` varchar(1) DEFAULT 'Y',
  `COST_ELEMENT` varchar(12) NOT NULL,
  `RATE_CLASS_CODE` varchar(3) NOT NULL,
  `RATE_TYPE_CODE` varchar(3) NOT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`COST_ELEMENT`,`RATE_CLASS_CODE`,`RATE_TYPE_CODE`),
  KEY `FK2g3t4p840bh00ki2pyhqeaaum` (`RATE_CLASS_CODE`,`RATE_TYPE_CODE`),
  CONSTRAINT `VALID_CE_RATE_TYPES_FK1` FOREIGN KEY (`COST_ELEMENT`) REFERENCES `cost_element` (`COST_ELEMENT`),
  CONSTRAINT `VALID_CE_RATE_TYPES_FK2` FOREIGN KEY (`RATE_CLASS_CODE`) REFERENCES `rate_class` (`RATE_CLASS_CODE`),
  CONSTRAINT `VALID_CE_RATE_TYPES_FK3` FOREIGN KEY (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`) REFERENCES `rate_type` (`RATE_CLASS_CODE`, `RATE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `valid_person_entity_rel_type` (
  `VALID_PERSON_ENTITY_REL_TYPE_CODE` int NOT NULL AUTO_INCREMENT,
  `DISCLOSURE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `RELATIONSHIP_TYPE_CODE` varchar(3) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `QUESTIONNAIRE_NUMBER` int DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`VALID_PERSON_ENTITY_REL_TYPE_CODE`),
  KEY `VALID_PERSON_ENTITY_REL_TYPE_FK1_idx` (`DISCLOSURE_TYPE_CODE`),
  KEY `VALID_PERSON_ENTITY_REL_TYPE_FK2_idx` (`RELATIONSHIP_TYPE_CODE`),
  CONSTRAINT `VALID_PERSON_ENTITY_REL_TYPE_FK1` FOREIGN KEY (`DISCLOSURE_TYPE_CODE`) REFERENCES `coi_disclosure_type` (`DISCLOSURE_TYPE_CODE`),
  CONSTRAINT `VALID_PERSON_ENTITY_REL_TYPE_FK2` FOREIGN KEY (`RELATIONSHIP_TYPE_CODE`) REFERENCES `person_entity_rel_type` (`RELATIONSHIP_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `valid_report_class` (
  `REPORT_CLASS_ID` int NOT NULL,
  `REPORT_CLASS_CODE` varchar(3) DEFAULT NULL,
  `REPORT_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`REPORT_CLASS_ID`),
  KEY `VALID_REPORT_CLASS_FK1` (`REPORT_CLASS_CODE`),
  KEY `VALID_REPORT_CLASS_FK2` (`REPORT_CODE`),
  CONSTRAINT `VALID_REPORT_CLASS_FK1` FOREIGN KEY (`REPORT_CLASS_CODE`) REFERENCES `report_class` (`REPORT_CLASS_CODE`),
  CONSTRAINT `VALID_REPORT_CLASS_FK2` FOREIGN KEY (`REPORT_CODE`) REFERENCES `report` (`REPORT_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `variation_request_header` (
  `VARIATION_REQUEST_HEADER_ID` int NOT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(255) DEFAULT NULL,
  `SECTION_CODE` varchar(255) DEFAULT NULL,
  `SUB_MODULE_CODE` int DEFAULT NULL,
  `TYPE_CODE` varchar(3) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`VARIATION_REQUEST_HEADER_ID`),
  KEY `VARIATION_REQUEST_HEADER_FK2` (`SECTION_CODE`),
  KEY `VARIATION_REQUEST_HEADER_FK1` (`TYPE_CODE`),
  CONSTRAINT `VARIATION_REQUEST_HEADER_FK1` FOREIGN KEY (`TYPE_CODE`) REFERENCES `sr_type` (`TYPE_CODE`),
  CONSTRAINT `VARIATION_REQUEST_HEADER_FK2` FOREIGN KEY (`SECTION_CODE`) REFERENCES `section_type` (`SECTION_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `variation_section_mapping` (
  `SECTION_CODE` varchar(255) NOT NULL,
  `TYPE_CODE` varchar(3) NOT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SECTION_CODE`,`TYPE_CODE`),
  KEY `VARIATION_SECTION_MAPPING_FK1` (`TYPE_CODE`),
  CONSTRAINT `VARIATION_SECTION_MAPPING_FK1` FOREIGN KEY (`TYPE_CODE`) REFERENCES `sr_type` (`TYPE_CODE`),
  CONSTRAINT `VARIATION_SECTION_MAPPING_FK2` FOREIGN KEY (`SECTION_CODE`) REFERENCES `section_type` (`SECTION_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `visibility` (
  `VISIBILITY_CODE` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`VISIBILITY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `web_socket_con_config` (
  `CONFIGURATION_KEY` varchar(100) NOT NULL,
  `CONFIGURATION_VALUE` varchar(250) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`CONFIGURATION_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `widget_lookup` (
  `WIDGET_ID` int NOT NULL,
  `DESCRIPTION` varchar(1000) DEFAULT NULL,
  `WIDGET_NAME` varchar(200) DEFAULT NULL,
  `ACTIVE_FLAG` varchar(1) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `SIZE` varchar(255) DEFAULT NULL,
  `IMAGE_PATH` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`WIDGET_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `workday_configuration_data` (
  `CONFIGURATION_KEY` varchar(255) NOT NULL,
  `CONFIGURATION_VALUE` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CONFIGURATION_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `workday_job_profile_change` (
  `WORKDAY_JOB_PROFILE_ID` int NOT NULL AUTO_INCREMENT,
  `EFFECTIVE_DATE` varchar(255) DEFAULT NULL,
  `JOB_CODE_CURRENT` varchar(255) DEFAULT NULL,
  `JOB_CODE_PROPOSED` varchar(255) DEFAULT NULL,
  `PERSON_ID` varchar(255) DEFAULT NULL,
  `TRIGGER_DATE` datetime(6) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`WORKDAY_JOB_PROFILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `workday_long_leave_details` (
  `LONG_LEAVE_ID` int NOT NULL AUTO_INCREMENT,
  `AU` varchar(255) DEFAULT NULL,
  `APPROVAL_DATE` datetime(6) DEFAULT NULL,
  `BUSINESS_TITLE` varchar(255) DEFAULT NULL,
  `FIRST_DAY_OF_LEAVE` datetime(6) DEFAULT NULL,
  `INITIATED` datetime DEFAULT NULL,
  `JOB_FAMILY` varchar(255) DEFAULT NULL,
  `JOB_FAMILY_GROUP` varchar(255) DEFAULT NULL,
  `JOB_PROFILE` varchar(255) DEFAULT NULL,
  `LAST_DAY_OF_LEAVE_ACTUAL` datetime(6) DEFAULT NULL,
  `LAST_DAY_OF_LEAVE_ESTIMATED` datetime(6) DEFAULT NULL,
  `LEAVE_TYPE_EXCLUDING_FAMILY` varchar(255) DEFAULT NULL,
  `NTU_DEPARTMENT` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `PERSON_ID` varchar(255) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `SUPERVISORY_ORGANIZATION` varchar(255) DEFAULT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `TRIGGER_DATE` datetime(6) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `WORKDAY_LONG_LEAVE` varchar(255) DEFAULT NULL,
  `UNIQUE_INITIATED` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`LONG_LEAVE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `workday_manpower_interface` (
  `WORKDAY_MANPOWER_INTERFACE_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_MANPOWER_RESOURCE_ID` int DEFAULT NULL,
  `AWARD_MANPOWER_ID` int DEFAULT NULL,
  `AWARD_ID` decimal(22,0) DEFAULT NULL,
  `INTERFACE_TYPE_CODE` varchar(3) DEFAULT NULL,
  `INTERFACE_STATUS_CODE` varchar(3) DEFAULT NULL,
  `CREATE_USER` varchar(60) DEFAULT NULL,
  `CREATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `INTERFACE_TIMESTAMP` datetime DEFAULT NULL,
  `RESOURCE_UNIQUE_ID` varchar(100) DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `END_DATE_CHANGE` int DEFAULT NULL,
  `NEW_END_DATE` datetime(6) DEFAULT NULL,
  `NEW_PI_PERSON` varchar(255) DEFAULT NULL,
  `OLD_PI_PERSON` varchar(255) DEFAULT NULL,
  `OLD_FREEZE_DATE` datetime(6) DEFAULT NULL,
  `OLD_END_DATE` datetime DEFAULT NULL,
  `IS_COST_ALLOCATION_CREATE` varchar(1) DEFAULT NULL,
  `OLD_SUPERIOR_SUP_ORG` varchar(60) DEFAULT NULL,
  `NEW_SUPERIOR_SUP_ORG` varchar(60) DEFAULT NULL,
  `COMMENTS` longtext,
  `MANPOWER_USER_ACTION_CODE` varchar(3) DEFAULT NULL,
  `IS_MAIL_ACTIVE` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`WORKDAY_MANPOWER_INTERFACE_ID`),
  KEY `WORKDAY_MANPOWER_INTERFACE_FK1` (`AWARD_MANPOWER_RESOURCE_ID`),
  KEY `WORKDAY_MANPOWER_INTERFACE_FK2` (`AWARD_MANPOWER_ID`),
  KEY `WORKDAY_MANPOWER_INTERFACE_FK3` (`INTERFACE_TYPE_CODE`),
  KEY `WORKDAY_MANPOWER_INTERFACE_FK4` (`INTERFACE_STATUS_CODE`),
  CONSTRAINT `WORKDAY_MANPOWER_INTERFACE_FK1` FOREIGN KEY (`AWARD_MANPOWER_RESOURCE_ID`) REFERENCES `award_manpower_resource` (`AWARD_MANPOWER_RESOURCE_ID`),
  CONSTRAINT `WORKDAY_MANPOWER_INTERFACE_FK2` FOREIGN KEY (`AWARD_MANPOWER_ID`) REFERENCES `award_manpower` (`AWARD_MANPOWER_ID`),
  CONSTRAINT `WORKDAY_MANPOWER_INTERFACE_FK3` FOREIGN KEY (`INTERFACE_TYPE_CODE`) REFERENCES `manpower_interface_type` (`INTERFACE_TYPE_CODE`),
  CONSTRAINT `WORKDAY_MANPOWER_INTERFACE_FK4` FOREIGN KEY (`INTERFACE_STATUS_CODE`) REFERENCES `manpower_interface_status` (`INTERFACE_STATUS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `workday_position_requisition` (
  `POSITION_ID` varchar(50) NOT NULL,
  `JOB_REQUISITION_STATUS` varchar(20) DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `JOB_REQUISITION_ID` varchar(40) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`POSITION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `workday_resource_log` (
  `WORKDAY_RESOURCE_LOG_ID` int NOT NULL AUTO_INCREMENT,
  `AWARD_ID` decimal(22,0) DEFAULT NULL,
  `AWARD_NUMBER` varchar(12) DEFAULT NULL,
  `SEQUENCE_NUMBER` int DEFAULT NULL,
  `WBS_NUMBER` varchar(50) DEFAULT NULL,
  `POSITION_STATUS_CODE` varchar(3) DEFAULT NULL,
  `COST_ALLOCATION` decimal(5,2) DEFAULT NULL,
  `CHARGE_START_DATE` datetime DEFAULT NULL,
  `CHARGE_END_DATE` datetime DEFAULT NULL,
  `BASE_SALARY_USED` varchar(200) DEFAULT NULL,
  `COMMITTED_COST` decimal(12,2) DEFAULT NULL,
  `MULTIPLIER_USED` decimal(12,2) DEFAULT NULL,
  `PERSON_ID` varchar(40) DEFAULT NULL,
  `POSITION_ID` varchar(50) DEFAULT NULL,
  `ADJUSTED_COMMITTED_COST` decimal(12,2) DEFAULT NULL,
  `JOB_PROFILE_TYPE_CODE` varchar(20) DEFAULT NULL,
  `WORKDAY_REFERENCE_ID` varchar(45) DEFAULT NULL,
  `ACTION_TYPE` varchar(30) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `ALLOCATION_PERSON_INACTIVE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`WORKDAY_RESOURCE_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `workday_termination_details` (
  `WORKDAY_TERMINATION_ID` int NOT NULL AUTO_INCREMENT,
  `AU` varchar(255) DEFAULT NULL,
  `BUSINESS_PROCESS_NAME` varchar(255) DEFAULT NULL,
  `BUSINESS_TITLE` varchar(255) DEFAULT NULL,
  `JOB_FAMILY` varchar(255) DEFAULT NULL,
  `JOB_FAMILY_GROUP` varchar(255) DEFAULT NULL,
  `JOB_PROFILE` varchar(255) DEFAULT NULL,
  `NTU_DEPARTMENT` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `NOTICE_PERIOD` varchar(255) DEFAULT NULL,
  `OVERALL_EVENT_INITIATION_DATE` datetime DEFAULT NULL,
  `OVERALL_EVENT_STATUS` varchar(255) DEFAULT NULL,
  `PERSON_ID` varchar(255) DEFAULT NULL,
  `RECOMMENDED_SYSTEM_TERMINATION_DATE` datetime(6) DEFAULT NULL,
  `RESIGNATION_APPROVE_DATE` datetime(6) DEFAULT NULL,
  `RESIGNATION_EVENT_STATUS` varchar(255) DEFAULT NULL,
  `SUPERVISORY_ORGANIZATION` varchar(255) DEFAULT NULL,
  `TERMINATION_APPROVE_DATE` varchar(45) DEFAULT NULL,
  `TERMINATION_EVENT_STATUS` varchar(255) DEFAULT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `TRIGGER_DATE` datetime(6) DEFAULT NULL,
  `UNIQUE_EVENT_INITIATION_DATE` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `WORKER_PROPOSED_RESIGNATIONN_DATE` datetime(6) DEFAULT NULL,
  `WORKER_PROPOSED_TERMINATION_DATE` datetime(6) DEFAULT NULL,
  `WORKER_RESIGNATION_NOTIFICATION_DATE` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`WORKDAY_TERMINATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `workflow` (
  `WORKFLOW_ID` int NOT NULL AUTO_INCREMENT,
  `MODULE_CODE` int DEFAULT NULL,
  `MODULE_ITEM_ID` varchar(20) DEFAULT NULL,
  `WORKFLOW_SEQUENCE` int DEFAULT NULL,
  `IS_WORKFLOW_ACTIVE` varchar(1) DEFAULT NULL,
  `WORKFLOW_START_DATE` datetime DEFAULT NULL,
  `WORKFLOW_END_DATE` datetime DEFAULT NULL,
  `WORKFLOW_START_PERSON` varchar(40) DEFAULT NULL,
  `WORKFLOW_END_PERSON` varchar(40) DEFAULT NULL,
  `COMMENTS` varchar(300) DEFAULT NULL,
  `MAP_TYPE` varchar(255) DEFAULT NULL,
  `SUB_MODULE_CODE` int DEFAULT NULL,
  `SUB_MODULE_ITEM_ID` varchar(255) DEFAULT NULL,
  `SUB_MODULE_ITEM_KEY` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`WORKFLOW_ID`),
  KEY `WORKFLOW_IX14` (`IS_WORKFLOW_ACTIVE`),
  KEY `WORKFLOW_IX8` (`MODULE_ITEM_ID`,`MODULE_CODE`,`IS_WORKFLOW_ACTIVE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `workflow_attachment` (
  `ATTACHMENT_ID` int NOT NULL AUTO_INCREMENT,
  `ATTACHMENT` longblob,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `FILE_NAME` varchar(300) DEFAULT NULL,
  `MIME_TYPE` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `WORKFLOW_DETAIL_ID` int DEFAULT NULL,
  `REVIEWER_DETAILS_ID` int DEFAULT NULL,
  `FILE_DATA_ID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ATTACHMENT_ID`),
  KEY `WORKFLOW_ATTACHMENT_FK1` (`WORKFLOW_DETAIL_ID`),
  KEY `WORKFLOW_ATTACHMENT_FK2` (`REVIEWER_DETAILS_ID`),
  CONSTRAINT `WORKFLOW_ATTACHMENT_FK1` FOREIGN KEY (`WORKFLOW_DETAIL_ID`) REFERENCES `workflow_detail` (`WORKFLOW_DETAIL_ID`),
  CONSTRAINT `WORKFLOW_ATTACHMENT_FK2` FOREIGN KEY (`REVIEWER_DETAILS_ID`) REFERENCES `workflow_reviewer_detail` (`REVIEWER_DETAILS_ID`),
  CONSTRAINT `WORKFLOW_ATTACHMENT_FK3` FOREIGN KEY (`REVIEWER_DETAILS_ID`) REFERENCES `workflow_reviewer_detail` (`REVIEWER_DETAILS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `workflow_column_nextvalue` (
  `RULE_ID` int NOT NULL,
  `WORKFLOW_ID` int DEFAULT NULL,
  `WORKFLOW_DETAIL_ID` int DEFAULT NULL,
  `MAP_ID` int DEFAULT NULL,
  `MAP_DETAIL_ID` int DEFAULT NULL,
  `RULES_EXPERSSION_ID` int DEFAULT NULL,
  `RULES_EXPERSSION_ARG_ID` int DEFAULT NULL,
  PRIMARY KEY (`RULE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `workflow_detail` (
  `WORKFLOW_DETAIL_ID` int NOT NULL AUTO_INCREMENT,
  `WORKFLOW_ID` int DEFAULT NULL,
  `MAP_ID` int DEFAULT NULL,
  `MAP_NUMBER` int DEFAULT NULL,
  `APPROVAL_STOP_NUMBER` int DEFAULT NULL,
  `APPROVER_NUMBER` int DEFAULT NULL,
  `PRIMARY_APPROVER_FLAG` varchar(1) DEFAULT NULL,
  `APPROVER_PERSON_ID` varchar(40) DEFAULT NULL,
  `APPROVER_PERSON_NAME` varchar(90) DEFAULT NULL,
  `APPROVAL_STATUS` varchar(1) DEFAULT NULL,
  `DELEGATED_BY_PERSON_ID` varchar(40) DEFAULT NULL,
  `APPROVAL_COMMENT` varchar(2000) DEFAULT NULL,
  `APPROVAL_DATE` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `ROLE_TYPE_CODE` int DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `STOP_NAME` varchar(200) DEFAULT NULL,
  `MAP_NAME` varchar(200) DEFAULT NULL,
  `MAP_DESCRIPTION` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`WORKFLOW_DETAIL_ID`),
  KEY `WORKFLOW_DETAIL_FK1` (`WORKFLOW_ID`),
  KEY `WORKFLOW_DETAIL_FK2` (`MAP_ID`),
  KEY `WORKFLOW_DETAIL_FK3` (`APPROVAL_STATUS`),
  KEY `WORKFLOW_DETAIL_FK4` (`ROLE_TYPE_CODE`),
  KEY `WORKFLOW_DETAIL_FK5` (`APPROVER_PERSON_ID`,`WORKFLOW_ID`),
  CONSTRAINT `WORKFLOW_DETAIL_FK1` FOREIGN KEY (`WORKFLOW_ID`) REFERENCES `workflow` (`WORKFLOW_ID`),
  CONSTRAINT `WORKFLOW_DETAIL_FK2` FOREIGN KEY (`MAP_ID`) REFERENCES `workflow_map` (`MAP_ID`),
  CONSTRAINT `WORKFLOW_DETAIL_FK3` FOREIGN KEY (`APPROVAL_STATUS`) REFERENCES `workflow_status` (`APPROVAL_STATUS`),
  CONSTRAINT `WORKFLOW_DETAIL_FK4` FOREIGN KEY (`ROLE_TYPE_CODE`) REFERENCES `person_role_type` (`ROLE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `workflow_detail_ext` (
  `WORKFLOW_DETAIL_EXT_ID` int NOT NULL AUTO_INCREMENT,
  `WORKFLOW_DETAIL_ID` int DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `MODULE_ITEM_KEY` varchar(20) DEFAULT NULL,
  `FEEDBACK_TYPE_CODE` varchar(3) DEFAULT NULL,
  `SUB_MODULE_CODE` int DEFAULT NULL,
  `SUB_MODULE_ITEM_KEY` varchar(20) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`WORKFLOW_DETAIL_EXT_ID`),
  KEY `WORKFLOW_DETAIL_EXT_FK1` (`WORKFLOW_DETAIL_ID`),
  KEY `WORKFLOW_DETAIL_EXT_FK2` (`FEEDBACK_TYPE_CODE`),
  CONSTRAINT `WORKFLOW_DETAIL_EXT_FK1` FOREIGN KEY (`WORKFLOW_DETAIL_ID`) REFERENCES `workflow_detail` (`WORKFLOW_DETAIL_ID`),
  CONSTRAINT `WORKFLOW_DETAIL_EXT_FK2` FOREIGN KEY (`FEEDBACK_TYPE_CODE`) REFERENCES `workflow_feedback_type` (`FEEDBACK_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `workflow_feedback_type` (
  `FEEDBACK_TYPE_CODE` varchar(3) NOT NULL,
  `ROLE_TYPE_CODE` int DEFAULT NULL,
  `MODULE_CODE` int DEFAULT NULL,
  `SUB_MODULE_CODE` int DEFAULT NULL,
  `DESCRIPTION` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `ACTIVE_FLAG` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`FEEDBACK_TYPE_CODE`),
  KEY `WORKFLOW_FEEDBACK_TYPE_FK1` (`ROLE_TYPE_CODE`),
  CONSTRAINT `WORKFLOW_FEEDBACK_TYPE_FK1` FOREIGN KEY (`ROLE_TYPE_CODE`) REFERENCES `person_role_type` (`ROLE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `workflow_map` (
  `MAP_ID` int NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `MAP_TYPE` varchar(1) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UNIT_NUMBER` varchar(8) DEFAULT NULL,
  `MAP_NAME` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`MAP_ID`),
  KEY `WORKFLOW_MAP_FK1` (`MAP_TYPE`),
  CONSTRAINT `WORKFLOW_MAP_FK1` FOREIGN KEY (`MAP_TYPE`) REFERENCES `workflow_map_type` (`MAP_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `workflow_map_detail` (
  `MAP_DETAIL_ID` int NOT NULL AUTO_INCREMENT,
  `MAP_ID` int DEFAULT NULL,
  `APPROVAL_STOP_NUMBER` int DEFAULT NULL,
  `APPROVER_NUMBER` int DEFAULT NULL,
  `APPROVER_PERSON_ID` varchar(40) DEFAULT NULL,
  `PRIMARY_APPROVER_FLAG` varchar(1) DEFAULT NULL,
  `IS_ROLE` varchar(1) DEFAULT NULL,
  `ROLE_TYPE_CODE` int DEFAULT NULL,
  `DESCRIPTION` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `STOP_NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MAP_DETAIL_ID`),
  KEY `WORKFLOW_MAP_DETAIL_FK1` (`MAP_ID`),
  KEY `WORKFLOW_MAP_DETAIL_FK2` (`ROLE_TYPE_CODE`),
  KEY `WORKFLOW_MAP_DETAIL_FK3` (`APPROVER_PERSON_ID`),
  CONSTRAINT `WORKFLOW_MAP_DETAIL_FK1` FOREIGN KEY (`MAP_ID`) REFERENCES `workflow_map` (`MAP_ID`),
  CONSTRAINT `WORKFLOW_MAP_DETAIL_FK2` FOREIGN KEY (`ROLE_TYPE_CODE`) REFERENCES `person_role_type` (`ROLE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `workflow_map_type` (
  `MAP_TYPE` varchar(1) NOT NULL,
  `DESCRIPTION` varchar(100) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `IS_ACTIVE` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`MAP_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `workflow_reviewer_attmnts` (
  `WORKFLOW_REVIEWER_ATTMNTS_ID` int NOT NULL AUTO_INCREMENT,
  `FILE_DATA_ID` varchar(255) DEFAULT NULL,
  `FILE_NAME` varchar(255) DEFAULT NULL,
  `MIME_TYPE` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `WORKFLOW_REVIEWER_COMMENTS_ID` int NOT NULL,
  PRIMARY KEY (`WORKFLOW_REVIEWER_ATTMNTS_ID`),
  KEY `WORKFLOW_REVIEWER_ATTMNTS_FK2` (`WORKFLOW_REVIEWER_COMMENTS_ID`),
  CONSTRAINT `WORKFLOW_REVIEWER_ATTMNTS_FK2` FOREIGN KEY (`WORKFLOW_REVIEWER_COMMENTS_ID`) REFERENCES `workflow_reviewer_comments` (`WORKFLOW_REVIEWER_COMMENTS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `workflow_reviewer_comments` (
  `WORKFLOW_REVIEWER_COMMENTS_ID` int NOT NULL AUTO_INCREMENT,
  `COMMENT` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `WORKFLOW_REVIEWER_SCORE_ID` int NOT NULL,
  `IS_PRIVATE` varchar(1) DEFAULT NULL,
  `IS_PUBLIC` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`WORKFLOW_REVIEWER_COMMENTS_ID`),
  KEY `WORKFLOW_REVIEWER_COMMENTS_FK1` (`WORKFLOW_REVIEWER_SCORE_ID`),
  CONSTRAINT `WORKFLOW_REVIEWER_COMMENTS_FK1` FOREIGN KEY (`WORKFLOW_REVIEWER_SCORE_ID`) REFERENCES `workflow_reviewer_score` (`WORKFLOW_REVIEWER_SCORE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `workflow_reviewer_detail` (
  `REVIEWER_DETAILS_ID` int NOT NULL,
  `APPROVAL_STATUS` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `REVIEW_DATE` date DEFAULT NULL,
  `REVIEWER_PERSON_ID` varchar(255) DEFAULT NULL,
  `REVIEWER_PERSON_NAME` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `WORKFLOW_DETAIL_ID` int DEFAULT NULL,
  PRIMARY KEY (`REVIEWER_DETAILS_ID`),
  KEY `WRKFLW_REVIWER_DTL_FK1` (`WORKFLOW_DETAIL_ID`),
  KEY `WRKFLW_REVIWER_DTL_FK2` (`APPROVAL_STATUS`),
  CONSTRAINT `WRKFLW_REVIWER_DTL_FK1` FOREIGN KEY (`WORKFLOW_DETAIL_ID`) REFERENCES `workflow_detail` (`WORKFLOW_DETAIL_ID`),
  CONSTRAINT `WRKFLW_REVIWER_DTL_FK2` FOREIGN KEY (`APPROVAL_STATUS`) REFERENCES `workflow_status` (`APPROVAL_STATUS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `workflow_reviewer_score` (
  `WORKFLOW_REVIEWER_SCORE_ID` int NOT NULL AUTO_INCREMENT,
  `SCORE` decimal(4,2) DEFAULT NULL,
  `SCORING_CRITERIA_TYPE_CODE` varchar(255) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime(6) DEFAULT NULL,
  `UPDATE_USER` varchar(255) DEFAULT NULL,
  `WORKFLOW_DETAIL_ID` int DEFAULT NULL,
  PRIMARY KEY (`WORKFLOW_REVIEWER_SCORE_ID`),
  KEY `WORKFLOW_REVIEWER_SCORE_FK2` (`SCORING_CRITERIA_TYPE_CODE`),
  KEY `WORKFLOW_REVIEWER_SCORE_FK1` (`WORKFLOW_DETAIL_ID`),
  CONSTRAINT `WORKFLOW_REVIEWER_SCORE_FK1` FOREIGN KEY (`WORKFLOW_DETAIL_ID`) REFERENCES `workflow_detail` (`WORKFLOW_DETAIL_ID`),
  CONSTRAINT `WORKFLOW_REVIEWER_SCORE_FK2` FOREIGN KEY (`SCORING_CRITERIA_TYPE_CODE`) REFERENCES `scoring_criteria` (`SCORING_CRITERIA_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `workflow_role_type` (
  `ROLE_TYPE_CODE` int NOT NULL,
  `DESCRIPTION` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`ROLE_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `workflow_status` (
  `APPROVAL_STATUS` varchar(1) NOT NULL,
  `DESCRIPTION` varchar(100) DEFAULT NULL,
  `UPDATE_USER` varchar(60) DEFAULT NULL,
  `UPDATE_TIMESTAMP` date DEFAULT NULL,
  PRIMARY KEY (`APPROVAL_STATUS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

SET FOREIGN_KEY_CHECKS=1;