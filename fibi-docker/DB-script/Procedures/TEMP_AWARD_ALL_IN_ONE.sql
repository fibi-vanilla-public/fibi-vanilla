DELIMITER //
CREATE  PROCEDURE `TEMP_AWARD_ALL_IN_ONE`(AV_TYPE INT)
BEGIN
DECLARE LS_LEGACY_PROJECT_ID	    VARCHAR(100);
DECLARE LS_LEGACY_WBS_NUMBER	    VARCHAR(100);
DECLARE LS_PERSON_ID				VARCHAR(40);
DECLARE LS_ROLODEX_ID				VARCHAR(10);
DECLARE LS_PERSON_ROLE				VARCHAR(50);
DECLARE LI_PERSON_ROLE_CODE 		INT(12);
DECLARE LS_PERCENTAGE_OF_EFFORT 	VARCHAR(5);
DECLARE LI_COUNT 					INT;
DECLARE LS_FILE_TYPE 				VARCHAR(50);
DECLARE LS_FULL_NAME 				VARCHAR(90);
DECLARE LS_EMAIL_ADDRESS			VARCHAR(255);
DECLARE LI_AWARD_ID  				INT(22);
DECLARE LS_AWARD_NUMBER 			VARCHAR(12);
DECLARE LI_AWARD_PERSON_ID 		INT(11);
DECLARE LS_HOME_UNIT               VARCHAR(8);
DECLARE LI_AWARD_PERSON_UNIT_ID DECIMAL(22,0);
DECLARE LS_IS_MULTI_PI             VARCHAR(1);
DECLARE LS_ACCOUNT_NUMBER	    VARCHAR(100);
DECLARE LS_INTERNAL_ORDER_CODE VARCHAR(100);
BEGIN 
		DECLARE LS_ERROR VARCHAR(2000);
        DECLARE DONE1 INT DEFAULT FALSE;
		DECLARE MIGRATION_AWARD_PERSONS_CURSOR CURSOR FOR 
		SELECT distinct ACCOUNT_NUMBER
		FROM MIGRATED_AWARD_NUMBERS;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE1 = TRUE;
		DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
			GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, 
			 @errno = MYSQL_ERRNO, @msg = MESSAGE_TEXT;
			SET @full_error = CONCAT("ERROR ", @errno, " (", @sqlstate, "): ", @msg);
			SELECT @full_error INTO LS_ERROR;
			SELECT 'error';
		END;
		OPEN MIGRATION_AWARD_PERSONS_CURSOR;
		MIGRATION_AWARD_PERSONS_CURSOR_LOOP : LOOP
				FETCH MIGRATION_AWARD_PERSONS_CURSOR INTO LS_ACCOUNT_NUMBER;
				IF DONE1 THEN
					LEAVE MIGRATION_AWARD_PERSONS_CURSOR_LOOP;
				END IF;
				INSERT INTO MIGRATED_AWARD_NUMBERS_EXT(NEW_COL)
				VALUES(
				LS_ACCOUNT_NUMBER
				);
				COMMIT;
				BEGIN 
						DECLARE LS_ERROR VARCHAR(2000);
						DECLARE DONE2 INT DEFAULT FALSE;
						DECLARE MIGRATION_AWARD_PERSONS_CURSOR CURSOR FOR 
						SELECT  INTERNAL_ORDER_CODE
						FROM MIGRATED_AWARD_NUMBERS WHERE ACCOUNT_NUMBER = LS_ACCOUNT_NUMBER;
						DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE2 = TRUE;
						DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
						BEGIN
							GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, 
							 @errno = MYSQL_ERRNO, @msg = MESSAGE_TEXT;
							SET @full_error = CONCAT("ERROR ", @errno, " (", @sqlstate, "): ", @msg);
							SELECT @full_error INTO LS_ERROR;
							SELECT 'error';
						END;
						OPEN MIGRATION_AWARD_PERSONS_CURSOR;
						MIGRATION_AWARD_PERSONS_CURSOR_LOOP2 : LOOP
								FETCH MIGRATION_AWARD_PERSONS_CURSOR INTO LS_INTERNAL_ORDER_CODE;
								IF DONE2 THEN
									LEAVE MIGRATION_AWARD_PERSONS_CURSOR_LOOP2;
								END IF;
								INSERT INTO MIGRATED_AWARD_NUMBERS_EXT(NEW_COL)
												VALUES(
												LS_INTERNAL_ORDER_CODE
												);
												COMMIT;
								END LOOP;
						CLOSE MIGRATION_AWARD_PERSONS_CURSOR;
				END;
		END LOOP;
		CLOSE MIGRATION_AWARD_PERSONS_CURSOR;
END;
SELECT 1  AS RESULT;
END
//
