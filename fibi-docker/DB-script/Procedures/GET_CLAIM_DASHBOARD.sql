DELIMITER //
CREATE  PROCEDURE `GET_CLAIM_DASHBOARD`(
AV_AWARD_ID                     VARCHAR(20),
AV_AWARD_NUMBER         VARCHAR(20),
AV_ACCOUNT_NUMBER       VARCHAR(200),
AV_TITLE                VARCHAR(200),
AV_LEAD_UNIT_NUMBER     VARCHAR(200),
AV_SPONSOR_CODE         VARCHAR(200),
AV_RESEARCH_PERSON_ID   VARCHAR(200),
AV_CLAIM_STATUS             VARCHAR(200),
AV_PREPARER_PERSON_ID                             VARCHAR(200),
AV_CREATED_USER                              VARCHAR(200),
AV_PERSON_ID                             VARCHAR(200),
AV_SORT_TYPE                             VARCHAR(500),
AV_PAGED                                         INT(10),
AV_LIMIT                                         INT(10),
AV_TAB_TYPE                                  VARCHAR(30),
AV_UNLIMITED                     BOOLEAN,
AV_TYPE                          VARCHAR(1),
AV_ROLE_ID                          VARCHAR(20),
AV_SPONSOR_AWARD_NUMBER          VARCHAR(200),
AV_CLAIM_NUMBER          VARCHAR(200)
)
BEGIN
DECLARE LS_DYN_SQL LONGTEXT;
DECLARE LS_FILTER_CONDITION LONGTEXT;
DECLARE LS_OFFSET_CONDITION VARCHAR(600);
DECLARE LS_OFFSET INT(11);
DECLARE TAB_QUERY LONGTEXT;
DECLARE TAB_QUERY1 LONGTEXT;
DECLARE JOIN_CONDITION LONGTEXT;
DECLARE SELECTED_FIELD_LIST LONGTEXT;
SET LS_OFFSET = (AV_LIMIT * AV_PAGED);
SET LS_FILTER_CONDITION ='';
SET LS_DYN_SQL ='';
SET JOIN_CONDITION = '';
SET SELECTED_FIELD_LIST= '';
SET TAB_QUERY ='';
IF AV_TYPE = 'A'   THEN
                        IF AV_AWARD_ID IS NOT NULL AND AV_AWARD_ID <> '' THEN
                          SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.AWARD_ID LIKE ''%',AV_AWARD_ID,'%'' AND ');
                        END IF;
                        IF AV_ACCOUNT_NUMBER IS NOT NULL  AND AV_ACCOUNT_NUMBER <> '' THEN
                                SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.ACCOUNT_NUMBER LIKE ''%',AV_ACCOUNT_NUMBER,'%'' AND ');
                        END IF;
                        IF AV_TITLE IS NOT NULL  AND AV_TITLE <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.TITLE LIKE ''%',AV_TITLE,'%'' AND ');
            END IF;
                        IF AV_LEAD_UNIT_NUMBER IS NOT NULL  AND AV_LEAD_UNIT_NUMBER <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.LEAD_UNIT_NUMBER LIKE ''%',AV_LEAD_UNIT_NUMBER,'%'' AND ');
                        END IF;
                        IF AV_SPONSOR_CODE IS NOT NULL  AND AV_SPONSOR_CODE <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.SPONSOR_CODE = ',AV_SPONSOR_CODE,' AND ');
                        END IF;
                        IF AV_PREPARER_PERSON_ID IS NOT NULL  AND AV_PREPARER_PERSON_ID <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.PREPARER_PERSON_ID IN (',AV_PREPARER_PERSON_ID,') AND ');
                        END IF;
            IF AV_AWARD_NUMBER IS NOT NULL  AND AV_AWARD_NUMBER <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.AWARD_NUMBER LIKE ''%',AV_AWARD_NUMBER,'%'' AND ');
                        END IF;
                        IF AV_CREATED_USER IS NOT NULL  AND AV_CREATED_USER <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.CREATE_USER LIKE ''%',AV_CREATED_USER,'%'' AND ');
                        END IF;
                        IF (AV_ROLE_ID IS NOT NULL AND AV_ROLE_ID <> '') and (AV_RESEARCH_PERSON_ID IS NOT NULL AND AV_RESEARCH_PERSON_ID <> '') THEN
                                        SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.AWARD_ID IN (SELECT AWARD_ID
                                                                                                                                                                                 FROM AWARD_PERSONS WHERE PERSON_ROLE_ID IN(
                                                                                                                                                                ',AV_ROLE_ID,') AND (PERSON_ID =''',AV_RESEARCH_PERSON_ID,''' OR ROLODEX_ID =''',AV_RESEARCH_PERSON_ID,''')) AND ');
                        END IF;
                        IF (AV_ROLE_ID IS NOT NULL AND AV_ROLE_ID <> '') and (AV_RESEARCH_PERSON_ID IS NULL OR AV_RESEARCH_PERSON_ID = '') THEN
                                SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.AWARD_ID IN (SELECT AWARD_ID
                                                                                                                                                                                 FROM AWARD_PERSONS WHERE PERSON_ROLE_ID IN(
                                                                                                                                                                ',AV_ROLE_ID,')) AND ');
                        END IF;
                        IF (AV_ROLE_ID is null OR AV_ROLE_ID = '') AND (AV_RESEARCH_PERSON_ID IS NOT NULL AND AV_RESEARCH_PERSON_ID <> '') THEN
                                SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.AWARD_ID IN(SELECT AWARD_ID FROM AWARD_PERSONS
                                                                                                                                                                        WHERE (PERSON_ID =''',AV_RESEARCH_PERSON_ID,''' OR ROLODEX_ID =''',AV_RESEARCH_PERSON_ID,''')) AND ' );
                        END IF;
                        IF AV_SPONSOR_AWARD_NUMBER IS NOT NULL  AND AV_SPONSOR_AWARD_NUMBER <> '' THEN
                         SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.SPONSOR_AWARD_NUMBER LIKE ''%',AV_SPONSOR_AWARD_NUMBER,'%'' AND ');
                        END IF;
						IF AV_CLAIM_NUMBER IS NOT NULL  AND AV_CLAIM_NUMBER <> '' THEN
                                SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.CLAIM_NUMBER LIKE ''%',AV_CLAIM_NUMBER,'%'' AND ');
                        END IF;
						IF AV_CLAIM_STATUS IS NOT NULL  AND AV_CLAIM_STATUS <> '' THEN
						SET LS_FILTER_CONDITION = CONCAT(LS_FILTER_CONDITION,' T.CLAIM_STATUS_CODE IN (',AV_CLAIM_STATUS,')  AND ');
						END IF;
END IF;
IF AV_TAB_TYPE = 'QUALIFIED_CLAIMS' THEN
		SET JOIN_CONDITION = CONCAT(JOIN_CONDITION,' LEFT JOIN CLAIM T24 ON T24.AWARD_NUMBER = T1.AWARD_NUMBER AND T24.CLAIM_ID IN (SELECT MAX(T27.CLAIM_ID) FROM CLAIM T27
                                    WHERE T27.AWARD_NUMBER = T24.AWARD_NUMBER )
									LEFT JOIN PERSON T25 ON T25.USER_NAME = T24.CREATE_USER
									LEFT JOIN PERSON T26 ON T26.USER_NAME = T24.UPDATE_USER
									LEFT OUTER JOIN CUSTOM_DATA_V T29 ON T1.AWARD_ID = T29.MODULE_ITEM_KEY 
                                    LEFT JOIN PERSON T30 ON T30.PERSON_ID = T29.CLAIM_PREPARER
                                    LEFT JOIN CLAIM_STATUS T33 ON T33.CLAIM_STATUS_CODE = T24.CLAIM_STATUS_CODE	');
		SET TAB_QUERY = CONCAT(' WHERE T1.AWARD_ID NOT IN (SELECT AWARD_ID FROM CLAIM WHERE CLAIM_STATUS_CODE IN (1,2,3,5))
		AND T1.AWARD_SEQUENCE_STATUS = ''ACTIVE''
        AND T1.STATUS_CODE <> ''5''
        AND T1.ACCOUNT_TYPE_CODE NOT IN (7,3,5)
		AND T1.PAYMENT_INVOICE_FREQ_CODE in (6,10,11,12,5,4)
		AND T1.METHOD_OF_PAYMENT_CODE IN (1)
		AND T1.BASIS_OF_PAYMENT_CODE IN (1,7)
		AND T1.AWARD_TYPE_CODE IN (2,4)
		AND ((T1.LEAD_UNIT_NUMBER IN(SELECT DISTINCT UNIT_NUMBER FROM PERSON_ROLE_RT WHERE RIGHT_NAME
		IN (''CLAIM_PREPARER'') AND PERSON_ID =''',AV_PERSON_ID,'''))
        OR T7.HOME_UNIT_NUMBER IN (SELECT DISTINCT UNIT_NUMBER FROM PERSON_ROLE_RT WHERE RIGHT_NAME IN (''CLAIM_PREPARER'') AND PERSON_ID = ''',AV_PERSON_ID,''' AND ROLE_ID =''100''))' );
ELSEIF AV_TAB_TYPE = 'PENDING_CLAIMS' THEN
		SET JOIN_CONDITION = CONCAT(JOIN_CONDITION,' INNER JOIN CLAIM T24 ON T24.AWARD_NUMBER = T1.AWARD_NUMBER 
									INNER JOIN PERSON T25 ON T25.USER_NAME = T24.CREATE_USER
									INNER JOIN PERSON T26 ON T26.USER_NAME = T24.UPDATE_USER
                                    LEFT JOIN PERSON T30 ON T30.PERSON_ID = null
                                    INNER JOIN CLAIM_STATUS T33 ON T33.CLAIM_STATUS_CODE = T24.CLAIM_STATUS_CODE');
		SET TAB_QUERY = CONCAT(' WHERE T24.CLAIM_STATUS_CODE <> 4 AND T1.AWARD_SEQUENCE_STATUS = ''ACTIVE''
		AND ((T1.LEAD_UNIT_NUMBER IN(SELECT DISTINCT UNIT_NUMBER FROM PERSON_ROLE_RT WHERE RIGHT_NAME
		IN (''VIEW_CLAIMS'',''CLAIM_PREPARER'',''MODIFY_CLAIMS'') AND PERSON_ID =''',AV_PERSON_ID,'''))
        OR T7.HOME_UNIT_NUMBER IN (SELECT DISTINCT UNIT_NUMBER FROM PERSON_ROLE_RT WHERE RIGHT_NAME IN (''VIEW_CLAIMS'',''CLAIM_PREPARER'',''MODIFY_CLAIMS'') AND PERSON_ID = ''',AV_PERSON_ID,''' AND ROLE_ID =''100'')
        OR (T1.AWARD_ID IN(SELECT DISTINCT S1.AWARD_ID FROM AWARD_PERSON_ROLES S1
		INNER JOIN ROLE R1 ON R1.ROLE_ID = S1.ROLE_ID
		INNER JOIN ROLE_RIGHTS R2 ON R2.ROLE_ID = R1.ROLE_ID
		INNER JOIN RIGHTS R3 ON R3.RIGHT_ID = R2.RIGHT_ID AND R3.RIGHT_NAME IN (''VIEW_CLAIMS'',''MODIFY_CLAIMS'') WHERE S1.PERSON_ID=''',AV_PERSON_ID,''' )))');
ELSEIF AV_TAB_TYPE = 'COMPLETED_CLAIMS' THEN
		SET JOIN_CONDITION = CONCAT(JOIN_CONDITION,' INNER JOIN CLAIM T24 ON T24.AWARD_NUMBER = T1.AWARD_NUMBER 
									INNER JOIN PERSON T25 ON T25.USER_NAME = T24.CREATE_USER
									INNER JOIN PERSON T26 ON T26.USER_NAME = T24.UPDATE_USER
                                    LEFT JOIN PERSON T30 ON T30.PERSON_ID = null
                                    INNER JOIN CLAIM_STATUS T33 ON T33.CLAIM_STATUS_CODE = T24.CLAIM_STATUS_CODE');
		SET TAB_QUERY = CONCAT(' WHERE T1.AWARD_SEQUENCE_STATUS NOT IN (''ARCHIVE'', ''CANCELLED'') AND T24.CLAIM_STATUS_CODE = 4
		AND T1.AWARD_SEQUENCE_STATUS = ''ACTIVE'' AND ((T1.LEAD_UNIT_NUMBER IN(SELECT DISTINCT UNIT_NUMBER FROM PERSON_ROLE_RT WHERE RIGHT_NAME
		IN (''VIEW_CLAIMS'',''CLAIM_PREPARER'',''MODIFY_CLAIMS'') AND PERSON_ID =''',AV_PERSON_ID,'''))
        OR T7.HOME_UNIT_NUMBER IN (SELECT DISTINCT UNIT_NUMBER FROM PERSON_ROLE_RT WHERE RIGHT_NAME IN (''VIEW_CLAIMS'',''CLAIM_PREPARER'',''MODIFY_CLAIMS'') AND PERSON_ID = ''',AV_PERSON_ID,''' AND ROLE_ID =''100'')
        OR (T1.AWARD_ID IN(SELECT DISTINCT S1.AWARD_ID FROM AWARD_PERSON_ROLES S1
		INNER JOIN ROLE R1 ON R1.ROLE_ID = S1.ROLE_ID
		INNER JOIN ROLE_RIGHTS R2 ON R2.ROLE_ID = R1.ROLE_ID
		INNER JOIN RIGHTS R3 ON R3.RIGHT_ID = R2.RIGHT_ID AND R3.RIGHT_NAME IN (''VIEW_CLAIMS'',''MODIFY_CLAIMS'') WHERE S1.PERSON_ID=''',AV_PERSON_ID,''' )))' );
ELSEIF AV_TAB_TYPE = 'MANUAL_QUALIFIED_CLAIMS' THEN
		SET JOIN_CONDITION = CONCAT(JOIN_CONDITION,' LEFT JOIN CLAIM T24 ON T24.AWARD_NUMBER = T1.AWARD_NUMBER AND T24.CLAIM_ID IN (SELECT MAX(T27.CLAIM_ID) FROM CLAIM T27
                                    WHERE T27.AWARD_NUMBER = T24.AWARD_NUMBER ) 
									LEFT JOIN PERSON T25 ON T25.USER_NAME = T24.CREATE_USER
									LEFT JOIN PERSON T26 ON T26.USER_NAME = T24.UPDATE_USER
									LEFT OUTER JOIN CUSTOM_DATA_V T29 ON T1.AWARD_ID = T29.MODULE_ITEM_KEY 
                                    LEFT JOIN PERSON T30 ON T30.PERSON_ID = T29.CLAIM_PREPARER
                                    LEFT JOIN CLAIM_STATUS T33 ON T33.CLAIM_STATUS_CODE = T24.CLAIM_STATUS_CODE');
		SET TAB_QUERY = CONCAT(' WHERE T1.AWARD_ID NOT IN (SELECT AWARD_ID FROM CLAIM WHERE CLAIM_STATUS_CODE IN (1,2,3,5))
		AND T1.AWARD_SEQUENCE_STATUS = ''ACTIVE''
        AND T1.STATUS_CODE <> ''5''
        AND T1.ACCOUNT_TYPE_CODE NOT IN (7,3,5)
		AND
		(
			( T1.METHOD_OF_PAYMENT_CODE NOT IN (1)
				OR T1.BASIS_OF_PAYMENT_CODE NOT IN (1,7)
                OR T1.PAYMENT_INVOICE_FREQ_CODE NOT IN (6,10,11,12,5,4)
			)
			OR
			(
			T1.METHOD_OF_PAYMENT_CODE IN (1)
			AND T1.BASIS_OF_PAYMENT_CODE IN (1,7)
			AND T1.AWARD_TYPE_CODE NOT IN (2,4)
			)
		)
		AND ((T1.LEAD_UNIT_NUMBER IN (SELECT DISTINCT UNIT_NUMBER FROM PERSON_ROLE_RT WHERE RIGHT_NAME
		IN (''CLAIM_PREPARER'') AND PERSON_ID =''',AV_PERSON_ID,'''))) ' );
END IF;
IF AV_SORT_TYPE IS NULL THEN
        SET AV_SORT_TYPE =  CONCAT(' ORDER BY T.UPDATE_TIMESTAMP DESC ');
ELSE
    SET AV_SORT_TYPE = CONCAT(' ORDER BY ',AV_SORT_TYPE);
END IF;
IF AV_UNLIMITED = TRUE THEN
        SET LS_OFFSET_CONDITION = '';
ELSE
        SET LS_OFFSET_CONDITION = CONCAT(' LIMIT ',AV_LIMIT,' OFFSET ',LS_OFFSET);
END IF;
IF LS_FILTER_CONDITION <>'' THEN
SET LS_FILTER_CONDITION = CONCAT(' WHERE T.AWARD_SEQUENCE_STATUS NOT IN (''ARCHIVE'', ''CANCELLED'') AND ',LS_FILTER_CONDITION);
SELECT TRIM(TRAILING 'AND ' FROM LS_FILTER_CONDITION) into LS_FILTER_CONDITION from dual;
END IF;
     SET LS_DYN_SQL =CONCAT('SELECT DISTINCT *  FROM(SELECT T1.AWARD_ID,
                                                                        T1.AWARD_NUMBER,
                                                                        T1.ACCOUNT_NUMBER,
                                                                        T1.TITLE,
                                                                        T2.FULL_NAME,
																		COALESCE(T2.PERSON_ID,T2.ROLODEX_ID) AS PI_PERSON_ID,
                                                                        T1.LEAD_UNIT_NUMBER,
                                                                        T3.SPONSOR_CODE,
                                                                        T3.SPONSOR_NAME,
                                                                        T3.ACRONYM,
                                                                        T1.STATUS_CODE,
                                                                        T1.GRANT_HEADER_ID,
																		T1.UPDATE_TIMESTAMP,
                                                                        T8.UNIT_NAME ,
																		T1.AWARD_DOCUMENT_TYPE_CODE AS AWARD_DOCUMENT_TYPE,
																		T1.AWARD_SEQUENCE_STATUS,
																		T1.WORKFLOW_AWARD_STATUS_CODE,
																		T1.SPONSOR_AWARD_NUMBER,
                                                                        T24.CLAIM_ID,
																		T24.CLAIM_NUMBER,
																		T24.CLAIM_STATUS_CODE,
																		T24.CLAIM_SUBMISSION_DATE,
																		T25.FULL_NAME AS CREATE_USERNAME,
																		T26.FULL_NAME AS UPDATE_USERNAME,
																		T24.UPDATE_TIMESTAMP AS CLAIM_UPDATE_TIMESTAMP,
                                                                        T1.BEGIN_DATE AS AWARD_START_DATE,
                                                                        T1.FINAL_EXPIRATION_DATE AS AWARD_END_DATE,
                                                                        T24.END_DATE AS CLAIM_END_DATE,
                                                                        T30.FULL_NAME AS FINANCE_OFFICER,
                                                                        T30.PERSON_ID AS PREPARER_PERSON_ID,
                                                                        T24.CREATE_USER,
																		T33.DESCRIPTION AS CLAIM_STATUS
                                                                        ',SELECTED_FIELD_LIST,
                                    ' FROM AWARD T1
									INNER JOIN  AWARD_PERSONS T2 ON T1.AWARD_ID=T2.AWARD_ID AND T2.PERSON_ROLE_ID = 3
									LEFT JOIN SPONSOR T3 ON T3.SPONSOR_CODE = T1.SPONSOR_CODE
                                    INNER JOIN  UNIT T8 ON T8.UNIT_NUMBER = T1.LEAD_UNIT_NUMBER
                                    LEFT JOIN GRANT_CALL_HEADER T7 ON T7.GRANT_HEADER_ID = T1.GRANT_HEADER_ID
                                    ',JOIN_CONDITION,TAB_QUERY,
                                     ' )T ',LS_FILTER_CONDITION,' ',AV_SORT_TYPE,' ',LS_OFFSET_CONDITION );
SET @QUERY_STATEMENT = LS_DYN_SQL;
PREPARE EXECUTABLE_STAEMENT FROM @QUERY_STATEMENT;
EXECUTE EXECUTABLE_STAEMENT;
END
//
