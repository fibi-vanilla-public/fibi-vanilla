DELIMITER //
CREATE  PROCEDURE `GET_AWARD_HIERARCHY`(
AV_AWARD_NUMBER 	varchar(12)
)
    DETERMINISTIC
BEGIN
DECLARE LS_VAR        LONGTEXT;
DECLARE LS_INTO_VAR   LONGTEXT;
DECLARE LS_CHILD_LIST LONGTEXT;
DECLARE LS_AWARD_NUMBER VARCHAR(20);
SET LS_AWARD_NUMBER = CONCAT(substring_index(AV_AWARD_NUMBER, '-', 1),'-00001');
SET LS_VAR = LS_AWARD_NUMBER;
SET LS_CHILD_LIST = '';
SELECT T2.AWARD_ID,T1.ROOT_AWARD_NUMBER,T1.AWARD_NUMBER, T1.LVL AS LEVEL, T1.PARENT_AWARD_NUMBER, T1.PI_NAME, T2.ACCOUNT_NUMBER, T2.STATUS_CODE,T2.AWARD_SEQUENCE_STATUS,T2.IS_LATEST,T2.WORKFLOW_AWARD_STATUS_CODE,T2.AWARD_SEQUENCE_STATUS,T2.AWARD_DOCUMENT_TYPE_CODE
FROM
( 
 SELECT ROOT_AWARD_NUMBER,AWARD_NUMBER,FN_GET_AWARD_HIERARCHY_LEVEL(AWARD_NUMBER,LS_AWARD_NUMBER) AS LVL,
 PARENT_AWARD_NUMBER,FN_AWARD_GET_PI_FULL_NAME(AWARD_NUMBER) AS PI_NAME
 FROM AWARD_HIERARCHY WHERE ROOT_AWARD_NUMBER = LS_AWARD_NUMBER 
) T1
INNER JOIN AWARD T2 ON T1.AWARD_NUMBER = T2.AWARD_NUMBER
WHERE T2.SEQUENCE_NUMBER IN (
						    SELECT MAX(T3.SEQUENCE_NUMBER) FROM AWARD T3
						    WHERE T2.AWARD_NUMBER = T3.AWARD_NUMBER
                            AND (T3.AWARD_SEQUENCE_STATUS IN('ACTIVE')
                            OR (T3.AWARD_SEQUENCE_STATUS IN('PENDING') AND AWARD_DOCUMENT_TYPE_CODE = 1))
						    )
ORDER BY  T1.LVL,T1.PARENT_AWARD_NUMBER,T1.AWARD_NUMBER;
END
//
