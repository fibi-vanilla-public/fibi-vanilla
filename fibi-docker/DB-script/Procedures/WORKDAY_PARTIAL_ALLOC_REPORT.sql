DELIMITER //
CREATE  PROCEDURE `WORKDAY_PARTIAL_ALLOC_REPORT`()
BEGIN
SELECT T2.AWARD_NUMBER,T3.BUDGET_REFERENCE_NUMBER,T2.PLAN_START_DATE,T2.PLAN_END_DATE,T2.CHARGE_START_DATE,
T2.CHARGE_END_DATE,T2.COST_ALLOCATION,T2.PERSON_ID,T2.POSITION_ID,T2.FULL_NAME,FN_CAMPUS_FOR_UNIT(T4.LEAD_UNIT_NUMBER) AS CAMPUS,T5.DESCRIPTION AS POSITION_STATUS,
T2.DESCRIPTION,T2.IS_REMAINING_CA_FROM_WBS,T2.UPDATE_TIMESTAMP, COALESCE(T7.APPROVER_PERSON_NAME, T9.FULL_NAME) AS SUBMIT_USER,T4.AWARD_ID,T1.WORKDAY_MANPOWER_INTERFACE_ID,
T10.FULL_NAME AS PI_NAME,T11.UNIT_NAME
FROM WORKDAY_MANPOWER_INTERFACE T1 
LEFT JOIN AWARD_MANPOWER_RESOURCE T2
LEFT JOIN AWARD_MANPOWER T3 ON T3.AWARD_MANPOWER_ID = T2.AWARD_MANPOWER_ID
LEFT JOIN AWARD T4 ON T4.AWARD_ID = T3.AWARD_ID ON T2.AWARD_MANPOWER_RESOURCE_ID = T1.AWARD_MANPOWER_RESOURCE_ID 
LEFT JOIN MANPOWER_POSITION_STATUS T5 ON T5.POSITION_STATUS_CODE = T2.POSITION_STATUS_CODE
LEFT JOIN WORKFLOW T6 ON T6.MODULE_ITEM_ID = T3.AWARD_ID AND T6.MODULE_CODE = '1'
LEFT JOIN WORKFLOW_DETAIL T7 ON T7.WORKFLOW_ID = T6.WORKFLOW_ID  AND T7.WORKFLOW_DETAIL_ID = (SELECT MAX(WORKFLOW_DETAIL_ID) 
																							FROM WORKFLOW_DETAIL
																							 WHERE WORKFLOW_ID = T6.WORKFLOW_ID 
																							 AND ROLE_TYPE_CODE= '22' 
																							 AND APPROVAL_STATUS ='A') 
LEFT JOIN PERSON T9 ON T9.USER_NAME = T4.SUBMIT_USER
LEFT JOIN AWARD_PERSONS T10 ON T10.AWARD_ID = T4.AWARD_ID AND T10.PI_FLAG ='Y'
LEFT JOIN UNIT T11 ON T11.UNIT_NUMBER = T4.LEAD_UNIT_NUMBER
WHERE T1.INTERFACE_TYPE_CODE IN(2,17)
AND T2.PERSON_ID IS NOT NULL
AND T2.COST_ALLOCATION < 100
AND T1.IS_MAIL_ACTIVE = 'N';
END
//
