DELIMITER //
CREATE  PROCEDURE `GET_AWARD_PAYMENT`(
IN AV_AWARD_ID INT)
    DETERMINISTIC
BEGIN
 SELECT T1.AWARD_ID,
       T1.AWARD_NUMBER ,
       T1.BASIS_OF_PAYMENT_CODE,
       T2.DESCRIPTION AS PAYMENT_BASIS,
       T1.METHOD_OF_PAYMENT_CODE,
       T3.DESCRIPTION AS PAYMENT_METHOD
    FROM AWARD T1
    LEFT OUTER JOIN AWARD_BASIS_OF_PAYMENT T2 ON T1.BASIS_OF_PAYMENT_CODE = T2.BASIS_OF_PAYMENT_CODE
    LEFT OUTER JOIN AWARD_METHOD_OF_PAYMENT T3 ON T1.METHOD_OF_PAYMENT_CODE = T3.METHOD_OF_PAYMENT_CODE
    WHERE T1.AWARD_ID=AV_AWARD_ID;
END
//
