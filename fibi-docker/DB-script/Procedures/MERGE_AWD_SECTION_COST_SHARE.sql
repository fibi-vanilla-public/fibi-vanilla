DELIMITER //
CREATE  PROCEDURE `MERGE_AWD_SECTION_COST_SHARE`(
AV_AWARD_ID 			INT,
AV_AWARD_NUMBER			VARCHAR(12),
AV_SEQUENCE_NUMBER		INT(4),
AV_VARIATION_TYPE_CODE	VARCHAR(3),
AV_UPDATE_USER			VARCHAR(60)
)
BEGIN
DECLARE LS_ERROR_MSG	VARCHAR(1000);
DECLARE LS_TABLE_NAME	VARCHAR(30);
DECLARE LI_FLAG			INT;
DECLARE LI_AWARD_COST_SHARE_ID    INT ;
DECLARE LS_AWARD_NUMBER   VARCHAR(12)  ;
DECLARE LI_SEQUENCE_NUMBER    INT(8) ;
DECLARE LS_PROJECT_PERIOD    VARCHAR(4) ;
DECLARE LI_COST_SHARE_PERCENTAGE  	DECIMAL(5,2)   ;
DECLARE LI_COST_SHARE_TYPE_CODE  INT(3)   ;
DECLARE LS_SOURCE   VARCHAR(32)  ;
DECLARE LS_COMMENTS   VARCHAR(4000)  ;
DECLARE LS_DESTINATION  VARCHAR(32)   ;
DECLARE LI_COMMITMENT_AMOUNT  DECIMAL(12,2)   ;
DECLARE LD_VERIFICATION_DATE  DATETIME   ;
DECLARE LI_COST_SHARE_MET   DECIMAL(12,2)  ;
DECLARE LS_UNIT_NUMBER   VARCHAR(8)  ;
DECLARE LD_UPDATE_TIMESTAMP  DATETIME   ;
DECLARE LS_UPDATE_USER   VARCHAR(60) ;
DECLARE LI_COST_SHARE_DISTRIBUTABLE  DECIMAL(12,2);
DECLARE LI_MASTER_AWARD_ID	DECIMAL(22,0);
DECLARE LI_AWARD_ID decimal(22,0);
DECLARE LS_DOCUMENT_UPDATE_USER			VARCHAR(60);
DECLARE LS_DOCUMENT_UPDATE_TIMESTAMP	DATETIME;
		BEGIN
				DECLARE EXIT HANDLER FOR SQLEXCEPTION
				BEGIN
					GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, 
					 @errno = MYSQL_ERRNO, @msg = MESSAGE_TEXT;
				SET @full_error = CONCAT(@msg,'|SEC111|',LS_TABLE_NAME );
				SELECT @full_error INTO LS_ERROR_MSG;
				RESIGNAL SET MESSAGE_TEXT = LS_ERROR_MSG;
				END;
				SET SQL_SAFE_UPDATES = 0;
				SET LS_TABLE_NAME = 'AWARD_COST_SHARE';
				SELECT COUNT(1) INTO LI_FLAG
				FROM AWARD 
				WHERE AWARD_NUMBER = AV_AWARD_NUMBER
				AND SEQUENCE_NUMBER = 0;
				IF LI_FLAG > 0 THEN
					SELECT AWARD_ID INTO LI_MASTER_AWARD_ID
					FROM AWARD 
					WHERE AWARD_NUMBER = AV_AWARD_NUMBER
					AND SEQUENCE_NUMBER = 0;
				END IF;
				DELETE FROM AWARD_COST_SHARE 
				WHERE AWARD_ID = LI_MASTER_AWARD_ID;
				BEGIN
					DECLARE DONE1 INT DEFAULT FALSE;
					DECLARE CUR_AWARD_COST_SHARE CURSOR FOR
					SELECT AWARD_COST_SHARE_ID,AWARD_ID,AWARD_NUMBER,SEQUENCE_NUMBER,
					PROJECT_PERIOD,COST_SHARE_PERCENTAGE,COST_SHARE_TYPE_CODE,
					SOURCE,COMMENTS,DESTINATION,
					COMMITMENT_AMOUNT,VERIFICATION_DATE,COST_SHARE_MET,
					UNIT_NUMBER,UPDATE_TIMESTAMP,UPDATE_USER,
					COST_SHARE_DISTRIBUTABLE
					FROM AWARD_COST_SHARE 
					WHERE AWARD_ID = AV_AWARD_ID;
					DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE1 = TRUE;
					OPEN CUR_AWARD_COST_SHARE;
					INSERT_AWARD_COST_SHARE_LOOP: LOOP 
					FETCH CUR_AWARD_COST_SHARE INTO
					LI_AWARD_COST_SHARE_ID,
					LI_AWARD_ID,
					LS_AWARD_NUMBER,
					LI_SEQUENCE_NUMBER,
					LS_PROJECT_PERIOD,
					LI_COST_SHARE_PERCENTAGE,
					LI_COST_SHARE_TYPE_CODE,
					LS_SOURCE,
					LS_COMMENTS,
					LS_DESTINATION,
					LI_COMMITMENT_AMOUNT,
					LD_VERIFICATION_DATE,
					LI_COST_SHARE_MET,
					LS_UNIT_NUMBER,
					LD_UPDATE_TIMESTAMP,
					LS_UPDATE_USER,
					LI_COST_SHARE_DISTRIBUTABLE;
					IF DONE1 THEN
						LEAVE INSERT_AWARD_COST_SHARE_LOOP;
					END IF;
						INSERT INTO AWARD_COST_SHARE
						(AWARD_ID,AWARD_NUMBER,SEQUENCE_NUMBER,
						PROJECT_PERIOD,COST_SHARE_PERCENTAGE,COST_SHARE_TYPE_CODE,
						SOURCE,COMMENTS,DESTINATION,
						COMMITMENT_AMOUNT,VERIFICATION_DATE,COST_SHARE_MET,
						UNIT_NUMBER,UPDATE_TIMESTAMP,UPDATE_USER,
						COST_SHARE_DISTRIBUTABLE)
						VALUES
						(
						LI_MASTER_AWARD_ID,
						LS_AWARD_NUMBER,
						0,
						LS_PROJECT_PERIOD,
						LI_COST_SHARE_PERCENTAGE,
						LI_COST_SHARE_TYPE_CODE,
						LS_SOURCE,
						LS_COMMENTS,
						LS_DESTINATION,
						LI_COMMITMENT_AMOUNT,
						LD_VERIFICATION_DATE,
						LI_COST_SHARE_MET,
						LS_UNIT_NUMBER,
						LD_UPDATE_TIMESTAMP,
						LS_UPDATE_USER,
						LI_COST_SHARE_DISTRIBUTABLE);
					END LOOP;
					CLOSE CUR_AWARD_COST_SHARE;
				END;
		END;
END
//
