DELIMITER //
CREATE  PROCEDURE `DELETE_CLAIM_RECORD`( AV_CLAIM_ID INT(10))
BEGIN
DELETE FROM CLAIM_ATTACHMENT WHERE CLAIM_ID = AV_CLAIM_ID;
DELETE FROM CLAIM_INVOICE_DETAILS WHERE CLAIM_ID = AV_CLAIM_ID;
DELETE FROM CLAIM_INVOICE WHERE CLAIM_ID = AV_CLAIM_ID;
DELETE FROM CLAIM_ACTION_LOG WHERE CLAIM_ID = AV_CLAIM_ID;
DELETE FROM CLAIM_SUMMARY_DETAILS WHERE CLAIM_SUMMARY_ID IN(SELECT CLAIM_SUMMARY_ID FROM CLAIM_SUMMARY WHERE CLAIM_ID = AV_CLAIM_ID);
DELETE FROM CLAIM_SUMMARY WHERE CLAIM_ID = AV_CLAIM_ID;
DELETE FROM  CLAIM_MANPOWER WHERE CLAIM_ID = AV_CLAIM_ID;
DELETE FROM CLAIM WHERE CLAIM_ID = AV_CLAIM_ID;
END
//
