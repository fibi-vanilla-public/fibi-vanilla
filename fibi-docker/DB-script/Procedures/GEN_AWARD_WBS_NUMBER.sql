DELIMITER //
CREATE  PROCEDURE `GEN_AWARD_WBS_NUMBER`(
AV_AWARD_ID             INT,
AV_ACCOUNT_NUMBER       VARCHAR(20),
AV_BUDGET_HEADER_ID  	INT(12),
AV_BUDGET_CATEGORY_CODE VARCHAR(3),
AV_TYPE                 VARCHAR(1)
)
    DETERMINISTIC
BEGIN
DECLARE DONE1 INT DEFAULT FALSE;
DECLARE DONE2 INT DEFAULT FALSE;
DECLARE LS_AWARD_WBS_NUMBER 			VARCHAR(20);
DECLARE LS_SPONSOR_TYPE_CODE 			VARCHAR(3);
DECLARE LS_INPUT_GST_CATEGORY 			VARCHAR(30);
DECLARE LS_WBS_CODE						VARCHAR(1);
DECLARE LI_NEXT_NUMBER					INT(6);
DECLARE LS_LEAD_UNIT_NUMBER 			VARCHAR(8); 
DECLARE LS_INCR_NEXT_NUMBER 			VARCHAR(6);
DECLARE LS_ACCOUNT_NUMBER				VARCHAR(20);
DECLARE LS_LINE_ITEM_NEW_SEQ 			VARCHAR(2);
DECLARE LI_LINE_ITEM_SEQ 				INT;
DECLARE LS_BUDGET_CATEGORY_TYPE_CODE	VARCHAR(3);
DECLARE LI_BUDGET_PERIOD_ID 			INT(12);
DECLARE LS_BUDGET_CATEGORY_CODE 	    VARCHAR(3);
DECLARE LS_COST_ELEMENT					VARCHAR(12);
DECLARE LI_BUDGET_DETAILS_ID 			INT(12);
DECLARE LI_COUNT 						INT;
DECLARE LS_BUDGET_PERIOD_ID 			VARCHAR(4000);
DECLARE LI_BUDGET_HEADER_ID             INT(12); 
SELECT ACCOUNT_NUMBER INTO LS_ACCOUNT_NUMBER
FROM AWARD WHERE AWARD_ID = AV_AWARD_ID;
IF AV_TYPE = 'Y' THEN
		SELECT SPONSOR_TYPE_CODE INTO  LS_SPONSOR_TYPE_CODE
		FROM SPONSOR WHERE SPONSOR_CODE = (
		SELECT SPONSOR_CODE FROM AWARD WHERE AWARD_ID = AV_AWARD_ID);
		IF LS_SPONSOR_TYPE_CODE IS NULL THEN
			SELECT  'ER01';
		END IF;
		SELECT COUNT(NEXT_NUMBER) INTO LI_COUNT
		FROM WBS_GEN_NEXTNUM_FOR_SPONS_TYPE WHERE SPONSOR_TYPE_CODE = LS_SPONSOR_TYPE_CODE;
		IF LI_COUNT = 0 THEN
			INSERT INTO WBS_GEN_NEXTNUM_FOR_SPONS_TYPE (SPONSOR_TYPE_CODE, NEXT_NUMBER, UPDATE_TIMESTAMP, UPDATE_USER) 
			VALUES (LS_SPONSOR_TYPE_CODE,1,NOW(),'quickstart');
			SET LI_NEXT_NUMBER = 1;
		ELSE
			SELECT NEXT_NUMBER INTO LI_NEXT_NUMBER
			FROM WBS_GEN_NEXTNUM_FOR_SPONS_TYPE WHERE SPONSOR_TYPE_CODE = LS_SPONSOR_TYPE_CODE;
		END IF;
		UPDATE WBS_GEN_NEXTNUM_FOR_SPONS_TYPE SET NEXT_NUMBER = LI_NEXT_NUMBER+1 WHERE SPONSOR_TYPE_CODE = LS_SPONSOR_TYPE_CODE ;
		SELECT VALUE INTO LS_INPUT_GST_CATEGORY
		 FROM CUSTOM_DATA 
		WHERE  MODULE_ITEM_CODE = 1 
		AND MODULE_SUB_ITEM_CODE = 0 
		AND MODULE_ITEM_KEY = AV_AWARD_ID
		AND MODULE_SUB_ITEM_KEY =0
		AND COLUMN_ID = 5 
		AND COLUMN_VERSION_NUMBER = (SELECT MAX(COLUMN_VERSION_NUMBER) FROM CUSTOM_DATA 
										WHERE COLUMN_ID = 5 
										AND MODULE_ITEM_CODE = 1 
										AND MODULE_SUB_ITEM_CODE = 0 
										AND MODULE_ITEM_KEY = AV_AWARD_ID
										AND MODULE_SUB_ITEM_KEY =0);
		IF LS_INPUT_GST_CATEGORY IS NULL THEN
			SELECT 'ER02';
		END IF;
		 SELECT WBS_CODE INTO LS_WBS_CODE
		 FROM WBS_GEN_INPUT_GST_MAPPING WHERE GST_CATEGORY = LS_INPUT_GST_CATEGORY;
		IF LS_WBS_CODE IS NULL THEN
			SELECT 'ER03';
		END IF;
		 SELECT LPAD(LI_NEXT_NUMBER, 6, "0") INTO LS_INCR_NEXT_NUMBER;
		 SELECT LEAD_UNIT_NUMBER INTO LS_LEAD_UNIT_NUMBER
		 FROM AWARD WHERE AWARD_ID = AV_AWARD_ID;
		 IF LS_LEAD_UNIT_NUMBER IS NULL THEN
		 	SELECT 'ER04';
		 END IF;
		 SELECT CONCAT('04',LS_SPONSOR_TYPE_CODE,LS_WBS_CODE,LS_INCR_NEXT_NUMBER,LS_LEAD_UNIT_NUMBER) INTO LS_ACCOUNT_NUMBER;
		 UPDATE AWARD SET ACCOUNT_NUMBER = LS_ACCOUNT_NUMBER WHERE AWARD_ID = AV_AWARD_ID;
		 SELECT BUDGET_HEADER_ID INTO LI_BUDGET_HEADER_ID 
		 FROM AWARD_BUDGET_HEADER 
		 WHERE AWARD_ID = AV_AWARD_ID; 
		 SELECT GROUP_CONCAT(BUDGET_PERIOD_ID SEPARATOR ',')  INTO LS_BUDGET_PERIOD_ID 
		 FROM AWARD_BUDGET_PERIOD 
		 WHERE BUDGET_HEADER_ID = LI_BUDGET_HEADER_ID;
		 BEGIN
				DECLARE BUDGET_DETAIL_CURSOR CURSOR FOR 
				SELECT BUDGET_CATEGORY_CODE,COST_ELEMENT,BUDGET_DETAILS_ID
				FROM  AWARD_BUDGET_DETAIL 
				WHERE   FIND_IN_SET(BUDGET_PERIOD_ID,LS_BUDGET_PERIOD_ID);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE1 = TRUE;
				OPEN BUDGET_DETAIL_CURSOR;
				BUDGET_DETAIL_CURSOR_LOOP : LOOP
						FETCH BUDGET_DETAIL_CURSOR INTO  LS_BUDGET_CATEGORY_CODE,LS_COST_ELEMENT,LI_BUDGET_DETAILS_ID; 
						IF DONE1 THEN
							LEAVE BUDGET_DETAIL_CURSOR_LOOP;
						END IF;  
						SELECT BUDGET_CATEGORY_TYPE_CODE INTO LS_BUDGET_CATEGORY_TYPE_CODE 
						FROM BUDGET_CATEGORY WHERE BUDGET_CATEGORY_CODE = LS_BUDGET_CATEGORY_CODE;
						COMMIT;
						SELECT COUNT(T1.INTERNAL_ORDER_CODE) INTO LI_COUNT
						FROM  AWARD_BUDGET_DETAIL T1 LEFT JOIN BUDGET_CATEGORY T2 ON T1.BUDGET_CATEGORY_CODE = T2.BUDGET_CATEGORY_CODE
						WHERE T1.BUDGET_PERIOD_ID IN (LS_BUDGET_PERIOD_ID)
						AND T2.BUDGET_CATEGORY_TYPE_CODE = LS_BUDGET_CATEGORY_TYPE_CODE
						AND T1.INTERNAL_ORDER_CODE IS NOT NULL;
						IF LI_COUNT = 0 THEN
							SET LI_LINE_ITEM_SEQ = 1;
						ELSE
							SELECT MAX(SUBSTRING(INTERNAL_ORDER_CODE,-2)+1 ) INTO LI_LINE_ITEM_SEQ
							FROM  AWARD_BUDGET_DETAIL T1 LEFT JOIN BUDGET_CATEGORY T2 ON T1.BUDGET_CATEGORY_CODE = T2.BUDGET_CATEGORY_CODE
							WHERE T1.BUDGET_PERIOD_ID IN (LS_BUDGET_PERIOD_ID)
							AND T2.BUDGET_CATEGORY_TYPE_CODE = LS_BUDGET_CATEGORY_TYPE_CODE
							AND T1.INTERNAL_ORDER_CODE IS NOT NULL;
						END IF;
						SELECT LPAD(LI_LINE_ITEM_SEQ, 2, "0") INTO LS_LINE_ITEM_NEW_SEQ;
						UPDATE AWARD_BUDGET_DETAIL 
						SET INTERNAL_ORDER_CODE = CONCAT(LS_ACCOUNT_NUMBER,LS_BUDGET_CATEGORY_TYPE_CODE,LS_LINE_ITEM_NEW_SEQ)
						WHERE BUDGET_DETAILS_ID = LI_BUDGET_DETAILS_ID ;
						COMMIT;
		 		END LOOP;
				CLOSE BUDGET_DETAIL_CURSOR;
		 END;	
END IF;
END
//
